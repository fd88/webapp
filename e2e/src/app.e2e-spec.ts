import {
	URL_CHECK,
	URL_GONKA,
	URL_INIT,
	URL_INSTANT,
	URL_LOTTERIES,
	URL_REGISTRY, URL_RESULTS,
	URL_TICKETS,
	URL_ZABAVA
} from '../../src/app/util/route-utils';

import { AppPage } from './app.po';
import { browser, by, element, ElementFinder } from 'protractor';

xdescribe('ANDROID LOTTERY WEB TERMINAL', () => {
	let page: AppPage;
	const timeOut = 5000;
	const DOM_TIMEOUT = 120000;
	let url: string;
	let elem: ElementFinder;
	let nearestDraw;

	beforeEach(() => {
		page = new AppPage();
		jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;
	});

	it('Проверка окна логина (заголовок, поля ввода, кнопка логина)', async () => {
		await page.navigateTo();
		await page.setPause(timeOut);

		await expect(page.getAuthHeaderText()).toEqual('ВХІД');
		await expect(page.getAuthInputLogin().isPresent()).toBeTruthy();
		await expect(page.getAuthInputPassword().isPresent()).toBeTruthy();
		await expect(page.getAuthButton().isPresent()).toBeTruthy();
	});

	it('Тест авторизации', async () => {
		// логинимся в приложение
		await page.loginToApp();

		// закрываем модальные окна ченджлога и джекпотов, если есть
		await page.closeModals();

		// Ставим русский язык
		await page.setDefaultLang();

		const v = await page.getCurrentUrl();
		await expect(v.endsWith(URL_LOTTERIES)).toBe(true);
	});

	xit('Регистрация билета лотереи Лото-Забава с последующей отменой', async () => {
		// 1. Нажать иконку лотереи «Лото-Забава».
		await page.selectLottery('Лото-Забава');

		// 2. Выбрать количество билетов — 3.
		await page.click('.zc-bg-tickets .bgc-button', 2);

		// 3. Выбрать парочку — 2.
		await page.click('.zc-bg-pairs .bgc-button', 1);

		// 4. Выбрать Богатые и известные.
		await page.click('.zc-bg-studio .bgc-button');

		// выбранный тираж
		nearestDraw = await (await page.el('.dbc-number')).getText();

		// 5. Нажать кнопку «К оплате»
		await page.click('app-register-bet-button button');

		// Открыто окно подтверждения регистрации билета лотереи «Лото-Забава».
		await page.el('app-check-information');
		url = await page.getCurrentUrl();
		await expect(url.indexOf(URL_REGISTRY)).toBeGreaterThan(-1);

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage([nearestDraw, 'Да', '2', '3']);

		// 6. Нажать кнопку «Назад» (она первая по счету)
		await page.click('.ci-buttons app-green-button button', 1);

		// Открыто окно регистрации «Лото-Забава»
		url = await page.getCurrentUrl();
		await expect(url.endsWith(`${URL_LOTTERIES}/${URL_ZABAVA}/${URL_INIT}`)).toBeTruthy();

		// Выбран ближайший тираж.
		elem = await page.el('.dbc-number');
		await expect(await (await elem.getText()).trim()).toEqual(nearestDraw);

		// Выбрано количество — 3.
		elem = await page.el('.zc-bg-tickets .bgc-button', 2);
		await expect(await page.hasClass(elem, 'bgc-button-selected')).toBeTruthy();

		// Выбрано парочек — 2.
		elem = await page.el('.zc-bg-pairs .bgc-button', 1);
		await expect(await page.hasClass(elem, 'bgc-button-selected')).toBeTruthy();

		// Выбрано Богатые и известные
		elem = await page.el('.zc-bg-studio .bgc-button');
		await expect(await page.hasClass(elem, 'bgc-button-selected')).toBeTruthy();

		// 7. Нажать кнопку «К оплате»
		await page.click('app-register-bet-button button');

		// 8. Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Лото-Забава / Регистрация ставок / 3 / Успешно
		await page.testCalculator('Лото-Забава', 'Регистрация ставок', '3', '66.00 грн', 'Успешно');

		// 9. Нажать кнопку «Меню» (гамбургер меню).
		await page.click('.hc-hamburger-button');

		// 10. Нажать кнопку «Отмена операции».
		const cancelBtn = await page.el('.cancel-last');
		await browser.wait(page.until.visibilityOf(cancelBtn), DOM_TIMEOUT, `Элемент .cancel-last слишком долго не показывается`);
		await page.click('.cancel-last');

		// 11. Нажать «Укажите причину отмены последней операции»
		await page.click('.dlc-title');

		// 12. Нажать «Отказ покупателя»
		await page.click('.dlc-item[value="11"]');

		// 13. Нажать кнопку«Отменить»
		await page.click('.modal-buttons-container app-green-button button.green-fill');

		// Открыто поп-ап окно «Отмена операции прошла успешно»
		elem = await page.el('.modal-alt-message');
		await browser.wait(page.until.textToBePresentInElement(elem, 'Отмена операции прошла успешно'), DOM_TIMEOUT, `Элемент слишком долго не показывается`);

		// 14. Нажать кнопку «Продолжить»
		await page.click('.modal-buttons-container app-green-button button.green-fill');

		// Открыта вкладка «Лотереи»
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Лото-Забава / Отмена регистрации / 3 / Успешно
		await page.testCalculator('Лото-Забава', 'Отмена регистрации', '3', '-66.00 грн', 'Успешно');

		// Очистим калькулятор, чтоб дальше небыло ошибок
		await page.click('.tc-clear button');
	});

	it('Регистрация билета лотереи «Горячая Штучка» с последующей попыткой отмены и проверкой на выигрыш.', async () => {
		// 1. Нажать иконку лотерей «Моменталки».
		await page.selectLottery('Швидкограй');

		// 2. Выбрать в фильтре стоимость - 20 грн.
		elem = await page.el('.il-bg-filters');
		const elemValuesStr = await elem.getAttribute('ng-reflect-values-array');
		const elemValues = elemValuesStr.split(',');
		const index20 = elemValues.indexOf('20');
		await page.click('.il-bg-filters .bgc-button', index20);

		// 3. Выбрать лотерею «Горячая Штучка».
		await page.click('.igc-game-item-content.game-code-144');

		// 4. Выбрать количество билетов — 3.
		await page.click('.il-bg-tickets .bgc-button', 2);

		// 5. Выбрать серию — 7.
		await browser.executeScript('document.querySelector("app-custom-scroll.bets-scroll .custom-scrollbar__inner").scrollTo(0, 10000);');
		elem = await element.all(by.css('.draws-item')).filter(el => {
			return el.element(by.cssContainingText('.draws-item .draws-item__serie_num', '7')).isPresent();
		}).get(0);
		await elem.click();

		// 6. Нажать кнопку «К оплате».
		await page.click('app-register-bet-button');

		// Открыто окно подтверждения регистрации билета лотереи «Гаряча штучка!».
		await page.el('app-check-information');
		url = await page.getCurrentUrl();
		await expect(url.indexOf(URL_REGISTRY)).toBeGreaterThan(-1);

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['Гаряча штучка!', '7', '3', '60.00 грн']);

		// 7. Нажать кнопку «Назад».
		await page.click('app-green-button button', 1);

		// Открыто окно регистрации билета лотереи «Гаряча штучка!».
		url = await page.getCurrentUrl();
		await expect(url.endsWith(`${URL_LOTTERIES}/${URL_INSTANT}/${URL_INIT}/1`)).toBeTruthy();

		// Выбран фильтр стоимости — 20.
		elem = await page.el('.il-bg-filters .bgc-button', index20);
		await expect(await page.hasClass(elem, 'bgc-button-selected')).toBeTruthy();

		// Выбрана лотерея «Гаряча штучка!».
		elem = await page.el('.igc-game-item-content.game-code-144');
		await expect(await page.hasClass(elem, 'game-selected')).toBeTruthy();

		// Выбрано количество билетов — 3.
		elem = await page.el('.il-bg-tickets .bgc-button', 2);
		await expect(await page.hasClass(elem, 'bgc-button-selected')).toBeTruthy();

		// Выбрана серия — 7.
		elem = await element.all(by.css('.draws-item')).filter(el => {
			return el.element(by.cssContainingText('.draws-item .draws-item__serie_num', '7')).isPresent();
		}).get(0);
		await expect(await page.hasClass(elem, 'draws-item_selected')).toBeTruthy();

		// 7. Нажать кнопку «К оплате»
		await page.click('app-register-bet-button button');

		// 8. Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// 10. Нажать кнопку «Меню» (гамбургер меню).
		await page.click('.hc-hamburger-button');

		// 11. Нажать кнопку «Отмена операции».
		// Ничего не происходит.
		const cancelBtn = await page.el('.cancel-last');
		await browser.wait(page.until.visibilityOf(cancelBtn), DOM_TIMEOUT, `Элемент .cancel-last слишком долго не показывается`);
		await expect((await page.el('.cancel-last')).isEnabled()).toBeTruthy();

		// получить мак-код
		const maccode = (await page.el('.tco-action-row')).getAttribute('data-maccode');

		// 12. Нажать кнопку «Проверка билетов».
		await page.click('.main-navigation-container li[value="3"]');

		// 13. Ввести мак-код последнего распечатанного билета.
		(await page.el('app-msl-input-with-keyboard input')).sendKeys(maccode);

		// 14. Нажать кнопку «Проверить».
		await page.click('app-green-button button', 1);

		// получить сумму выигрыша
		const winSum = parseInt(await (await page.el('.tpc-info-value')).getText(), 10);

		if (winSum === 0) {
			// 14а. Нажать кнопку «Продолжить».
			await page.click('.tpc-controls-panel app-green-button button');
		} else if ((winSum >= 1) && (winSum <= 1000)) {
			// 14б. Нажать кнопку «Выплатить»
			await page.click('app-green-button', 2);

			// Открыто поп-ап окно "Выигрыш выплачен"
			elem = await page.el('.modal-alt-message');
			await browser.wait(page.until.textToBePresentInElement(elem, 'Выигрыш выплачен'), DOM_TIMEOUT, `Элемент слишком долго не показывается`);

			// 15. Нажать кнопку «Продолжить».
			await page.click('.modal-buttons-container app-green-button');
		} else {
			// 14в. Нажать кнопку «Продолжить».
			await page.click('.tpc-controls-panel app-green-button button');
		}

		// Открыта вкладка «Проверка билетов».
		url = await page.getCurrentUrl();
		await expect(url.endsWith(`${URL_TICKETS}/${URL_CHECK}`)).toBeTruthy();
	});

	it('ТС №4. Передача средств. Оператор менеджеру, менеджер оператору.', async () => {
		// 2. Кликнуть по кнопке вкладки «Другое».
		await page.click('app-central-menu li[value="5"]');

		// 3. Кликнуть по меню «Передача средств».
		await page.click('app-reporting-menu > .reporting-menu-container > ul > li .reporting-menu-list__title', 1);

		// 4. Ввести логин менеджера принимающего средства.
		elem = await page.el('.rp-columns__right app-input-operation-field input');
		await page.keyBoardInput(elem, browser.params.credentials.manager.login);

		// 5. Ввести сумму (100).
		elem = await page.el('.rp-columns__right app-input-operation-field input', 1);
		await page.keyBoardInput(elem, '100');

		// 6. Нажать кнопку «Запросить».
		await page.click('.rp-columns__right app-green-button');

		// Информация о передаче средств с номером операции.
		elem = await page.el('.reporting-navigation-content.bc-list-expanded.with-results');
		await browser.wait(page.until.visibilityOf(elem), DOM_TIMEOUT, `Информация о передаче средств с номером операции слишком долго не показывается`);
		elem = await page.el('.rp-scroll .result i', -1);
		const checkStrNo = (await elem.getText()).trim();
		const checkNo = /\d+/.exec(checkStrNo);

		// 7. Нажать кнопку «Печать».
		await page.click('app-green-button.rnc-print-button');

		// Вернулись в начальное состояние.
		elem = await page.el('.modal-dialog-background');
		await browser.wait(page.until.invisibilityOf(elem), DOM_TIMEOUT, `Модальный диалог слишком долго не скрывается`);

		// 8. Нажать кнопку «Выход из сеанса».
		await page.click('app-log-out-button .log-out-container');

		// 9. Кликнуть по «Укажите причину завершения сеанса терминала».
		await page.click('.dlc-title');

		// 10. Кликнуть по «Перерыв на обед».
		await page.click('.dlc-item[value="1"]');

		// 11. Нажать кнопку «Выйти».
		await page.click('.modal-buttons-container app-green-button button.green-fill');

		// 12. Авторизоваться под ролью «Менеджер».
		await page.loginToApp('manager');

		// закрываем модальные окна ченджлога и джекпотов, если есть
		await page.closeModals();

		// Ставим русский язык
		await page.setDefaultLang();

		// 13. Кликнуть по кнопке вкладки «Другое».
		await page.click('app-central-menu li[value="5"]');

		// 14. Кликнуть по меню «Передача средств».
		await page.click('app-reporting-menu > .reporting-menu-container > ul > li .reporting-menu-list__title', 2);

		// 15. Кликнуть по меню «Підтвердження прийому грошей».
		await page.click('.rp-menu__item[value="1"]', 2);

		// 16. Ввести логин оператора передающего средства.
		elem = await page.el('app-input-operation-field input', 0);
		await page.keyBoardInput(elem, browser.params.credentials.operator.login);

		// 17. Ввести сумму (100).
		elem = await page.el('app-input-operation-field input', 1);
		await page.keyBoardInput(elem, '100');

		// 18. Ввести номер операции (из п.6)
		elem = await page.el('app-input-operation-field input', 2);
		await page.keyBoardInput(elem, checkNo[0]);

		// 19. Нажать кнопку «Запросить».
		await page.click('app-green-button.rp-request');

		// Ждем, пока не появится кнопка печати
		await page.el('app-green-button.rnc-print-button');

		// Информация про передачу денег.
		elem = await page.el('.rp-scroll .result');
		let innerText = await elem.getText();
		await expect(!!innerText).toBeTruthy();

		// 20. Нажать кнопку «Печать».
		await page.click('app-green-button.rnc-print-button');

		// Распечатается квитанция.
		// Признак - вернемся на стартовый экран и там должна быть кнопка Запросить
		await page.el('app-green-button.rp-request');

		// 21. Кликнуть по меню «Передача грошей»
		await page.click('.rp-menu__item[value="0"]', 2);

		// 22. Ввести логин оператора принимающего средства.
		elem = await page.el('app-input-operation-field input', 0);
		await page.keyBoardInput(elem, browser.params.credentials.operator.login);

		// 23. Ввести сумму (1438).
		elem = await page.el('app-input-operation-field input', 1);
		await page.keyBoardInput(elem, '1438');

		// 24. Нажать кнопку «Запросить».
		await page.click('app-green-button.rp-request');

		// Ждем, пока не появится кнопка печати
		await page.el('app-green-button.rnc-print-button');

		// Информация о передаче средств с номером операции.
		elem = await page.el('.rp-scroll .result');
		innerText = await elem.getText();
		elem = await page.el('.rp-scroll .result i', -1);
		const checkStrNo2 = (await elem.getText()).trim();
		const checkNo2 = /\d+/.exec(checkStrNo2);
		await expect(!!innerText).toBeTruthy();

		// 25. Нажать кнопку «Печать».
		await page.click('app-green-button.rnc-print-button');

		// Распечатается квитанция.
		// Признак - вернемся на стартовый экран и там должна быть кнопка Запросить
		await page.el('app-green-button.rp-request');

		// 26. Нажать кнопку «Выход из сеанса».
		await page.click('app-log-out-button .log-out-container');

		// 27. Нажать кнопку «Выйти».
		await page.click('.modal-buttons-container app-green-button button.green-fill');

		// 28. Авторизоваться под ролью «Оператор».
		// логинимся в приложение
		await page.loginToApp();

		// закрываем модальные окна ченджлога и джекпотов, если есть
		await page.closeModals();

		// Ставим русский язык
		await page.setDefaultLang();

		// 29. Кликнуть по кнопке вкладки «Другое».
		await page.click('app-central-menu li[value="5"]');

		// 30. Кликнуть по меню «Передача средств».
		await page.click('app-reporting-menu > .reporting-menu-container > ul > li .reporting-menu-list__title', 1);

		// 31. Кликнуть по меню «Підтвердження прийому грошей»
		await page.click('.rp-menu__item[value="1"]', 1);

		// 32. Ввести логин менеджера передающего средства.
		elem = await page.el('app-input-operation-field input', 0);
		await page.keyBoardInput(elem, browser.params.credentials.manager.login);

		// 33. Ввести сумму (1438).
		elem = await page.el('app-input-operation-field input', 1);
		await page.keyBoardInput(elem, '1438');

		// 34. Ввести номер операции (из п.24)
		elem = await page.el('app-input-operation-field input', 2);
		await page.keyBoardInput(elem, checkNo2[0]);

		// 35. Нажать кнопку «Запросить».
		await page.click('app-green-button.rp-request');

		// Ждем, пока не появится кнопка печати
		await page.el('app-green-button.rnc-print-button');

		// Информация о передаче средств с номером операции.
		elem = await page.el('.rp-scroll .result');
		innerText = await elem.getText();
		await expect(!!innerText).toBeTruthy();

		// 36. Нажать кнопку «Печать».
		// Распечатается квитанция.
		await page.click('app-green-button.rnc-print-button');

		// Распечатается квитанция.
		// Признак - вернемся на стартовый экран и там должна быть кнопка Запросить
		await page.el('app-green-button.rp-request');
	});

	it('ТС №5.1 Регистрация билетов лотереи: Лото-Забава', async () => {

		// переходим на начальный экран
		await page.click('.hc-home-button');

		// Нажать иконку лотереи «Лото-Забава».
		await page.selectLottery('Лото-Забава');

		// Выбрать количество билетов — 1.
		await page.click('.zc-bg-tickets .bgc-button', 0);

		// Выбрать парочку — 2.
		await page.click('.zc-bg-pairs .bgc-button', 1);

		// Выбрать Богатые и известные.
		await page.click('.zc-bg-studio .bgc-button');

		// выбранный тираж
		nearestDraw = await (await page.el('.dbc-number')).getText();

		// Нажать кнопку «К оплате»
		await page.click('app-register-bet-button button');

		// Открыто окно подтверждения регистрации билета лотереи «Лото-Забава».
		await page.el('app-check-information');
		url = await page.getCurrentUrl();
		await expect(url.indexOf(URL_REGISTRY)).toBeGreaterThan(-1);

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage([nearestDraw, 'Да', '2', '1']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись:
		await page.testCalculator('Лото-Забава', 'Регистрация ставок', '1', '22.00 грн', 'Успешно');
	});

	it('ТС №5.2 Регистрация билетов лотереи: Мегалот', async () => {

		// Нажать иконку лотереи «Мегалот».
		await page.selectLottery('Мегалот');

		// Нажать кнопку "Автоставка".
		await page.click('.mc-control-auto button') ;

		// В корзине 1 ставка.
		await expect(await element.all(by.css('.mc-bets .mc-bet')).count()).toEqual(1);

		// Нажать кнопку «Ручная ставка».
		await page.click('.mc-control-add button') ;

		// Выбрать числа: 5, 16, 17, 23, 29, 38, 42.
		for (const num of [5, 16, 17, 23, 29, 38, 42]) {
			await page.click('.mc-bg-numbers .bgc-button', num - 1);
		}

		// Выбрать мегашарик: 4.
		await page.click('.mc-bg-megaballs .bgc-button', 4);

		// Нажать кнопку «Подтвердить»
		await page.click('.mc-manual-dialog .modal-buttons-container app-green-button button');

		// В корзине 2 ставки.
		await expect(await element.all(by.css('.mc-bets .mc-bet')).count()).toEqual(2);

		// Выбрать количество тиражей — 1.
		await page.click('.mc-bg-draws .bgc-button');

		// Нажать кнопку «К оплате»
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['1', '2']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Лото-Забава / Регистрация ставок / 1 / Успешно
		await page.testCalculator('Мегалот', 'Регистрация ставок', '1', '40.00 грн', 'Успешно');
	});

	it('ТС №5.3 Регистрация билетов лотереи: Гаряча штучка', async () => {
		//Нажать иконку лотерей «Моменталки».
		await page.selectLottery('Швидкограй');

		//Выбрать в фильтре стоимость - 20 грн.
		elem = await page.el('.il-bg-filters');
		const elemValuesStr = await elem.getAttribute('ng-reflect-values-array');
		const elemValues = elemValuesStr.split(',');
		const index20 = elemValues.indexOf('20');
		await page.click('.il-bg-filters .bgc-button', index20);

		// Выбрать лотерею «Горячая Штучка».
		await page.click('.igc-game-item-content.game-code-144');

		// Выбрать количество билетов — 1.
		await page.click('.il-bg-tickets .bgc-button', 0);

		// Выбрать серию — 7.
		await browser.executeScript('document.querySelector("app-custom-scroll.bets-scroll .custom-scrollbar__inner").scrollTo(0, 10000);');
		elem = await element.all(by.css('.draws-item')).filter(el => {
			return el.element(by.cssContainingText('.draws-item .draws-item__serie_num', '7')).isPresent();
		}).get(0);
		await elem.click();

		// 6. Нажать кнопку «К оплате».
		await page.click('app-register-bet-button');

		// Открыто окно подтверждения регистрации билета лотереи «Гаряча штучка!».
		await page.el('app-check-information');
		url = await page.getCurrentUrl();
		await expect(url.indexOf(URL_REGISTRY)).toBeGreaterThan(-1);

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['Гаряча штучка!', '7', '1', '20.00 грн']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись:  / Регистрация ставок / 1 / 20.00 грн. / Успешно
		await page.testCalculator('Гаряча штучка!', 'Регистрация ставок', '1', '20.00 грн', 'Успешно');
	});

	it('ТС №5.4 Регистрация билетов лотереи: Тип', async () => {

		// Нажать иконку лотереи «ТипТоп».
		await page.selectLottery('ТипТоп');

		// Выбрать количество тиражей — 2.
		await page.click('.ttc-bg-draws .bgc-button', 1);

		// Выбрать количество ставок — 2.
		await page.click('.ttc-bg-bets .bgc-button', 1);

		// Нажать кнопку «К оплате».
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['Тип', '2', '2', 'Разные', '8.00 грн']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Тип / Регистрация ставок / 2 / 8.00 грн. / Успешно
		await page.testCalculator('Тип', 'Регистрация ставок', '2', '8.00 грн', 'Успешно');
	});

	it('ТС №5.5 Регистрация билетов лотереи: Топ', async () => {

		// Нажать иконку лотереи «ТипТоп».
		await page.selectLottery('ТипТоп');

		// Выбрать лотерею ТОП.
		await page.click('.ttc-bg-game-type .bgc-button.bgc-button-last');

		// Выбрать количество тиражей — 2.
		await page.click('.ttc-bg-draws .bgc-button', 1);

		// Выбрать количество ставок — 1.
		await page.click('.ttc-bg-bets .bgc-button', 0);

		// Выбрать комбинации — одинаковые
		await page.click('.ttc-bg-unique .bgc-button.bgc-button-last');

		// Нажать кнопку «К оплате».
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['Топ', '2', '1', 'Одинаковые', '10.00 грн']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Топ / Регистрация ставок / 2 / 10.00 грн. / Успешно
		await page.testCalculator('Топ', 'Регистрация ставок', '2', '10.00 грн', 'Успешно');
	});

	it('ТС №5.6 Регистрация лотереи: Гонка на деньги', async () => {

		// Нажать иконку лотереи «Гонка на Деньги».
		await page.selectLottery('Гонка на деньги');

		// Выбрать количество тиражей — 1.
		await page.click('.gg-bg-draws .bgc-button', 0);

		// Выбрать количество ставок — 2.
		await page.click('.gg-bg-bet-count .bgc-button', 1);

		// Выбрать тип игры — Д.
		await page.click('.gg-bg-game-type .bgc-button', 4);

		// Нажать кнопку «Выбор победителей»
		await page.click('.gg-bg-game-subtype .bgc-button', 1);

		// Нажать кнопку «Продолжить».
		await page.click('app-confirm-two-buttons app-green-button button', 1);

		// Выбрать номера заездов — 1, 2, 3, 4.
		await page.click('.gg-race-numbers .bgc-button', 0);
		await page.click('.gg-race-numbers .bgc-button', 1);
		await page.click('.gg-race-numbers .bgc-button', 2);
		await page.click('.gg-race-numbers .bgc-button', 3);

		// В «Заезд 1» выбрать победителей — 1, 2.
		elem = await page.el('.game-type-d .gg-bg-race', 0);
		await elem.all(by.css('.bgc-button')).get(0).click();
		await elem.all(by.css('.bgc-button')).get(1).click();

		// В «Заезд 2» выбрать победителей — 3, 4.
		elem = await page.el('.game-type-d .gg-bg-race', 1);
		await elem.all(by.css('.bgc-button')).get(2).click();
		await elem.all(by.css('.bgc-button')).get(3).click();

		// В «Заезд 3» выбрать победителей — 5, 6.
		elem = await page.el('.game-type-d .gg-bg-race', 2);
		await elem.all(by.css('.bgc-button')).get(4).click();
		await elem.all(by.css('.bgc-button')).get(5).click();

		// В «Заезд 4» выбрать победителей — 8, 10.
		elem = await page.el('.game-type-d .gg-bg-race', 3);
		await elem.all(by.css('.bgc-button')).get(7).click();
		await elem.all(by.css('.bgc-button')).get(9).click();

		// Нажать кнопку «Выбрать».
		await page.click('.game-type-d .modal-buttons-container button');

		// Выбрать сумму ставки (грн) — 2.
		await page.click('.gg-bg-bet-sum .bgc-button', 0);

		// Нажать кнопку «К оплате».
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['1', '8', 'Д', '2.00 грн', '16.00 грн']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Гонка на деньги / Регистрация ставок / 1 / 16.00 грн. / Успешно
		await page.testCalculator('Гонка на деньги', 'Регистрация ставок', '1', '16.00 грн', 'Успешно');
	});

	it('ТС №5.7 Регистрация лотереи: Каре', async () => {

		// Нажать иконку лотереи «Каре».
		await page.selectLottery('Каре');

		// Выбрать количество тиражей — 1.
		await page.click('.kc-bg-draws .bgc-button');

		// Нажать кнопку «Стол 1 — Карты».
		await page.click('.kc-table button');

		// Выбрать карты — 4П, ДП, 8Ч, КБ, 3Т.
		for (const card of [8, 40, 25, 46, 7]) {
			await page.click('.cards-row__btn', card);
		}

		// В блоке подсказок выбраны карты: 4П, ДП, 8Ч, КБ, 3Т.
		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label label', 0);
		await expect(await elem.getText()).toEqual('4');
		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label .kcwl-icon', 0);
		await expect(await page.hasClass(elem, 'card-icon-0')).toBeTruthy();

		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label label', 1);
		await expect(await elem.getText()).toEqual('ДАМА');
		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label .kcwl-icon', 1);
		await expect(await page.hasClass(elem, 'card-icon-0')).toBeTruthy();

		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label label', 2);
		await expect(await elem.getText()).toEqual('8');
		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label .kcwl-icon', 2);
		await expect(await page.hasClass(elem, 'card-icon-1')).toBeTruthy();

		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label label', 3);
		await expect(await elem.getText()).toEqual('КОРОЛЬ');
		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label .kcwl-icon', 3);
		await expect(await page.hasClass(elem, 'card-icon-2')).toBeTruthy();

		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label label', 4);
		await expect(await elem.getText()).toEqual('3');
		elem = await page.el('app-kare-selected-cards-one app-kare-card-with-label .kcwl-icon', 4);
		await expect(await page.hasClass(elem, 'card-icon-3')).toBeTruthy();

		// Выбрать размер ставки (грн) — 5.
		await page.click('.kc-bets-buttons .bgc-button');

		// Нажать кнопку «Продолжить».
		await page.click('.dialog-continue');

		// В корзине 1 ставка.
		await expect(await element.all(by.css('.kc-bets-scroll .kc-bet')).count()).toEqual(1);

		// Нажать кнопку «Стол 2 — Кобинации».
		await page.click('.kc-table button', 1);

		// Выбрать комбинации — Флеш-рояль, Стрит-флеш, Каре, Фул-хаус, Флеш, Стрит, тройка, Две пары, Пара, Людая комбинация.
		for (let i = 0; i < 10; i++) {
			await page.click('.cards-row__btn', i);
		}

		// В блоке подсказок выбраны комбинации: Флеш-рояль, Стрит-флеш, Каре, Фул-хаус, Флеш, Стрит, тройка, Две пары, Пара, Людая
		elem = await page.el('.ksco-cards');
		const combs = await elem.getText();
		const combsArr = combs.split(',').map(elem => elem.trim());
		const mustContain = ["Флеш-рояль", "стрит-флеш", "каре", "фул-хаус", "флеш", "стрит", "тройка", "две пары", "пара", "любая комбинация"];
		for (const word of mustContain) {
			await expect(combsArr.indexOf(word)).toBeGreaterThan(-1);
		}

		// Нажать кнопку виртуальной клавиатуры.
		elem = await page.el('app-msl-input-with-keyboard input');
		await elem.click();

		// Ввести сумму ставки - 10.
		await page.keyBoardInput(elem, '10');

		// Нажать кнопку «Продолжить».
		await page.click('.dialog-continue');

		// В корзине 2 ставки.
		await expect(await element.all(by.css('.kc-bets-scroll .kc-bet')).count()).toEqual(2);

		// Нажать кнопку «К оплате».
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['1 (Карты)', '1 (Комбинации)', '1 (Карты)', '1 (Комбинации)', '105.00 грн']);

		// Нажать кнопку «Регистрация».
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Каре / Регистрация ставок / 1 / 105.00 грн. / Успешно
		await page.testCalculator('Каре', 'Регистрация ставок', '1', '105.00 грн', 'Успешно');
	});

	it('ТС №5.8 Просмотр и печать программы Спортпрогноз', async () => {

		// Нажать иконку лотереи «Спортпрогноз»
		await page.selectLottery('Спортпрогноз');

		// Нажать кнопку «Программа тиража»
		await page.click('.spc-show-program');
		await page.el('.spcpc-program');

		// Нажать кнопку количества «>» (увеличить до 2х).
		await page.click('.pbc-page-count__btn', 1);

		// Нажать кнопку «Печать».
		await page.click('.rnc-print-button button');

		// Распечатано 2 программы.
		// elem = await page.el('app-dialog-container app-none-button-error');
		// await browser.wait(page.until.stalenessOf(elem), DOM_TIMEOUT, 'Диалог печати слишком долго не исчезает');
		await page.waitForModalHide();

		// Закрыть окно программы — нажать «крестик».
		await page.click('.spcpc-background .close-button');

		// Нажать кнопку «Домой».
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});

	it('Регистрация Гонка на деньги (авто)', async () => {
		// Нажать иконку лотереи «Гонка на Деньги».
		await page.selectLottery('Гонка на деньги');

		// Выбрать количество тиражей — 2.
		await page.click('.gg-bg-draws .bgc-button', 1);

		// Выбрать количество ставок — 1.
		await page.click('.gg-bg-bet-count .bgc-button', 0);

		// Выбрать тип игры — Г.
		await page.click('.gg-bg-game-type .bgc-button', 3);

		// Выбрать сумму ставки (грн) — 4.
		await page.click('.gg-bg-bet-sum .bgc-button', 1);

		// Нажать кнопку «К оплате».
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['2', '1', 'Г', '4.00 грн', '8.00 грн']);

		// Нажать кнопку «Регистрация»
		await page.click('app-green-button button', 2);

		// Ждем, пока не откроется список лотерей
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись: Гонка на деньги / Регистрация ставок / 1 / 16.00 грн. / Успешно
		await page.testCalculator('Гонка на деньги', 'Регистрация ставок', '2', '8.00 грн', 'Успешно');
	});

	it('Акции. Регистрация Лото-Забава. 1й вариант.', async () => {
		await page.testActions(
			0,
			[nearestDraw, 'Нет', '2', '1', '20.00 грн'],
			['Лото-Забава', 'Регистрация ставок', '1', '20.00 грн', 'Успешно']
		);
	});

	it('Акции. Регистрация Лото-Забава. 2й вариант.', async () => {
		await page.testActions(
			1,
			[nearestDraw, 'Нет', '3', '2', '50.00 грн'],
			['Лото-Забава', 'Регистрация ставок', '2', '50.00 грн', 'Успешно']
		);
	});

	it('Акции. Регистрация Мегалот. 1й вариант.', async () => {
		await page.testActions(
			2,
			['1', '4', '20.00 грн'],
			['Мегалот', 'Регистрация ставок', '1', '20.00 грн', 'Успешно']
		);
	});

	it('Акции. Регистрация Мегалот. 2й вариант. ', async () => {
		await page.testActions(
			3,
			['1', '10', '50.00 грн'],
			['Мегалот', 'Регистрация ставок', '1', '50.00 грн', 'Успешно']
		);
	});

	it('Акции. Регистрация ТипТоп. 1й вариант.', async () => {
		await page.testActions(
			4,
			['Тип', '1', '10', '20.00 грн'],
			['Тип', 'Регистрация ставок', '1', '20.00 грн', 'Успешно']
		);
	});

	// на деве не раюотает, так как кнопка не активная
	it('Акции. Регистрация ТипТоп. 2й вариант.', async () => {
		await page.testActions(
			5,
			['Топ', '1', '10', '50.00 грн'],
			['Топ', 'Регистрация ставок', '1', '50.00 грн', 'Успешно']
		);
	});

	it('Акции. Швидкограй. 1й вариант.', async () => {
		await page.testActions(
			6,
			['Весела скарбничка', '01', '4', '20.00 грн'],
			['Весела скарбничка', 'Регистрация ставок', '4', '20.00 грн', 'Успешно']
		);
	});

	it('Акции. Швидкограй. 2й вариант.', async () => {
		await page.testActions(
			7,
			['Морський бій', '1', '5', '50.00 грн'],
			['Морський бій', 'Регистрация ставок', '5', '50.00 грн', 'Успешно']
		);
	});

	it('Акции. Швидкограй. 3й вариант.', async () => {
		await page.testActions(
			8,
			['Товстий гаманець', '06', '2', '20.00 грн'],
			['Товстий гаманець', 'Регистрация ставок', '2', '20.00 грн', 'Успешно']
		);
	});

	// на деве не работает, так как кнопка не активная
	it('Акции. Швидкограй. 4й вариант.', async () => {
		await page.testActions(
			9,
			['Максі лото', '13', '2', '50.00 грн'],
			['Максі лото', 'Регистрация ставок', '2', '50.00 грн', 'Успешно']
		);
	});

	it('Акции. Регистрация через баннер.', async () => {
		// Нажать кнопку вкладки «Акции».
		await page.click('app-central-menu li[value="1"]');

		// Нажать по баннеру.
		await page.click('app-action-banner');

		// Выбрать количество билетов — 1
		await page.click('.il-bg-tickets .bgc-button');

		// Нажать кнопку «К оплате»
		await page.click('app-register-bet-button button');

		// Проверка страницы подтверждения регистрации.
		await page.testConfirmRegPage(['ЛАБІРИНТ', '01', '1', '20.00 грн']);

		// Нажать кнопку «Регистрация».
		await page.click('app-green-button button', 2);

		// Проверка на главную страницу => Билет распечатался,
		await page.checkForMainPage();

		// В калькулятор операций добавлена запись
		// ЛАБІРИНТ / Регистрация ставок / 1 / 20.00 грн / Успешно
		await page.testCalculator('ЛАБІРИНТ', 'Регистрация ставок', '1', '20.00 грн', 'Успешно');
	});

	it('Результаты. Лото-Забава.', async () => {

		// 1. Нажать кнопку вкладки «Результаты».
		await page.click('app-central-menu li[value="2"]');

		// 2. Нажать иконку лотереи «Лото-Забава».
		await page.selectLottery('Лото-Забава');

		// подождем немного, пока не закроется модальный диалог
		await page.waitForModalHide();

		// 3. Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// 4. Нажать кнопку "Домой".
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});

	it('Результаты. Мегалот.', async () => {

		// 1. Нажать кнопку вкладки «Результаты».
		await page.click('app-central-menu li[value="2"]');

		// 2. Нажать иконку лотереи «Мегалот».
		await page.selectLottery('Мегалот');

		// подождем немного, пока не закроется модальный диалог
		await page.waitForModalHide();

		// Открыть календарь - нажать кнопку "Дата тиража".
		await page.click('.cdi-input');

		// 4. Выбрать вчерашнюю дату (от дня выполнения теста).
		const currDate = new Date();
		const currDay =  currDate.getDate();
		currDate.setDate(currDay - 1);
		const prevDay = currDate.getDate();

		// если пред. день больше текущего, то уменьшить месяц
		if (prevDay > currDay) {
			await page.click('.cds-month .material-icons');
		}

		elem = element(by.cssContainingText('.activeMonth', prevDay.toString()));
		await elem.click();

		// 5. Нажать кнопку "Выбрать".
		await page.click('.select-button button');

		// 6. Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// 7. Нажать кнопку "Домой".
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});

	it('Результаты. ТипТоп.', async () => {

		// Нажать кнопку вкладки «Результаты».
		await page.click('app-central-menu li[value="2"]');

		// Нажать иконку лотереи «ТипТоп».
		await page.selectLottery('ТипТоп');

		// Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// Выбрать лотерею - Топ.
		await page.click('.ttr-bg-game-type .bgc-button.bgc-button-last');

		// На время загрузки результатов сообщение "Загрузка результатов, ожидайте".
		await page.waitForModalHide();

		// Выбрать количество копий = 2 - нажать ">".
		await page.click('app-print-button .pbc-page-count__btn', 1);

		// Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// Нажать кнопку "Домой".
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});

	it('Результаты. Гонка на деньги.', async () => {

		// Нажать кнопку вкладки «Результаты».
		await page.click('app-central-menu li[value="2"]');

		// Нажать иконку лотереи «Гонка на деньги».
		await page.selectLottery('Гонка на деньги');

		// Подождать
		await page.waitForModalHide();

		// номер первого тиража с текущей страницы
		const draw1Num = await (await page.el('.dbc-number')).getText();

		// Нажать кнопку "Раньше".
		await page.click('.rnbc-prev');

		// Подождать
		await page.waitForModalHide();

		// номер первого тиража с предыдущей страницы
		const prevDraw1Num = await (await page.el('.dbc-number')).getText();

		// Список тиражей обновился
		await expect(draw1Num).not.toEqual(prevDraw1Num);

		// Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// Нажать кнопку "Продажа билетов Гонка на деньги".
		await page.click('.rnc-sell-btn');

		// Открыто окно регистрации "Гонка на деньги".
		url = await page.getCurrentUrl();
		await expect(url.endsWith(`${URL_LOTTERIES}/${URL_GONKA}/${URL_INIT}`)).toBeTruthy();

		// Нажать кнопку "Результаты Гонка на деньги".
		await page.click('.gonka-goto-results');

		// Подождать
		await page.waitForModalHide();

		// Открыто окно результатов "Гонка на деньги".
		url = await page.getCurrentUrl();
		await expect(url.endsWith(`${URL_LOTTERIES}/${URL_GONKA}/${URL_RESULTS}`)).toBeTruthy();

		// Нажать кнопку "Домой".
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});

	it('Результаты. Спортпрогноз.', async () => {

		// Нажать кнопку вкладки «Результаты».
		await page.click('app-central-menu li[value="2"]');

		// Нажать иконку лотереи «Спортпрогноз».
		await page.selectLottery('Спортпрогноз');

		// ждем загрузки тиражей
		await page.waitForModalHide();

		// Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// Нажать кнопку "Домой".
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});

	it('Результаты. Каре.', async () => {

		// Нажать кнопку вкладки «Результаты».
		await page.click('app-central-menu li[value="2"]');

		// Нажать иконку лотереи «Каре».
		await page.selectLottery('Каре');

		// ждем загрузки тиражей
		await page.waitForModalHide();

		// Нажать кнопку "Печать".
		await page.click('app-green-button.rnc-print-button');

		// На время печати сообщение "Печать отчёта, ожидайте".
		await page.waitForModalHide();

		// Нажать кнопку "Домой".
		await page.click('.hc-home-button');

		// Открыто главное меню
		await page.checkForMainPage();
	});
});
