import { browser, by, element, ElementFinder, protractor, ProtractorExpectedConditions } from 'protractor';
import { URL_LOTTERIES } from '../../src/app/util/route-utils';

/**
 * Таймаут для ожидания элементов DOM.
 */
const DOM_TIMEOUT = 120000;

/**
 * Класс для работы с приложением.
 */
export class AppPage {
	/**
	 * Ссылка на ожидаемые условия.
	 */
	until: ProtractorExpectedConditions = protractor.ExpectedConditions;

	/**
	 * Функция паузы.
	 * @param time Время паузы в миллисекундах.
	 */
	setPause(time: number = 100): Promise<void> {
		return browser.driver.sleep(time) as Promise<void>;
	}

	/**
	 * Функция получения текущего URL.
	 */
	getCurrentUrl(): Promise<string> {
		return browser.getCurrentUrl() as Promise<string>;
	}

	/**
	 * Функция перехода на главную страницу приложения.
	 */
	navigateTo(): Promise<any> {
		browser.waitForAngularEnabled(false);

		return browser.get('/') as Promise<any>;
	}

	/**
	 * Функция получения заголовка страницы авторизации.
	 */
	getAuthHeaderText(): any {
		return element(by.css('.title'))
			.getText();
	}

	/**
	 * Функция получения поля ввода логина.
	 */
	getAuthInputLogin(): ElementFinder {
		return element(by.css('app-msl-input[ng-reflect-name=login] input'));
	}

	/**
	 * Функция получения поля ввода пароля.
	 */
	getAuthInputPassword(): ElementFinder {
		return element(by.css('app-msl-input[ng-reflect-name=password] input'));
	}

	/**
	 * Функция получения кнопки авторизации.
	 */
	getAuthButton(): ElementFinder {
		return element.all(by.css('app-green-button')).get(1);
	}

	/**
	 * Функция выбора лотереи.
	 * @param lotteryTitle Название лотереи.
	 */
	async selectLottery(lotteryTitle: string) {
		await this.setPause(300);
		const elem = element(by.cssContainingText('.game-list-container li', lotteryTitle));
		return elem.click();
	}

	/**
	 * Функция ввода текста с клавиатуры.
	 * @param field Поле ввода.
	 * @param text Текст.
	 */
	async keyBoardInput(field: ElementFinder, text: string): Promise<void> {
		await field.clear();
		await field.click();
		await this.setPause(300);
		const chars = text.split('');
		for (const char of chars) {
			element(by.css(`[vk-key-value-attr="${char}"]`)).click();
			await this.setPause(300);
		}
		return this.setPause(300);
	}

	/**
	 * Функция проверки наличия класса у элемента.
	 * @param element Элемент.
	 * @param cls Класс.
	 */
	async hasClass(element: ElementFinder, cls: string): Promise<boolean> {
		return element.getAttribute('class').then(function (classes) {
			return classes.split(' ').indexOf(cls) !== -1;
		});
	}

	/**
	 * Функция тестирования компонента суммарного чека
	 * @param lottName Название лотереи
	 * @param actType Тип действия
	 * @param q Количество билетов
	 * @param summ Сумма по операции
	 * @param result Результат операции
	 */
	async testCalculator(lottName: string, actType: string, q: string, summ: string, result: string) {
		const lastRow = await element.all(by.css('.tco-action-row')).last();
		const labels = lastRow.all(by.tagName('label'));
		await expect(await labels.get(1).getText()).toEqual(lottName);
		await expect(await labels.get(2).getText()).toEqual(actType);
		await expect(await labels.get(3).getText()).toEqual(q);
		await expect(await labels.get(4).getText()).toEqual(summ);
		return expect(await labels.get(5).getText()).toEqual(result);
	}

	/**
	 * Функция получения элемента по CSS-пути.
	 * @param cssPath CSS-путь.
	 * @param index Индекс элемента.
	 */
	async el(cssPath: string, index = 0): Promise<ElementFinder> {
		const elem = element.all(by.css(cssPath)).get(index);
		await browser.wait(this.until.presenceOf(elem), DOM_TIMEOUT, `Элемента по пути "${cssPath}" слишком долго нет в DOM`);
		return new Promise<ElementFinder>(resolve => resolve(elem));
	}

	/**
	 * Функция нажатия на элемент по CSS-пути.
	 * @param cssPath CSS-путь.
	 * @param index Индекс элемента.
	 */
	async click(cssPath: string, index = 0): Promise<void> {
		const elem = await this.el(cssPath, index);
		await browser.wait(this.until.elementToBeClickable(elem), DOM_TIMEOUT, `Элемент по пути "${cssPath}" так и не стал кликабельным`);
		return elem.click();
	}

	/**
	 * Проверка на главную страницу приложения.
	 */
	async checkForMainPage() {
		// Ждем, пока не откроется список лотерей
		await this.el('app-total-check-info-panel');
		const url = await this.getCurrentUrl();

		// Открыта вкладка «Лотереи».
		return expect(url.endsWith(URL_LOTTERIES) || url.endsWith(`${URL_LOTTERIES}?manualMenuClick=home`)).toBeTruthy();
	}

	/**
	 * Функция логина в приложение.
	 * @param userType Тип пользователя.
	 */
	async loginToApp(userType = 'operator') {
		// await page.navigateTo();

		const loginField = await this.el('app-msl-input[ng-reflect-name=login] input');
		const hasLoginField = await loginField.isPresent();

		const creds = browser.params.credentials[userType];

		if (hasLoginField) {
			await this.keyBoardInput(loginField, creds.login);
		}

		const passwdField = this.getAuthInputPassword();
		const hasPasswdField = await passwdField.isPresent();
		if (hasPasswdField) {
			await this.keyBoardInput(passwdField, creds.password);
		}

		await this.click('app-green-button', 1);

		// Ждем, пока не откроется страница с верхней панелью
		let elem = await this.el('app-top-bar-panel');
		await browser.wait(this.until.visibilityOf(elem), DOM_TIMEOUT, 'Элемент app-top-bar-panel слишком долго не показывается');
		await browser.wait(this.until.elementToBeClickable(elem), DOM_TIMEOUT, 'Элемент app-top-bar-panel слишком долго не доступен для взаимодействия');

		elem = element.all(by.css('.modal-dialog-container.info')).get(0);
		await browser.wait(this.until.invisibilityOf(elem), DOM_TIMEOUT, 'Элемент .modal-dialog-container.info слишком долго не скрывается');
	}

	/**
	 * Функция выбора языка по-умолчанию.
	 */
	async setDefaultLang(): Promise<void> {
		// Выбор языка по-умолчанию
		await this.click('.hc-hamburger-button');
		const lngBtn = await this.el('.hm-change-lang__row');
		await browser.wait(this.until.visibilityOf(lngBtn), DOM_TIMEOUT, `Элемент слишком долго не показывается`);
		await this.click('.hm-change-lang__row');
		await this.click('.hc-hamburger-button');
		return browser.wait(this.until.invisibilityOf(lngBtn), DOM_TIMEOUT, `Элемент слишком долго не показывается`);
	}

	/**
	 * Функция закрытия модальных окон.
	 */
	async closeModals(): Promise<void> {
		await this.setPause(500);
		const isChangeLogPresent = await element(by.css('app-change-log app-green-button button')).isPresent();
		if (isChangeLogPresent) {
			await this.click('app-change-log app-green-button button');
		}

		await this.setPause(500);
		const isJackpotsDialogPresent = await element(by.css('app-one-button-custom.jackpots-dialog app-green-button button')).isPresent();
		if (isJackpotsDialogPresent) {
			const isJackpotsDialogShown = await element(by.css('app-one-button-custom.jackpots-dialog app-green-button button')).isDisplayed();
			if (isJackpotsDialogShown) {
				await this.click('app-one-button-custom.jackpots-dialog app-green-button button');
			}
		}
	}

	/**
	 * Функция тестирует страницу подтверждения регистрации лотереи.
	 * @param regData Данные регистрации.
	 */
	async testConfirmRegPage(regData: Array<string>): Promise<void> {
		// Проверить данные на странице регистрации лотереи
		for (let i = 0; i < (regData.length - 1); i++) {
			if (!!regData[i]) {
				const elem = await this.el('.ci-data-value', i);
				await expect((await elem.getText()).trim()).toEqual(regData[i]);
			}
		}
		const elem = await this.el('.ci-data-value', regData.length - 1);
		return expect((await elem.getText()).trim()).toEqual(regData[regData.length - 1]);
	}

	/**
	 * Функция тестирует страницу акций
	 * @param actNo Номер акции
	 * @param regData Данные в акции
	 * @param calcData Данные для калькулятора операций
	 */
	async testActions(actNo: number, regData: Array<string>, calcData: Array<string>): Promise<void> {

		// Нажать кнопку вкладки «Акции».
		await this.click('app-central-menu li[value="1"]');

		// Нажать кнопку акции actNo
		await this.click('app-action-button app-green-button button', actNo);

		// Проверить данные на странице регистрации лотереи
		await this.testConfirmRegPage(regData);

		// Нажать кнопку «Регистрация».
		await this.click('app-green-button button', 2);

		// Билет распечатался
		// Ждем, пока не откроется страница акций
		await this.el('app-action-banner-row');

		// Нажать кнопку «Домой».
		await this.click('.hc-home-button');
		await this.checkForMainPage();

		// В калькулятор операций добавлена запись
		return this.testCalculator(calcData[0], calcData[1], calcData[2], calcData[3], calcData[4]);
	}

	/**
	 * Подождать, пока не исчезнет модальный диалог
	 */
	async waitForModalHide(): Promise<void> {
		const elem = await this.el('.modal-dialog-background');
		return browser.wait(this.until.invisibilityOf(elem), DOM_TIMEOUT, `Модальный диалог слишком долго не скрывается`);
	}
}
