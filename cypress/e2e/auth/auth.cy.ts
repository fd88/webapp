describe('Проверки страницы авторизации при десктопном разрешении', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('guest').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/');
		});
	})

	it('Проверки стилей страницы авторизации при десктопном разрешении (1920px)', function () {
		cy.get('[data-cy="authorization-container"]')
			.should('have.css', 'padding', '8px');

		cy.get('[data-cy="ac-top-container"]')
			.should('have.css', 'width', '1844px')
			.and('have.css', 'padding-left', '30px')
			.and('have.css', 'padding-right', '30px')
			.and('have.css', 'margin-top', '30px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="ac-info"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="ac-center"]')
			.should('have.css', 'margin', '25px 0px')
			.and('have.css', 'width', '1844px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '43px 200px 80px 0px')
			.and('have.css', 'height', '323px');

		cy.get('[data-cy="title"]')
			.should('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="login-form"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '1844px');

		cy.get('[data-cy="auth-step-1"]')
			.should('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="login-form-item-type-user"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(0, 151, 136)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="login-form-item-type-password"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-element"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'border-radius', '2px')
			.and('have.css', 'border', '0px none rgb(90, 90, 90)')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'transition', 'padding-top 0.2s ease 0s, margin-top 0.2s ease 0s')
			.and('have.css', 'overflow-x', 'hidden');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(0, 151, 136)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="fake-submit-btn"]')
			.should('have.css', 'display', 'none');

		cy.get('[data-cy="auth-button-type-confirm"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'margin', '0px 747px')
			.and('have.css', 'font-size', '36px');

	});

	it('Проверка экрана выбора номера телефона и ввода СМС при десктопном разрешении (1920px)', function () {
		cy.get('[data-cy="msl-input-element"]').eq(0).clear();
		cy.get('[data-cy="msl-input-element"]').eq(1).clear();
		cy.get('[data-cy="msl-input-element"]').eq(0).type('100605');
		cy.get('[data-cy="msl-input-element"]').eq(1).type('100605');
		cy.get('[data-cy="auth-button-type-confirm"]').click();

		cy.get('[data-cy="authorization-container"]')
			.should('have.css', 'padding', '8px');

		cy.get('[data-cy="ac-top-container"]')
			.should('have.css', 'width', '1844px')
			.and('have.css', 'padding-left', '30px')
			.and('have.css', 'padding-right', '30px')
			.and('have.css', 'margin-top', '30px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="ac-info"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="ac-center"]')
			.should('have.css', 'margin', '25px 0px')
			.and('have.css', 'width', '1844px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '43px 200px 411px 80px')
			.and('have.css', 'height', '734px');

		cy.get('[data-cy="title"]')
			.should('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="login-form"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '1844px');

		cy.get('[data-cy="auth-step-1"]')
			.should('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="login-form-item-type-user"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="login-form-item-type-password"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-element"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'border-radius', '2px')
			.and('have.css', 'border', '0px none rgb(128, 128, 128)')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'transition', 'padding-top 0.2s ease 0s, margin-top 0.2s ease 0s')
			.and('have.css', 'overflow-x', 'hidden');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="fake-submit-btn"]')
			.should('have.css', 'display', 'none');

		cy.get('[data-cy="auth-button-type-confirm"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'margin', '0px 747px')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="select-phone"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '16px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'margin-bottom', '8px')
			.and('have.css', 'margin-top', '12px');

		cy.get('[data-cy="client-phone"]')
			.should('have.css', 'max-width', '344px')
			.and('have.css', 'width', '344px')
			.and('have.css', 'margin', '8px 750px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'border', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'padding', '10px')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="client-phone-radio"]')
			.should('have.css', 'display', 'none');

		cy.get('[data-cy="auth-button-type-confirm"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'margin', '0px 747px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '0px none rgb(161, 161, 161)');

		cy.get('[data-cy="client-phone"]').first().click();

		cy.get('[data-cy="client-phone"]')
			.should('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'background-color', 'rgb(0, 151, 136)')
			.and('have.css', 'border-color', 'rgb(0, 151, 136)')
			.and('have.css', 'max-width', '344px')
			.and('have.css', 'width', '344px')
			.and('have.css', 'margin', '8px 750px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'padding', '10px')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="login-form"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '1844px');

		cy.get('[data-cy="auth-step-3"]')
			.should('have.css', 'overflow', 'hidden')
			.and('have.css', 'height', '0px')
			.and('have.css', 'animation-name', 'slideDown')
			.and('have.css', 'animation-duration', '0.6s')
			.and('have.css', 'animation-fill-mode', 'forwards')
			.and('have.css', 'animation-timing-function', 'ease')
			.and('have.css', 'width', '1844px');

		cy.get('[data-cy="pin-title"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-bottom', '10px');

		cy.get('[data-cy="login-form-item-type-sms"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'height', '56px')
			.and('have.css', 'margin-bottom', '8px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'max-width', '350px')
			.and('have.css', 'margin-left', '747px')
			.and('have.css', 'margin-right', '747px')
			.and('have.css', 'letter-spacing', '50px')
			.and('have.css', 'font-size', '30px');

		cy.get('[data-cy="pin"]')
			.should('have.css', 'letter-spacing', '50px')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'height', '56px');

		cy.get('[data-cy="pin-wrapper"]')
			.should('have.css', 'letter-spacing', '50px')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'height', '56px');

		cy.get('[data-cy="pin-inner"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'letter-spacing', '50px')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'height', '56px');

		cy.get('[data-cy="pin-input"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'letter-spacing', '50px')
			.and('have.css', 'padding-left', '63.75px')
			.and('have.css', 'padding-right', '0px')
			.and('have.css', 'padding-bottom', '5px')
			.and('have.css', 'border-radius', '6px')
			.and('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '56px');

		cy.get('[data-cy="pin-padding"]')
			.should('have.css', 'background-color', 'rgb(255, 255, 255)')
			.and('have.css', 'width', '30px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'top', '1px')
			.and('have.css', 'bottom', '1px');

		cy.get('[data-cy="pin-spacers"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'left', '30px')
			.and('have.css', 'right', '30px')
			.and('have.css', 'bottom', '10px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'space-around')
			.and('have.css', 'padding-left', '10px')
			.and('have.css', 'padding-right', '10px')
			.and('have.css', 'height', '2px');

		cy.get('[data-cy="pin-spacer"]')
			.should('have.css', 'width', '40.5px')
			.and('have.css', 'height', '2px')
			.and('have.css', 'background-color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="pin-clear"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'right', '10px')
			.and('have.css', 'top', '27px')
			.and('have.css', 'left', '15px')
			.and('have.css', 'transform', 'matrix(1, 0, 0, 1, -12, -12)')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'z-index', '10');

		cy.get('[data-cy="pin-clear-icon"]')
			.should('have.css', 'color', 'rgb(223, 80, 80)')
			.and('have.css', 'display', 'inline-block')
			.and('have.css', 'max-width', 'none')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'background-color', 'rgba(0, 0, 0, 0)')
			.and('have.css', 'border-color', 'rgb(223, 80, 80)')
			.and('have.css', 'position', 'static')
			.and('have.css', 'font-family', '"Material Icons"')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'none')
			.and('have.css', 'border-radius', '0px');

		cy.get('[data-cy="auth-button-type-confirm"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'margin', '0px 747px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'background-color', 'rgb(0, 90, 81)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'border-radius', '8px');
	});

	it('Проверка экрана выбора точки продаж при десктопном разрешении (1920px)', function () {
		cy.get('[data-cy="msl-input-element"]').eq(0).clear();
		cy.get('[data-cy="msl-input-element"]').eq(1).clear();
		cy.get('[data-cy="msl-input-element"]').eq(0).type('100605');
		cy.get('[data-cy="msl-input-element"]').eq(1).type('100605');
		cy.get('[data-cy="auth-button-type-confirm"]').click();

		cy.get('[data-cy="client-phone"]').first().click();
		cy.get('[data-cy="auth-button-type-confirm"]').click();


		cy.get('[data-cy="authorization-container"]')
			.should('have.css', 'padding', '8px');

		cy.get('[data-cy="ac-top-container"]')
			.should('have.css', 'width', '1844px')
			.and('have.css', 'padding-left', '30px')
			.and('have.css', 'padding-right', '30px')
			.and('have.css', 'margin-top', '30px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="ac-info"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="ac-center"]')
			.should('have.css', 'margin', '25px 0px')
			.and('have.css', 'width', '1844px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '43px 200px 187px 0px')
			.and('have.css', 'height', '430px');

		cy.get('[data-cy="title"]')
			.should('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="login-form"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '1844px');

		cy.get('[data-cy="auth-step-1"]')
			.should('have.css', 'max-width', '350px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="login-form-item-type-user"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="login-form-item-type-password"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-element"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'border-radius', '2px')
			.and('have.css', 'border', '0px none rgb(128, 128, 128)')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'transition', 'padding-top 0.2s ease 0s, margin-top 0.2s ease 0s')
			.and('have.css', 'overflow-x', 'hidden');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="fake-submit-btn"]')
			.should('have.css', 'display', 'none');

		cy.get('[data-cy="login-form"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '1844px');

		cy.get('[data-cy="login-form-item-type-user"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="login-form-item-type-password"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="msl-input-container"]')
			.should('have.css', 'margin-top', '30px')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="msl-input-element"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'border-radius', '2px')
			.and('have.css', 'border', '0px none rgb(128, 128, 128)')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'transition', 'padding-top 0.2s ease 0s, margin-top 0.2s ease 0s')
			.and('have.css', 'overflow-x', 'hidden');

		cy.get('[data-cy="msl-input-label"]')
			.should('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '2px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'white-space', 'nowrap')
			.and('have.css', 'border-top', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'transition', 'width 0.4s ease 0s');

		cy.get('[data-cy="msl-input-placeholder"]')
			.should('have.css', 'top', '-70px')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="terminals"]')
			.should('have.css', 'max-width', '600px')
			.and('have.css', 'width', '600px')
			.and('have.css', 'margin', '0px 0px 30px');

		cy.get('[data-cy="select-terminal"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-bottom', '12px');

		cy.get('[data-cy="terminal-item"]')
			.should('have.css', 'max-width', '600px')
			.and('have.css', 'width', '600px')
			.and('have.css', 'margin', '0px 0px 8px')
			.and('have.css', 'padding', '0px 24px')
			.and('have.css', 'min-height', '50px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'border', '2px solid rgb(128, 128, 128)')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '46px / 274px 274px / none')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="terminal-name"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="terminal-location"]')
			.should('have.css', 'justify-self', 'end')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="terminal-item"]').first().click();
	});
});
