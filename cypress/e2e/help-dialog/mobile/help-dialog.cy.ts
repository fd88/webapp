describe('Проверка диалогового окна справки при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="top-bar-hamburger"]').click();
			cy.get('[data-cy="hm-help"]').click();
		});
	});

	it('Проверка диалогового окна справки при мобильном разрешении (393 x 873 px)', () => {
		cy.get('[data-cy="chp-dialog"]').within(() => {

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '16px')
				.and('have.css', 'height', '16px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', 'auto')
				.and('have.css', 'position', 'fixed')
				.and('have.css', 'right', 'auto')
				.and('have.css', 'top', '14px');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '100%')
				.and('have.css', 'height', '100%')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(0, 151, 136)');

			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '70')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)')
				.and('have.css', 'padding', '0px')
				.and('have.css', 'display', 'none');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', '890px')
				.and('have.css', 'width', '100%')
				.and('have.css', 'padding', '45px 0px 0px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '0px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'width', '100%')
				.and('have.css', 'max-height', 'none')
				.and('have.css', 'height', '100%')
				.and('have.css', 'position', 'relative')
				.and('have.css', 'text-align', 'left');
		});

		cy.get('[data-cy="help-dialog-wrapper"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '49px 588px / 369px / none')
			.and('have.css', 'height', '637px');

		cy.get('[data-cy="help-dialog-header"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="spoilers"]')
			.should('have.css', 'padding-left', '0px')
			.and('have.css', 'padding-right', '0px');

		cy.get('[data-cy="spoiler"]')
			.should('have.css', 'max-height', '50px')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'border', '2px solid rgb(128, 128, 128)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.08) -4px 4px 4px 0px');

		cy.get('[data-cy="spoiler-header"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'padding', '5px 30px')
			.and('have.css', 'min-height', '50px')
			.and('have.css', 'border-bottom', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'transition', 'color 0.4s ease 0s');

		cy.get('[data-cy="spoiler-body"]')
			.should('have.css', 'padding', '20px')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'overflow-x', 'auto')
			.and('have.css', 'width', '343px')
			.and('have.css', 'line-height', '20.7px')
			.and('have.css', '-webkit-text-size-adjust', '100%');

		cy.get('[data-cy="content-scroller"]')
			.should('have.css', 'position', 'relative');

		cy.get('[data-cy="custom-scrollbar"]')
			.should('have.css', 'position', 'relative')
			.and('have.css', 'overflow-y', 'scroll')
			.and('have.css', 'height', '588px');


	});
});
