import {checkTopPanelStyles} from "../common/check-top-panel-styles";

describe('Проверка верхней панели при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
		});
	});

	it('Проверка стилей верхней панели при десктопном разрешении (1920px)', () => {
		checkTopPanelStyles();
	});
});
