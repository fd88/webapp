import {checkTopPanelStyles} from "../../common/mobile/check-top-panel-styles";

describe('Проверка верхней панели при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
		});
	});

	it('Проверка стилей верхней панели при мобильном разрешении (393 x 873 px)', () => {
		checkTopPanelStyles();
	});
});
