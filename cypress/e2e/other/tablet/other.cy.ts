describe('Проверка страницы "Другое" при планшетном разрешении (767 x 1024 px)', () => {

	beforeEach(() => {
		cy.viewport(767, 1024);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(4).click();

		});
	});

	it('Проверка страницы "Другое" при планшетном разрешении (767 x 1024 px)', () => {
		cy.get('[data-cy="reporting-menu-list-title"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'padding', '10px 25px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'transition', 'background 0.5s ease 0s, color 0.5s ease 0s');

		cy.get('[data-cy="reporting-menu-list-item"]')
			.should('have.css', 'list-style', 'outside none none')
			.and('have.css', 'display', 'block')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'border', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'transition', 'background 0.6s ease 0s');

		cy.get('[data-cy="rp-menu"]')
			.should('have.css', 'padding-left', '10px')
			.and('have.css', 'padding-right', '10px')
			.and('have.css', 'max-height', '0px')
			.and('have.css', 'transition', 'max-height 0.3s ease 0s, padding-top 0.3s ease 0s, padding-bottom 0.3s ease 0s')
			.and('have.css', 'overflow', 'hidden');

		cy.get('[data-cy="rp-menu-item"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'width', '721px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '16px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'border', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'max-height', '0px')
			.and('have.css', 'opacity', '0')
			.and('have.css', 'transition', 'opacity 0.5s ease 0s, max-height 1s ease 0s, padding 1s ease 0s');

		cy.get('[data-cy="reporting-right-section"]')
			.should('have.css', 'width', '743px')
			.and('have.css', 'border', '0px none rgb(40, 40, 40)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'margin-bottom', '0px')
			.and('have.css', 'height', '178px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="report-form"]')
			.should('have.css', 'width', '743px')
			.and('have.css', 'margin', '0px');

		cy.get('[data-cy="rp-request"]')
			.should('have.css', 'width', '743px')
			.and('have.css', 'height', '60px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="op-content"]')
			.should('have.css', 'padding', '10px')
			.and('have.css', 'display', 'none')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'height', '178px')
			.and('have.css', 'overflow-y', 'auto');

		cy.get('[data-cy="rp-columns-left"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'align-items', 'normal')
			.and('have.css', 'justify-content', 'normal');

		cy.get('[data-cy="rp-columns-right"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'align-items', 'normal')
			.and('have.css', 'justify-content', 'space-between');

		cy.get('[data-cy="operation-field"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'max-width', '400px')
			.and('have.css', 'margin', '0px 171.5px 20px');

		cy.get('[data-cy="reporting-inputs"]')
			.should('have.css', 'padding-top', '15px');

		cy.get('[data-cy="op-field-title"]')
			.should('have.css', 'margin-bottom', '5px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="input-operation-field-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'margin-bottom', '12px');

		cy.get('[data-cy="field-wrp"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="field-wrp-input"]')
			.should('have.css', 'margin-right', '0px')
			.and('have.css', 'width', '400px')
			.and('have.css', 'padding', '4px')
			.and('have.css', 'color', 'rgb(102, 102, 102)')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '1px solid rgb(153, 153, 153)')
			.and('have.css', 'border-radius', '5px');

	});
});
