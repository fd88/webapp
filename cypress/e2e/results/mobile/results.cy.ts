import {checkCentralMenu} from "../../common/mobile/check-central-menu";

describe('Проверка страницы с результатами при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="top-bar-hamburger"]').click();
			cy.get('[data-cy="hamburger-menu-item"]').eq(2).click();
		});
	});

	it('Проверка страницы с результатами по лотереям при мобильном разрешении (393 x 873 px)', () => {
		cy.get('[data-cy="page-main"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '54px 0px 775px')
			.and('have.css', 'height', '829px')
			.and('have.css', 'padding', '0px');

		checkCentralMenu();

		cy.get('[data-cy="panel"]')
			.should('have.css', 'margin-bottom', '0px');

		cy.get('[data-cy="game-list"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'justify-content', 'normal')
			.and('have.css', 'flex-flow', 'row nowrap')
			.and('have.css', 'overflow-y', 'auto');

		cy.get('[data-cy="game-list-item"]')
			.should('have.css', 'max-width', 'none')
			.and('have.css', 'margin', '6px 0px')
			.and('have.css', 'cursor', 'pointer');

		cy.get('[data-cy="game-list-inner"]')
			.should('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(rgb(255, 255, 255) 0%, rgb(255, 255, 255) 50%, rgb(251, 245, 220) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'height', '86px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'padding', '10px 20px')
			.and('have.css', 'margin-bottom', '5px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'border', '2px solid rgb(227, 185, 11)');

		cy.get('[data-cy="game-list-bg"]')
			.should('have.css', 'left', '20px')
			.and('have.css', 'top', '5px')
			.and('have.css', 'right', '20px')
			.and('have.css', 'bottom', '5px')
			.and('have.css', 'background-size', 'contain')
			.and('have.css', 'background-position', '50% 50%')
			.and('have.css', 'background-repeat', 'no-repeat')
			.and('have.css', 'position', 'absolute');

		cy.get('[data-cy="game-list-title"]')
			.should('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', 'auto');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '100%')
			.and('have.css', 'max-height', 'none');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '14px');
	});
});
