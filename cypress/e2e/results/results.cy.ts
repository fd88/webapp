import {checkCentralMenu} from "../common/check-central-menu";

describe('Проверка страницы с результатами при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(2).click();
		});
	});

	it('Проверка страницы с результатами по лотереям при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="page-main"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '124px 0px 806px')
			.and('have.css', 'height', '930px')
			.and('have.css', 'padding', '0px 30px');

		checkCentralMenu();

		cy.get('[data-cy="panel"]')
			.should('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="game-list"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'justify-content', 'normal')
			.and('have.css', 'flex-flow', 'row nowrap')
			.and('have.css', 'overflow-y', 'auto');

		cy.get('[data-cy="game-list-item"]')
			.should('have.css', 'max-width', '240px')
			.and('have.css', 'width', '240px')
			.and('have.css', 'margin', '0px 16px 30px')
			.and('have.css', 'cursor', 'pointer');

		cy.get('[data-cy="game-list-inner"]')
			.should('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(255, 255, 255) 50%, rgb(251, 245, 220) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '10px')
			.and('have.css', 'height', '150px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'padding', '10px 20px')
			.and('have.css', 'margin-bottom', '10px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'border', '2px solid rgb(227, 185, 11)');

		cy.get('[data-cy="game-list-bg"]')
			.should('have.css', 'left', '20px')
			.and('have.css', 'top', '10px')
			.and('have.css', 'right', '20px')
			.and('have.css', 'bottom', '10px')
			.and('have.css', 'background-size', 'contain')
			.and('have.css', 'background-position', '50% 50%')
			.and('have.css', 'background-repeat', 'no-repeat')
			.and('have.css', 'position', 'absolute');

		cy.get('[data-cy="game-list-title"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '14px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '100%')
			.and('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '24px');
	});
});
