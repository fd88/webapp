describe('Проверка лотереи Каре при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(6).click();
			cy.get('[data-cy="goto-results"]').first().click();
		});
	});

	it('Проверка стилей страницы результатов Каре при мобильном разрешении (393 x 873 px)', () => {
		cy.get('[data-cy="kare-results-container"]')
			.should('have.css', 'height', '829px');

		cy.get('[data-cy="results-navigator-container"]')
			.should('have.css', 'grid-template', '44px 55px 652px 55px / 53.2031px 323.797px / "logo lott-button" "switcher switcher" "content content" "bottom-cell bottom-cell"')
			.and('have.css', 'grid-gap', '0px 16px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'padding-bottom', '0px')
			.and('have.css', 'top', '13px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '10px')
			.and('have.css', 'left', '0px');

		cy.get('[data-cy="rnc-logo"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '8px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'max-height', '32px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '12px');

		// .rt-cell
		cy.get('[data-cy="rt-cell"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-items', 'center');

		// .rnc-sell-btn
		cy.get('[data-cy="rnc-sell-btn"]')
			.should('have.css', 'padding', '0px 8px')
			.and('have.css', 'min-height', '44px')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'font-size', '15px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'justify-self', 'end')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'display', 'block');

		// .rnc-left
		cy.get('[data-cy="rnc-left"]')
			.should('have.css', 'width', '393px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '44px 584px / 393px / none')
			.and('have.css', 'row-gap', '8px');

		// .rnc-left__date-input
		cy.get('[data-cy="rnc-left-date-input"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '44px');

		cy.get('[data-cy="calendar-date-input-container"]')
			.should('have.css', 'width', '240px')
			.and('have.css', 'height', '44px');

		// .cdi-input
		cy.get('[data-cy="cdi-input"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '240px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)');

		// .select-date
		cy.get('[data-cy="select-date"]').should('have.css', 'pointer-events', 'none')
			.and('have.css', 'color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="prompt-title"]')
			.should('have.css', 'width', '181px')
			.and('have.css', 'margin-right', '55px')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'text-overflow', 'ellipsis')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '500')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="draws-buttons-wrapper"]')
			.should('have.css', 'overflow-y', 'auto');

		cy.get('[data-cy="dbc-draw"]')
			.should('have.css', 'height', '44px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '240px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '45px')
			.and('have.css', 'border', '1px solid rgb(227, 185, 11)');

		cy.get('[data-cy="dbc-number"]')
			.should('have.css', 'font-weight', '400')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="dbc-data"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="dbc-date"]')
			.should('have.css', 'font-size', '14px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="rnc-content-container"]')
			.should('have.css', 'border', '1px solid rgb(227, 185, 11)')
			.and('have.css', 'border-radius', '10px')
			.and('have.css', 'padding-right', '0px');

		cy.get('[data-cy="rnc-content-scroll"]')
			.should('have.css', 'height', '100%')
			.and('have.css', 'padding-top', '0px');

		cy.get('[data-cy="rnc-header-date"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(186, 151, 7)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'padding', '0px 20px');

		cy.get('[data-cy="rnc-content"]')
			.should('have.css', 'display', 'block');

		cy.get('[data-cy="kare-result-wrp"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'center');

		cy.get('[data-cy="kare-cards"], [data-cy="kare-poker"]')
			.should('have.css', 'width', '50%')
			.and('have.css', 'padding', '10px');

		cy.get('[data-cy="kare-result-title"]')
			.should('have.css', 'font-size', '20px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="krc-win-header"]')
			.should('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'margin-bottom', '10px');

		cy.get('[data-cy="card-with-label"]')
			.should('have.css', 'width', '90px');

		cy.get('[data-cy="kare-result-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'border-top', '1px solid rgb(161, 161, 161)')
			.and('have.css', 'height', '100%');

		cy.get('[data-cy="kare-poker"]')
			.should('have.css', 'border-left', '1px solid rgb(161, 161, 161)')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="krc-comb-items"]')
			.should('have.css', 'text-align', 'center');

		cy.get('[data-cy="krc-comb-none"]')
			.should('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'margin-right', '10px')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="kare-card-with-label-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="kcwl-label"]')
			.should('have.css', 'margin-bottom', '5px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="results-navigation-button-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between');

		cy.get('[data-cy="rnbc-prev"], [data-cy="rnbc-next"]')
			.should('have.css', 'width', '150px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="rb-cell"]')
			.should('have.css', 'display', 'none')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="rnc-print-results"]')
			.should('have.css', 'justify-self', 'center');

		cy.get('[data-cy="print-button-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'height', '100%');

		cy.get('[data-cy="pbc-page-count"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'padding-right', '10px');

		cy.get('[data-cy="pbc-page-count-lbl"]')
			.should('have.css', 'margin-right', '10px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="pbc-page-count-btn"]')
			.should('have.css', 'height', '44px')
			.and('have.css', 'width', '44px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '50px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(128, 128, 128)')
			.and('have.css', 'border-radius', '6px');

		cy.get('[data-cy="pbc-page-label"]')
			.should('have.css', 'margin', '0px 10px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="rnc-print-button"]')
			.should('have.css', 'width', '100px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'font-size', '18px');

	});
});
