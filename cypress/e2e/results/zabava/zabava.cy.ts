describe('Проверка лотереи Лото-Забава при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(0).click();
			cy.get('[data-cy="goto-results"]').click();
		});
	});

	it('Проверка стилей страницы результатов Лото-Забавы при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="lottery-container"]')
			.should('have.css', 'height', '930px')
			.and('have.css', 'padding', '20px 0px 30px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'grid-template', 'auto auto / 540px 600px / none')
			.and('have.css', 'grid-gap', '30px 10px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'margin-left', '360px')
			.and('have.css', 'height', '880px');

		cy.get('[data-cy="outlet-wrapper"]')
			.should('have.css', 'grid-column-start', '1')
			.and('have.css', 'grid-column-end', '3')
			.and('have.css', 'margin-left', '-360px')
			.and('have.css', 'height', '880px');

		cy.get('[data-cy="zabava-results-container"]')
			.should('have.css', 'display', 'block');

		cy.get('[data-cy="results-navigator-container"]')
			.should('have.css', 'grid-template', '80px 590px 80px / 350px 1470px / none')
			.and('have.css', 'grid-gap', '45px 40px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'padding-bottom', '5px')
			.and('have.css', 'top', '20px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '15px')
			.and('have.css', 'left', '0px');

		cy.get('[data-cy="rnc-logo"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '350px');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '80px')
			.and('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '36px');

		// .rt-cell
		cy.get('[data-cy="rt-cell"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-items', 'center');

		// .rnc-sell-btn
		cy.get('[data-cy="rnc-sell-btn"]')
			.should('have.css', 'padding', '0px 20px')
			.and('have.css', 'min-height', '80px')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'justify-self', 'end')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'display', 'block');

		// .rnc-left
		cy.get('[data-cy="rnc-left"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '66px 512px / 350px / none')
			.and('have.css', 'row-gap', '12px');

		// .rnc-left__date-input
		cy.get('[data-cy="rnc-left-date-input"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '66px');

		cy.get('[data-cy="calendar-date-input-container"]')
			.should('have.css', 'width', '350px')
			.and('have.css', 'height', '66px');

		// .cdi-input
		cy.get('[data-cy="cdi-input"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '66px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)');

		// .select-date
		cy.get('[data-cy="select-date"]').should('have.css', 'pointer-events', 'none')
			.and('have.css', 'color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="prompt-title"]')
			.should('have.css', 'width', '291px')
			.and('have.css', 'margin-right', '55px')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'text-overflow', 'ellipsis')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '500')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="draws-buttons-wrapper"]')
			.should('have.css', 'overflow-y', 'auto');

		cy.get('[data-cy="dbc-draw"]')
			.should('have.css', 'height', '66px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '350px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '45px')
			.and('have.css', 'border', '2px solid rgb(227, 185, 11)');

		cy.get('[data-cy="dbc-number"]')
			.should('have.css', 'font-weight', '700')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '10px');

		cy.get('[data-cy="dbc-data"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'text-align', 'left');

		cy.get('[data-cy="dbc-date"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="rnc-content-container"]')
			.should('have.css', 'border', '2px solid rgb(227, 185, 11)')
			.and('have.css', 'border-radius', '10px')
			.and('have.css', 'padding-right', '5px');

		cy.get('[data-cy="rnc-content-scroll"]')
			.should('have.css', 'height', '586px')
			.and('have.css', 'padding-top', '30px');

		cy.get('[data-cy="rnc-header-date"]')
			.should('have.css', 'width', '1461px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '50px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(186, 151, 7)')
			.and('have.css', 'display', 'block')
			.and('have.css', 'padding', '10px');

		cy.get('[data-cy="extra-info"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(186, 151, 7)')
			.and('have.css', 'animation', '2.2s cubic-bezier(0, 0, 0.35, 0.47) 0s infinite normal none running blinker')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'margin-bottom', '25px');

		cy.get('[data-cy="zrc-drawing-name"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="zrc-win-comb"]')
			.should('have.css', 'margin', '5px 34px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-wrap', 'wrap')
			.and('have.css', 'justify-content', 'center');

		cy.get('[data-cy="zrc-win-comb-item"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'width', '58px')
			.and('have.css', 'height', '58px')
			.and('have.css', 'margin', '3px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '50%')
			.and('have.css', 'border', '3px solid rgb(0, 151, 136)');

		cy.get('[data-cy="win-cat-table"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '53px 53px 53px 53px 53px')
			.and('have.css', 'height', '265px');

		cy.get('[data-cy="win-cat-table-row"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-columns', '730.5px 730.5px')
			.and('have.css', 'justify-items', 'center')
			.and('have.css', 'padding', '5px 0px');

		cy.get('[data-cy="win-cat-table-cell"]')
			.should('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="results-navigation-button-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between');

		cy.get('[data-cy="rnbc-prev"], [data-cy="rnbc-next"]')
			.should('have.css', 'width', '170px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'text-transform', 'none');

		cy.get('[data-cy="rb-cell"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="rnc-print-results"]')
			.should('have.css', 'justify-self', 'end');

		cy.get('[data-cy="print-button-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'height', '80px');

		cy.get('[data-cy="pbc-page-count"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'padding-right', '30px');

		cy.get('[data-cy="pbc-page-count-lbl"]')
			.should('have.css', 'margin-right', '16px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="pbc-page-count-btn"]')
			.should('have.css', 'height', '78px')
			.and('have.css', 'width', '80px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '50px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(128, 128, 128)')
			.and('have.css', 'border-radius', '6px');

		cy.get('[data-cy="pbc-page-label"]')
			.should('have.css', 'margin', '0px 24px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '46px')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="rnc-print-button"]')
			.should('have.css', 'width', '300px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px');

	});
});
