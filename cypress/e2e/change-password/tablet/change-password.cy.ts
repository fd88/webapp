describe('Проверка диалогового окна смены пароля при планшетном разрешении (767 x 1024 px)', () => {

	beforeEach(() => {
		cy.viewport(767, 1024);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="top-bar-hamburger"]').click();
			cy.get('[data-cy="change-password"]').click();
		});
	});

	it('Проверка диалогового окна смены пароля при планшетном разрешении (767 x 1024 px)', () => {
		cy.get('[data-cy="chp-dialog"]').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '70')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)')

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', 'none')
				.and('have.css', 'width', '767px')
				.and('have.css', 'padding', '54px 0px 0px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '0px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'margin', '0px')
				.and('have.css', 'height', '1024px')
				.and('have.css', 'border-radius', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'overflow-y', 'auto')
				.and('have.css', 'max-width', 'none')
				.and('have.css', 'position', 'relative')
				.and('have.css', 'text-align', 'left');

			cy.get('[data-cy="modal-buttons-container"]')
				.should('have.css', 'width', '767px')
				.and('have.css', 'margin-top', '30px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'padding', '10px 12px')
				.and('have.css', 'text-align', 'center');

			cy.get('[data-cy="button-modal-button"]')
				.should('have.css', 'font-size', '24px')
				.and('have.css', 'height', '60px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'max-width', 'none')
				.and('have.css', 'margin', '0px')
				.and('have.css', 'height', '60px')
				.and('have.css', 'color', 'rgb(161, 161, 161)')
				.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box');

			cy.get('[data-cy="circle-arc"]')
				.should('have.css', 'height', '175px')
				.and('have.css', 'border-top', '12px solid rgb(0, 151, 136)')
				.and('have.css', 'border-left', '12px solid rgb(183, 228, 225)')
				.and('have.css', 'border-bottom', '12px solid rgb(183, 228, 225)')
				.and('have.css', 'border-right', '12px solid rgb(183, 228, 225)')
				.and('have.css', 'border-radius', '50%')
				.and('have.css', 'animation', '2s linear 0s infinite normal none running rotate')
				.and('have.css', 'margin', '0px auto');

			cy.get('[data-cy="q-button"]')
				.should('have.css', 'width', '32px')
				.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'text-transform', 'uppercase')
				.and('have.css', 'cursor', 'pointer')
				.and('have.css', 'border-radius', '4px')
				.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
				.and('have.css', 'color', 'rgb(0, 151, 136)')
				.and('have.css', 'position', 'relative')
				.and('have.css', 'border', '1px solid rgb(0, 151, 136)');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(0, 151, 136)');

			cy.get('[data-cy="chp-dialog-inner"]')
				.should('have.css', 'overflow-y', 'hidden')
				.and('have.css', 'display', 'inline')
				.and('have.css', 'width', '0px')
				.and('have.css', 'height', '0px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'left', '24px')
				.and('have.css', 'top', '14px')
				.and('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'z-index', '20')
				.and('have.css', 'right', '719px');

			cy.get('[data-cy="chp-dialog-header"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'left', '0px')
				.and('have.css', 'top', '0px')
				.and('have.css', 'width', '767px')
				.and('have.css', 'height', '54px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 8px 0px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
				.and('have.css', 'z-index', '10');

			cy.get('[data-cy="ch-status"]')
				.should('have.css', 'padding', '8px')
				.and('have.css', 'margin-top', '0px');

			cy.get('[data-cy="chp-fields-item"]')
				.should('have.css', 'margin', '16px 16px 24px');

			cy.get('[data-cy="eye-input"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'margin-bottom', '5px')
				.and('have.css', 'width', '372px')
				.and('have.css', 'height', '71px')
				.and('have.css', 'padding', '0px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'color', 'rgb(40, 40, 40)')
				.and('have.css', 'font-size', '16px')
				.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
				.and('have.css', 'border', '0px none rgb(40, 40, 40)')
				.and('have.css', 'border-radius', '0px');
		});
	});
});
