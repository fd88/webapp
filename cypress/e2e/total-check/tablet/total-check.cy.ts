import {checkCalculator} from "../../common/tablet/check-calculator";

describe('Проверка суммарного чека при планшетном разрешении (767 x 1024 px)', () => {

	beforeEach(() => {
		cy.viewport(767, 1024);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
		});
	});

	it('Проверка стилей суммарного чека при планшетном разрешении (767 x 1024 px)', () => {
		checkCalculator();
	});
});
