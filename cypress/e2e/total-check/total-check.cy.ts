import {checkCalculator} from "../common/check-calculator";

describe('Проверка суммарного чека при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
		});
	});

	it('Проверка стилей суммарного чека при десктопном разрешении (1920px)', () => {
		checkCalculator();
	});
});
