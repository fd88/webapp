import {checkCalculator} from "../../common/mobile/check-calculator";

describe('Проверка суммарного чека при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
		});
	});

	it('Проверка стилей суммарного чека при мобильном разрешении (393 x 873 px)', () => {
		checkCalculator();
	});
});
