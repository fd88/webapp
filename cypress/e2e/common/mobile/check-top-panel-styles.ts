export function checkTopPanelStyles(): void {
	cy.get('[data-cy="scandit-scanner-wrapper"]')
		.should('have.css', 'position', 'fixed')
		.and('have.css', 'display', 'none')
		.and('have.css', 'width', '393px')
		.and('have.css', 'height', '382px')
		.and('have.css', 'overflow', 'hidden');

	cy.get('[data-cy="scandit-scanner"]')
		.should('have.css', 'display', 'inline-block')
		.and('have.css', 'width', '100%')
		.and('have.css', 'height', '100%')
		.and('have.css', 'vertical-align', 'middle');

	cy.get('[data-cy="hamburger-menu-container"]')
		.should('have.css', 'position', 'absolute')
		.and('have.css', 'top', '0px')
		.and('have.css', 'width', '380px')
		.and('have.css', 'z-index', '20')
		.and('have.css', 'color', 'rgb(137, 137, 137)')
		.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.2) 0px 0px 8px 0px');

	cy.get('[data-cy="hamburger-close"]')
		.should('have.css', 'display', 'block')
		.and('have.css', 'width', '100%')
		.and('have.css', 'height', '44px')
		.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.1) 0px -8px 8px -4px inset')
		.and('have.css', 'position', 'relative');

	cy.get('[data-cy="hamburger-close-button"]')
		.should('have.css', 'position', 'absolute')
		.and('have.css', 'width', '16px')
		.and('have.css', 'height', '16px')
		.and('have.css', 'left', '24px')
		.and('have.css', 'top', '50%')
		.and('have.css', 'transform', 'none')
		.and('have.css', 'display', 'block')
		.and('have.css', 'margin-right', '0px')
		.and('have.css', 'margin-left', 'auto');

	cy.get('[data-cy="cross-image"]')
		.should('have.css', 'width', '100%')
		.and('have.css', 'height', '100%')
		.and('have.css', 'display', 'block');

	cy.get('[data-cy="hamburger-menu-main"]')
		.should('have.css', 'background-color', 'rgb(255, 255, 255)')
		.and('have.css', 'overflow', 'hidden');

	cy.get('[data-cy="hamburger-menu"]')
		.should('have.css', 'margin', '0px')
		.and('have.css', 'list-style', 'outside none disc')
		.and('have.css', 'padding-left', '0px');

	cy.get('[data-cy="hamburger-menu-item"]')
		.should('have.css', 'display', 'list-item')
		.and('have.css', 'color', 'rgb(161, 161, 161)')
		.and('have.css', 'font-size', '24px')
		.and('have.css', 'margin-bottom', '15px');

	cy.get('[data-cy="hamburger-menu-text"]')
		.should('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-size', '24px');

	cy.get('[data-cy="cancel-last"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'width', '0px')
		.and('have.css', 'min-height', '42px')
		.and('have.css', 'margin', '15px auto')
		.and('have.css', 'color', 'rgb(161, 161, 161)')
		.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'border', '0px none rgb(161, 161, 161)')
		.and('have.css', 'border-radius', '6px')
		.and('have.css', 'font-size', '18px');

	cy.get('[data-cy="lang-switcher"]')
		.should('have.css', 'padding', '0px 5px')
		.and('have.css', 'margin-bottom', '15px')
		.and('have.css', 'border-top', '1px solid rgb(161, 161, 161)')
		.and('have.css', 'border-bottom', '1px solid rgb(161, 161, 161)');

	cy.get('[data-cy="lang-switcher-lang"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'margin', '15px 5px');

	cy.get('[data-cy="lang-switcher-radio"]')
		.should('have.css', 'width', '20px')
		.and('have.css', 'height', '20px')
		.and('have.css', 'border-radius', '50%')
		.and('have.css', 'border', '2px solid rgb(90, 90, 90)')
		.and('have.css', 'position', 'relative')
		.and('have.css', 'margin-right', '10px');

	cy.get('[data-cy="lang-switcher-title"]')
		.should('have.css', 'font-size', '24px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)');

	cy.get('[data-cy="change-password"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'position', 'relative')
		.and('have.css', 'border', '1px solid rgb(90, 90, 90)')
		.and('have.css', 'width', '0px')
		.and('have.css', 'min-height', '42px')
		.and('have.css', 'padding-right', '22px')
		.and('have.css', 'margin-left', '10px')
		.and('have.css', 'margin-bottom', '15px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-size', '24px')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'border-radius', '8px')
		.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box');

	cy.get('[data-cy="change-password-icon-1"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'position', 'absolute')
		.and('have.css', 'right', '11px')
		.and('have.css', 'width', '32px')
		.and('have.css', 'height', '32px')
		.and('have.css', 'top', '5px')
		.and('have.css', 'background-color', 'rgb(229, 248, 248)')
		.and('have.css', 'border-radius', '100%')
		.and('have.css', 'border', '1px solid rgb(0, 151, 136)');

	cy.get('[data-cy="change-password-icon-2"]')
		.should('have.css', 'color', 'rgb(0, 151, 136)')
		.and('have.css', 'font-family', '"Material Icons"')
		.and('have.css', 'font-weight', '400')
		.and('have.css', 'font-style', 'normal')
		.and('have.css', 'font-size', '24px')
		.and('have.css', 'display', 'block')
		.and('have.css', 'line-height', '24px')
		.and('have.css', 'text-transform', 'none')
		.and('have.css', 'letter-spacing', '0')
		.and('have.css', 'word-wrap', 'normal')
		.and('have.css', 'white-space', 'nowrap')
		.and('have.css', 'direction', 'ltr')
		.and('have.css', '-webkit-font-smoothing', 'antialiased')
		.and('have.css', 'text-rendering', 'optimizelegibility')
		.and('have.css', 'font-feature-settings', '"liga"');

	cy.get('[data-cy="hm-help"]')
		.should('have.css', 'padding', '12px 5px')
		.and('have.css', 'position', 'relative')
		.and('have.css', 'text-align', 'center')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'font-size', '18px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'border-top', '1px solid rgb(161, 161, 161)');

	cy.get('[data-cy="hm-help-q"]')
		.should('have.css', 'width', '26px')
		.and('have.css', 'height', '26px')
		.and('have.css', 'top', '50%')
		.and('have.css', 'right', '28px')
		.and('have.css', 'color', 'rgb(255, 255, 255)')
		.and('have.css', 'font-size', '22px')
		.and('have.css', 'font-family', 'sans-serif')
		.and('have.css', 'font-weight', '700')
		.and('have.css', 'display', 'block')
		.and('have.css', 'position', 'absolute')
		.and('have.css', 'transform', 'none')
		.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'border-radius', '5px');

	cy.get('[data-cy="hm-system-info"]')
		.should('have.css', 'padding', '16px 5px')
		.and('have.css', 'position', 'relative')
		.and('have.css', 'text-align', 'center')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'font-size', '18px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'border-top', '1px solid rgb(161, 161, 161)');

	cy.get('[data-cy="page-wrapper"]')
		.should('have.css', 'display', 'grid')
		.and('have.css', 'grid-template', '44px 829px / 393px / none')
		.and('have.css', 'grid-template-rows', '44px 829px')
		.and('have.css', 'grid-template-columns', '393px')
		.and('have.css', 'min-height', '100%')
		.and('have.css', 'background-color', 'rgba(0, 0, 0, 0)');

	cy.get('[data-cy="top-bar-panel"]')
		.should('have.css', 'display', 'flex');

	cy.get('[data-cy="top-bar"]')
		.should('have.css', 'width', '393px')
		.and('have.css', 'height', '44px')
		.and('have.css', 'display', 'grid')
		.and('have.css', 'grid-auto-columns', '1fr')
		.and('have.css', 'grid-auto-flow', 'column')
		.and('have.css', 'border-bottom', '1px solid rgb(0, 151, 136)')
		.and('have.css', 'background', 'rgba(0, 0, 0, 0) linear-gradient(rgb(180, 225, 220) 0%, rgb(255, 255, 255) 100%) repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'padding', '0px 8px');

	cy.get('[data-cy="top-bar-left"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'align-items', 'center');

	cy.get('[data-cy="top-bar-hamburger"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'width', '40px')
		.and('have.css', 'height', '32px')
		.and('have.css', 'border-radius', '8px')
		.and('have.css', 'border', '0px none rgb(40, 40, 40)')
		.and('have.css', 'background-color', 'rgba(0, 0, 0, 0)');

	cy.get('[data-cy="tbp-hamburger-icon"]')
		.should('have.css', 'padding-left', '0px')
		.and('have.css', 'color', 'rgb(0, 151, 136)')
		.and('have.css', 'font-size', '40px')
		.and('have.css', 'pointer-events', 'none');

	cy.get('[data-cy="tbp-hamburger-title"]')
		.should('have.css', 'padding-left', '30px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-size', '18px')
		.and('have.css', 'font-weight', '700')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'pointer-events', 'none');

	cy.get('[data-cy="top-bar-button"]')
		.should('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'margin', '0px 10px')
		.and('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'width', '32px')
		.and('have.css', 'height', '32px')
		.and('have.css', 'border-radius', '100%');

	cy.get('[data-cy="button-home-icon"]')
		.should('have.css', 'color', 'rgb(161, 161, 161)')
		.and('have.css', 'font-size', '24px');

	cy.get('[data-cy="top-bar-date"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center');

	cy.get('[data-cy="tbp-date-date"]')
		.should('have.css', 'color', 'rgb(161, 161, 161)')
		.and('have.css', 'margin-right', '5px')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-size', '14px');

	cy.get('[data-cy="tbp-date-time"]')
		.should('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-size', '14px');

	cy.get('[data-cy="top-bar-right"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'flex-end');

	cy.get('[data-cy="top-bar-status-item"]')
		.should('have.css', 'margin', '0px 10px');

	cy.get('[data-cy="top-bar-button"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'width', '32px')
		.and('have.css', 'height', '32px')
		.and('have.css', 'border-radius', '100%');

	cy.get('[data-cy="log-out-container"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'width', '32px')
		.and('have.css', 'height', '32px')
		.and('have.css', 'border-radius', '100%');
}
