export function checkCalculator(): void {
	cy.get('[data-cy="panel"]')
		.should('have.css', 'margin-bottom', '20px')
		.and('have.css', 'position', 'relative');

	cy.get('[data-cy="total-check-component"]')
		.should('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'display', 'grid')
		.and('have.css', 'align-items', 'end')
		.and('have.css', 'grid-template-rows', '284px 0px 80px');

	cy.get('[data-cy="tcc-list"]')
		.should('have.css', 'max-height', '272px')
		.and('have.css', 'opacity', '1')
		.and('have.css', 'margin-bottom', '12px')
		.and('have.css', 'display', 'grid')
		.and('have.css', 'grid-template-rows', '67px 203px')
		.and('have.css', 'border-radius', '8px')
		.and('have.css', 'border', '1px solid rgb(128, 128, 128)')
		.and('have.css', 'background-color', 'rgb(255, 255, 255)')
		.and('have.css', 'position', 'relative')
		.and('have.css', 'overflow', 'hidden')
		.and('have.css', 'transition', 'max-height 0.3s ease 0s, opacity 0.3s ease 0s, margin-bottom 0.3s ease 0s');

	cy.get('[data-cy="tco-actions"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'column')
		.and('have.css', 'overflow-y', 'auto')
		.and('have.css', 'margin-left', '5px')
		.and('have.css', 'margin-right', '5px')
		.and('have.css', 'order', '2');

	cy.get('[data-cy="tco-action-row"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'min-height', '67px')
		.and('have.css', 'padding-left', '8px')
		.and('have.css', 'padding-right', '8px')
		.and('have.css', 'position', 'relative');

	cy.get(	'[data-cy="tcc-list-cell-col-date"],' +
		'[data-cy="tcc-list-cell-col-lottery"],' +
		'[data-cy="tcc-list-cell-col-action"],' +
		'[data-cy="tcc-list-cell-col-count"],' +
		'[data-cy="tcc-list-cell-col-amount"],' +
		'[data-cy="tcc-list-cell-col-result"]'
	)
		.should('have.css', 'font-size', '36px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'text-align', 'center');

	cy.get('[data-cy="tcc-list-label"]')
		.should('have.css', 'display', 'none')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

	cy.get('[data-cy="tcc-list-cell-col-lottery"]')
		.should('have.css', 'font-size', '36px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'text-align', 'center');

	cy.get('[data-cy="tcc-list-cell-col-action"]')
		.should('have.css', 'font-size', '36px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'text-align', 'center');

	cy.get('[data-cy="tcc-list-cell-col-count"]')
		.should('have.css', 'font-size', '36px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'text-align', 'center');

	cy.get('[data-cy="tcc-list-cell-col-amount"]')
		.should('have.css', 'font-size', '36px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'text-align', 'center');

	cy.get('[data-cy="tcc-list-cell-col-result"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'font-size', '36px')
		.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'color', 'rgb(90, 90, 90)')
		.and('have.css', 'text-align', 'center');

	cy.get('[data-cy="tcc-list-icon"]')
		.should('have.css', 'width', '50px')
		.and('have.css', 'height', '50px');

	cy.get('[data-cy="tco-header-row"]')
		.should('have.css', 'padding-right', '33px')
		.and('have.css', 'border-bottom', '1px solid rgb(128, 128, 128)')
		.and('have.css', 'order', '1')
		.and('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'min-height', '67px')
		.and('have.css', 'position', 'relative');

	cy.get('[data-cy="tcc-main"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row');

	cy.get('[data-cy="tc-up-down"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'width', '80px')
		.and('have.css', 'height', '80px')
		.and('have.css', 'font-size', '36px')
		.and('have.css', 'color', 'rgb(255, 255, 255)')
		.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'width', '80px')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'text-align', 'center')
		.and('have.css', 'text-transform', 'uppercase')
		.and('have.css', 'cursor', 'pointer')
		.and('have.css', 'border-radius', '8px');

	cy.get('[data-cy="tc-amount"]')
		.should('have.css', 'height', '80px')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-size', '36px')
		.and('have.css', 'color', 'rgb(161, 161, 161)')
		.and('have.css', 'text-align', 'center')
		.and('have.css', 'text-transform', 'uppercase')
		.and('have.css', 'cursor', 'pointer')
		.and('have.css', 'border-radius', '8px')
		.and('have.css', 'margin-right', '12px')
		.and('have.css', 'margin-left', '12px')
		.and('have.css', 'flex-grow', '1')
		.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'position', 'relative')
		.and('have.css', 'display', 'flex')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'align-items', 'center');

	cy.get('[data-cy="total-order-amount"]')
		.should('have.css', 'font-size', '36px')
		.and('have.css', 'color', 'rgb(161, 161, 161)');

	cy.get('[data-cy="tc-up-down-icon"]')
		.should('have.css', 'margin-right', '0px');

	cy.get('[data-cy="tc-clear"]')
		.should('have.css', 'width', '350px')
		.and('have.css', 'height', '80px')
		.and('have.css', 'font-size', '36px')
		.and('have.css', 'color', 'rgb(255, 255, 255)')
		.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'text-align', 'center')
		.and('have.css', 'text-transform', 'uppercase')
		.and('have.css', 'cursor', 'pointer')
		.and('have.css', 'border-radius', '8px');

}
