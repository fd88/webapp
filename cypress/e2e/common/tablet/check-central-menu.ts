export function checkCentralMenu(): void {
	cy.get('[data-cy="page-main-central-menu"]')
		.should('have.css', 'display', 'block')
		.and('have.css', 'margin', '12px 0px 0px');

	cy.get('[data-cy="central-menu"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'margin', '0px')
		.and('have.css', 'max-width', '616px')
		.and('have.css', 'width', '616px')
		.and('have.css', 'border-radius', '8px')
		.and('have.css', 'border-color', 'rgb(0, 151, 136)')
		.and('have.css', 'border-style', 'solid')
		.and('have.css', 'border-width', '2px');

	cy.get('[data-cy="central-menu-item"]')
		.should('have.css', 'display', 'flex')
		.and('have.css', 'flex-direction', 'row')
		.and('have.css', 'flex-grow', '1')
		.and('have.css', 'align-items', 'center')
		.and('have.css', 'justify-content', 'center')
		.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
		.and('have.css', 'font-weight', '500')
		.and('have.css', 'font-size', '14px')
		.and('have.css', 'text-transform', 'uppercase')
		.and('have.css', 'text-align', 'center')
		.and('have.css', 'cursor', 'pointer')
		.and('have.css', 'min-height', '44px')
		.and('have.css', 'padding-left', '5px')
		.and('have.css', 'padding-right', '5px')
		.and('have.css', 'border-width', '0px');

}
