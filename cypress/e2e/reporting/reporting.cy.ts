describe('Проверка страницы отчетов при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(3).click();

		});
	});

	it('Проверка страницы отчетов при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="reporting-page-container"]')
			.should('have.css', 'height', '806px');

		cy.get('[data-cy="rp-columns"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'padding', '0px 15px')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'height', '806px');

		cy.get('[data-cy="rp-columns-left"], [data-cy="rp-columns-right"]')
			.should('have.css', 'width', '640px')
			.and('have.css', 'margin', '50px 80px');

		cy.get('[data-cy="rp-columns-left"]').should('have.css', 'max-width', '640px');

		cy.get('[data-cy="rp-columns-outline"]').should('have.css', 'border-radius', '8px')
			.and('have.css', 'border-width', '2px')
			.and('have.css', 'border-style', 'solid');

		cy.get('[data-cy="rp-hdr"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'margin', '0px 0px 12px')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="reports-menu"]')
			.should('have.css', 'padding', '0px 30px');

		cy.get('[data-cy="reports-menu-item"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'width', '536px')
			.and('have.css', 'padding', '10px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="rp-scroll"]')
			.should('have.css', 'position', 'relative');

		cy.get('[data-cy="custom-scrollbar"]')
			.should('have.css', 'position', 'relative')
			.and('have.css', 'overflow-y', 'scroll')
			.and('have.css', 'height', '100%');

		cy.get('[data-cy="op-field-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')

		cy.get('[data-cy="report-form"]')
			.should('have.css', 'width', '856px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')

		cy.get('[data-cy="result"]').should('have.css', 'padding', '10px')

		cy.get('[data-cy="inputs-periods-item"]').should('have.css', 'display', 'block')
			.and('have.css', 'margin', '12px 253px')
			.and('have.css', 'width', '350px')
			.and('have.css', 'max-width', '350px')

		cy.get('[data-cy="inputs-periods-radio"]')
			.should('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(0, 0, 0)');

		cy.get('[data-cy="inputs-periods-button"]')
			.should('have.css', 'height', '66px')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="rp-request"]')
			.should('have.css', 'width', '870px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'flex-shrink', '0');

		cy.get('[data-cy="rp-scroll"]')
			.should('have.css', 'height', '569px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="reporting-navigation-content"]')
			.should('have.css', 'height', '569px');

		cy.get('[data-cy="rp-columns-right"]')
			.should('have.css', 'max-width', '880px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'column nowrap')
			.and('have.css', 'justify-content', 'flex-start')
			.and('have.css', 'width', '870px')
			.and('have.css', 'margin', '50px 80px');

		cy.get('[data-cy="rp-columns-outline"]')
			.should('have.css', 'border-radius', '8px')
			.and('have.css', 'border-width', '2px')
			.and('have.css', 'border-style', 'solid')
			.and('have.css', 'padding', '10px 20px 20px')
			.and('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'margin-bottom', '0px')
			.and('have.css', 'height', '250px')
			.and('have.css', 'max-height', 'none');

		cy.get('[data-cy="reporting-inputs"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'justify-content', 'space-evenly')
			.and('have.css', 'height', '569px');

		cy.get('[data-cy="inputs-periods"]')
			.should('have.css', 'max-width', '100%')
			.and('have.css', 'width', '856px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'display', 'block')

		cy.get('[data-cy="result"]')
			.should('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="op-field-title"]')
			.should('have.css', 'margin-bottom', '8px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="input-operation-field-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="cdi-input"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '350px')
			.and('have.css', 'height', '66px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)');

		cy.get('[data-cy="cdi-input-title"]')
			.should('have.css', 'width', '291px')
			.and('have.css', 'margin-right', '55px')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'text-overflow', 'ellipsis')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '500')
			.and('have.css', 'font-size', '36px');

	});
});
