describe('Проверка страницы отчетов при планшетном разрешении (767 x 1024 px)', () => {

	beforeEach(() => {
		cy.viewport(767, 1024);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(3).click();

		});
	});

	it('Проверка страницы отчетов при планшетном разрешении (767 x 1024 px)', () => {
		cy.get('[data-cy="reporting-page-container"]')
			.should('have.css', 'height', '911px');

		cy.get('[data-cy="rp-columns"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'height', '895px');

		cy.get('[data-cy="rp-columns-left"], [data-cy="rp-columns-right"]')
			.should('have.css', 'width', '743px')
			.and('have.css', 'margin', '0px 0px 25px');

		cy.get('[data-cy="rp-columns-left"]').should('have.css', 'max-width', 'none');

		cy.get('[data-cy="rp-columns-outline"]').should('have.css', 'border-radius', '8px')
			.and('have.css', 'border-width', '1px')
			.and('have.css', 'border-style', 'solid');

		cy.get('[data-cy="rp-hdr"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'margin', '0px 0px 12px')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="reports-menu"]')
			.should('have.css', 'padding', '0px 30px');

		cy.get('[data-cy="reports-menu-item"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'width', '665px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'border', '0px none rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="rp-scroll"]')
			.should('have.css', 'position', 'relative');

		cy.get('[data-cy="custom-scrollbar"]')
			.should('have.css', 'position', 'relative')
			.and('have.css', 'overflow-y', 'scroll')
			.and('have.css', 'height', '100%');

		cy.get('[data-cy="op-field-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')

		cy.get('[data-cy="report-form"]')
			.should('have.css', 'width', '741px')
			.and('have.css', 'margin', '35px 0px 25px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')

		cy.get('[data-cy="result"]').should('have.css', 'padding', '10px')

		cy.get('[data-cy="inputs-periods-item"]').should('have.css', 'display', 'block')
			.and('have.css', 'margin', '0px 250.5px')
			.and('have.css', 'width', '240px')
			.and('have.css', 'max-width', '240px')

		cy.get('[data-cy="inputs-periods-radio"]')
			.should('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(0, 0, 0)');

		cy.get('[data-cy="inputs-periods-button"]')
			.should('have.css', 'height', '44px')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="rp-request"]')
			.should('have.css', 'width', '743px')
			.and('have.css', 'height', '60px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'background', 'rgb(0, 90, 81) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'flex-shrink', '0');

		cy.get('[data-cy="rp-scroll"]')
			.should('have.css', 'height', '732px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="reporting-navigation-content"]')
			.should('have.css', 'height', '732px');

		cy.get('[data-cy="rp-columns-right"]')
			.should('have.css', 'max-width', 'none')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'column nowrap')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'width', '743px')
			.and('have.css', 'margin', '0px');

		cy.get('[data-cy="rp-columns-outline"]')
			.should('have.css', 'border-radius', '8px')
			.and('have.css', 'border-width', '1px')
			.and('have.css', 'border-style', 'solid')
			.and('have.css', 'padding', '0px 8px')
			.and('have.css', 'border-color', 'rgb(128, 128, 128)')
			.and('have.css', 'margin-bottom', '0px')
			.and('have.css', 'height', '46px')
			.and('have.css', 'max-height', 'none');

		cy.get('[data-cy="reporting-inputs"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'justify-content', 'flex-start')
			.and('have.css', 'height', '732px');

		cy.get('[data-cy="inputs-periods"]')
			.should('have.css', 'max-width', '100%')
			.and('have.css', 'width', '741px')
			.and('have.css', 'margin', '5px 0px 40px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="result"]')
			.should('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="op-field-title"]')
			.should('have.css', 'margin-bottom', '4px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="input-operation-field-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'margin-bottom', '10px');

		cy.get('[data-cy="cdi-input"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '240px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)');

		cy.get('[data-cy="cdi-input-title"]')
			.should('have.css', 'width', '206px')
			.and('have.css', 'margin-right', '30px')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'text-overflow', 'ellipsis')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '500')
			.and('have.css', 'font-size', '24px');

	});
});
