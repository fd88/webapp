import {checkTopPanelStyles} from "../common/check-top-panel-styles";
import {checkCalculator} from "../common/check-calculator";
import {checkCentralMenu} from "../common/check-central-menu";

describe('Проверка страницы со списком лотерей при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
		});
	});

	it('Проверка диалогового окна со списком джек-потов при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="jackpots-dialog"] [data-cy="modal-dialog-background"]')
			.should('have.css', 'padding', '15px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'position', 'fixed')
			.and('have.css', 'top', '0px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '0px')
			.and('have.css', 'z-index', '75')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)')

		cy.get('[data-cy="jackpots-dialog"] [data-cy="modal-dialog-container"]')
			.should('have.css', 'max-width', '960px')
			.and('have.css', 'width', '960px')
			.and('have.css', 'padding', '30px 76px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'text-align', 'left')
			.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
			.and('have.css', 'background-color', 'rgb(255, 255, 255)')
			.and('have.css', 'border-radius', '8px')

		cy.get('[data-cy="jackpots-dialog"] [data-cy="modal-dialog-close"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'right', '15px')
			.and('have.css', 'top', '15px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'margin-right', '0px')
			.and('have.css', 'margin-left', '0px')

		cy.get('[data-cy="jackpots-title"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-bottom', '50px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'color', 'rgb(90, 90, 90)')


		cy.get('[data-cy="jackpots-title-sub"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')

		cy.get('[data-cy="jackpot-block"]')
			.should('have.css', 'width', '580px')
			.and('have.css', 'max-width', '580px')
			.and('have.css', 'margin', '30px 114px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center')

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'width', '140px')
			.and('have.css', 'height', '70px')
			.and('have.css', 'border', '1px solid rgb(229, 229, 229)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'margin-right', '14px')
			.and('have.css', 'padding', '5px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '128px')
			.and('have.css', 'height', '58px')
			.and('have.css', 'object-fit', 'contain');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="jackpot-sum"]')
			.should('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'right');

		cy.get('[data-cy="modal-buttons-container"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '100%')
			.and('have.css', 'margin-top', '18px')
			.and('have.css', 'flex-direction', 'column');

		cy.get('[data-cy="button-modal-button"]')
			.should('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'width', '100%')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'height', '60px')
			.and('have.css', 'display', 'block');
	});

	it('Проверка страницы лотерей при десктопном разрешении (1920px)', () => {
		checkTopPanelStyles();

		cy.get('[data-cy="page-main"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '124px 0px 806px')
			.and('have.css', 'height', '930px')
			.and('have.css', 'padding', '0px 30px');

		checkCentralMenu();

		cy.get('[data-cy="panel"]')
			.should('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="game-list"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'justify-content', 'normal')
			.and('have.css', 'flex-flow', 'row nowrap')
			.and('have.css', 'overflow-y', 'auto');

		cy.get('[data-cy="game-list-item"]')
			.should('have.css', 'max-width', '240px')
			.and('have.css', 'width', '240px')
			.and('have.css', 'margin', '0px 16px 30px')
			.and('have.css', 'cursor', 'pointer');

		cy.get('[data-cy="game-list-inner"]')
			.should('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(255, 255, 255) 50%, rgb(219, 240, 238) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '10px')
			.and('have.css', 'height', '150px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'padding', '10px 20px')
			.and('have.css', 'margin-bottom', '10px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)');

		cy.get('[data-cy="game-list-bg"]')
			.should('have.css', 'left', '20px')
			.and('have.css', 'top', '10px')
			.and('have.css', 'right', '20px')
			.and('have.css', 'bottom', '10px')
			.and('have.css', 'background-size', 'contain')
			.and('have.css', 'background-position', '50% 50%')
			.and('have.css', 'background-repeat', 'no-repeat')
			.and('have.css', 'position', 'absolute');

		cy.get('[data-cy="game-list-title"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '14px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '128px')
			.and('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '24px');


		checkCalculator();
	});
});
