describe('Проверка Стирачек при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(4).click();
		});
	});

	it('Проверка стилей страницы БМЛ при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="tmlbml-container"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '76px 666px 88px')
			.and('have.css', 'height', '930px')
			.and('have.css', 'row-gap', '25px')
			.and('have.css', 'grid-row-gap', '25px')
			.and('have.css', 'padding', '30px 0px 20px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '30px')
			.and('have.css', 'right', '30px')
			.and('have.css', 'top', '0px')
			.and('have.css', 'bottom', '0px');

		cy.get('[data-cy="bml-game-logo"]')
			.should('have.css', 'margin-left', '-5px');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '5px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-right"]')
			.should('have.css', 'margin-top', '-10px');

		cy.get('[data-cy="input-tmlbml"]')
			.should('have.css', 'height', '666px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="input-tmlbml-container"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '300px 362px')
			.and('have.css', 'height', '662px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="bml-list"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '300px');

		cy.get('[data-cy="tml-bml-list-container"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '66px 168px 66px')
			.and('have.css', 'max-height', '100%')
			.and('have.css', 'padding', '0px 30px')
			.and('have.css', 'margin-right', '-5px');

		cy.get('[data-cy="bml-scroll"]')
			.should('have.css', 'padding-right', '5px')
			.and('have.css', 'overflow-y', 'auto')
			.and('have.css', 'order', '2');

		cy.get('[data-cy="bml-group"]')
			.should('have.css', 'border', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'padding-top', '12px')
			.and('have.css', 'padding-bottom', '6px');

		cy.get('[data-cy="bml-group-title"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="cell"]').should('have.css', 'flex', '1 1 0%')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="close-icon"]').should('have.css', 'color', 'rgb(223, 80, 80)')
			.and('have.css', 'vertical-align', 'middle')
			.and('have.css', 'width', '30px')
			.and('have.css', 'height', '30px')
			.and('have.css', 'font-size', '30px');

		cy.get('[data-cy="bml-row"]').should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-columns', '104px 300px 650px 255px 115px 260px 60px')
			.and('have.css', 'grid-column-gap', '5px')
			.and('have.css', 'column-gap', '5px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'justify-items', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin', '20px 0px 17px');

		cy.get('[data-cy="two-step-panel-container"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '362px')
			.and('have.css', 'align-content', 'space-between')
			.and('have.css', 'height', '362px');

		cy.get('[data-cy="bottom-block"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'justify-content', 'flex-end')
			.and('have.css', 'height', '362px');

		cy.get('[data-cy="camera-block"]')
			.should('have.css', 'flex-grow', '1')
			.and('have.css', 'padding', '0px 8px 8px')
			.and('have.css', 'height', '282px');

		cy.get('[data-cy="camera-spoiler"]')
			.should('have.css', 'max-width', '616px')
			.and('have.css', 'margin', '0px 612px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '49px 221px / 612px / none')
			.and('have.css', 'border', '2px solid rgb(128, 128, 128)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'width', '616px')
			.and('have.css', 'height', '274px')
			.and('have.css', 'max-height', '1080px')
			.and('have.css', 'transition', 'max-height 0.3s ease 0s')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="spoiler-header"]')
			.should('have.css', 'padding', '8px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '0px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'align-items', 'normal')
			.and('have.css', 'justify-content', 'normal')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="close-button"]').should('have.css', 'position', 'absolute')
			.and('have.css', 'top', '12px')
			.and('have.css', 'right', '14px')
			.and('have.css', 'width', '16px')
			.and('have.css', 'height', '16px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-right', '0px')
			.and('have.css', 'margin-left', '0px');

		cy.get('[data-cy="spoiler-body"]').should('have.css', 'padding', '20px')
			.and('have.css', 'overflow', 'auto');

		cy.get('[data-cy="scanner-block"]').should('have.css', 'height', '211px');

		cy.get('[data-cy="scanner-component"]').should('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '211px');

		cy.get('[data-cy="scandit-block"]').should('have.css', 'border-radius', '8px')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'height', '211px');

		cy.get('[data-cy="tsp-panel"]').should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'padding', '13px 40px')
			.and('have.css', 'margin-left', '2px')
			.and('have.css', 'margin-right', '2px')
			.and('have.css', 'border-bottom-right-radius', '10px')
			.and('have.css', 'border-bottom-left-radius', '10px')
			.and('have.css', 'background', 'rgb(255, 255, 204) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="tsp-row"]').should('have.css', 'position', 'relative')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-columns', '1522px 240px')
			.and('have.css', 'grid-column-gap', '10px')
			.and('have.css', 'column-gap', '10px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="barcode-col"]').should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="step-title"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'line-height', '24px')
			.and('have.css', 'text-align', 'right')
			.and('have.css', 'margin-right', '40px')
			.and('have.css', 'white-space', 'nowrap');

		cy.get('[data-cy="msl-input-barcode"]').should('have.css', 'max-width', '300px')
			.and('have.css', 'width', '300px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'height', '54px');

		cy.get('[data-cy="barcode-input"]')
			.should('have.css', 'padding', '0px 10px 0px 5px')
			.and('have.css', 'width', '300px')
			.and('have.css', 'height', '54px')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'border', '1px solid rgb(128, 128, 128)')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="barcode-input-input"]')
			.should('have.css', 'padding-left', '5px')
			.and('have.css', 'flex-grow', '1')
			.and('have.css', 'height', '52px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'border-radius', '4px');

		cy.get('[data-cy="mib-score"]').should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'display', 'none');

		cy.get('[data-cy="manual-enter"]')
			.should('have.css', 'border', '0px none rgb(0, 0, 0)')
			.and('have.css', 'display', 'none')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'width', '54px')
			.and('have.css', 'height', '54px')
			.and('have.css', 'border-radius', '4px');

		cy.get('[data-cy="by-range"]').should('have.css', 'width', '240px')
			.and('have.css', 'height', '54px')
			.and('have.css', 'border-radius', '6px')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="bottom-panel"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'width', '1860px')
			.and('have.css', 'justify-content', 'flex-end');

		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'max-width', '600px')
			.and('have.css', 'width', '600px')
			.and('have.css', 'height', '88px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="button"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'width', '600px')
			.and('have.css', 'height', '88px')
			.and('have.css', 'padding-left', '10px')
			.and('have.css', 'padding-right', '10px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgb(0, 90, 81) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'cursor', 'pointer');
	});
});
