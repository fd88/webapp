describe('Проверка лотереи ГнГ при десктопном разрешении (1920px)', () => {

	function checkAttentionModal(nextDialogIndex): void {
		cy.get('[data-cy="gg-bg-draws"] [data-cy="buttons-group-item"]').eq(4).click();
		cy.get('[data-cy="gg-bg-bet-count"] [data-cy="buttons-group-item"]').eq(1).click();
		cy.get('[data-cy="gg-bg-game-type"] [data-cy="buttons-group-item"]').eq(nextDialogIndex).click();
		cy.get('[data-cy="gg-bg-game-subtype"] [data-cy="buttons-group-item"]').eq(1).click();

		cy.get('[data-cy="two-buttons-modal"]')
			.should('have.css', 'position', 'fixed')
			.and('have.css', 'top', '0px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '0px')
			.and('have.css', 'z-index', '90')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

		cy.get('[data-cy="modal-dialog-container"]')
			.should('have.css', 'max-width', '890px')
			.and('have.css', 'width', '890px')
			.and('have.css', 'padding', '32px')
			.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
			.and('have.css', 'background-color', 'rgb(255, 255, 255)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="modal-alt-title"]')
			.should('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '46px')
			.and('have.css', 'margin-bottom', '30px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0');

		cy.get('[data-cy="modal-alt-message"]')
			.should('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'margin-top', '0px')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0');

		cy.get('[data-cy="modal-alt-extra"]')
			.should('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'margin-top', '0px')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="modal-buttons-container"]')
			.should('have.css', 'flex-direction', 'row')
			.and('have.css', 'width', '826px')
			.and('have.css', 'margin-top', '30px')
			.and('have.css', 'display', 'flex');

		cy.get('[data-cy="modal-button"]')
			.should('have.css', 'height', '80px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="modal-button"]').eq(1).click();
	}

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(3).click();
		});
	});

	it('Проверка стилей страницы лотереи ГнГ при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '118px')
			.and('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="common-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'width', '640px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'margin-bottom', '25px');

		cy.get('[data-cy="lottery-container"]')
			.should('have.css', 'height', '930px')
			.and('have.css', 'padding', '20px 0px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'height', '890px')
			.and('have.css', 'grid-template', '99.5px 178.5px 267.5px 19.5px 167.5px 107.5px / 640px 600px / none')
			.and('have.css', 'grid-gap', '10px 10px')
			.and('have.css', 'gap', '10px')
			.and('have.css', 'margin-left', '320px');

		cy.get('[data-cy="logo-in-lottery"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'top', '35px');

		cy.get('[data-cy="goto-results"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '30px');

		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '88px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="rc-5"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-items', 'end');

		cy.get('[data-cy="draws-like"]')
			.should('have.css', 'margin-bottom', '0px');

		cy.get('[data-cy="draws-info"]')
			.should('have.css', 'background', 'rgb(227, 185, 11) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'padding', '5px 20px');

		cy.get('[data-cy="gg-bg-draws"]')
			.should('have.css', 'grid-row-start', '3')
			.and('have.css', 'grid-row-end', '6')
			.and('have.css', 'padding-top', '70px');

		cy.get('[data-cy="gg-bg-game-type"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="buttons-group-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '600px')
			.and('have.css', 'margin-bottom', '25px');

		cy.get('[data-cy="buttons-group-items"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'flex-flow', 'row wrap');
	});

	it('Проверка стилей диалоговых окон на странице лотереи ГнГ: тип игры А', () => {
		checkAttentionModal(0);
		cy.get('[data-cy="game-type"].game-type_a').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '85')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', '890px')
				.and('have.css', 'width', '890px')
				.and('have.css', 'padding', '32px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="modal-buttons-container"]')
				.should('have.css', 'width', '826px')
				.and('have.css', 'margin-top', '30px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'flex-direction', 'column');

			cy.get('[data-cy="button-modal-button"]')
				.should('have.css', 'font-size', '24px')
				.and('have.css', 'height', '54px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'width', '240px')
				.and('have.css', 'margin', '0px 293px');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'right', '15px')
				.and('have.css', 'top', '15px');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(90, 90, 90)');

			cy.get('[data-cy="game-type-title"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin-bottom', '10px');

			cy.get('[data-cy="game-type-text"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px');

			cy.get('[data-cy="gg-bg-race"]')
				.should('have.css', 'display', 'block');

			cy.get('[data-cy="buttons-group"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'align-items', 'center');

			cy.get('[data-cy="buttons-group-title"]')
				.should('have.css', 'font-weight', '400')
				.and('have.css', 'margin-right', '20px')
				.and('have.css', 'white-space', 'nowrap')
				.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'margin-bottom', '0px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'justify-content', 'start')
				.and('have.css', 'flex-flow', 'row wrap')
				.and('have.css', 'display', 'grid')
				.and('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'grid-row-gap', '30px')
				.and('have.css', 'row-gap', '30px')
				.and('have.css', 'justify-items', 'center')
				.and('have.css', 'justify-content', 'start');

			cy.get('[data-cy="buttons-group-item"]')
				.should('have.css', 'width', '54px')
				.and('have.css', 'height', '45px')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'border-width', '2px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin', '5px');

			cy.get('[data-cy="gg-race-button-item"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'margin', '20px 0px')
				.and('have.css', 'padding', '0px 25px');

			cy.get('[data-cy="modal-dialog-close"]').click();
		});
	});

	it('Проверка стилей диалоговых окон на странице лотереи ГнГ: тип игры Б', () => {
		checkAttentionModal(1);

		cy.get('[data-cy="game-type"].game-type_b').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '85')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', '890px')
				.and('have.css', 'width', '890px')
				.and('have.css', 'padding', '32px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="modal-buttons-container"]')
				.should('have.css', 'width', '826px')
				.and('have.css', 'margin-top', '30px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'flex-direction', 'column');

			cy.get('[data-cy="button-modal-button"]')
				.should('have.css', 'font-size', '24px')
				.and('have.css', 'height', '54px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'width', '240px')
				.and('have.css', 'margin', '0px 293px');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'right', '15px')
				.and('have.css', 'top', '15px');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(90, 90, 90)');

			cy.get('[data-cy="game-type-title"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin-bottom', '10px');

			cy.get('[data-cy="game-type-text"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px');

			cy.get('[data-cy="gg-bg-race"]')
				.should('have.css', 'display', 'block');

			cy.get('[data-cy="buttons-group"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'align-items', 'center');

			cy.get('[data-cy="buttons-group-title"]')
				.should('have.css', 'font-weight', '400')
				.and('have.css', 'margin-right', '20px')
				.and('have.css', 'white-space', 'nowrap')
				.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'margin-bottom', '0px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'justify-content', 'start')
				.and('have.css', 'flex-flow', 'row wrap')
				.and('have.css', 'display', 'grid')
				.and('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'grid-row-gap', '30px')
				.and('have.css', 'row-gap', '30px')
				.and('have.css', 'justify-items', 'center')
				.and('have.css', 'justify-content', 'start');

			cy.get('[data-cy="buttons-group-item"]')
				.should('have.css', 'width', '54px')
				.and('have.css', 'height', '45px')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'border-width', '2px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin', '5px');

			cy.get('[data-cy="gg-race-button-item"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'margin', '20px 0px')
				.and('have.css', 'padding', '0px 25px');

		});
	});

	it('Проверка стилей диалоговых окон на странице лотереи ГнГ: тип игры В', () => {
		checkAttentionModal(2);

		cy.get('[data-cy="game-type"].game-type_v').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '85')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', '890px')
				.and('have.css', 'width', '890px')
				.and('have.css', 'padding', '32px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="modal-buttons-container"]')
				.should('have.css', 'width', '826px')
				.and('have.css', 'margin-top', '30px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'flex-direction', 'column');

			cy.get('[data-cy="button-modal-button"]')
				.should('have.css', 'font-size', '24px')
				.and('have.css', 'height', '54px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'width', '240px')
				.and('have.css', 'margin', '0px 293px');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'right', '15px')
				.and('have.css', 'top', '15px');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(90, 90, 90)');

			cy.get('[data-cy="game-type-title"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin-bottom', '10px');

			cy.get('[data-cy="game-type-text"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px');

			cy.get('[data-cy="gg-bg-race"]')
				.should('have.css', 'display', 'block');

			cy.get('[data-cy="buttons-group"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'align-items', 'center');

			cy.get('[data-cy="buttons-group-title"]')
				.should('have.css', 'font-weight', '400')
				.and('have.css', 'margin-right', '20px')
				.and('have.css', 'white-space', 'nowrap')
				.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'margin-bottom', '0px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'justify-content', 'start')
				.and('have.css', 'flex-flow', 'row wrap')
				.and('have.css', 'display', 'grid')
				.and('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'grid-row-gap', '30px')
				.and('have.css', 'row-gap', '30px')
				.and('have.css', 'justify-items', 'center')
				.and('have.css', 'justify-content', 'start');

			cy.get('[data-cy="buttons-group-item"]')
				.should('have.css', 'width', '54px')
				.and('have.css', 'height', '45px')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'border-width', '2px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin', '5px');

			cy.get('[data-cy="gg-race-button-item"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'margin', '20px 0px')
				.and('have.css', 'padding', '0px 25px');

		});
	});

	it('Проверка стилей диалоговых окон на странице лотереи ГнГ: тип игры Г', () => {
		checkAttentionModal(3);

		cy.get('[data-cy="game-type"].game-type_g').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '85')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', '890px')
				.and('have.css', 'width', '890px')
				.and('have.css', 'padding', '32px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="modal-buttons-container"]')
				.should('have.css', 'width', '826px')
				.and('have.css', 'margin-top', '30px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'flex-direction', 'column');

			cy.get('[data-cy="button-modal-button"]')
				.should('have.css', 'font-size', '24px')
				.and('have.css', 'height', '54px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'width', '240px')
				.and('have.css', 'margin', '0px 293px');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'right', '15px')
				.and('have.css', 'top', '15px');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(90, 90, 90)');

			cy.get('[data-cy="game-type-title"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin-bottom', '10px');

			cy.get('[data-cy="game-type-text"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px');

			cy.get('[data-cy="gg-bg-race"]')
				.should('have.css', 'display', 'block');

			cy.get('[data-cy="buttons-group"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'align-items', 'center');

			cy.get('[data-cy="buttons-group-title"]')
				.should('have.css', 'font-weight', '400')
				.and('have.css', 'margin-right', '20px')
				.and('have.css', 'white-space', 'nowrap')
				.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'margin-bottom', '0px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'justify-content', 'start')
				.and('have.css', 'flex-flow', 'row wrap')
				.and('have.css', 'display', 'grid')
				.and('have.css', 'grid-template-columns', '64.5469px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px 64.5469px 64.5469px 64.5625px')
				.and('have.css', 'grid-row-gap', '30px')
				.and('have.css', 'row-gap', '30px')
				.and('have.css', 'justify-items', 'center')
				.and('have.css', 'justify-content', 'start');

			cy.get('[data-cy="buttons-group-item"]')
				.should('have.css', 'width', '54px')
				.and('have.css', 'height', '45px')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'border-width', '0px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin', '5px');

			cy.get('[data-cy="gg-race-button-item"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'margin', '20px 0px')
				.and('have.css', 'padding', '0px 25px');

		});
	});

	it('Проверка стилей диалоговых окон на странице лотереи ГнГ: тип игры Д', () => {
		checkAttentionModal(4);

		cy.get('[data-cy="game-type"].game-type_d').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '85')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', '890px')
				.and('have.css', 'width', '890px')
				.and('have.css', 'padding', '32px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="modal-buttons-container"]')
				.should('have.css', 'width', '826px')
				.and('have.css', 'margin-top', '30px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'flex-direction', 'column');

			cy.get('[data-cy="button-modal-button"]')
				.should('have.css', 'font-size', '24px')
				.and('have.css', 'height', '54px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'width', '240px')
				.and('have.css', 'margin', '0px 293px');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'right', '15px')
				.and('have.css', 'top', '15px');

			cy.get('[data-cy="cross-image"]')
				.should('have.css', 'width', '24px')
				.and('have.css', 'height', '24px')
				.and('have.css', 'display', 'block');

			cy.get('[data-cy="cross-path"]')
				.should('have.css', 'fill', 'rgb(90, 90, 90)');

			cy.get('[data-cy="game-type-title"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'font-weight', '400')
				.and('have.css', 'margin-bottom', '10px');

			cy.get('[data-cy="game-type-text"]')
				.should('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '18px');

			cy.get('[data-cy="gg-bg-race"]')
				.should('have.css', 'display', 'block');

			cy.get('[data-cy="buttons-group"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'align-items', 'center');

			cy.get('[data-cy="buttons-group-title"]')
				.should('have.css', 'font-weight', '400')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'white-space', 'normal')
				.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '24px')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'margin-bottom', '0px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'grid-template-columns', '66px 66px 66px 66px')
				.and('have.css', 'justify-content', 'end')
				.and('have.css', 'flex-flow', 'row wrap')
				.and('have.css', 'display', 'grid')
				.and('have.css', 'grid-row-gap', '30px')
				.and('have.css', 'row-gap', '30px')
				.and('have.css', 'justify-items', 'center');

			cy.get('[data-cy="buttons-group-item"]')
				.should('have.css', 'width', '66px')
				.and('have.css', 'height', '66px')
				.and('have.css', 'font-size', '30px')
				.and('have.css', 'border-radius', '50%')
				.and('have.css', 'border-width', '2px')
				.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-weight', '400');

			cy.get('[data-cy="gg-race-button-item"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'margin', '20px 0px')
				.and('have.css', 'padding', '0px 25px');

		});
	});
});
