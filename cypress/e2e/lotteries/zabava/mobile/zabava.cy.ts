describe('Проверка лотереи Лото-Забава при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(0).click();
		});
	});

	function checkCommonElements() {
		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '6px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '32px')
			.and('have.css', 'max-height', '40px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '12px');

		cy.get('[data-cy="lottery-container"]')
			.should('have.css', 'height', '829px')
			.and('have.css', 'padding', '8px 8px 10px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="mega-items"]')
			.should('have.css', 'text-align', 'right')
			.and('have.css', 'font-size', '11px');

		cy.get('[data-cy="mega-item"]')
			.should('have.css', 'margin', '5px 0px');

		cy.get('[data-cy="mega-item-lbl"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '11px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="mega-item-val"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '11px')
			.and('have.css', 'color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="goto-results"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '32px')
			.and('have.css', 'font-size', '11px');

		cy.get('[data-cy="logo-in-lottery"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '8px')
			.and('have.css', 'top', '8px');
	}

	it('Проверка стилей страницы лотереи Лото-Забава при мобильном разрешении (393 x 873 px)', () => {

		checkCommonElements();

		cy.get('[data-cy="common-title"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '15px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'width', '385px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'margin-bottom', '8px');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '32px 35px 130px 614px / 377px / "a1" "a2" "a3" "a4"')
			.and('have.css', 'grid-gap', '0px 10px')
			.and('have.css', 'gap', '0px 10px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'height', '811px');

		cy.get('[data-cy="rb-cell"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '558px 44px')
			.and('have.css', 'align-content', 'space-between')
			.and('have.css', 'grid-row-gap', '12px')
			.and('have.css', 'row-gap', '12px');



		cy.get('[data-cy="bet-settings"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '231.328px 193.328px 113.344px')
			.and('have.css', 'grid-row-gap', '10px')
			.and('have.css', 'row-gap', '10px');









		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'height', '44px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="zc-summary"]')
			.should('have.css', 'font-size', '30px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'margin-bottom', '10px');

		cy.get('[data-cy="zc-summary-value"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="draws-buttons"]')
			.should('have.css', 'margin-bottom', '0px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '32px 35px 130px 614px / 377px / "a1" "a2" "a3" "a4"')
			.and('have.css', 'grid-gap', '0px 10px')
			.and('have.css', 'gap', '0px 10px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'height', '811px');

		cy.get('[data-cy="bet-settings"]')
			.should('have.css', 'grid-row-gap', '10px')
			.and('have.css', 'row-gap', '10px');

		cy.get('[data-cy="dbc-draw"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '272px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '45px')
			.and('have.css', 'border', '1px solid rgb(227, 185, 11)');

		cy.get('[data-cy="dbc-number"]')
			.should('have.css', 'font-weight', '400')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'margin-left', '5px')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="dbc-data"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'text-align', 'left');

		cy.get('[data-cy="dbc-time-left"]')
			.should('have.css', 'text-transform', 'none')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'margin-top', '2px');

		cy.get('[data-cy="dbc-date"]')
			.should('have.css', 'font-size', '15px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="buttons-group-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '15px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '377px')
			.and('have.css', 'margin-bottom', '8px');

		cy.get('[data-cy="buttons-group-items"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'flex-flow', 'row wrap');

		cy.get('[data-cy="buttons-group-item"]')
			.should('have.css', 'width', '48px')
			.and('have.css', 'height', '42px')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'border', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-feature-settings', '"lnum"')
			.and('have.css', 'font-variant', 'lining-nums')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="button"]')
			.should('have.css', 'width', '377px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'padding-left', '10px')
			.and('have.css', 'padding-right', '10px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'cursor', 'pointer');
	});

	it('Проверка стилей страницы проверки бланков при мобильном разрешении (393 x 873 px)', () => {

		cy.get('[data-cy="scan-blanks-button"]').click();

		checkCommonElements();

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '32px 35px 744px / 367px 0px / "a1" "a2" "a3"')
			.and('have.css', 'grid-gap', '0px 10px')
			.and('have.css', 'gap', '0px 10px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'height', '811px');

		cy.get('[data-cy="cross-image"]')
			.should('have.css', 'width', '100%')
			.and('have.css', 'height', '100%')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="cross-path"]')
			.should('have.css', 'fill', 'rgb(0, 151, 136)');

		cy.get('[data-cy="top-panel"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="top-panel-left"]')
			.should('have.css', 'display', 'flex');

		cy.get('[data-cy="top-panel-right"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'flex-flow', 'row wrap')
			.and('have.css', 'grid-template-columns', '145.641px 143.625px')
			.and('have.css', 'grid-column-gap', '10px')
			.and('have.css', 'column-gap', '10px');

		cy.get('[data-cy="codes-input-wrapper"]')
			.should('have.css', 'flex-grow', '1')
			.and('have.css', 'margin-right', '10px')
			.and('have.css', 'width', '332px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="codes-input"]')
			.should('have.css', 'width', '332px')
			.and('have.css', 'height', '35px');

		cy.get('[data-cy="input-field"]')
			.should('have.css', 'border-top', '0px none rgb(102, 102, 102)')
			.and('have.css', 'border-left', '0px none rgb(102, 102, 102)')
			.and('have.css', 'border-right', '0px none rgb(102, 102, 102)')
			.and('have.css', 'border-radius', '0px');

		cy.get('[data-cy="codes-input"] .input-label')
			.should('have.css', 'height', '35px');

		cy.get('[data-cy="switch-camera"]')
			.should('have.css', 'width', '35px')
			.and('have.css', 'border-radius', '5px');

		cy.get('[data-cy="switch-camera-pic"]')
			.should('have.css', 'display', 'block');

		cy.get('[data-cy="switch-camera"] path')
			.should('have.css', 'fill', 'rgb(255, 255, 255)');

		cy.get('[data-cy="zc-summary"]')
			.should('have.css', 'font-size', '11px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'white-space', 'nowrap');

		cy.get('[data-cy="zc-summary-value"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="main-area"]')
			.should('have.css', 'position', 'relative');

		cy.get('[data-cy="main-area-inner"]')
			.should('have.css', 'border', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'overflow-y', 'auto')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '630px');

		cy.get('[data-cy="bc-list"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'padding', '5px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'top', '0px')
			.and('have.css', 'width', '375px')
			.and('have.css', 'height', '628px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '14px 599px');

		cy.get('[data-cy="bc-list-header"]')
			.should('have.css', 'padding-right', '14px');

		cy.get('[data-cy="bc-list-cell-actions"]')
			.should('have.css', 'width', '16px');

		cy.get('[data-cy="bottom-panel"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '32px / 80px 80px 201px / none')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'align-content', 'center')
			.and('have.css', 'grid-column-gap', '8px');

		cy.get('[data-cy="back-button"], [data-cy="clear-button"], [data-cy="register-bet-button"]')
			.should('have.css', 'width', '80px')
			.and('have.css', 'height', '32px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="back-button"], [data-cy="clear-button"]')
			.should('have.css', 'justify-self', 'start')
			.and('have.css', 'padding-left', '5px')
			.and('have.css', 'padding-right', '5px')
			.and('have.css', 'width', '80px');

		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'justify-self', 'end');

		cy.get('[data-cy="register-bet-button"] .button')
			.should('have.css', 'padding-left', '10px')
			.and('have.css', 'padding-right', '10px');

		cy.get('[data-cy="camera-block"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'bottom', '8px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'max-height', '200px')
			.and('have.css', 'max-width', '480px')
			.and('have.css', 'left', '188.5px')
			.and('have.css', 'transform', 'matrix(1, 0, 0, 1, -180.5, 0)')
			.and('have.css', 'height', '200px')
			.and('have.css', 'width', '361px');

		cy.get('[data-cy="camera-spoiler"]')
			.should('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="input-label"]')
			.should('have.css', 'width', '332px');

		cy.get('[data-cy="input-title"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'padding', '5px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'top', '3px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="input-field"]')
			.should('have.css', 'width', '332px')
			.and('have.css', 'height', '35px')
			.and('have.css', 'padding', '4px')
			.and('have.css', 'color', 'rgb(102, 102, 102)')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '0px');

		cy.get('[data-cy="spoiler-header"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'padding', '5px 30px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '0px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="close-button"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'top', '8px')
			.and('have.css', 'right', '14px')
			.and('have.css', 'width', '12px')
			.and('have.css', 'height', '12px');

		cy.get('[data-cy="spoiler-body"]')
			.should('have.css', 'overflow', 'auto');

		cy.get('[data-cy="scanner-block"]')
			.should('have.css', 'height', '163px');

		cy.get('[data-cy="scanner-component"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '163px');

		cy.get('[data-cy="outlet-wrapper"]')
			.should('have.css', 'border-radius', '0px')
			.and('have.css', 'overflow', 'visible')
			.and('have.css', 'height', '744px');

	});
});
