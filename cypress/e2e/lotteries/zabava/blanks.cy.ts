describe('Проверка бага с дублированием при сканировании нативным сканером', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('8200035').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('https://test.alt.emict.net/');
			cy.wait(2000);
		});
	});

	it('Проверка бага с дублированием при сканировании нативным сканером', () => {
		cy.get('[data-cy="game-list-item"]').eq(0).click();
		cy.get('[data-cy="scan-blanks-button"]').click();

		cy.get('[data-cy="input-field"]').type('https://msl.ua/qr/433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('https://msl.ua/qr/433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('https://msl.ua/qr/433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('https://msl.ua/qr/433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('433000030557473940\n');

		cy.get('[data-cy="input-field"]').type('433000030557473940\n');

	});

});
