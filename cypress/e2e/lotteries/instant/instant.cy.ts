describe('Проверка ЭМЛ при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(2).click();
		});
	});

	it('Проверка стилей страницы ЭМЛ при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="button"]')
			.should('have.css', 'width', '600px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'normal');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '116px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="columns"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '880px / 1215px 600px / none')
			.and('have.css', 'height', '880px')
			.and('have.css', 'grid-column-gap', '45px')
			.and('have.css', 'column-gap', '45px');

		cy.get('[data-cy="left-column"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '190px 650px')
			.and('have.css', 'grid-row-gap', '40px')
			.and('have.css', 'row-gap', '40px')
			.and('have.css', 'height', '880px');

		cy.get('[data-cy="left-column-top"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'flex-flow', 'row wrap')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'width', '1215px');

		cy.get('[data-cy="right-column"]')
			.should('have.css', 'padding-top', '42px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'align-content', 'space-between')
			.and('have.css', 'grid-template-rows', '338px 420px 80px');

		cy.get('[data-cy="il-header-wrapper"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'flex-end')
			.and('have.css', 'justify-content', 'space-between');

		cy.get('[data-cy="il-bg-filters"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'flex-end')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '33px');

		cy.get('[data-cy="il-draws-column"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '346px 64px')
			.and('have.css', 'margin-bottom', '10px');

		cy.get('[data-cy="il-draws-column-sel-game"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'margin-top', '80px')
			.and('have.css', 'margin-bottom', '180px')
			.and('have.css', 'padding', '0px 10px')
			.and('have.css', 'animation', '2.2s cubic-bezier(0, 0, 0.35, 0.47) 0s infinite normal none running blinker-inv')
			.and('have.css', 'opacity', '0');

		cy.get('[data-cy="instant-lotteries-container"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'left', '30px')
			.and('have.css', 'right', '30px')
			.and('have.css', 'bottom', '20px')
			.and('have.css', 'top', '100px');

		cy.get('[data-cy="il-bg-tickets"]')
			.should('have.css', 'margin-bottom', '80px');

		cy.get('[data-cy="il-bg-tickets"] [data-cy="buttons-group-items"]')
			.should('have.css', 'grid-gap', '30px 30px')
			.and('have.css', 'gap', '30px');

		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'width', '600px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="filters-panel"]')
			.should('have.css', 'width', '1215px');

		cy.get('[data-cy="igc-games-content"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-columns', '220.391px 220.406px 220.391px 220.406px 220.406px')
			.and('have.css', 'grid-gap', '20px 20px')
			.and('have.css', 'gap', '20px')
			.and('have.css', 'width', '1182px')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="igc-game-price"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'right', '170.391px')
			.and('have.css', 'top', '140px');
	});

	it('Проверка №2 стилей страницы ЭМЛ при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="igc-game-item-content"]').eq(0).click();
		cy.get('[data-cy="il-bg-tickets"] [data-cy="buttons-group-item"]').eq(0).click();

		cy.get('[data-cy="draws-item"]')
			.should('have.css', 'padding', '10px 12px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'cursor', 'pointer');

		cy.get('[data-cy="draws-item-serie"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'pointer-events', 'none')
			.and('have.css', 'margin-bottom', '10px');

		cy.get('[data-cy="max-win"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'margin', '5px 0px')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="max-win-lbl"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="max-win-val"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

	});
});
