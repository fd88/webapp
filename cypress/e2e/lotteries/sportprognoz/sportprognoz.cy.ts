describe('Проверка лотереи Спортпрогноз при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(5).click();
		});
	});

	it('Проверка стилей страницы лотереи Спортпрогноз при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '138px')
			.and('have.css', 'max-height', '80px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="lottery-container"]')
			.should('have.css', 'height', '930px')
			.and('have.css', 'padding', '20px 0px 30px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'left', '0px')
			.and('have.css', 'top', '0px');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '66px 784px / 468px 1192px / none')
			.and('have.css', 'grid-gap', '30px 10px')
			.and('have.css', 'gap', '30px 10px')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'margin-left', '190px')
			.and('have.css', 'height', '880px');

		cy.get('[data-cy="spc-goto-results"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '66px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(250, 239, 196) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'border', '2px solid rgb(227, 185, 11)')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'width', '468px')
			.and('have.css', 'justify-self', 'end');

		cy.get('[data-cy="spc-temp-text"]')
			.should('have.css', 'animation', '2.2s cubic-bezier(0, 0, 0.35, 0.47) 0s infinite normal none running blinker')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-self', 'center')
			.and('have.css', 'margin-top', '130px');

		cy.get('[data-cy="spc-dp"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'text-align', 'right')
			.and('have.css', 'width', '468px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'padding-top', '8px');

		cy.get('[data-cy="spc-dp-lbl"]')
			.should('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="spc-dp-val"]')
			.should('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="spc-show-program"]')
			.should('have.css', 'width', '468px')
			.and('have.css', 'height', '70px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'line-height', '68px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="spc-show-program-drawnum"]')
			.should('have.css', 'font-size', '36px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="spc-nearest-draw"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'margin-bottom', '10px')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="spc-draws"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '30px');

		cy.get('[data-cy="dbc-number"]')
			.should('have.css', 'width', '100px')
			.and('have.css', 'text-align', 'left')
			.and('have.css', 'font-weight', '700')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '10px');

		cy.get('[data-cy="dbc-data"]')
			.should('have.css', 'flex-grow', '1')
			.and('have.css', 'width', '344px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'width', '344px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'text-align', 'left');

		cy.get('[data-cy="dbc-draw"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '468px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '45px')
			.and('have.css', 'border', '2px solid rgb(227, 185, 11)');

		cy.get('[data-cy="dbc-time-left"]')
			.should('have.css', 'text-transform', 'none')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'margin-top', '2px');

		cy.get('[data-cy="dbc-date"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400');
	});
});
