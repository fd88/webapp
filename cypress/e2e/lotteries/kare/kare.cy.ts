describe('Проверка лотереи Каре при десктопном разрешении (1920px)', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(6).click();
		});
	});

	xit('Проверка стилей страницы лотереи Каре при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'width', '90px')
			.and('have.css', 'max-height', 'none');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '36px');

		cy.get('[data-cy="buttons-group-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '540px')
			.and('have.css', 'margin-bottom', '25px');

		cy.get('[data-cy="buttons-group-items"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'flex-flow', 'row wrap');

		cy.get('[data-cy="common-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'width', '540px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'margin-bottom', '25px');

		cy.get('[data-cy="lottery-container"]')
			.should('have.css', 'height', '930px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'padding', '20px 0px');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'height', '890px')
			.and('have.css', 'grid-template-areas', '"left rt" "left rm" "left rb"')
			.and('have.css', 'grid-gap', '24px 10px')
			.and('have.css', 'gap', '24px 10px')
			.and('have.css', 'margin-left', '360px');

		cy.get('[data-cy="rb-cell"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'align-content', 'space-between')
			.and('have.css', 'grid-row-gap', '30px')
			.and('have.css', 'row-gap', '30px');

		cy.get('[data-cy="logo-in-lottery"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'left', '0px')
			.and('have.css', 'top', '25px');

		cy.get('[data-cy="goto-results"]')
			.should('have.css', 'height', '80px')
			.and('have.css', 'font-size', '30px');

		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'height', '88px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="left-cell"]')
			.should('have.css', 'grid-area', '1 / left / 4 / left')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '243px 338px 0px 0px')
			.and('have.css', 'grid-row-start', '1')
			.and('have.css', 'grid-row-end', '4')
			.and('have.css', 'align-content', 'center');

		cy.get('[data-cy="kare-bets-list"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '469px');

		cy.get('[data-cy="kc-draws"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '95px');

		cy.get('[data-cy="kc-add-bet-panel"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'justify-content', 'space-around');

		cy.get('[data-cy="kc-table"]')
			.should('have.css', 'margin-bottom', '10px')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'width', '600px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)');

		cy.get('[data-cy="top-panel"]')
			.should('have.css', 'position', 'fixed')
			.and('have.css', 'top', '44px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'display', 'none')
			.and('have.css', 'grid-template-columns', 'auto')
			.and('have.css', 'justify-content', 'end')
			.and('have.css', 'justify-items', 'end')
			.and('have.css', 'padding', '8px')
			.and('have.css', 'z-index', '5')
			.and('have.css', 'box-shadow', 'rgb(161, 161, 161) 0px 0px 8px 0px');

		cy.get('[data-cy="register-bet-button"]').should('have.css', 'height', '88px');

		cy.get('[data-cy="dbc-draw"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '540px')
			.and('have.css', 'height', '80px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '45px')
			.and('have.css', 'border', '2px solid rgb(227, 185, 11)');

		cy.get('[data-cy="dbc-number"]')
			.should('have.css', 'font-weight', '700')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '10px');

		cy.get('[data-cy="dbc-data"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'width', '516px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'text-align', 'left');

		cy.get('[data-cy="dbc-time-left"]')
			.should('have.css', 'text-transform', 'none')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'margin-top', '2px');

		cy.get('[data-cy="dbc-date"]')
			.should('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="kc-register-msg"]')
			.should('have.css', 'height', '348px')
			.and('have.css', 'padding-top', '50px')
			.and('have.css', 'padding-left', '3px')
			.and('have.css', 'padding-right', '3px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'animation', '2.2s cubic-bezier(0, 0, 0.35, 0.47) 0s infinite normal none running blinker-inv');

		cy.get('[data-cy="bets-list"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '348px 0px 121px');
	});

	xit('Проверка диалога выбора карт в Каре (Стол 1) при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="kc-table"]').first().click();

		cy.get('[data-cy="cards-row-btn"]').eq(0).click();
		cy.get('[data-cy="cards-row-btn"]').eq(5).click();
		cy.get('[data-cy="cards-row-btn"]').eq(10).click();
		cy.get('[data-cy="cards-row-btn"]').eq(15).click();
		// cy.get('[data-cy="cards-row-btn"]').eq(18).click();
		cy.get('[data-cy="kc-bets-buttons"] [data-cy="buttons-group-item"]').first().click();

		cy.get('[data-cy="kare-edit-bet-dialog-container"]')
			.should('have.css', 'position', 'fixed')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'top', '0px')
			.and('have.css', 'bottom', '0px')
			.and('have.css', 'z-index', '19');

		cy.get('[data-cy="msl-dialog-overlay"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'top', '0px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '0px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0.24) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="keb-dialog"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'top', '24px')
			.and('have.css', 'left', '24px')
			.and('have.css', 'right', '24px')
			.and('have.css', 'bottom', '24px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '778px 80px')
			.and('have.css', 'padding', '40px')
			.and('have.css', 'grid-row-gap', '10px')
			.and('have.css', 'row-gap', '10px');

		cy.get('[data-cy="close-button"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'right', '40px')
			.and('have.css', 'top', '40px')
			.and('have.css', 'width', '32px')
			.and('have.css', 'height', '32px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-right', '0px')
			.and('have.css', 'margin-left', '0px');

		cy.get('[data-cy="keb-dialog-wrp"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'space-between');

		cy.get('[data-cy="keb-dialog-left"]')
			.should('have.css', 'width', '600px')
			.and('have.css', 'margin-left', '100px');

		cy.get('[data-cy="keb-dialog-bet-type"]')
			.should('have.css', 'width', '580px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="cards-table-row"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="cards-row-cell"]')
			.should('have.css', 'color', 'rgb(128, 128, 128)')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'margin', '5px 50px 5px 5px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '30px');

		cy.get('[data-cy="card-icon"]')
			.should('have.css', 'background-repeat', 'no-repeat')
			.and('have.css', 'background-size', 'contain')
			.and('have.css', 'background-position', '50% 50%')
			.and('have.css', 'display', 'block')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px');

		cy.get('[data-cy="cards-row-btn"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'width', '66px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'background', 'rgb(227, 185, 11) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '1px solid rgb(227, 185, 11)')
			.and('have.css', 'border-radius', '4px');

		cy.get('[data-cy="keb-dialog-selected-cards"]')
			.should('have.css', 'margin-bottom', '20px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="table-block-content"]')
			.should('have.css', 'position', 'relative')
			.and('have.css', 'padding', '15px')
			.and('have.css', 'min-height', '150px')
			.and('have.css', 'max-height', '150px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'box-shadow', 'rgb(128, 128, 128) 0px 0px 0px 1px inset')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="table-block-subtitle"]')
			.should('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="ksco-cards"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-evenly')
			.and('have.css', 'padding', '0px 15px');

		cy.get('[data-cy="kcwl-icon"]')
			.should('have.css', 'background-repeat', 'no-repeat')
			.and('have.css', 'background-size', 'contain')
			.and('have.css', 'background-position', '50% 50%')
			.and('have.css', 'display', 'block')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'margin', '0px auto');

		cy.get('[data-cy="keb-dialog-right"]')
			.should('have.css', 'max-width', '580px')
			.and('have.css', 'width', '580px')
			.and('have.css', 'margin', '0px 254px');

		cy.get('[data-cy="table-header"]')
			.should('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-top', '7px')
			.and('have.css', 'margin-bottom', '37px');

		cy.get('[data-cy="bet-sum-header"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'width', '580px')
			.and('have.css', 'margin-bottom', '10px')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="bet-amount"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="bets-sums"]')
			.should('have.css', 'margin-bottom', '60px');

		cy.get('[data-cy="kc-bets-buttons"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '30px');

		cy.get('[data-cy="custom-bet"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'width', '580px');

		cy.get('[data-cy="input-label"]')
			.should('have.css', 'width', '580px');

		cy.get('[data-cy="input-field"]')
			.should('have.css', 'width', '580px')
			.and('have.css', 'height', '54px')
			.and('have.css', 'padding', '4px')
			.and('have.css', 'color', 'rgb(102, 102, 102)')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '1px solid rgb(153, 153, 153)')
			.and('have.css', 'border-radius', '5px');

		cy.get('[data-cy="kc-clear-button"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '40px')
			.and('have.css', 'width', '580px')
			.and('have.css', 'height', '70px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="kc-confirm-button"]')
			.should('have.css', 'display', 'none')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '0px none rgb(255, 255, 255)');

		cy.get('[data-cy="dialog-continue"]')
			.should('have.css', 'width', '820px')
			.and('have.css', 'max-width', '820px')
			.and('have.css', 'margin', '0px 484px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px');
	});

	xit('Проверка диалога выбора комбинаций в Каре (Стол 2) при десктопном разрешении (1920px)', () => {
		cy.get('[data-cy="kc-table"]').last().click();

		cy.get('[data-cy="cards-row-btn"]').eq(0).click();
		cy.get('[data-cy="cards-row-btn"]').eq(1).click();
		cy.get('[data-cy="cards-row-btn"]').eq(2).click();
		cy.get('[data-cy="cards-row-btn"]').eq(3).click();
		cy.get('[data-cy="cards-row-btn"]').eq(4).click();

		cy.get('[data-cy="kc-bets-buttons"] [data-cy="buttons-group-item"]').eq(0).click();

		cy.get('[data-cy="kare-edit-bet-dialog-container"]')
			.should('have.css', 'position', 'fixed')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'top', '0px')
			.and('have.css', 'bottom', '0px')
			.and('have.css', 'z-index', '19');

		cy.get('[data-cy="msl-dialog-overlay"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'top', '0px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '0px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0.24) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="keb-dialog"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'top', '24px')
			.and('have.css', 'left', '24px')
			.and('have.css', 'right', '24px')
			.and('have.css', 'bottom', '24px')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '778px 80px')
			.and('have.css', 'padding', '40px')
			.and('have.css', 'grid-row-gap', '10px')
			.and('have.css', 'row-gap', '10px');

		cy.get('[data-cy="close-button"]')
			.should('have.css', 'position', 'absolute')
			.and('have.css', 'right', '40px')
			.and('have.css', 'top', '40px')
			.and('have.css', 'width', '32px')
			.and('have.css', 'height', '32px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-right', '0px')
			.and('have.css', 'margin-left', '0px');

		cy.get('[data-cy="keb-dialog-wrp"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'space-between');

		cy.get('[data-cy="keb-dialog-left"]')
			.should('have.css', 'width', '600px')
			.and('have.css', 'margin-left', '100px');

		cy.get('[data-cy="keb-dialog-bet-type"]')
			.should('have.css', 'display', 'block');

		cy.get('[data-cy="cards-table-row"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="cards-row-cell"]')
			.should('have.css', 'width', '250px')
			.and('have.css', 'margin-right', '50px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="input-label"]')
			.should('have.css', 'width', '580px');

		cy.get('[data-cy="input-field"]')
			.should('have.css', 'width', '580px')
			.and('have.css', 'height', '54px')
			.and('have.css', 'padding', '4px')
			.and('have.css', 'color', 'rgb(102, 102, 102)')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '1px solid rgb(153, 153, 153)')
			.and('have.css', 'border-radius', '5px');

		cy.get('[data-cy="custom-bet"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'width', '580px');

		cy.get('[data-cy="keb-dialog-selected-cards"]')
			.should('have.css', 'margin-bottom', '20px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="table-block-content"]')
			.should('have.css', 'padding', '15px')
			.and('have.css', 'min-height', '150px')
			.and('have.css', 'max-height', '150px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'box-shadow', 'rgb(128, 128, 128) 0px 0px 0px 1px inset')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px');

		cy.get('[data-cy="table-block-subtitle"]')
			.should('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="ksco-cards"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'line-height', '26px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-evenly')
			.and('have.css', 'padding', '0px 15px');

		cy.get('[data-cy="kc-clear-button"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '40px')
			.and('have.css', 'width', '580px')
			.and('have.css', 'height', '70px')
			.and('have.css', 'font-size', '36px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="kc-confirm-button"]')
			.should('have.css', 'display', 'none')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '0px none rgb(255, 255, 255)');

		cy.get('[data-cy="dialog-continue"]')
			.should('have.css', 'width', '820px')
			.and('have.css', 'max-width', '820px')
			.and('have.css', 'margin', '0px 484px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'height', '80px')
			.and('have.css', 'font-size', '36px');
	});

	it('Проверка списка ставок и элементов в нем', () => {
		cy.get('[data-cy="kc-table"]').first().click();

		cy.get('[data-cy="cards-row-btn"]').eq(0).click();
		cy.get('[data-cy="cards-row-btn"]').eq(5).click();
		cy.get('[data-cy="cards-row-btn"]').eq(10).click();
		cy.get('[data-cy="cards-row-btn"]').eq(15).click();
		// cy.get('[data-cy="cards-row-btn"]').eq(18).click();
		cy.get('[data-cy="kc-bets-buttons"] [data-cy="buttons-group-item"]').first().click();

		cy.get('[data-cy="dialog-continue"]').click();

		cy.get('[data-cy="kc-table"]').last().click();

		cy.get('[data-cy="cards-row-btn"]').eq(0).click();
		cy.get('[data-cy="cards-row-btn"]').eq(1).click();
		cy.get('[data-cy="cards-row-btn"]').eq(2).click();
		cy.get('[data-cy="cards-row-btn"]').eq(3).click();
		cy.get('[data-cy="cards-row-btn"]').eq(4).click();

		cy.get('[data-cy="kc-bets-buttons"] [data-cy="buttons-group-item"]').first().click();

		cy.get('[data-cy="dialog-continue"]').click();

		cy.get('[data-cy="kc-bets"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '417px')
			.and('have.css', 'overflow-y', 'scroll');

		cy.get('[data-cy="kc-bet"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'row nowrap')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="kc-bet-button"]')
			.should('have.css', 'width', '540px')
			.and('have.css', 'min-height', '60px')
			.and('have.css', 'margin-right', '8px')
			.and('have.css', 'padding', '5px')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="kc-bet-delete"]')
			.should('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'font-size', '30px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'background-color', 'rgb(223, 80, 80)')
			.and('have.css', 'border-radius', '50%')
			.and('have.css', 'cursor', 'pointer');

		cy.get('[data-cy="kare-bet-list-item-one-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="cards-with-labels"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'space-around')
			.and('have.css', 'width', '408px');

		cy.get('[data-cy="bet-price"]')
			.should('have.css', 'width', '118px')
			.and('have.css', 'padding-right', '5px')
			.and('have.css', 'border-left', '1px solid rgb(161, 161, 161)')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'right')
			.and('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="kare-card-with-label-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'align-items', 'center');

		cy.get('[data-cy="kcwl-label"]')
			.should('have.css', 'margin-bottom', '0px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="kcwl-icon"]')
			.should('have.css', 'background-repeat', 'no-repeat')
			.and('have.css', 'background-size', 'contain')
			.and('have.css', 'background-position', '50% 50%')
			.and('have.css', 'display', 'block')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'margin', '0px');
	});
});
