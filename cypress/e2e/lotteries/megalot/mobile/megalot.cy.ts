describe('Проверка лотереи Мегалот при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(1).click();
		});
	});

	it('Проверка стилей страницы лотереи Мегалот при мобильном разрешении (393 x 873 px)', () => {
		cy.get('[data-cy="game-logo"]')
			.should('have.css', 'display', 'flex');

		cy.get('[data-cy="game-logo-left"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '12px');

		cy.get('[data-cy="game-logo-image"]')
			.should('have.css', 'max-height', '25px');

		cy.get('[data-cy="game-logo-title"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '18px')

		cy.get('[data-cy="common-title"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '15px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'width', '377px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'margin-bottom', '4px')

		cy.get('[data-cy="lottery-container"]')
			.should('have.css', 'height', '829px')
			.and('have.css', 'padding', '12px 8px 0px')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="lott-grid"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template', '261px 80px 376px 52px / 377px / "left" "rt" "rm" "rb"')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'margin-left', '0px')
			.and('have.css', 'height', '817px')
			.and('have.css', 'grid-template-areas', '"left" "rt" "rm" "rb"')
			.and('have.css', 'grid-template-rows', '261px 80px 376px 52px')
			.and('have.css', 'grid-gap', '16px 10px')
			.and('have.css', 'gap', '16px 10px');

		cy.get('[data-cy="rb-cell"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '44px')
			.and('have.css', 'align-content', 'space-between')
			.and('have.css', 'grid-row-gap', '30px')
			.and('have.css', 'row-gap', '30px')
			.and('have.css', 'grid-template', '44px / 377px / none');

		cy.get('[data-cy="logo-in-lottery"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'fixed')
			.and('have.css', 'left', '12px')
			.and('have.css', 'top', '56px');

		cy.get('[data-cy="mega-items"]')
			.should('have.css', 'text-align', 'right')
			.and('have.css', 'font-size', '11px');

		cy.get('[data-cy="mega-item"]')
			.should('have.css', 'margin', '4px 0px');

		cy.get('[data-cy="mega-item-lbl"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '11px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-transform', 'uppercase');

		cy.get('[data-cy="mega-item-val"]')
			.should('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '11px');

		cy.get('[data-cy="goto-results"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'height', '32px')
			.and('have.css', 'font-size', '11px')
			.and('have.css', 'margin-bottom', '4px');

		cy.get('[data-cy="register-bet-button"]')
			.should('have.css', 'height', '44px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="left-cell"]')
			.should('have.css', 'grid-area', 'left / left / left / left')
			.and('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '90px 91px 0px');

		cy.get('[data-cy="mc-control-add"]')
			.should('have.css', 'height', '36px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'margin-bottom', '8px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '15px');

		cy.get('[data-cy="mc-control-auto"]')
			.should('have.css', 'height', '36px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'font-size', '15px');

		cy.get('[data-cy="mc-register-msg"]')
			.should('have.css', 'text-align', 'center')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'font-size', '12px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'height', '376px')
			.and('have.css', 'padding-top', '0px')
			.and('have.css', 'animation', '2.2s cubic-bezier(0, 0, 0.35, 0.47) 0s infinite normal none running blinker-inv')
			.and('have.css', 'opacity', '0')
			.and('have.css', 'overflow', 'hidden');

		cy.get('[data-cy="mega-item"]')
			.should('have.css', 'margin-top', '4px')
			.and('have.css', 'margin-bottom', '4px');

		cy.get('[data-cy="dbc-draw"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'width', '272px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '45px')
			.and('have.css', 'border', '1px solid rgb(227, 185, 11)');

		cy.get('[data-cy="dbc-number"]')
			.should('have.css', 'font-weight', '400')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '20px');

		cy.get('[data-cy="dbc-data"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'column')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '20px')
			.and('have.css', 'text-align', 'left');

		cy.get('[data-cy="dbc-time-left"]')
			.should('have.css', 'text-transform', 'none')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'margin-top', '2px');

		cy.get('[data-cy="dbc-date"]')
			.should('have.css', 'font-size', '15px')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-weight', '400');

		cy.get('[data-cy="buttons-group-title"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'font-size', '15px')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '385px')
			.and('have.css', 'margin-bottom', '4px');

		cy.get('[data-cy="buttons-group-items"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'flex-flow', 'row wrap');

		cy.get('[data-cy="button"]')
			.should('have.css', 'width', '377px')
			.and('have.css', 'height', '44px')
			.and('have.css', 'padding-left', '10px')
			.and('have.css', 'padding-right', '10px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-weight', '400')
			.and('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'border', '0px none rgb(161, 161, 161)')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'display', 'inline-block')
			.and('have.css', 'align-items', 'flex-start')
			.and('have.css', 'justify-content', 'normal');
	});

	it('Проверка стилей динамически создаваемого DOM в лотерее Мегалот при мобильном разрешении (393 x 873 px)', () => {
		cy.get('[data-cy="mc-bg-draws"] [data-cy="buttons-group-item"]').eq(3).click();
		cy.get('[data-cy="mc-control-add"]').click();

		cy.get('[data-cy="mc-manual-dialog"]').within(() => {
			cy.get('[data-cy="modal-dialog-background"]')
				.should('have.css', 'position', 'fixed')
				.and('have.css', 'top', '0px')
				.and('have.css', 'left', '0px')
				.and('have.css', 'right', '0px')
				.and('have.css', 'bottom', '0px')
				.and('have.css', 'z-index', '85')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'flex-direction', 'column')
				.and('have.css', 'align-items', 'center')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'background-color', 'rgba(0, 0, 0, 0.24)');

			cy.get('[data-cy="modal-dialog-container"]')
				.should('have.css', 'max-width', 'none')
				.and('have.css', 'width', '377px')
				.and('have.css', 'padding', '10px')
				.and('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.24) 0px 0px 20px 0px')
				.and('have.css', 'background-color', 'rgb(255, 255, 255)')
				.and('have.css', 'border-radius', '8px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'text-align', 'left')
				.and('have.css', 'left', '8px')
				.and('have.css', 'right', '8px')
				.and('have.css', 'top', '8px')
				.and('have.css', 'bottom', '8px')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'max-width', 'none');

			cy.get('[data-cy="modal-dialog-close"]')
				.should('have.css', 'display', 'block')
				.and('have.css', 'width', '16px')
				.and('have.css', 'height', '16px')
				.and('have.css', 'margin-right', '0px')
				.and('have.css', 'margin-left', '0px')
				.and('have.css', 'position', 'absolute')
				.and('have.css', 'right', '15px')
				.and('have.css', 'top', '15px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'grid-row-gap', '0px')
				.and('have.css', 'row-gap', '0px');

			cy.get('[data-cy="mc-input-clear-button"]')
				.should('have.css', 'height', '32px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'font-size', '16px');

			cy.get('[data-cy="mc-all-megaballs-text"]')
				.should('have.css', 'width', '357px')
				.and('have.css', 'height', '44px')
				.and('have.css', 'font-size', '18px')
				.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'text-transform', 'uppercase')
				.and('have.css', 'cursor', 'pointer')
				.and('have.css', 'border-radius', '4px')
				.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
				.and('have.css', 'color', 'rgb(0, 151, 136)')
				.and('have.css', 'border', '1px solid rgb(0, 151, 136)')
				.and('have.css', 'position', 'relative')
				.and('have.css', 'display', 'flex')
				.and('have.css', 'justify-content', 'center')
				.and('have.css', 'align-items', 'center');

			cy.get('[data-cy="mc-dlg-bordered"]')
				.should('have.css', 'border-radius', '8px')
				.and('have.css', 'padding', '0px 8px')
				.and('have.css', 'min-height', '0px');

			cy.get('[data-cy="mc-dlg-bordered-emph"]')
				.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif');

			cy.get('[data-cy="mc-dlg-bordered-selected"]')
				.should('have.css', 'text-align', 'center')
				.and('have.css', 'font-size', '14px')
				.and('have.css', 'margin-bottom', '10px');

			cy.get('[data-cy="mc-dlg-bordered-to-select"]')
				.should('have.css', 'text-align', 'center')
				.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'font-size', '14px')
				.and('have.css', 'color', 'rgb(0, 151, 136)')
				.and('have.css', 'animation', '2.2s cubic-bezier(0, 0, 0.35, 0.47) 0s infinite normal none running blinker');

			cy.get('[data-cy="dialog-grid"]')
				.should('have.css', 'display', 'grid')
				.and('have.css', 'grid-template-areas', '"balls balls" "all-megaballs all-megaballs" "megaballs megaballs" "selection selection" "bottom-buttons bottom-buttons"')
				.and('have.css', 'grid-template-columns', '171px 171px')
				.and('have.css', 'grid-column-gap', '15px')
				.and('have.css', 'column-gap', '15px')
				.and('have.css', 'justify-content', 'space-between')
				.and('have.css', 'align-content', 'space-between')
				.and('have.css', 'width', '357px')
				.and('have.css', 'max-width', '100%')
				.and('have.css', 'height', '837px')
				.and('have.css', 'padding-top', '25px')
				.and('have.css', 'padding-bottom', '0px');

			cy.get('[data-cy="balls"]')
				.should('have.css', 'grid-area', '1 / 1 / 2 / 3')
				.and('have.css', 'grid-row-start', '1')
				.and('have.css', 'grid-row-end', '2');

			cy.get('[data-cy="clear-button"]')
				.should('have.css', 'grid-area', 'bottom-buttons / 1 / bottom-buttons / 2')
				.and('have.css', 'grid-row-start', 'bottom-buttons')
				.and('have.css', 'grid-row-end', 'bottom-buttons')
				.and('have.css', 'align-self', 'center')
				.and('have.css', 'padding-bottom', '0px');

			cy.get('[data-cy="megaballs"]')
				.should('have.css', 'grid-area', 'megaballs / megaballs / megaballs / megaballs');

			cy.get('[data-cy="all-megaballs"]')
				.should('have.css', 'grid-area', 'megaballs / 1 / megaballs / 3')
				.and('have.css', 'align-self', 'start')
				.and('have.css', 'padding-bottom', '0px');

			cy.get('[data-cy="bottom-buttons"]')
				.should('have.css', 'grid-area', 'bottom-buttons / 2 / bottom-buttons / 3');

			cy.get('[data-cy="mc-confirm"]')
				.should('have.css', 'max-width', '180px')
				.and('have.css', 'width', '171px')
				.and('have.css', 'height', '32px')
				.and('have.css', 'margin', '0px')
				.and('have.css', 'display', 'block')
				.and('have.css', 'font-size', '16px');

			cy.get('[data-cy="buttons-group-title"]')
				.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
				.and('have.css', 'color', 'rgb(90, 90, 90)')
				.and('have.css', 'font-size', '15px')
				.and('have.css', 'text-align', 'center')
				.and('have.css', 'width', '377px')
				.and('have.css', 'margin-bottom', '8px');

			cy.get('[data-cy="buttons-group-items"]')
				.should('have.css', 'display', 'flex')
				.and('have.css', 'flex-flow', 'row wrap');

			for (let i = 0; i < 10; i++) {
				cy.get('[data-cy="mc-bg-numbers"] [data-cy="buttons-group-item"]').eq(i).click();
			}

			cy.get('[data-cy="mc-all-megaballs"]').click();
			cy.get('[data-cy="mc-confirm"]').click();
		});

		cy.get('[data-cy="mc-control-auto"]').click();

		cy.get('[data-cy="mc-bet"]').should('have.length', 2);

		cy.get('[data-cy="mc-bet"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'row nowrap')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'margin-right', '0px');

		cy.get('[data-cy="mc-bet-inner"]')
			.should('have.css', 'max-width', '270px')
			.and('have.css', 'border', '1px solid rgb(0, 151, 136)')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'padding', '2px')
			.and('have.css', 'flex-grow', '1')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(at center center, rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="mc-bet-balls"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'row wrap')
			.and('have.css', 'justify-content', 'center');

		cy.get('[data-cy="mc-bet-megaballs"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'row wrap')
			.and('have.css', 'justify-content', 'center');

		cy.get('[data-cy="mc-bet-ball"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'margin', '4px 1px')
			.and('have.css', 'border-radius', '50%')
			.and('have.css', 'border-color', 'rgb(0, 151, 136)')
			.and('have.css', 'border-style', 'solid')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'border-width', '1px');

		cy.get('[data-cy="mc-bet-megaball"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'width', '24px')
			.and('have.css', 'height', '24px')
			.and('have.css', 'margin', '4px 1px')
			.and('have.css', 'border-radius', '50%')
			.and('have.css', 'border-color', 'rgb(0, 151, 136)')
			.and('have.css', 'border-style', 'solid')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'border-width', '2px');

		cy.get('[data-cy="mc-bet-plus"]')
			.should('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'margin', '0px 4px')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="mc-bet-close"]')
			.should('have.css', 'width', '16px')
			.and('have.css', 'height', '16px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'color', 'rgb(255, 255, 255)')
			.and('have.css', 'background-color', 'rgb(223, 80, 80)')
			.and('have.css', 'border-radius', '50%')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'line-height', '30px')
			.and('have.css', 'margin-left', '7px')
			.and('have.css', 'margin-right', '-16px');
	});
});
