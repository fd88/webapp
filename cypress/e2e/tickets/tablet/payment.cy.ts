describe('Тесты для страницы выплаты выигрыша при планшетном разрешении (767 x 1024 px)', () => {

	beforeEach(() => {
		cy.viewport(767, 1024);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(1).click();
		});
	});

	it('Тесты для страницы выплаты выигрыша при планшетном разрешении (767 x 1024 px)', () => {
		cy.get('[data-cy="page-main"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '0px 971px')
			.and('have.css', 'height', '971px')
			.and('have.css', 'padding', '0px');

		cy.get('[data-cy="input-field"]').type('1111222233334444');
		cy.get('[data-cy="ticket-check-item"]').click();

		cy.get('[data-cy="page-main-central-menu"]')
			.should('not.be.visible');

		cy.get('[data-cy="ticket-payment-container"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'column wrap')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'flex-start')
			.and('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'width', '767px')
			.and('have.css', 'height', '971px')
			.and('have.css', 'margin-top', '0px');

		cy.get('[data-cy="tpc-header"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'font-style', 'normal')
			.and('have.css', 'font-stretch', '100%')
			.and('have.css', 'line-height', 'normal')
			.and('have.css', 'letter-spacing', '0')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'width', '767px')
			.and('have.css', 'text-align', 'center');

		cy.get('[data-cy="tpc-info"]')
			.should('have.css', 'max-width', '820px')
			.and('have.css', 'width', '767px')
			.and('have.css', 'margin-bottom', '20px');

		cy.get('[data-cy="tpc-info-table"]')
			.should('have.css', 'padding', '15px')
			.and('have.css', 'margin', '11px 151.5px 0px')
			.and('have.css', 'border', '2px solid rgb(161, 161, 161)')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="tpc-info-row"]')
			.should('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row');

		cy.get('[data-cy="tpc-info-text"], [data-cy="tpc-info-value"]')
			.should('have.css', 'width', '215px')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif');

		cy.get('[data-cy="tpc-info-text"]').should('have.css', 'color', 'rgb(90, 90, 90)');

		cy.get('[data-cy="tpc-info-value"]')
			.should('have.css', 'text-align', 'right')
			.and('have.css', 'color', 'rgb(0, 151, 136)');

		cy.get('[data-cy="tpc-payment-state"]')
			.should('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'margin', '15px 0px 0px')
			.and('have.css', 'font-family', 'RobotoBold, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '18px');

		cy.get('[data-cy="tpc-bottom-panel"]').should('have.css', 'width', '767px');

		cy.get('[data-cy="sms-message"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'margin-bottom', '16px');

		cy.get('[data-cy="tpc-controls-panel"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'center')
			.and('have.css', 'width', '767px')
			.and('have.css', 'margin', '0px')
			.and('have.css', 'padding', '0px 12px');

		cy.get('[data-cy="bottom-button"]')
			.should('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'width', '464px')
			.and('have.css', 'max-width', '464px')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '10px')
			.and('have.css', 'height', '60px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'border', '2px solid rgb(90, 90, 90)')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'cursor', 'pointer');

		cy.get('[data-cy="bottom-button-repeat"]')
			.should('have.css', 'width', '464px')
			.and('have.css', 'max-width', '464px')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '10px')
			.and('have.css', 'height', '60px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) radial-gradient(rgb(255, 255, 255) 0%, rgb(216, 244, 244) 100%) repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'color', 'rgb(0, 151, 136)')
			.and('have.css', 'border', '2px solid rgb(0, 151, 136)')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'border-radius', '8px');

		cy.get('[data-cy="bottom-button-payment"]')
			.should('have.css', 'width', '464px')
			.and('have.css', 'max-width', '464px')
			.and('have.css', 'margin-left', '10px')
			.and('have.css', 'margin-right', '10px')
			.and('have.css', 'height', '60px')
			.and('have.css', 'font-size', '24px')
			.and('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'background', 'rgb(128, 128, 128) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border', '0px none rgb(161, 161, 161)')
			.and('have.css', 'font-family', 'RobotoMedium, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'text-transform', 'uppercase')
			.and('have.css', 'cursor', 'pointer')
			.and('have.css', 'border-radius', '8px');
	});
});
