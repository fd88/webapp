describe('Тесты для страницы проверки билетов при мобильном разрешении (393 x 873 px)', () => {

	beforeEach(() => {
		cy.viewport(393, 873);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(1).click();
		});
	});

	it('Тесты для страницы проверки билетов при мобильном разрешении (393 x 873 px)', () => {
		cy.get('[data-cy="page-main"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '0px 829px')
			.and('have.css', 'height', '829px')
			.and('have.css', 'padding', '0px');

		cy.get('[data-cy="ticket-check"]').should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '793px 0px')
			.and('have.css', 'align-content', 'space-between')
			.and('have.css', 'grid-row-gap', '16px')
			.and('have.css', 'row-gap', '16px')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'top', '10px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'right', '0px')
			.and('have.css', 'bottom', '10px')
			.and('have.css', 'padding', '0px 8px');

		cy.get('[data-cy="ticket-check-item"]')
			.should('have.css', 'max-width', '440px')
			.and('have.css', 'width', '377px')
			.and('have.css', 'margin', '10px 0px 0px')
			.and('have.css', 'display', 'block');

		cy.get('[data-cy="switch-camera"]')
			.should('have.css', 'width', '35px')
			.and('have.css', 'border-radius', '5px')
			.and('have.css', 'background', 'rgb(0, 151, 136) none repeat scroll 0% 0% / auto padding-box border-box');

		cy.get('[data-cy="switch-camera-pic"]')
			.should('have.css', 'display', 'block');

		cy.get('[data-cy="tc-input-container"]')
			.should('have.css', 'display', 'grid')
			.and('have.css', 'grid-template-rows', '58px 709px')
			.and('have.css', 'grid-row-gap', '16px')
			.and('have.css', 'row-gap', '16px')
			.and('have.css', 'width', '377px');

		cy.get('[data-cy="codes-input-outer"]')
			.should('have.css', 'display', 'flex');

		cy.get('[data-cy="codes-input"]')
			.should('have.css', 'flex-grow', '1')
			.and('have.css', 'margin-right', '10px')
			.and('have.css', 'width', '332px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-direction', 'row')
			.and('have.css', 'justify-content', 'space-between')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'position', 'relative');

		cy.get('[data-cy="scanner-cell"]')
			.should('have.css', 'width', '377px')
			.and('have.css', 'height', '709px')
			.and('have.css', 'grid-template-rows', 'minmax(0px, 1fr) auto');

		cy.get('[data-cy="spoiler"]')
			.should('have.css', 'font-size', '16px')
			.and('have.css', 'border', '2px solid rgb(128, 128, 128)')
			.and('have.css', 'border-radius', '8px')
			.and('have.css', 'padding', '0px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'height', '0px')
			.and('have.css', 'max-height', '50px');

		cy.get('[data-cy="ticket-check-item"]')
			.should('have.css', 'height', '44px')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'margin-top', '10px');

		cy.get('[data-cy="bc-examples"]')
			.should('have.css', 'color', 'rgb(90, 90, 90)')
			.and('have.css', 'text-align', 'center')
			.and('have.css', 'font-size', '16px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'margin-top', '12px')
			.and('have.css', 'margin-bottom', '4px');

		cy.get('[data-cy="bc-samples"]').should('have.css', 'display', 'flex')
			.and('have.css', 'align-items', 'center')
			.and('have.css', 'justify-content', 'center');

		cy.get('[data-cy="bc-sample"]').should('have.css', 'margin', '4px 10px')
			.and('have.css', 'max-width', '100%')
			.and('have.css', 'height', '60px')
			.and('have.css', 'display', 'block')
			.and('have.css', 'border-radius', '4px')
			.and('have.css', 'object-fit', 'cover')
			.and('have.css', 'object-position', '50% 50%');

		cy.get('[data-cy="input-label"]').should('have.css', 'width', '332px');

		cy.get('[data-cy="input-title"]').should('have.css', 'display', 'block')
			.and('have.css', 'padding', '5px')
			.and('have.css', 'font-family', 'RobotoRegular, Geneva, Arial, Helvetica, sans-serif')
			.and('have.css', 'font-size', '14px')
			.and('have.css', 'color', 'rgb(161, 161, 161)')
			.and('have.css', 'position', 'absolute')
			.and('have.css', 'top', '3px')
			.and('have.css', 'left', '0px')
			.and('have.css', 'z-index', '-1')
			.and('have.css', 'transition', 'top 0.2s ease 0s, font-size 0.2s ease 0s, color 0.2s ease 0s');

		cy.get('[data-cy="input-field"]')
			.should('have.css', 'width', '332px')
			.and('have.css', 'height', '30px')
			.and('have.css', 'padding', '4px')
			.and('have.css', 'color', 'rgb(102, 102, 102)')
			.and('have.css', 'font-size', '18px')
			.and('have.css', 'background', 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box')
			.and('have.css', 'border-radius', '0px');

		cy.get('[data-cy="scanner-block"]')
			.should('have.css', 'height', '194px');

		cy.get('[data-cy="scanner-component"]')
			.should('have.css', 'display', 'block')
			.and('have.css', 'position', 'relative')
			.and('have.css', 'height', '194px');

		cy.get('[data-cy="scandit-block"]')
			.should('have.css', 'border-radius', '8px')
			.and('have.css', 'overflow', 'hidden')
			.and('have.css', 'height', '194px');

		cy.get('[data-cy="spoiler-camera"]')
			.should('have.css', 'position', 'relative')
			.and('have.css', 'width', '365px')
			.and('have.css', 'height', '194px')
			.and('have.css', 'display', 'flex')
			.and('have.css', 'flex-flow', 'column nowrap');


	});
});
