describe('Проверка логики работы страницы результатов в Забаве', () => {

	it('Проверка логики работы страницы результатов в лотерее Лото-Забава', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(2).click();

			cy.get('[data-cy="game-list-item"]').eq(0).within(() => {
				cy.get('[data-cy="game-list-title"]').click();
			});

			cy.url().should('include', 'zabava-lottery/results');

			cy.get('[data-cy="dbc-draw"]').should('have.length', 6);

			cy.get('[data-cy="dbc-draw"]').eq(1).click();

			cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_3');

			cy.get('[data-cy="extra-info"]').should('exist');

			cy.get('[data-cy="zrc-win-comb"]').eq(0).within(() => {
				cy.get('[data-cy="zrc-win-comb-item"]').should('have.length', 53);
			});

			cy.get('[data-cy="win-table"]').within(() => {
				cy.get('[data-cy="win-cat-table-row"]').should('have.length', 5);
			});

			cy.get('[data-cy="zrc-win-comb"]').eq(1).within(() => {
				cy.get('[data-cy="zrc-win-comb-item"]').should('have.length', 9);
			});

			cy.get('[data-cy="win-cat-table"]').eq(1).within(() => {
				cy.get('[data-cy="win-cat-table-row"]').should('have.length', 5);
			});

			cy.get('[data-cy="dbc-draw"]').eq(2).click();

			cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_3');

			cy.get('[data-cy="extra-info"]').should('exist');

			cy.get('[data-cy="zrc-win-comb"]').eq(0).within(() => {
				cy.get('[data-cy="zrc-win-comb-item"]').should('have.length', 16);
			});

			cy.get('[data-cy="win-table"]').within(() => {
				cy.get('[data-cy="win-cat-table-row"]').should('have.length', 5);
			});

			cy.get('[data-cy="zrc-win-comb"]').eq(1).within(() => {
				cy.get('[data-cy="zrc-win-comb-item"]').should('have.length', 1);
			});

			cy.get('[data-cy="win-cat-table"]').eq(1).within(() => {
				cy.get('[data-cy="win-cat-table-row"]').should('have.length', 5);
			});
		});
	});
});
