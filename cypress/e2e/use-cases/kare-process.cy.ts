describe('Проверка регистрации лотереи Каре', () => {

	it('Проверка регистрации лотереи Каре', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(6).click();
		});

		cy.get('[data-cy="kc-draws"]').should('exist');
		cy.get('[data-cy="kc-register-msg"]').should('exist');

		cy.url().should('include', '/kare');
		cy.get('[data-cy="dbc-draw"]').should('have.length', 1);

		cy.get('[data-cy="kc-bg-draws"] [data-cy="buttons-group-item"]').eq(7).click();

		cy.get('[data-cy="kc-table"]').eq(0).click();
		cy.get('[data-cy="cards-row-btn"]').eq(0).click();
		cy.get('[data-cy="cards-row-btn"]').eq(5).click();
		cy.get('[data-cy="cards-row-btn"]').eq(10).click();
		cy.get('[data-cy="cards-row-btn"]').eq(15).click();
		cy.get('[data-cy="cards-row-btn"]').eq(18).click();

		cy.get('[data-cy="kc-bets-buttons"] [data-cy="buttons-group-item"]').eq(0).click();

		cy.get('[data-cy="bet-amount"]')
			.invoke('text')
			.then(t => parseInt(t))
			.should('equal', 5);

		cy.get('[data-cy="input-field"]').should('have.value', 5);
		cy.get('[data-cy="kare-card-with-label"]').should('have.length', 10);

		cy.get('[data-cy="dialog-continue"]').click();


		cy.get('[data-cy="kc-table"]').eq(1).click();

		for (let i = 0; i < 10; i++) {
			cy.get('[data-cy="cards-row-btn"]').eq(i).click();
		}

		cy.get('[data-cy="kc-bets-buttons"] [data-cy="buttons-group-item"]').eq(0).click();

		cy.get('[data-cy="bet-amount"]')
			.invoke('text')
			.then(t => parseInt(t))
			.should('equal', 5);

		cy.get('[data-cy="input-field"]').should('have.value', 5);

		cy.get('[data-cy="dialog-continue"]').click();

		cy.get('[data-cy="kc-bet"]').should('have.length', 2);

		cy.get('[data-cy="register-bet-button"]').click();

		cy.get('[data-cy="ci-data-value"]')
			.eq(0)
			.invoke('text')
			.then((txt) => txt.trim())
			.then((txt) => parseInt(txt))
			.should('equal', 8);

		cy.get('[data-cy="ci-data-value"]')
			.eq(1)
			.invoke('text')
			.then((txt) => txt.trim())
			.then((txt) => parseInt(txt))
			.should('equal', 8);

		cy.get('[data-cy="ci-data-value"]')
			.eq(2)
			.invoke('text')
			.then((txt) => txt.trim())
			.then((txt) => parseInt(txt))
			.should('equal', 1);

		cy.get('[data-cy="ci-data-value"]')
			.eq(3)
			.invoke('text')
			.then((txt) => txt.trim())
			.then((txt) => parseInt(txt))
			.should('equal', 1);

		cy.get('[data-cy="ci-data-value"]')
			.eq(4)
			.invoke('text')
			.then((txt) => txt.trim())
			.then((txt) => parseInt(txt))
			.should('equal', 440);
	});
});
