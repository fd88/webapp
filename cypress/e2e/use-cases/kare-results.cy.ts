describe('Проверка логики работы страницы результатов в Каре', () => {

	it('Проверка логики работы страницы результатов в лотерее Каре', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(2).click();

			cy.get('[data-cy="game-list-item"]').eq(4).within(() => {
				cy.get('[data-cy="game-list-title"]').click();
			});

			cy.url().should('include', 'kare/results');
			cy.get('[data-cy="rnc-content-container"]').should('have.class', 'rnc-content-container_game-code_52');

			cy.get('[data-cy="dbc-draw"]').should('have.length', 5);

			for (let i = 0; i < 5; i++) {
				cy.get('[data-cy="dbc-draw"]').eq(i).click();
				cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_52');

				cy.get('[data-cy="kare-cards"]').should('exist');
				cy.get('[data-cy="kare-poker"]').should('exist');

				cy.get('[data-cy="card-with-label"]').should('have.length', 5);
			}
		});
	});
});
