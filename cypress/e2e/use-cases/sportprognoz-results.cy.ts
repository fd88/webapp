describe('Проверка логики работы страницы результатов в Спортпрогнозе', () => {

	it('Проверка логики работы страницы результатов в лотерее Спортпрогноз', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(2).click();

			cy.get('[data-cy="game-list-item"]').eq(3).within(() => {
				cy.get('[data-cy="game-list-title"]').click();
			});

			cy.url().should('include', 'sportprognoz/results');
			cy.get('[data-cy="rnc-content-container"]').should('have.class', 'rnc-content-container_game-code_2');

			cy.get('[data-cy="dbc-draw"]').should('have.length', 5);

			for (let i = 0; i < 5; i++) {
				cy.get('[data-cy="dbc-draw"]').eq(i).click();
				cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_2');

				cy.get('[data-cy="participants-list"] [data-cy="plc-row"]').should('have.length', 13);

				for (let j = 0; j < 13; j++) {
					cy.get('[data-cy="participants-list"] [data-cy="plc-row"]').eq(j).within(() => {
						cy.get('[data-cy="plc-row-cell"]').should('have.length', 7);
					});
				}

				cy.get('[data-cy="win-category-table"] [data-cy="win-cat-table-row"]').should('have.length', 4);

				for (let j = 0; j < 4; j++) {
					cy.get('[data-cy="win-category-table"] [data-cy="win-cat-table-row"]').eq(j).within(() => {
						cy.get('[data-cy="win-cat-table-cell"]').should('have.length', 2);
					});
				}

				cy.get('[data-cy="extra-info"]').should('exist');
			}
		});
	});
});
