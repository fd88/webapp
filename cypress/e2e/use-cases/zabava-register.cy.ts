describe('Проверка регистрации лотереи Лото-Забава', () => {

	it('Проверка регистрации лотереи Лото-Забава', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(0).click();
		});

		cy.get('[data-cy="zc-summary"]').should('exist');

		cy.get('[data-cy="zc-bg-tickets"] [data-cy="buttons-group-item"]').eq(4).click();

		cy.get('[data-cy="zc-bg-pairs"] [data-cy="buttons-group-item"]').eq(2).click();

		cy.get('[data-cy="register-bet-button"]').click();

		cy.url().should('include', '/registry');

		cy.get('[data-cy="ci-data-value"]')
			.eq(2)
			.invoke('text')
			.then((txt) => txt.trim())
			.should('equal', '3');

		cy.get('[data-cy="ci-data-value"]')
			.eq(3)
			.invoke('text')
			.then((txt) => txt.trim())
			.should('equal', '5');

		cy.get('[data-cy="ci-data-value"]')
			.eq(4)
			.invoke('text')
			.then((txt) => parseInt(txt.trim()))
			.should('equal', 125);
	});
});
