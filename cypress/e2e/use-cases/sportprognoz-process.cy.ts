describe('Проверка регистрации Спортпрогноза', () => {

	it('Проверка регистрации лотереи Спортпрогноз', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(5).click();
		});

		cy.get('[data-cy="spc-dp"]').should('exist');
		cy.get('[data-cy="spc-temp-text"]').should('exist');
		cy.url().should('include', '/sportprognoz');
		cy.get('[data-cy="dbc-draw"]').should('have.length', 3);

		for (let i = 0; i < 3; i++) {
			cy.get('[data-cy="dbc-draw"]').eq(i).click();
			cy.get('[data-cy="spc-show-program"]').click();
			cy.get('[data-cy="program-table-row"]').should('have.length', 26);
			cy.get('[data-cy="close-button"]').click();
		}
	});
});
