describe('Проверка логики работы страницы результатов в ГнД', () => {

	it('Проверка логики работы страницы результатов в лотерее Гонка на Деньги', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(2).click();

			cy.get('[data-cy="game-list-item"]').eq(2).within(() => {
				cy.get('[data-cy="game-list-title"]').click();
			});

			cy.url().should('include', 'gonka-lottery/results');

			cy.get('[data-cy="dbc-draw"]').should('have.length', 5);

			for (let i = 0; i < 5; i++) {
				cy.get('[data-cy="dbc-draw"]').eq(i).click();
				cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_13');
				cy.get('[data-cy="grc-comb"]').should('have.length', 4);
			}
		});
	});
});
