describe('Проверка билета и выплата выигрыша', () => {

	beforeEach(() => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(1).click();
		});
	});

	it('Выигрышный билет, можно выплатить', () => {
		cy.get('[data-cy="codes-input"] [data-cy="input-field"]').type('1111222233334444');
		cy.get('[data-cy="ticket-check-item"]').click();

		cy.get('[data-cy="tpc-info-value"]')
			.eq(0)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 100);

		cy.get('[data-cy="tpc-info-value"]')
			.eq(1)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 80.5);

		cy.get('[data-cy="tpc-payment-state"]').should('have.class', 'allowed-payment');
		cy.get('[data-cy="bottom-button-repeat"]').click();

		cy.get('[data-cy="pin-input"]').type('1234');
		cy.get('[data-cy="bottom-button-sms"]').should('exist');

		cy.get('[data-cy="bottom-button-payment"]').click();

		cy.get('[data-cy="modal-button"]').click();
	});

	it('Выигрышный билет, нельзя выплатить', () => {
		cy.get('[data-cy="codes-input"] [data-cy="input-field"]').type('1111111122222222');
		cy.get('[data-cy="ticket-check-item"]').click();

		cy.get('[data-cy="tpc-info-value"]')
			.eq(0)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 3000);

		cy.get('[data-cy="tpc-info-value"]')
			.eq(1)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 2415);

		cy.get('[data-cy="tpc-payment-state"]').should('not.have.class', 'allowed-payment');
		cy.get('[data-cy="bottom-button-repeat"]').should('not.exist');

		cy.get('[data-cy="pin-input"]').should('not.exist');
		cy.get('[data-cy="bottom-button-sms"]').should('not.exist');
		cy.get('[data-cy="bottom-button-payment"]').should('not.exist');

		cy.get('[data-cy="bottom-button"]').click();

	});

	it('Проигрышный билет', () => {
		cy.get('[data-cy="codes-input"] [data-cy="input-field"]').type('0729003245090860');
		cy.get('[data-cy="ticket-check-item"]').click();

		cy.get('[data-cy="tpc-info-value"]')
			.eq(0)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 0);

		cy.get('[data-cy="tpc-info-value"]')
			.eq(1)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 0);

		cy.get('[data-cy="bottom-button-repeat"]').should('not.exist');

		cy.get('[data-cy="pin-input"]').should('not.exist');
		cy.get('[data-cy="bottom-button-sms"]').should('not.exist');
		cy.get('[data-cy="bottom-button-payment"]').should('not.exist');

		cy.get('[data-cy="bottom-button"]').click();

		cy.get('[data-cy="pin-input"]').type('1234');
		cy.get('[data-cy="bottom-button"]').last().click();

		cy.get('[data-cy="modal-button"]').click();
	});
});
