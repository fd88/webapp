describe('Проверка процесса авторизации пользователя', () => {

	it('Проверка процесса авторизации пользователя', () => {
		cy.viewport(1920, 1000);
		cy.visit('/');

		cy.url().should('include', '/auth');

		cy.get('[data-cy="auth-button-type-confirm"]').click();

		cy.get('[data-cy="phones-form"]').should('exist');

		cy.get('[data-cy="client-phone"]').eq(0).click();

		cy.get('[data-cy="auth-button-type-confirm"]').click();

		cy.get('[data-cy="terminals"]').should('exist');

		cy.get('[data-cy="terminal-item"]').eq(0).click();

		cy.get('[data-cy="changelog-close"]').should('exist');

		cy.get('[data-cy="changelog-close"]').click();

		cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();

		cy.get('[data-cy="game-list"]').should('exist');
	});

});
