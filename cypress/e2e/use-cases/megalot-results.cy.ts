describe('Проверка логики работы страницы результатов в Мегалоте', () => {

	it('Проверка логики работы страницы результатов в лотерее Мегалот', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="central-menu-item"]').eq(2).click();

			cy.get('[data-cy="game-list-item"]').eq(1).within(() => {
				cy.get('[data-cy="game-list-title"]').click();
			});

			cy.url().should('include', 'megalot-lottery/results');

			cy.get('[data-cy="dbc-draw"]').should('have.length', 6);

			cy.get('[data-cy="dbc-draw"]').eq(0).click();
			cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_1');
			cy.get('[data-cy="extra-info"]').should('exist');
			cy.get('[data-cy="mrc-ball"]').should('have.length', 6);
			cy.get('[data-cy="mrc-mega-ball"]').should('have.length', 1);
			cy.get('[data-cy="win-cat-table-row"]').should('have.length', 9);

			cy.get('[data-cy="dbc-draw"]').eq(1).click();
			cy.get('[data-cy="rnc-header-date"]').should('have.class', 'rnc-header-date_game-code_1');
			cy.get('[data-cy="extra-info"]').should('exist');
			cy.get('[data-cy="mrc-ball"]').should('have.length', 6);
			cy.get('[data-cy="mrc-mega-ball"]').should('have.length', 1);
			cy.get('[data-cy="win-cat-table-row"]').should('have.length', 9);
		});
	});
});
