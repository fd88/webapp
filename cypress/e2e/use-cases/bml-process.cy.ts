import {BML_LIST} from "../../../src/app/modules/features/mocks/tml-bml-list";

describe('Проверка регистрации БМЛ', () => {

	it('Проверка регистрации бумажных моментальных лотерей (Стирачки)', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(4).click();
		});

		cy.get('[data-cy="input-tmlbml"]').should('exist');
		cy.url().should('include', '/tmlbml');

		cy.get('[data-cy="bml-row"]').should('have.length', BML_LIST.length + 2);

		for (let i = BML_LIST.length - 1; i >= 0; i--) {
			cy.get('[data-cy="close-icon"]').eq(i).click();
		}

		cy.get('[data-cy="barcode-input-input"]').type('0662542857001006');
		cy.get('[data-cy="bml-row"]').should('have.length', 3);

		cy.get('[data-cy="register-bet-button"]')
			.should('contain.text', '49.55');

		cy.get('[data-cy="register-bet-button"]').click();

		cy.get('[data-cy="ci-data-value"]')
			.eq(1)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 1);

		cy.get('[data-cy="ci-data-value"]')
			.eq(2)
			.invoke('text')
			.then(v => parseFloat(v.trim()))
			.should('equal', 49.55);
	});
});
