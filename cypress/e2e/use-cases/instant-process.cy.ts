describe('Проверка регистрации ЭМЛ', () => {

	it('Проверка регистрации электронных моментальных лотерей', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(2).click();
		});

		cy.get('[data-cy="instant-lotteries-container"]').should('exist');
		cy.url().should('include', '/instant-lotteries');

		cy.get('[data-cy="igc-game-item-content"]').eq(0).click();
		cy.get('[data-cy="il-bg-tickets"] [data-cy="buttons-group-item"]').eq(4).click();
		cy.get('[data-cy="draws-item"]').should('have.length', 1);
		cy.get('[data-cy="draws-item-serie-num"]')
			.eq(0)
			.invoke('text')
			.then((txt) => txt.trim())
			.should('equal', '17');

		cy.get('[data-cy="draws-item-serie-sum"]')
			.eq(0)
			.invoke('text')
			.then((txt) => parseInt(txt.trim()))
			.should('equal', 10);

		cy.get('[data-cy="register-bet-button"]').click();

		cy.get('[data-cy="ci-data-value"]')
			.eq(1)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 17);
		cy.get('[data-cy="ci-data-value"]')
			.eq(2)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 5);
		cy.get('[data-cy="ci-data-value"]')
			.eq(3)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 50);
	});
});
