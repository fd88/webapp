describe('Проверка регистрации ГнД', () => {

	it('Проверка регистрации лотереи Гонка на Деньги', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(3).click();
		});

		cy.get('[data-cy="gg-bg-draws"]').should('exist');
		cy.url().should('include', '/gonka-lottery');

		cy.get('[data-cy="gg-bg-draws"] [data-cy="buttons-group-item"]').eq(2).click();
		cy.get('[data-cy="gg-bg-bet-count"] [data-cy="buttons-group-item"]').eq(3).click();
		cy.get('[data-cy="gg-bg-game-type"] [data-cy="buttons-group-item"]').eq(4).click();
		cy.get('[data-cy="gg-bg-game-subtype"] [data-cy="buttons-group-item"]').eq(1).click();
		cy.get('[data-cy="two-buttons-modal"] [data-cy="modal-button"]').eq(1).click();

		cy.get('[data-cy="gg-race-numbers"] [data-cy="buttons-group-item"]').eq(0).click();
		cy.get('[data-cy="gg-race-numbers"] [data-cy="buttons-group-item"]').eq(1).click();
		cy.get('[data-cy="gg-race-numbers"] [data-cy="buttons-group-item"]').eq(2).click();
		cy.get('[data-cy="gg-race-numbers"] [data-cy="buttons-group-item"]').eq(3).click();

		cy.get('[data-cy="game-type"].game-type_d').within(() => {
			cy.get('[data-cy="gg-bg-race"]').eq(0).within(() => {
				cy.get('[data-cy="buttons-group-item"]').eq(0).click();
				cy.get('[data-cy="buttons-group-item"]').eq(1).click();
			});
			cy.get('[data-cy="gg-bg-race"]').eq(1).within(() => {
				cy.get('[data-cy="buttons-group-item"]').eq(1).click();
				cy.get('[data-cy="buttons-group-item"]').eq(2).click();
			});
			cy.get('[data-cy="gg-bg-race"]').eq(2).within(() => {
				cy.get('[data-cy="buttons-group-item"]').eq(2).click();
				cy.get('[data-cy="buttons-group-item"]').eq(3).click();
			});
			cy.get('[data-cy="gg-bg-race"]').eq(3).within(() => {
				cy.get('[data-cy="buttons-group-item"]').eq(3).click();
				cy.get('[data-cy="buttons-group-item"]').eq(4).click();
			});
			cy.get('[data-cy="button-modal-button"]').click();
		});

		cy.get('[data-cy="gg-bg-bet-sum"] [data-cy="buttons-group-item"]').eq(1).click();

		cy.get('[data-cy="register-bet-button"]').click();

		cy.get('[data-cy="ci-data-value"]')
			.eq(0)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 3);

		cy.get('[data-cy="ci-data-value"]')
			.eq(1)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 16);

		cy.get('[data-cy="ci-data-value"]')
			.eq(2)
			.invoke('text')
			.then(v => v.trim())
			.should('equal', 'Д');

		cy.get('[data-cy="ci-data-value"]')
			.eq(3)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 4);

		cy.get('[data-cy="ci-data-value"]')
			.eq(4)
			.invoke('text')
			.then(v => parseInt(v.trim()))
			.should('equal', 192);
	});
});
