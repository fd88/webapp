describe('Проверка регистрации лотереи Мегалот', () => {

	it('Проверка регистрации лотереи Мегалот', () => {
		cy.viewport(1920, 1000);
		cy.fixture('100605').then(storage => {
			localStorage.setItem('ua.msl.lottery_terminal', JSON.stringify(storage));
			cy.visit('/lotteries');
			cy.get('[data-cy="jackpots-dialog"] [data-cy="button-modal-button"]').click();
			cy.get('[data-cy="game-list-item"]').eq(1).click();
		});

		cy.get('[data-cy="mc-control-add"]').should('exist');
		cy.url().should('include', '/megalot-lottery/init');


		cy.get('[data-cy="mc-bg-draws"] [data-cy="buttons-group-item"]').eq(3).click();

		for (let i = 0; i < 4; i++) {
			cy.get('[data-cy="mc-control-auto"]').click();
		}

		cy.get('[data-cy="mc-bet"]').should('have.length', 4);

		cy.get('[data-cy="mc-control-add"]').click();

		for (let i = 0; i < 10; i++) {
			cy.get('[data-cy="mc-bg-numbers"] [data-cy="buttons-group-item"]').eq(i).click();
		}

		cy.get('[data-cy="mc-all-megaballs-text"]').click();

		cy.get('[data-cy="mc-confirm"]').click();

		cy.get('[data-cy="mc-bet"]').should('have.length', 5);

		cy.get('[data-cy="register-bet-button"]').click();

		cy.url().should('include', '/registry');

		cy.get('[data-cy="ci-data-value"]')
			.eq(0)
			.invoke('text')
			.then((txt) => txt.trim())
			.should('equal', '4');

		cy.get('[data-cy="ci-data-value"]')
			.eq(1)
			.invoke('text')
			.then((txt) => txt.trim())
			.should('equal', '5');

		cy.get('[data-cy="ci-data-value"]')
			.eq(2)
			.invoke('text')
			.then((txt) => txt.replace(' ', ''))
			.then((txt) => parseInt(txt))
			.should('equal', 11840);
	});
});
