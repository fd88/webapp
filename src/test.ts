/**
 * Этот файл требуется для karma.conf.js и рекурсивно загружает все *.spec-файлы и файлы фреймворка
 */
import 'zone.js/dist/zone-testing';
import { getTestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import 'hammerjs';

/**
 * Требуется для нахождения всех тестов.
 */
declare const require: any;

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
	BrowserDynamicTestingModule,
	platformBrowserDynamicTesting(),
	{
		teardown: { destroyAfterEach: false }
	}
);

/**
 * Нахождение всех тестов.
 */
const context = require.context('./', true, /\.spec\.ts$/);

// And load the modules.
context.keys()
	.map(context);
