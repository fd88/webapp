const packageJson = require("../package.json");
const fs = require('fs');

const distFolderPath = './dist/';
const buildFileName = 'buildInfo.json';
const versionFileName = 'version.txt';

const buildInfo = {
	"msl_main": {
		"name": "msl_main",
		"order": 6,
		"type": "webapp",
		"version": packageJson.version,
		"dependsOn": "ua.msl.lottery_terminal"
	}
};

fs.writeFile(`${distFolderPath}${buildFileName}`, JSON.stringify(buildInfo, null, 4), (err) => {
	if (err) {
		console.error(err);
		return;
	}

	console.log(`File "${buildFileName}" has been created`);
});

fs.writeFile(`${distFolderPath}${packageJson.name}/${versionFileName}`, packageJson.version, (err) => {
	if (err) {
		console.error(err);
		return;
	}

	console.log(`File "${versionFileName}" has been created`);
});
