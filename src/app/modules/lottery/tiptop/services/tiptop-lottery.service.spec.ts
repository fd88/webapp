import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {TranslateModule} from '@ngx-translate/core';

import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {HttpService} from '@app/core/net/http/services/http.service';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {TransactionService} from '@app/core/services/transaction/transaction.service';
import {TiptopLotteryService} from '@app/tiptop/services/tiptop-lottery.service';
import {StorageService} from '@app/core/net/ws/services/storage/storage.service';
import {PrintService} from '@app/core/net/ws/services/print/print.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LotteriesService} from '@app/core/services/lotteries.service';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

const draws = [{
	"draw": {
		"bet_sum": "2.00",
		"code": "4262",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-21T14:25:00",
		"dr_edate": "2020-05-21T15:00:00",
		"num": "02374",
		"sale_bdate": "2020-05-20T14:20:00",
		"sale_edate": "2020-05-21T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-21T16:00:00",
		"win_edate": "2020-11-17T16:00:00"
	}
}, {
	"draw": {
		"bet_sum": "2.00",
		"code": "4264",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-22T14:25:00",
		"dr_edate": "2020-05-22T15:00:00",
		"num": "02375",
		"sale_bdate": "2020-05-21T14:20:00",
		"sale_edate": "2020-05-22T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-22T16:00:00",
		"win_edate": "2020-11-18T16:00:00"
	}
}, {
	"draw": {
		"bet_sum": "2.00",
		"code": "4266",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-23T14:25:00",
		"dr_edate": "2020-05-23T15:00:00",
		"num": "02376",
		"sale_bdate": "2020-05-22T14:20:00",
		"sale_edate": "2020-05-23T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-23T16:00:00",
		"win_edate": "2020-11-19T16:00:00"
	}
}, {
	"draw": {
		"bet_sum": "2.00",
		"code": "4268",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-24T14:25:00",
		"dr_edate": "2020-05-24T15:00:00",
		"num": "02377",
		"sale_bdate": "2020-05-23T14:20:00",
		"sale_edate": "2020-05-24T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-24T16:00:00",
		"win_edate": "2020-11-20T16:00:00"
	}
}, {
	"draw": {
		"bet_sum": "2.00",
		"code": "4270",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-25T14:25:00",
		"dr_edate": "2020-05-25T15:00:00",
		"num": "02378",
		"sale_bdate": "2020-05-24T14:20:00",
		"sale_edate": "2020-05-25T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-25T16:00:00",
		"win_edate": "2020-11-21T16:00:00"
	}
}, {
	"draw": {
		"bet_sum": "2.00",
		"code": "4274",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-26T14:25:00",
		"dr_edate": "2020-05-26T15:00:00",
		"num": "02379",
		"sale_bdate": "2020-05-25T14:20:00",
		"sale_edate": "2020-05-26T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-26T16:00:00",
		"win_edate": "2020-11-22T16:00:00"
	}
}, {
	"draw": {
		"bet_sum": "2.00",
		"code": "4277",
		"data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
		"dr_bdate": "2020-05-27T14:25:00",
		"dr_edate": "2020-05-27T15:00:00",
		"num": "02380",
		"sale_bdate": "2020-05-26T14:20:00",
		"sale_edate": "2020-05-27T14:20:00",
		"serie_code": "2",
		"serie_num": "",
		"status": "Регистрация многотиражных ставок",
		"status_code": "DRST_BET_MULTI",
		"version": "1",
		"win_bdate": "2020-05-27T16:00:00",
		"win_edate": "2020-11-23T16:00:00"
	}
}];

describe('TiptopLotteryService', () => {
	let service: TiptopLotteryService;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				TiptopLotteryService,
				AppStoreService,
				StorageService,
				HttpService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},
				PrintService,
				TransactionService,
				LotteriesService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(TiptopLotteryService);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Tip, {
			lott_code: LotteryGameCode.Tip.toString(),
			lott_name: 'lottery.tiptop.game_name',
			currency: 'UAH',
			lott_extra: undefined,
			draws
		});
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		service.init();
		service.listGameItem.push({
			lotteryType: TiptopLotteryService.Tip,
			inputMode: '0',
			draws,
			amount: 500,
			drawsCount: 1,
			betsCount: 1,
			isUnique: 1,
			isSingle: '1',
			lotteryName: 'lottery.tiptop.game_name'
		});
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test init method', () => {
		service.init();
		expect(service.listGameItem).toEqual([]);
	});

	it('test getTipTopGameItem method', () => {
		service.init();
		expect(service.getTipTopGameItem(0)).toBeNull();
	});

	it('test getTipTopGameItem method', () => {
		expect(service.getTipTopGameItem(0)).not.toBeNull();
	});

	it('test getGameItemCount method', () => {
		service.listGameItem = undefined;
		expect(service.getGameItemCount()).toEqual(0);
	});

	it('test getGameItemCount method', () => {
		expect(service.getGameItemCount()).toEqual(1);
	});

	it('test refreshBets method', () => {
		spyOn(service, 'refreshBets').and.callThrough();
		service.refreshBets();
		expect(service.refreshBets).toHaveBeenCalled();
	});

	it('test buyLottery method', () => {
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();
	});

	it('test buyLottery method 2', () => {
		service.listGameItem.push({
			lotteryType: TiptopLotteryService.Top,
			inputMode: '0',
			draws,
			amount: 500,
			drawsCount: 1,
			betsCount: 1,
			isUnique: 1,
			isSingle: '1',
			lotteryName: 'lottery.tiptop.game_name'
		});
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();
	});
});
