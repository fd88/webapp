import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { URL_TIPTOP } from '@app/util/route-utils';
import { numberFromStringCurrencyFormat } from '@app/util/utils';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { TipRegBetReq, TipTopRegBetResp } from '@app/core/net/http/api/models/tip-reg-bet';
import { TopRegBetReq } from '@app/core/net/http/api/models/top-reg-bet';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { BaseGameService, IBetDataItem } from '@app/core/game/base-game.service';
import { LogOutService } from '@app/logout/services/log-out.service';

/**
 * Режим ввода ставки.
 */
type BetsInputMode = '0' | '1' | '2' | '3';

/**
 * Переключатель - мультитиражный ли чек (нет/да)
 */
type BetsToggle = '0' | '1';

/**
 * Интерфейс для хранения данных, заполняемых оператором на странице лотерей "Тип и Топ".
 */
export interface ITipTopBetDataItem extends IBetDataItem {
	/**
	 * Лотерея - Тип или Топ.
	 */
	lotteryType: string;
}

/**
 * Возвращает модель с начальными данными для лотереи "Тип и Топ".
 */
const initBetDataFn = (): ITipTopBetDataItem => {
	return {
		drawCount: 0,
		lotteryType: TiptopLotteryService.Tip
	};
};

/**
 * Данные ставки для лотереи "Тип и Топ".
 */
export interface ITipTopGameItem {

	/**
	 * Текущий тип лотереи, по которому будут производится расчеты.
	 */
	lotteryType: string;

	/**
	 * Режим ввода ставки.
	 */
	inputMode: BetsInputMode; // = '2';

	/**
	 * Тиражи.
	 */
	draws: Array<UpdateDrawInfoDraws>;

	/**
	 * Текущая сумма к оплате.
	 */
	amount: number;

	/**
	 * Выбранное пользователем количество тиражей.
	 */
	drawsCount: number;

	/**
	 * Выбранное пользователем количество ставок.
	 */
	betsCount: number;

	/**
	 * Признак того, что комбинации в каждом тираже должны быть одинаковые.
	 */
	isUnique: number;

	/**
	 * Признак того, что запрашивается единый мультитиражный чек.
	 */
	isSingle: BetsToggle;

	/**
	 * Текущий имя лотереи.
	 */
	lotteryName: string;

}

/**
 * Сервис лотерей Тип и Топ.
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей, и взаимодейсвия с ЦС.
 * @todo Нахрен переделать эту шнягу!
 */
@Injectable()
export class TiptopLotteryService extends BaseGameService<ITipTopBetDataItem> {

	// -----------------------------
	//  Constants
	// -----------------------------
	/**
	 * Тип лотереи - Тип.
	 */
	static readonly Tip = 'tip';

	/**
	 * Тип лотереи - Топ.
	 */
	static readonly Top = 'top';

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * URL игры.
	 */
	readonly gameUrl = URL_TIPTOP;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.tiptop.game_name';

	/**
	 * Общее количество тиражей, отображаемых на экране.
	 */
	readonly DRAWS_COUNT = 10;

	/**
	 * Максимальное допустимое количество ставок в билете.
	 */
	readonly BETS_COUNT = 10;

	/**
	 * Элемент ставки в интерфейсе
	 */
	listGameItem: Array<ITipTopGameItem> = [];

	/**
	 * Текущие данные ставки.
	 */
	currentBetsData: ITipTopGameItem;

	/**
	 * Были ли изменения в выборе тиражей на странице лотереи
	 */
	drawsTouched = false;

	/**
	 * Были ли изменения в выборе ставок на странице лотереи
	 */
	betsTouched = false;

	/**
	 * Были ли изменения в выборе комбинаций на странице лотереи
	 */
	combsTouched = false;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {TranslateService} translateService Сервис переводов.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект местоположения.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();

		this.currentGameCode = LotteryGameCode.Tip;
		this.currentGroupCode = LotteriesGroupCode.Regular;
		this.initialBetDataFactory = initBetDataFn;
	}

	/**
	 * Инициализация начальных параметров сервиса.
	 */
	init(): void {
		this.listGameItem = [];
		this.refreshAmount();
	}

	/**
	 * Создание и добавление элемента ставки в список.
	 * @param initLotteryType Тип лотереи
	 */
	createAndAddGameItem(initLotteryType?: string): ITipTopGameItem {
		const mLotteryType = initLotteryType ? initLotteryType : TiptopLotteryService.Tip;
		const gameItem: ITipTopGameItem = {
			lotteryType: mLotteryType,
			inputMode: '2',
			draws: this.getDrawsByLotteryType(mLotteryType),
			amount: 0,
			drawsCount: 0,
			betsCount: 0,
			isUnique: 0,
			isSingle: '0',
			lotteryName: this.getLotteryNameByType(mLotteryType)
		};
		this.listGameItem.push(gameItem);

		return gameItem;
	}

	// addGameItem(mIsTip: boolean, mDrawsCount: number, mIsUnique: boolean, mBetsCount): void {
	// 	const mLotteryType = mIsTip ? TiptopLotteryService.Tip : TiptopLotteryService.Top;
	// 	const gameItem: ITipTopGameItem = {
	// 		lotteryType: mLotteryType,
	// 		inputMode: '2',
	// 		draws: this.getDrawsByLotteryType(mLotteryType),
	// 		amount: 0,
	// 		drawsCount: mDrawsCount,
	// 		betsCount: mBetsCount,
	// 		isUnique: mIsUnique,
	// 		isSingle: '0',
	// 		lotteryName: this.getLotteryNameByType(mLotteryType)
	// 	};
	//
	// 	this.listGameItem.push(gameItem);
	//
	// 	this.refreshBetsInItem(gameItem);
	// 	this.refreshAmount();
	// }

	/**
	 * Получает элемент ставки по позиции.
	 * @param position Позиция элемента ставки
	 */
	getTipTopGameItem(position: number): ITipTopGameItem {
		if (this.listGameItem && position < this.listGameItem.length) {
			return this.listGameItem[position];
		}

		return null;
	}

	/**
	 * Устанавливает тип лотереи Тип или Топ.
	 * @param item Элемент ставки
	 * @param type Тип лотереи
	 */
	setLotteryType(item: ITipTopGameItem, type: string): void {
		if (item) {
			item.lotteryType = type;
			item.draws = this.getDrawsByLotteryType(item.lotteryType);
			item.lotteryName = this.getLotteryNameByType(item.lotteryType);
		}
	}

	// --- ITipTopGameService ----------------
	/**
	 * Получает количество элементов ставки.
	 */
	getGameItemCount(): number {
		if (this.listGameItem) {
			return this.listGameItem.length;
		}

		return 0;
	}

	/**
	 * Обновляет сумму ставки в элементе ставки.
	 * @param item Элемент ставки
	 */
	refreshBetsInItem(item: ITipTopGameItem): void {
		let amount = 0;
		if (item && !!item.draws && item.drawsCount <= item.draws.length) {
			for (let draw_index = 0; draw_index < item.drawsCount; draw_index++) {
				amount += (numberFromStringCurrencyFormat(item.draws[draw_index].draw.bet_sum) * item.betsCount);
			}

			item.amount = amount / 100;
		} else {
			item.amount = 0;
		}

		this.refreshAmount();
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновляет ставки (пустой метод, унаследован от BaseGameService)
	 */
	refreshBets(): void {
	}

	/**
	 * Функция покупки лотереи.
	 */
	buyLottery(): void {
		let buyTipTop;
		let lotteryGameCode;
		if (this.listGameItem && this.listGameItem.length > 0 && this.listGameItem[0]) {
			buyTipTop = this.makeRequest(this.listGameItem[0]);
			lotteryGameCode = this.listGameItem[0].lotteryType === TiptopLotteryService.Tip ? LotteryGameCode.Tip : LotteryGameCode.Top;
			if (buyTipTop !== null && this.listGameItem.length > 1 && this.listGameItem[1]) {
				this.appendParam(buyTipTop, this.listGameItem[1]);
			}
		}

		if (buyTipTop) {
			this.buyTipTop(buyTipTop, lotteryGameCode);
		}
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Выполняет запрос на покупку лотереи.
	 * @param item Элемент ставки
	 * @private
	 */
	private makeRequest(item: ITipTopGameItem): CancelableApiClient {
		if (item) {
			if (item.lotteryType === TiptopLotteryService.Tip) {
				return new TipRegBetReq(
					this.appStoreService,
					this.betData,
					item.inputMode,
					item.betsCount.toString(),
					item.drawsCount.toString(),
					item.isUnique === 1,
					item.isSingle
				);
			} else if (item.lotteryType === TiptopLotteryService.Top) {
				return new TopRegBetReq(
					this.appStoreService,
					this.betData,
					item.inputMode,
					item.betsCount.toString(),
					item.drawsCount.toString(),
					item.isUnique === 1,
					item.isSingle
				);
			}
		}

		return undefined;
	}

	/**
	 * Получает список тиражей по типу лотереи.
	 * @param lotteryType Тип лотереи
	 * @private
	 */
	private getDrawsByLotteryType(lotteryType: string): Array<UpdateDrawInfoDraws> {
		switch (lotteryType) {
			case TiptopLotteryService.Tip:
				return this.appStoreService.Draws.getDraws(LotteryGameCode.Tip);

			case TiptopLotteryService.Top:
				return this.appStoreService.Draws.getDraws(LotteryGameCode.Top);

			default:
				return undefined;
		}
	}

	/**
	 * Возвращает имя текущей лотереи
	 * @param type Тип лотереи
	 * @private
	 */
	private getLotteryNameByType(type: string): string {
		switch (type) {
			case TiptopLotteryService.Top:
				return 'lottery.tiptop.game_top_name';

			case TiptopLotteryService.Tip:
				return 'lottery.tiptop.game_tip_name';

			default:
				return '';
		}
	}

	/**
	 * Обновляет сумму ставки.
	 * @private
	 */
	private refreshAmount(): void {
		this.amount = 0;
		if (this.listGameItem) {
			this.listGameItem
				.forEach(item => {
					this.amount += item.amount;
				});
		}
	}

	/**
	 * Добавляет параметры в запрос.
	 * @param requestParam Параметры запроса
	 * @param mTipTopGameItem Элемент ставки
	 * @private
	 */
	private appendParam(requestParam: any, mTipTopGameItem: ITipTopGameItem): void {
		if (requestParam && mTipTopGameItem) {
			if (mTipTopGameItem.lotteryType === TiptopLotteryService.Tip) {
				TipRegBetReq.appendParam(requestParam, mTipTopGameItem.betsCount.toString(),
					mTipTopGameItem.drawsCount.toString(), mTipTopGameItem.isUnique === 1, mTipTopGameItem.isSingle);
			} else if (mTipTopGameItem.lotteryType === TiptopLotteryService.Top) {
				TopRegBetReq.appendParam(requestParam, mTipTopGameItem.betsCount.toString(),
					mTipTopGameItem.drawsCount.toString(), mTipTopGameItem.isUnique === 1, mTipTopGameItem.isSingle);
			}
		}
	}

	/**
	 * Функция покупки лотереи.
	 * @param tipTopRegBetReq - запрос на покупку лотереи
	 * @param lotteryGameCode - код лотереи
	 * @private
	 */
	private buyTipTop(tipTopRegBetReq: CancelableApiClient, lotteryGameCode: LotteryGameCode): void {
		Logger.Log.i('TiptopLotteryService', 'start buying lottery (%s)', lotteryGameCode)
			.console();

		this.transactionService.buyLottery(TipTopRegBetResp, lotteryGameCode, tipTopRegBetReq);
		// this.currentBetsData = undefined; - было при успешной продаже в промисе
	}

}
