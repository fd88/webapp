import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { URL_INIT, URL_REGISTRY, URL_RESULTS } from '@app/util/route-utils';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { TiptopRegistryResolver } from '@app/tiptop/tiptop-registry-resolver.service';
import { TiptopLotteryContentComponent } from '@app/tiptop/components/tiptop-lottery-content/tiptop-lottery-content.component';
import { TiptopLotteryInitComponent } from '@app/tiptop/components/tiptop-lottery-init/tiptop-lottery-init.component';
import { TiptopResultsComponent } from '@app/tiptop/components/tiptop-results/tiptop-results.component';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Тип и Топ".
 */
const routes: Routes = [
	{
		path: ``,
		component: TiptopLotteryContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: URL_INIT,
				component: TiptopLotteryInitComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: TiptopRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_RESULTS,
				component: TiptopResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}
];

/**
 * Список маршрутов для модуля игры "Тип и Топ".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		TiptopRegistryResolver
	]
})
export class TiptopModuleRoutingModule {}
