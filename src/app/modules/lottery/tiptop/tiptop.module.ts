import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared/shared.module';
import { TiptopModuleRoutingModule } from '@app/tiptop/tiptop-routing.module';

import { TiptopLotteryService } from '@app/tiptop/services/tiptop-lottery.service';

import { TiptopLotteryContentComponent } from '@app/tiptop/components/tiptop-lottery-content/tiptop-lottery-content.component';
import { TiptopLotteryInitComponent } from '@app/tiptop/components/tiptop-lottery-init/tiptop-lottery-init.component';
import { TiptopResultsComponent } from '@app/tiptop/components/tiptop-results/tiptop-results.component';

/**
 * Модуль предназначен для работы с лотореей "Тип и Топ".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		TiptopModuleRoutingModule,
		SharedModule,
		FormsModule
	],
	declarations: [
		TiptopLotteryContentComponent,
		TiptopLotteryInitComponent,
		TiptopResultsComponent
	],
	providers: [
		TiptopLotteryService
	]
})
export class TiptopModule {}
