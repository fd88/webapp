import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { formattedAmount } from '@app/util/utils';
import { ITipTopGameItem, TiptopLotteryService } from '@app/tiptop/services/tiptop-lottery.service';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource, GameSaleMode } from '@app/core/game/base-game.service';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';
import { ICheckInfoRow } from '../../features/check-information/check-information/interfaces/icheck-info-row';

/**
 * Резолвер данных для отображения информации о ручной ставке в игре ТипТоп.
 */
@Injectable()
export class TiptopRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {TiptopLotteryService} tiptopLotteryService Сервис для работы с лотереями Тип и Топ
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью
	 * @param {Router} router Сервис для работы с маршрутизацией
	 */
	constructor(
		private readonly tiptopLotteryService: TiptopLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		const parsedUrl = this.router.parseUrl(state.url);
		// проверить наличие акционных параметров

		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET) || parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			let betData;
			let gameCode;
			if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
				const actionBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionsButton;
				betData = actionBet.data;
				gameCode = actionBet.gameCode === LotteryGameCode.Tip ? 'tip' : 'top';
			}

			if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
				const demoBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET)) as IActionsButton;
				betData = demoBet.data;
				gameCode = demoBet.gameCode === LotteryGameCode.Tip ? 'tip' : 'top';
			}

			this.tiptopLotteryService.initBetData(BetDataSource.Manual);
			this.tiptopLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			this.tiptopLotteryService.init();
			this.tiptopLotteryService.currentBetsData = this.tiptopLotteryService.createAndAddGameItem();
			this.tiptopLotteryService.currentBetsData.drawsCount = Number.isInteger(betData.drawCount) ? betData.drawCount : 1;
			this.tiptopLotteryService.currentBetsData.betsCount = Number.isInteger(betData.betCount) ? betData.betCount : 1;
			this.tiptopLotteryService.setLotteryType(this.tiptopLotteryService.currentBetsData, gameCode);
			this.tiptopLotteryService.refreshBetsInItem(this.tiptopLotteryService.currentBetsData);
		}

		if (!this.tiptopLotteryService.activeDraws || !this.tiptopLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.tiptopLotteryService
			};
		}

		let checkInfoRowList: Array<ICheckInfoRow>;
		if (this.tiptopLotteryService.getGameItemCount() > 0) {
			let gameItem: ITipTopGameItem = this.tiptopLotteryService.getTipTopGameItem(0);
			checkInfoRowList = this.makeInfoRowList(gameItem);

			if (this.tiptopLotteryService.getGameItemCount() > 1) {
				gameItem = this.tiptopLotteryService.getTipTopGameItem(1);
				checkInfoRowList.push(undefined, ...this.makeInfoRowList(gameItem));
			}
		}

		return {
			checkInfoRowList,
			baseGameService: this.tiptopLotteryService
		};
	}

	/**
	 * Создает список строк для отображения информации о ставке.
	 * @param gameItem Элемент игры.
	 * @private
	 */
	private makeInfoRowList(gameItem: ITipTopGameItem): Array<ICheckInfoRow> {
		const combinationRow = (): Array<ICheckInfoRow> => {
			return gameItem.drawsCount > 1
				? [{
					langKeyName: 'lottery.tiptop.combinations',
					value: `${gameItem.isUnique ? 'lottery.the_same' : 'lottery.various'}`
				}]
				: [];
		};

		return [
			{
				langKeyName: 'lottery.lottery',
				value: `${gameItem.lotteryName}`
			},
			{
				langKeyName: 'lottery.draws_count',
				value: `${gameItem.drawsCount}`
			},
			{
				langKeyName: 'lottery.tickets_count',
				value: `${gameItem.betsCount}`
			},
			...combinationRow(),
			{
				langKeyName: 'lottery.sum',
				value: formattedAmount(gameItem.amount),
				redColor: true,
				valueIsCurrency: true
			}
		];
	}

}
