import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiptopLotteryInitComponent } from './tiptop-lottery-init.component';
import {TiptopLotteryService} from "@app/tiptop/services/tiptop-lottery.service";
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {LotteriesService} from "@app/core/services/lotteries.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {IDialog} from "@app/core/dialog/types";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";

describe('TiptopLotteryInitComponent', () => {
  let component: TiptopLotteryInitComponent;
  let fixture: ComponentFixture<TiptopLotteryInitComponent>;
  let appStoreService: AppStoreService;
  let tiptopLotteryService: TiptopLotteryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			RouterTestingModule.withRoutes(ROUTES),
			SharedModule,
			TranslateModule.forRoot({
				loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
				}
			}),
			HttpClientTestingModule,
			FormsModule
		],
      declarations: [ TiptopLotteryInitComponent ],
		providers: [
			TiptopLotteryService,
			LogService,
			LotteriesService,
			HttpService,
			AppStoreService,
			{
				provide: DialogContainerService,
				useValue: {
					showOneButtonInfo: (): IDialog => {
						return;
					}
				}
			}
		]
    })
    .compileComponents();

	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.Draws = new LotteriesDraws();
	  appStoreService.Draws.setLottery(LotteryGameCode.Tip, {
		  "lott_name": "Тіп",
		  "lott_code": "71",
		  "currency": "",
		  "draws": [{
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4262",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-21T14:25:00",
				  "dr_edate": "2020-05-21T15:00:00",
				  "num": "02374",
				  "sale_bdate": "2020-05-20T14:20:00",
				  "sale_edate": "2020-05-21T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-21T16:00:00",
				  "win_edate": "2020-11-17T16:00:00"
			  }
		  }, {
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4264",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-22T14:25:00",
				  "dr_edate": "2020-05-22T15:00:00",
				  "num": "02375",
				  "sale_bdate": "2020-05-21T14:20:00",
				  "sale_edate": "2020-05-22T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-22T16:00:00",
				  "win_edate": "2020-11-18T16:00:00"
			  }
		  }, {
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4266",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-23T14:25:00",
				  "dr_edate": "2020-05-23T15:00:00",
				  "num": "02376",
				  "sale_bdate": "2020-05-22T14:20:00",
				  "sale_edate": "2020-05-23T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-23T16:00:00",
				  "win_edate": "2020-11-19T16:00:00"
			  }
		  }, {
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4268",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-24T14:25:00",
				  "dr_edate": "2020-05-24T15:00:00",
				  "num": "02377",
				  "sale_bdate": "2020-05-23T14:20:00",
				  "sale_edate": "2020-05-24T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-24T16:00:00",
				  "win_edate": "2020-11-20T16:00:00"
			  }
		  }, {
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4270",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-25T14:25:00",
				  "dr_edate": "2020-05-25T15:00:00",
				  "num": "02378",
				  "sale_bdate": "2020-05-24T14:20:00",
				  "sale_edate": "2020-05-25T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-25T16:00:00",
				  "win_edate": "2020-11-21T16:00:00"
			  }
		  }, {
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4274",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-26T14:25:00",
				  "dr_edate": "2020-05-26T15:00:00",
				  "num": "02379",
				  "sale_bdate": "2020-05-25T14:20:00",
				  "sale_edate": "2020-05-26T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-26T16:00:00",
				  "win_edate": "2020-11-22T16:00:00"
			  }
		  }, {
			  "draw": {
				  "bet_sum": "2.00",
				  "code": "4277",
				  "data": {"bet_sum_max": "22500.00", "bet_sum_min": "100.00", "max_win_sum": "0.00"},
				  "dr_bdate": "2020-05-27T14:25:00",
				  "dr_edate": "2020-05-27T15:00:00",
				  "num": "02380",
				  "sale_bdate": "2020-05-26T14:20:00",
				  "sale_edate": "2020-05-27T14:20:00",
				  "serie_code": "2",
				  "serie_num": "",
				  "status": "Регистрация многотиражных ставок",
				  "status_code": "DRST_BET_MULTI",
				  "version": "1",
				  "win_bdate": "2020-05-27T16:00:00",
				  "win_edate": "2020-11-23T16:00:00"
			  }
		  }],
		  lott_extra: undefined
	  });
	  // const operator = new Operator('testMegalotUser', `${Date.now()}`, '123456', 1);
	  // appStoreService.operator.next(operator);
	  tiptopLotteryService = TestBed.inject(TiptopLotteryService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TiptopLotteryInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onSelectedTipTopBetsHandler', () => {
		component.onSelectedTipTopBetsHandler({
			index: 4,
			label: '5',
			selected: true,
			disabled: false
		});
		expect(component.betsData.betsCount).toEqual(5);
	});

	it('test onSelectedTipTopDrawsHandler', () => {
		component.onSelectedTipTopDrawsHandler({
			index: 4,
			label: '5',
			selected: true,
			disabled: false
		});
		expect(component.betsData.drawsCount).toEqual(5);
	});

	it('test onChangeUniqueModeHandler', () => {
		component.onChangeUniqueModeHandler();
		expect(tiptopLotteryService.combsTouched).toBeTruthy();
	});

	it('test onSelectedLotteryTypeHandler', () => {
		const ev = new Event('onSelectedLotteryTypeHandler');
		const target = document.createElement('input');
		target.value = TiptopLotteryService.Tip;
		component.onSelectedLotteryTypeHandler({...ev, target});
		expect(component.betsData.lotteryType).toEqual(TiptopLotteryService.Tip);
	});


});
