import { Component, OnInit, ViewChild } from '@angular/core';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { ITipTopGameItem, TiptopLotteryService } from '@app/tiptop/services/tiptop-lottery.service';

/**
 * Компонент ввода лотереи "Тип и Топ".
 */
@Component({
	selector: 'app-tiptop-lottery-init',
	templateUrl: './tiptop-lottery-init.component.html',
	styleUrls: ['./tiptop-lottery-init.component.scss']
})
export class TiptopLotteryInitComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Компонент кнопок для выбора количества тиражей
	 */
	@ViewChild('drawsComponent', { static: true })
	drawsComponent: ButtonsGroupComponent;

	/**
	 * Компонент кнопок для выбора количества ставок
	 */
	@ViewChild('betsComponent', { static: true })
	betsComponent: ButtonsGroupComponent;

	/**
	 * Список стилей группы кнопок.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Список режимов работы группы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Сервис лотереи "Тип и Топ".
	 */
	readonly TiptopLotteryService = TiptopLotteryService;

	/**
	 * Список типов игр.
	 */
	listOfGameTypes = [TiptopLotteryService.Tip, TiptopLotteryService.Top];

	/**
	 * Данные для ставки в лотереях "Тип и Топ".
	 */
	betsData: ITipTopGameItem;

	/**
	 * Ближайшие тиражи.
	 */
	nearestDraws: any = {tip: null, top: null};

	/**
	 * Разрешена ли кнопка покупки лотереи
	 */
	buyEnabled: boolean;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента регистрации для лотереи "ТипТоп".
	 *
	 * @param {TiptopLotteryService} tiptopLotteryService Сервис для работы с лотереей ТипТоп.
	 */
	constructor(
		readonly tiptopLotteryService: TiptopLotteryService
	) {}

	/**
	 * Обработчик выбора лотереи: "Тип" или "Топ".
	 *
	 * @param {Event} event Передаваемый объект события.
	 */
	onSelectedLotteryTypeHandler(event: Event): void {
		this.tiptopLotteryService.setLotteryType(this.betsData, (event.target as HTMLInputElement).value);
		this.tiptopLotteryService.refreshBetsInItem(this.betsData);
		if (!this.nearestDraws[this.betsData.lotteryType]) {
			this.nearestDraws[this.betsData.lotteryType] = this.betsData;
		}
	}

	/**
	 * Обработчик выбора количества тиражей.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedTipTopDrawsHandler(button: IButtonGroupItem): void {
		const prevDrawsCount = this.betsData.drawsCount;
		this.betsData.drawsCount = button ? Number.parseInt(button.label, 10) : 0;
		this.tiptopLotteryService.drawsTouched = this.betsData.drawsCount > 0;
		if (this.betsData.drawsCount < 2) {
			this.betsData.isUnique = null;
		} else if (this.betsData.isUnique === null) {
			this.betsData.isUnique = 0;
		}

		if (this.betsData.drawsCount > 1) {
			this.tiptopLotteryService.combsTouched = true;
		}

		this.updateBuyState();
		this.tiptopLotteryService.refreshBetsInItem(this.betsData);
	}

	/**
	 * Обработчик выбора количества ставок.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedTipTopBetsHandler(button: IButtonGroupItem): void {
		this.betsData.betsCount = button ? Number.parseInt(button.label, 10) : 0;
		this.tiptopLotteryService.betsTouched = this.betsData.betsCount > 0;
		this.updateBuyState();
		this.tiptopLotteryService.refreshBetsInItem(this.betsData);
	}

	/**
	 * Обработчик выбора уникальной ставки (одинаковые комбинации) по всем тиражам.
	 */
	onChangeUniqueModeHandler(): void {
		this.tiptopLotteryService.combsTouched = true;
		// this.betsData.isUnique = button.index === 1;
		this.updateBuyState();
		this.tiptopLotteryService.refreshBetsInItem(this.betsData);
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Обновление состояния кнопки покупки лотереи
	 * @private
	 */
	private updateBuyState(): void {
		this.buyEnabled = this.tiptopLotteryService.drawsTouched
			&& this.tiptopLotteryService.betsTouched &&	(this.tiptopLotteryService.combsTouched || this.betsData.drawsCount > 0);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		if (!this.tiptopLotteryService.currentBetsData) {
			this.tiptopLotteryService.currentBetsData = this.tiptopLotteryService.createAndAddGameItem();
			// Mock Data
			// TODO: Параметризовать и перенести потом в session-data
			// this.tiptopLotteryService.currentBetsData.draws = [{
			// 	draw: {
			// 		bet_sum: '1.00',
			// 		code: '62011',
			// 		data: {
			// 			bet_sum_max: '20000.00',
			// 			bet_sum_min: '100.00',
			// 			max_win_sum: '10000.00'
			// 		},
			// 		dr_bdate: '2015-05-28 10:45:20',
			// 		dr_edate: '2026-10-25 23:59:59',
			// 		num: '1234',
			// 		sale_bdate: '2015-05-22 00:00:00',
			// 		sale_edate: '2025-10-22 00:00:00',
			// 		serie_code: '192',
			// 		serie_num: '1234',
			// 		status: 'Продажа и выплата выигрышей',
			// 		status_code: 'SALES AND PAYS',
			// 		version: '19',
			// 		win_bdate: '2015-05-22 00:00:00',
			// 		win_edate: '2026-08-22 00:00:00'
			// 	}
			// }];
			// MockData
		}

		this.betsData = this.tiptopLotteryService.currentBetsData;

		this.drawsComponent.selectedButtons = this.betsData.drawsCount === 0 ? [] : this.betsData.drawsCount - 1;
		this.betsComponent.selectedButtons = this.betsData.betsCount === 0 ? [] : this.betsData.betsCount - 1;

		if (!this.tiptopLotteryService.combsTouched) {
			this.betsData.isUnique = null;
		}

		this.tiptopLotteryService.refreshBetsInItem(this.betsData);

		if (!this.nearestDraws[this.betsData.lotteryType]) {
			this.nearestDraws[this.betsData.lotteryType] = this.betsData;
		}

		this.updateBuyState();
	}
}
