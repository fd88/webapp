import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { ReportsService } from '@app/core/services/report/reports.service';
import { DatePipe } from '@angular/common';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { TiptopResultsComponent } from '@app/tiptop/components/tiptop-results/tiptop-results.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import {LotteryGameCode} from "@app/core/configuration/lotteries";

describe('TiptopResultsComponent', () => {
	let component: TiptopResultsComponent;
	let fixture: ComponentFixture<TiptopResultsComponent>;
	let gameResultService: GameResultsService | any;
	let csConfig;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				SharedModule,
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot({})
			],
			declarations: [
				TiptopResultsComponent
			],
			providers: [
				LogService,
				GameResultsService,
				HttpService,
				ReportsService,
				DatePipe,
				MslCurrencyPipe,
				StorageService,
				TransactionService,
				PrintService,
				LotteriesService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				TranslateService
			]

		})
		.compileComponents();

		csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		TestBed.inject(LogService);
		gameResultService = TestBed.inject(GameResultsService);
		gameResultService.appStoreService = TestBed.inject(AppStoreService);
		gameResultService.appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(TiptopResultsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectedLotteryTypeHandler', () => {
		component.onSelectedLotteryTypeHandler(LotteryGameCode.Tip);
		expect(component.resultsNavigatorComponent.gameCode).toEqual(LotteryGameCode.Tip);
	});

	it('test onSelectedLotteryTypeHandler 2', () => {
		component.onSelectedLotteryTypeHandler(LotteryGameCode.Top);
		expect(component.resultsNavigatorComponent.gameCode).toEqual(LotteryGameCode.Top);
	});

	it('test onSelectedResultsHandler', () => {
		component.onSelectedResultsHandler(undefined);
		expect(component.viewData).toBeUndefined();
	});
});
