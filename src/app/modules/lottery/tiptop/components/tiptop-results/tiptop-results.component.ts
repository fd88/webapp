import { Component, OnInit, ViewChild } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDrawingResult } from '@app/core/net/http/api/models/get-draw-results';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import {
	addWinCatRows,
	IAbstractViewResults,
	IDrawingView,
	IResultPrintItem,
	PrintResultTypes
} from '@app/core/services/results/results-utils';
import { ResultsNavigatorComponent } from '@app/shared/components/results-navigator/results-navigator.component';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import { TiptopLotteryService } from '@app/tiptop/services/tiptop-lottery.service';
import { TranslateService } from '@ngx-translate/core';

/**
 * Интерфейс результатов "Тип" и "Топ".
 */
interface ITipTopResults  extends IAbstractViewResults {
	/**
	 * Массив данных по тиражам для отображения.
	 */
	drawing: Array<IDrawingView>;
}

/**
 * Компонент результатов игр "Тип" и "Топ".
 */
@Component({
	selector: 'app-tiptop-results',
	templateUrl: './tiptop-results.component.html',
	styleUrls: ['./tiptop-results.component.scss']
})
export class TiptopResultsComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на компонент навигации по результатам.
	 */
	@ViewChild(ResultsNavigatorComponent, { static: true })
	resultsNavigatorComponent: ResultsNavigatorComponent;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Список стилей кнопок.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Список режимов работы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Список типов игр.
	 */
	readonly listOfGameTypes = [
		TiptopLotteryService.Tip,
		TiptopLotteryService.Top
	];

	/**
	 * Текущая выбранная игра: "Тип" или "Топ".
	 */
	currentGame: LotteryGameCode = LotteryGameCode.Tip;

	/**
	 * Текущее имя выбранной игры: "Тип" или "Топ".
	 */
	currentGameName: string;

	/**
	 * Данные для отображения.
	 */
	viewData: ITipTopResults;

	/**
	 * Данные для печати.
	 */
	printData: Array<IResultPrintItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GameResultsService} gameResultsService Сервис результатов игр.
	 * @param {TranslateService} translateService Сервис переводов.
	 */
	constructor(
		readonly gameResultsService: GameResultsService,
		private readonly translateService: TranslateService
	) {}

	/**
	 * Обработчик выбора лотереи: "Тип" или "Топ".
	 *
	 * @param {LotteryGameCode} lottCode Код игры.
	 */
	onSelectedLotteryTypeHandler(lottCode: LotteryGameCode): void {
		this.currentGame = lottCode;
		this.currentGameName = lottCode === LotteryGameCode.Tip
			? 'lottery.tiptop.game_tip_name'
			: 'lottery.tiptop.game_top_name';
		this.viewData = undefined;
		this.printData = undefined;
		this.resultsNavigatorComponent.gameCode = this.currentGame;
		this.resultsNavigatorComponent.initComponent();
	}

	/**
	 * Обработчик нажатия выбора результатов по тиражу.
	 *
	 * @param {IDrawingResult} result Результаты тиража.
	 */
	onSelectedResultsHandler(result: IDrawingResult): void {
		if (!result) {
			this.viewData = undefined;
			this.printData = undefined;

			return;
		}

		const drawNumber = result.draw_name;
		const drawDate = result.drawing_date_begin;
		const extraInfo = result.drawing[0].extra_info;
		const drawing = result.drawing.map(m => {
			const winComb: Array<string> = m.win_comb ? m.win_comb.split('') : undefined;

			return {...m, winComb};
		});

		this.viewData = {drawNumber, drawDate, extraInfo, drawing};
		this.printData = this.printDataParser();
	}

	/**
	 * Функция-трекер для отслеживания изменений в массиве выигрышных комбинаций.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByWinCombFn = (index, item: string) => index;

	/**
	 * Функция-трекер для отслеживания изменений в массиве выигрышных тирожей.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByWinDrawingFn = (index, item: IDrawingView) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Функция-парсер данных для печати.
	 */
	private printDataParser(): Array<IResultPrintItem> {
		const gameName = `lottery.tiptop.${this.currentGame === LotteryGameCode.Tip ? 'game_tip_name' : 'game_top_name'}`;
		const result = [
			{key: PrintResultTypes.DoubleLine, value: undefined},
			{key: PrintResultTypes.TextCenter, value: ['lottery.game_results', gameName]},
			{key: PrintResultTypes.DrawNumber, value: [this.viewData.drawNumber]},
			{key: PrintResultTypes.DrawDate, value: [this.viewData.drawDate]}
		];

		this.viewData.drawing.forEach(d => {
			if (d.winComb) {
				result.push({key: PrintResultTypes.TwoColumnTableCustom, value: ['lottery.win_combination', d.winComb.join('')]});
				result.push({key: PrintResultTypes.DoubleLine, value: undefined});
				result.push({
					key: PrintResultTypes.TwoColumnTable,
					value: ['lottery.category', 'lottery.win']
				});
				result.push({key: PrintResultTypes.Line, value: undefined});
				addWinCatRows(d, result, false, false);
			}
		});

		result.push({key: PrintResultTypes.Line, value: undefined});

		return result;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.currentGame = LotteryGameCode.Tip;
		this.currentGameName = 'lottery.tiptop.game_tip_name';
	}

}
