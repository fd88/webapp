import { Component, OnDestroy } from '@angular/core';
import { TiptopLotteryService } from '@app/tiptop/services/tiptop-lottery.service';

/**
 * Компонент-контейнер для компонентов лотереи Типтоп
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class TiptopLotteryContentComponent implements  OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TiptopLotteryService} tiptopLotteryService Сервис для работы с лотереей Типтоп.
	 */
	constructor(
		private readonly tiptopLotteryService: TiptopLotteryService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.tiptopLotteryService.init();
		this.tiptopLotteryService.currentBetsData = undefined;
		this.tiptopLotteryService.drawsTouched = false;
		this.tiptopLotteryService.betsTouched = false;
		this.tiptopLotteryService.combsTouched = false;

		this.tiptopLotteryService.resetBetData();
		// this.tiptopCouponService.resetConfiguration();
	}

}
