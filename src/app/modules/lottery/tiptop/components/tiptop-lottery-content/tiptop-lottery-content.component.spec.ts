import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';


import { HttpService } from '@app/core/net/http/services/http.service';


import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';
import { TiptopLotteryContentComponent } from '@app/tiptop/components/tiptop-lottery-content/tiptop-lottery-content.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { TiptopLotteryService } from '@app/tiptop/services/tiptop-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TiptopLotteryContentComponent', () => {
	let component: TiptopLotteryContentComponent;
	let fixture: ComponentFixture<TiptopLotteryContentComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			declarations: [
				TiptopLotteryContentComponent
			],
			providers: [
				LogService,
				LotteriesService,
				DeclineWordPipe,
				TiptopLotteryService,


				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TiptopLotteryContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
