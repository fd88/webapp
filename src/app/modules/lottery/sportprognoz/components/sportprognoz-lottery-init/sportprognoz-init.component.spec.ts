import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { BetDataSource } from '@app/core/game/base-game.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { RouterTestingModule } from '@angular/router/testing';

import { SportprognozInitComponent } from '@app/sportprognoz/components/sportprognoz-lottery-init/sportprognoz-init.component';
import { SportprognozLotteryService } from '@app/sportprognoz/services/sportprognoz-lottery.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDialog } from "@app/core/dialog/types";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {DRAWS_FOR_GAME_SPORTPROGNOZ} from "../../../../features/mocks/sportprognoz-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {ROUTES} from "../../../../../app-routing.module";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient} from "@angular/common/http";
import {LotteriesService} from "@app/core/services/lotteries.service";

describe('SportprognozInitComponent', () => {
	let component: SportprognozInitComponent | any;
	let appStoreService: AppStoreService;
	let fixture: ComponentFixture<SportprognozInitComponent>;
	let sportprognozLotteryService: SportprognozLotteryService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes(ROUTES),
				SharedModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				}),
				HttpClientTestingModule
			],
			declarations: [
				SportprognozInitComponent,
			],
			providers: [
				LogService,
				SportprognozLotteryService,
				HttpService,
				AppStoreService,
				LotteriesService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);

		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Sportprognoz, DRAWS_FOR_GAME_SPORTPROGNOZ.lottery);

		sportprognozLotteryService = TestBed.inject(SportprognozLotteryService);
		sportprognozLotteryService.initBetData(BetDataSource.Manual);

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		sportprognozLotteryService.appStoreService.Settings.populateEsapActionsMapping(csConfig);

	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SportprognozInitComponent);
		component = fixture.componentInstance;
		component.sportprognozLotteryService.initBetData(BetDataSource.Manual);
		component.sportprognozLotteryService.dialogInfoService.showNoneButtonsInfo = (): IDialog => undefined;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onClickShowProgramHandler handler', () => {

		component.isVisibleProgram = false;
		component.onClickShowProgramHandler();
		expect(component.isVisibleProgram).toBeTruthy();

		component.isVisibleProgram = true;
		component.onClickShowProgramHandler();
		expect(component.isVisibleProgram).toBeFalsy();

	});

	it('test onClickOutsideProgramHandler handler', () => {
		component.isVisibleProgram = true;
		component.onClickOutsideProgramHandler({
			target: {
				id: 'spc-program-container'
			}
		});
		expect(component.isVisibleProgram).toBeFalsy();

		component.isVisibleProgram = true;
		component.onClickOutsideProgramHandler({});
		expect(component.isVisibleProgram).toBeTruthy();

	});

	it('test onSelectedDrawHandler handler', () => {
		spyOn(component, 'onSelectedDrawHandler').and.callThrough();
		component.onSelectedDrawHandler(0);
		expect(component.onSelectedDrawHandler).toHaveBeenCalled();
	});

	it('test trackByProgram function', () => {
		expect(component.trackByProgram(1, undefined)).toEqual(1);
	});

	it('test onClickPrintProgramHandler handler', () => {
		spyOn(component, 'onClickPrintProgramHandler').and.callThrough();
		component.onClickPrintProgramHandler({copiesCount: 1});
		expect(component.onClickPrintProgramHandler).toHaveBeenCalled();
	});

	it('test onNoActiveDrawsHandler handler', () => {
		component.onNoActiveDrawsHandler();
		expect(component.sportprognozLotteryService.betData.selectedDraw).toBeUndefined();
	});

	it('test updateUIPropertiesForCurrentDraw handler', () => {
		component.sportprognozLotteryService.betData.selectedDraw = 0;
		spyOn(component, 'updateUIPropertiesForCurrentDraw').and.callThrough();
		component.updateUIPropertiesForCurrentDraw();
		expect(component.updateUIPropertiesForCurrentDraw).toHaveBeenCalled();
	});

});
