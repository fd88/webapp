import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject, timer} from 'rxjs';
import {DATE_TEMPLATE_DD_MM_YYYY_HH_MM, UserInputMode} from '@app/util/utils';
import { BetDataSource } from '@app/core/game/base-game.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IPrintButtonAction } from '@app/shared/components/print-button/print-button.component';
import { SportprognozLotteryService } from '@app/sportprognoz/services/sportprognoz-lottery.service';
import { ISportPrognozProgramItem } from '@app/sportprognoz/interfaces/isport-prognoz-program-item';
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {takeUntil} from "rxjs/operators";
import {ScannerInputComponent} from "../../../../features/camera/components/scanner-input/scanner-input.component";
import {ApplicationAppId} from "@app/core/services/store/settings";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";

const POSSIBLE = [
	'00001', //  (1) - ничья
	'00010', //  (2) - победа 1
	'00100', //   (4) - победа 2
	'00011', //  (3) - победа 1 или ничья
	'00101', //  (5) - победа 2 или ничья
	'00110', //  (6) - победа 1 или победа 2
	'00111', //  (7) - победа 1 или победа 2 или ничья
	'01001', //  (9) - ничья
	'01010', //  (10) - победа 1
	'01100', //  (12) - победа 2
	'10001', //  (17) - ничья
	'10010', //  (18) - победа 1
	'10100', //  (20) - победа 2
];

/**
 * Компонент ввода лотереи "Спортпрогноз".
 */
@Component({
	templateUrl: './sportprognoz-init.component.html',
	styleUrls: ['./sportprognoz-init.component.scss'],
	animations: []
})
export class SportprognozInitComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Формат короткой записи даты.
	 */
	readonly shortDate = DATE_TEMPLATE_DD_MM_YYYY_HH_MM;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Текущий номер тиража.
	 */
	currentDrawNum: string;

	/**
	 * Текущая дата тиража.
	 */
	currentDrawDate: Date;

	scannerForm!: FormGroup;

	/**
	 * Признак видимости программы на экране.
	 */
	isVisibleProgram = false;

	@ViewChild('scannerInput', { static: true }) scannerInput: ScannerInputComponent;

	/**
	 * Сохраненный режим ввода для каждого пользователя (вручную или со сканера).
	 */
	userInputStorage: {[userID: number]: UserInputMode};

	/**
	 * Текущий режим ввода (вручную или со сканера).
	 */
	userInputMode: UserInputMode = 0;

	/**
	 * Раскрыта ли камера?
	 */
	isCameraShown = false;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента регистрации для лотереи "Спортпрогноз".
	 *
	 * @param {SportprognozLotteryService} sportprognozLotteryService Сервис лотереи "Спортпрогноз".
	 * @param barcodeReaderService
	 * @param fb
	 * @param appStoreService
	 * @param storageService
	 */
	constructor(
		readonly sportprognozLotteryService: SportprognozLotteryService,
		private readonly barcodeReaderService: BarcodeReaderService,
		private fb: FormBuilder,
		readonly appStoreService: AppStoreService,
		private readonly storageService: StorageService
	) {}

	/**
	 * Слушатель нажатия кнопки показа тиража.
	 */
	onClickShowProgramHandler(): void {
		this.isVisibleProgram = !this.isVisibleProgram;

		if (this.isVisibleProgram) {
			this.sportprognozLotteryService.getProgramData()
				.subscribe();
		}
	}

	/**
	 * Слушатель клика по области за контейнером с программой.
	 * Закрывает окно с программой.
	 *
	 * @param event Событие клика.
	 */
	onClickOutsideProgramHandler(event): void {
		if (event.target && event.target.id && event.target.id === 'spc-program-container') {
			this.isVisibleProgram = false;
		}
	}

	/**
	 * Слушатель нажатия кнопок тиражей.
	 *
	 * @param {number} index Индекс в списке тиражей.
	 */
	onSelectedDrawHandler(index: number): void {
		this.sportprognozLotteryService.betData.selectedDraw = this.sportprognozLotteryService.activeDraws[index];
		this.updateUIPropertiesForCurrentDraw();

		this.sportprognozLotteryService.getProgramData()
			.subscribe();
	}

	/**
	 * Обработчик кнопки нажатия печати программы.
	 *
	 * @param {IPrintButtonAction} printData Данные для печати.
	 */
	onClickPrintProgramHandler(printData: IPrintButtonAction): void {
		this.sportprognozLotteryService.printProgram(printData.copiesCount);
	}

	/**
	 * Обработчик ситуации, когда по таймеру пропали все активные тиражи.
	 * Текущий выбранный тираж удаляется.
	 */
	onNoActiveDrawsHandler(): void {
		this.sportprognozLotteryService.betData.selectedDraw = undefined;
	}

	goToRegistryPage(): void {
		this.sportprognozLotteryService.betData.selectedDraw = {
			draw: {
				bet_sum: '0',
				code: this.sportprognozLotteryService.lotteryBarcode.substring(0, 3),
				num: this.sportprognozLotteryService.lotteryBarcode.substring(0, 3)
			}
		};
		this.sportprognozLotteryService.goToRegistryPage();
	}

	onChangeValue(bc: string): void {
		this.sportprognozLotteryService.lotteryBarcode = bc;
		const control = this.scannerForm.get('spBarcode');

		if (control) {
			control.setValue(bc); // Устанавливаем значение
			control.markAsDirty(); // Помечаем поле как "измененное"
			control.markAsTouched(); // Помечаем поле как "посетили"
			control.updateValueAndValidity(); // Принудительно запускаем валидацию
		}
	}

	onStateChanged(state: boolean): void {
		this.isCameraShown = state;
		this.userInputMode = this.isCameraShown ? UserInputMode.Scanner : UserInputMode.Manual;
		this.userInputStorage[this.appStoreService.operator.value.userId] = this.userInputMode;
		this.storageService.putSync(ApplicationAppId, [{
			key: 'user_input_mode',
			value: JSON.stringify(this.userInputStorage)
		}]);
	}

	/**
	 * Функция для отслеживания изменений в массиве программы.
	 * @param index Индекс элемента в массиве.
	 * @param item Элемент массива.
	 */
	trackByProgram = (index, item: ISportPrognozProgramItem) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Обновление номера и даты текущего тиража.
	 * @private
	 */
	private updateUIPropertiesForCurrentDraw(): void {
		if (!!this.sportprognozLotteryService.betData && !!this.sportprognozLotteryService.betData.selectedDraw) {
			this.currentDrawNum = this.sportprognozLotteryService.betData.selectedDraw.draw.num;
			this.currentDrawDate = new Date(this.sportprognozLotteryService.betData.selectedDraw.draw.sale_edate);
		}
	}

	private isPossibleCombination(decEncoded6Results: string): boolean {
		const nDecEncoded6Results = parseInt(decEncoded6Results, 10);
		const first6 = (nDecEncoded6Results >>> 0)
			.toString(2)
			.padStart(30, '0')
			.match(/.{5}/g);

		return first6.every((comb) => POSSIBLE.includes(comb));
	}

	private spBarcodeValidator(): ValidatorFn {
		return (control: AbstractControl): ValidationErrors | null => {
			const value = control.value;
			const drawNum = value.substring(0, 3);
			const draw = this.sportprognozLotteryService.activeDraws
				.find((ad) => ad.draw.num === drawNum);
			if (!draw) {
				return {
					spBarcodeInvalid: 'lottery.invalid-draw-number'
				};
			}

			// const check1 = this.isPossibleCombination(value.substring(3, 13));
			//
			// if (!check1) {
			// 	return {
			// 		spBarcodeInvalid: 'lottery.invalid-draw-number'
			// 	};
			// }
			//
			// const check2 = this.isPossibleCombination(value.substring(13, 23));
			//
			// if (!check2) {
			// 	return {
			// 		spBarcodeInvalid: 'lottery.invalid-draw-number'
			// 	};
			// }

			const sumOfDigits = value.substring(0, 23)
				.split('')
				.map((v) => +v)
				.reduce((acc, curr) => acc + curr, 0); // Суммирование цифр

			const crc = (sumOfDigits * 37 % 99)
				.toString()
				.padStart(2, '0');

			// Return error object if invalid, otherwise return null
			return crc === value.substring(23) ? null : { spBarcodeInvalid: 'lottery.invalid-draw-number' };
		};
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.sportprognozLotteryService.initBetData(BetDataSource.Manual);

		if (!this.sportprognozLotteryService.betData.selectedDraw) {
			this.sportprognozLotteryService.betData.selectedDraw = this.sportprognozLotteryService.activeDraws[0];
		}

		this.updateUIPropertiesForCurrentDraw();

		this.scannerForm = this.fb.group({
			spBarcode: [
				'',
				[
					Validators.required,
					Validators.minLength(25),
					Validators.maxLength(25),
					this.spBarcodeValidator()
				]
			]
		});

		const uInputStorage = this.storageService.getSync(ApplicationAppId, ['user_input_mode']);
		this.userInputStorage = uInputStorage && uInputStorage.data && (uInputStorage.data.length > 0) ?
			JSON.parse(uInputStorage.data[0].value) : {};
		this.userInputMode = this.userInputStorage[this.appStoreService.operator.value.userId] ?
			this.userInputStorage[this.appStoreService.operator.value.userId] : UserInputMode.Scanner;
		this.isCameraShown = this.userInputMode === UserInputMode.Scanner;
		this.appStoreService.showCameraComponent$$
			.pipe(
				takeUntil(this.unsubscribe$$)
			)
			.subscribe((showCameraComponent) => {
				if (showCameraComponent) {
					if (this.isCameraShown) {
						this.scannerInput.showScanner();
					}
				} else {
					this.scannerInput.hideScanner();
				}
			});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

	protected readonly BetDataSource = BetDataSource;
}
