import { Component, OnDestroy } from '@angular/core';
import { SportprognozLotteryService } from '@app/sportprognoz/services/sportprognoz-lottery.service';

/**
 * Главный компонент игры "Спортпрогноз".
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class SportprognozContentComponent implements  OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {SportprognozLotteryService} sportprognozLotteryService Сервис игры "Спортпрогноз".
	 */
	constructor(
		private readonly sportprognozLotteryService: SportprognozLotteryService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Вызывается при уничтожении компонента.
	 */
	ngOnDestroy(): void {
		this.sportprognozLotteryService.resetBetData();
	}

}
