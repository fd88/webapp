import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';
import { HttpService } from '@app/core/net/http/services/http.service';
import { SportprognozContentComponent } from '@app/sportprognoz/components/sportprognoz-content/sportprognoz-content.component';
import { SportprognozLotteryService } from '@app/sportprognoz/services/sportprognoz-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LogService } from '@app/core/net/ws/services/log/log.service';

describe('SportprognozContentComponent', () => {
	let component: SportprognozContentComponent;
	let fixture: ComponentFixture<SportprognozContentComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			declarations: [
				SportprognozContentComponent
			],
			providers: [
				DeclineWordPipe,
				SportprognozLotteryService,
				LotteriesService,
				HttpService,
				LogService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SportprognozContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
