import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { HttpService } from '@app/core/net/http/services/http.service';

import { ReportsService } from '@app/core/services/report/reports.service';
import { DatePipe } from '@angular/common';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { SportprognozResultsComponent } from '@app/sportprognoz/components/sportprognoz-results/sportprognoz-results.component';
import { IDrawing, IDrawingSPData, IDrawingSPDataEvent, IDrawingSPDataEventResult } from '@app/core/net/http/api/models/get-draw-results';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { LotteriesService } from '@app/core/services/lotteries.service';

describe('SportprognozResultsComponent', () => {
	let component: SportprognozResultsComponent;
	let fixture: ComponentFixture<SportprognozResultsComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				SharedModule,
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot({})
			],
			declarations: [
				SportprognozResultsComponent
			],
			providers: [
				GameResultsService,
				HttpService,


				ReportsService,
				DatePipe,
				MslCurrencyPipe,
				LogService,
				LotteriesService,
				AppStoreService,
				StorageService,
				TransactionService,
				PrintService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				TranslateService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SportprognozResultsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectedResultsHandler', () => {
		component.onSelectedResultsHandler(undefined);
		expect(component.viewData).toBeUndefined();

		const result = {
			"draw_code": 1516,
			"draw_name": "603",
			"drawing": [
				{
					"data": {
						"events": [
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Украина",
								"number": 1,
								"result": {
									"result": 4,
									"score": "1:3"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Бразилия",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Нигерия",
								"number": 2,
								"result": {
									"result": 4,
									"score": "1:3"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Таиланд",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2015",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Израиль",
								"number": 3,
								"result": {
									"result": 4,
									"score": "1:3"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Австрия",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Финляндия",
								"number": 4,
								"result": {
									"result": 4,
									"score": "1:3"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Польша",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Канада",
								"number": 5,
								"result": {
									"result": 4,
									"score": "1:3"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Швейцария",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Россия",
								"number": 6,
								"result": {
									"result": 2,
									"score": "1:0"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Корея",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Китай",
								"number": 7,
								"result": {
									"result": 2,
									"score": "2:0"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Молдова",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Англия",
								"number": 8,
								"result": {
									"result": 2,
									"score": "2:0"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Испания",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Италия",
								"number": 9,
								"result": {
									"result": 2,
									"score": "2:0"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Франция",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "США",
								"number": 10,
								"result": {
									"result": 1,
									"score": "1:1"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Германия",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Белоруссия",
								"number": 11,
								"result": {
									"result": 1,
									"score": "1:1"
								} as IDrawingSPDataEventResult,
								"secondTeam": "Латвия",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							},
							{
								"eventDate": "2019-03-07 15:37:13",
								"firstTeam": "Нидерланды",
								"number": 12,
								"result": {
									"result": 1,
									"score": "1:1",
								} as IDrawingSPDataEventResult,
								"secondTeam": "Литва",
								"t1": '30',
								"t2": '30',
								"tournament": "Чемпионат Мира 2019",
								"x": '40'
							}
						]
					},
					"extra_info": "Джекпот наступн.тиражу -  грн.",
					"name": "Основний розiграш",
					"win_cat": [
						{
							"name": "за 12:",
							"win_cnt": "0",
							"win_sum": ""
						},
						{
							"name": "за 11:",
							"win_cnt": "0",
							"win_sum": ""
						},
						{
							"name": "за 10:",
							"win_cnt": "0",
							"win_sum": ""
						}
					],
					"win_comb": "2 2 2 2 2 1 1 1 1 0 0 0"
				}
			],
			"drawing_date_begin": "2019-04-15 18:26:26",
			"drawing_date_end": "2020-04-15 18:31:25",
			"winning_date_end": "2020-04-15 18:31:25"
		};

		component.onSelectedResultsHandler(result);

		expect(component.viewData).toEqual({
			"drawNumber": "603",
			"drawDate": "2019-04-15 18:26:26",
			"extraInfo": "Джекпот наступн.тиражу -  грн.",
			"drawing": {
				"data": {
					"events": [
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Украина",
							"number": 1,
							"result": {
								"result": 4,
								"score": "1:3"
							},
							"secondTeam": "Бразилия",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Нигерия",
							"number": 2,
							"result": {
								"result": 4,
								"score": "1:3"
							},
							"secondTeam": "Таиланд",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2015",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Израиль",
							"number": 3,
							"result": {
								"result": 4,
								"score": "1:3"
							},
							"secondTeam": "Австрия",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Финляндия",
							"number": 4,
							"result": {
								"result": 4,
								"score": "1:3"
							},
							"secondTeam": "Польша",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Канада",
							"number": 5,
							"result": {
								"result": 4,
								"score": "1:3"
							},
							"secondTeam": "Швейцария",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Россия",
							"number": 6,
							"result": {
								"result": 2,
								"score": "1:0"
							},
							"secondTeam": "Корея",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Китай",
							"number": 7,
							"result": {
								"result": 2,
								"score": "2:0"
							},
							"secondTeam": "Молдова",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Англия",
							"number": 8,
							"result": {
								"result": 2,
								"score": "2:0"
							},
							"secondTeam": "Испания",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Италия",
							"number": 9,
							"result": {
								"result": 2,
								"score": "2:0"
							},
							"secondTeam": "Франция",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "США",
							"number": 10,
							"result": {
								"result": 1,
								"score": "1:1"
							},
							"secondTeam": "Германия",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Белоруссия",
							"number": 11,
							"result": {
								"result": 1,
								"score": "1:1"
							},
							"secondTeam": "Латвия",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						},
						{
							"eventDate": "2019-03-07 15:37:13",
							"firstTeam": "Нидерланды",
							"number": 12,
							"result": {
								"result": 1,
								"score": "1:1"
							},
							"secondTeam": "Литва",
							"t1": '30',
							"t2": '30',
							"tournament": "Чемпионат Мира 2019",
							"x": '40'
						} as IDrawingSPDataEvent
					] as  Array<IDrawingSPDataEvent>
				} as IDrawingSPData,
				"extra_info": "Джекпот наступн.тиражу -  грн.",
				"name": "Основний розiграш",
				"win_cat": [
					{
						"name": "за 12:",
						"win_cnt": "0",
						"win_sum": ""
					},
					{
						"name": "за 11:",
						"win_cnt": "0",
						"win_sum": ""
					},
					{
						"name": "за 10:",
						"win_cnt": "0",
						"win_sum": ""
					}
				],
				"win_comb": "2 2 2 2 2 1 1 1 1 0 0 0"
			} as IDrawing,
			"winCat": [
				{
					"name": "за 12:",
					"win_cnt": "0",
					"win_sum": ""
				},
				{
					"name": "за 11:",
					"win_cnt": "0",
					"win_sum": ""
				},
				{
					"name": "за 10:",
					"win_cnt": "0",
					"win_sum": ""
				}
			],
			"winComb": "2 2 2 2 2 1 1 1 1 0 0 0"
		});

		result.drawing[0].win_cat = undefined;
		result.drawing[0].win_comb = undefined;
		result.drawing[0].data = undefined;

		component.onSelectedResultsHandler(result);
		expect(component.participantsList).toEqual([]);

	});



});
