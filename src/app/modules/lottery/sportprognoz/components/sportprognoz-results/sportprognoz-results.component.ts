import { Component } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDrawing, IDrawingResult, IWinCat } from '@app/core/net/http/api/models/get-draw-results';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { addWinCatRows, IAbstractViewResults, IResultPrintItem, PrintResultTypes } from '@app/core/services/results/results-utils';
import { IParticipantsListItem, ParticipantsListMode } from '@app/shared/components/participants-list/participants-list.component';

/**
 * Интерфейс результатов для игры "Спортпрогноз".
 */
interface ISportprognozResults extends IAbstractViewResults {
	/**
	 * Результат розыгрыша.
	 */
	drawing: IDrawing;
	/**
	 * Выигрышные категории.
	 */
	winCat: Array<IWinCat>;
	/**
	 * Выигрышная комбинация.
	 */
	winComb: string;
	/**
	 * Дополнительная информация.
	 */
	extraInfo: string;
}

/**
 * Компонент для отображения результатов игры "Спортпрогноз".
 */
@Component({
	selector: 'app-sportprognoz-results',
	templateUrl: './sportprognoz-results.component.html',
	styleUrls: ['./sportprognoz-results.component.scss']
})
export class SportprognozResultsComponent  {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Режим отображения списка участников.
	 */
	readonly ParticipantsListMode = ParticipantsListMode;

	/**
	 * Данные для отображения результатов.
	 */
	viewData: ISportprognozResults;

	/**
	 * Данные для печати результатов.
	 */
	printData: Array<IResultPrintItem>;

	/**
	 * Список участников.
	 */
	participantsList: Array<IParticipantsListItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GameResultsService} gameResultsService Сервис для работы с результатами игр.
	 */
	constructor(
		readonly gameResultsService: GameResultsService
	) {}

	/**
	 * Обработчик нажатия выбора результатов по тиражу.
	 *
	 * @param {IDrawingResult} result Результат розыгрыша по тиражу.
	 */
	onSelectedResultsHandler(result: IDrawingResult): void {
		if (!result) {
			this.viewData = undefined;
			this.printData = undefined;
			this.participantsList = undefined;

			return;
		}

		const drawNumber = result.draw_name;
		const drawDate = result.drawing_date_begin;
		const extraInfo = result.drawing[0].extra_info;

		const drawing = result.drawing[0];

		let winCat: Array<IWinCat>;
		if (Array.isArray(result.drawing[0].win_cat)) {
			winCat = result.drawing[0].win_cat;
		}

		let winComb: string;
		if (result.drawing[0].win_comb) {
			winComb = result.drawing[0].win_comb;
		}

		this.viewData = {drawNumber, drawDate, extraInfo, drawing, winCat, winComb};
		this.printData = this.printDataParser();

		if (result.drawing[0].data && Array.isArray(result.drawing[0].data.events)) {
			this.participantsList = result.drawing[0].data.events.map(item => {
				let p: IParticipantsListItem;
				p = {
					gameDate: new Date(item.eventDate),
					position: item.number,
					teamName1: item.firstTeam,
					teamName2: item.secondTeam,
					tournament: item.tournament,
					outcome_forecast_1: item.t1,
					outcome_forecast_2: item.t2,
					outcome_forecast_X: item.x,
					score: item.result && item.result.score ? item.result.score : undefined,
					result: item.result && item.result.result ? `${Math.floor(item.result.result / 2)}` : undefined
				};

				return p;
			});
		} else {
			this.participantsList = [];
		}
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Функция-парсер данных для печати.
	 */
	private printDataParser(): Array<IResultPrintItem> {
		const result = [
			{key: PrintResultTypes.LotteryName, value: ['lottery.sportprognoz.game_name']},
			{key: PrintResultTypes.DrawNumber, value: [this.viewData.drawNumber]},
			{key: PrintResultTypes.DrawDate, value: [this.viewData.drawDate]}
		];

		result.push({key: PrintResultTypes.Line, value: undefined});
		result.push({key: PrintResultTypes.OneColumnTable, value: [this.viewData.winComb]});

		addWinCatRows(this.viewData.drawing, result, true, false, {
			key: PrintResultTypes.TwoColumnTable,
			value: ['lottery.category', 'lottery.win']
		});

		result.push({key: PrintResultTypes.Line, value: undefined});
		result.push({key: PrintResultTypes.OneColumnTable, value: [this.viewData.extraInfo]});

		return result;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
}
