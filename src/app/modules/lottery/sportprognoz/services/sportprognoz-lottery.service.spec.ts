import {TestBed} from '@angular/core/testing';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {HttpService} from '@app/core/net/http/services/http.service';
import {HttpServiceStub} from '@app/core/net/http/services/http.service.spec';
import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {DialogContainerServiceStub} from '@app/core/dialog/services/dialog-container.service.spec';
import {PrintService} from '@app/core/net/ws/services/print/print.service';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {Injector} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

import {BetDataSource} from '@app/core/game/base-game.service';
import {ApplicationAppId} from '@app/core/services/store/settings';
import {StorageGetResp} from '@app/core/net/ws/api/models/storage/storage-get';
import {IResponse} from '@app/core/net/ws/api/types';
import {LotteryGameCode} from '@app/core/configuration/lotteries';
import {EsapActions} from '@app/core/configuration/esap';
import {calculateAmount, SportprognozLotteryService} from '@app/sportprognoz/services/sportprognoz-lottery.service';
import {LogOutService} from "@app/logout/services/log-out.service";
import {LogOutServiceStub} from "@app/logout/services/log-out.service.spec";
import {DRAWS_FOR_GAME_SPORTPROGNOZ} from "../../../features/mocks/sportprognoz-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";

describe('SportprognozLotteryService', () => {
	const couponData = [{
		'outcomes': [
			{o: 3, s: 2, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0},
			{o: 2, s: 0, b: 0}
		]
	}];
	const PROGRAM_REPORT_ID = '14';
	const STORAGE_KEY_PREFIX = `sportprogn_program_${PROGRAM_REPORT_ID}_`;
	const STORAGE_KEY = `${STORAGE_KEY_PREFIX}_${DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0].draw.num}`;
	const programData = [{
		key: STORAGE_KEY,
		value: `3|${DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0].draw.num}|11.04.2019|12:42|12.09.2019|12:42|12.09.2019|12:42|12.09.2019|12:42|12.09.2019|12:42|21.04.2020|12:42\n21|1|Johnny Cage|Jax|q|11.04.19|12:42|30|40|30\n21|2|Kano|Kitana|a|11.04.19|12:42|30|40|30\n21|3|Liu Kang|Kung Lao|z|11.04.19|12:42|30|40|30\n21|4|Raiden|Mileena|w|11.04.19|12:42|30|40|30\n21|5|Scorpion|Noob Saibot|s|11.04.19|12:42|30|40|30\n21|6|Sonya Blade|Shao Kahn|x|11.04.19|12:42|30|40|30\n21|7|Sub-Zero|Smoke|e|11.04.19|12:42|30|40|30\n21|8|Goro|Chameleon|d|11.04.19|12:42|30|40|30\n21|9|Shang Tsung|Cyrax|c|11.04.19|12:42|30|40|30\n21|10|Reptile|Ermac|r|11.04.19|12:42|30|40|30\n21|11|Baraka|Kabal|f|11.04.19|12:42|30|40|30\n21|12|Jade|Khameleon|v|11.04.19|12:42|30|40|30\n22|12:42 12.09.2019\n`
	}];

	let service: SportprognozLotteryService | any;
	let appStoreService: AppStoreService;
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				RouterTestingModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,

				PrintService,
				Injector,
				SportprognozLotteryService,
				{
					provide: LogOutService,
					useValue: LogOutServiceStub,
				},
				{
					provide: HttpService,
					useValue: new HttpServiceStub()
				},
				AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				}
				// {
				// 	provide: TransactionService,
				// 	useValue: new TransactionServiceStub()
				// }
			]
		});

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		TestBed.inject(LogService);
		service = TestBed.inject(SportprognozLotteryService);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Sportprognoz, DRAWS_FOR_GAME_SPORTPROGNOZ.lottery);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test parseProgramData method', () => {
		const rawProgram = '3|604|11.04.2019|12:42|12.09.2019|12:42|12.09.2019|12:42|12.09.2019|12:42|12.09.2019|12:42|21.04.2020|12:42\n' +
			'21|1|Johnny Cage|Jax|q|11.04.19|12:42|30|40|30\n' +
			'21|2|Kano|Kitana|a|11.04.19|12:42|30|40|30\n' +
			'21|3|Liu Kang|Kung Lao|z|11.04.19|12:42|30|40|30\n' +
			'21|4|Raiden|Mileena|w|11.04.19|12:42|30|40|30\n' +
			'21|5|Scorpion|Noob Saibot|s|11.04.19|12:42|30|40|30\n' +
			'21|6|Sonya Blade|Shao Kahn|x|11.04.19|12:42|30|40|30\n' +
			'21|7|Sub-Zero|Smoke|e|11.04.19|12:42|30|40|30\n' +
			'21|8|Goro|Chameleon|d|11.04.19|12:42|30|40|30\n' +
			'21|9|Shang Tsung|Cyrax|c|11.04.19|12:42|30|40|30\n' +
			'21|10|Reptile|Ermac|r|11.04.19|12:42|30|40|30\n' +
			'21|11|Baraka|Kabal|f|11.04.19|12:42|30|40|30\n' +
			'21|12|Jade|Khameleon|v|11.04.19|12:42|30|40|30\n' +
			'22|12:42 12.09.2019';

		const expectedTeams = [{"type":"21","position":"1","teamName1":"Johnny Cage","teamName2":"Jax","tournament":"q","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"2","teamName1":"Kano","teamName2":"Kitana","tournament":"a","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"3","teamName1":"Liu Kang","teamName2":"Kung Lao","tournament":"z","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"4","teamName1":"Raiden","teamName2":"Mileena","tournament":"w","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"5","teamName1":"Scorpion","teamName2":"Noob Saibot","tournament":"s","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"6","teamName1":"Sonya Blade","teamName2":"Shao Kahn","tournament":"x","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"7","teamName1":"Sub-Zero","teamName2":"Smoke","tournament":"e","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"8","teamName1":"Goro","teamName2":"Chameleon","tournament":"d","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"9","teamName1":"Shang Tsung","teamName2":"Cyrax","tournament":"c","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"10","teamName1":"Reptile","teamName2":"Ermac","tournament":"r","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"11","teamName1":"Baraka","teamName2":"Kabal","tournament":"f","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"},{"type":"21","position":"12","teamName1":"Jade","teamName2":"Khameleon","tournament":"v","date":"11.04.19","time":"12:42","outcome_forecast_1":"30","outcome_forecast_X":"40","outcome_forecast_2":"30"}];
		service.parseProgramData(rawProgram);
		expect(JSON.stringify(service.programData.teams)).toEqual(JSON.stringify(expectedTeams));
	});

	it('test calculateAmount function', () => {
		const betsList = [...couponData];
		const betSumma = 5;
		expect(calculateAmount(betsList, betSumma)).toEqual(10);
	});

	it('test refreshBets method', () => {
		service.initBetData(BetDataSource.Scanner);
		service.betData.drawCount = 1;
		service.betData.betsList = [...couponData];
		service.betData.selectedDraw = DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0];
		service.refreshBets();
		expect(service.amount).toEqual(10);
	});

	it('test #1 for getProgramData method', () => {
		service.initBetData(BetDataSource.Scanner);
		service.betData.selectedDraw = undefined;
		service.getProgramData().subscribe(result => {
			expect(result).toBeFalsy();
		});
	});

	it('test #2 for getProgramData method', () => {
		service.initBetData(BetDataSource.Scanner);
		service.betData.drawCount = 1;
		service.betData.betsList = [...couponData];
		service.betData.selectedDraw = DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0];

		service.storageService.get = (): Promise<IResponse> => {
			return new Promise((resolve) => {
				resolve({"data": programData,"requestId":"1565796290184009","errorCode":0,"errorDesc":"ok"} as StorageGetResp);
			});
		};

		service.getProgramData().subscribe(result => {
			expect(result).toBeTruthy();
		});
	});

	it('test #3 for getProgramData method', () => {
		service.initBetData(BetDataSource.Scanner);
		service.betData.drawCount = 1;
		service.betData.betsList = [...couponData];
		service.betData.selectedDraw = DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0];

		service.storageService.get = (): Promise<IResponse> => {
			return new Promise((resolve, reject) => {
				reject({code: 1000});
			});
		};

		service.transactionService.setLastUnCanceled = () => {};
		const tmpMap = new Map();
		tmpMap.set(EsapActions.GetReportData, 'ReportSrvDev');
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(LotteryGameCode.Undefined, tmpMap);
		service.getProgramData().subscribe(result => {
			expect(result).toBeFalsy();
		});
	});

	// вот тут
	it('test #4 for getProgramData method', () => {
		service.initBetData(BetDataSource.Scanner);
		service.betData.drawCount = 1;
		service.betData.betsList = [...couponData];
		service.betData.selectedDraw = DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0];

		service.storageService.get = (): Promise<IResponse> => {
			return new Promise((resolve) => {
				resolve({"data": [],"requestId":"1565796290184009","errorCode":0,"errorDesc":"ok"} as StorageGetResp);
			});
		};

		const tmpMap = new Map();
		tmpMap.set(EsapActions.GetReportData, 'ReportSrvDev');
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(LotteryGameCode.Undefined, tmpMap);

		service.getProgramData().subscribe(result => {
			expect(result).toBeFalsy();
		});
	});

	it('test saveProgramDataToStorage method', () => {
		const saveProgramDataToStorageSpy = spyOn(service, 'saveProgramDataToStorage').and.callThrough();
		service.storageService.put = () => new Promise(resolve => resolve(null));
		service.saveProgramDataToStorage(programData);
		expect(saveProgramDataToStorageSpy).toHaveBeenCalled();
	});

	it('test #2 saveProgramDataToStorage method', () => {
		service.storageService.put = (): Promise<IResponse> => {
				return new Promise((resolve, reject) => reject({code: 1000}));
			};
		service.saveProgramDataToStorage(programData);
		service.storageService.get(ApplicationAppId, [STORAGE_KEY], null, 10000, 5)
			.then(storageGetResp => {
				const resp = storageGetResp as StorageGetResp;
				expect(resp.data && resp.data.length > 0).toBeFalsy();
			});

	});

	it('test #1 for printProgram method', () => {
		const printProgramSpy = spyOn(service, 'printProgram').and.callThrough();
		service.printProgram(1);
		expect(printProgramSpy).toHaveBeenCalled();
	});

	it('test #2 for printProgram method',  () => {
		const printProgramSpy = spyOn(service, 'printProgram').and.callThrough();
		service.programData = [...programData];
		service.reportsService = {
			printXMLContentFromReport: (): void => {}
		};
		service.printProgram(1);
		expect(printProgramSpy).toHaveBeenCalled();
	});

	it('test #2 buyLottery method', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();
		service.initBetData(BetDataSource.Scanner);
		service.betData.drawCount = 1;
		service.betData.betsList = [...couponData];
		service.betData.selectedDraw = DRAWS_FOR_GAME_SPORTPROGNOZ.lottery.draws[0];
		service.refreshBets();
		service.printService.isReady = () => true;

		const tmpMap = new Map();
		tmpMap.set(EsapActions.SportPrognozRegBet, 'SPRegSrvDev');
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(LotteryGameCode.Sportprognoz, tmpMap);
		service.transactionService.executeRequest = (): Promise<void> => new Promise(resolve => resolve());
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});
});
