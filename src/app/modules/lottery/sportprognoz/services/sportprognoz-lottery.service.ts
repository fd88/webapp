import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import {from, Observable, timer} from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { URL_SPORTPROGNOZ } from '@app/util/route-utils';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { IError, NetError } from '@app/core/error/types';
import { GetReportDataReq, GetReportDataResp } from '@app/core/net/http/api/models/get-report-data';
import { ISportPrognozBet, SportPrognozRegBetReq, SportPrognozRegBetResp } from '@app/core/net/http/api/models/sportprognoz-reg-bet';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { HttpService } from '@app/core/net/http/services/http.service';
import { KeyValue } from '@app/core/net/ws/api/types';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApplicationAppId } from '@app/core/services/store/settings';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { ReportsService } from '@app/core/services/report/reports.service';
import { ISportPrognozProgramData } from '../interfaces/isport-prognoz-program-data';
import { ISportPrognozProgramItem } from '../interfaces/isport-prognoz-program-item';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { StorageGetResp } from '@app/core/net/ws/api/models/storage/storage-get';
import { BaseGameService, IBetDataItem } from '@app/core/game/base-game.service';
import { LogOutService } from '@app/logout/services/log-out.service';
import { PrintData } from "@app/core/net/ws/api/models/print/print-models";
import html2canvas from "html2canvas";
import {delay, tap} from "rxjs/operators";
import {concatMap} from "rxjs/internal/operators";

/**
 * Модель с данными по игре.
 */
export interface ISportprognozBetDataItem extends IBetDataItem {

	/**
	 * Список ставок.
	 */
	betsList: Array<ISportPrognozBet>;

	/**
	 * Выбранный тираж.
	 */
	selectedDraw: UpdateDrawInfoDraws;

}

// -----------------------------
//  Constants
// -----------------------------

/**
 * Функция парсинга части программы турниров
 * @param fp Ключ для парсинга информации
 * @param arr Массив строк программы турниров
 */
const parse = (fp: string, arr: Array<string>): Array<any> => {
	let result: Array<string> = [];
	const str: string = arr.find(f => f.indexOf(fp) === 0);
	if (str) {
		result = str.split('|')
			.map(m => {
				return m;
			});
	}

	return result;
};

/**
 * Функция вычисления полной суммы ставки
 * @param betsList Список ставок
 * @param betSumma Сумма одной ставки
 */
export const calculateAmount = (betsList: Array<ISportPrognozBet>, betSumma: number): number => {
	const BLOCK1_SUM = [1, 3,  5,  7,  9, 11, 13,  15,  17,  19,  21, 23,  25];
	const BLOCK2_SUM = [1, 1,  9, 19, 33, 51, 73, 99, 129, 163, 201, 243, 289];

	let amount = 0;
	betsList.forEach(bet => {
		if (bet.outcomes.find(p => p.s > 0) || bet.outcomes.find(p => p.b > 0)) {
			// sys
			const s2count = bet.outcomes.filter(p => p.s === 2).length;
			const s3count = bet.outcomes.filter(p => p.s === 3).length;
			const totalSystemVariants = Math.pow(2, s2count) * Math.pow(3, s3count);

			// block
			const b1count = bet.outcomes.filter(p => p.b === 1).length;
			const b2count = bet.outcomes.filter(p => p.b === 2).length;
			const block1Variants = BLOCK1_SUM[b1count];
			const block2Variants = BLOCK2_SUM[b2count];
			let totalBlockVariants = block1Variants * block2Variants;
			if (totalBlockVariants === 0) {
				totalBlockVariants = 1;
			}

			amount += totalBlockVariants * totalSystemVariants * betSumma;
		} else {
			amount += betSumma;
		}
	});

	return amount;
};

/**
 * Идентификатор программы турниров в отчетах
 */
const PROGRAM_REPORT_ID = '14';

/**
 * Часть ключа для сохранения в storage service
 */
const STORAGE_KEY_PREFIX = `sportprogn_program_${PROGRAM_REPORT_ID}_`;

/**
 * Возвращает модель с начальными данными для игры "Спортпрогноз".
 * Вызывается в момент перехода на форму ручного ввода или ввод с купона.
 */
const initBetDataFn = (): ISportprognozBetDataItem => {
	return {
		drawCount: 1,
		selectedDraw: undefined,
		betsList: []
	};
};

/**
 * Сервис лотерей "Спортпрогноз".
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей и взаимодействия с ЦС.
 */
@Injectable()
export class SportprognozLotteryService extends BaseGameService<ISportprognozBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * URL игры.
	 */
	readonly gameUrl = URL_SPORTPROGNOZ;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.sportprognoz.game_name';

	/**
	 * Программа выбранного тиража.
	 */
	programData: ISportPrognozProgramData;

	lotteryBarcode = '';

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {HttpService} httpService Сервис для работы с HTTP.
	 * @param {ReportsService} reportsService Сервис отчетов.
	 * @param {StorageService} storageService Сервис для работы с localStorage.
	 * @param {TranslateService} translateService Сервис локализации.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект для работы с адресной строкой.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		private readonly httpService: HttpService,
		private readonly reportsService: ReportsService,
		private readonly storageService: StorageService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();
		this.currentGameCode = LotteryGameCode.Sportprognoz;
		this.currentGroupCode = LotteriesGroupCode.Regular;
		this.initialBetDataFactory = initBetDataFn;
	}

	/**
	 * Пропарсить данные отчета и построить модель программы.
	 *
	 * @param content Содержимое программы.
	 */
	parseProgramData(content: string): void {

		const arr = content.split('\n');

		// номер тиража
		const p3 = parse('3|', arr);

		// джекпот
		const p20 = parse('20|', arr);

		// дата окончания приема ставок
		const p22 = parse('22|', arr);

		// примечание
		const p23 = parse('23|', arr);

		// пропарсить тимы (21 параметр)
		const p21: Array<ISportPrognozProgramItem> = arr
			.filter(f => f.startsWith('21|'))
			.map(m => {
				const itemArr = m.split('|');

				return {
					type: itemArr[0],
					position: itemArr[1],
					teamName1: itemArr[2],
					teamName2: itemArr[3],
					tournament: itemArr[4],
					date: itemArr[5],
					time: itemArr[6],
					outcome_forecast_1: itemArr[7],
					outcome_forecast_X: itemArr[8],
					outcome_forecast_2: itemArr[9]
				};
			})
			.sort((a, b) => +a.position - +b.position);

		this.programData = {
			report: content,
			drawNumber: +p3[1],
			teams: p21,
			jackpot: p20[1],
			betsEnds: p22[1],
			note: p23[1]
		};

		// для печати заголовка на термопринтере добавлен 0й параметр
		this.programData.report = `0|${p3[1]}\n${this.programData.report}`;

		Logger.Log.i('SportprognozLotteryService', `parsed data:\np3: %s\np20: %s\np21: %s\np22: %s`, p3, p20, p21, p22)
			.console();
	}

	/**
	 * Распечатать программу на термо-принтере.
	 * @param cpCount Количество копий.
	 */
	printProgram(cpCount: number): void {
		if (this.printService.isReady()) {
			if (!!this.programData && !!this.programData.report) {

				timer(200)
					.pipe(
						tap(() => {
							this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.report_printing_wait_info');
						}),
						delay(200),
						concatMap(() => {
							const element = document.getElementById('receipt');
							return from(html2canvas(element));
						}),
						concatMap((canvas) => {
							const pngBase64 = canvas.toDataURL('image/png');
							const printData = {
								control: 'image',
								data: pngBase64.substring(22),
								key: `program-${Date.now()}`
							};
							return from(this.printService.printDocument([printData] as Array<PrintData>, 30000, cpCount));
						})
					)
					.subscribe(() => {
						this.dialogInfoService.hideActive();
					});
			} else {
				Logger.Log.e('SportprognozLotteryService', `can't print program, programData is empty`)
					.console();

				// TODO do we need to show any dialog?
			}
		} else {
			Logger.Log.i('LoyaltyCardInfo', 'onPrint -> printer not ready')
				.console();

			this.dialogInfoService.showOneButtonInfo('dialog.attention', 'dialog.printer_not_ready_info', {
				text: 'dialog.dialog_button_continue'
			});
		}
	}

	/**
	 * Запросить отчет для постройки программы тиража из Storage-сервиса.
	 *
	 * @returns {Observable<boolean>} Наблюдаемое значение, с значением true, если данные получены или false - ошибка.
	 */
	getProgramData(): Observable<boolean> {
		return new Observable<boolean>(result => {
			// проверить индекс тиража
			if (!this.betData.selectedDraw) {
				result.next(false);
				result.complete();
			} else {
				// получить текущий тираж
				const storageKey = STORAGE_KEY_PREFIX + this.betData.selectedDraw.draw.num;
				const storageKeys: Array<string> = [storageKey];

				Logger.Log.i('SportprognozLotteryService', `load program data from storage: ${storageKey}, %s`, storageKeys)
					.console();

				this.storageService.get(ApplicationAppId, storageKeys, null, 10000, 5)
					.then(storageGetResp => {
						const resp = storageGetResp as StorageGetResp;
						Logger.Log.i('SportprognozLotteryService', `program data was loaded from storage: %s`, resp)
							.console();

						if (resp.data && resp.data.length > 0) {
							this.parseProgramData(resp.data[0].value);
						} else {
							throw new Error('processParamTag, flowStack is empty');
						}

						result.next(true);
						result.complete();
					})
					.catch((error: IError) => {
						Logger.Log.i('SportprognozLotteryService',
							`not found program in the storage, will make ESAP request: %s`, error)
							.console();

						this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.program_loading_wait');

						// загрузить программу из отчетов
						this.transactionService.setLastUnCanceled();
						const request = new GetReportDataReq(this.appStoreService, PROGRAM_REPORT_ID, this.betData.selectedDraw.draw.num);

						this.httpService.sendApi(request)
							.then((response: GetReportDataResp) => {
								Logger.Log.i('SportprognozLotteryService', `load data from ESAP response: %s`, response)
									.console();

								const data = [{
									key: storageKey,
									value: response.report
								}];

								this.saveProgramDataToStorage(data);

								Logger.Log.i('SportprognozLotteryService', `save loaded data to storage: %s`, data)
									.console();

								this.parseProgramData(response.report);
								this.dialogInfoService.hideAll();

								result.next(true);
								result.complete();
							})
							.catch((errorApi: IError) => {
								Logger.Log.e('SportprognozLotteryService', `requestProgramData(): %s`, errorApi)
									.console();

								this.dialogInfoService.hideAll();

								this.programData = undefined;
								this.dialogInfoService.showOneButtonError(errorApi as NetError, {
									click: () => {
										// TODO: add handler?
										result.next(false);
										result.complete();
									},
									text: 'dialog.dialog_button_continue'
								});
							});
					});
			}
		});
	}

	/**
	 * Сохранить данные по программе в хранилище.
	 *
	 * @param {Array<KeyValue>} data Данные, которые будут сохранены.
	 */
	saveProgramDataToStorage(data: Array<KeyValue>): void {
		this.storageService.put(ApplicationAppId, data, 10000, 5)
			.then(() => {
				Logger.Log.i('SportprognozLotteryService', `program was stored successfully`)
					.console();
			})
			.catch((error: IError) => {
				Logger.Log.e('SportprognozLotteryService', `can't save data in store: ${error.code}, ${error.message}`)
					.console();
			});
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновить ставки.
	 */
	refreshBets(): void {
		let amount = 0;
		if (this.betData.selectedDraw) {
			const betSumma = parseFloat(this.betData.selectedDraw.draw.bet_sum);
			amount = calculateAmount(this.betData.betsList, betSumma);
		}

		this.amount = amount;
	}

	/**
	 * Купить лотерею.
	 */
	buyLottery(): void {
		Logger.Log.i('SportprognozLotteryService', 'buyLottery -> start buying Sportprognoz lottery')
			.console();

		const request = new SportPrognozRegBetReq(this.appStoreService, this.lotteryBarcode);
		this.transactionService.buyLottery(SportPrognozRegBetResp, LotteryGameCode.Sportprognoz, request);
	}
}
