import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { URL_INIT, URL_REGISTRY, URL_RESULTS } from '@app/util/route-utils';

import { AuthGuard } from '@app/core/guards/auth.guard';


import { SportprognozRegistryResolver } from '@app/sportprognoz/sportprognoz-registry-resolver';
import { SportprognozInitComponent } from '@app/sportprognoz/components/sportprognoz-lottery-init/sportprognoz-init.component';
import { SportprognozResultsComponent } from '@app/sportprognoz/components/sportprognoz-results/sportprognoz-results.component';
import { SportprognozContentComponent } from '@app/sportprognoz/components/sportprognoz-content/sportprognoz-content.component';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Спортпрогноз".
 */
const routes: Routes = [
	{
		path: '',
		component: SportprognozContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: URL_INIT,
				component: SportprognozInitComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: SportprognozRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_RESULTS,
				component: SportprognozResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}
];

/**
 * Модуль маршрутизации лотереи "Спортпрогноз".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		SportprognozRegistryResolver
	]
})
export class SportprognozRoutingModule {}
