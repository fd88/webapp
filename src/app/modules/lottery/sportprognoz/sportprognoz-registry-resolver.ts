import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SportprognozLotteryService } from '@app/sportprognoz/services/sportprognoz-lottery.service';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';

/**
 * Резолвер данных для отображения информации о ручной ставке в "Спортпрогноз".
 */
@Injectable()
export class SportprognozRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {SportprognozLotteryService} sportprognozLotteryService Сервис игры "Спортпрогноз".
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 */
	constructor(
		private readonly sportprognozLotteryService: SportprognozLotteryService,
		private readonly translateService: TranslateService
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		const draw = this.sportprognozLotteryService.betData.selectedDraw.draw;
		const betSumma = parseFloat(draw.bet_sum);
		const drawNum = draw.num;
		const checkInfoRowList = [
			{
				langKeyName: 'lottery.selected_draw',
				value: `${drawNum}`
			},
			{
				langKeyName: 'lottery.bets_count',
				value: '1' // `${this.sportprognozLotteryService.betData.betsList.length}`
			},
			// {
			// 	langKeyName: 'lottery.variants_count',
			// 	value: `${this.sportprognozLotteryService.amount / betSumma}`
			// },
			// {
			// 	langKeyName: 'lottery.sum',
			// 	value: this.sportprognozLotteryService.formattedAmount,
			// 	redColor: true,
			// 	valueIsCurrency: true
			// }
		];

		return {
			checkInfoRowList,
			baseGameService: this.sportprognozLotteryService
		};
	}

}
