import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';

import { SportprognozLotteryService } from '@app/sportprognoz/services/sportprognoz-lottery.service';

import { SportprognozRoutingModule } from '@app/sportprognoz/sportprognoz-routing.module';
import { SportprognozInitComponent } from '@app/sportprognoz/components/sportprognoz-lottery-init/sportprognoz-init.component';
import { SportprognozResultsComponent } from '@app/sportprognoz/components/sportprognoz-results/sportprognoz-results.component';
import { SportprognozContentComponent } from '@app/sportprognoz/components/sportprognoz-content/sportprognoz-content.component';
import {ReactiveFormsModule} from "@angular/forms";
import {CameraModule} from "../../features/camera/camera.module";

/**
 * Модуль лотереи "Спортпрогноз".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        SportprognozRoutingModule,
        ReactiveFormsModule,
        CameraModule
    ],
	declarations: [
		SportprognozContentComponent,
		SportprognozInitComponent,
		SportprognozResultsComponent
	],
	providers: [
		SportprognozLotteryService
	]
})
export class SportprognozModule {}
