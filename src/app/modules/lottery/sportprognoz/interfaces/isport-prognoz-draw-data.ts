/**
 * Интерфейс модели секции номера тиража.
 */
export interface ISportprognozDrawData {

	/**
	 * Признак наличия любых меток.
	 */
	hasMarks: boolean;

	/**
	 * Признак наличия всех корректных меток.
	 */
	hasAllMarks: boolean;

	/**
	 * Пропарсеный номер тиража с купона.
	 */
	drawNumber: number;

	/**
	 * Признак существующего номера тиража на терминале.
	 */
	existingDrawNumber: boolean;
}
