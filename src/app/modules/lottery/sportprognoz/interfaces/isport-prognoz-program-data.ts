import { ISportPrognozProgramItem } from './isport-prognoz-program-item';

/**
 * Модель программы тиража.
 */
export interface ISportPrognozProgramData {
	/**
	 * Программа в виде строки.
	 */
	report: string;
	/**
	 * Номер тиража.
	 */
	drawNumber: number;
	/**
	 * Список соревнующихся команд.
	 */
	teams: Array<ISportPrognozProgramItem>;
	/**
	 * Джекпот.
	 */
	jackpot?: string;
	/**
	 * Дата окончания приема ставок.
	 */
	betsEnds: string;
	/**
	 * Примечание
	 */
	note?: string;
}
