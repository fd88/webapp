/**
 * Интерфейс модели с анализом одной ставки (секции).
 */
export interface ISportprognozBetBoxData {

	/**
	 * Признак наличия пустых строк в секции.
	 */
	hasEmptyRows: boolean;

	/**
	 * Признак наличия более одной метки в строке.
	 */
	hasMultiMarksRows: boolean;

	/**
	 * Признак корректности заполнения системной ставки на 2.
	 */
	hasCorrectSystem2: boolean;

	/**
	 * Признак корректности заполнения системной ставки на 3.
	 */
	hasCorrectSystem3: boolean;

	/**
	 * Массив номеров строк по системе 2.
	 */
	system2Marks: Array<number>;

	/**
	 * Массив номеров строк по системе 3.
	 */
	system3Marks: Array<number>;

	/**
	 * Блок с отметками №1.
	 */
	blockMarks1: Array<number>;

	/**
	 * Блок с отметками №2.
	 */
	blockMarks2: Array<number>;

	/**
	 * Признак корректности заполнения блока №1.
	 */
	hasCorrectBlock1: boolean;

	/**
	 * Признак корректности заполнения блока №2.
	 */
	hasCorrectBlock2: boolean;

	/**
	 * Признак наличия в системной секции (№1) меток (любых) с блоками или системами.
	 * Для остальных - <code>false</code>
	 */
	hasSystemOrBlockMarks: boolean;
}
