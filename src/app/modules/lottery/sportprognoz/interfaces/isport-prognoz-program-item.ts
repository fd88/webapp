/**
 * Модель элемента тиража в программе {@link ISportPrognozProgramData}.
 */
export interface ISportPrognozProgramItem {
	/**
	 * Тип блока данных в структуре отчета.
	 */
	type: string;

	/**
	 * Номер позиции в программе
	 */
	position: string;

	/**
	 * Участник 1.
	 */
	teamName1: string;

	/**
	 * Участник 2.
	 */
	teamName2: string;

	/**
	 * Тернир, лига и т.д.
	 */
	tournament: string;

	/**
	 * Дата проведения события.
	 */
	date: string;

	/**
	 * Время проведения события.
	 */
	time: string;

	/**
	 * Прогноз победы 1й команды.
	 */
	outcome_forecast_1: string;

	/**
	 * Прогноз ничьей.
	 */
	outcome_forecast_X: string;

	/**
	 * Прогноз победы 2й команды.
	 */
	outcome_forecast_2: string;
}
