import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { URL_INIT, URL_REGISTRY } from '@app/util/route-utils';

import { AuthGuard } from '@app/core/guards/auth.guard';


import { TmlbmlRegistryResolver } from '@app/tmlbml/tmlbml-registry-resolver';
import { TmlBmlLotteryContentComponent } from '@app/tmlbml/components/tmlbml-lottery-content/tmlbml-lottery-content.component';
import { TmlBmlLotteryInitComponent } from '@app/tmlbml/components/tmlbml-lottery-init/tmlbml-lottery-init.component';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Стирачки".
 */
const routes: Routes = [{
	path: ``,
	component: TmlBmlLotteryContentComponent,
	canActivate: [
		AuthGuard
	],
	canDeactivate: [
		NavigationGuard
	],
	children: [
		{
			path: URL_INIT,
			component: TmlBmlLotteryInitComponent,
			canActivate: [
				AuthGuard
			],
			canDeactivate: [
				NavigationGuard
			]
		},
		{
			path: URL_REGISTRY,
			component: CheckInformationComponent,
			resolve: {
				registry: TmlbmlRegistryResolver
			},
			canActivate: [
				AuthGuard
			],
			canDeactivate: [
				NavigationGuard
			]
		}
	]
}];

/**
 * Модуль маршрутизации лотереи "Стирачки".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		TmlbmlRegistryResolver
	]
})
export class TmlBmlRoutingModule {}
