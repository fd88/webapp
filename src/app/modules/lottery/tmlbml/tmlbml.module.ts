import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';
import { TmlBmlInputModule } from '@app/tml-bml-input/tml-bml-input.module';
import { TmlBmlRoutingModule } from '@app/tmlbml/tmlbml-routing.module';

import { TmlBmlLotteryService } from '@app/tmlbml/services/tmlbml-lottery.service';

import { TmlBmlLotteryContentComponent } from '@app/tmlbml/components/tmlbml-lottery-content/tmlbml-lottery-content.component';
import { TmlBmlLotteryInitComponent } from '@app/tmlbml/components/tmlbml-lottery-init/tmlbml-lottery-init.component';

/**
 * Модуль лотереи "Стирачки".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		TmlBmlRoutingModule,
		TmlBmlInputModule.forChild()
	],
	declarations: [
		TmlBmlLotteryContentComponent,
		TmlBmlLotteryInitComponent
	],
	providers: [
		TmlBmlLotteryService
	]
})
export class TmlBmlModule {}
