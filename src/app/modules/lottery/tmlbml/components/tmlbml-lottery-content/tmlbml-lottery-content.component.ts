import { Component, OnDestroy } from '@angular/core';
import { TmlBmlLotteryService } from '@app/tmlbml/services/tmlbml-lottery.service';

/**
 * Компонент-контейнер для компонентов Бумажных Моментальных Лотерей.
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class TmlBmlLotteryContentComponent implements OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TmlBmlLotteryService} tmlBmlLotteryService Сервис для работы с Бумажными Моментальными Лотереями.
	 */
	constructor(
		private readonly tmlBmlLotteryService: TmlBmlLotteryService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.tmlBmlLotteryService.resetBetData();
	}

}
