import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, timer } from 'rxjs';
import { ButtonGroupStyle } from '@app/shared/components/buttons-group/buttons-group.component';
import { ItemListType } from '@app/shared/components/input-barcode/input-barcode.component';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource } from '@app/core/game/base-game.service';
import { InputTmlbmlComponent } from '@app/tml-bml-input/components/input-tmlbml/input-tmlbml.component';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';
import { TmlBmlInputStoreService } from '@app/tml-bml-input/services/tml-bml-input-store.service';
import { TmlBmlLotteryService } from '@app/tmlbml/services/tmlbml-lottery.service';

/**
 * Компонент ввода лотереи "ТМЛ/БМЛ".
 */
@Component({
	templateUrl: './tmlbml-lottery-init.component.html',
	styleUrls: ['./tmlbml-lottery-init.component.scss']
})
export class TmlBmlLotteryInitComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Constants
	// -----------------------------
	/**
	 * Список стилей группы кнопок.
	 */
	readonly buttonGroupStyle = ButtonGroupStyle;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;
	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на компонент ввода баркодов для лотереи "Стирачки".
	 */
	@ViewChild(InputTmlbmlComponent, { static: true })
	inputTmlbmlComponent: InputTmlbmlComponent;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента регистрации для лотереи "ТМЛ/БМЛ".
	 *
	 * @param tmlBmlLotteryService Сервис для работы с лотереей "ТМЛ/БМЛ".
	 * @param barcodeReaderService Сервис для считывания баркодов.
	 * @param dialogInfoService Сервис для работы с диалоговыми окнами.
	 * @param {ActivatedRoute} route Активированный маршрут.
	 * @param tmlBmlInputStoreService Сервис для работы с хранилищем данных ввода билетов для лотереи "ТМЛ/БМЛ".
	 */
	constructor(
		readonly tmlBmlLotteryService: TmlBmlLotteryService,
		private readonly barcodeReaderService: BarcodeReaderService,
		private readonly dialogInfoService: DialogContainerService,
		private readonly route: ActivatedRoute,
		private readonly tmlBmlInputStoreService: TmlBmlInputStoreService,
	) {}

	/**
	 * Слушатель изменения списка баркодов.
	 *
	 * @param {Array<ItemListType>} list Список баркодов.
	 */
	onBCListChangedHandler(list: Array<ITmlBmlListItem>): void {
		this.tmlBmlLotteryService.betData.ticketList = list;
		this.tmlBmlLotteryService.refreshBets();
	}

	// /**
	//  * Слушатель ошибок парсера баркодов.
	//  *
	//  * @param {string} message Сообщение с ошибкой.
	//  */
	// onParserErrorHandler(message: string): void {
	// 	this.dialogInfoService.showOneButtonInfo('dialog.attention', message, {
	// 		text: 'dialog.dialog_button_continue'
	// 	});
	// }
	//
	// /**
	//  * Слушатель предупреждений от парсера баркодов, требующих логирования.
	//  *
	//  * @param {string} message Сообщение с ошибкой.
	//  */
	// onParserWarningHandler(message: string): void {
	// 	Logger.Log.i('TmlBmlLotteryInitComponent', message)
	// 		.console();
	// }
	//
	// onComponentStateChanged(state: string): void {
	// 	this.bcComponentState = state;
	// }

	// -----------------------------
	//  Private functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.tmlBmlLotteryService.initBetData(BetDataSource.Manual);
		this.tmlBmlLotteryService.init();

		this.tmlBmlInputStoreService.ticketsList$$.next(this.tmlBmlLotteryService.betData.ticketList);

		// при наличии параметра "barcode" добавить его в список
		if (this.route.snapshot.queryParamMap.has('barcode')) {
			const bc = this.route.snapshot.queryParamMap.get('barcode');
			timer(100)
				.subscribe(() => this.tmlBmlInputStoreService.parseNewBarcode(bc));
		}
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}
}
