import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TmlBmlLotteryInitComponent } from './tmlbml-lottery-init.component';
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CoreModule} from "@app/core/core.module";
import {MslGameLogoComponent} from "@app/shared/components/msl-game-logo/msl-game-logo.component";
import {TmlBmlLotteryService} from "@app/tmlbml/services/tmlbml-lottery.service";
import {InputTmlbmlComponent} from "@app/tml-bml-input/components/input-tmlbml/input-tmlbml.component";
import {TmlBmlListComponent} from "@app/tml-bml-input/components/tml-bml-list/tml-bml-list.component";
import {TwoStepPanelComponent} from "@app/tml-bml-input/components/two-step-panel/two-step-panel.component";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {SESS_DATA} from "../../../../features/mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {DRAWS_FOR_GAME_100} from "../../../../features/mocks/game100-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {MslInputBarcodeComponent} from "@app/tml-bml-input/components/msl-input-barcode/msl-input-barcode.component";

describe('TmlbmlLotteryInitComponent', () => {
  let component: TmlBmlLotteryInitComponent;
  let fixture: ComponentFixture<TmlBmlLotteryInitComponent>;
  let appStoreService: AppStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			RouterTestingModule.withRoutes(ROUTES),
			SharedModule,
			TranslateModule.forRoot({
				loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
				}
			}),
			HttpClientModule,
			CoreModule
		],
      declarations: [
		  MslGameLogoComponent,
		  TmlBmlLotteryInitComponent,
		  InputTmlbmlComponent,
		  TmlBmlListComponent,
		  TwoStepPanelComponent,
		  MslInputBarcodeComponent
	  ],
		providers: [
			TmlBmlLotteryService,
			AppStoreService
		]
    })
    .compileComponents();

	  const op = JSON.parse(SESS_DATA.data[0].value);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
	  appStoreService.isLoggedIn$$.next(true);
	  appStoreService.Draws = new LotteriesDraws();
	  appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);
	  const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TmlBmlLotteryInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
