import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TmlBmlLotteryService } from '@app/tmlbml/services/tmlbml-lottery.service';
import { getTranslatedStringByKey } from '@app/util/utils';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';
import { ICheckInfoRow } from '../../features/check-information/check-information/interfaces/icheck-info-row';

/**
 * Резолвер данных для отображения информации о ручной ставке в "Стирачки".
 */
@Injectable()
export class TmlbmlRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {TmlBmlLotteryService} tmlBmlLotteryService Сервис игр "Стирачки".
	 * @param {TranslateService} translateService Сервис переводов.
	 * @param {Router} router Сервис маршрутизации.
	 */
	constructor(
		private readonly tmlBmlLotteryService: TmlBmlLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {

		if (!this.tmlBmlLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.tmlBmlLotteryService
			};
		}

		const allNames = this.tmlBmlLotteryService.betData.ticketList.map(m => m.draws.draw.inst_lott_name);
		const allUniqueNames = Array.from(new Set(allNames));
		const firstThreeName = [...allUniqueNames];
		firstThreeName.splice(3, firstThreeName.length);

		// подсичтать кол-во билетов для продажи
		const count = this.tmlBmlLotteryService.betData.ticketList
			.map(m => Math.abs(m.bcFrom.intTicketNumber - m.bcTo.intTicketNumber) + 1)
			.reduce((p, c) => p + c, 0);

		// если игр больше 3х - показываем только 3 и в последней дописываем "и др."
		if (allUniqueNames.length > 3) {
			const lang = this.translateService.store.translations[this.translateService.currentLang];
			firstThreeName[2] = `${firstThreeName[2]} ${getTranslatedStringByKey(lang, 'lottery.tmlbml.and_others')}`;
		}

		const others: Array<ICheckInfoRow> = firstThreeName.slice(1)
			.map(m => {
				return {
					langKeyName: '',
					value: m
				};
			});

		const checkInfoRowList = [
			{
				langKeyName: 'lottery.lottery',
				value: `${firstThreeName[0]}`
			},
			...others,
			{
				langKeyName: 'lottery.tickets_count',
				value: `${count}`
			},
			{
				langKeyName: 'lottery.sum',
				value: this.tmlBmlLotteryService.formattedAmount,
				redColor: true,
				valueIsCurrency: true
			}
		];

		return {
			checkInfoRowList,
			baseGameService: this.tmlBmlLotteryService
		};
	}

}
