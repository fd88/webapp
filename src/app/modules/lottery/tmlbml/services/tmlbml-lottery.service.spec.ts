import {TestBed} from '@angular/core/testing';

import {TmlBmlLotteryService} from './tmlbml-lottery.service';
import {CoreModule} from "@app/core/core.module";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../app.module";
import {RouterTestingModule} from "@angular/router/testing";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {BetDataSource} from "@app/core/game/base-game.service";
import {SESSION_DATA, TML_BML_LIST} from "../../../features/mocks/mocks";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

describe('TmlBmlLotteryService', () => {
  let service: TmlBmlLotteryService;
  let appStoreService: AppStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
		imports: [
			CoreModule,
			HttpClientModule,
			TranslateModule.forRoot({
				loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
				}
			}),
			RouterTestingModule
		],
		providers: [
			TmlBmlLotteryService,
			HttpService,
			HttpClient,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			}
		]
	});

	TestBed.inject(LogService);
    service = TestBed.inject(TmlBmlLotteryService);
	appStoreService = TestBed.inject(AppStoreService);
	appStoreService.Draws = new LotteriesDraws();
	appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, JSON.parse(SESSION_DATA.data[10].value));
	service.initBetData(BetDataSource.Manual);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

	it('test init method', () => {
		service.betData.ticketList = [...TML_BML_LIST];
		service.init();
		expect(service.amount).toEqual(25140.650000000005);
	});


	it('test buyLottery method', () => {
		service.betData.ticketList = undefined;
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();
	});

	it('test buyLottery method 2', () => {
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();
	});

	it('test buyLottery method 3', async () => {
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		service.betData.ticketList = [...TML_BML_LIST];
		service.init();
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();
	});
});
