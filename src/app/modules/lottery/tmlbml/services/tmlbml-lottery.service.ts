import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { URL_TMLBML } from '@app/util/route-utils';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { PaperInstantRegBetReq, PaperInstantRegBetResp } from '@app/core/net/http/api/models/paper-instant-reg-bet';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { BaseGameService, IBetDataItem } from '@app/core/game/base-game.service';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';
import { LogOutService } from '@app/logout/services/log-out.service';
import {MOCK_DATA} from "../../../features/mocks/mocks";

/**
 * Возвращает модель с начальными данными для лотереи "Стирачки".
 * Вызывается в момент перехода на форму ручного ввода ставки по игре.
 */
const initBetDataFn = (): ITmlBmlBetDataItem => {
	return {
		drawCount: 1,
		ticketList: []
	};
};

/**
 * Интерфейс данных ставки для лотереи "ТМЛ/БМЛ".
 */
export interface ITmlBmlBetDataItem extends IBetDataItem {
	/**
	 * Список введенных билетов.
	 */
	ticketList: Array<ITmlBmlListItem>;
}

/**
 * Сервис лотерей "ТМЛ/БМЛ".
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей, и взаимодействия с ЦС.
 */
@Injectable()
export class TmlBmlLotteryService extends BaseGameService<ITmlBmlBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * URL игры.
	 */
	readonly gameUrl = URL_TMLBML;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.tmlbml.game_name';

	/**
	 * Массив доступных тиражей в игре
	 */
	readonly draws: Array<UpdateDrawInfoDraws> = [];

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {TranslateService} translateService Сервис локализации.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект браузера для работы с адресной строкой.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();
		this.currentGameCode = LotteryGameCode.TML_BML;
		this.currentGroupCode = LotteriesGroupCode.Regular;
		this.initialBetDataFactory = initBetDataFn;
	}

	/**
	 * Инициализация параметров сервиса.
	 */
	init(): void {
		this.draws.splice(0);
		this.draws.push(...this.appStoreService.Draws.getDraws(LotteryGameCode.TML_BML));

		this.refreshBets();
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновить ставки.
	 */
	refreshBets(): void {
		let amount = 0;
		if (this.betData.ticketList) {
			this.betData.ticketList
				.forEach(v => {
					const udi = v.draws;
					const bc1 = v.bcFrom;
					const bc2 = v.bcTo;
					if (bc1 && bc2 && udi && udi.draw) {
						const price = +udi.draw.bet_sum;
						amount += (Math.abs(bc2.intTicketNumber - bc1.intTicketNumber) + 1) * price;
					}
				});
		}

		this.amount = amount;
	}

	buyLotteryAction(): void {
		if (!this.betData.ticketList) {
			Logger.Log.e('TmlBmlLotteryService', `can't buy TML/BML, list is empty!`)
				.console();

			return;
		}

		// найти код тиража
		const dci: ITmlBmlListItem = this.betData.ticketList.find(f => f.draws && f.draws.draw && !!f.draws.draw.code);
		if (!dci) {
			Logger.Log.e('TmlBmlLotteryService', `can't buy TML/BML, draws code not found!`)
				.console();

			return;
		}

		Logger.Log.i('TmlBmlLotteryService', `start buying TML/BML`)
			.console();

		const request = new PaperInstantRegBetReq(this.appStoreService, dci.draws.draw.code, this.betData);
		this.transactionService.buyLottery(PaperInstantRegBetResp, LotteryGameCode.TML_BML, request);
	}

	/**
	 * Купить лотерею.
	 */
	buyLottery(): void {
		if (this.printService.isReady()) {
			this.buyLotteryAction();
		} else {
			if (!!this.appStoreService.cardNum) {
				this.dialogInfoService.showTwoButtonsInfo(
					'dialog.attention',
					'dialog.printer_loyalty_card',
					{
						first: {
							text: 'dialog.dialog_button_continue',
							click: () => {
								this.appStoreService.cardNum = '';
								this.appStoreService.bonusPaySum = 0;
								this.buyLotteryAction();
							}
						},
						second: {
							text: 'common.cancel'
						}
					}
				);
			} else {
				this.buyLotteryAction();
			}
		}


	}

}
