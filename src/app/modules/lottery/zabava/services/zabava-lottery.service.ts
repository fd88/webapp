import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {URL_ZABAVA} from '@app/util/route-utils';
import {numberFromStringCurrencyFormat, TicketFormat} from '@app/util/utils';
import {LotteriesGroupCode, LotteryGameCode} from '@app/core/configuration/lotteries';
import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {UpdateDrawInfoDraws, ZabavaData} from '@app/core/net/http/api/models/update-draw-info';
import {ZabavaBetsData, ZabavaRegBetReq, ZabavaRegBetResp} from '@app/core/net/http/api/models/zabava-reg-bet';
import {Logger} from '@app/core/net/ws/services/log/logger';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {TransactionService} from '@app/core/services/transaction/transaction.service';
import {PrintService} from '@app/core/net/ws/services/print/print.service';
import {BaseGameService, GameSaleMode} from '@app/core/game/base-game.service';
import {IZabavaBetDataItem} from '@app/zabava/interfaces/izabava-bet-data-item';
import {LogOutService} from '@app/logout/services/log-out.service';
import {from, Observable} from "rxjs";
import {GetBlankInfoReq, GetBlankInfoResp} from "@app/core/net/http/api/models/get-blank-info";
import {HttpService} from "@app/core/net/http/services/http.service";
import {IZabavaBlanksData} from "@app/zabava/interfaces/i-zabava-blanks-data";
import {IBlanksList} from "@app/zabava/interfaces/i-blanks-list";
import {EsapParams} from "@app/core/configuration/esap";

/**
 * Возвращает модель с начальными данными для игры "Забава".
 */
const initBetDataFn = (): IZabavaBetDataItem => {
	return {
		betsCount: 0,
		drawCount: 1,
		drawIndex: -1,
		hasExtraGame: false,
		pairsCount: 0
	};
};

/**
 * Сервис лотереи "Забава".
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей, и взаимодействия с ЦС.
 */
@Injectable()
export class ZabavaLotteryService extends BaseGameService<IZabavaBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * URL игры.
	 */
	readonly gameUrl = URL_ZABAVA;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.zabava.loto_zabava';

	/**
	 * Максимальное допустимое количество билетов.
	 */
	readonly BETS_COUNT = 10;

	/**
	 * Список видимых тиражей.
	 */
	visibleDraws: Array<UpdateDrawInfoDraws>;

	/**
	 * Сеттер выбранного тиража.
	 * @param {number} value Индекс тиража.
	 */
	set drawIndex(value: number) {
		this.betData.drawIndex = value;
		this.updateProperties();
	}

	/**
	 * Геттер выбранного тиража.
	 */
	get drawIndex(): number {
		return this.betData.drawIndex;
	}

	/**
	 * Признак наличия бонусной игры участия в студии.
	 */
	isVisibleStudio: boolean;

	/**
	 * Признак наличия бонусной игры "Парочки".
	 */
	isVisiblePairs: boolean;

	/**
	 * Максимальное допустимое количество парочек.
	 */
	maxPairsCount = 0;

	/**
	 * Источник ввода ставок
	 * 1 - вручную, 2 - типографские бланки
	 */
	betInputMode = 1;

	/**
	 * Параметры для продажи с помощью бланков
	 */
	blanksModeData: IZabavaBlanksData;

	/**
	 * Список бланков, введенных в таблицу
	 */
	blanksList: Array<IBlanksList> = [];

	/**
	 * Текущий тираж.
	 */
	currentDraw: UpdateDrawInfoDraws;

	/**
	 * Количество ставок (отдельно)
	 */
	betsCount = 0;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Цена участия в конкурсе "Парочка".
	 * @private
	 */
	private _pairPrice = 0;

	/**
	 * Цена участия в студии.
	 * @private
	 */
	private _studioPrice = 0;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param logoutService Сервис выхода из системы.
	 * @param {TranslateService} translateService Сервис локализации.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект для работы с адресной строкой браузера.
	 * @param httpService
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		readonly logoutService: LogOutService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		private readonly httpService: HttpService
	) {
		super();

		this.currentGameCode = LotteryGameCode.Zabava;
		this.currentGroupCode = LotteriesGroupCode.Regular;
		this.initialBetDataFactory = initBetDataFn;
	}

	/**
	 * Инициализация и очистка модели (предыдущего состояния).
	 */
	init(): void {
		this.visibleDraws = this.activeDraws.slice(0, 3);

		// TODO: временное решение, пока нет возможности получать бонусные игры ---------------------------------
		this.activeDraws
			.forEach(draw => {
				if (!draw.draw.data) {
					draw.draw.data = {};
				}

				draw.draw.data.extra_game = [
					{
						code: '1',
						name: 'Парочка',
						sum: '5.00',
						max_count: 5
					},
					{
						code: '2',
						name: 'СТУДІЯ',
						sum: '2.00',
						max_count: 1
					}
				];
			});
		// -------------------------------------------------------------------------------------------------------

		// this.draws[0].draw.sale_edate = new Date().toLocaleString();
		// this.draws[1].draw.sale_edate = new Date().toLocaleString();

		this.updateProperties();
	}

	/**
	 * Возвращает данные для ставки в формате указанном в API {@link ZabavaBetsData}.
	 *
	 * @returns {ZabavaBetsData}
	 */
	getZabavaBetsData(): ZabavaBetsData {
		return {
			tickets: [{
				t: this.betData.betsCount,
				f: 0,
				e: [
					{c: 1, n: this.betData.pairsCount ? this.betData.pairsCount : 0},
					{c: 2, n: this.betData.hasExtraGame ? this.betData.betsCount : 0}
				]
			}]
		};
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновить ставки.
	 */
	refreshBets(): void {
		if (this.drawIndex >= 0) {
			const pairsCount = Number.isInteger(this.betData.pairsCount) ? this.betData.pairsCount : 0;
			const betsCount = Number.isInteger(this.betData.betsCount) ? this.betData.betsCount : 0;

			this.amount = (numberFromStringCurrencyFormat(this.activeDraws[this.drawIndex].draw.bet_sum)
				+ (pairsCount * this._pairPrice)
				+ (this.betData.hasExtraGame ? this._studioPrice : 0)
			) * betsCount / 100;
		} else {
			this.amount = 0;
		}
	}

	/**
	 * Получение информации о типографском бланке
	 */
	getBlankInfo(drawCode: number, macCode: string): Observable<GetBlankInfoResp> {
		const blankInfoReq = new GetBlankInfoReq(this.appStoreService, drawCode, macCode);
		return from(this.httpService.sendApi(blankInfoReq)) as Observable<GetBlankInfoResp>;
	}

	/**
	 * Покупка лотереи.
	 */
	buyLottery(): void {
		Logger.Log.i('ZabavaLotteryService', 'buyLottery -> start buying Zabava')
			.console();

		const buyZabava = new ZabavaRegBetReq(
			this.appStoreService,
			this.amount,
			this.betData,
			// this.betData.betsCount,
			this.getZabavaBetsData(),
			this.activeDraws[this.drawIndex].draw.num,
			// this.betData.drawCount
		);

		this.transactionService.buyLottery(ZabavaRegBetResp, LotteryGameCode.Zabava, buyZabava);
	}

	/**
	 * Покупка лотереи с помощью бланков
	 */
	buyLotteryWithBlanks(): void {
		Logger.Log.i('ZabavaLotteryService', 'buyLottery -> start buying Zabava')
			.console();

		const buyZabava = new ZabavaRegBetReq(
			this.appStoreService,
			this.blanksModeData.amount,
			{
				betsCount: this.blanksModeData.betsCount,
				pairsCount: this.blanksModeData.pairsCount,
				hasExtraGame: false,
				drawIndex: this.blanksModeData.drawIndex,
				drawCount: 1,
				saleMode: GameSaleMode.RegularSale
			},
			{
				tickets: [{
					t: this.blanksModeData.betsCount,
					f: 0,
					e: [
						{c: 1, n: this.blanksModeData.pairsCount}
						// {c: 2, n: 0}
					]
				}]
			},
			this.blanksModeData.drawNum,
			this.blanksModeData.macCode
		);
		buyZabava.params.set(EsapParams.TICKET_FORMAT, TicketFormat.TEXT_32);
		this.transactionService.saleMode = 1;
		this.transactionService.buyLottery(ZabavaRegBetResp, LotteryGameCode.Zabava, buyZabava);
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Обновить свойства модели.
	 * @private
	 */
	private updateProperties(): void {
		// проверить наличие выбранного тиража
		if (this.drawIndex === -1) {
			this.amount = 0;

			return;
		}

		// определить параметры текущего тиража по бонусным играм
		const draw = this.activeDraws[this.drawIndex];
		if (draw.draw.data && draw.draw.data.extra_game) {
			const pairs = (draw.draw.data as ZabavaData).extra_game.find(p => p.code === '1');
			if (pairs) {
				this._pairPrice = numberFromStringCurrencyFormat(pairs.sum);
				this.maxPairsCount = pairs.max_count;
				this.isVisiblePairs = true;
			}

			const studio = (draw.draw.data as ZabavaData).extra_game.find(p => p.code === '2');
			if (studio) {
				this._studioPrice = numberFromStringCurrencyFormat(studio.sum);
				this.isVisibleStudio = true;
			}
		} else {
			this._pairPrice = 0;
			this._studioPrice = 0;
			this.maxPairsCount = 0;
			this.isVisibleStudio = false;
			this.isVisiblePairs = false;
		}
	}
}
