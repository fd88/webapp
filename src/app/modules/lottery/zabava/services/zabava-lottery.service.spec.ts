import { TestBed } from '@angular/core/testing';

import { ZabavaLotteryService } from './zabava-lottery.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { Injector } from '@angular/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { HttpServiceStub } from '@app/core/net/http/services/http.service.spec';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { TransactionServiceStub } from '@app/core/services/transaction/transaction.service.spec';
import { BetDataSource } from '@app/core/game/base-game.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { EsapActions } from '@app/core/configuration/esap';
import { PrinterState } from '@app/core/net/ws/api/models/print/print-models';
import {LogOutService} from "@app/logout/services/log-out.service";
import {LogOutServiceStub} from "@app/logout/services/log-out.service.spec";
import {DRAWS_FOR_GAME_ZABAVA} from "../../../features/mocks/zabava-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";

xdescribe('ZabavaLotteryService', () => {
	let service: ZabavaLotteryService | any;
	let appStoreService: AppStoreService;
	let printService: PrintService | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				ZabavaLotteryService,
				LogService,
				PrintService,
				Injector,
				{
					provide: LogOutService,
					useValue: LogOutServiceStub,
				},
				{
					provide: HttpService,
					useValue: new HttpServiceStub()
				},
				AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				{
					provide: TransactionService,
					useValue: new TransactionServiceStub()
				}
			]
		});

		TestBed.inject(LogService);

		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Zabava, {...DRAWS_FOR_GAME_ZABAVA.lottery});

		printService = TestBed.inject(PrintService);

		service = TestBed.inject(ZabavaLotteryService);
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(
			LotteryGameCode.Zabava,
			new Map<string, string>([[EsapActions.ZabavaRegBet, 'url']]
		));

		service.initBetData(BetDataSource.Manual);
		service.init();
	});

	it('should be created', async () => {
		await expect(service).toBeTruthy();
	});

	it('test getZabavaBetsData function', async () => {
		await expect(service.getZabavaBetsData()).toEqual({
			tickets: [{
				t: service.betData.betsCount,
				f: 0,
				e: [
					{c: 1, n: 0},
					{c: 2, n: 0}
				]
			}]
		});

		service.betData.betsCount = 1;
		service.betData.pairsCount = 1;
		service.betData.hasExtraGame = true;

		expect(service.getZabavaBetsData()).toEqual({
			tickets: [{
				t: service.betData.betsCount,
				f: 0,
				e: [
					{c: 1, n: 1},
					{c: 2, n: 1}
				]
			}]
		});

	});

	it('test refreshBets function', () => {
		service.drawIndex = -1;
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.drawIndex = 0;
		service.betData.betsCount = undefined;
		service.betData.pairsCount = undefined;
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.drawIndex = 0;
		service.betData.betsCount = 0;
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.refreshBets();
		expect(service.amount).toEqual(10);

		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.betData.hasExtraGame = true;
		service.betData.pairsCount = 1;
		service.refreshBets();
		expect(service.amount).toEqual(17);
	});

	xit('test buyLottery function #1', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();

		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.refreshBets();

		printService.printerState = PrinterState.OffLine;
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});

	xit('test buyLottery function #2', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();

		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.refreshBets();

		printService.printerState = PrinterState.OnLine;
		service.transactionService.executeRequest = (): Promise<void> => new Promise((resolve, reject) => reject({code: 1000}));
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});

	xit('test buyLottery function #3', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();

		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.refreshBets();

		printService.printerState = PrinterState.OnLine;
		service.transactionService.executeRequest = (): Promise<void> => new Promise(resolve => resolve());
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});

	it('test updateProperties function', () => {
		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.refreshBets();
		service.updateProperties();
		expect(service._pairPrice).toEqual(500);
		expect(service._studioPrice).toEqual(200);
		expect(service.maxPairsCount).toEqual(5);
		expect(service.isVisibleStudio).toBeTruthy();
		expect(service.isVisiblePairs).toBeTruthy();

		service.activeDraws[0].draw.data = undefined;
		service.drawIndex = 0;
		service.betData.betsCount = 1;
		service.refreshBets();
		service.updateProperties();
		expect(service._pairPrice).toEqual(0);
		expect(service._studioPrice).toEqual(0);
		expect(service.maxPairsCount).toEqual(0);
		expect(service.isVisibleStudio).toBeFalsy();
		expect(service.isVisiblePairs).toBeFalsy();
	});
});
