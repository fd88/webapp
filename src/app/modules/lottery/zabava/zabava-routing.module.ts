import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {URL_BLANKS, URL_INIT, URL_REGISTRY, URL_RESULTS} from '@app/util/route-utils';
import { AuthGuard } from '@app/core/guards/auth.guard';
import { ZabavaRegistryResolver } from '@app/zabava/zabava-registry-resolver';
import { ZabavaLotteryContentComponent } from '@app/zabava/components/zabava-lottery-content/zabava-lottery-content.component';
import { ZabavaLotteryInitComponent } from '@app/zabava/components/zabava-lottery-init/zabava-lottery-init.component';
import { ZabavaResultsComponent } from '@app/zabava/components/zabava-results/zabava-results.component';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';
import {ZabavaScanBlanksComponent} from "@app/zabava/components/zabava-scan-blanks/zabava-scan-blanks.component";

/**
 * Список маршрутов для модуля игры "Забава".
 */
const routes: Routes = [
	{
		path: ``,
		component: ZabavaLotteryContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: URL_INIT,
				component: ZabavaLotteryInitComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_BLANKS,
				component: ZabavaScanBlanksComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},

			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: ZabavaRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_RESULTS,
				component: ZabavaResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}];

/**
 * Модуль маршрутизации лотереи "Забава".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		ZabavaRegistryResolver
	]
})
export class ZabavaRoutingModule {}
