import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { BetDataSource, GameSaleMode } from '@app/core/game/base-game.service';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { ZabavaLotteryService } from '@app/zabava/services/zabava-lottery.service';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';
import {formattedAmount} from "@app/util/utils";

/**
 * Резолвер данных для отображения информации о ручной ставке в "Забава".
 */
@Injectable()
export class ZabavaRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {ZabavaLotteryService} zabavaLotteryService Сервис игры "Забава".
	 * @param {TranslateService} translateService Сервис переводов.
	 * @param {Router} router Сервис маршрутизации.
	 */
	constructor(
		private readonly zabavaLotteryService: ZabavaLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		// проверить наличие акционных параметров
		const parsedUrl = this.router.parseUrl(state.url);
		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET) || parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			let betData;
			if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
				const actionBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionsButton;
				betData = actionBet.data;
			}

			if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
				const demoBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET));
				betData = demoBet.data;
			}

			this.zabavaLotteryService.initBetData(BetDataSource.Manual);
			this.zabavaLotteryService.init();
			this.zabavaLotteryService.drawIndex = 0;
			this.zabavaLotteryService.betData.drawCount = Number.isInteger(betData.drawCount) ? betData.drawCount : 1;
			this.zabavaLotteryService.betData.betsCount = Number.isInteger(betData.betCount) ? betData.betCount : 1;
			this.zabavaLotteryService.betData.hasExtraGame = false;
			this.zabavaLotteryService.betData.pairsCount = Number.isInteger(betData.pairs) ? betData.pairs : 0;
			this.zabavaLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			this.zabavaLotteryService.refreshBets();
		}

		if (!this.zabavaLotteryService.activeDraws || !this.zabavaLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.zabavaLotteryService
			};
		}

		const checkInfoRowList = [
			{
				langKeyName: 'lottery.selected_draw',
				value: `${this.zabavaLotteryService.activeDraws[this.zabavaLotteryService.betData.drawIndex].draw.num}`
			},
			{
				langKeyName: 'lottery.zabava.rich_and_famous',
				value: `${this.zabavaLotteryService.betData.hasExtraGame ? 'dialog.yes' : 'dialog.no'}`
			},
			{
				langKeyName: 'lottery.zabava.pair',
				value: `${this.zabavaLotteryService.betData.pairsCount ? this.zabavaLotteryService.betData.pairsCount : 0}`
			},
			{
				langKeyName: 'lottery.all_tickets_number',
				value: `${this.zabavaLotteryService.betData.betsCount}`
			},
			{
				langKeyName: 'lottery.sum',
				value: this.zabavaLotteryService.betInputMode === 2 ?
					formattedAmount(this.zabavaLotteryService.blanksModeData.amount) :
					`${this.zabavaLotteryService.formattedAmount}`,
				redColor: true,
				valueIsCurrency: true
			}
		];

		return {
			checkInfoRowList,
			baseGameService: this.zabavaLotteryService
		};
	}

}
