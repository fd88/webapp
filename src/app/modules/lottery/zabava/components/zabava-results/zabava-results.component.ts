import { Component } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDrawingResult } from '@app/core/net/http/api/models/get-draw-results';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import {
	addWinCatRows,
	IAbstractViewResults,
	IDrawingView,
	IResultPrintItem,
	PrintResultTypes
} from '@app/core/services/results/results-utils';

/**
 * Модель отображения данных по результатам "Забавы".
 */
interface IZabavaResults extends IAbstractViewResults {
	/**
	 * Тиражи, по которым отображаются результаты.
	 */
	drawing: Array<IDrawingView>;
}

/**
 * Компонент результатов по "Забаве".
 */
@Component({
	selector: 'app-zabava-results',
	templateUrl: './zabava-results.component.html',
	styleUrls: ['./zabava-results.component.scss']
})
export class ZabavaResultsComponent  {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Код игры.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Данные для отображения результатов.
	 */
	viewData: IZabavaResults;

	/**
	 * Данные для печати результатов.
	 */
	printData: Array<IResultPrintItem>;

	/**
	 * Класс для задания высоты прокрутки.
	 */
	scrollHeightClass = '';

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GameResultsService} gameResultsService Сервис результатов игр.
	 */
	constructor(
		readonly gameResultsService: GameResultsService
	) {}

	/**
	 * Обработчик нажатия выбора результатов по тиражу.
	 *
	 * @param {IDrawingResult} result Результаты по тиражу.
	 */
	onSelectedResultsHandler(result: IDrawingResult): void {
		if (!result) {
			this.viewData = undefined;
			this.printData = undefined;

			return;
		}

		const drawNumber = result.draw_name;
		const drawDate = result.drawing_date_begin;
		const extraInfo = result.drawing[0].extra_info;
		const drawing = result.drawing
			.map(m => {
				const winComb: Array<string> = m.win_comb ? m.win_comb.split(' ') : undefined;

				return {...m, winComb};
			});

		this.viewData = {drawNumber, drawDate, extraInfo, drawing};
		this.printData = this.printDataParser();
		this.updateHeightClass();
	}

	/**
	 * Функция для отслеживания изменений в массиве.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByWinCombFn = (index, item: string) => index;

	/**
	 * Функция для отслеживания изменений в массиве.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByWinDrawingFn = (index, item: IDrawingView) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Костыль для хромиума, обновляет высоту скролл-области.
	 */
	private updateHeightClass(): void {
		if (this.viewData && this.viewData.drawing && this.viewData.drawing.length > 0) {
			this.scrollHeightClass = (this.viewData.drawing[0].winComb.length > 57) ? 'scroll-height-4' :
				(this.viewData.drawing[0].winComb.length > 38) ? 'scroll-height-3' :
					(this.viewData.drawing[0].winComb.length > 19) ? 'scroll-height-2' : 'scroll-height-1';
		} else {
			this.scrollHeightClass = '';
		}
	}

	/**
	 * Функция-парсер данных для печати.
	 */
	private printDataParser(): Array<IResultPrintItem> {
		const result = [
			{key: PrintResultTypes.LotteryName, value: ['lottery.zabava.loto_zabava']},
			{key: PrintResultTypes.DrawNumber, value: [this.viewData.drawNumber]},
			{key: PrintResultTypes.DrawDate, value: [this.viewData.drawDate]}
		];

		if (Array.isArray(this.viewData.drawing) && this.viewData.drawing.length > 0) {
			const p0: IDrawingView = this.viewData.drawing[0];
			result.push({key: PrintResultTypes.WinCombWithLines, value: p0.winComb});

			// основные категории
			if (p0.win_cat) {
				addWinCatRows(p0, result, false, false, {
					key: PrintResultTypes.TwoColumnTable,
					value: ['lottery.category', 'lottery.win']
				});
			}

			// парочки
			if (this.viewData.drawing.length > 1 && this.viewData.drawing[1].win_cat) {
				addWinCatRows(this.viewData.drawing[1], result, true, true, {
					key: PrintResultTypes.TwoColumnTable,
					value: ['lottery.sub_category', 'lottery.win']
				}, [
					{key: PrintResultTypes.OneColumnTable, value: ['lottery.win_combination']},
					{key: PrintResultTypes.WinCombWOLines, value: this.viewData.drawing[1].winComb}
				]);
			}

			// конкурс багаті та відомі
			if (this.viewData.drawing.length > 2 && this.viewData.drawing[2].win_comb !== '-') {
				addWinCatRows(this.viewData.drawing[2], result, true, true);
				result.push({key: PrintResultTypes.OneColumnTable, value: ['lottery.first_serial_number']});
				const paddedStr = this.viewData.drawing[2].win_comb.padStart(5, '0');
				result.push({key: PrintResultTypes.OneColumnTable, value: [` ${paddedStr}`]});
			}

			// джек-пот
			result.push({key: PrintResultTypes.OneColumnTable, value: [p0.extra_info]});
		}

		return result;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
