import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { ReportsService } from '@app/core/services/report/reports.service';
import { DatePipe } from '@angular/common';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { ZabavaResultsComponent } from '@app/zabava/components/zabava-results/zabava-results.component';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {ResultsNavigatorComponent} from "@app/shared/components/results-navigator/results-navigator.component";
import {HttpClientModule} from "@angular/common/http";

describe('ZabavaResultsComponent', () => {
	let component: ZabavaResultsComponent | any;
	let fixture: ComponentFixture<ZabavaResultsComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				SharedModule,
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot({})
			],
			declarations: [
				ZabavaResultsComponent,
				ResultsNavigatorComponent
			],
			providers: [
				GameResultsService,
				HttpService,
				ReportsService,
				DatePipe,
				MslCurrencyPipe,
				AppStoreService,
				StorageService,
				TransactionService,
				PrintService,
				LotteriesService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				TranslateService
			]
		})
		.compileComponents();

		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ZabavaResultsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});


	it('test onSelectedResultsHandler', () => {
		component.onSelectedResultsHandler(undefined);
		expect(component.viewData).toBeUndefined();

		component.onSelectedResultsHandler({
			"draw_code": 1461,
			"draw_name": '1246',
			"drawing": [
				{
					extra_info: 'Джекпот наступн.тиражу - 3,000,000 грн.',
					name: 'Основний розiграш',
					win_comb: '-',
					win_cat: [],
					data: {
						events: []
					}
				},
				{
					extra_info: '',
					name: 'Конкурс Парочка',
					win_cat: [
						{
							name: 'Повний збiг',
							win_cnt: '0',
							win_sum: '300000.00'
						},
						{
							name: 'Будь-який кут з 5-ти',
							win_cnt: '1',
							win_sum: '7500.00'
						},
						{
							name: 'Будь-яка лiнiя з 3-х',
							win_cnt: '8',
							win_sum: '100.00'
						},
						{
							name: 'Верхня вершина',
							win_cnt: '364',
							win_sum: '5.00'
						}
					],
					win_comb: '02 05 19 08 11 37 42 50 72',
					data: {
						events: []
					}
				},
				{
					extra_info: '',
					name: 'Конкурс Багаті та відомі',
					win_comb: '202',
					win_cat: [],
					data: {
						events: []
					}
				}
			],
			drawing_date_begin:'2019-02-25 14:44:32',
			drawing_date_end:'2020-09-12 13:44:31',
			winning_date_end:'2020-09-12 13:44:31'
		});
		expect(component.viewData).toEqual({
			drawNumber:'1246',
			drawDate:'2019-02-25 14:44:32',
			extraInfo:'Джекпот наступн.тиражу - 3,000,000 грн.',
			drawing:[
				{
					extra_info:'Джекпот наступн.тиражу - 3,000,000 грн.',
					name:'Основний розiграш',
					win_comb:'-',
					winComb:[
						'-'
					],
					win_cat: [],
					data: {
						events: []
					}
				},
				{
					extra_info:'',
					name:'Конкурс Парочка',
					win_cat:[
						{
							'name':'Повний збiг',
							'win_cnt':'0',
							'win_sum':'300000.00'
						},
						{
							'name':'Будь-який кут з 5-ти',
							'win_cnt':'1',
							'win_sum':'7500.00'
						},
						{
							'name':'Будь-яка лiнiя з 3-х',
							'win_cnt':'8',
							'win_sum':'100.00'
						},
						{
							'name':'Верхня вершина',
							'win_cnt':'364',
							'win_sum':'5.00'
						}
					],
					win_comb:'02 05 19 08 11 37 42 50 72',
					winComb:[
						'02',
						'05',
						'19',
						'08',
						'11',
						'37',
						'42',
						'50',
						'72'
					],
					data: {
						events: []
					}
				},
				{
					extra_info:'',
					name:'Конкурс Багаті та відомі',
					win_comb:'202',
					winComb:[
						'202'
					],
					win_cat: [],
					data: {
						events: []
					}
				}
			]
		});
	});

	it('test updateHeightClass', () => {
		component.viewData = undefined;
		component.updateHeightClass();
		expect(component.scrollHeightClass).toEqual('');
	});

	it('test trackByWinCombFn', () => {
		expect(component.trackByWinCombFn(1, 5)).toEqual(1);
	});

	it('test trackByWinDrawingFn', () => {
		expect(component.trackByWinDrawingFn(1, undefined)).toEqual(1);
	});

});
