import {Component, OnDestroy, ViewChild} from '@angular/core';
import { ZabavaLotteryService } from '@app/zabava/services/zabava-lottery.service';
import {DrawsButtonsComponent} from "@app/shared/components/draws-buttons/draws-buttons.component";
import {URL_BLANKS, URL_INIT, URL_RESULTS} from "@app/util/route-utils";
import {NavigationEnd, Router} from "@angular/router";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {BetDataSource} from "@app/core/game/base-game.service";
import {filter, takeUntil} from "rxjs/operators";
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	ButtonTextStyle,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import {LotteryGameCode} from '@app/core/configuration/lotteries';
import {interval, Subject} from "rxjs";
import {UpdateDrawInfoDraws} from "@app/core/net/http/api/models/update-draw-info";


/**
 * Компонент-контейнер для компонентов лотереи "Забава".
 */
@Component({
	selector: 'app-zabava-lottery-content',
	templateUrl: './zabava-lottery-content.component.html',
	styleUrls: ['./zabava-lottery-content.component.scss']
})
export class ZabavaLotteryContentComponent implements  OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Кнопки выбора тиражей.
	 */
	@ViewChild(DrawsButtonsComponent)
	drawsButtons: DrawsButtonsComponent;

	/**
	 * Кнопки выбора парочек.
	 */
	@ViewChild('pairsComponent', { static: true })
	pairsComponent: ButtonsGroupComponent;

	/**
	 * Кнопки выбора количества билетов.
	 */
	@ViewChild('ticketsComponent', { static: true })
	ticketsComponent: ButtonsGroupComponent;

	/**
	 * Кнопки выбора участия в студии.
	 */
	@ViewChild('studioComponent')
	studioComponent: ButtonsGroupComponent;

	/**
	 * Перечисление стилей кнопок, доступных к использованию.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Перечисление стилей текста кнопок, доступных к использованию.
	 */
	readonly ButtonTextStyle = ButtonTextStyle;

	/**
	 * Перечисление режимов работы группы кнопок
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Перечисление источников данных для создания ставок.
	 */
	readonly BetDataSource = BetDataSource;

	/**
	 * Путь к результатам.
	 */
	readonly Results = `../${URL_RESULTS}`;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Джекпот текущего тиража.
	 */
	jackpot: string;

	/**
	 * Текущая страница
	 */
	currentPage = URL_INIT;

	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Конструктор компонента.
	 *
	 * @param {Router} router Объект маршрутизации.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {ZabavaLotteryService} zabavaLotteryService Сервис лотереи "Забава".
	 */
	constructor(
		private readonly router: Router,
		private readonly dialogInfoService: DialogContainerService,
		readonly zabavaLotteryService: ZabavaLotteryService
	) {}

	// -----------------------------
	//  Protected functions
	// -----------------------------

	/**
	 * Обработчик выбора парочек.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedPairsHandler(button: IButtonGroupItem): void {
		this.zabavaLotteryService.betData.pairsCount = button ? +button.label : 0;
		this.zabavaLotteryService.refreshBets();
	}

	/**
	 * Обработчик выбора количества ставок.
	 *
	 * @param $event Количество ставок
	 */
	onBetsCountChange($event: number): void {
		if (this.zabavaLotteryService.betData) {
			this.zabavaLotteryService.betData.betsCount = $event;
		}
		this.zabavaLotteryService.refreshBets();
	}

	/**
	 * Обработчик выбора тиража.
	 *
	 * @param draw
	 */
	onSelectedDrawHandler(draw: UpdateDrawInfoDraws): void {
		this.zabavaLotteryService.drawIndex = this.zabavaLotteryService.visibleDraws.findIndex(v => v.draw.num === draw.draw.num);
		const intJackpot = parseInt(draw.draw.jackpot, 10);
		this.jackpot = isNaN(intJackpot) ? '' : intJackpot.toLocaleString();
		this.zabavaLotteryService.refreshBets();
	}

	/**
	 * Обработчик ситуации, когда по таймеру пропали все активные тиражи.
	 */
	onNoActiveDrawsHandler(): void {
		this.zabavaLotteryService.drawIndex = -1;
	}

	/**
	 * Обработчик выбора участия игрока в студии.
	 *
	 * @param button Выбранная кнопка.
	 */
	onChangeStudioHandler(button: IButtonGroupItem): void {
		this.zabavaLotteryService.betData.hasExtraGame = button.selected;
		this.zabavaLotteryService.refreshBets();
	}

	/**
	 * Метод инициализации компонента Забавы для упрощения тестирования
	 */
	zabavaInit(): void {
		this.zabavaLotteryService.init();
		// выставить кол-во парочек
		if (this.zabavaLotteryService.betData.pairsCount) {
			this.pairsComponent.selectedButtons = this.zabavaLotteryService.betData.pairsCount - 1;
		}

		// выставить кол-во ставок
		if (this.zabavaLotteryService.betData.betsCount) {
			this.ticketsComponent.selectedButtons = this.zabavaLotteryService.betData.betsCount - 1;
		}

		// выставить кнопку "студия"
		// this.studioComponent.selectedButtons = (this.zabavaLotteryService.betData.hasExtraGame ? [0] : []);

		// текущий тираж
		if (this.zabavaLotteryService.betData.drawIndex >= 0) {
			this.zabavaLotteryService.drawIndex = this.zabavaLotteryService.betData.drawIndex;
		} else {
			this.zabavaLotteryService.drawIndex = this.zabavaLotteryService.activeDraws
				.findIndex(elem => new Date(elem.draw.sale_edate).getTime() >= Date.now());
		}
		if (this.zabavaLotteryService.drawIndex > -1) {
			this.zabavaLotteryService.currentDraw = this.zabavaLotteryService.activeDraws[this.zabavaLotteryService.drawIndex];
			if (this.zabavaLotteryService.currentDraw.draw.jackpot) {
				const intJackpot = parseInt(this.zabavaLotteryService.currentDraw.draw.jackpot, 10);
				this.jackpot = isNaN(intJackpot) ? '' : intJackpot.toLocaleString();
			}
		}

		this.zabavaLotteryService.refreshBets();
	}

	private setCurrentPage(url: string): void {
		const objURL = new URL(`https://site.com${url}`);
		const urlParts = objURL.pathname.split('/');
		this.currentPage = urlParts[urlParts.length - 1];
		if (this.currentPage === URL_INIT) {
			this.zabavaLotteryService.betInputMode = 1;
		}
		if (this.currentPage === URL_BLANKS) {
			this.zabavaLotteryService.betInputMode = 2;
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.setCurrentPage(this.router.url);
		// подписаться на изменение событий роутера
		this.router.events
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(e => {
				if (e instanceof NavigationEnd) {
					this.setCurrentPage(e.url);
				}
			});
		this.zabavaLotteryService.initBetData(BetDataSource.Manual);
		this.zabavaInit();
		const tmr = interval(50)
			.pipe(
				filter(() => !!this.drawsButtons)
			)
			.subscribe(() => {
				this.drawsButtons.activeDraw = this.zabavaLotteryService.activeDraws[this.zabavaLotteryService.drawIndex];
				this.zabavaLotteryService.currentDraw = this.zabavaLotteryService.visibleDraws[0];
				tmr.unsubscribe();
			});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.zabavaLotteryService.betsCount = 0;
		this.zabavaLotteryService.resetBetData();
		this.zabavaLotteryService.blanksList = [];
		this.zabavaLotteryService.blanksModeData = undefined;
	}
}
