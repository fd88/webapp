import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';
import { HttpService } from '@app/core/net/http/services/http.service';
import { ZabavaLotteryContentComponent } from '@app/zabava/components/zabava-lottery-content/zabava-lottery-content.component';
import { ZabavaLotteryService } from '@app/zabava/services/zabava-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_ZABAVA} from "../../../../features/mocks/zabava-draws";
import {BetDataSource} from "@app/core/game/base-game.service";
import {Operator} from "@app/core/services/store/operator";

describe('ZabavaLotteryContentComponent', () => {
	let component: ZabavaLotteryContentComponent;
	let fixture: ComponentFixture<ZabavaLotteryContentComponent>;

	let appStoreService: AppStoreService;
	let csConfig: any;
	let zabavaLotteryService: ZabavaLotteryService;

	beforeEach(waitForAsync(async () => {
		await TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			declarations: [
				ZabavaLotteryContentComponent
			],
			providers: [
				LogService,
				DeclineWordPipe,
				ZabavaLotteryService,
				HttpService,
				LotteriesService
			]
		})
			.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Zabava, {...DRAWS_FOR_GAME_ZABAVA.lottery});

		zabavaLotteryService = TestBed.inject(ZabavaLotteryService);
		zabavaLotteryService.initBetData(BetDataSource.Manual);
		zabavaLotteryService.init();

		csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		const operator = new Operator('testInstantUser', `${Date.now()}`, '123456', 1);
		appStoreService.operator.next(operator);

		zabavaLotteryService.currentDraw = DRAWS_FOR_GAME_ZABAVA.lottery.draws[0];
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ZabavaLotteryContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
