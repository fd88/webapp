import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZabavaScanBlanksComponent } from './zabava-scan-blanks.component';
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {CoreModule} from "@app/core/core.module";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClient} from "@angular/common/http";
import {ZabavaLotteryService} from "@app/zabava/services/zabava-lottery.service";
import {
	MslInputWithKeyboardComponent
} from "@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component";
import {SpoilerComponent} from "@app/shared/components/spoiler/spoiler.component";
import {RegisterBetButtonComponent} from "@app/shared/components/register-bet-button/register-bet-button.component";
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {Operator} from "@app/core/services/store/operator";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_ZABAVA} from "../../../../features/mocks/zabava-draws";
import {BetDataSource} from "@app/core/game/base-game.service";
import {UserInputMode} from "@app/util/utils";
import {CameraService} from "@app/core/services/camera.service";

describe('ZabavaScanBlanksComponent', () => {
  let component: ZabavaScanBlanksComponent | any;
  let fixture: ComponentFixture<ZabavaScanBlanksComponent>;
  let appStoreService: AppStoreService;
  let csConfig: any;
  let zabavaLotteryService: ZabavaLotteryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			SharedModule,
			TranslateModule.forRoot(),
			HttpClientTestingModule,
			CoreModule,
			RouterTestingModule.withRoutes(ROUTES)
		],
      declarations: [
		  ZabavaScanBlanksComponent,
		  MslInputWithKeyboardComponent,
		  SpoilerComponent,
		  RegisterBetButtonComponent
	  ],
		providers: [
			AppStoreService,
			HttpService,
			HttpClient,
			ZabavaLotteryService,
			StorageService,
			BarcodeReaderService,
			CameraService,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			}
		]
    })
    .compileComponents();

	  TestBed.inject(LogService);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.Draws = new LotteriesDraws();
	  appStoreService.Draws.setLottery(LotteryGameCode.Zabava, {...DRAWS_FOR_GAME_ZABAVA.lottery});

	  zabavaLotteryService = TestBed.inject(ZabavaLotteryService);
	  zabavaLotteryService.initBetData(BetDataSource.Manual);
	  zabavaLotteryService.init();

	  csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);
	  const operator = new Operator('testInstantUser', `${Date.now()}`, '123456', 1);
	  appStoreService.operator.next(operator);

	  zabavaLotteryService.currentDraw = DRAWS_FOR_GAME_ZABAVA.lottery.draws[0];
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZabavaScanBlanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onInputChangeHandler handler', () => {
		(component as any).keyboardInputSource = false;
		component.onInputChangeHandler('0662123412345678');
		expect((component as any).keyboardInputSource).toBeTruthy();
	});

	it('test onInputChangeHandler handler 2', () => {
		(component as any).keyboardInputSource = false;
		component.onInputChangeHandler('003030290000012059900562');
		expect((component as any).keyboardInputSource).toBeTruthy();
	});

	it('test onInputChangeHandler handler 3', () => {
		(component as any).keyboardInputSource = false;
		component.onInputChangeHandler('123456789123456789');
		expect((component as any).keyboardInputSource).toBeTruthy();
	});

	it('test onSwitchCamera handler', () => {
		component.isCameraShown = false;
		(component as any).keyboardInputSource = true;
		component.onSwitchCamera();
		expect(component.isCameraShown).toBeTruthy();
		expect((component as any).keyboardInputSource).toBeFalsy();
		expect(component.userInputMode).toEqual(UserInputMode.Scanner);
	});

	it('test onClearList handler', () => {
		zabavaLotteryService.blanksList = [{
			bc: '123456789123456789',
			cost: 100
		}];
		component.onClearList();
		expect(zabavaLotteryService.blanksList).toEqual([]);
	});

	it('test onRemoveItem handler', () => {
		zabavaLotteryService.blanksList = [{
			bc: '123456789123456789',
			cost: 100
		}];
		component.onRemoveItem(0);
		expect(zabavaLotteryService.blanksList).toEqual([]);
	});

	it('test onCameraClose handler', () => {
		component.isCameraShown = true;
		component.onCameraClose();
		expect(component.isCameraShown).toBeFalsy();
		expect(component.userInputMode).toEqual(UserInputMode.Scanner);
	});

	it('test onSubmit handler', () => {
		component.isFormBusy = false;
		spyOn(component, 'onSubmit').and.callThrough();
		component.onSubmit();
		expect(component.onSubmit).toHaveBeenCalled();
	});

	it('test errorsHandler handler', () => {
		spyOn(component, 'errorsHandler').and.callThrough();
		(component as any).errorsHandler({
			err_code: 2345,
			err_descr: 'Тестовая ошибка',
			client_trans_id: '12345'
		});
		expect((component as any).errorsHandler).toHaveBeenCalled();
	});
});
