import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ZabavaLotteryService} from "@app/zabava/services/zabava-lottery.service";
import {BetDataSource} from '@app/core/game/base-game.service';
import {UserInputMode} from "@app/util/utils";
import {Subject, timer} from "rxjs";
import {
	MslInputWithKeyboardComponent
} from "@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component";
import {ApplicationAppId} from "@app/core/services/store/settings";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GetBlankInfoResp} from "@app/core/net/http/api/models/get-blank-info";
import {finalize} from "rxjs/internal/operators";
import {IError} from "@app/core/error/types";
import {DialogError} from "@app/core/error/dialog";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {IResponse} from "@app/core/net/http/api/types";
import {BarcodeReaderService, IBarcodeGameTypeAnalysis} from "@app/core/barcode/barcode-reader.service";
import {takeUntil} from "rxjs/operators";
import {ScannerInputComponent} from "../../../../features/camera/components/scanner-input/scanner-input.component";

const BC_PATTERN = /^\d{18}$/;

@Component({
  selector: 'app-zabava-scan-blanks',
  templateUrl: './zabava-scan-blanks.component.html',
  styleUrls: ['./zabava-scan-blanks.component.scss']
})
export class ZabavaScanBlanksComponent implements OnInit, OnDestroy {

	readonly BetDataSource = BetDataSource;

	/**
	 * Ссылка на поле ввода штрих-кода.
	 */
	@ViewChild('barcodeInput', { static: true }) barcodeInput: MslInputWithKeyboardComponent;

	@ViewChild('scannerInput', { static: true }) scannerInput: ScannerInputComponent;

	/**
	 * Сохраненный режим ввода для каждого пользователя (вручную или со сканера).
	 */
	userInputStorage: {[userID: number]: UserInputMode};

	/**
	 * Текущий режим ввода (вручную или со сканера).
	 */
	userInputMode: UserInputMode = 0;

	/**
	 * Раскрыта ли камера?
	 */
	isCameraShown = false;

	/**
	 * Показано ли состояние ошибки на поле ввода штрих-кодов?
	 */
	showErrorState = false;

	/**
	 * Находится ли штрих-код бланка в процессе проверки?
	 */
	isFormBusy = false;

	/**
	 * Форма ввода штрих-кода
	 */
	userForm: FormGroup = this.formBuilder.group({
		barcodeInput: ['', [Validators.required, Validators.pattern(BC_PATTERN)]]
	});

	/**
	 * Источник ввода - клавиатура или пистолетный сканер
	 * @private
	 */
	private keyboardInputSource = false;

	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	get totalSum(): number {
		return ZabavaScanBlanksComponent.sumBy(this.zabavaLotteryService.blanksList, 'cost');
	}

	constructor(
		readonly appStoreService: AppStoreService,
		private readonly storageService: StorageService,
		readonly zabavaLotteryService: ZabavaLotteryService,
		private readonly formBuilder: FormBuilder,
		private readonly dialogInfoService: DialogContainerService,
		private readonly barcodeReaderService: BarcodeReaderService
	) { }

	onInputChangeHandler(event): void {
		this.keyboardInputSource = true;
		this.barcodeProcessHandler(event);
	}

	onInputFieldFocus(): void {

	}

	onBarcodeChangeHandler(bc: string): void {
		this.userForm.controls['barcodeInput'].setValue(bc);
		this.barcodeProcessHandler(bc);
	}

	onSwitchCamera(): void {
		this.isCameraShown = !this.isCameraShown;
		this.keyboardInputSource = !this.isCameraShown;
		this.userInputMode = this.isCameraShown ? UserInputMode.Scanner : UserInputMode.Manual;
		this.putIntoStorage();
		this.toggleInputMode();
		if (this.isCameraShown) {
			this.scannerInput.showScanner();
		} else {
			this.scannerInput.hideScanner();
		}
	}

	onClearList(): void {
		this.zabavaLotteryService.blanksList = [];
		this.updateGameState();
	}

	onRemoveItem(itemIndex: number): void {
		this.zabavaLotteryService.blanksList.splice(itemIndex, 1);
		this.updateGameState();
	}

	onCameraClose(): void {
		this.isCameraShown = false;
		this.scannerInput.hideScanner();
		this.userInputMode = this.isCameraShown ? UserInputMode.Manual : UserInputMode.Scanner;
		this.putIntoStorage();
		this.toggleInputMode();
	}

	onSubmit(): void {
		if (!this.isFormBusy) {
			this.barcodeProcessHandler(this.userForm.controls['barcodeInput'].value);
		}
	}

	private errorsHandler(error: IResponse | IError): void {
		this.showErrorState = true;
		const errCode = (error as IResponse).err_code || (error as IError).code;
		const message = (error as IResponse).err_descr || (error as IError).message;
		const details = (error as IError).messageDetails || '';
		const dialogError = new DialogError(errCode, message, details);
		this.dialogInfoService.showOneButtonError(dialogError, {
			click: () => {
				this.resetForm();
				this.showErrorState = false;
				if (this.keyboardInputSource) {
					this.setInputFocus();
				}
			},
			text: 'dialog.dialog_button_continue'
		});
	}

	private resetForm(): void {
		const tmr = timer(50)
			.subscribe(() => {
				this.userForm.reset();
				tmr.unsubscribe();
			});
	}

	private setInputFocus(nDelay = 50): void {
		const tmr = timer(nDelay)
			.subscribe(() => {
				this.barcodeInput.setFocus();
				tmr.unsubscribe();
			});
	}

	private removeInputFocus(nDelay = 50): void {
		const tmr = timer(nDelay)
			.subscribe(() => {
				this.barcodeInput.removeFocus();
				tmr.unsubscribe();
			});
	}

	private barcodeProcessHandler(bc: string): void {
		if (BC_PATTERN.test(bc)) {
			if (this.zabavaLotteryService.blanksList.find(v => v.bc === bc)) {
				this.resetForm();
			} else {
				this.showErrorState = false;
				this.isFormBusy = true;
				this.zabavaLotteryService.getBlankInfo(+this.zabavaLotteryService.currentDraw.draw.code, bc)
					.pipe(
						finalize(() => {
							this.isFormBusy = false;
						})
					)
					.subscribe((resp: GetBlankInfoResp) => {
						if (resp.err_code === 0) {
							this.zabavaLotteryService.blanksList.push({ bc, cost: resp.price / 100, add_comb_cnt: resp.add_comb_cnt });
							this.updateGameState();
							this.zabavaLotteryService.blanksModeData = {
								betsCount: this.zabavaLotteryService.blanksList.length,
								pairsCount: ZabavaScanBlanksComponent.sumBy(this.zabavaLotteryService.blanksList, 'add_comb_cnt'),
								drawIndex: this.zabavaLotteryService.drawIndex,
								amount: this.totalSum,
								drawNum: this.zabavaLotteryService.currentDraw.draw.num,
								macCode: this.zabavaLotteryService.blanksList.map(v => v.bc).join(';')
							};
							this.resetForm();
							if (this.keyboardInputSource) {
								this.setInputFocus();
							} else {
								this.removeInputFocus();
							}
						} else {
							this.errorsHandler(resp);
						}
					}, this.errorsHandler.bind(this));
			}
		} else if (bc?.length === 0) {
			this.showErrorState = false;
		}
	}

	private updateGameState(): void {
		this.zabavaLotteryService.betData.drawCount = 1;
		this.zabavaLotteryService.betData.betsCount = this.zabavaLotteryService.blanksList.length;
		this.zabavaLotteryService.betData.pairsCount = ZabavaScanBlanksComponent.sumBy(this.zabavaLotteryService.blanksList, 'add_comb_cnt');
		this.zabavaLotteryService.refreshBets();
	}

	private toggleInputMode(): void {
		this.keyboardInputSource = !this.isCameraShown;
		if (this.isCameraShown) {
			this.removeInputFocus();
		} else {
			this.setInputFocus();
		}
	}

	ngOnInit(): void {
		const uInputStorage = this.storageService.getSync(ApplicationAppId, ['user_input_mode']);
		this.userInputStorage = uInputStorage && uInputStorage.data && (uInputStorage.data.length > 0) ?
			JSON.parse(uInputStorage.data[0].value) : {};
		this.userInputMode = this.userInputStorage[this.appStoreService.operator.value.userId] ?
			this.userInputStorage[this.appStoreService.operator.value.userId] : UserInputMode.Scanner;
		this.isCameraShown = this.userInputMode === UserInputMode.Scanner;
		this.toggleInputMode();
		this.updateGameState();
		this.barcodeReaderService.barcodeSubject$$
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe((bcObject: IBarcodeGameTypeAnalysis) => {
				this.onCameraClose();
				this.keyboardInputSource = false;
				this.removeInputFocus(100);
				if (!this.isFormBusy) {
					this.userForm.controls['barcodeInput'].setValue(bcObject.barcode);
					this.barcodeProcessHandler(bcObject.barcode);
				}
			});
		this.appStoreService.showCameraComponent$$
			.pipe(
				takeUntil(this.unsubscribe$$)
			)
			.subscribe((showCameraComponent) => {
			if (showCameraComponent) {
				if (this.isCameraShown) {
					this.scannerInput.showScanner();
				}
			} else {
				this.scannerInput.hideScanner();
			}
		});
	}

	ngOnDestroy(): void {
		// this.zabavaLotteryService.betData.betsCount = 0;
		// this.zabavaLotteryService.betData.pairsCount = 0;
		// this.zabavaLotteryService.amount = 0;
		// this.zabavaLotteryService.betsCount = 0;
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

	/**
	 * Сохранить режим ввода штрих-кода в браузерное хранилище.
	 * @private
	 */
	private putIntoStorage(): void {
		this.userInputStorage[this.appStoreService.operator.value.userId] = this.userInputMode;
		this.storageService.putSync(ApplicationAppId, [{
			key: 'user_input_mode',
			value: JSON.stringify(this.userInputStorage)
		}]);
	}

	private static sumBy(arr: Array<any>, field: string): number {
		let sum = 0;
		for (const arrItem of arr) {
			sum += (arrItem[field] || 0);
		}
		return sum;
	}

}
