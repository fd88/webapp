import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SharedModule } from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { BetDataSource } from '@app/core/game/base-game.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ZabavaLotteryInitComponent } from '@app/zabava/components/zabava-lottery-init/zabava-lottery-init.component';
import { ZabavaLotteryService } from '@app/zabava/services/zabava-lottery.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {DRAWS_FOR_GAME_ZABAVA} from "../../../../features/mocks/zabava-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {ROUTES} from "../../../../../app-routing.module";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient} from "@angular/common/http";

xdescribe('ZabavaLotteryInitComponent', () => {
	let component: ZabavaLotteryInitComponent | any;
	let appStoreService: AppStoreService;
	let fixture: ComponentFixture<ZabavaLotteryInitComponent>;
	let zabavaLotteryService: ZabavaLotteryService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes(ROUTES),
				SharedModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				}),
				HttpClientTestingModule,
			],
			declarations: [
				ZabavaLotteryInitComponent
			],
			providers: [
				LogService,
				LotteriesService,
				ZabavaLotteryService,
				HttpService,
				AppStoreService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);

	}));

	beforeEach(() => {
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Zabava, {...DRAWS_FOR_GAME_ZABAVA.lottery});

		zabavaLotteryService = TestBed.inject(ZabavaLotteryService);
		zabavaLotteryService.initBetData(BetDataSource.Manual);
		zabavaLotteryService.init();

		fixture = TestBed.createComponent(ZabavaLotteryInitComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	function prepareData(): void {
		component.zabavaLotteryService.betData.drawCount = 1;
		component.zabavaLotteryService.betData.drawIndex = 0;
		component.zabavaLotteryService.betData.pairsCount = 1;
	}

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectedPairsHandler', () => {
		prepareData();
		component.onSelectedPairsHandler({
			index: 0,
			label: '1',
			selected: true,
			disabled: false
		});
		expect(component.zabavaLotteryService.betData.pairsCount).toEqual(1);

		component.onSelectedPairsHandler(undefined);
		expect(component.zabavaLotteryService.betData.pairsCount).toEqual(0);
	});

	it('test onSelectedTicketHandler', () => {
		prepareData();
		component.onSelectedTicketHandler({
			index: 0,
			label: '1',
			selected: true,
			disabled: false
		});
		expect(component.zabavaLotteryService.betData.betsCount).toEqual(1);

		component.onSelectedTicketHandler(undefined);
		expect(component.zabavaLotteryService.betData.betsCount).toEqual(0);
	});

	it('test onSelectedDrawHandler', () => {
		component.onSelectedDrawHandler(0);
		expect(component.currentDrawNum).toEqual('1305');

		component.zabavaLotteryService.activeDraws[0].draw.jackpot = 'test';
		component.onSelectedDrawHandler(0);
		expect(component.currentDrawNum).toEqual('1305');
	});

	it('test onSelectedDrawHandler', () => {
		prepareData();
		component.onNoActiveDrawsHandler();
		expect(component.zabavaLotteryService.drawIndex).toEqual(-1);
	});

	it('test onChangeStudioHandler', () => {
		prepareData();
		component.zabavaLotteryService.betData.hasExtraGame = false;

		component.onChangeStudioHandler({
			index: 0,
			label: 'lottery.zabava.pair',
			selected: true,
			disabled: false
		});
		expect(component.zabavaLotteryService.betData.hasExtraGame).toBeTruthy();
	});

	it('test onChangeStudioHandler', () => {
		prepareData();
		component.zabavaLotteryService.betData.hasExtraGame = false;

		component.onChangeStudioHandler({
			index: 0,
			label: 'lottery.zabava.pair',
			selected: true,
			disabled: false
		});
		expect(component.zabavaLotteryService.betData.hasExtraGame).toBeTruthy();
	});

	it('test zabavaInit', () => {
		prepareData();
		component.zabavaLotteryService.betData.pairsCount = 1;
		component.zabavaLotteryService.betData.betsCount = 1;
		component.zabavaLotteryService.betData.hasExtraGame = true;
		component.zabavaInit();
		expect(component.zabavaLotteryService.amount).toEqual(17);
	});
});
