import {Component, OnInit} from '@angular/core';
import {ZabavaLotteryService} from "@app/zabava/services/zabava-lottery.service";

/**
 * Компонент ввода лотереи "Забава".
 */
@Component({
	selector: 'app-zabava-lottery-init',
	templateUrl: './zabava-lottery-init.component.html',
	styleUrls: ['./zabava-lottery-init.component.scss']
})
export class ZabavaLotteryInitComponent {

}
