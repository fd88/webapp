import { IBetDataItem } from '@app/core/game/base-game.service';

/**
 * Интерфейс данных ставки для лотереи "Забава".
 */
export interface IZabavaBetDataItem extends IBetDataItem {

	/**
	 * Выбранное количество билетов.
	 */
	betsCount: number;

	/**
	 * Выбранное количество парочек.
	 */
	pairsCount: number;

	/**
	 * Признак наличия бонусной игры "Багаті та відомі".
	 */
	hasExtraGame: boolean;

	/**
	 * Индекс выбранного тиража.
	 */
	drawIndex: number;

}
