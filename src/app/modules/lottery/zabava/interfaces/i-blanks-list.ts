export interface IBlanksList {
	bc: string;
	cost: number;
	add_comb_cnt?: number;
}
