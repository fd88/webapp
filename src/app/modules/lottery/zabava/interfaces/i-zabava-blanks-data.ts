export interface IZabavaBlanksData {
	betsCount: number;
	pairsCount: number;
	drawIndex: number;
	amount: number;
	drawNum: string;
	macCode: string;
}
