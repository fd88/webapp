import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';

import { ZabavaRoutingModule } from '@app/zabava/zabava-routing.module';

import { ZabavaLotteryService } from '@app/zabava/services/zabava-lottery.service';

import { ZabavaLotteryContentComponent } from '@app/zabava/components/zabava-lottery-content/zabava-lottery-content.component';
import { ZabavaLotteryInitComponent } from '@app/zabava/components/zabava-lottery-init/zabava-lottery-init.component';
import { ZabavaResultsComponent } from '@app/zabava/components/zabava-results/zabava-results.component';
import { ZabavaScanBlanksComponent } from './components/zabava-scan-blanks/zabava-scan-blanks.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CameraModule} from "../../features/camera/camera.module";

/**
 * Модуль лотереи "Забава".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
    imports: [
        CommonModule,
        ZabavaRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        CameraModule
    ],
	declarations: [
		ZabavaLotteryContentComponent,
		ZabavaLotteryInitComponent,
		ZabavaResultsComponent,
  ZabavaScanBlanksComponent
	],
	providers: [
		ZabavaLotteryService
	]
})
export class ZabavaModule {}
