import { IMegalotBetsBalls } from '@app/core/net/http/api/models/megalot-reg-bet';
import { IBetDataItem } from '@app/core/game/base-game.service';

/**
 * Интерфейс модели данных со списком ставок по игре "Мегалот".
 */
export interface IMegalotBetDataItem extends IBetDataItem {
	/**
	 * Массив ставок.
	 */
	bets: Array<IMegalotBetsBalls>;
}
