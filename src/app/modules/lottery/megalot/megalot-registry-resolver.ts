import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BetDataSource, GameSaleMode } from '@app/core/game/base-game.service';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';

/**
 * Резолвер данных для отображения информации о ручной ставке в "Мегалот".
 */
@Injectable()
export class MegalotRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {MegalotLotteryService} megalotLotteryService Сервис игры "Мегалот".
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 */
	constructor(
		private readonly megalotLotteryService: MegalotLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		// проверить наличие акционных параметров
		const parsedUrl = this.router.parseUrl(state.url);
		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET) || parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			let betData;
			if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
				const actionBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionsButton;
				betData = actionBet.data;
			}

			if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
				const demoBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET));
				betData = demoBet.data;
			}

			this.megalotLotteryService.initBetData(BetDataSource.Manual);
			this.megalotLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			this.megalotLotteryService.betData.drawCount = Number.isInteger(betData.drawCount) ? betData.drawCount : 1;
			const ballCount = Number.isInteger(betData.ballCount) ? betData.ballCount : 6;
			const megaBallCount = Number.isInteger(betData.megaBallCount) ? betData.megaBallCount : 1;
			const betCount = Number.isInteger(betData.betCount) ? betData.betCount : 1;
			Array.from(Array(betCount))
				.forEach(() => this.megalotLotteryService.generateRandomBet(true, ballCount));
			this.megalotLotteryService.refreshBets();
		}

		if (!this.megalotLotteryService.activeDraws || !this.megalotLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.megalotLotteryService
			};
		}

		return {
			checkInfoRowList: [
				{
					langKeyName: 'lottery.draws_count',
					value: `${this.megalotLotteryService.betData.drawCount}`
				},
				{
					langKeyName: 'lottery.bets_count',
					value: `${this.megalotLotteryService.betData.bets.length}`
				},
				{
					langKeyName: 'lottery.sum',
					value: this.megalotLotteryService.formattedAmount,
					redColor: true,
					valueIsCurrency: true
				}
			],
			baseGameService: this.megalotLotteryService
		};
	}

}
