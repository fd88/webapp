import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { URL_INIT, URL_REGISTRY, URL_RESULTS, URL_SCANNER } from '@app/util/route-utils';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { MegalotLotteryContentComponent } from '@app/megalot/components/megalot-lottery-content/megalot-lottery-content.component';
import { MegalotLotteryInitComponent } from '@app/megalot/components/megalot-lottery-init/megalot-lottery-init.component';
import { MegalotResultsComponent } from '@app/megalot/components/megalot-results/megalot-results.component';
import { MegalotRegistryResolver } from '@app/megalot/megalot-registry-resolver';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

const routes: Routes = [
	{
		path: '',
		component: MegalotLotteryContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: URL_INIT,
				component: MegalotLotteryInitComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: MegalotRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_RESULTS,
				component: MegalotResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}
];

/**
 * Модуль маршрутизации лотереи "Мегалот".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		MegalotRegistryResolver
	]
})
export class MegalotRoutingModule {}
