import { Component } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDrawingResult, IWinCat } from '@app/core/net/http/api/models/get-draw-results';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { IAbstractViewResults, IResultPrintItem, PrintResultTypes } from '@app/core/services/results/results-utils';

/**
 * Модель данных для отображения результатов по "Мегалоту".
 */
interface IMegalotResults extends IAbstractViewResults {
	/**
	 * Номера выигрышных шаров.
	 */
	balls: Array<number>;
	/**
	 * Номер выигрышного мега-шара.
	 */
	megaBall: number;

	/**
	 * Список категорий выигрышей.
	 */
	winCat: Array<IWinCat>;
}

/**
 * Компонент результатов по "Мегалоту".
 */
@Component({
	selector: 'app-megalot-results',
	templateUrl: './megalot-results.component.html',
	styleUrls: ['./megalot-results.component.scss']
})
export class MegalotResultsComponent  {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Данные для отображения результатов.
	 */
	viewData: IMegalotResults;

	/**
	 * Данные для печати результатов.
	 */
	printData: Array<IResultPrintItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GameResultsService} gameResultsService Сервис результатов по играм.
	 */
	constructor(
		readonly gameResultsService: GameResultsService
	) {}

	/**
	 * Обработчик нажатия выбора результатов по тиражу.
	 *
	 * @param {IDrawingResult} result Результаты по тиражу.
	 */
	onSelectedResultsHandler(result: IDrawingResult): void {
		if (!result) {
			this.viewData = undefined;
			this.printData = undefined;

			return;
		}

		const drawNumber = result.draw_name;
		const drawDate = result.drawing_date_begin;
		const extraInfo = result.drawing[0].extra_info;
		let balls: Array<number>;
		let megaBall: number;
		let winCat: Array<IWinCat>;

		const main = result.drawing[0];

		if (main.win_comb) {
			const arr = main.win_comb.split(' ');

			if (arr[0] && arr[0].length) {
				balls = arr[0].split(',')
					.map(b => {
						return parseInt(b, 10);
					});
			}

			if (arr[1] && arr[1].length) {
				megaBall = parseInt(arr[1].replace(/:/g, ''), 10);
			}
		}

		if (Array.isArray(main.win_cat)) {
			winCat = main.win_cat;
		}

		this.viewData = {drawNumber, drawDate, extraInfo, balls, megaBall, winCat};
		this.printData = this.printDataParser();
	}

	/**
	 * Функция-трекер для отслеживания изменений в массиве шаров.
	 * @param index Индекс элемента в массиве.
	 * @param item Элемент массива.
	 */
	trackByBallsFn = (index, item: number) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Функция-парсер данных для печати.
	 */
	private printDataParser(): Array<IResultPrintItem> {
		const result = [
			{key: PrintResultTypes.LotteryName, value: ['lottery.megalot.megalot_name']},
			{key: PrintResultTypes.DrawNumber, value: [this.viewData.drawNumber]},
			{key: PrintResultTypes.DrawDate, value: [this.viewData.drawDate]}
		];

		// шары
		if (Array.isArray(this.viewData.balls) && this.viewData.balls.length > 0) {
			const arr = this.viewData.balls.map(m => `${m}`);
			arr.push(': ', 'lottery.megalot.megaball', `- ${this.viewData.megaBall}`);
			result.push({
				key: PrintResultTypes.WinCombWithLines,
				value: arr
			});
		}

		// категории выигрыша
		if (this.viewData.winCat.length > 0) {
			result.push({
				key: PrintResultTypes.TwoColumnTable,
				value: ['lottery.category', 'lottery.win']
			});

			this.viewData.winCat
				.forEach(v => {
					result.push({
						key: PrintResultTypes.TwoColumnTable,
						value: [v.name, v.win_sum]
					});
				});
		}

		result.push({key: PrintResultTypes.Line, value: undefined});

		return result;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
