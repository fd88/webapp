import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MegalotResultsComponent } from './megalot-results.component';
import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { HttpService } from '@app/core/net/http/services/http.service';


import { ReportsService } from '@app/core/services/report/reports.service';
import { DatePipe } from '@angular/common';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';

describe('MegalotResultsComponent', () => {
	let component: MegalotResultsComponent;
	let fixture: ComponentFixture<MegalotResultsComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				SharedModule,
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot({})
			],
			declarations: [
				MegalotResultsComponent
			],
			providers: [
				GameResultsService,
				HttpService,


				ReportsService,
				DatePipe,
				MslCurrencyPipe,
				AppStoreService,
				StorageService,
				TransactionService,
				PrintService,
				LotteriesService,
				LogService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				TranslateService
			]

		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(MegalotResultsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectedResultsHandler', () => {
		component.onSelectedResultsHandler(undefined);
		expect(component.viewData).toBeUndefined();

		component.onSelectedResultsHandler({
			draw_code: 1481,
			draw_name: '1059',
			drawing: [
				{
					extra_info: 'Мега джекпот наступн.тиражу - 230,000 грн.',
					name: 'Основний розiграш',
					data: {
						events: []
					},
					win_cat: [
						{
							name: 'за 6+1:',
							win_cnt: '0',
							win_sum: '0.00'
						},
						{
							name: 'за 6:',
							win_cnt: '0',
							win_sum: '0.00'
						},
						{
							name: 'за 5:',
							win_cnt: '0',
							win_sum: '0.00'
						},
						{
							name: 'за 4:',
							win_cnt: '2',
							win_sum: '8.00'
						},
						{
							name: 'за 3:',
							win_cnt: '3',
							win_sum: '7.00'
						},
						{
							name: '5+1:',
							win_cnt: '5',
							win_sum: '151.00'
						},
						{
							name: '4+1:',
							win_cnt: '50',
							win_sum: '16.00'
						},
						{
							name: '3+1:',
							win_cnt: '101',
							win_sum: '14.00'
						}
					],
					win_comb: '1,2,3,4,9,11 :5'
				}
			],
			drawing_date_begin: '2019-06-25 13:05:51',
			drawing_date_end: '2021-11-20 15:05:50',
			winning_date_end: '2021-11-20 15:05:50'
		});


		expect(component.viewData).toEqual({
			drawNumber: '1059',
			drawDate: '2019-06-25 13:05:51',
			extraInfo: 'Мега джекпот наступн.тиражу - 230,000 грн.',
			balls: [1, 2, 3, 4, 9, 11],
			megaBall: 5,
			winCat: [{
				name: 'за 6+1:',
				win_cnt: '0',
				win_sum: '0.00'
			}, {
				name: 'за 6:',
				win_cnt: '0',
				win_sum: '0.00'
			}, {
				name: 'за 5:',
				win_cnt: '0',
				win_sum: '0.00'
			}, {
				name: 'за 4:',
				win_cnt: '2',
				win_sum: '8.00'
			}, {
				name: 'за 3:',
				win_cnt: '3',
				win_sum: '7.00'
			}, {
				name: '5+1:',
				win_cnt: '5',
				win_sum: '151.00'
			}, {
				name: '4+1:',
				win_cnt: '50',
				win_sum: '16.00'
			}, {
				name: '3+1:',
				win_cnt: '101',
				win_sum: '14.00'
			}]
		});
	});

	it('test trackByBallsFn', () => {
		expect(component.trackByBallsFn(1, 5)).toEqual(1);
	});

});
