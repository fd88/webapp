import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';

import { HttpService } from '@app/core/net/http/services/http.service';
import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';

import { MegalotLotteryContentComponent } from '@app/megalot/components/megalot-lottery-content/megalot-lottery-content.component';
import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('MegalotLotteryContentComponent', () => {
	let component: MegalotLotteryContentComponent;
	let fixture: ComponentFixture<MegalotLotteryContentComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			declarations: [
				MegalotLotteryContentComponent
			],
			providers: [
				DeclineWordPipe,
				MegalotLotteryService,
				HttpService,
				LogService,
				LotteriesService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MegalotLotteryContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
