import { Component, OnDestroy } from '@angular/core';
import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';

/**
 * Компонент-контейнер для компонентов лотереи Мегалот.
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class MegalotLotteryContentComponent implements OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {MegalotLotteryService} megalotLotteryService Сервис для работы с лотереей Мегалот.
	 */
	constructor(
		private readonly megalotLotteryService: MegalotLotteryService,
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.megalotLotteryService.resetBetData();
	}

}
