import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { IMegalotBetsBalls } from '@app/core/net/http/api/models/megalot-reg-bet';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';
import { OneButtonCustomComponent } from '@app/shared/components/one-button-custom/one-button-custom.component';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource } from '@app/core/game/base-game.service';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonTextStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';

/**
 * Компонент ввода лотереи "МЕГАЛОТ".
 */
@Component({
	selector: 'app-megalot-lottery-init',
	templateUrl: './megalot-lottery-init.component.html',
	styleUrls: ['./megalot-lottery-init.component.scss']
})
export class MegalotLotteryInitComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Constants
	// -----------------------------
	/**
	 * Стиль группы кнопок.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Режим работы группы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Стиль текста на кнопках.
	 */
	readonly ButtonTextStyle = ButtonTextStyle;

	/**
	 * Источник данных для ставки.
	 */
	readonly BetDataSource = BetDataSource;

	/**
	 * Код игры.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Содержит ссылку на компонент группы кнопок "Тиражи".
	 */
	@ViewChild('drawsComponent', { static: true })
	drawsComponent: ButtonsGroupComponent;

	/**
	 * Содержит ссылку на компонент группы кнопок "Номера шариков".
	 */
	@ViewChild('numbersComponent', { static: true })
	numbersComponent: ButtonsGroupComponent;

	/**
	 * Содержит ссылку на компонент группы кнопок "Мегашарики".
	 */
	@ViewChild('megaballsComponent', { static: true })
	megaballsComponent: ButtonsGroupComponent;

	/**
	 * Содержит ссылку на компонент диалогового окна добавления новой ставки.
	 */
	@ViewChild('manualInput', { static: true })
	manualInput: OneButtonCustomComponent;

	/**
	 * Содержит количество максимально разрешенных тиражей на данный момент времени.
	 */
	countEnabledDraws: number;

	/**
	 * Признак валидности введенных шариков для добавления новой ставки.
	 */
	isValidInput: boolean;

	/**
	 * Количество выбранных шариков.
	 */
	balls = 0;

	/**
	 * Количество выбранных мегашариков.
	 */
	megaballs = 0;

	/**
	 * мегашарик(-а,-ов).
	 */
	strMegaballs = 'lottery.megalot.megaballs_5_10';

	/**
	 * варианты сумм ставок в диалоге
	 */
	betsSums: Array<number>;

	/**
	 * текущая редактируемая ставка
	 * -1 - добавление
	 */
	currentBetIndex = -1;

	/**
	 * предыдущий выбор мегашариков
	 */
	prevMBallsSelection = [];

	/**
	 * Джекпот
	 */
	jackpot: string;

	/**
	 * Мегаприз
	 */
	megaPrize: string;

	/**
	 * Выбраны все мегашарики
	 */
	allMegaBallsSelected = false;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 * @param {DialogContainerService} dialogInfoService Сервис для работы с диалоговыми окнами.
	 * @param {MegalotLotteryService} megalotLotteryService Сервис для работы с лотереей "Мегалот".
	 */
	constructor(
		private readonly router: Router,
		private readonly dialogInfoService: DialogContainerService,
		readonly megalotLotteryService: MegalotLotteryService
	) {}

	/**
	 * Обработчик нажатия кнопки в группе "Тиражи".
	 *
	 * @param {IButtonGroupItem} button Кнопка, по которой был произведен клик.
	 */
	onSelectedDrawsHandler(button: IButtonGroupItem): void {
		this.megalotLotteryService.betData.drawCount = button ? button.index + 1 : 0;
		this.megalotLotteryService.refreshBets();
	}

	/**
	 * Обработчик нажатия кнопки в группе "Номера".
	 *
	 * @param {IButtonGroupItem} button Кнопка, по которой был произведен клик.
	 */
	onSelectedNumberHandler(button: IButtonGroupItem): void {
		this.validateInput();
		if (this.balls < this.megalotLotteryService.MAX_BALLS_IN_BET) {
			this.numbersComponent.buttons.forEach(btn => btn.disabled = false);
		}
		this.updateSumsVariants();
	}

	/**
	 * Обработчик нажатия кнопки в группе "Мегашарики".
	 *
	 * @param {IButtonGroupItem} button Кнопка, по которой был произведен клик.
	 */
	onSelectedMegaballHandler(button: IButtonGroupItem): void {
		this.validateInput();
		if (!button.selected) {
			this.allMegaBallsSelected = false;
		}
		// перечитать мегашарики
		this.prevMBallsSelection = this.megaballsComponent.buttons.filter(p => p.selected)
			.map(v => Number.parseInt(v.label, 10));
		if (this.megaballs === this.megalotLotteryService.MEGA_BALLS_COUNT) {
			this.allMegaBallsSelected = true;
			this.prevMBallsSelection = [];
		}

		this.updateSumsVariants();
	}

	/**
	 * Слушатель превышения максимального количества выбранных номеров.
	 *
	 * @param {IButtonGroupItem} $event Передаваемый объект.
	 */
	onMaximumNumbersHandler($event: IButtonGroupItem): void {
		this.numbersComponent.buttons.forEach(btn => btn.disabled = !btn.selected);
	}

	/**
	 * Выполняет очистку формы с шариками и мегашариками.
	 */
	onClearInputHandler(): void {
		this.numbersComponent.clearButtons();
		this.megaballsComponent.clearButtons();
		this.allMegaBallsSelected = false;
		this.prevMBallsSelection = [];
		this.validateInput();
	}

	/**
	 * Слушатель нажатия кнопки "АВТО".
	 * Формирует ставку на 6-10 шариков и 1 мегакульку.
	 */
	onAutoButtonClickHandler(): void {
		const bet = this.megalotLotteryService.generateRandomBet(false);
		this.addBet(bet);
	}

	/**
	 * Обработчик нажатия кнопки редактирования ставки.
	 * @param evt Передаваемое событие.
	 * @param betIndex Индекс ставки.
	 */
	onManualClickHandler(evt, betIndex): void {
		this.currentBetIndex = betIndex;
		if (betIndex > -1) {
			this.balls = this.megalotLotteryService.betData.bets[betIndex].balls.length;
			this.megaballs = this.megalotLotteryService.betData.bets[betIndex].megaballs.length;
			if (this.balls === this.megalotLotteryService.MAX_BALLS_IN_BET) {
				for (let i = 0; i < this.megalotLotteryService.BALLS_COUNT; i++) {
					this.numbersComponent.buttons[i].disabled = true;
				}
			}

			this.prevMBallsSelection = [...this.megalotLotteryService.betData.bets[betIndex].megaballs];
			this.megalotLotteryService.betData.bets[betIndex].balls.forEach(ball => {
				this.numbersComponent.buttons[ball - 1].selected = true;
				this.numbersComponent.buttons[ball - 1].disabled = false;
			});
			this.megalotLotteryService.betData.bets[betIndex].megaballs.forEach(mball => {
				this.megaballsComponent.buttons[mball].selected = true;
			});
			this.allMegaBallsSelected = this.megaballs === this.megalotLotteryService.MEGA_BALLS_COUNT;
		}
		this.validateInput();
		this.updateSumsVariants();
		this.manualInput.show();
	}

	/**
	 * Слушатель нажатия кнопки добавления ставки с выбранными шариками.
	 */
	onAddEditBetClickHandler(): void {
		this.manualInput.hide();
		const balls = this.numbersComponent.buttons.filter(p => p.selected)
			.map(v => Number.parseInt(v.label, 10));
		const megaballs = this.megaballsComponent.buttons.filter(p => p.selected)
			.map(v => Number.parseInt(v.label, 10));

		const input_mode = 2;
		if (this.currentBetIndex === -1) {
			this.addBet({balls, megaballs, input_mode});
		} else {
			this.megalotLotteryService.betData.bets[this.currentBetIndex].balls = [...balls];
			this.megalotLotteryService.betData.bets[this.currentBetIndex].megaballs = [...megaballs];
			this.megalotLotteryService.betData.bets[this.currentBetIndex].input_mode = input_mode;
		}

		this.onClearInputHandler();
		this.megalotLotteryService.refreshBets();
	}

	/**
	 * Слушатель нажатия кнопки удаления ставки из списка ставок.
	 *
	 * @param {number} index Индекс удаляемой ставки из списка ставок.
	 */
	onRemoveBetClickHandler(index: number): void {
		this.megalotLotteryService.removeBet(index);
		this.megalotLotteryService.refreshBets();
	}

	/**
	 * Смахивание (удаление) ставки из списка ставок.
	 * @param bet Ставка.
	 */
	onSwipeBetHandler(bet: IMegalotBetsBalls): void {
		const idx = this.megalotLotteryService.betData.bets.indexOf(bet);
		this.onRemoveBetClickHandler(idx);
	}

	/**
	 * Слушатель нажатия кнопки выбора всех мегашариков.
	 */
	onClickAllMegaballsHandler(): void {
		if (this.allMegaBallsSelected) {
			for (let i = 0; i < this.megalotLotteryService.MEGA_BALLS_COUNT; i++) {
				if (!this.megaballsComponent.buttons[i].selected) {
					this.megaballsComponent.selectButtonByIndex(i);
				}
			}
			this.megaballs = this.megalotLotteryService.MEGA_BALLS_COUNT;
			this.updateSumsVariants();
		} else {
			let i: number;
			for (i = 0; i < this.megalotLotteryService.MEGA_BALLS_COUNT; i++) {
				if (this.megaballsComponent.buttons[i].selected) {
					this.megaballsComponent.selectButtonByIndex(i);
				}
			}
			if (this.prevMBallsSelection.length === this.megalotLotteryService.MEGA_BALLS_COUNT) {
				this.prevMBallsSelection = [];
			}
			this.megaballs = this.prevMBallsSelection.length;
			for (i = 0; i < this.megaballs; i++) {
				if (!this.megaballsComponent.buttons[this.prevMBallsSelection[i]].selected) {
					this.megaballsComponent.selectButtonByIndex(this.prevMBallsSelection[i]);
				}
			}
		}

		this.validateInput();
	}

	/**
	 * Функция для отслеживания изменений в массиве ставок.
	 * @param index Индекс элемента в массиве.
	 */
	trackByBets = (index) => index;

	/**
	 * Функция для отслеживания изменений в массиве шариков.
	 * @param index Индекс элемента в массиве.
	 */
	trackByBalls = (index) => index;

	/**
	 * Функция для отслеживания изменений в массиве мегашариков.
	 * @param index Индекс элемента в массиве.
	 */
	trackByMegaballs = (index: number) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Обновление возможных вариантов ставок и их сумм.
	 * @private
	 */
	private updateSumsVariants(): void {
		let drBetSum = '0';
		let drBetESum = '0';
		if (Array.isArray(this.megalotLotteryService.activeDraws) && this.megalotLotteryService.activeDraws.length > 0) {
			const firstDraw = this.megalotLotteryService.activeDraws[0];
			drBetSum = firstDraw.draw.bet_sum;
			drBetESum = firstDraw.draw.data.mega.extra_sum;
		}

		if (this.megaballs > 0) {
			this.betsSums = [
				this.megalotLotteryService.getBetSum(6, this.megaballs, drBetSum, drBetESum, true),
				this.megalotLotteryService.getBetSum(7, this.megaballs, drBetSum, drBetESum, true),
				this.megalotLotteryService.getBetSum(8, this.megaballs, drBetSum, drBetESum, true),
				this.megalotLotteryService.getBetSum(9, this.megaballs, drBetSum, drBetESum, true),
				this.megalotLotteryService.getBetSum(10, this.megaballs, drBetSum, drBetESum, true)
			];
		}
	}

	/**
	 * Добавляет ставку в массив.
	 *
	 * @param {IMegalotBetsBalls} bet Ставка.
	 */
	private addBet(bet: IMegalotBetsBalls): void {
		if (this.megalotLotteryService.betData.bets.length < this.megalotLotteryService.MAX_BETS) {
			this.megalotLotteryService.betData.bets.unshift(bet);
			this.megalotLotteryService.refreshBets();
			this.onClearInputHandler();
		} else {
			this.callInfoDialog('dialog.attention', 'dialog.maximum_10_bets');
		}
	}

	/**
	 * Функция для отображения диалога с информацией.
	 * @param title Заголовок диалога.
	 * @param msg Текст сообщения.
	 * @private
	 */
	private callInfoDialog(title: string, msg: string): void {
		this.dialogInfoService.showOneButtonInfo(title, msg, {text: 'dialog.dialog_button_continue'});
	}

	/**
	 * Функция валидации введенных шариков для возможности добавления в список ставок.
	 */
	private validateInput(): void {
		if (Array.isArray(this.numbersComponent.buttons) && Array.isArray(this.megaballsComponent.buttons)) {
			this.balls = this.numbersComponent.buttons.filter(p => p.selected).length;
			this.megaballs = this.megaballsComponent.buttons.filter(p => p.selected).length;

			this.strMegaballs = (this.megaballs === 1)
				? 'lottery.megalot.megaballs_1'
				: (this.megaballs >= 2 && this.megaballs <= 4)
					? 'lottery.megalot.megaballs_2_4'
					: 'lottery.megalot.megaballs_5_10';
			this.isValidInput = this.balls >= this.megalotLotteryService.MIN_BALLS_IN_BET && this.megaballs > 0;
		}
	}

	/**
	 * Функция автообновления тиражей.
	 */
	private autoUpdateDraws(): void {
		this.megalotLotteryService.updateActiveDraws();

		// проверить изменилось ли макс кол-во доступных тиражей
		const actualCount = this.megalotLotteryService.activeDraws.length;
		if (actualCount !== this.countEnabledDraws) {
			const drawCount = this.megalotLotteryService.betData.drawCount;
			Logger.Log.i('MegalotLotteryInitComponent',
				`actual draws list changed from (${this.countEnabledDraws}) to (${actualCount})`)
				.console();

			// если тиражей стало меньше, проверить и пересчитать текущий выбранный тираж
			if (this.countEnabledDraws > actualCount && drawCount > actualCount) {

				Logger.Log.i('MegalotLotteryInitComponent', `selected draw ${drawCount} will decreased!`)
					.console();

				this.megalotLotteryService.betData.drawCount = actualCount;
				this.drawsComponent.selectButtonByIndex(actualCount - 1);
			}

			this.countEnabledDraws = actualCount;
			this.megalotLotteryService.refreshBets();
		}
	}

	/**
	 * Функция задает или восстанавливает первый ближайший тираж
	 */
	private restoreDraw(): void {
		// восстанавливаем или задаем первый ближайший тираж
		if (this.countEnabledDraws > 0) {
			const firstDraw = this.megalotLotteryService.activeDraws[0];
			this.drawsComponent.selectedButtons = this.megalotLotteryService.betData.drawCount <= this.countEnabledDraws
				? this.megalotLotteryService.betData.drawCount - 1
				: this.countEnabledDraws - 1;
			if (firstDraw.draw.jackpot) {
				const intJackpot = parseInt(firstDraw.draw.jackpot, 10);
				this.jackpot = isNaN(intJackpot) ? '' : intJackpot.toLocaleString();
			}
			if (firstDraw.draw.data.mega_prize) {
				const intMegaPrize = parseInt(firstDraw.draw.data.mega_prize, 10);
				this.megaPrize = isNaN(intMegaPrize) ? '' : intMegaPrize.toLocaleString();
			}
		}

	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.megalotLotteryService.initBetData(BetDataSource.Manual);
		this.countEnabledDraws = this.megalotLotteryService.activeDraws.length;

		this.restoreDraw();

		this.validateInput();
		this.megalotLotteryService.refreshBets();

		// подписаться на таймер автообновления тиражей
		timer(0, 1000)
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(this.autoUpdateDraws.bind(this));
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}
}
