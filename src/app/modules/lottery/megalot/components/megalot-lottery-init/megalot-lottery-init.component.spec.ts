import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MegalotLotteryInitComponent } from './megalot-lottery-init.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { HttpService } from '@app/core/net/http/services/http.service';

import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';
import { BetDataSource } from '@app/core/game/base-game.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { IDialog } from '@app/core/dialog/types';
import { IButtonGroupItem } from '@app/shared/components/buttons-group/buttons-group.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import {DRAWS_FOR_GAME_MEGALOT} from "../../../../features/mocks/megalot-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {ROUTES} from "../../../../../app-routing.module";
import {FormsModule} from "@angular/forms";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient} from "@angular/common/http";

describe('MegalotLotteryInitComponent', () => {
	let component: MegalotLotteryInitComponent | any;
	let appStoreService: AppStoreService;
	let megalotLotteryService: MegalotLotteryService;
	let fixture: ComponentFixture<MegalotLotteryInitComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes(ROUTES),
				SharedModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				}),
				HttpClientTestingModule,
				FormsModule
			],
			declarations: [
				MegalotLotteryInitComponent
			],
			providers: [
				LogService,
				LotteriesService,
				MegalotLotteryService,
				HttpService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useValue: {
						showOneButtonInfo: (): IDialog => {
							return;
						}
					}
				}
			]
		})
		.compileComponents();

		TestBed.inject(LogService);

		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.MegaLot, {...DRAWS_FOR_GAME_MEGALOT.lottery});
		// const operator = new Operator('testMegalotUser', `${Date.now()}`, '123456', 1);
		// appStoreService.operator.next(operator);

		megalotLotteryService = TestBed.inject(MegalotLotteryService);
		megalotLotteryService.initBetData(BetDataSource.Manual);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MegalotLotteryInitComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should create 2', () => {
		megalotLotteryService.activeDraws = [];
		fixture = TestBed.createComponent(MegalotLotteryInitComponent);
		component = fixture.componentInstance;
		component.megalotLotteryService.activeDraws = [];
		fixture.detectChanges();
		expect(component).toBeTruthy();
	});

	it('test onSelectedDrawsHandler function', () => {
		component.ngOnInit();
		component.onSelectedDrawsHandler(undefined);
		expect(megalotLotteryService.betData.drawCount).toEqual(0);

		component.onSelectedDrawsHandler({index: 4, label: '5', selected: true, disabled: false});
		expect(megalotLotteryService.betData.drawCount).toEqual(5);
	});

	it('test onSelectedNumberHandler function', () => {
		spyOn(component, 'onSelectedNumberHandler').and.callThrough();
		component.onSelectedNumberHandler({index: 0, label: '1', selected: true, disabled: false});
		expect(component.onSelectedNumberHandler).toHaveBeenCalled();

		component.onManualClickHandler(undefined, -1);
		let btn: IButtonGroupItem;
		for (let i = 0; i < megalotLotteryService.MAX_BALLS_IN_BET; i++) {
			btn = {index: i, label: (i + 1).toString(), selected: true, disabled: false};
			component.numbersComponent.buttons[i] = {...btn};
			component.onSelectedNumberHandler(btn);
		}
		component.megaballsComponent.buttons = [{index: 0, label: '0', selected: true, disabled: false}];
		expect(component.balls).toEqual(megalotLotteryService.MAX_BALLS_IN_BET);
	});

	it('test onSelectedMegaballHandler function', () => {
		component.onSelectedMegaballHandler({index: 0, label: '0', selected: true, disabled: false});
		expect(component.allMegaBallsSelected).toBeFalsy();

		component.onSelectedMegaballHandler({index: 0, label: '0', selected: false, disabled: false});
		expect(component.allMegaBallsSelected).toBeFalsy();

		component.onManualClickHandler(undefined, -1);
		let btn: IButtonGroupItem;
		for (let i = 0; i < megalotLotteryService.MAX_BALLS_IN_BET; i++) {
			btn = {index: i, label: (i + 1).toString(), selected: true, disabled: false};
			component.numbersComponent.buttons[i] = {...btn};
		}

		for (let i = 0; i < megalotLotteryService.MEGA_BALLS_COUNT; i++) {
			btn = {index: i, label: i.toString(), selected: true, disabled: false};
			component.megaballsComponent.buttons[i] = {...btn};
			component.onSelectedMegaballHandler(btn);
		}

		expect(component.megaballs).toEqual(megalotLotteryService.MEGA_BALLS_COUNT);
	});

	it('test onMaximumNumbersHandler function', () => {
		spyOn(component, 'onMaximumNumbersHandler').and.callThrough();
		component.onMaximumNumbersHandler(undefined);
		expect(component.onMaximumNumbersHandler).toHaveBeenCalled();
	});

	it('test onClearInputHandler function', () => {
		component.onClearInputHandler();
		expect(component.allMegaBallsSelected).toBeFalsy();
		expect(component.prevMBallsSelection).toEqual([]);
	});

	it('test onAutoButtonClickHandler function', () => {
		for (let i = 0; i < megalotLotteryService.MAX_BETS; i++) {
			component.onAutoButtonClickHandler();
			expect(component.megalotLotteryService.betData.bets[i].balls.length).toEqual(megalotLotteryService.MIN_BALLS_IN_BET);
			expect(component.megalotLotteryService.betData.bets[i].megaballs.length).toEqual(1);
		}

		component.onAutoButtonClickHandler();
		expect(component.megalotLotteryService.betData.bets[megalotLotteryService.MAX_BETS]).toBeUndefined();
	});

	it('test onManualClickHandler function', () => {
		component.onManualClickHandler(undefined, -1);
		expect(component.balls).toEqual(0);
		expect(component.megaballs).toEqual(0);

		component.onAutoButtonClickHandler();
		component.onManualClickHandler(undefined, 0);
		expect(component.balls).toEqual(megalotLotteryService.MIN_BALLS_IN_BET);
		expect(component.megaballs).toEqual(1);
	});


	it('test onManualClickHandler function 2', () => {
		component.onAutoButtonClickHandler();
		component.megalotLotteryService.betData.bets[0].balls = [];
		for (let i = 0; i < megalotLotteryService.MAX_BALLS_IN_BET; i++) {
			component.megalotLotteryService.betData.bets[0].balls.push(i + 1);
		}
		component.onManualClickHandler(undefined, 0);
		expect(component.balls).toEqual(megalotLotteryService.MAX_BALLS_IN_BET);
		expect(component.megaballs).toEqual(1);
	});

	it('test onAddEditBetClickHandler function', () => {
		component.onAddEditBetClickHandler();
		expect(component.balls).toEqual(0);
		expect(component.megaballs).toEqual(0);
	});

	it('test onAddEditBetClickHandler function 2', () => {
		component.onAutoButtonClickHandler();
		component.onManualClickHandler(undefined, 0);
		component.currentBetIndex = 0;
		component.onAddEditBetClickHandler();

		expect(component.megalotLotteryService.betData.bets[component.currentBetIndex].balls.length).toEqual(megalotLotteryService.MIN_BALLS_IN_BET);
		expect(component.megalotLotteryService.betData.bets[component.currentBetIndex].megaballs.length).toEqual(1);
	});

	it('test onSwipeBetHandler function', () => {
		component.onAutoButtonClickHandler();
		component.onSwipeBetHandler(component.megalotLotteryService.betData.bets[0]);
		expect(component.megalotLotteryService.betData.bets.length).toEqual(0);
	});

	it('test onClickAllMegaballsHandler function', () => {
		component.onManualClickHandler(undefined, -1);

		component.allMegaBallsSelected = true;
		component.onClickAllMegaballsHandler();
		expect(component.megaballs).toEqual(megalotLotteryService.MEGA_BALLS_COUNT);

		component.allMegaBallsSelected = false;
		component.onClickAllMegaballsHandler();
		expect(component.megaballs).toEqual(0);

		let btn: IButtonGroupItem;
		for (let i = 0; i < megalotLotteryService.MEGA_BALLS_COUNT; i++) {
			btn = {index: i, label: i.toString(), selected: true, disabled: false};
			component.megaballsComponent.buttons[i] = {...btn};
		}
		component.allMegaBallsSelected = true;
		component.onClickAllMegaballsHandler();
		expect(component.megaballs).toEqual(megalotLotteryService.MEGA_BALLS_COUNT);

		for (let i = 0; i < megalotLotteryService.MEGA_BALLS_COUNT; i++) {
			btn = {index: i, label: i.toString(), selected: false, disabled: false};
			component.megaballsComponent.buttons[i] = {...btn};
		}
		component.allMegaBallsSelected = false;
		component.onClickAllMegaballsHandler();
		expect(component.megaballs).toEqual(0);

		component.prevMBallsSelection = [0];
		component.allMegaBallsSelected = false;
		component.onClickAllMegaballsHandler();
		expect(component.megaballs).toEqual(1);

		component.prevMBallsSelection = [0];
		for (let i = 0; i < megalotLotteryService.MEGA_BALLS_COUNT; i++) {
			btn = {index: i, label: i.toString(), selected: true, disabled: false};
			component.megaballsComponent.buttons[i] = {...btn};
		}
		component.allMegaBallsSelected = false;
		component.onClickAllMegaballsHandler();
		expect(component.megaballs).toEqual(1);
	});

	it('test trackByBets function', () => {
		expect(component.trackByBets(1)).toEqual(1);
	});

	it('test trackByBalls function', () => {
		expect(component.trackByBalls(1)).toEqual(1);
	});

	it('test trackByMegaballs function', () => {
		expect(component.trackByMegaballs(1)).toEqual(1);
	});

	it('test validateInput function', () => {
		spyOn(component, 'validateInput').and.callThrough();
		component.numbersComponent.buttons = undefined;
		component.validateInput();
		expect(component.validateInput).toHaveBeenCalled();
	});

	it('test updateSumsVariants function', () => {
		spyOn(component, 'updateSumsVariants').and.callThrough();
		component.megalotLotteryService.activeDraws = undefined;
		component.updateSumsVariants();
		expect(component.updateSumsVariants).toHaveBeenCalled();
	});

	it('test autoUpdateDraws function', () => {
		component.onSelectedDrawsHandler({index: 0, label: '1', selected: true, disabled: false});
		component.onAutoButtonClickHandler();
		component.autoUpdateDraws();
		expect(component.megalotLotteryService.amount).toEqual(5);

		component.countEnabledDraws++;
		component.megalotLotteryService.betData.drawCount = component.megalotLotteryService.activeDraws.length + 1;
		component.autoUpdateDraws();
		expect(component.megalotLotteryService.amount).toEqual(20);

		component.countEnabledDraws = 1;
		component.megalotLotteryService.betData.drawCount = component.megalotLotteryService.activeDraws.length + 1;
		component.autoUpdateDraws();
		expect(component.megalotLotteryService.amount).toEqual(0);
	});

	it('test restoreDraw function', () => {
		spyOn(component, 'restoreDraw').and.callThrough();
		component.megalotLotteryService.activeDraws[0].draw.jackpot = 'Джекпот: 1234567890 грн';
		component.megalotLotteryService.activeDraws[0].draw.data.mega_prize = 'Мегаприз: 9876543210 грн';
		component.restoreDraw();
		expect(component.restoreDraw).toHaveBeenCalled();

		component.megalotLotteryService.activeDraws[0].draw.jackpot = undefined;
		component.megalotLotteryService.activeDraws[0].draw.data.mega_prize = undefined;
		component.restoreDraw();
		expect(component.restoreDraw).toHaveBeenCalled();


		component.megalotLotteryService.betData.drawCount = component.countEnabledDraws + 1;
		component.restoreDraw();
		expect(component.restoreDraw).toHaveBeenCalled();

		component.countEnabledDraws = 0;
		component.restoreDraw();
		expect(component.restoreDraw).toHaveBeenCalled();
	});

});
