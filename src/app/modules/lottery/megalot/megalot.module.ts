import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';
import { MegalotRoutingModule } from '@app/megalot/megalot-routing.module';

import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';

import { MegalotLotteryContentComponent } from '@app/megalot/components/megalot-lottery-content/megalot-lottery-content.component';
import { MegalotLotteryInitComponent } from '@app/megalot/components/megalot-lottery-init/megalot-lottery-init.component';
import { MegalotResultsComponent } from '@app/megalot/components/megalot-results/megalot-results.component';
import { FormsModule } from '@angular/forms';

/**
 * Модуль лотереи "Мегалот".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		MegalotRoutingModule,
		FormsModule
	],
	declarations: [
		MegalotLotteryContentComponent,
		MegalotLotteryInitComponent,
		MegalotResultsComponent
	],
	providers: [
		MegalotLotteryService
	]
})
export class MegalotModule {}
