import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { TranslateService } from '@ngx-translate/core';

import { Profile } from '@app/util/profile';
import { URL_MEGALOT } from '@app/util/route-utils';
import { numberFromStringCurrencyFormat } from '@app/util/utils';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { IError } from '@app/core/error/types';
import { IMegalotBetsBalls, MegalotRegBetReq, MegalotRegBetResp } from '@app/core/net/http/api/models/megalot-reg-bet';
import { HttpService } from '@app/core/net/http/services/http.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { IMegalotBetDataItem } from '@app/megalot/interfaces/imegalot-bet-data-item';
import { BaseGameService } from '@app/core/game/base-game.service';
import { LogOutService } from '@app/logout/services/log-out.service';

/**
 * Таблица множителей для вычисления стоимости ставки.
 */
const MULTIPLIER_TABLE = [0, 0, 0, 0, 0, 1, 7, 28, 84, 210];

/**
 * Возвращает модель с начальными данными для игры "Мегалот".
 * Вызывается в момент перехода на форму ручного ввода ставки по игре.
 */
const initBetDataFn = (): IMegalotBetDataItem => {
	return {
		drawCount: 0,
		bets: []
	};
};

/**
 * Сервис лотереи "Мегалот".
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей, и взаимодейсвия с ЦС.
 */
@Injectable()
export class MegalotLotteryService extends BaseGameService<IMegalotBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * URL игры.
	 */
	readonly gameUrl = URL_MEGALOT;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.megalot.megalot_name';

	/**
	 * Общее количество номеров шариков, доступное для выбора.
	 */
	readonly BALLS_COUNT = 42;

	/**
	 * Максимальное количество мега-шариков в ставке.
	 */
	readonly MEGA_BALLS_COUNT = 10;

	/**
	 * Общее количество тиражей.
	 */
	readonly DRAWS_COUNT = 10;

	/**
	 * Минимальное количество шариков в ставке.
	 */
	readonly MIN_BALLS_IN_BET = 6;

	/**
	 * Максимальное количество шариков в ставке.
	 */
	readonly MAX_BALLS_IN_BET = 10;

	/**
	 * Максимальное количество ставок в билете.
	 */
	readonly MAX_BETS = 10;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения.
	 * @param {HttpService} httpService Сервис HTTP-запросов.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {TranslateService} translateService Сервис локализации.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект местоположения в браузере.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly httpService: HttpService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();
		this.currentGameCode = LotteryGameCode.MegaLot;
		this.currentGroupCode = LotteriesGroupCode.Regular;
		this.initialBetDataFactory = initBetDataFn;

		// TODO: just for test
		// this.appStoreService.Draws.getDraws(LotteryGameCode.MegaLot)
		// .forEach((v, i) => v.draw.sale_edate = new Date(new Date().getTime() + (i + 1) * 10000).toISOString());
	}

	/**
	 * Генерация случайной ставки.
	 *
	 * @param {boolean} addToBetsList Признак, который указывает на необходимость
	 * сразу добавить сгенерированную ставку в массив ставок {@link currentBets}.
	 * @param {number} countBalls Кол-во шариков, которые необходимо сгенерировать.
	 * @param {boolean} fromCoupon Признак, указывающий, что генерация будет для метки купона "АВТО".
	 * @returns {IMegalotBetsBalls} Сгенерированная случайная ставка.
	 */
	generateRandomBet(addToBetsList: boolean = true, countBalls = this.MIN_BALLS_IN_BET, fromCoupon = false): IMegalotBetsBalls {
		// сформировать случайные шарики
		const balls = [];
		const rnd = () => Math.floor(Math.random() * (this.BALLS_COUNT + 1));
		let bc = countBalls;
		while (bc > 0) {
			let num;
			do {
				num = rnd();
			} while (num <= 0 || num > this.BALLS_COUNT || balls.indexOf(num) >= 0);
			balls.push(num);
			bc--;
		}

		// сформировать мегакульку
		const megaballs = [Math.floor(Math.random() * this.MEGA_BALLS_COUNT)];

		const input_mode = fromCoupon ? 1 : 3;
		const result: IMegalotBetsBalls = {balls, megaballs, input_mode};

		// если необходимо, добавляем ставку в массив
		if (addToBetsList) {
			this.betData.bets.push(result);
		}

		return result;
	}

	/**
	 * Удалить ставку из массива по указанному индексу.
	 *
	 * @param {number} index Индекс удаляемой ставки.
	 */
	removeBet(index: number): void {
		this.betData.bets.splice(index, 1);
	}

	/**
	 * Получить сумму ставки.
	 *
	 * @param {number} betBallsLen Кол-во обычных шариков в ставке.
	 * @param {number} betMegaballsLen Кол-во мега-шариков в ставке.
	 * @param {string} drawBetSum Сумма ставки на обычные шарики.
	 * @param {string} drawBetExtraSum Сумма ставки на мега-шарики.
	 * @param {boolean} divBy100 Признак, указывающий, что необходимо разделить сумму на 100.
	 * @returns {number}
	 */
	getBetSum(betBallsLen: number, betMegaballsLen: number, drawBetSum: string, drawBetExtraSum: string, divBy100 = false): number {
		const tmpSum = (MULTIPLIER_TABLE[betBallsLen - 1] * numberFromStringCurrencyFormat(drawBetSum)
			+ MULTIPLIER_TABLE[betBallsLen - 1] * numberFromStringCurrencyFormat(drawBetExtraSum) * (betMegaballsLen - 1));

		return divBy100 ? Math.round(tmpSum / 100) : tmpSum;
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновить ставки.
	 */
	refreshBets(): void {
		let amount = 0;

		// если тиражи по игре существуют - выполнить обсчет ставки
		if (Array.isArray(this.activeDraws) && this.betData.drawCount <= this.activeDraws.length) {
			for (let draw_index = 0; draw_index < this.betData.drawCount; draw_index++) {
				this.betData.bets.forEach(bet => {
					if (bet.balls.length !== 0 && bet.megaballs.length !== 0) {
						amount += this.getBetSum(bet.balls.length, bet.megaballs.length,
							this.activeDraws[draw_index].draw.bet_sum,
							this.activeDraws[draw_index].draw.data.mega.extra_sum);
					}
				});
			}
		}

		this.amount = amount / 100;
	}

	/**
	 * Купить лотерею.
	 */
	buyLottery(): void {
		Logger.Log.i('MegalotLotteryService', 'buyLottery -> start buying Megalot')
			.console();
		const buyMegalot = new MegalotRegBetReq(this.appStoreService, `${this.amount * 100}`, this.betData);
		this.transactionService.buyLottery(MegalotRegBetResp, LotteryGameCode.MegaLot, buyMegalot);
	}

}
