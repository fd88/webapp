import { TestBed } from '@angular/core/testing';
import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { HttpServiceStub } from '@app/core/net/http/services/http.service.spec';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { TransactionServiceStub } from '@app/core/services/transaction/transaction.service.spec';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { Injector } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource } from '@app/core/game/base-game.service';
import { EsapActions } from '@app/core/configuration/esap';
import { Operator } from '@app/core/services/store/operator';
import {LogOutService} from "@app/logout/services/log-out.service";
import {LogOutServiceStub} from "@app/logout/services/log-out.service.spec";
import {DRAWS_FOR_GAME_MEGALOT} from "../../../features/mocks/megalot-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";

xdescribe('MegalotLotteryService', () => {
	let service: MegalotLotteryService | any;
	let appStoreService: AppStoreService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,

				PrintService,
				Injector,
				MegalotLotteryService,
				{
					provide: LogOutService,
					useValue: LogOutServiceStub,
				},
				{
					provide: HttpService,
					useValue: new HttpServiceStub()
				},
AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				{
					provide: TransactionService,
					useValue: new TransactionServiceStub()
				}
			]
		});

		TestBed.inject(LogService);

		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.MegaLot, DRAWS_FOR_GAME_MEGALOT);
		const operator = new Operator('testMegalotUser', `${Date.now()}`, '123456', 1);
		appStoreService.operator.next(operator);

		service = TestBed.inject(MegalotLotteryService);
		service.initBetData(BetDataSource.Manual);
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(
			LotteryGameCode.MegaLot,
			new Map<string, string>([[EsapActions.MegalotRegBet, 'url']]
		));
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test #1 generateRandomBet method', () => {
		const megalotBetBalls = service.generateRandomBet(false, 7, true);
		expect(megalotBetBalls.balls.length).toEqual(7);
		expect(megalotBetBalls.input_mode).toEqual(1);
		expect(megalotBetBalls.megaballs.length).toEqual(1);
	});

	it('test #2 generateRandomBet method', () => {
		const megalotBetBalls = service.generateRandomBet();
		expect(megalotBetBalls.balls.length).toEqual(service.MIN_BALLS_IN_BET);
		expect(megalotBetBalls.input_mode).toEqual(3);
		expect(megalotBetBalls.megaballs.length).toEqual(1);
	});

	it('test removeBet method', () => {
		service.generateRandomBet(true);
		service.removeBet(0);
		expect(service.betData.bets.length).toEqual(0);
	});

	it('test getBetSum method', () => {
		expect(service.getBetSum(6, 1, '10', '15', true)).toEqual(10);
	});

	it('test refreshBets method', () => {
		service.betData.drawCount = 1;
		service.betData.bets = [{balls: [2,16,18,38,39,42], megaballs: [0], input_mode: 2}];
		service.refreshBets();
		expect(service.amount).toEqual(5);

		service.betData.drawCount = 1;
		service.betData.bets = [{balls: [], megaballs: [], input_mode: 2}];
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.activeDraws = undefined;
		service.betData.drawCount = 1;
		service.betData.bets = [{balls: [2,16,18,38,39,42], megaballs: [0], input_mode: 2}];
		service.refreshBets();
		expect(service.amount).toEqual(0);
	});

	xit('test #1 buyLottery method', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();
		service.betData.drawCount = 1;
		service.betData.bets.push({balls: [2,16,18,38,39,42], megaballs: [0], input_mode: 2});
		service.refreshBets();
		service.printService.isReady = () => true;
		service.transactionService.executeRequest = (): Promise<void> => new Promise((resolve, reject) => reject({code: 1000}));
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});

	xit('test #2 buyLottery method', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();
		service.betData.drawCount = 1;
		service.betData.bets.push({balls: [4,16,18,38,39,42], megaballs: [0], input_mode: 2});
		service.refreshBets();
		service.printService.isReady = () => true;
		service.transactionService.executeRequest = (): Promise<void> => new Promise(resolve => resolve());
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});

	xit('test #3 buyLottery method', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();
		service.betData.drawCount = 1;
		service.betData.bets.push({balls: [3,16,18,38,39,42], megaballs: [0], input_mode: 2});
		service.refreshBets();
		service.printService.isReady = () => false;
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});

});
