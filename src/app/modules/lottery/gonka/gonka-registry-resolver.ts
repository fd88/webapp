import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { formattedAmount } from '@app/util/utils';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { GAME_TYPES, GonkaLotteryService } from '@app/gonka/services/gonka-lottery.service';
import { BetDataSource, GameSaleMode } from '@app/core/game/base-game.service';
import { IActionParameters } from '@app/actions/interfaces/iactions-settings';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';

/**
 * Резолвер данных для отображения информации о ручной ставке в "Гонка на гроши".
 */
@Injectable()
export class GonkaRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {GonkaLotteryService} gonkaLotteryService Сервис игры "Гонка на гроши".
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 */
	constructor(
		private readonly gonkaLotteryService: GonkaLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		// проверить наличие акционных параметров
		const parsedUrl = this.router.parseUrl(state.url);
		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
			const actionBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionParameters;
			this.gonkaLotteryService.initBetData(BetDataSource.Manual);
			this.gonkaLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			// ... TODO доделать акционные ставки
			this.gonkaLotteryService.refreshBets();
		}
		// проверить наличие демо-параметров
		if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			const demoBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET));
			this.gonkaLotteryService.initBetData(BetDataSource.Manual);
			this.gonkaLotteryService.betData.saleMode = GameSaleMode.QuickSale;

			this.gonkaLotteryService.drawsCount = 1;
			this.gonkaLotteryService.betCount = demoBet.data.betCount;
			this.gonkaLotteryService.gameType = demoBet.data.gameType;
			this.gonkaLotteryService.betSumma = demoBet.data.betSumma;
			this.gonkaLotteryService.refreshBets();
		}

		if (!this.gonkaLotteryService.activeDraws || !this.gonkaLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.gonkaLotteryService
			};
		}

		const checkInfoRowList = [
			{
				langKeyName: 'lottery.draws_count',
				value: `${this.gonkaLotteryService.drawsCount}`
			},
			{
				langKeyName: 'lottery.bets_count',
				value: `${this.gonkaLotteryService.commonBetCount}`
			},
			{
				langKeyName: 'lottery.race_for_money.game_type',
				value: `${GAME_TYPES[this.gonkaLotteryService.gameType - 1]}`
			},
			{
				langKeyName: 'lottery.bet_sum',
				value: `${formattedAmount(this.gonkaLotteryService.betSumma)}`,
				valueIsCurrency: true
			},
			{
				langKeyName: 'lottery.sum',
				value: `${this.gonkaLotteryService.formattedAmount}`,
				redColor: true,
				valueIsCurrency: true
			}
		];

		return {
			checkInfoRowList,
			baseGameService: this.gonkaLotteryService
		};
	}

}
