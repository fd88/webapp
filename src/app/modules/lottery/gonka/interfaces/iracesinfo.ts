/**
 * Интерфейс для описания заезда
 */
interface IRace {
	/**
	 * Максимально допустимое количество победителей в заезде
	 */
	winnersToSelect: number;
	/**
	 * Массив с номерами победителей в заездах
	 */
	selection: Array<number>;
}

/**
 * Интерфейс для описания информации о заездах
 */
export interface IRacesInfo {
	/**
	 * Количество участников в заезде
	 */
	participCount: number;
	/**
	 * Доступные для выбора заезды
	 */
	enabledRaces: Array<boolean>;
	/**
	 * Массив с заездами
	 */
	races: Array<IRace>;
	/**
	 * Признак валидности информации о заездах
	 */
	isValid: boolean;
}
