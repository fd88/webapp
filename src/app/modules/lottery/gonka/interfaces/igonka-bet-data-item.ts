import { IBetDataItem } from '@app/core/game/base-game.service';

/**
 * Интерфейс модели данных для игры "Гонка".
 */
export interface IGonkaBetDataItem extends IBetDataItem {

	/**
	 * Выбранное количество ставок.
	 */
	betsCount: number;

	/**
	 * Тип игры.
	 */
	gameType: number;

	/**
	 * Сумма ставки.
	 */
	betSumma: number;

}
