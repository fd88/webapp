import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { URL_INIT, URL_REGISTRY, URL_RESULTS, URL_SCANNER } from '@app/util/route-utils';
import { AuthGuard } from '@app/core/guards/auth.guard';

import { GonkaLotteryContentComponent } from '@app/gonka/components/gonka-lottery-content/gonka-lottery-content.component';
import { GonkaLotteryInitComponent } from '@app/gonka/components/gonka-lottery-init/gonka-lottery-init.component';
import { GonkaResultsComponent } from '@app/gonka/components/gonka-results/gonka-results.component';
import { GonkaRegistryResolver } from '@app/gonka/gonka-registry-resolver';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Гонка на гроши".
 */
const routes: Routes = [
	{
		path: '',
		component: GonkaLotteryContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: URL_INIT,
				component: GonkaLotteryInitComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: GonkaRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_RESULTS,
				component: GonkaResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}
];

/**
 * Модуль маршрутизации лотереи "Гонка на гроши".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		GonkaRegistryResolver
	]
})
export class GonkaRoutingModule {}
