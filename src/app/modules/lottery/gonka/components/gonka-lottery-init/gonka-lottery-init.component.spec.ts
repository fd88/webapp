import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {GonkaLotteryInitComponent} from '@app/gonka/components/gonka-lottery-init/gonka-lottery-init.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '@app/shared/shared.module';
import {RouterTestingModule} from '@angular/router/testing';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {GonkaGameType, GonkaLotteryService} from '@app/gonka/services/gonka-lottery.service';

import {HttpService} from '@app/core/net/http/services/http.service';

import {RacesButtonsComponent} from '@app/gonka/components/races-buttons/races-buttons.component';
import {IDialog} from '@app/core/dialog/types';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LotteriesService} from '@app/core/services/lotteries.service';
import {LotteriesDraws} from '@app/core/services/store/draws';
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_GONKA} from "../../../../features/mocks/gonka-draws";
import {timer} from "rxjs";
import {ROUTES} from "../../../../../app-routing.module";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient} from "@angular/common/http";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

describe('GonkaLotteryInitComponent', () => {
	let component: GonkaLotteryInitComponent;
	let fixture: ComponentFixture<GonkaLotteryInitComponent>;
	let gonkaLotteryService: GonkaLotteryService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes(ROUTES),
				SharedModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				}),
				HttpClientTestingModule
			],
			declarations: [
				GonkaLotteryInitComponent,
				RacesButtonsComponent
			],
			providers: [
				LogService,
				GonkaLotteryService,
				HttpService,
				LotteriesService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				}
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		gonkaLotteryService = TestBed.inject(GonkaLotteryService);
		gonkaLotteryService.appStoreService.Draws = new LotteriesDraws();
		gonkaLotteryService.appStoreService.Draws.setLottery(LotteryGameCode.Gonka, DRAWS_FOR_GAME_GONKA.lottery);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GonkaLotteryInitComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('test updateState method', () => {
		component.gonkaLotteryService.drawsCount = 0;
		component.updateState();
		expect(component.gonkaLotteryService.buyEnabled).toBeFalsy();

		component.gonkaLotteryService.drawsCount = 1;
		component.updateState();
		expect(component.gonkaLotteryService.buyEnabled).toBeFalsy();

		component.gonkaLotteryService.gameType = 0;
		component.updateState();
		expect(component.gonkaLotteryService.buyEnabled).toBeFalsy();

		component.gonkaLotteryService.gameType = 1;
		component.updateState();
		expect(component.gonkaLotteryService.buyEnabled).toBeFalsy();

		component.gonkaLotteryService.betSumma = 2;
		component.updateState();
		expect(component.gonkaLotteryService.buyEnabled).toBeFalsy();

		component.gonkaLotteryService.betCount = 1;
		component.updateState();
		expect(component.gonkaLotteryService.buyEnabled).toBeTruthy();
	});

	it('test onSelectedBetCountHandler method', () => {
		component.ngOnInit();
		component.gonkaLotteryService.drawsCount = 1;
		component.gonkaLotteryService.gameType = 1;
		component.gonkaLotteryService.betSumma = 2;

		component.onSelectedBetCountHandler({index: 0, label: '1', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);

		component.gonkaLotteryService.isManualInput = true;
		component.showWarning = () => {};
		component.onSelectedBetCountHandler({index: 0, label: '1', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);

		component.gonkaLotteryService.betCountPrev = 0;
		component.onSelectedBetCountHandler({index: 0, label: '2', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);


		component.onSelectedBetCountHandler({index: 0, label: '1', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);
	});

	it('test onSelectedBetSummaHandler method', () => {
		component.ngOnInit();
		component.gonkaLotteryService.drawsCount = 1;
		component.gonkaLotteryService.gameType = 1;
		component.gonkaLotteryService.betCount = 1;
		component.onSelectedBetSummaHandler({index: 0, label: '2', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);
	});

	it('test onSelectedDrawsHandler method', () => {
		component.ngOnInit();
		component.gonkaLotteryService.gameType = 1;
		component.gonkaLotteryService.betCount = 1;
		component.gonkaLotteryService.betSumma = 2;
		component.onSelectedDrawsHandler({index: 0, label: '1', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);
	});

	it('test onSelectedGameTypeHandler method', () => {
		component.ngOnInit();
		component.gonkaLotteryService.betCount = 1;
		component.gonkaLotteryService.betSumma = 2;
		component.gonkaLotteryService.drawsCount = 1;

		component.onSelectedGameTypeHandler({index: 0, label: GonkaGameType.A, selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);

		component.gonkaLotteryService.isManualInput = true;
		component.onSelectedGameTypeHandler({index: 0, label: GonkaGameType.A, selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);

		component.gonkaLotteryService.isManualInput = true;
		component.gameTypeComponent.buttons[1].selected = true;
		component.onSelectedGameTypeHandler(component.gameTypeComponent.buttons[1]);
		expect(component.gonkaLotteryService.amount).toEqual(2);

		component.onSelectedGameTypeHandler({index: 2, label: '3', selected: true, disabled: false});
		expect(component.gonkaLotteryService.amount).toEqual(2);
	});

	it('test onWarningBackHandler method', () => {

		component.toReturnWarning = 1;
		component.onWarningBackHandler();
		expect(component.gonkaLotteryService.betCount).toEqual(component.gonkaLotteryService.betCountPrev);

		component.toReturnWarning = 2;
		component.onWarningBackHandler();
		expect(component.gonkaLotteryService.isManualInput).toEqual(component.gonkaLotteryService.isManualInput);
	});


	it('test onWarningConfirmHandler method #1', () => {
		spyOn(component, 'onWarningConfirmHandler').and.callThrough();
		component.toReturnWarning = 1;
		component.onWarningConfirmHandler();
		expect(component.onWarningConfirmHandler).toHaveBeenCalled();
	});

	it('test onWarningConfirmHandler method #2', () => {
		component.toReturnWarning = 2;
		component.showDialog = () => {};
		component.onWarningConfirmHandler();
		expect(component.toReturnDialog).toEqual(2);
	});

	it('test onDialogCancelHandler method', () => {
		component.ngOnInit();
		spyOn(component, 'onDialogCancelHandler').and.callThrough();
		component.onDialogCancelHandler();
		expect(component.onDialogCancelHandler).toHaveBeenCalled();

		component.gonkaLotteryService.isManualInput = true;
		component.gonkaLotteryService.isManualInputPrev = false;
		component.onDialogCancelHandler();
		expect(component.onDialogCancelHandler).toHaveBeenCalled();

		component.toReturnDialog = 1;
		component.onDialogCancelHandler();
		expect(component.onDialogCancelHandler).toHaveBeenCalled();

		component.toReturnDialog = 2;
		component.onDialogCancelHandler();
		expect(component.onDialogCancelHandler).toHaveBeenCalled();
	});

	it('test onDialogSelectHandler method', () => {
		spyOn(component, 'onDialogSelectHandler').and.callThrough();
		component.onDialogSelectHandler();
		expect(component.onDialogSelectHandler).toHaveBeenCalled();
	});

	it('test onSelectedGameSubTypeHandler method', () => {
		component.ngOnInit();
		component.onSelectedGameSubTypeHandler({index: 0, label: 'auto', selected: true, disabled: false});
		expect(component.gonkaLotteryService.isManualInput).toBeFalsy();

		component.gonkaLotteryService.betCount = 2;
		component.onSelectedGameSubTypeHandler({index: 0, label: 'auto', selected: true, disabled: false});
		expect(component.gonkaLotteryService.isManualInput).toBeFalsy();

		component.gonkaLotteryService.isManualInput = true;
		component.onSelectedGameSubTypeHandler({index: 0, label: 'auto', selected: true, disabled: false});
		expect(component.gonkaLotteryService.isManualInput).toBeFalsy();

		component.gonkaLotteryService.isManualInput = false;
		component.onSelectedGameSubTypeHandler({index: 1, label: 'manual', selected: true, disabled: false});
		expect(component.gonkaLotteryService.isManualInput).toBeFalsy();

		component.gonkaLotteryService.betCount = 1;

		component.gonkaLotteryService.isManualInput = true;
		component.onSelectedGameSubTypeHandler({index: 0, label: 'auto', selected: true, disabled: false});
		expect(component.gonkaLotteryService.isManualInput).toBeFalsy();

		component.gonkaLotteryService.isManualInput = false;
		component.onSelectedGameSubTypeHandler({index: 1, label: 'manual', selected: true, disabled: false});
		expect(component.gonkaLotteryService.isManualInput).toBeFalsy();

	});

	it('test showWarning method', () => {
		spyOn(component, 'showWarning').and.callThrough();
		component.dialogContainerService.showConfirmTwoButtons = (): IDialog => undefined;
		component.showWarning();
		expect(component.showWarning).toHaveBeenCalled();

	});

	it('test showDialog method', () => {
		component.ngOnInit();
		spyOn(component, 'showDialog').and.callThrough();
		component.gonkaLotteryService.gameType = 1;

		component.gonkaLotteryService.racesA = {
			"participCount": 10,
			"enabledRaces": [
				true,
				true,
				true,
				true
			],
			"isValid": true,
			"races": [
				{
					"winnersToSelect": 1,
					"selection": [
						0
					]
				},
				{
					"winnersToSelect": 1,
					"selection": [
						1
					]
				},
				{
					"winnersToSelect": 1,
					"selection": [
						2
					]
				},
				{
					"winnersToSelect": 1,
					"selection": [
						3
					]
				}
			]
		};

		component.showDialog();
		expect(component.showDialog).toHaveBeenCalled();


	});

	it('test trackByFn function', () => {
		expect(component.trackByFn(1, 5)).toEqual(1);
	});

	it('test buyLottery method in gonkaLotteryService', async () => {
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		gonkaLotteryService.appStoreService.Settings.populateEsapActionsMapping(csConfig);
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(gonkaLotteryService, 'buyLottery').and.callThrough();
		gonkaLotteryService.buyLottery();
		expect(gonkaLotteryService.buyLottery).toHaveBeenCalled();
	});

});
