import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { OneButtonCustomComponent } from '@app/shared/components/one-button-custom/one-button-custom.component';
import { RacesButtonsComponent } from '../races-buttons/races-buttons.component';
import { IGonkaBet } from '@app/core/net/http/api/models/gonka-reg-bet';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import {
	BET_SUMMA_LIST,
	DRAWS_LABELS,
	GAME_TYPE_VALUES,
	GAME_TYPES,
	GAME_TYPES_EN,
	GonkaLotteryService
} from '../../services/gonka-lottery.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource } from '@app/core/game/base-game.service';

/**
 * Компонент ввода лотереи "ГОНКА НА ДЕНЬГИ".
 */
@Component({
	selector: 'app-gonka-lottery-init',
	templateUrl: './gonka-lottery-init.component.html',
	styleUrls: ['./gonka-lottery-init.component.scss']
})
export class GonkaLotteryInitComponent implements OnInit {

	// -----------------------------
	//  Constants
	// -----------------------------
	/**
	 * Перечисление стилей кнопок, доступных к использованию.
	 */
	readonly buttonGroupStyle = ButtonGroupStyle;

	/**
	 * Перечисление режимов работы группы кнопок
	 */
	readonly buttonGroupMode = ButtonGroupMode;

	/**
	 * Список количества тиражей.
	 */
	readonly DRAWS_LABELS = DRAWS_LABELS;

	/**
	 * Список возможных сумм ставок.
	 */
	readonly BET_SUMMA_LIST = BET_SUMMA_LIST;

	/**
	 * Типы заездов на русском языке.
	 */
	readonly GAME_TYPES = GAME_TYPES;

	/**
	 * Список значений типов игры "Гонка на деньги".
	 */
	readonly GAME_TYPE_VALUES = GAME_TYPE_VALUES;

	/**
	 * Типы заездов на английском языке.
	 */
	readonly GAME_TYPES_EN = GAME_TYPES_EN;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на группу кнопок с заездами.
	 */
	@ViewChildren(ButtonsGroupComponent)
	races: QueryList<ButtonsGroupComponent>;

	/**
	 * Ссылка на группу кнопок с тиражами.
	 */
	@ViewChild('drawsComponent', { static: true })
	drawsComponent: ButtonsGroupComponent;

	/**
	 * Ссылка на группу кнопок с суммами ставок.
	 */
	@ViewChild('betSumComponent', { static: true })
	betSumComponent: ButtonsGroupComponent;

	/**
	 * Ссылка на группу кнопок с типами игры.
	 */
	@ViewChild('gameTypeComponent', { static: true })
	gameTypeComponent: ButtonsGroupComponent;

	/**
	 * Ссылка на группу кнопок с подтипами игры.
	 */
	@ViewChild('gameSubTypeComponent', { static: true })
	gameSubTypeComponent: ButtonsGroupComponent;

	/**
	 * Ссылка на группу кнопок с количеством ставок.
	 */
	@ViewChild('betsCountButtons', { static: true })
	betsCountButtons: ButtonsGroupComponent;

	/**
	 * Ссылка на диалоговое окно с типом игры "А".
	 */
	@ViewChild('gameTypeA', { static: true })
	gameTypeA: OneButtonCustomComponent;

	/**
	 * Ссылка на диалоговое окно с типом игры "Б".
	 */
	@ViewChild('gameTypeB', { static: true })
	gameTypeB: OneButtonCustomComponent;

	/**
	 * Ссылка на диалоговое окно с типом игры "В".
	 */
	@ViewChild('gameTypeV', { static: true })
	gameTypeV: OneButtonCustomComponent;

	/**
	 * Ссылка на диалоговое окно с типом игры "Г".
	 */
	@ViewChild('gameTypeG', { static: true })
	gameTypeG: OneButtonCustomComponent;

	/**
	 * Ссылка на диалоговое окно с типом игры "Д".
	 */
	@ViewChild('gameTypeD', { static: true })
	gameTypeD: OneButtonCustomComponent;

	/**
	 * Ссылка на группу кнопок с заездами типа игры "А".
	 */
	@ViewChild('racesButtonsA', { static: true })
	racesButtonsA: RacesButtonsComponent;

	/**
	 * Ссылка на группу кнопок с заездами типа игры "Б".
	 */
	@ViewChild('racesButtonsB', { static: true })
	racesButtonsB: RacesButtonsComponent;

	/**
	 * Ссылка на группу кнопок с заездами типа игры "В".
	 */
	@ViewChild('racesButtonsV', { static: true })
	racesButtonsV: RacesButtonsComponent;

	/**
	 * Ссылка на группу кнопок с заездами типа игры "Г".
	 */
	@ViewChild('racesButtonsG', { static: true })
	racesButtonsG: RacesButtonsComponent;

	/**
	 * Ссылка на группу кнопок с заездами типа игры "Д".
	 */
	@ViewChild('racesButtonsD', { static: true })
	racesButtonsD: RacesButtonsComponent;

	/**
	 * Видимый ли блок с подтипами игры.
	 */
	isVisibleSubtype: boolean;

	/**
	 * Индекс, чтоб выставить тираж(и)
	 */
	drawsIndex: number;

	/**
	 * Индекс, чтоб выставить тип игры
	 */
	gtIndex: number;

	/**
	 * Выставить сумму по индексу кнопки
	 */
	summIndex: number;

	/**
	 * Куда вернуться при закрытии предупреждения?
	 * 0 - никуда
	 * 1 - на предыдущее кол. ставок (то есть 1)
	 * 2 - на предыдущий режим
	 */
	toReturnWarning = 0;

	/**
	 * Куда вернуться при закрытии диалога?
	 * 	0 - никуда
	 * 	1 - на предыдущий тип игры
	 * 	2 - на предыдущий режим
	 */
	toReturnDialog = 0;

	/**
	 * Предыдущий выбор в диалоге
	 */
	previousDialogSelection: IGonkaBet;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента регистрации для лотереи "Гонка на гроши".
	 *
	 * @param {GonkaLotteryService} gonkaLotteryService Сервис лотереи "Гонка на деньги".
	 * @param {DialogContainerService} dialogContainerService Сервис контейнера диалоговых окон.
	 */
	constructor(
		readonly gonkaLotteryService: GonkaLotteryService,
		readonly dialogContainerService: DialogContainerService
	) {}

	/**
	 * Обновить состояние кнопок
	 */
	updateState(): void {
		// Выставить тираж(и)
		this.drawsIndex = DRAWS_LABELS.indexOf(this.gonkaLotteryService.drawsCount);
		if (this.drawsIndex > -1) {
			this.drawsComponent.selectedButtons = Array.from({length: this.drawsIndex + 1}, (v, k) => k);
		}

		// Выставить тип игры
		this.gtIndex = GAME_TYPE_VALUES.indexOf(this.gonkaLotteryService.gameType);
		if (this.gtIndex === -1) {
			if (this.gameTypeComponent.buttons) {
				this.gameTypeComponent.buttons.forEach(elem => elem.selected = false);
			}
		} else {
			if (this.gameSubTypeComponent.buttons) {
				if (this.gameTypeComponent.buttons.length && !this.gameTypeComponent.buttons[this.gtIndex].selected) {
					this.gameTypeComponent.selectButtonByIndex(this.gtIndex);
				}
			}

			if (this.gameSubTypeComponent.buttons) {
				// Выставить подтип игры
				this.gameSubTypeComponent.selectButtonByIndex(this.gonkaLotteryService.isManualInput ? 1 : 0);
			}
		}

		// выставить сумму
		this.summIndex = BET_SUMMA_LIST.indexOf(this.gonkaLotteryService.betSumma);
		if (this.betSumComponent.buttons) {
			if (this.summIndex === -1) {
				this.betSumComponent.buttons.forEach(elem => elem.selected = false);
			} else {
				if (this.betSumComponent.buttons.length && !this.betSumComponent.buttons[this.summIndex].selected) {
					this.betSumComponent.selectButtonByIndex(this.summIndex);
				}
			}
		}

		// выставить количество ставок
		if (this.betsCountButtons && this.betsCountButtons.buttons) {
			this.betsCountButtons.buttons.forEach((elem, i) => elem.selected = i < this.gonkaLotteryService.betCount);
		}

		this.gonkaLotteryService.buyEnabled = this.gonkaLotteryService.drawsCount > 0 &&
			this.gonkaLotteryService.betCount > 0 &&
			this.gonkaLotteryService.gameType > 0 &&
			this.gonkaLotteryService.betSumma > 0;
	}

	/**
	 * Обработчик нажатия кнопок выбора количества ставок.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedBetCountHandler(button: IButtonGroupItem): void {
		this.gonkaLotteryService.betCountPrev = this.gonkaLotteryService.betCount;
		this.gonkaLotteryService.betCount = button ? parseInt(button.label, 10) : 0;

		if (this.gonkaLotteryService.isManualInput) {
			if (this.gonkaLotteryService.betCount > 1 && this.gonkaLotteryService.betCountPrev <= 1) {
				// <=1	Ручной	Предупреждение закрыто	Диалог закрыт	=>	>1	Ручной	Предупреждение открыто	Диалог закрыт
				this.toReturnWarning = 1;
				this.showWarning();
			} else if (this.gonkaLotteryService.betCount === 1 && this.gonkaLotteryService.betCountPrev > 1) {
				// >1	Ручной	Предупреждение закрыто	Диалог закрыт	=>	1	Ручной	Предупреждение закрыто	Диалог закрыт
				this.gonkaLotteryService.refreshBets();
			} else {
				this.updateState();
				this.gonkaLotteryService.refreshBets();
			}

		} else {
			// 1	Авто	Предупреждение закрыто	Диалог закрыт	=>	1	Авто	Предупреждение закрыто	Диалог закрыт
			// 1	Авто	Предупреждение закрыто	Диалог закрыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт
			// >1	Авто	Предупреждение закрыто	Диалог закрыт	=>	1	Авто	Предупреждение закрыто	Диалог закрыт
			// >1	Авто	Предупреждение закрыто	Диалог закрыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт
			this.updateState();
			this.gonkaLotteryService.refreshBets();
		}
	}


	/**
	 * Обработчик нажатия кнопок суммы ставки.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedBetSummaHandler(button: IButtonGroupItem): void {
		this.gonkaLotteryService.betSumma = button.selected ? parseInt(button.label, 10) : 0;
		this.updateState();
		this.gonkaLotteryService.refreshBets();
	}

	/**
	 * Обработчик нажатия кнопок количества тиражей.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedDrawsHandler(button: IButtonGroupItem): void {
		this.gonkaLotteryService.drawsCount = button ? parseInt(button.label, 10) : 0;
		this.updateState();
		this.gonkaLotteryService.refreshBets();
	}

	/**
	 * Обработчик нажатия типа игры.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedGameTypeHandler(button: IButtonGroupItem): void {
		this.gonkaLotteryService.gameTypePrev = this.gonkaLotteryService.gameType;
		this.gonkaLotteryService.gameType = button.selected ? +this.gameTypeComponent.getValueByButton(button) : 0;

		if (this.gonkaLotteryService.isManualInput) {
			// 1	Ручной	Предупреждение закрыто	Диалог закрыт	=>	1	Ручной	Предупреждение закрыто	Диалог открыт
			// >1	Ручной	Предупреждение закрыто	Диалог закрыт	=>	>1	Ручной	Предупреждение закрыто	Диалог открыт
			if (this.gonkaLotteryService.gameType) {
				this.toReturnDialog = 1;
				this.showDialog();
			} else {
				this.gonkaLotteryService.isManualInput = false;
				for (const gameType of GAME_TYPE_VALUES) {
					this.gonkaLotteryService.resetSelectedRaces(gameType);
				}
			}
		} else {
			// 1	Авто	Предупреждение закрыто	Диалог закрыт	=>	1	Авто	Предупреждение закрыто	Диалог закрыт
			// >1	Авто	Предупреждение закрыто	Диалог закрыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт
			this.updateState();
			this.gonkaLotteryService.refreshBets();
		}
	}

	/**
	 * Обработчик нажатия кнопки Назад из предупреждения.
	 */
	onWarningBackHandler(): void {

		// >1	Ручной	Предупреждение открыто	Диалог закрыт	=>	1	Ручной	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение открыто	Диалог закрыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение открыто	Диалог закрыт	=>	>1	Ручной	Предупреждение закрыто	Диалог закрыт

		if (this.toReturnWarning === 1) {
			this.gonkaLotteryService.betCount = this.gonkaLotteryService.betCountPrev;
		} else if (this.toReturnWarning === 2) {
			this.gonkaLotteryService.isManualInput = this.gonkaLotteryService.isManualInputPrev;
		}

		// this.gonkaLotteryService.gameType = this.gonkaLotteryService.gameTypePrev;

		this.updateState();
	}

	/**
	 * Обработчик нажатия кнопки "Подтвердить" из предупреждения.
	 */
	onWarningConfirmHandler(): void {
		// >1	Ручной	Предупреждение открыто	Диалог закрыт	=>	>1	Ручной	Предупреждение закрыто	Диалог открыт
		if (this.toReturnWarning === 1) {
			this.updateState();
			this.gonkaLotteryService.refreshBets();
		} else if (this.toReturnWarning === 2) {
			this.toReturnDialog = 2;
			this.showDialog();
		}
	}

	/**
	 * Обработчик нажатия кнопки "Закрыть" из диалога.
	 */
	onDialogCancelHandler(): void {
		// 1	Ручной	Предупреждение закрыто	Диалог открыт	=>	1	Авто	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение закрыто	Диалог открыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт

		// 1	Ручной	Предупреждение закрыто	Диалог открыт	=>	1	Ручной	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение закрыто	Диалог открыт	=>	1	Ручной	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение закрыто	Диалог открыт	=>	>1	Ручной	Предупреждение закрыто	Диалог закрыт

		// Возвращаем предыдущий выбор
		const gameLetter = this.GAME_TYPES_EN[this.gonkaLotteryService.gameType - 1];
		this.gonkaLotteryService[`races${gameLetter}`] = {...this.previousDialogSelection};

		// Очищаем выбор если
		if ((this.gonkaLotteryService.isManualInput && !this.gonkaLotteryService.isManualInputPrev) ||
			(this.gonkaLotteryService.isManualInput && (this.gonkaLotteryService.gameType !== this.gonkaLotteryService.gameTypePrev))) {
			this.gonkaLotteryService.resetSelectedRaces(this.gonkaLotteryService.gameType);
		}

		// this.gonkaLotteryService.betCount = this.gonkaLotteryService.betCountPrev;
		if (this.toReturnDialog === 1) {
			this.gonkaLotteryService.gameType = this.gonkaLotteryService.gameTypePrev;
		} else if (this.toReturnDialog === 2) {
			this.gonkaLotteryService.isManualInput = this.gonkaLotteryService.isManualInputPrev;
		}

		this.updateState();
	}

	/**
	 * Обработчик нажатия кнопки "Выбрать" из диалога.
	 */
	onDialogSelectHandler(): void {
		// 1	Ручной	Предупреждение закрыто	Диалог открыт	=>	1	Авто	Предупреждение закрыто	Диалог закрыт
		// 1	Ручной	Предупреждение закрыто	Диалог открыт	=>	1	Ручной	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение закрыто	Диалог открыт	=>	1	Ручной	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение закрыто	Диалог открыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт
		// >1	Ручной	Предупреждение закрыто	Диалог открыт	=>	>1	Ручной	Предупреждение закрыто	Диалог закрыт

		this.gonkaLotteryService.betCountPrev = this.gonkaLotteryService.betCount;
		this.gonkaLotteryService.gameTypePrev = this.gonkaLotteryService.gameType;
		this.gonkaLotteryService.isManualInputPrev = this.gonkaLotteryService.isManualInput;

		for (const gameTypeNum of this.GAME_TYPE_VALUES) {
			if (this.gonkaLotteryService.gameType !== gameTypeNum) {
				this.gonkaLotteryService.resetSelectedRaces(gameTypeNum);
			}
		}

		this.updateState();
		this.gonkaLotteryService.refreshBets();
		// console.log('Текущие ставки:', this.gonkaLotteryService.currentBets);
	}

	/**
	 * Обработчик нажатия подтипа игры.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedGameSubTypeHandler(button: IButtonGroupItem): void {
		this.gonkaLotteryService.isManualInputPrev = this.gonkaLotteryService.isManualInput;
		this.gonkaLotteryService.isManualInput = !!this.gameSubTypeComponent.getValueByButton(button);

		// если авто-режим, то очищаем выбор
		if (!this.gonkaLotteryService.isManualInput) {
			for (const gameTypeNum of this.GAME_TYPE_VALUES) {
				this.gonkaLotteryService.resetSelectedRaces(gameTypeNum);
			}
		}

		if (this.gonkaLotteryService.betCount > 1) {
			if (!this.gonkaLotteryService.isManualInput && this.gonkaLotteryService.isManualInputPrev) {
				// >1	Ручной	Предупреждение закрыто	Диалог закрыт	=>	>1	Авто	Предупреждение закрыто	Диалог закрыт
				this.gonkaLotteryService.refreshBets();
				// console.log('Текущие ставки:', this.gonkaLotteryService.currentBets);
			} else if (this.gonkaLotteryService.isManualInput && !this.gonkaLotteryService.isManualInputPrev) {
				// >1	Авто	Предупреждение закрыто	Диалог закрыт	=>	>1	Ручной	Предупреждение открыто	Диалог закрыт
				this.toReturnWarning = 2;
				this.showWarning();
			} else if (this.gonkaLotteryService.isManualInput && this.gonkaLotteryService.isManualInputPrev) {
				this.toReturnDialog = 0;
				this.showDialog();
			}
		} else {
			if (!this.gonkaLotteryService.isManualInput && this.gonkaLotteryService.isManualInputPrev) {
				// 1	Ручной	Предупреждение закрыто	Диалог закрыт	=>	1	Авто	Предупреждение закрыто	Диалог закрыт
				this.gonkaLotteryService.refreshBets();
				// console.log('Текущие ставки:', this.gonkaLotteryService.currentBets);
			} else if (this.gonkaLotteryService.isManualInput && !this.gonkaLotteryService.isManualInputPrev) {
				// 1	Авто	Предупреждение закрыто	Диалог закрыт	=>	1	Ручной	Предупреждение закрыто	Диалог открыт
				this.toReturnDialog = 2;
				this.showDialog();
			} else if (this.gonkaLotteryService.isManualInput && this.gonkaLotteryService.isManualInputPrev) {
				this.toReturnDialog = 0;
				this.showDialog();
			}
		}
	}

	/**
	 * Показать предупреждение о том, что выбранные числа победителей заездов,
	 * указанные в ручном режиме, будут применены для всех ставок
	 */
	showWarning(): void {
		this.dialogContainerService.showConfirmTwoButtons(
			'lottery.race_for_money.manual_winners_selection',
			'lottery.race_for_money.winners_extra',
			'lottery.race_for_money.winners_selection_message',
			{
				first: {
					text: 'header.continue_btn',
					click: () => this.onWarningConfirmHandler()
				},
				second: {
					text: 'header.back',
					click: () => this.onWarningBackHandler()
				}
			});
	}

	/**
	 * Сбросить ранее выбранные кнопки и показать диалог выбора победителей заездов
	 */
	showDialog(): void {
		const gameLetter = this.GAME_TYPES_EN[this.gonkaLotteryService.gameType - 1];
		this.previousDialogSelection = {...this.gonkaLotteryService[`races${gameLetter}`]};
		(this[`gameType${gameLetter}`] as OneButtonCustomComponent).show();
	}

	/**
	 * Функция для отслеживания изменений в массиве.
	 * @param index Индекс элемента
	 * @param item Элемент
	 */
	trackByFn = (index, item: any) => index;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.isVisibleSubtype = false;

		this.gonkaLotteryService.initBetData(BetDataSource.Manual);
		this.gonkaLotteryService.init();

		this.updateState();

		this.gonkaLotteryService.refreshBets();
	}

}
