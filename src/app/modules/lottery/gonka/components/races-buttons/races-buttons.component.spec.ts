import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RacesButtonsComponent } from './races-buttons.component';
import { TranslateModule } from '@ngx-translate/core';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { SharedModule } from '@app/shared/shared.module';
import { ButtonsGroupComponent } from "@app/shared/components/buttons-group/buttons-group.component";
import { QueryList } from "@angular/core";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {HttpClientModule} from "@angular/common/http";

describe('RacesButtonsComponent', () => {
	let component: RacesButtonsComponent;
	let fixture: ComponentFixture<RacesButtonsComponent>;
	let fakeButtons: any;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				SharedModule,
				CoreModule,
				HttpClientModule
			],
			declarations: [
				RacesButtonsComponent
			],
			providers: [
				LogService,
				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(RacesButtonsComponent);
		component = fixture.componentInstance;
		component.races = {
			participCount: 4,
			enabledRaces: [true, true, true, true],
			races: [
				{
					winnersToSelect: 2,
					selection: [0, 1]
				},
				{
					winnersToSelect: 2,
					selection: [0, 1]
				},
				{
					winnersToSelect: 2,
					selection: [0, 1]
				},
				{
					winnersToSelect: 2,
					selection: [0, 1]
				}
			],
			isValid: true
		};

		fakeButtons = {
			clearButtons: () => {},
			selectButtonByIndex: (index: number) => {},
			buttons: []
		};

		fakeButtons.buttons = [
			{selected: true},
			{selected: true},
			{selected: true},
			{selected: true}
		];

		component.selectRacesNumbers = {...fakeButtons} as ButtonsGroupComponent;

		fakeButtons.buttons = [
			{selected: true},
			{selected: true},
			{selected: false},
			{selected: false},
			{selected: false},
			{selected: false},
			{selected: false},
			{selected: false},
			{selected: false},
			{selected: false}
		];

		component.raceButtons = {
			toArray: () => [
				{...fakeButtons} as ButtonsGroupComponent,
				{...fakeButtons} as ButtonsGroupComponent,
				{...fakeButtons} as ButtonsGroupComponent,
				{...fakeButtons} as ButtonsGroupComponent
		]} as QueryList<ButtonsGroupComponent>;

		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectRaceNumbers handler', () => {
		component.selectRacesNumbers = {
			clearButtons: () => {},
			selectButtonByIndex: (index: number) => {},
			buttons: [
				{selected: true, index: 0, disabled: false, label: '1'},
				{selected: false, index: 1, disabled: false, label: '1'},
				{selected: false, index: 2, disabled: false, label: '1'},
				{selected: false, index: 3, disabled: false, label: '1'}
			]
		} as ButtonsGroupComponent;

		component.onSelectRaceNumbers({
			index: 0,
			label: '1',
			selected: true,
			disabled: false
		});

		expect(component.racesInfo.enabledRaces[0]).toBeTruthy();
	});

	it('test onSelectRaceNumbers handler', () => {
		component.onSelectedParticipantHandler({selected: true, index: 0, disabled: false, label: '1'}, 0);
		expect(component.racesInfo.races[0].selection).toEqual([1]);
	});

	it('test get races getter', () => {
		expect(component.races).toEqual(	{
				participCount: 4,
				enabledRaces: [true, true, true, true],
				races: [
					{
						winnersToSelect: 2,
						selection: [0, 1]
					},
					{
						winnersToSelect: 2,
						selection: [0, 1]
					},
					{
						winnersToSelect: 2,
						selection: [0, 1]
					},
					{
						winnersToSelect: 2,
						selection: [0, 1]
					}
				],
				isValid: true
			}
		);
	});
});
