import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import { IRacesInfo } from '@app/gonka/interfaces/iracesinfo';

/**
 * Компонент заездов.
 */
@Component({
	selector: 'app-races-buttons',
	templateUrl: './races-buttons.component.html',
	styleUrls: ['./races-buttons.component.scss']
})
export class RacesButtonsComponent implements OnInit, AfterViewInit {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Сеттер информации о заездах.
	 * @param val Информация о заездах.
	 */
	@Input()
	set races(val: IRacesInfo) {
		this.racesInfo = val;
		this.updateButtons();
		this.racesChange.emit(this.racesInfo);
	}

	/**
	 * Геттер информации о заездах.
	 */
	get races(): IRacesInfo {
		return this.racesInfo;
	}

	/**
	 * Показывать ли счетчики выбранных победителей.
	 */
	@Input()
	rowsCounters = false;

	/**
	 * Количество участников в заезде.
	 */
	@Input()
	participCount = 10;

	/**
	 * Количество заездов.
	 */
	@Input()
	racesCount = 4;

	/**
	 * Показывать ли кнопки выбора доступных заездов.
	 */
	@Input()
	racesSelection = false;

	/**
	 * Количество победителей в заезде.
	 */
	@Input()
	winnersToSelect = 1;

	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие, которое будет вызвано при изменении информации о заездах.
	 */
	@Output()
	readonly racesChange = new EventEmitter<IRacesInfo>();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на кнопки выбора доступных заездов.
	 */
	@ViewChild('selectRacesNumbers', { static: false })
	selectRacesNumbers: ButtonsGroupComponent;

	/**
	 * Ссылки на компоненты групп кнопок (участники заездов)
	 */
	@ViewChildren('raceButtons')
	raceButtons: QueryList<ButtonsGroupComponent>;

	/**
	 * Перечисление стилей кнопок, доступных к использованию
	 */
	readonly buttonGroupStyle = ButtonGroupStyle;

	/**
	 * Перечисление режимов работы групп кнопок
	 */
	readonly buttonGroupMode = ButtonGroupMode;

	/**
	 * Информация о заездах.
	 */
	racesInfo: IRacesInfo;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Функция для отслеживания изменений в массиве.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByFn = (index, item: any) => index;

	/**
	 * Обработчик нажатия кнопок выбора заездов.
	 *
	 * @param {IButtonGroupItem} button Нажатая кнопка.
	 */
	onSelectRaceNumbers(button: IButtonGroupItem): void {
		this.racesInfo.enabledRaces = this.selectRacesNumbers.buttons.map(elem => elem.selected);
		if (this.racesInfo.races) {
			this.racesInfo.races = this.racesInfo.races.map((elem: any, i) => this.racesInfo.enabledRaces[i] ?
				elem : {selection: [], winnersToSelect: elem.winnersToSelect});
		}

		for (let i = 0; i < this.racesInfo.enabledRaces.length; i++)  {
			if (!this.racesInfo.enabledRaces[i]) {
				this.raceButtons.toArray()[i]
					.clearButtons();
			}
		}

		this.checkValidity();
		this.racesChange.emit(this.racesInfo);
	}

	/**
	 * Обработчик нажатия кнопок выбора победителей заездов.
	 *
	 * @param {IButtonGroupItem} button Нажатая кнопка.
	 */
	onSelectedParticipantHandler(button: IButtonGroupItem, r): void {
		this.racesInfo.races[r].selection = this.raceButtons.toArray()[r].buttons
			.filter(el => el.selected)
			.map(el => el.index);

		this.checkValidity();
		this.racesChange.emit(this.racesInfo);
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Проверить валидность выбора.
	 */
	private checkValidity(): void {
		this.racesInfo.isValid = true;
		for (let i = 0; i < this.racesInfo.races.length; i++) {
			if (this.racesInfo.enabledRaces[i] &&
				this.racesInfo.races[i].selection.length !== this.racesInfo.races[i].winnersToSelect) {
				this.racesInfo.isValid = false;
			}
		}
		this.racesInfo.isValid = this.racesInfo.enabledRaces.filter(el => el).length === 0 ? false : this.racesInfo.isValid;
	}

	/**
	 * Сделать кнопки выбранными.
	 */
	private updateButtons(): void {
		if (this.selectRacesNumbers) {
			this.selectRacesNumbers.clearButtons();
			this.racesInfo.enabledRaces.forEach((el, i) => {
				if (el) {
					this.selectRacesNumbers.selectButtonByIndex(i);
				}
			});
		}

		if (this.raceButtons) {
			for (let i = 0; i < this.racesCount; i++) {
				this.raceButtons.toArray()[i]
					.clearButtons();

				if (this.racesInfo.enabledRaces[i]) {
					for (const btnIndex of this.racesInfo.races[i].selection) {
						this.raceButtons.toArray()[i]
							.selectButtonByIndex(btnIndex);
					}
				}
			}
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.updateButtons();
	}

	/**
	 * Жизненный цикл-хук, после инициализации представления и элементов DOM
	 */
	ngAfterViewInit(): void {
		this.updateButtons();
	}

}
