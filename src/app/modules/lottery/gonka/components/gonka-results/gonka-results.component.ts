import { Component } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDrawingResult } from '@app/core/net/http/api/models/get-draw-results';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { IAbstractViewResults, IResultPrintItem, PrintResultTypes } from '@app/core/services/results/results-utils';

/**
 * Интерфейс результатов гонки.
 */
interface IGonkaResults extends IAbstractViewResults {
	/**
	 * Выигрышные комбинации.
	 */
	winComb: Array<string>;
}

/**
 * Компонент результатов гонки на деньги.
 */
@Component({
	selector: 'app-gonka-results',
	templateUrl: './gonka-results.component.html',
	styleUrls: ['./gonka-results.component.scss']
})
export class GonkaResultsComponent  {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Данные для отображения.
	 */
	viewData: IGonkaResults;

	/**
	 * Данные для печати.
	 */
	printData: Array<IResultPrintItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GameResultsService} gameResultsService Сервис результатов игр.
	 */
	constructor(
		readonly gameResultsService: GameResultsService
	) {}

	/**
	 * Обработчик нажатия выбора результатов по тиражу.
	 *
	 * @param {IDrawingResult} result Результаты тиража.
	 */
	onSelectedResultsHandler(result: IDrawingResult): void {
		if (!result) {
			this.viewData = undefined;
			this.printData = undefined;

			return;
		}

		const drawNumber = result.draw_name;
		const drawDate = result.drawing_date_begin;
		const extraInfo = result.drawing[0].extra_info;

		let winComb: Array<string>;
		if (result.drawing[0].win_comb) {
			winComb = result.drawing[0].win_comb
				.split(',')
				.filter(f => !!f);
		}

		this.viewData = {drawNumber, drawDate, extraInfo, winComb};
		this.printData = this.printDataParser();
	}

	/**
	 * Функция для отслеживания изменений в массиве выигрышных комбинаций.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByWinCombFn = (index, item: string) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Функция-парсер данных для печати.
	 */
	private printDataParser(): Array<IResultPrintItem> {
		const result = [
			{key: PrintResultTypes.LotteryName, value: ['lottery.race_for_money.loto_gonka']},
			{key: PrintResultTypes.DrawNumber, value: [this.viewData.drawNumber]},
			{key: PrintResultTypes.DrawDate, value: [this.viewData.drawDate]}
		];

		result.push({key: PrintResultTypes.Line, value: undefined});
		result.push({key: PrintResultTypes.TwoColumnTable, value: ['lottery.draw', 'main_navigation.results']});
		result.push({key: PrintResultTypes.Line, value: undefined});
		result.push({key: PrintResultTypes.TwoColumnTableFast, value: [
			this.viewData.drawNumber,
			this.viewData.winComb ? this.viewData.winComb.join(' ') : undefined
		]});
		result.push({key: PrintResultTypes.Line, value: undefined});

		return result;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
