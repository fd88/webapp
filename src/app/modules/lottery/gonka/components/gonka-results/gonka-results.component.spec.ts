import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { SharedModule } from '@app/shared/shared.module';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';

import { GameResultsService } from '@app/core/services/results/game-results.service';
import { HttpService } from '@app/core/net/http/services/http.service';


import { AppStoreService } from '@app/core/services/store/app-store.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';

import { GonkaResultsComponent } from '@app/gonka/components/gonka-results/gonka-results.component';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';

describe('GonkaResultsComponent', () => {
	let component: GonkaResultsComponent;
	let fixture: ComponentFixture<GonkaResultsComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				SharedModule,
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot({})
			],
			declarations: [
				GonkaResultsComponent
			],
			providers: [
				LogService,
				GameResultsService,
				HttpService,


				DatePipe,
				MslCurrencyPipe,
				AppStoreService,
				StorageService,
				TransactionService,
				PrintService,
				LotteriesService,
				TranslateService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				}
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(GonkaResultsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});


	it('test onSelectedResultsHandler', () => {
		component.onSelectedResultsHandler(undefined);
		expect(component.viewData).toBeUndefined();

		const result = {
			"draw_code": 32663,
			"draw_name": "270619/125",
			"drawing": [
				{
					"extra_info": "",
					"name": "Основний розiграш",
					"win_comb": "10,7,10,7",
					"win_cat": [],
					"data": {
						"events": []
					}
				}
			],
			"drawing_date_begin": "2019-06-27 18:45:50",
			"drawing_date_end": "2019-12-24 18:49:40",
			"winning_date_end": "2019-12-24 18:49:40"
		};

		component.onSelectedResultsHandler(result);
		expect(component.viewData).toEqual({
			"drawNumber":"270619/125",
			"drawDate":"2019-06-27 18:45:50",
			"extraInfo":"",
			"winComb":["10","7","10","7"]
		});

		result.drawing[0].win_comb = '';
		component.onSelectedResultsHandler(result);
		expect(component.viewData).toEqual({
			"drawNumber":"270619/125",
			"drawDate":"2019-06-27 18:45:50",
			"extraInfo":"",
			"winComb": undefined
		});
	});

	it('test trackByWinCombFn', () => {
		expect(component.trackByWinCombFn(1, '5')).toEqual(1);
	});

});
