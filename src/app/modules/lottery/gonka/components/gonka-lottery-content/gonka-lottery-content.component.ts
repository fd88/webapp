import { Component, OnDestroy } from '@angular/core';

import { GonkaLotteryService } from '@app/gonka/services/gonka-lottery.service';

/**
 * Корневой компонент модуля игры "Гонка на гроши".
 * Относительно данного компонента будет осуществляться дальнейшая навигация по под-компонентам игры.
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class GonkaLotteryContentComponent implements  OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GonkaLotteryService} gonkaLotteryService Сервис игры "Гонка на гроши".
	 */
	constructor(
		private readonly gonkaLotteryService: GonkaLotteryService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Вызывается при уничтожении компонента.
	 */
	ngOnDestroy(): void {
		this.gonkaLotteryService.setDefaults();
		this.gonkaLotteryService.resetBetData();
	}
}
