import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';


import { HttpService } from '@app/core/net/http/services/http.service';

import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';
import { GonkaLotteryContentComponent } from '@app/gonka/components/gonka-lottery-content/gonka-lottery-content.component';
import { GonkaLotteryService } from '@app/gonka/services/gonka-lottery.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';

describe('GonkaLotteryContentComponent', () => {
	let component: GonkaLotteryContentComponent;
	let fixture: ComponentFixture<GonkaLotteryContentComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			declarations: [
				GonkaLotteryContentComponent
			],
			providers: [
				DeclineWordPipe,
				GonkaLotteryService,
				HttpService,
				LotteriesService
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GonkaLotteryContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
