import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { URL_GONKA } from '@app/util/route-utils';
import { generateRandomNumber, generateRandomNumberWithExclude } from '@app/util/utils';
import { TranslateService } from '@ngx-translate/core';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { GonkaRegBetReq, GonkaRegBetResp, IGonkaBet } from '@app/core/net/http/api/models/gonka-reg-bet';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { BaseGameService } from '@app/core/game/base-game.service';
import { IGonkaBetDataItem } from '@app/gonka/interfaces/igonka-bet-data-item';
import { IRacesInfo } from '@app/gonka/interfaces/iracesinfo';
import { LogOutService } from '@app/logout/services/log-out.service';

/**
 * Типы заездов на русском языке.
 */
export const GAME_TYPES = ['А', 'Б', 'В', 'Г', 'Д'];

/**
 * Типы заездов на английском языке.
 */
export const GAME_TYPES_EN = ['A', 'B', 'V', 'G', 'D'];

/**
 * Список значений типов игры "Гонка на гроши".
 */
export const GAME_TYPE_VALUES = [1, 2, 3, 4, 5];

/**
 * Список количества тиражей.
 */
export const DRAWS_LABELS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20];

/**
 * Список возможных сумм ставок.
 */
export const BET_SUMMA_LIST = [2, 4, 6, 8];

/**
 * Список типов игры "Гонка на гроши".
 */
export enum GonkaGameType {
	/**
	 * А - 4 заезда по 10 участников.
	 */
	A = 'А',
	/**
	 * Б - 4 заезда по 10 участников.
	 */
	B = 'Б',
	/**
	 * В - 2 заезда по 10 участников.
	 */
	V = 'В',
	/**
	 * Г - 2 заезда по 10 участников.
	 */
	G = 'Г',
	/**
	 * Д - Любые из 4-х заездов по 1 или 2 победителей из 10 участников.
	 */
	D = 'Д'
}

/**
 * Возвращает модель с начальными данными для игры "Гонка на гроши".
 * Вызывается в момент перехода на форму ручного ввода ставки по игре.
 */
const initBetDataFn = (): IGonkaBetDataItem => {
	return {
		gameType: 1,
		betsCount: 1,
		drawCount: 1,
		betSumma: 0
	};
};

/**
 * Возвращает модель с начальными данными для игры "Гонка на гроши" для типа игры А.
 */
const INITIAL_A: IRacesInfo = {
	participCount: 10,
	enabledRaces: [true, true, true, true],
	isValid: false,
	races: Array.from({length: 4}, () => ({
		winnersToSelect: 1,
		selection: []
	}))
};

/**
 * Возвращает модель с начальными данными для игры "Гонка на гроши" для типа игры Б.
 */
const INITIAL_B: IRacesInfo = {
	participCount: 10,
	enabledRaces: [true, true, true, true],
	isValid: false,
	races: Array.from({length: 4}, () => ({
		winnersToSelect: 1,
		selection: []
	}))
};

/**
 * Возвращает модель с начальными данными для игры "Гонка на гроши" для типа игры В.
 */
const INITIAL_V: IRacesInfo = {
	participCount: 10,
	enabledRaces: [true, true, false, false],
	isValid: false,
	races: Array.from({length: 4}, () => ({
		winnersToSelect: 1,
		selection: []
	}))
};

/**
 * Возвращает модель с начальными данными для игры "Гонка на гроши" для типа игры Г.
 */
const INITIAL_G: IRacesInfo = {
	participCount: 10,
	enabledRaces: [false, false, true, true],
	isValid: false,
	races: Array.from({length: 4}, () => ({
		winnersToSelect: 1,
		selection: []
	}))
};

/**
 * Возвращает модель с начальными данными для игры "Гонка на гроши" для типа игры Д.
 */
const INITIAL_D: IRacesInfo = {
	enabledRaces: [false, false, false, false],
	participCount: 10,
	isValid: false,
	races: Array.from({length: 4}, () => ({
		winnersToSelect: 2,
		selection: []
	}))
};

/**
 * Сервис лотереи "Гонка на гроши".
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей, и взаимодейсвия с ЦС.
 */
@Injectable({
	providedIn: 'root'
})
export class GonkaLotteryService extends BaseGameService<IGonkaBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * URL игры.
	 */
	readonly gameUrl = URL_GONKA;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.race_for_money.loto_gonka';

	/**
	 * Выбранные участники заездов для типа игры А.
	 */
	racesA: IRacesInfo;

	/**
	 * Выбранные участники заездов для типа игры Б.
	 */
	racesB: IRacesInfo;

	/**
	 * Выбранные участники заездов для типа игры В.
	 */
	racesV: IRacesInfo;

	/**
	 * Выбранные участники заездов для типа игры Г.
	 */
	racesG: IRacesInfo;

	/**
	 * Выбранные участники заездов для типа игры Д.
	 */
	racesD: IRacesInfo;

	/**
	 * Список текущих ставок.
	 * Содержит массив моделей типа {@link IGonkaBet}.
	 */
	currentBets: Array<IGonkaBet> = [];

	/**
	 * Количество тиражей, в которых сыграет билет.
	 */
	drawsCount = 0;

	/**
	 * Количество ставок в тираже.
	 */
	betCount = 0;

	/**
	 * Количество ставок в тираже (предыдущее).
	 */
	betCountPrev = 0;

	/**
	 * Сумма ставки.
	 * Используется в расчете суммы чека.
	 */
	betSumma = 0;

	/**
	 * Тип игры (1..5).
	 */
	gameType = 0;

	/**
	 * Тип игры (1..5) (предыдущее).
	 */
	gameTypePrev = 0;

	/**
	 * Подтип игры (1..5).
	 */
	gameSubtype: Array<number>;

	/**
	 * Список текущих тиражей.
	 */
	draws: Array<UpdateDrawInfoDraws>;

	/**
	 * Общее количество ставок, отображаемое в чеке.
	 */
	commonBetCount = 0;

	/**
	 * Режим ручного ввода купона.
	 * Соответствует функционалу метки "АВТО" на купоне.
	 */
	isManualInput = false;

	/**
	 * Режим ручного ввода купона.
	 * Предыдущее значение
	 */
	isManualInputPrev = false;

	/**
	 * Флаги активности кнопок.
	 */
	buyEnabled = false;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {PrintService} printService Сервис печати.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TranslateService} translateService Сервис переводов.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект для работы с адресной строкой браузера.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		private readonly printService: PrintService,
		private readonly transactionService: TransactionService,
		readonly appStoreService: AppStoreService,
		readonly dialogInfoService: DialogContainerService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();

		this.initialBetDataFactory = initBetDataFn;
		this.currentGameCode = LotteryGameCode.Gonka;
		this.currentGroupCode = LotteriesGroupCode.Regular;
	}

	/**
	 * Инициализация и очистка модели (предыдущего состояния).
	 */
	init(): void {
		this.draws = this.appStoreService.Draws.getDraws(LotteryGameCode.Gonka);

		if (!this.racesA) {
			this.resetSelectedRaces(1);
		}

		if (!this.racesB) {
			this.resetSelectedRaces(2);
		}

		if (!this.racesV) {
			this.resetSelectedRaces(3);
		}

		if (!this.racesG) {
			this.resetSelectedRaces(4);
		}

		if (!this.racesD) {
			this.resetSelectedRaces(5);
		}
	}

	/**
	 * Возврат к дефолтным значениям.
	 */
	setDefaults(): void {
		this.drawsCount = 0;

		// параметры с предыдущимим значениями
		this.betCount = 0;
		this.betCountPrev = 0;

		this.isManualInput = false;
		this.isManualInputPrev = false;

		this.gameType = 0;
		this.gameTypePrev = 0;

		this.betSumma = 0;
		this.currentBets = [];

		this.buyEnabled = false;

		for (const gameType of GAME_TYPE_VALUES) {
			this.resetSelectedRaces(gameType);
		}
	}

	/**
	 * Сброс выбранных заездов для указанного типа игры.
	 * @param gameType Тип игры.
	 */
	resetSelectedRaces(gameType: number): void {
		if (gameType === 1) {
			this.racesA = JSON.parse(JSON.stringify(INITIAL_A));
		} else if (gameType === 2) {
			this.racesB = JSON.parse(JSON.stringify(INITIAL_B));
		} else if (gameType === 3) {
			this.racesV = JSON.parse(JSON.stringify(INITIAL_V));
		} else if (gameType === 4) {
			this.racesG = JSON.parse(JSON.stringify(INITIAL_G));
		} else if (gameType === 5) {
			this.racesD = JSON.parse(JSON.stringify(INITIAL_D));
		}
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновление ставок.
	 * @param fromScanner Признак, что ввод произошел со сканера.
	 */
	refreshBets(fromScanner?: boolean): void {
		if (!fromScanner && this.isManualInput) {
			// ручной или авторежим
			let betElem: IGonkaBet;
			const gameTypeCurr = (this[`races${GAME_TYPES_EN[this.gameType - 1]}`] as IRacesInfo);

			betElem = {
				R: gameTypeCurr.races.map(el => {
					return el.selection.map(num => num + 1);
				}),
				S: this.betSumma / 2,
				T: this.gameType
			};

			if (this.gameType === 5) {
				betElem['RN'] = gameTypeCurr.enabledRaces
					.map((el, i) => {
						if (el) {
							return i;
						}
					})
					.filter(el => typeof el !== 'undefined');
			}

			this.currentBets = (new Array<IGonkaBet>(this.betCount)).fill(betElem);
		} else if (!this.isManualInput) {
			// сгенерировать авто-ставку
			const arr: Array<IGonkaBet> = [];
			const S = this.betSumma / 2;
			Array.from(Array(this.betCount))
				.forEach(() => {
					let pos: Array<Array<number>>;
					let RN: Array<number>;
					if (this.gameType === 1 || this.gameType === 2) {
						pos = Array.from(new Array(4))
							.map(() => [generateRandomNumber(0, 9)]);
					} else if (this.gameType === 3 || this.gameType === 4) {
						pos = Array.from(new Array(2))
							.map(() => [generateRandomNumber(0, 9)]);
						pos = (this.gameType === 3 ? pos.concat([[], []]) : [[], []].concat(pos));
					} else if (this.gameType === 5) {
						// если массив заездов задан - генерируем ставки по нему
						// если массив заездов не задан - делаем одну случайную ставку
						RN = this.gameSubtype && this.gameSubtype.length > 0
							? this.gameSubtype
							: [generateRandomNumber(1, 4)];

						// генерируем пары победителей для RN
						pos = [[], [], [], []];
						RN.forEach(rn => {
							const v1 = generateRandomNumber(0, 9);
							const v2 = generateRandomNumberWithExclude(0, 9, v1);
							pos[rn - 1] = [v1, v2];
						});
					}

					const bet: IGonkaBet = {
						R: pos,
						S,
						T: this.gameType,
						RN
					};

					Logger.Log.i('GonkaLotteryService', 'AUTO generated a new bet: %s', bet)
						.console();

					arr.push(bet);
				});

			this.currentBets = arr;
		}

		// this.calculateAmount();
		// расчитать сумму ставки
		let amount = 0;
		let commonBetCount = 0;
		this.currentBets
			.forEach(v => {
				const multiplier = v.RN ? v.RN.length : 1;
				amount += (multiplier * this.drawsCount * this.betSumma);
				commonBetCount += multiplier;
			});

		this.commonBetCount = commonBetCount;
		this.amount = amount;
	}

	/**
	 * Купить лотерею
	 */
	buyLottery(): void {
		Logger.Log.i('GonkaLotteryService', 'buyLottery -> start buying Gonka: %s', this.currentBets)
			.console();

		// корректировка ставок для случая, если в них есть 10-ки
		this.currentBets = this.currentBets.map(elem => {
			elem.R = elem.R.map(el => {
				return el.map(el2 => el2 === 10 ? 0 : el2);
			});

			return elem;
		});

		const request = new GonkaRegBetReq(
			this.appStoreService,
			this.currentBets.length,
			this.currentBets,
			this.drawsCount,
			this.betData
		);

		this.transactionService.buyLottery(GonkaRegBetResp, LotteryGameCode.Gonka, request);
	}
}
