import {TestBed, waitForAsync} from '@angular/core/testing';
import { Injector } from '@angular/core';

import { AppStoreService } from '@app/core/services/store/app-store.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource } from '@app/core/game/base-game.service';
import { EsapActions } from '@app/core/configuration/esap';
import { Operator } from '@app/core/services/store/operator';
import { GonkaLotteryService } from '@app/gonka/services/gonka-lottery.service';
import {LogOutService} from "@app/logout/services/log-out.service";
import {LotteriesService} from "@app/core/services/lotteries.service";
import {DRAWS_FOR_GAME_GONKA} from "../../../features/mocks/gonka-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";

xdescribe('GonkaLotteryService', () => {
	let service: GonkaLotteryService | any;
	let appStoreService: AppStoreService;
	let lotteriesService: LotteriesService;

	beforeEach(waitForAsync(() => {
			TestBed.configureTestingModule({
				imports: [
					RouterTestingModule,
					HttpClientModule,
					TranslateModule.forRoot()
				],
				providers: [
					LogService,
					PrintService,
					Injector,
					GonkaLotteryService,
					LogOutService,
					HttpService,
					AppStoreService,
					DialogContainerService,
					TransactionService,
					LotteriesService
				]
			}).compileComponents();

		TestBed.inject(LogService);

		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Gonka, DRAWS_FOR_GAME_GONKA);
		const operator = new Operator('testMegalotUser', `${Date.now()}`, '123456', 1);
		appStoreService.operator.next(operator);

		service = TestBed.inject(GonkaLotteryService);
		service.initBetData(BetDataSource.Manual);
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(
			LotteryGameCode.Gonka,
			new Map<string, string>([[EsapActions.GonkaRegBet, 'url']]
		));
		lotteriesService = TestBed.inject(LotteriesService);
		lotteriesService.reload().then();
	}));

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test init method', () => {
		spyOn(service, 'init').and.callThrough();

		service.init();
		expect(service.init).toHaveBeenCalled();

		service.racesA = {};
		service.racesB = {};
		service.racesV = {};
		service.racesG = {};
		service.racesD = {};

		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test refreshBets method', () => {
		service.isManualInput = true;
		// позже скорее всего эти свойства будут в betData
		service.betCount = 1;
		service.gameType = 1;
		service.betSumma = 2;
		service.drawsCount = 1;

		service.racesA = {
			"participCount": 10,
			"enabledRaces": [
				true,
				true,
				true,
				true
			],
			"isValid": true,
			"races": [
				{
					"winnersToSelect": 1,
					"selection": [
						0
					]
				},
				{
					"winnersToSelect": 1,
					"selection": [
						1
					]
				},
				{
					"winnersToSelect": 1,
					"selection": [
						2
					]
				},
				{
					"winnersToSelect": 1,
					"selection": [
						3
					]
				}
			]
		};

		service.refreshBets(false);
		expect(service.amount).toEqual(2);


		service.gameType = 5;
		service.racesD = {...service.racesA};
		service.racesD.races = service.racesD.races.map(() => ({
			"winnersToSelect": 2,
			"selection": [
				0, 1
			]
		}));

		service.refreshBets(false);
		expect(service.amount).toEqual(8);


		service.isManualInput = false;

		service.gameType = 1;
		service.refreshBets(false);
		expect(service.amount).toEqual(2);

		service.gameType = 2;
		service.racesB = {...service.racesA};
		service.refreshBets(false);
		expect(service.amount).toEqual(2);

		service.gameType = 3;
		service.racesV = {...service.racesA};
		service.refreshBets(false);
		expect(service.amount).toEqual(2);

		service.gameType = 4;
		service.racesG = {...service.racesA};
		service.refreshBets(false);
		expect(service.amount).toEqual(2);

		service.gameType = 5;
		service.refreshBets(false);
		expect(service.amount).toEqual(2);
	});


	xit('test buyLottery method', () => {
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();

		service.currentBets = [{"R":[[10],[10],[10],[10]],"S":1,"T":1}];
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();


		service.printService.isReady = () => true;
		service.transactionService.executeRequest = (): Promise<void> => new Promise((resolve, reject) => reject({code: 1000}));
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();

		service.printService.isReady = () => true;
		service.transactionService.executeRequest = (): Promise<void> => new Promise(resolve => resolve());
		service.buyLottery();
		expect(service.buyLottery).toHaveBeenCalled();
	});

});
