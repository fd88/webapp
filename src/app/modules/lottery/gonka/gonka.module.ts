import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';
import { GonkaRoutingModule } from '@app/gonka/gonka-routing.module';

import { GonkaLotteryService } from '@app/gonka/services/gonka-lottery.service';

import { GonkaLotteryContentComponent } from '@app/gonka/components/gonka-lottery-content/gonka-lottery-content.component';
import { GonkaLotteryInitComponent } from '@app/gonka/components/gonka-lottery-init/gonka-lottery-init.component';
import { GonkaResultsComponent } from '@app/gonka/components/gonka-results/gonka-results.component';
import { RacesButtonsComponent } from '@app/gonka/components/races-buttons/races-buttons.component';

/**
 * Модуль лотереи "Гонка на гроши".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		GonkaRoutingModule,
		SharedModule
	],
	declarations: [
		GonkaLotteryContentComponent,
		GonkaLotteryInitComponent,
		GonkaResultsComponent,
		RacesButtonsComponent
	],
	providers: [
		GonkaLotteryService
	]
})
export class GonkaModule {}
