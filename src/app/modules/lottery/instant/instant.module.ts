import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';
import { InstantRoutingModule } from '@app/instant/instant-routing.module';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { InstantLotteriesContentComponent } from '@app/instant/components/instant-lotteries-content/instant-lotteries-content.component';
import { InstantLotteriesInitComponent } from '@app/instant/components/instant-lotteries-init/instant-lotteries-init.component';
import { InstantGameListComponent } from '@app/instant/components/instant-game-list/instant-game-list.component';
import { InstantResultsComponent } from '@app/instant/components/instant-results/instant-results.component';

/**
 * Модуль лотереи "Стирачки и Золотой триумф".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		InstantRoutingModule,
		SharedModule
	],
	declarations: [
		InstantLotteriesContentComponent,
		InstantLotteriesInitComponent,
		InstantGameListComponent,
		InstantResultsComponent
	],
	providers: [
		InstantLotteryService
	]
})
export class InstantModule {}
