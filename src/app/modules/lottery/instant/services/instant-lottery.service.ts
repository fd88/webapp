import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { URL_INSTANT } from '@app/util/route-utils';
import { numberFromStringCurrencyFormat } from '@app/util/utils';
import { Lotteries, LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { EInstantRegBetReq, EInstantRegBetResp } from '@app/core/net/http/api/models/e-instant-reg-bet';
import { UpdateDrawInfoDraw, UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { HttpService } from '@app/core/net/http/services/http.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { BaseGameService } from '@app/core/game/base-game.service';
import { IInstantBetDataItem, IInstantBetDataModel } from '@app/instant/interfaces/iinstant-bet-data-item';
import { LogOutService } from '@app/logout/services/log-out.service';

/**
 * Возвращает модель с начальными данными для игры "Стирачки и Золотой триумф".
 */
const initBetDataFn = (): IInstantBetDataItem => {
	const group: Array<IInstantBetDataModel> = [];
	group[LotteriesGroupCode.EInstant] = {
		gameIndex: NaN,
		drawIndex: NaN,
		betsCount: NaN
	};
	group[LotteriesGroupCode.VBL] = {
		gameIndex: NaN,
		drawIndex: NaN,
		betsCount: NaN
	};

	return {
		priceFilter: 0,
		drawCount: NaN,
		group
	};
};

/**
 * Интерфейс элемента лотереи для показа на экране.
 */
export interface IInstantGameData {
	/**
	 * Код лотереи.
	 */
	code: LotteryGameCode | number;
	/**
	 * Название лотереи.
	 */
	name: string;
	/**
	 * Цена билета.
	 */
	price: string;
	/**
	 * Список тиражей для лотереи.
	 */
	draws: Array<UpdateDrawInfoDraws>;
	/**
	 * Индекс игры в списке игр.
	 */
	gameListIndex: number;
}

/**
 * Сервис электронных моменталок.
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице моментальных лотерей, и взаимодействия с ЦС.
 */
@Injectable()
export class InstantLotteryService extends BaseGameService<IInstantBetDataItem> {

	// -----------------------------
	//  Protected properties
	// -----------------------------
	/**
	 * Текущая ставка в грн.
 	 * @protected
	 */
	protected _amount: number;

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на страницу моментальных лотерей.
	 */
	readonly gameUrl = URL_INSTANT;

	/**
	 * Название выбранной игры.
	 */
	gameName = '';

	/**
	 * Максимальное количество билетов.
	 */
	readonly TICKETS_COUNT = 10;

	/**
	 * Группа моментальных лотерей.
	 */
	groupId: number;

	/**
	 * Текущий выбранный тираж.
	 */
	gameDraw: UpdateDrawInfoDraw;

	/**
	 * Код выбранного тиража.
	 */
	gameDrawCode: string;

	/**
	 * Номер серии выбранного тиража.
	 */
	gameDrawSelected: string;

	/**
	 * Список моментальных лотерей.
	 */
	lotteries: Array<IInstantGameData> = [];

	/**
	 * Список тиражей для моментальных лотерей.
	 */
	gameDraws: Array<UpdateDrawInfoDraws> = [];

	/**
	 * Название группы моментальных лотерей.
	 */
	gameLabel = '';

	/**
	 * Список значений фильтров.
	 */
	filtersValues: Array<number>;

	/**
	 * Список названий фильтров.
	 */
	filtersLabels: Array<string>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения.
	 * @param {HttpService} httpService Сервис для работы с http запросами.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {TranslateService} translateService Сервис перевода.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект ервис для работы с адресной строкой.
	 * @param logoutService Сервис для работы с выходом из системы.
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly httpService: HttpService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();

		this.initialBetDataFactory = initBetDataFn;
	}

	/**
	 * Сеттер текущей суммы ставки в грн.
	 *
	 * @param {number} value Новое значение суммы ставки.
	 */
	set amount(value: number) {
		this._amount = value;
	}

	/**
	 * Геттер текущей суммы ставки в грн.
	 */
	get amount(): number {
		return this._amount;
	}

	/**
	 * Задать группу моментальных лотерей.
	 * Строит список моментальных лотерей на основе выбранной группы.
	 * После постройки активирует по возможности первый элемент.
	 *
	 * @param {number} id Идентификатор группы моменталок.
	 */
	setGroupId(id: LotteriesGroupCode): void {
		if (id === LotteriesGroupCode.EInstant) {
			this.gameLabel = 'lottery.instant.loto_momentary';
		} else if (id === LotteriesGroupCode.VBL) {
			this.gameLabel = 'lottery.instant.loto_golden_triumph';
		} else {
			Logger.Log.e('InstantLotteriesService', `setGroupId -> unknown instant lottery group: ${id}`)
				.console();

			return;
		}

		Logger.Log.i('InstantLotteriesService', `setGroupId -> selected new group: ${id}`)
			.console();

		this.groupId = id;
		this.currentGroupCode = id;

		// заполнить массив игр для выбранной группы
		this.lotteries.splice(0);
		this.lotteries = Lotteries.getInstantLotteriesCodes(this.groupId)
			.map(code => {
				const lottery = this.appStoreService.Draws.getLottery(code);
				let price;
				if (!!lottery && Array.isArray(lottery.draws) && lottery.draws.length > 0) {
					price = +lottery.draws[0].draw.bet_sum;
				}

				return {
					code,
					name: lottery.lott_name,
					price,
					draws: lottery.draws
				};
			})
			.sort((el1, el2) => el1.name.localeCompare(el2.name))
			.map((el, gameListIndex) => {
				return {...el, gameListIndex};
			});
	}

	/**
	 * Выбор моментальной игры из списка лотерей.
	 * Назначает выбранную лотерею текущей и получает список тиражей по ней.
	 * После этого выбирается первый тираж из списка.
	 *
	 * @param {number} index Индекс лотереи в списке {@link lotteries}.
	 */
	selectGame(index: number): void {
		const gameData = this.lotteries[index];

		Logger.Log.i('InstantLotteriesService', 'selected game %s', gameData)
			.console();

		this.betData.group[this.groupId].gameIndex = index;
		this.gameDraws = [...this.appStoreService.Draws.getDraws(gameData.code)];
		this.gameName = gameData.name;
		this.selectDraw(0);

		this.currentGameCode = gameData.code;
	}

	/**
	 * Выбор тиража по его индексу или по объекту.
	 *
	 * @param {number | UpdateDrawInfoDraws} value Индекс тиража в списке тиражей или непосредственно сам объект тиража.
	 */
	selectDraw(value: number | UpdateDrawInfoDraws): void {
		let drawItem: UpdateDrawInfoDraws;
		if (value instanceof UpdateDrawInfoDraws) {
			drawItem = value;
			this.betData.group[this.groupId].drawIndex = this.gameDraws.indexOf(value);
		} else {
			drawItem = this.gameDraws ? this.gameDraws[value] : undefined;
			this.betData.group[this.groupId].drawIndex = value;
		}

		if (drawItem) {
			this.gameDraw = drawItem.draw;
			this.gameDrawSelected = drawItem.draw.serie_num;
			this.gameDrawCode = drawItem.draw.code;
		}
	}

	/**
	 * Создает список фильтров по текущему списку лотерей.
	 */
	createFilterByLotteries(): void {
		const uniqBetSumm = Array.from(new Set(
			this.lotteries
				.map(m => m.draws.map(mm => Math.round(+mm.draw.bet_sum)))
				.reduce((p, c) => p.concat(c), [])
				.sort((a, b) => a - b)
		));
		const valuesForFilter = uniqBetSumm.slice(0, 7);
		this.filtersValues = [0, ...valuesForFilter];
		this.filtersLabels = ['lottery.instant.all', ...valuesForFilter.map(m => `${m}`)];
	}

	// -----------------------------
	//  IBaseGameService interface
	// -----------------------------
	/**
	 * Обновляет сделанные ставки.
	 */
	refreshBets(): void {
		let amount = 0;
		if (this.gameDraws.length > this.betData.group[this.groupId].drawIndex) {
			if (this.gameDraws[this.betData.group[this.groupId].drawIndex]) {
				if (this.gameDraws[this.betData.group[this.groupId].drawIndex].draw) {
					const bs = this.gameDraws[this.betData.group[this.groupId].drawIndex].draw.bet_sum;
					amount = numberFromStringCurrencyFormat(bs) * this.betData.group[this.groupId].betsCount / 100;
				}
			}
		}
		this.amount = amount;
	}

	/**
	 * Покупка лотереи.
	 */
	buyLottery(): void {
		Logger.Log.i('InstantLotteryService', 'buyLottery -> start buying EInstant')
			.console();

		const buyEInstant = new EInstantRegBetReq(
			this.appStoreService,
			this.lotteries[this.betData.group[this.groupId].gameIndex].code,
			this.betData.group[this.groupId].betsCount,
			this.gameDrawCode,
			this.betData
		);

		const code = this.lotteries[this.betData.group[this.groupId].gameIndex].code;
		this.transactionService.buyLottery(EInstantRegBetResp, code, buyEInstant);
	}
}
