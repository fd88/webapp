import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';

import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';

import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { Lotteries, LotteriesGroupCode } from '@app/core/configuration/lotteries';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { BetDataSource } from "@app/core/game/base-game.service";
import { EsapActions } from "@app/core/configuration/esap";
import { UpdateDrawInfoDraws } from "@app/core/net/http/api/models/update-draw-info";
import { IInstantBetDataItem, IInstantBetDataModel } from "@app/instant/interfaces/iinstant-bet-data-item";
import { IDialog } from "@app/core/dialog/types";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import {DRAWS_FOR_INSTANT} from "../../../features/mocks/instant-draws";

xdescribe('InstantLotteryService', () => {
	let service: InstantLotteryService | any;
	let appStoreService: AppStoreService | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				LotteriesService,
				InstantLotteryService,
				AppStoreService,
				StorageService,
				HttpService,
				DialogContainerService,
				PrintService,

				TransactionService,


				Lotteries
			]
		});

		TestBed.inject(LogService);
		TestBed.inject(Lotteries);
		Lotteries.getInstantLotteriesCodes = () => [106,107,108,109,120,121,122,123,124,125,127,
			128,129,130,131,132,133,134,135,136,137,
			139,140,141,142,143,144,145,146,147,149,
			126,151,152,153,154,155,156,157,160,161,162,
			163];

		appStoreService = TestBed.inject(AppStoreService);
		/*
                const operator = new Operator('testInstantUser', `${Date.now()}`, '123456', 1);
                appStoreService.operator.next(operator);
        */

		service = TestBed.inject(InstantLotteryService);
		service.updateActiveDraws = () => {
			service.activeDraws = [...DRAWS_FOR_INSTANT.lottery.draws];
		};
		service.initialBetDataFactory = (): IInstantBetDataItem => {
			const group: Array<IInstantBetDataModel> = [];
			group[LotteriesGroupCode.EInstant] = {
				gameIndex: 0,
				drawIndex: 0,
				betsCount: 1
			};
			group[LotteriesGroupCode.VBL] = {
				gameIndex: 0,
				drawIndex: 0,
				betsCount: 1
			};

			return {
				priceFilter: 0,
				drawCount: 1,
				group
			};
		};
		service.initBetData(BetDataSource.Manual);
		service.appStoreService.Draws = {
			getDraws: (): Array<UpdateDrawInfoDraws> | undefined => [...DRAWS_FOR_INSTANT.lottery.draws],
			getLottery: () => ({...DRAWS_FOR_INSTANT.lottery})
		};
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(
			DRAWS_FOR_INSTANT.lottery.lott_code,
			new Map<string, string>([[EsapActions.EInstantRegBet, 'url'], [EsapActions.GetReportData, 'url']]
			));
	});

	it('should be created', async () => {
		await expect(service).toBeTruthy();
	});

	it('test setGroupId method', () => {

		service.setGroupId(LotteriesGroupCode.Regular);
		expect(service.groupId).toBeUndefined();

		service.setGroupId(LotteriesGroupCode.VBL);
		expect(service.groupId).toEqual(LotteriesGroupCode.VBL);

		service.setGroupId(LotteriesGroupCode.EInstant);
		expect(service.groupId).toEqual(LotteriesGroupCode.EInstant);

	});

	it('test selectGame method', async () => {

		service.lotteries = [{
			code: DRAWS_FOR_INSTANT.lott_code,
			name: DRAWS_FOR_INSTANT.lott_extra,
			price: 5,
			draws: [...DRAWS_FOR_INSTANT.lottery.draws],
			gameListIndex: 0
		}];

		service.groupId = LotteriesGroupCode.EInstant;
		service.selectGame(0);
		await expect(service.currentGameCode).toEqual(DRAWS_FOR_INSTANT.lott_code);

	});

	it('test selectDraw method', () => {
		service.groupId = LotteriesGroupCode.EInstant;
		const draw = new UpdateDrawInfoDraws();
		draw.draw = {...DRAWS_FOR_INSTANT.lottery.draws[0]};
		service.selectDraw(draw);
		expect(service.gameDraw).toEqual(draw.draw);
	});

	it('test createFilterByLotteries method', async () => {
		service.lotteries = [{
			code: DRAWS_FOR_INSTANT.lott_code,
			name: DRAWS_FOR_INSTANT.lott_extra,
			price: 5,
			draws: [...DRAWS_FOR_INSTANT.lottery.draws],
			gameListIndex: 0
		}];

		service.createFilterByLotteries();
		await expect(service.filtersValues).toEqual([0, 5]);
	});

	it('test refreshBets method', () => {
		service.groupId = LotteriesGroupCode.EInstant;
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.gameDraws = [undefined];
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.gameDraws = [{}];
		service.refreshBets();
		expect(service.amount).toEqual(0);

		service.gameDraws = [...DRAWS_FOR_INSTANT.lottery.draws];
		service.refreshBets();
		expect(service.amount).toEqual(5);
	});


	it('test buyLottery method', async () => {
		service.setGroupId(LotteriesGroupCode.EInstant);
		service.lotteries = [{
			code: DRAWS_FOR_INSTANT.lott_code,
			name: DRAWS_FOR_INSTANT.lott_extra,
			price: 5,
			draws: [...DRAWS_FOR_INSTANT.lottery.draws],
			gameListIndex: 0
		}];
		service.appStoreService.Settings.getEsapActionUrl = () => {
			// поиск по коду лотереи
			return {action: EsapActions.EInstantRegBet, url: 'url'};

		};
		service.dialogInfoService = {
			showOneButtonInfo: (): IDialog => {return undefined}
		};
		service.printService.isReady = () => true;
		service.transactionService.executeRequest = (): Promise<void> => new Promise(resolve => resolve());
		spyOn(service, 'buyLottery').and.callThrough();
		service.buyLottery();
		await expect(service.buyLottery).toHaveBeenCalled();

		service.printService.isReady = () => false;
		service.buyLottery();
		await expect(service.buyLottery).toHaveBeenCalled();

		service.printService.isReady = () => true;
		service.transactionService.executeRequest = (): Promise<void> => {
			return new Promise((resolve, reject) => reject({code: 12345, message: 'Test error'}));
		};
		service.buyLottery();
		await expect(service.buyLottery).toHaveBeenCalled();

	});

});
