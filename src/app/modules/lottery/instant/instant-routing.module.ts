import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { URL_INIT, URL_REGISTRY, URL_RESULTS } from '@app/util/route-utils';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { InstantLotteriesContentComponent } from '@app/instant/components/instant-lotteries-content/instant-lotteries-content.component';
import { InstantLotteriesInitComponent } from '@app/instant/components/instant-lotteries-init/instant-lotteries-init.component';
import { InstantResultsComponent } from '@app/instant/components/instant-results/instant-results.component';
import { InstantRegistryResolver } from '@app/instant/instant-registry-resolver';
import { InstantInitResolver } from '@app/instant/instant-init-resolver';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Стирачки и Золотой триумф".
 */
const routes: Routes = [
	{
		path: ``,
		component: InstantLotteriesContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: `${URL_INIT}/:id`,
				component: InstantLotteriesInitComponent,
				resolve: {
					init: InstantInitResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: InstantRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: `${URL_RESULTS}/:id`,
				component: InstantResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}
];

/**
 * Список маршрутов для модуля игры "Стирачки и Золотой триумф".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		InstantInitResolver,
		InstantRegistryResolver
	]
})
export class InstantRoutingModule {}
