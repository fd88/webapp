import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';

/**
 * Резолвер данных для отображения информации о ручной ставке в ЭМЛ.
 */
@Injectable()
export class InstantInitResolver implements Resolve<IActionsButton> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {InstantLotteryService} instantLotteryService Сервис игр "ЭМЛ и Золотой триумф".
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 */
	constructor(
		private readonly instantLotteryService: InstantLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IActionsButton {
		// проверить наличие акционных параметров
		const parsedUrl = this.router.parseUrl(state.url);
		let result: IActionsButton;
		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
			result = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionsButton;
		}
		if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			result = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET)) as IActionsButton;
		}

		return result;
	}

}
