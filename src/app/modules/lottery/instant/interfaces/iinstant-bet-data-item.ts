import { IBetDataItem } from '@app/core/game/base-game.service';

/**
 * Интерфейс модели данных для игр "ЭМЛ".
 */
export interface IInstantBetDataModel {

	/**
	 * Текущий индекс выбранной лотереи из списка {@link lotteries}.
	 */
	gameIndex: number;

	/**
	 * Текущий индекс выбранного тиража из списка {@link lotteries}.
	 */
	drawIndex: number;

	/**
	 * Количество билетов.
	 */
	betsCount: number;

}

/**
 * Модель ставки для игры "ЭМЛ".
 */
export interface IInstantBetDataItem extends IBetDataItem {

	/**
	 * Массив моделей по группам.
	 */
	group: Array<IInstantBetDataModel>;

	/**
	 * Фильтр по цене серии.
	 */
	priceFilter: number;

}
