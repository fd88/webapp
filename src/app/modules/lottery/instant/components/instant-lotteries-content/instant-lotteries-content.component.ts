import { Component, OnDestroy } from '@angular/core';

import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';

/**
 * Корневой компонент модуля игр "ЭМЛ и Золотой триумф".
 * Относительно данного компонента будет осуществляться дальнейшая навигация по под-компонентам игры.
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class InstantLotteriesContentComponent implements  OnDestroy {

	/**
	 * Конструктор компонента.
	 *
	 * @param {InstantLotteryService} instantLotteryService Сервис игр "ЭМЛ и Золотой триумф".
	 */
	constructor(
		private readonly instantLotteryService: InstantLotteryService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Вызывается при уничтожении компонента.
	 */
	ngOnDestroy(): void {
		this.instantLotteryService.resetBetData();
	}
}
