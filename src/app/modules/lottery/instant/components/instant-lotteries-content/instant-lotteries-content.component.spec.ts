import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InstantLotteriesContentComponent } from '@app/instant/components/instant-lotteries-content/instant-lotteries-content.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';


import { HttpService } from '@app/core/net/http/services/http.service';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('InstantLotteriesContentComponent', () => {
	let component: InstantLotteriesContentComponent;
	let fixture: ComponentFixture<InstantLotteriesContentComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			declarations: [
				InstantLotteriesContentComponent
			],
			providers: [
				LogService,
				DeclineWordPipe,
				InstantLotteryService,


				LotteriesService,
				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InstantLotteriesContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
