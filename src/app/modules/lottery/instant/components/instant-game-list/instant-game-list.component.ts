import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IInstantGameData, InstantLotteryService } from '@app/instant/services/instant-lottery.service';

/**
 * Компонент, отображающий список моментальных игр в ЭМЛ
 */
@Component({
	selector: 'app-instant-game-list',
	templateUrl: './instant-game-list.component.html',
	styleUrls: ['./instant-game-list.component.scss']
})
export class InstantGameListComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Сеттер фильтра по цене
	 * @param value Цена
	 */
	@Input()
	set filter(value: number) {
		this._filter = value;
		this.filterByPrice();
	}

	/**
	 * Геттер фильтра по цене
	 */
	get filter(): number {
		return this._filter;
	}

	// -----------------------------
	//  Output properties
	// -----------------------------
	/**
	 * Событие изменения кода игры (выбора другой игры)
	 */
	@Output()
	readonly gameCodeChanged = new EventEmitter<number>();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Отфильтрованный список игр
	 */
	filteredGames: Array<IInstantGameData> = [];

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Фильтр по цене
	 * @private
	 */
	private _filter = 0;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {InstantLotteryService} instantLotteryService Сервис электронных моментальных лотерей
	 */
	constructor(
		readonly instantLotteryService: InstantLotteryService
	) {}

	/**
	 * Обработчик выбора лотереи (игры).
	 *
	 * @param gc Код игры
	 */
	onClickLotteryHandler(gc: number): void {
		this.gameCodeChanged.emit(gc);
	}

	/**
	 * Функция для отслеживания изменений в массиве игр
	 * @param index Индекс элемента
	 * @param item Элемент
	 */
	trackByGames = (index, item: IInstantGameData) => item.code;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Функция фильтрации игр по цене
	 * @private
	 */
	private filterByPrice(): void {
		this.filteredGames = this._filter === 0
			? this.instantLotteryService.lotteries
			: this.instantLotteryService.lotteries.filter((elem: IInstantGameData) => {
				for (const draw of elem.draws) {
					if (Math.round(+draw.draw.bet_sum) === Math.round(this._filter)) {
						return true;
					}
				}

				return false;
			});
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.filterByPrice();
	}
}
