import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InstantGameListComponent } from './instant-game-list.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { SharedModule } from '@app/shared/shared.module';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import {DRAWS_FOR_INSTANT} from "../../../../features/mocks/instant-draws";

describe('InstantGameListComponent', () => {
	let component: InstantGameListComponent | any;
	let fixture: ComponentFixture<InstantGameListComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			declarations: [
				InstantGameListComponent
			],
			providers: [
				LogService,
				InstantLotteryService,
				HttpService,

				LotteriesService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InstantGameListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();

		component.instantLotteryService.lotteries = [{
			code: DRAWS_FOR_INSTANT.lott_code,
			name: DRAWS_FOR_INSTANT.lott_extra,
			price: '5',
			draws: [...DRAWS_FOR_INSTANT.lottery.draws],
			gameListIndex: 0
		}];

	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test filter property', () => {
		component.filter = 50;
		expect(component.filter).toEqual(50);
	});

	it('test onClickLotteryHandler', () => {
		component.filter = 5;
		expect(component.filter).toEqual(5);
	});

	it('test trackByGames', () => {
		expect(component.trackByGames(1, {code: '123'})).toEqual('123');
	});

	it('test filterByPrice method', () => {
		component.filter = 5;
		spyOn(component, 'filterByPrice').and.callThrough();
		component.filterByPrice();
		expect(component.filterByPrice).toHaveBeenCalled();
	});

	it('test onClickLotteryHandler', () => {
		spyOn(component, 'onClickLotteryHandler').and.callThrough();
		component.onClickLotteryHandler({target: {
			getAttribute: () => 150
		}});
		expect(component.onClickLotteryHandler).toHaveBeenCalled();

		component.onClickLotteryHandler({target: {
				getAttribute: () => undefined
			}});
		expect(component.onClickLotteryHandler).toHaveBeenCalled();
	});

});
