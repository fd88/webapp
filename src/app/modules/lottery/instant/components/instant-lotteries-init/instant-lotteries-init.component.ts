import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { MonoTypeOperatorFunction, Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { LotteriesGroupCode } from '@app/core/configuration/lotteries';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { BetDataSource } from '@app/core/game/base-game.service';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { IInstantBetDataModel } from '@app/instant/interfaces/iinstant-bet-data-item';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import {DOCUMENT} from "@angular/common";

/**
 * Компонент ввода для моментальных лотерей.
 */
@Component({
	selector: 'app-instant-lotteries-init',
	templateUrl: './instant-lotteries-init.component.html',
	styleUrls: ['./instant-lotteries-init.component.scss']
})
export class InstantLotteriesInitComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на группу кнопок фильтров.
	 */
	@ViewChild('filtersComponent', { static: true })
	filtersComponent: ButtonsGroupComponent;

	/**
	 * Ссылка на группу кнопок количества тиражей.
	 */
	@ViewChild('ticketsComponent', { static: true })
	ticketsComponent: ButtonsGroupComponent;

	/**
	 * Перечисление стилей кнопок, доступных к использованию
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Перечисление режимов кнопок, доступных к использованию
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Перечисление источников данных для ввода ставок
	 */
	readonly BetDataSource = BetDataSource;

	/**
	 * Список групп электронных моментальных лотерей
	 */
	readonly LotteriesGroupCode = LotteriesGroupCode;

	/**
	 * Есть ли прокрутка в списке ставок
	 */
	isScrolledBets = false;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Акционные данные по игре
	 * @private
	 */
	private actionData: IActionsButton;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {InstantLotteryService} instantLotteryService Сервис для работы с электронными моментальными лотереями.
	 * @param {ActivatedRoute} route Текущий активный маршрут.
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 * @param {DialogContainerService} dialogInfoService Сервис для работы с диалоговыми окнами.
	 * @param doc Ссылка на DOM-документ
	 */
	constructor(
		readonly instantLotteryService: InstantLotteryService,
		private readonly route: ActivatedRoute,
		private readonly translateService: TranslateService,
		private readonly router: Router,
		private readonly dialogInfoService: DialogContainerService,
		@Inject(DOCUMENT) private readonly doc: Document
	) {}

	/**
	 * Обработчик выбора моментальной игры.
	 *
	 * @param {number} newGameIndex Новый индекс моментальной игры.
	 */
	onGameCodeChangedHandler(newGameIndex: number): void {
		if (this.instantLotteryService.betData.group[this.instantLotteryService.groupId].gameIndex === newGameIndex) {
			Logger.Log.i('InstantLotteriesService', 'Deselected game %s', this.instantLotteryService.currentGameCode)
				.console();
			this.instantLotteryService.betData.group[this.instantLotteryService.groupId].gameIndex = NaN;
			this.instantLotteryService.currentGameCode = null;
			this.instantLotteryService.gameName = null;
			this.instantLotteryService.gameDraws = [];
		} else {
			this.instantLotteryService.selectGame(newGameIndex);
			this.isScrolledBets = (this.instantLotteryService.gameDraws.length > 3);
			this.doc.getElementById(`game_index_${newGameIndex}`).scrollIntoView({behavior: 'smooth'});
			// при ручном клике на игру удалить параметры
			if (this.router.url.includes('?')) {
				this.router.navigate([this.router.url.split('?')[0]])
					.catch();
			}
		}

		this.instantLotteryService.betData.group[this.instantLotteryService.groupId].betsCount = 0;
		this.ticketsComponent.selectedButtons = null;
		this.ticketsComponent.buttons = this.ticketsComponent.buttons.map(elem => ({...elem, selected: false}));
		this.instantLotteryService.refreshBets();
	}

	/**
	 * Обработчик выбора тиража по выбранной лотерее (игре).
	 *
	 * @param event Событие клика по тиражу.
	 */
	onClickDrawHandler(event: MouseEvent): void {
		const liElem = (event.target as HTMLElement).closest('.draws-item');
		if (liElem) {
			const index = (liElem as HTMLLIElement).value;
			if (!isNaN(index)) {
				const draw = this.instantLotteryService.gameDraws[index];
				this.instantLotteryService.selectDraw(draw);
				this.instantLotteryService.refreshBets();
			}
		}
	}

	/**
	 * Обработчик выбора количества билетов.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectedTicketHandler(button: IButtonGroupItem): void {
		const grId = this.instantLotteryService.groupId;
		this.instantLotteryService.betData.group[grId].betsCount = button ? parseInt(button.label, 10) : 0;
		this.instantLotteryService.refreshBets();
	}

	/**
	 * Обработчик выбора фильтра по цене.
	 * @param button Выбранная кнопка.
	 */
	onSelectedFilterHandler(button: IButtonGroupItem): void {
		this.instantLotteryService.betData.priceFilter = this.instantLotteryService.filtersValues[button.index];
	}

	/**
	 * Функция для отслеживания изменений в массиве тиражей
	 * @param index Индекс элемента
	 * @param item Элемент массива
	 */
	trackByDraws = (index, item: UpdateDrawInfoDraws) => item.draw.code;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Начальная инициализация компонента.
	 */
	private instantInit(): void {
		this.route.data
			.pipe(
				takeUntil(this.unsubscribe$$),
				this.updateDataWhenRouteChanges()
			)
			.subscribe();

		this.instantLotteryService.initBetData(BetDataSource.Manual);
		this.instantLotteryService.setGroupId(Number.parseInt(this.route.snapshot.paramMap.get('id'), 10));
		this.instantLotteryService.createFilterByLotteries();

		const groupData: IInstantBetDataModel = {...this.instantLotteryService.betData.group[this.instantLotteryService.groupId]};
		if (isNaN(groupData.gameIndex)) {
			this.instantLotteryService.gameDraws = [];
		} else {
			// восстановить игру
			this.instantLotteryService.selectGame(groupData.gameIndex);

			// восстановить серию
			if (!isNaN(groupData.drawIndex)) {
				this.instantLotteryService.selectDraw(groupData.drawIndex);
			}
		}

		// выделить игру, если имеются аккционные данные
		if (!!this.actionData) {
			const g = this.instantLotteryService.lotteries.find(f => f.code === this.actionData.gameCode);
			if (g) {
				this.instantLotteryService.selectGame(g.gameListIndex);
				this.instantLotteryService.refreshBets();
			}
		}

		// восстановить кол-во билетов
		this.ticketsComponent.selectedButtons = isNaN(groupData.betsCount) ? undefined : groupData.betsCount - 1;

		// восстановить фильтр
		const idx = this.instantLotteryService.filtersValues.indexOf(this.instantLotteryService.betData.priceFilter);
		this.filtersComponent.selectedButtons = [idx];
		this.isScrolledBets = this.instantLotteryService.gameDraws.length > 5;
	}

	/**
	 * Обновление данных при изменении маршрута.
	 * @private
	 */
	private updateDataWhenRouteChanges(): MonoTypeOperatorFunction<Data> {
		return (source: Observable<Data>) => source.pipe(
			tap(data => {
				if (!!data && !!data.init) {
					this.actionData = data.init;
				}
			})
		);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.instantInit();
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		// this.instantLotteryService.initBetData(BetDataSource.Manual);
		this.instantLotteryService.gameName = '';
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}
}
