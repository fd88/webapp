import {ComponentFixture, TestBed} from '@angular/core/testing';

import {InstantLotteriesInitComponent} from './instant-lotteries-init.component';
import {RouterTestingModule} from '@angular/router/testing';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {HttpService} from '@app/core/net/http/services/http.service';

import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {IDialog} from '@app/core/dialog/types';
import {InstantLotteryService} from '@app/instant/services/instant-lottery.service';
import {InstantGameListComponent} from '@app/instant/components/instant-game-list/instant-game-list.component';
import {BetDataSource} from '@app/core/game/base-game.service';
import {Lotteries, LotteriesGroupCode} from "@app/core/configuration/lotteries";
import {LotteriesService} from "@app/core/services/lotteries.service";
import {DRAWS_FOR_INSTANT} from "../../../../features/mocks/instant-draws";
import {ROUTES} from "../../../../../app-routing.module";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {ActivatedRoute} from "@angular/router";
import {of, timer} from "rxjs";

class ActivatedRouteStub {
	snapshot = {
		paramMap: new Map([['id', '1']])
	};
	data = of({
		init: {
			labelKey: '2 шт, “Товстий гаманець”',
			gameCode: 124,
			groupCode: 1,
			data: {
				summ: 20,
				betCount: 2
			}
		}
	});
}

describe('InstantLotteriesInitComponent', () => {
	let component: InstantLotteriesInitComponent | any;
	let fixture: ComponentFixture<InstantLotteriesInitComponent>;
	let appStoreService: AppStoreService;
	let instantLotteryService: InstantLotteryService;
	let route: ActivatedRoute;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes(ROUTES),
				SharedModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				}),
				HttpClientModule
			],
			declarations: [
				InstantLotteriesInitComponent,
				InstantGameListComponent
			],
			providers: [
				LogService,
				InstantLotteryService,
				LotteriesService,
				Lotteries,
				AppStoreService,
				HttpService,
				{
					provide: DialogContainerService,
					useValue: {
						showOneButtonInfo: (): IDialog => {
							return;
						}
					}
				},
				{
					provide: ActivatedRoute,
					useClass: ActivatedRouteStub,
				},
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		route = TestBed.inject(ActivatedRoute);
		appStoreService = TestBed.inject(AppStoreService);

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);

		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(107, DRAWS_FOR_INSTANT.lottery);
		appStoreService.Draws.setLottery(124, { ...DRAWS_FOR_INSTANT.lottery, lott_code: 124});
		instantLotteryService = TestBed.inject(InstantLotteryService);
		instantLotteryService.initBetData(BetDataSource.Manual);

		await timer(200).toPromise();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(InstantLotteriesInitComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onGameCodeChangedHandler', () => {
		component.onGameCodeChangedHandler(0);
		expect(instantLotteryService.amount).toEqual(0);
	});

	it('test onGameCodeChangedHandler 2', () => {
		instantLotteryService.betData.group[LotteriesGroupCode.EInstant].gameIndex = 0;
		component.onGameCodeChangedHandler(0);
		expect(instantLotteryService.gameDraws.length).toEqual(0);
	});

	it('test onClickDrawHandler', async () => {
		const ev = new MouseEvent('onClickDrawHandler');
		const target = document.createElement('li');
		target.className = 'draws-item';
		target.setAttribute('value', '1');
		spyOn(component, 'onClickDrawHandler').and.callThrough();
		component.onClickDrawHandler({...ev, target});
		expect(component.onClickDrawHandler).toHaveBeenCalled();
	});

	it('test onSelectedTicketHandler', async () => {
		component.onSelectedTicketHandler({
			index: 2,
			label: '3',
			selected: true,
			disabled: false
		});
		expect(instantLotteryService.betData.group[instantLotteryService.groupId].betsCount).toEqual(3);
	});

	it('test onSelectedFilterHandler',async () => {
		component.ngOnInit();
		await timer(400).toPromise();
		instantLotteryService.filtersValues = [0, 50];
		component.onSelectedFilterHandler({
			index: 1,
			label: '50',
			selected: true,
			disabled: false
		});

		expect(instantLotteryService.betData.priceFilter).toEqual(50);
	});

	it('test trackByDraws method', () => {
		expect(component.trackByDraws(1, DRAWS_FOR_INSTANT.lottery.draws[0]))
			.toEqual(DRAWS_FOR_INSTANT.lottery.draws[0].draw.code);
	});

	it('test instantInit method', () => {
		spyOn(component, 'instantInit').and.callThrough();
		instantLotteryService.betData.group[instantLotteryService.groupId].gameIndex = 1;
		component.instantInit();
		expect(component.instantInit).toHaveBeenCalled();
	});

});
