import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { BetDataSource, GameSaleMode } from '@app/core/game/base-game.service';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';

/**
 * Резолвер данных для отображения информации о ручной ставке в ЭМЛ.
 */
@Injectable()
export class InstantRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {InstantLotteryService} instantLotteryService Сервис игр "ЭМЛ и Золотой триумф".
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 */
	constructor(
		private readonly instantLotteryService: InstantLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		// проверить наличие акционных параметров
		const parsedUrl = this.router.parseUrl(state.url);

		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
			const actionBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionsButton;
			this.instantLotteryService.groupId = Number.isInteger(actionBet.groupCode) ? actionBet.groupCode : 1;
			this.instantLotteryService.initBetData(BetDataSource.Manual);
			this.instantLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			this.instantLotteryService.createFilterByLotteries();
			this.instantLotteryService.setGroupId(this.instantLotteryService.groupId);

			const game = this.instantLotteryService.lotteries.find(f => f.code === actionBet.gameCode);
			this.instantLotteryService.selectGame(this.instantLotteryService.lotteries.indexOf(game));

			const draw = this.instantLotteryService.gameDraws.find(f => +f.draw.bet_sum === actionBet.data.summ / actionBet.data.betCount);
			this.instantLotteryService.selectDraw(draw);

			this.instantLotteryService.betData.group[this.instantLotteryService.groupId].betsCount = actionBet.data.betCount;
			this.instantLotteryService.refreshBets();
		}

		if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			const demoBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET));
			this.instantLotteryService.groupId = Number.isInteger(demoBet.groupCode) ? demoBet.groupCode : 1;
			this.instantLotteryService.initBetData(BetDataSource.Manual);
			this.instantLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			this.instantLotteryService.createFilterByLotteries();
			this.instantLotteryService.setGroupId(this.instantLotteryService.groupId);

			const game = this.instantLotteryService.lotteries.find(f => f.code === demoBet.gameCode);
			this.instantLotteryService.selectGame(this.instantLotteryService.lotteries.indexOf(game));

			const draw = this.instantLotteryService.gameDraws.find(f => +f.draw.bet_sum === demoBet.data.summ / demoBet.data.betCount);
			this.instantLotteryService.selectDraw(draw);

			this.instantLotteryService.betData.group[this.instantLotteryService.groupId].betsCount = demoBet.data.betCount;
			this.instantLotteryService.refreshBets();
		}

		if (!this.instantLotteryService.gameDrawSelected || !this.instantLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.instantLotteryService
			};
		}

		const checkInfoRowList = [
			{
				langKeyName: 'lottery.lottery',
				value: `${this.instantLotteryService.gameName}`
			},
			{
				langKeyName: 'lottery.instant.series',
				value: `${this.instantLotteryService.gameDrawSelected}`
			},
			{
				langKeyName: 'lottery.tickets_count',
				value: `${this.instantLotteryService.betData.group[this.instantLotteryService.groupId].betsCount}`
			},
			{
				langKeyName: 'lottery.sum',
				value: this.instantLotteryService.formattedAmount,
				redColor: true,
				valueIsCurrency: true
			}
		];

		return {
			checkInfoRowList,
			baseGameService: this.instantLotteryService
		};
	}

}
