import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { KareRoutingModule } from '@app/kare/kare-routing.module';

import { KareLotteryService } from '@app/kare/services/kare-lottery.service';

import { KareLotteryContentComponent } from '@app/kare/components/kare-lottery-content/kare-lottery-content.component';
import { KareResultsComponent } from '@app/kare/components/kare-results/kare-results.component';
import { KareLotteryInitComponent } from '@app/kare/components/kare-lottery-init/kare-lottery-init.component';
import { KareEditBetDialogComponent } from '@app/kare/components/kare-edit-bet-dialog/kare-edit-bet-dialog.component';
import { KareBetTypeOneComponent } from '@app/kare/components/kare-bet-type-one/kare-bet-type-one.component';
import { KareBetTypeTwoComponent } from '@app/kare/components/kare-bet-type-two/kare-bet-type-two.component';
import { KareBetsListComponent } from '@app/kare/components/kare-bets-list/kare-bets-list.component';
import { KareBetListItemOneComponent } from '@app/kare/components/kare-bet-list-item-one/kare-bet-list-item-one.component';
import { KareBetListItemTwoComponent } from '@app/kare/components/kare-bet-list-item-two/kare-bet-list-item-two.component';
import { KareSelectedCardsOneComponent } from '@app/kare/components/kare-selected-cards-one/kare-selected-cards-one.component';
import { KareSelectedCardsTwoComponent } from '@app/kare/components/kare-selected-cards-two/kare-selected-cards-two.component';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';

/**
 * Модуль лотереи "Каре".
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		KareRoutingModule
	],
	declarations: [
		KareLotteryContentComponent,
		KareLotteryInitComponent,
		KareResultsComponent,
		KareEditBetDialogComponent,
		KareBetTypeOneComponent,
		KareBetTypeTwoComponent,
		KareBetsListComponent,
		KareBetListItemOneComponent,
		KareBetListItemTwoComponent,
		KareSelectedCardsOneComponent,
		KareSelectedCardsTwoComponent,
		KareCardWithLabelComponent
	],
	providers: [
		KareLotteryService
	],
	entryComponents: [
		KareEditBetDialogComponent
	]
})
export class KareModule {}
