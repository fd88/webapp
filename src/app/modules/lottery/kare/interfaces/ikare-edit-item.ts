import { KareGameType, KarePokerCombinations } from '@app/core/net/http/api/models/kare-reg-bet';

/**
 * Модель ставки, редактируемой или создаваемой внутри диалога {@link KareEditBetDialogComponent}.
 */
export interface IKareEditItem {

	/**
	 * Идентификатор модели (индекс в массиве), задаваемый при создании.
	 */
	id?: number;

	/**
	 * Тип игры.
	 */
	gameType: KareGameType;

	/**
	 * Список покерных комбинаций или список карт.
	 */
	cards: Array<number | KarePokerCombinations>;

	/**
	 * Сумма выбранной ставки в копейках.
	 */
	betSum: number;

	/**
	 * Сумма выбранной ставки в гривнах, для отображения на формах.
	 */
	betSumLabel: string;

}
