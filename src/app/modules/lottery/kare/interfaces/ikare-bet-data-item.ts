import { IBetDataItem } from '@app/core/game/base-game.service';
import { IKareBetsData } from '@app/core/net/http/api/models/kare-reg-bet';

/**
 * Модель данных в игре "Каре".
 * Хранит основные параметры типа: ставки по игре, количество тиражей и т.д.
 */
export interface IKareBetDataItem extends IBetDataItem {

	/**
	 * Данные по ставке, передаваемые в запросе.
	 */
	kareBetsData: IKareBetsData;

}
