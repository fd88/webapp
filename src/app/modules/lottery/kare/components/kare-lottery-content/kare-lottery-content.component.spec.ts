import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { KareLotteryContentComponent } from '@app/kare/components/kare-lottery-content/kare-lottery-content.component';
import { KareLotteryService } from '@app/kare/services/kare-lottery.service';

import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';
import { HttpService } from '@app/core/net/http/services/http.service';

import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('KareLotteryContentComponent', () => {
	let component: KareLotteryContentComponent;
	let fixture: ComponentFixture<KareLotteryContentComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([
					// {
					// 	path: 'login',
					// 	component: MockLoginComponent
					// }
				]),
			],
			declarations: [
				KareLotteryContentComponent
			],
			providers: [
				DeclineWordPipe,
				KareLotteryService,
				HttpService,
				LotteriesService,
				LogService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareLotteryContentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
