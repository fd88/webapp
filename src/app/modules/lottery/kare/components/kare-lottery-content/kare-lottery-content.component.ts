import { Component, OnDestroy } from '@angular/core';
import { KareLotteryService } from '@app/kare/services/kare-lottery.service';

/**
 * Корневой компонент модуля игры "Каре".
 * Относительно данного компонента будет осуществляться дальнейшая навигация по под-компонентам игры.
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class KareLotteryContentComponent implements OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {KareLotteryService} kareLotteryService Сервис игры "Каре".
	 */
	constructor(
		private readonly kareLotteryService: KareLotteryService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Вызывается при уничтожении компонента.
	 */
	ngOnDestroy(): void {
		this.kareLotteryService.resetBetData();
	}

}
