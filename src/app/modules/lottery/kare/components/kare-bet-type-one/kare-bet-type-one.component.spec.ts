import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { KareBetTypeOneComponent } from './kare-bet-type-one.component';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { KareCards, KareCardsSuit } from '@app/core/net/http/api/models/kare-reg-bet';

describe('KareBetTypeOneComponent', () => {
	let component: KareBetTypeOneComponent | any;
	let fixture: ComponentFixture<KareBetTypeOneComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
				KareBetTypeOneComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareBetTypeOneComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('check onSelectButtonHandler function #1', () => {
		spyOn(component.entitiesListChanged, 'emit');
		component.entities = [];
		component.onSelectButtonHandler(KareCards.Ace, KareCardsSuit.Clubs);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([39]);
	});

	it('check onSelectButtonHandler function #2', () => {
		spyOn(component.entitiesListChanged, 'emit');
		component.entities = [];
		component.onSelectButtonHandler(KareCards.King, KareCardsSuit.Diamonds);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([38]);
	});

	it('check onSelectButtonHandler function #3', () => {
		spyOn(component.entitiesListChanged, 'emit');
		component.entities = [39];
		component.onSelectButtonHandler(KareCards.Ace, KareCardsSuit.Clubs);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([]);
	});
});
