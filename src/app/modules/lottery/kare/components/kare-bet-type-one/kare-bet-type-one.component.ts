import { ChangeDetectionStrategy, Component } from '@angular/core';
import { KareCards, KareCardsSuit } from '@app/core/net/http/api/models/kare-reg-bet';
import { CARDS_CODES_TABLE, KARE_CARDS_SUIT_VALUES, KARE_CARDS_VALUES } from '@app/kare/services/kare-lottery.service';
import { IKareEntityItem, KareBaseBetType } from '@app/kare/components/kare-base-bet-type';

/**
 * Список названий для карт.
 */
const CARD_LABELS = ['lottery.kare.cards', ...CARDS_CODES_TABLE.slice(1), CARDS_CODES_TABLE[0]];

/**
 * Массив всех карт.
 */
const ALL_CARDS = Array.from(Array(KARE_CARDS_VALUES.length * KARE_CARDS_SUIT_VALUES.length));

/**
 * Компонент для создания ставки по {@link KareGameType.Table1_Cards первому типу} игры "Каре".
 */
@Component({
	selector: 'app-kare-bet-type-one',
	templateUrl: './kare-bet-type-one.component.html',
	styleUrls: ['./kare-bet-type-one.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareBetTypeOneComponent extends KareBaseBetType  {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Массив карт в порядке выбора
	 */
	ordering = [];

	/**
	 * Массив меток для первого столбца.
	 */
	readonly CARD_LABELS = CARD_LABELS;

	/**
	 * Массив значений из списка карт {@link KareCards}.
	 */
	readonly KARE_CARDS_VALUES = KARE_CARDS_VALUES;

	/**
	 * Массив значений из списка мастей карт {@link KareCardsSuit}.
	 */
	readonly KARE_CARDS_SUIT_VALUES = KARE_CARDS_SUIT_VALUES;

	/**
	 * Фабрика для создания элементов списка.
	 * @param entityList Список выбранных карт.
	 */
	readonly entityItemFactory = (entityList: Array<number>): Array<IKareEntityItem> => {
		this.ordering = [...entityList];
		const result: Array<IKareEntityItem> = ALL_CARDS;
		KARE_CARDS_SUIT_VALUES
			.forEach(suit => {
				KARE_CARDS_VALUES
					.forEach(card => {
						const idx = suit * KARE_CARDS_VALUES.length + card;
						result[idx] = {
							entityCode: idx,
							selected: entityList.includes(idx)
						};
					});
			});

		return result;
	};

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Обработчик события выбора карты.
	 *
	 * @param {KareCards} card Значение карты.
	 * @param {KareCardsSuit} cardSuite Значение масти карты.
	 */
	onSelectButtonHandler(card: KareCards, cardSuite: KareCardsSuit): void {
		const idx = cardSuite * this.KARE_CARDS_VALUES.length + card;
		this.parseIndex(idx);
	}

	/**
	 * Обработчик клика по элементу из списка.
	 *
	 * @param {number} index Индекс элемента, по которому совершен клик, в списке {@link entityItemsList}
	 */
	parseIndex(index: number): void {
		this.entityItemsList[index].selected = !this.entityItemsList[index].selected;
		const newArr = this.entityItemsList
			.filter(f => f.selected)
			.map(m => m.entityCode);
		this.countSelected = newArr.length;

		if (this.entityItemsList[index].selected) {
			this.ordering.push(this.entityItemsList[index].entityCode);
		} else {
			const ecIndex = this.ordering.findIndex(elem => elem === this.entityItemsList[index].entityCode);
			if (ecIndex > -1) {
				this.ordering.splice(ecIndex, 1);
			}
		}
		this.entitiesListChanged.emit(this.ordering);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
