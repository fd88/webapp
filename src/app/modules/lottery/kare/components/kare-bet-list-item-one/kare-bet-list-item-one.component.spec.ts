import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { KareBetListItemOneComponent } from './kare-bet-list-item-one.component';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { TranslateModule } from '@ngx-translate/core';
import { KareCards, KareCardsSuit, KareGameType, KareInputMode } from '@app/core/net/http/api/models/kare-reg-bet';

describe('KareBetListItemOneComponent', () => {
	let component: KareBetListItemOneComponent;
	let fixture: ComponentFixture<KareBetListItemOneComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				KareBetListItemOneComponent,
				KareCardWithLabelComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareBetListItemOneComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test trackByCards function', () => {
		const result = component.trackByCards(0, {
			card: KareCards.Ace, suit: KareCardsSuit.Spades, shortLabel: '1', label: '2', cardNumber: 0
		});
		expect(result).toEqual('020');
	});

	it('test input bet', () => {
		const expected = [
			{suit: KareCardsSuit.Spades, label: '', shortLabel: '', card: KareCards.Ace, cardNumber: 0},
			{suit: KareCardsSuit.Hearts, label: '', shortLabel: '', card: KareCards.Ace, cardNumber: 13},
			{suit: KareCardsSuit.Diamonds, label: '', shortLabel: '', card: KareCards.Ace, cardNumber: 26},
			{suit: KareCardsSuit.Clubs, label: '', shortLabel: '', card: KareCards.Ace, cardNumber: 39}
		];
		component.bet = {i: KareInputMode.Manual, d: 1, t: KareGameType.Table1_Cards, c: [0, 13, 26, 39], a: false, s: 5};
		component.cardDescriptionItemList.forEach((r, i) => {
			expect(r.suit).toEqual(expected[i].suit);
			expect(r.card).toEqual(expected[i].card);
			expect(r.cardNumber).toEqual(expected[i].cardNumber);
		});
	});
});
