import { Component, Input } from '@angular/core';
import { IKareBetItemGameType1 } from '@app/core/net/http/api/models/kare-reg-bet';
import { IKareCardDescriptionItem } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { convertCodeToCardItem } from '@app/kare/services/kare-lottery.service';

/**
 * Компонент, отображающий ставку по типу 1 в виде кнопки в списке ставок.
 */
@Component({
	selector: 'app-kare-bet-list-item-one',
	templateUrl: './kare-bet-list-item-one.component.html',
	styleUrls: ['./kare-bet-list-item-one.component.scss']
})
export class KareBetListItemOneComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Сеттер ставки типа 1
	 * @param value Ставка типа 1
	 */
	@Input()
	set bet(value: IKareBetItemGameType1) {
		this._bet = value;
		this.cardDescriptionItemList = value.c.map(m => {
			return convertCodeToCardItem(m);
		});
	}

	/**
	 * Геттер ставки типа 1
	 */
	get bet(): IKareBetItemGameType1 {
		return this._bet;
	}

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список карт для отображения
	 */
	cardDescriptionItemList: Array<IKareCardDescriptionItem>;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Ставка типа 1
	 * @private
	 */
	private _bet: IKareBetItemGameType1;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Функция для отслеживания изменений в массиве карт
	 * @param index Индекс элемента
	 * @param item Элемент
	 */
	trackByCards = (index, item: IKareCardDescriptionItem) => `${index}${item.label}${item.suit}`;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
}
