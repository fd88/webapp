import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { KareSelectedCardsTwoComponent } from '@app/kare/components/kare-selected-cards-two/kare-selected-cards-two.component';
import { TranslateModule } from '@ngx-translate/core';

xdescribe('KareSelectedCardsTwoComponent', () => {
	let component: KareSelectedCardsTwoComponent;
	let fixture: ComponentFixture<KareSelectedCardsTwoComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				KareSelectedCardsTwoComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareSelectedCardsTwoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test cards input', () => {
		component.cards = [];
		expect(component.combinationsCount).toEqual(0);
		expect(component.combinationsString).toEqual('');

		component.cards = [1,2,3];
		expect(component.combinationsCount).toEqual(3);
		expect(component.combinationsString).toEqual('Lottery.kare.royal_flush, lottery.kare.straight_flush, lottery.kare.square');
	});
});
