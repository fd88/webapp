import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { KarePokerCombinations } from '@app/core/net/http/api/models/kare-reg-bet';
import { POKER_CODES } from '@app/kare/services/kare-lottery.service';

/**
 * Компонент отображения информации о наполняемой ставке.
 */
@Component({
	selector: 'app-kare-selected-cards-two',
	templateUrl: './kare-selected-cards-two.component.html',
	styleUrls: ['./kare-selected-cards-two.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareSelectedCardsTwoComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Показывать ли заголовок.
	 */
	@Input() showTitle = true;

	/**
	 * Задать список выбранных пользователем комбинаций, которые необходимо отобразить.
	 *
	 * @param {Array<KarePokerCombinations>} value Список комбинаций.
	 */
	@Input()
	set cards(value: Array<KarePokerCombinations>) {
		this.combinationsCount = value.length;
		const valueForSort = [...value];
		valueForSort.sort((a, b) => a - b);
		this.combinationsString = valueForSort
			.map(m => this.translateService.instant(POKER_CODES[m - 1]))
			.join(', ')
			.toLowerCase();

		if (!!this.combinationsString) {
			this.combinationsString = this.combinationsString.substr(0, 1)
				.toUpperCase() + this.combinationsString.substr(1);
		}
	}

	/**
	 * Максимальное количество комбинаций.
	 */
	@Input()
	maxCardsCount = 10;

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Количество выбранных комбинаций.
	 */
	combinationsCount = 0;

	/**
	 * Строка с комбинациями через запятую и с большой буквы первая комбинация.
	 */
	combinationsString: string;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TranslateService} translateService Сервис для работы с переводами.
	 */
	constructor(
		private readonly translateService: TranslateService
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
