import { Component } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IDrawing, IDrawingResult } from '@app/core/net/http/api/models/get-draw-results';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { IAbstractViewResults, IResultPrintItem, PrintResultTypes } from '@app/core/services/results/results-utils';
import { IKareCardDescriptionItem } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { KareCards, KareCardsSuit } from '@app/core/net/http/api/models/kare-reg-bet';
import { convertCodeToCardItem } from '@app/kare/services/kare-lottery.service';

/**
 * Интерфейс для представления результатов по игре Каре.
 */
interface IKareResults extends IAbstractViewResults {
	/**
	 * Массив разыгранных тиражей.
	 */
	drawing: Array<IDrawing>;
	/**
	 * Массив с описанием карт.
	 */
	cards: Array<IKareCardDescriptionItem>;
	/**
	 * Массив с описанием покерных комбинаций.
	 */
	poker: Array<string>;
	/**
	 * Описание карт в виде строки.
	 */
	cardsReport: string;
	/**
	 * Описание покерных комбинаций в виде строки.
	 */
	pokerReport: string;
}

/**
 * Компонент отображения результатов по игре "Каре".
 */
@Component({
	selector: 'app-kare-results',
	templateUrl: './kare-results.component.html',
	styleUrls: ['./kare-results.component.scss']
})
export class KareResultsComponent  {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список возможных лотерей
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Данные для отображения результатов.
	 */
	viewData: IKareResults;

	/**
	 * Данные для печати результатов.
	 */
	printData: Array<IResultPrintItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {GameResultsService} gameResultsService Сервис для работы с результатами игр.
	 */
	constructor(
		readonly gameResultsService: GameResultsService
	) {}

	/**
	 * Обработчик нажатия выбора результатов по тиражу.
	 *
	 * @param {IDrawingResult} result Результаты по тиражу.
	 */
	onSelectedResultsHandler(result: IDrawingResult): void {
		if (!result) {
			this.viewData = undefined;
			this.printData = undefined;

			return;
		}

		const drawNumber = result.draw_name;
		const drawDate = result.drawing_date_begin;
		const extraInfo = result.drawing && result.drawing[0] ? result.drawing[0].extra_info : undefined;
		const drawing = result.drawing;
		let cards: Array<IKareCardDescriptionItem>;
		let poker: Array<string>;
		let cardsReport: string;
		let pokerReport: string;
		if (Array.isArray(result.drawing) && result.drawing.length > 0 && !!result.drawing[0].win_comb) {
			const arr = result.drawing[0].win_comb.split('\n');
			cardsReport = arr[0];
			cards = cardsReport
				.split(',')
				.map(this.cardDescriptionItemFactory.bind(this));

			if (!!arr[1]) {
				pokerReport = arr[1];
				poker = pokerReport.split(',');
			}
		}

		this.viewData = {drawNumber, drawDate, extraInfo, drawing, cards, poker, cardsReport, pokerReport};
		this.printData = this.printDataParser();
	}

	/**
	 * Функция для отслеживания изменений в массиве с описанием карт.
	 * @param index Индекс элемента в массиве.
	 * @param item Элемент массива.
	 */
	trackByKareDescriptionItemFn = (index, item: IKareCardDescriptionItem) => `${item.card}${item.suit}${index}`;

	/**
	 * Функция для отслеживания изменений в массиве с выигрышными комбинациями.
	 * @param index Индекс элемента в массиве.
	 * @param item Элемент массива.
	 */
	trackByWinCombFn = (index, item: string) => `${item}${index}`;

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Функция для парсинга строкового представления карты в объект карты.
	 * @param cardStr Строковое представление карты.
	 * @private
	 */
	private cardDescriptionItemFactory(cardStr: string): IKareCardDescriptionItem {
		const s = new Map<string, KareCardsSuit>([
			['П', KareCardsSuit.Spades],
			['Ч', KareCardsSuit.Hearts],
			['Б', KareCardsSuit.Diamonds],
			['Т', KareCardsSuit.Clubs]
		]);
		const c = new Map<string, KareCards>([
			['2', KareCards.Card_2],
			['3', KareCards.Card_3],
			['4', KareCards.Card_4],
			['5', KareCards.Card_5],
			['6', KareCards.Card_6],
			['7', KareCards.Card_7],
			['8', KareCards.Card_8],
			['9', KareCards.Card_9],
			['10', KareCards.Card_10],
			['В', KareCards.Jack],
			['Д', KareCards.Queen],
			['К', KareCards.King],
			['Т', KareCards.Ace]
		]);

		const suit = s.get(cardStr.charAt(cardStr.length - 1));
		const card = c.get(cardStr.substring(0, cardStr.length - 1));

		return convertCodeToCardItem(suit * 13 + card);
	}

	/**
	 * Функция-парсер данных для печати.
	 */
	private printDataParser(): Array<IResultPrintItem> {
		const result = [
			{key: PrintResultTypes.LotteryName, value: ['lottery.kare.game_name']},
			{key: PrintResultTypes.DrawNumber, value: [this.viewData.drawNumber]},
			{key: PrintResultTypes.DrawDate, value: [this.viewData.drawDate]}
		];

		result.push({key: PrintResultTypes.Line, value: undefined});
		result.push({key: PrintResultTypes.TwoColumnTable, value: ['lottery.draw', 'main_navigation.results']});
		result.push({key: PrintResultTypes.Line, value: undefined});
		result.push({key: PrintResultTypes.TwoColumnTableFast, value: [
			this.viewData.drawNumber,
			this.viewData.cardsReport
		]});
		result.push({key: PrintResultTypes.TwoColumnTableFast, value: ['', this.viewData.pokerReport]});
		result.push({key: PrintResultTypes.Line, value: undefined});

		return result;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
