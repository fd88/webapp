import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KareResultsComponent } from '@app/kare/components/kare-results/kare-results.component';
import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GameResultsService } from '@app/core/services/results/game-results.service';
import { HttpService } from '@app/core/net/http/services/http.service';


import { ReportsService } from '@app/core/services/report/reports.service';
import { DatePipe } from '@angular/common';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { KareCards, KareCardsSuit } from '@app/core/net/http/api/models/kare-reg-bet';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { LotteriesService } from '@app/core/services/lotteries.service';

describe('KareResultsComponent', () => {
	let component: KareResultsComponent;
	let fixture: ComponentFixture<KareResultsComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				SharedModule,
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot({})
			],
			declarations: [
				KareResultsComponent,
				KareCardWithLabelComponent
			],
			providers: [
				GameResultsService,
				HttpService,


				ReportsService,
				DatePipe,
				MslCurrencyPipe,
				LogService,
				AppStoreService,
				StorageService,
				TransactionService,
				PrintService,
				TranslateService,
				LotteriesService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				}
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(KareResultsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectedResultsHandler function', () => {
		component.onSelectedResultsHandler(undefined);
		expect(component.viewData).toBeUndefined();
		expect(component.printData).toBeUndefined();

		component.onSelectedResultsHandler({
			draw_code: 84550,
			draw_name: '110919/006',
			drawing: [{
				extra_info: '',
				name: 'Основний розiграш',
				win_comb: '9Б,7Ч,9Ч,3Т,ДП\nПара',
				data: undefined,
				win_cat: undefined
			}],
			drawing_date_begin: '2019-09-11 08:33:45',
			drawing_date_end: '2019-10-12 08:37:25',
			winning_date_end: '2019-10-12 08:37:25'
		});
		// TODO expect component.viewData.cards

		component.onSelectedResultsHandler({
			draw_code: 84550,
			draw_name: '110919/006',
			drawing: [{
				extra_info: '',
				name: 'Основний розiграш',
				win_comb: '9Б,7Ч,9Ч,3Т,ДП',
				data: undefined,
				win_cat: undefined
			}],
			drawing_date_begin: '2019-09-11 08:33:45',
			drawing_date_end: '2019-10-12 08:37:25',
			winning_date_end: '2019-10-12 08:37:25'
		});
		// TODO expect component.viewData.cards

		component.onSelectedResultsHandler({
			draw_code: 84550,
			draw_name: '110919/006',
			drawing: [],
			drawing_date_begin: '2019-09-11 08:33:45',
			drawing_date_end: '2019-10-12 08:37:25',
			winning_date_end: '2019-10-12 08:37:25'
		});
		// TODO expect component.viewData.cards
	});

	it('test trackByKareDescriptionItemFn function', () => {
		expect(component.trackByKareDescriptionItemFn(12, {
			suit: KareCardsSuit.Spades, card: KareCards.Ace, label: '', shortLabel: '', cardNumber: 0
		})).toEqual('0012');
	});

	it('test trackByWinCombFn  function', () => {
		expect(component.trackByWinCombFn (13, 'xxx')).toEqual('xxx13');
	});
});
