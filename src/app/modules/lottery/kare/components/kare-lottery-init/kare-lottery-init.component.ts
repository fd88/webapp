import {
	ApplicationRef,
	Component,
	ComponentFactoryResolver,
	ComponentRef,
	EmbeddedViewRef,
	Injector,
	OnDestroy,
	OnInit,
	ViewChild
} from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
	IKareBetItem,
	IKareBetItemGameType1,
	IKareBetItemGameType2,
	KareGameType,
	KareInputMode
} from '@app/core/net/http/api/models/kare-reg-bet';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BetDataSource } from '@app/core/game/base-game.service';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import { KARE_DRAWS_LABELS, KareLotteryService } from '@app/kare/services/kare-lottery.service';
import { IKareEditItem } from '@app/kare/interfaces/ikare-edit-item';
import { KareEditBetDialogComponent } from '@app/kare/components/kare-edit-bet-dialog/kare-edit-bet-dialog.component';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { AppType } from '@app/core/services/store/settings';

/**
 * Компонент ручного ввода данных для лотереи "Каре".
 */
@Component({
	selector: 'app-kare-lottery-init',
	templateUrl: './kare-lottery-init.component.html',
	styleUrls: ['./kare-lottery-init.component.scss']
})
export class KareLotteryInitComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на компонент выбора тиражей.
	 */
	@ViewChild(ButtonsGroupComponent, { static: true })
	drawsComponent: ButtonsGroupComponent;

	/**
	 * Стиль группы кнопок.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Режим работы группы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Список кодов игр.
	 */
	readonly LotteryGameCode = LotteryGameCode;

	/**
	 * Перечисление используемых источников для создания ставок
	 */
	readonly BetDataSource = BetDataSource;

	/**
	 * Список вариантов ставок в игре "Каре"
	 */
	readonly KareGameType = KareGameType;

	/**
	 * Надписи на кнопках выбора количества тиражей
	 */
	readonly KARE_DRAWS_LABELS = KARE_DRAWS_LABELS.map(m => `${m}`);

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Ссылка на компонент диалога редактирования ставки.
	 * @private
	 */
	private _componentRef: ComponentRef<KareEditBetDialogComponent>;

	/**
	 * Подписка на события диалога редактирования ставки.
	 * @private
	 */
	private _dialogSubscription: Subscription;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {KareLotteryService} kareLotteryService Сервис лотереи "Каре".
	 * @param {Injector} injector Инжектор зависимостей.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 */
	constructor(
		readonly kareLotteryService: KareLotteryService,
		private readonly injector: Injector,
		private readonly appStoreService: AppStoreService
	) {}

	/**
	 * Обработчик выбора тиражей.
	 * При получении корректного значения обновит общий параметр количества
	 * тиражей {@link IKareBetDataItem.drawCount drawCount} и во всех ставках изменит параметр {@link IKareBetItem.d d}
	 * на указанное значение.
	 *
	 * @param {IButtonGroupItem} draw Кнопка, соответствующая количеству тиражей.
	 */
	onSelectedDrawsHandler(draw: IButtonGroupItem): void {
		const d = !!draw ? +draw.label : 0;

		this.kareLotteryService.betData.drawCount = d;
		this.kareLotteryService.betData.kareBetsData.bets = this.kareLotteryService.betData.kareBetsData.bets.map(bet => {
			return {...bet, d};
		});

		this.kareLotteryService.refreshBets();
	}

	/**
	 * Обработчик клика для показа редактора ставок.
	 *
	 * @param {KareGameType} gameType Тип игры.
	 */
	onClickCreateBetHandler(gameType: KareGameType): void {
		this.showBetEditor({gameType, cards: [], betSum: 0, betSumLabel: '0'});
	}

	/**
	 * Обработчик удаления выбранной ставки.
	 *
	 * @param {number} index Индекс выбранной ставки.
	 */
	onBetRemovedHandler(index: number): void {
		this.kareLotteryService.betData.kareBetsData.bets.splice(index, 1);
		this.updateBets();
	}

	/**
	 * Обработчик клика на выбранной ставке.
	 * Для данной ставки будет открыт диалог в режиме редактирования.
	 *
	 * @param {number} index Индекс выбранной ставки.
	 */
	onEditBetHandler(index: number): void {
		const bet = this.kareLotteryService.betData.kareBetsData.bets[index];
		this.showBetEditor({
			gameType: bet.t,
			cards: bet.t === KareGameType.Table1_Cards ? (bet as IKareBetItemGameType1).c : (bet as IKareBetItemGameType2).p,
			id: index,
			betSum: bet.s,
			betSumLabel: `${bet.s / 100}`
		});
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Показать диалог создания/редактирования ставки на игру "Каре".
	 *
	 * @param {IKareEditItem} kareEditItem Данные для редактирования ставки.
	 */
	private showBetEditor(kareEditItem: IKareEditItem | undefined): void {
		const componentFactoryResolver = this.injector.get<ComponentFactoryResolver>(ComponentFactoryResolver as any);
		const appRef = this.injector.get(ApplicationRef);
		this._componentRef = componentFactoryResolver
			.resolveComponentFactory(KareEditBetDialogComponent)
			.create(this.injector);

		this._componentRef.instance.kareEditItem = kareEditItem;
		this._componentRef.instance.showKeyboardIcon = this.appStoreService.Settings.appType === AppType.ALTTerminal;
		this._dialogSubscription = this._componentRef.instance.closeDialog$$
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(this.hideBetEditor.bind(this));

		appRef.attachView(this._componentRef.hostView);
		const domElem = (this._componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
		document.body.appendChild(domElem);
	}

	/**
	 * Спрятать диалог редактирования ставки.
	 *
	 * @param {IKareEditItem | undefined} kareEditItem Данные для редактирования ставки.
	 */
	private hideBetEditor(kareEditItem?: IKareEditItem): void {
		if (!!this._dialogSubscription) {
			this._dialogSubscription.unsubscribe();
		}

		// удалить диалог
		const appRef = this.injector.get(ApplicationRef);
		if (this._componentRef) {
			appRef.detachView(this._componentRef.hostView);
			this._componentRef.destroy();
			this._componentRef = undefined;
		}

		// если редактируемый элемент не undefined - добавить ставку в массив
		if (kareEditItem) {
			const newBetsArray = [...this.kareLotteryService.betData.kareBetsData.bets];
			const betItem: IKareBetItemGameType1 | IKareBetItemGameType2 = {
				i: KareInputMode.Manual,
				t: kareEditItem.gameType,
				s: kareEditItem.betSum,
				d: this.kareLotteryService.betData.drawCount
			};

			if (kareEditItem.gameType === KareGameType.Table1_Cards) {
				(betItem as IKareBetItemGameType1).a = false;
				(betItem as IKareBetItemGameType1).c = kareEditItem.cards;
			} else if (kareEditItem.gameType === KareGameType.Table2_Poker) {
				betItem.p = kareEditItem.cards;
			} else {
				// пропустить неизвестный тип ставки
				return;
			}

			// обновить или добавить элемент
			Number.isInteger(kareEditItem.id)
				? newBetsArray.splice(kareEditItem.id, 1, betItem)
				: newBetsArray.push(betItem);
			this.updateBets(newBetsArray);
		}
	}

	/**
	 * Функция обновления ставки.
	 *
	 * @param {Array<IKareBetItemGameType1 | IKareBetItemGameType2>} newBetsArray Новый массив ставок.
	 */
	private updateBets(newBetsArray?: Array<IKareBetItemGameType1 | IKareBetItemGameType2>): void {
		this.kareLotteryService.betData.kareBetsData.bets = Array.isArray(newBetsArray)
			? newBetsArray
			: [...this.kareLotteryService.betData.kareBetsData.bets];
		this.kareLotteryService.betData.kareBetsData = {...this.kareLotteryService.betData.kareBetsData};
		this.kareLotteryService.refreshBets();
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.kareLotteryService.initBetData(BetDataSource.Manual);

		// восстановить кол-во тиражей
		this.drawsComponent.clearButtons();
		if (this.kareLotteryService.betData.drawCount > 0) {
			this.drawsComponent.selectedButtons = this.kareLotteryService.betData.drawCount - 1;
		}
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.hideBetEditor();

		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
