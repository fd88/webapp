import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KareLotteryInitComponent} from './kare-lottery-init.component';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {AppStoreService} from '@app/core/services/store/app-store.service';

import {KareLotteryService} from '@app/kare/services/kare-lottery.service';
import {KareBetsListComponent} from '@app/kare/components/kare-bets-list/kare-bets-list.component';
import {
	KareBetListItemOneComponent
} from '@app/kare/components/kare-bet-list-item-one/kare-bet-list-item-one.component';
import {
	KareBetListItemTwoComponent
} from '@app/kare/components/kare-bet-list-item-two/kare-bet-list-item-two.component';
import {KareCardWithLabelComponent} from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {BetDataSource} from '@app/core/game/base-game.service';
import {
	IKareBetItemGameType1,
	IKareBetItemGameType2,
	KareGameType,
	KareInputMode
} from '@app/core/net/http/api/models/kare-reg-bet';
import {HttpService} from '@app/core/net/http/services/http.service';
import {HttpServiceStub} from '@app/core/net/http/services/http.service.spec';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';
import {KareEditBetDialogComponent} from '@app/kare/components/kare-edit-bet-dialog/kare-edit-bet-dialog.component';
import {KareBetTypeOneComponent} from '@app/kare/components/kare-bet-type-one/kare-bet-type-one.component';
import {KareBetTypeTwoComponent} from '@app/kare/components/kare-bet-type-two/kare-bet-type-two.component';
import {
	KareSelectedCardsOneComponent
} from '@app/kare/components/kare-selected-cards-one/kare-selected-cards-one.component';
import {
	KareSelectedCardsTwoComponent
} from '@app/kare/components/kare-selected-cards-two/kare-selected-cards-two.component';
import {LotteriesService} from '@app/core/services/lotteries.service';
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_KARE} from "../../../../features/mocks/kare-draws";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

describe('KareLotteryInitComponent', () => {
	const bet1: IKareBetItemGameType1 = {t: KareGameType.Table1_Cards, a: false, c: [0, 13, 26, 39], d: 1, s: 500, i: KareInputMode.Manual};
	const bet2: IKareBetItemGameType2 = {t: KareGameType.Table2_Poker, d: 1, s: 500, p: [1, 2], i: KareInputMode.Manual};

	let component: KareLotteryInitComponent | any;
	let appStoreService: AppStoreService;
	let fixture: ComponentFixture<KareLotteryInitComponent>;
	let kareLotteryService: KareLotteryService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
				KareLotteryInitComponent,
				KareBetsListComponent,
				KareBetListItemOneComponent,
				KareBetListItemTwoComponent,
				KareCardWithLabelComponent,
				KareEditBetDialogComponent,
				KareBetTypeOneComponent,
				KareBetTypeTwoComponent,
				KareSelectedCardsOneComponent,
				KareSelectedCardsTwoComponent,
				KareCardWithLabelComponent
			],
			providers: [
				LogService,
				LotteriesService,
				KareLotteryService,
				AppStoreService,
				{
					provide: HttpService,
					useValue: new HttpServiceStub()
				},
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				}
			]
		})
		.compileComponents();

		TestBed.overrideModule(BrowserDynamicTestingModule, {
			set: {
				entryComponents: [
					KareEditBetDialogComponent
				],
			},
		});

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Kare, DRAWS_FOR_GAME_KARE.lottery)
		appStoreService.Settings.populateEsapActionsMapping(csConfig);

		kareLotteryService = TestBed.inject(KareLotteryService);
		kareLotteryService.initBetData(BetDataSource.Manual);

		// let lotteriesService = TestBed.inject(LotteriesService);
		// await lotteriesService.reload();

		fixture = TestBed.createComponent(KareLotteryInitComponent);
		component = fixture.componentInstance;
		component.kareLotteryService.initBetData(BetDataSource.Manual);
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();

		// проверка восстановления кол-ва тиражей
		const f = TestBed.createComponent(KareLotteryInitComponent);
		const c = f.componentInstance;
		c.kareLotteryService.initBetData(BetDataSource.Manual);
		c.kareLotteryService.betData.drawCount = 5;
		f.detectChanges();
		expect(c.drawsComponent.selectedButtons).toEqual(4);
	});

	it('test onSelectedDrawsHandler function', () => {
		component.onSelectedDrawsHandler(undefined);
		expect(kareLotteryService.betData.drawCount).toEqual(0);
		kareLotteryService.betData.kareBetsData.bets.forEach(f => expect(f.d).toEqual(0));

		component.onSelectedDrawsHandler({index: 0, label: '5', selected: true, disabled: false});
		expect(kareLotteryService.betData.drawCount).toEqual(5);
		kareLotteryService.betData.kareBetsData.bets.forEach(f => expect(f.d).toEqual( 5));
	});

	it('test onClickCreateBetHandler function', () => {
		spyOn(component, 'showBetEditor').and.callThrough();
		component.onClickCreateBetHandler(KareGameType.Table1_Cards);
		expect(component.showBetEditor).toHaveBeenCalledWith({gameType: KareGameType.Table1_Cards, cards: [], betSum: 0, betSumLabel: '0'});
	});

	it('test onBetRemovedHandler function', () => {
		kareLotteryService.betData.kareBetsData.bets = [bet1, bet2];
		component.onBetRemovedHandler(0);
		expect(kareLotteryService.betData.kareBetsData.bets.includes(bet2)).toBeTruthy();

		kareLotteryService.betData.kareBetsData.bets = [bet1, bet2];
		component.onBetRemovedHandler(1);
		expect(kareLotteryService.betData.kareBetsData.bets.includes(bet1)).toBeTruthy();
	});

	it('test onEditBetHandler function', () => {
		spyOn(component, 'showBetEditor').and.callThrough();

		kareLotteryService.betData.kareBetsData.bets = [bet1, bet2];
		component.onEditBetHandler(0);
		expect(component.showBetEditor).toHaveBeenCalledWith({
			id: 0, gameType: bet1.t, cards: bet1.c, betSum: bet1.s, betSumLabel: `${bet1.s / 100}`
		});

		kareLotteryService.betData.kareBetsData.bets = [bet1, bet2];
		component.onEditBetHandler(1);
		expect(component.showBetEditor).toHaveBeenCalledWith(
			{id: 1, gameType: bet2.t, cards: bet2.p, betSum: bet2.s, betSumLabel: `${bet2.s / 100}`}
		);
	});

	it('test hideBetEditor function #1', () => {
		spyOn(component, 'updateBets').and.callThrough();
		component.hideBetEditor();
		expect(component.updateBets).not.toHaveBeenCalled();
	});

	it('test hideBetEditor function #2', () => {
		spyOn(component, 'updateBets').and.callThrough();
		component.hideBetEditor({cards: [5, 6, 7, 8], betSumLabel: '10', betSum: 1000, id: 0, gameType: 999});
		expect(component.updateBets).not.toHaveBeenCalled();
	});

	it('test hideBetEditor function', () => {
		spyOn(component, 'updateBets').and.callThrough();

		component.hideBetEditor({cards: [0, 1, 2, 3], betSumLabel: '5', betSum: 500, id: 0, gameType: KareGameType.Table1_Cards});
		expect(component.updateBets).toHaveBeenCalledWith([
			{t: KareGameType.Table1_Cards, d: 0, a: false, c: [0, 1, 2, 3], s: 500, i: KareInputMode.Manual}
		]);

		component.hideBetEditor({cards: [0, 13, 26, 39], betSumLabel: '5', betSum: 500, id: 1, gameType: KareGameType.Table2_Poker});
		expect(component.updateBets).toHaveBeenCalledWith([
			{t: KareGameType.Table1_Cards, d: 0, a: false, c: [0, 1, 2, 3], s: 500, i: KareInputMode.Manual},
			{t: KareGameType.Table2_Poker, d: 0, p: [0, 13, 26, 39], s: 500, i: KareInputMode.Manual}
		]);

		component.hideBetEditor({cards: [5, 6, 7, 8], betSumLabel: '10', betSum: 1000, id: 0, gameType: KareGameType.Table1_Cards});
		expect(component.updateBets).toHaveBeenCalledWith([
			{t: KareGameType.Table1_Cards, d: 0, a: false, c: [5, 6, 7, 8], s: 1000, i: KareInputMode.Manual},
			{t: KareGameType.Table2_Poker, d: 0, p: [0, 13, 26, 39], s: 500, i: KareInputMode.Manual}
		]);

		component.hideBetEditor({cards: [10, 11], betSumLabel: '20', betSum: 2000, id: undefined, gameType: KareGameType.Table1_Cards});
		expect(component.updateBets).toHaveBeenCalledWith([
			{t: KareGameType.Table1_Cards, d: 0, a: false, c: [5, 6, 7, 8], s: 1000, i: KareInputMode.Manual},
			{t: KareGameType.Table2_Poker, d: 0, p: [0, 13, 26, 39], s: 500, i: KareInputMode.Manual}
		]);
	});

});
