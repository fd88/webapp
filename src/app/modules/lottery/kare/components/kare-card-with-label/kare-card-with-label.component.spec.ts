import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { TranslateModule } from '@ngx-translate/core';

xdescribe('KareCardWithLabelComponent', () => {
	let component: KareCardWithLabelComponent;
	let fixture: ComponentFixture<KareCardWithLabelComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				KareCardWithLabelComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareCardWithLabelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
