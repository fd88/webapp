import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { KareCardsSuit, KareCards } from '@app/core/net/http/api/models/kare-reg-bet';

/**
 * Модель, описывающая карту.
 */
export interface IKareCardDescriptionItem {

	/**
	 * Номер карты (код карты), описанный в протоколе eSAP.
	 */
	cardNumber: number;

	/**
	 * Масть карты в списке {@link KareCardsSuit}.
	 */
	suit: KareCardsSuit;

	/**
	 * Номер карты в списке {@link KareCards}.
	 */
	card: KareCards;

	/**
	 * Длинное название карты.
	 */
	label: string;

	/**
	 * Короткое название карты.
	 */
	shortLabel: string;

}

/**
 * Компонент отображения имени карты и ее пиктограммы.
 * Элменты компонента расположены вертикально.
 */
@Component({
	selector: 'app-kare-card-with-label',
	templateUrl: './kare-card-with-label.component.html',
	styleUrls: ['./kare-card-with-label.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareCardWithLabelComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Модель с описанием карты.
	 */
	@Input()
	cardDescriptionItem: IKareCardDescriptionItem;

	/**
	 * Признак, указывающий на необходимость вывода короткого имени карты.
	 */
	@Input()
	useShortLabels: boolean;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
