import { EventEmitter, Input, Output, Directive } from '@angular/core';
import { KareCards, KareCardsSuit, KarePokerCombinations } from '@app/core/net/http/api/models/kare-reg-bet';

/**
 * Интерфейс для элемента списка выбранных карт или комбинаций.
 */
export interface IKareEntityItem {
	/**
	 * Код карты или комбинации.
	 */
	entityCode: KarePokerCombinations | number;
	/**
	 * Выбрана ли карта или комбинация.
	 */
	selected: boolean;
}

/**
 * Абстрактный класс для ввода ставки в игре "Каре".
 */
@Directive()
export abstract class KareBaseBetType {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Список номеров выбранных карт или комбинаций.
	 * Создает список элементов типа {@link IKareEntityItem}.
	 */
	@Input()
	set entities(value: Array<KarePokerCombinations | number>) {
		this.entityItemsList = this.entityItemFactory(value);
		this.countSelected = this.entityItemsList.filter(f => f.selected).length;
	}

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Событие изменения массива выбранных карт или комбинаций.
	 */
	@Output()
	readonly entitiesListChanged = new EventEmitter<Array<KarePokerCombinations | number>>();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Фабрика для создания списка элементов {@link IKareEntityItem}.
	 */
	abstract readonly entityItemFactory: (entityList: Array<KarePokerCombinations | number>) => Array<IKareEntityItem>;

	/**
	 * Полный список элементов {@link IKareEntityItem}.
	 */
	entityItemsList: Array<IKareEntityItem>;

	/**
	 * Количество выбранных элементов.
	 */
	countSelected: number;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Функция для отслеживания изменений в списке элементов {@link entityItemsList}.
	 * @param index Индекс элемента в списке.
	 * @param item Элемент списка.
	 */
	trackByKareEntityItemFn = (index, item: IKareEntityItem) => `${index}${item.selected}${item.entityCode}`;

	/**
	 * Функция для отслеживания изменений в списке элементов {@link KareCards}.
	 * @param index Индекс элемента в списке.
	 * @param item Элемент списка.
	 */
	trackByNumberFn = (index, item: KareCards | KareCardsSuit) => `${index}${item}`;

	/**
	 * Обработчик клика по элементу из списка.
	 *
	 * @param {number} index Индекс элемента, по которому совершен клик, в списке {@link entityItemsList}
	 */
	protected parseIndex(index: number): void {
		this.entityItemsList[index].selected = !this.entityItemsList[index].selected;
		const newArr = this.entityItemsList
			.filter(f => f.selected)
			.map(m => m.entityCode);
		this.countSelected = newArr.length;
		this.entitiesListChanged.emit(newArr);
	}

}
