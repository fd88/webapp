import { ChangeDetectionStrategy, Component } from '@angular/core';
import { KarePokerCombinations } from '@app/core/net/http/api/models/kare-reg-bet';
import { KARE_POKER_COMBINATIONS_VALUES, POKER_CODES } from '@app/kare/services/kare-lottery.service';
import { IKareEntityItem, KareBaseBetType } from '@app/kare/components/kare-base-bet-type';

/**
 * Список имен комбинаций для второго типа игры "Каре".
 */
const KARE_COMBINATION_NAMES = ['lottery.kare.combinations', ...POKER_CODES];

/**
 * Компонент для создания ставки по {@link KareGameType.Table2_Poker второму типу} игры "Каре".
 */
@Component({
	selector: 'app-kare-bet-type-two',
	templateUrl: './kare-bet-type-two.component.html',
	styleUrls: ['./kare-bet-type-two.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareBetTypeTwoComponent extends KareBaseBetType  {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список имен комбинаций для второго типа игры "Каре".
	 */
	readonly KARE_COMBINATION_NAMES = KARE_COMBINATION_NAMES;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Фабрика для создания списка сущностей второго типа игры "Каре".
	 * @param entityList
	 */
	readonly entityItemFactory = (entityList: Array<KarePokerCombinations>): Array<IKareEntityItem> => {
		return KARE_POKER_COMBINATIONS_VALUES
			.map(value => {
				return {
					entityCode: value,
					selected: entityList.includes(value)
				};
			});
	};

	/**
	 * Обработчик события выбора комбинации.
	 *
	 * @param {number} index Индекс комбинации в списке.
	 */
	onSelectButtonHandler(index: number): void {
		this.parseIndex(index);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
