import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { KareBetTypeTwoComponent } from './kare-bet-type-two.component';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

xdescribe('KareBetTypeTwoComponent', () => {
	let component: KareBetTypeTwoComponent;
	let fixture: ComponentFixture<KareBetTypeTwoComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
				KareBetTypeTwoComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareBetTypeTwoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onSelectButtonHandler function', () => {
		spyOn(component.entitiesListChanged, 'emit');
		component.entities = [];

		component.onSelectButtonHandler(0);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([1]);

		component.onSelectButtonHandler(1);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([1, 2]);

		component.onSelectButtonHandler(2);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([1, 2, 3]);

		component.onSelectButtonHandler(0);
		fixture.detectChanges();
		expect(component.entitiesListChanged.emit).toHaveBeenCalledWith([2, 3]);
	});
});
