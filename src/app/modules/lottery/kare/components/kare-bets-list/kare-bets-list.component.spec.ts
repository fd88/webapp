import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared/shared.module';
import { KareBetListItemOneComponent } from '@app/kare/components/kare-bet-list-item-one/kare-bet-list-item-one.component';
import { KareBetListItemTwoComponent } from '@app/kare/components/kare-bet-list-item-two/kare-bet-list-item-two.component';
import { KareBetsListComponent } from '@app/kare/components/kare-bets-list/kare-bets-list.component';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { KareGameType, KareInputMode } from '@app/core/net/http/api/models/kare-reg-bet';

describe('KareBetsListComponent', () => {
	let component: KareBetsListComponent;
	let fixture: ComponentFixture<KareBetsListComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
				KareBetsListComponent,
				KareBetListItemOneComponent,
				KareBetListItemTwoComponent,
				KareCardWithLabelComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareBetsListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test trackByBets function', () => {
		const result = component.trackByBets(0, {
			i: KareInputMode.Manual, t: KareGameType.Table1_Cards, a: true, d: 1, p: undefined, c: undefined, s: 5
		});
		expect(result).toEqual('10');
	});

	it('test onRemoveBetClickHandler function', () => {
		spyOn(component.betRemoved, 'emit');
		component.onRemoveBetClickHandler(0);
		fixture.detectChanges();
		expect(component.betRemoved.emit).toHaveBeenCalledWith(0);
	});

	it('test onBetClickHandler function', () => {
		spyOn(component.editBet, 'emit');
		component.onBetClickHandler(0);
		fixture.detectChanges();
		expect(component.editBet.emit).toHaveBeenCalledWith(0);
	});
});
