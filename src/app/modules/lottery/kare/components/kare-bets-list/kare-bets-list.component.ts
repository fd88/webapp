import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { IKareBetItemGameType1, IKareBetItemGameType2, IKareBetsData, KareGameType } from '@app/core/net/http/api/models/kare-reg-bet';

/**
 * Компонент, отображающий список ставок по игре "Каре".
 */
@Component({
	selector: 'app-kare-bets-list',
	templateUrl: './kare-bets-list.component.html',
	styleUrls: ['./kare-bets-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareBetsListComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Сеттер текущих ставок.
	 * @param value Список ставок.
	 */
	@Input()
	set kareBetsData(value: IKareBetsData) {
		this._kareBetsData = value;
	}

	/**
	 * Геттер текущих ставок.
	 */
	get kareBetsData(): IKareBetsData {
		return this._kareBetsData;
	}

	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие, оповещающее родителя о том, что ставка должна быть удалена из списка.
	 */
	@Output()
	readonly betRemoved = new EventEmitter<number>();

	/**
	 * Событие, оповещающее родителя о том, что ставку необходимо открыть в диалоге на редактирование.
	 */
	@Output()
	readonly editBet = new EventEmitter<number>();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Типы игры "Каре".
	 */
	readonly KareGameType = KareGameType;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Данные ставок в игре "Каре".
	 * @private
	 */
	private _kareBetsData: IKareBetsData;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Функция для отслеживания изменений в списке ставок.
	 * @param index Индекс элемента в списке.
	 * @param item Элемент списка.
	 */
	trackByBets = (index, item: IKareBetItemGameType1 | IKareBetItemGameType2) => `${item.t}${index}`;

	/**
	 * Обработчик клика по крестику.
	 *
	 * @param {number} index Индекс элемента в списке.
	 */
	onRemoveBetClickHandler(index: number): void {
		this.betRemoved.emit(index);
	}

	/**
	 * Обработчик клика по телу ставки.
	 *
	 * @param {number} index Индекс элемента в списке.
	 */
	onBetClickHandler(index: number): void {
		this.editBet.emit(index);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
