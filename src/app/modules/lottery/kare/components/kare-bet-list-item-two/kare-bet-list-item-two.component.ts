import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { IKareBetItemGameType2 } from '@app/core/net/http/api/models/kare-reg-bet';
import { POKER_CODES } from '@app/kare/services/kare-lottery.service';

/**
 * Компонент, отображающий ставку по типу 2 в виде кнопки в списке ставок.
 */
@Component({
	selector: 'app-kare-bet-list-item-two',
	templateUrl: './kare-bet-list-item-two.component.html',
	styleUrls: ['./kare-bet-list-item-two.component.scss']
})
export class KareBetListItemTwoComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Сеттер ставки
	 * @param value	Ставка
	 */
	@Input()
	set bet(value: IKareBetItemGameType2) {
		this._bet = value;
		this.parseBet();
	}

	/**
	 * Геттер ставки
	 */
	get bet(): IKareBetItemGameType2 {
		return this._bet;
	}

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Комбинации в строковом виде
	 */
	combinations: string;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Ставка второго типа
	 * @private
	 */
	private _bet: IKareBetItemGameType2;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 */
	constructor(
		private readonly translateService: TranslateService
	) {}

	/**
	 * Преобразование ставки в строку
	 * @private
	 */
	private parseBet(): void {
		this.combinations = this.bet && Array.isArray(this.bet.p)
			? this.bet.p.reduce((p, c, i) => `${p}${i > 0 ? ', ' : ''}${this.translateService.instant(POKER_CODES[c - 1])}`, '')
			: '';
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.translateService.onLangChange
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(this.parseBet.bind(this));
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
