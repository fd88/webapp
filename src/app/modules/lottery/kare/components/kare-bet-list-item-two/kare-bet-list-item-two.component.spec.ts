import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { KareBetListItemTwoComponent } from './kare-bet-list-item-two.component';
import { TranslateModule } from '@ngx-translate/core';
import { KareGameType } from '@app/core/net/http/api/models/kare-reg-bet';

describe('KareBetListItemTwoComponent', () => {
	let component: KareBetListItemTwoComponent | any;
	let fixture: ComponentFixture<KareBetListItemTwoComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				KareBetListItemTwoComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareBetListItemTwoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test parseBet function', () => {
		component.bet = undefined;
		component.parseBet();
		expect(component.combinations).toEqual('');

		component.bet = {p: [], s: 0, t: KareGameType.Table2_Poker, d: 0};
		component.parseBet();
		expect(component.combinations).toEqual('');

		component.bet = {p: [1, 2, 3], s: 15, t: KareGameType.Table2_Poker, d: 1};
		component.parseBet();
		expect(component.combinations).toEqual('lottery.kare.royal_flush, lottery.kare.straight_flush, lottery.kare.square');
	});
});
