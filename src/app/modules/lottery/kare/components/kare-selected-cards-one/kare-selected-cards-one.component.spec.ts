import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TranslateModule } from '@ngx-translate/core';
import { KareSelectedCardsOneComponent } from '@app/kare/components/kare-selected-cards-one/kare-selected-cards-one.component';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { KareCards, KareCardsSuit } from '@app/core/net/http/api/models/kare-reg-bet';

xdescribe('KareSelectedCardsOneComponent', () => {
	let component: KareSelectedCardsOneComponent;
	let fixture: ComponentFixture<KareSelectedCardsOneComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				KareSelectedCardsOneComponent,
				KareCardWithLabelComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KareSelectedCardsOneComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test cards input', () => {
		component.cards = [];
		expect(component.cardDescriptionItemList.length).toEqual(0);

		component.cards = [0, 13, 26, 39];
		expect(component.cardDescriptionItemList.length).toEqual(4);
		component.cardDescriptionItemList.forEach(item => {
			expect(item.card).toEqual(KareCards.Ace);
		});
	});

	it('test function trackByCardItemFn', () => {
		const test = component.trackByCardItemFn(0, {card: KareCards.Ace, suit: KareCardsSuit.Spades, cardNumber: 0, shortLabel: '', label: ''});
		expect(test).toEqual('000');
	});
});
