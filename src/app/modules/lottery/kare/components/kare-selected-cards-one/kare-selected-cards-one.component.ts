import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { convertCodeToCardItem } from '@app/kare/services/kare-lottery.service';
import { IKareCardDescriptionItem } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';

/**
 * Компонент отображения информации о выбранных картах в игре "Каре" по типу ставки №1.
 */
@Component({
	selector: 'app-kare-selected-cards-one',
	templateUrl: './kare-selected-cards-one.component.html',
	styleUrls: ['./kare-selected-cards-one.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareSelectedCardsOneComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Задать список кодов выбранных пользователем карт, которые необходимо отобразить.
	 *
	 * @param {Array<number>} value Список карт.
	 */
	@Input()
	set cards(value: Array<number>) {
		this.cardDescriptionItemList = value.map(m => {
			return convertCodeToCardItem(m);
		});
	}

	/**
	 * Максимальное количество карт.
	 */
	@Input()
	maxCardsCount = 5;

	/**
	 * Показывать ли заголовок
	 */
	@Input()
	showTitle = true;

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список карт для отображения.
	 */
	cardDescriptionItemList: Array<IKareCardDescriptionItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Функция для отслеживания изменений в списке карт.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByCardItemFn = (index, item: IKareCardDescriptionItem) => `${index}${item.suit}${item.cardNumber}`;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
