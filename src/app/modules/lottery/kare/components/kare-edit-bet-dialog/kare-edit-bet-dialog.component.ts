import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { KareGameType, KarePokerCombinations } from '@app/core/net/http/api/models/kare-reg-bet';
import {
	ButtonGroupMode,
	ButtonsGroupComponent,
	IButtonGroupItem,
	ButtonGroupStyle
} from '@app/shared/components/buttons-group/buttons-group.component';
import { MslInputWithKeyboardComponent } from '@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component';
import { IKareEditItem } from '@app/kare/interfaces/ikare-edit-item';
import { KARE_BET_SUMM_VALUES, KARE_MAX_BET_SUM, KARE_MIN_BET_SUM } from '@app/kare/services/kare-lottery.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { AppType } from '@app/core/services/store/settings';

/**
 * Состояние поля ввода
 * - {@link Empty} - пустое
 * - {@link Invalid} - невалидное
 * - {@link Valid} - валидное
 */
enum InputState {
	Empty = 0,
	Invalid = 1,
	Valid = 2
}

/**
 * Компонент-диалог ввода/редактирования ставки в игре "Каре" для обоих типов игр.
 */
@Component({
	selector: 'app-kare-edit-bet-dialog',
	templateUrl: './kare-edit-bet-dialog.component.html',
	styleUrls: ['./kare-edit-bet-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class KareEditBetDialogComponent implements OnInit, OnDestroy, AfterViewInit {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на компонент кнопок с выбором суммы ставки.
	 */
	@ViewChild(ButtonsGroupComponent, {static: true})
	kcBetsButtons: ButtonsGroupComponent;

	/**
	 * Ссылка на компонент ручного ввода суммы ставки.
	 */
	@ViewChild(MslInputWithKeyboardComponent, {static: true})
	manualSummaInput: MslInputWithKeyboardComponent;

	/**
	 * Список значений сумм ставок на кнопках выбора суммы ставки в гривнах.
	 */
	readonly KARE_BET_SUMM_VALUES = KARE_BET_SUMM_VALUES;

	/**
	 * Минимальная сумма ставки.
	 */
	readonly KARE_MIN_BET_SUM = KARE_MIN_BET_SUM;

	/**
	 * Максимальная сумма ставки.
	 */
	readonly KARE_MAX_BET_SUM = KARE_MAX_BET_SUM;

	/**
	 * Перечисление режимов работы компонента группы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Перечисление типов игр в "Каре".
	 */
	readonly KareGameType = KareGameType;

	/**
	 * Перечисление состояний поля ввода.
	 */
	readonly InputState = InputState;

	/**
	 * Перечисление стилей компонента группы кнопок.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Тип приложения.
	 */
	readonly AppType = AppType;

	/**
	 * Наблюдаемое значение, по которому определяется момент, когда пользователь пожелал закрыть окно диалога.
	 * Передается undefined, если пользователь просто закрыл диалог или модель ставки,
	 * которую необходимо обработать на главной форме.
	 */
	readonly closeDialog$$ = new Subject<IKareEditItem | undefined>();

	/**
	 * Модель редактора.
	 * Хранит основные параметры по редактируемой ставке.
	 */
	kareEditItem: IKareEditItem;

	/**
	 * Состояние поля ввода.
	 */
	inputState: InputState;

	/**
	 * Показывать ли иконку клавиатуры.
	 */
	@Input() showKeyboardIcon = true;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ChangeDetectorRef} cdr Ссылка на объект для обнаружения изменений.
	 * @param appStoreService Сервис хранилища приложения.
	 */
	constructor(
		private readonly cdr: ChangeDetectorRef,
		readonly appStoreService: AppStoreService
	) {}

	/**
	 * Обработчик клика на кнопках закрытия окна.
	 *
	 * @param {boolean} isClickOnCloseButton Признак клика по крестику в верхнем правом углу.
	 */
	onClickCloseHandler(isClickOnCloseButton: boolean): void {
		if ((this.inputState === InputState.Valid && this.kareEditItem.cards.length) || isClickOnCloseButton) {
			this.closeDialog$$.next(isClickOnCloseButton ? undefined : this.kareEditItem);
		}
	}

	/**
	 * Обработчик очистки выбора карт в диалоге.
	 */
	onClearCardsHandler(): void {
		this.kareEditItem.cards = [];
		this.kcBetsButtons.clearButtons();
		this.manualSummaInput.clearValue();
		this.updateBetSum(0);
	}

	/**
	 * Обработчик клика на кнопках выбора ставки.
	 *
	 * @param {IButtonGroupItem} button Выбранная кнопка.
	 */
	onSelectBetHandler(button: IButtonGroupItem): void {
		this.parseSumValue(button && button.selected ? button.label : '');

		// продублировать сумму в поле ввода
		this.manualSummaInput.value = this.kareEditItem.betSumLabel;
	}

	/**
	 * Обработчик ручного ввода суммы ставки.
	 *
	 * @param {string} sum Сумма ставки.
	 */
	onInputBetSumChangedHandler(sum: string): void {
		this.parseSumValue(sum, true);
	}

	/**
	 * Обработчик изменения списка карт или комбинаций.
	 *
	 * @param {Array<KarePokerCombinations | number>} value Список карт или комбинаций.
	 */
	onKareBetTypeChangedHandler(value: Array<KarePokerCombinations | number>): void {
		this.kareEditItem.cards = [...value];
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Функция принимает сумму и логический аргумент highlightSum, парсит сумму в число,
	 * вычисляет состояние поля ввода (inputState) и в зависимости от результата парсинга и вычисленной суммы,
	 * обновляет сумму ставки (updateBetSum). Если highlightSum истинный, то подсвечивает кнопку с вычисленной суммой.
	 * @param sum Сумма ставки
	 * @param highlightSum Подсветить ли сумму
	 * @private
	 */
	private parseSumValue(sum: string, highlightSum = false): void {
		let parsedSum = parseInt(sum, 10);
		if (isNaN(parsedSum)) {
			parsedSum = 0;
			// установить состояние поля ввода пустым
			this.inputState = InputState.Empty;
		} else {
			// установить состояние поля в зависимости от суммы
			const parsedSum100 = parsedSum * 100;
			this.inputState = parsedSum100 >= KARE_MIN_BET_SUM && parsedSum100 <= KARE_MAX_BET_SUM ? InputState.Valid : InputState.Invalid;
		}

		this.updateBetSum(parsedSum);

		// для введенной суммы подсветить кнопку (если необходимо)
		if (highlightSum) {
			const indexOfSum = KARE_BET_SUMM_VALUES.indexOf(this.kareEditItem.betSumLabel);
			if (indexOfSum === -1) {
				this.kcBetsButtons.clearButtons();
			} else {
				this.kcBetsButtons.selectButtonByIndex(indexOfSum);
			}
		}
	}

	/**
	 * Обновляет параметры суммы в моделе редактора.
	 *
	 * @param {number} summ Сумма, которой необходимо обновить модель.
	 */
	private updateBetSum(summ: number): void {
		this.kareEditItem.betSumLabel = `${summ}`;
		this.kareEditItem.betSum = summ * 100;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.inputState = this.kareEditItem.betSum === 0 ? InputState.Empty : InputState.Valid;
	}

	/**
	 * Жизненный цикл-хук, после инициализации представления и элементов DOM
	 */
	ngAfterViewInit(): void {
		const indexOfSum = !!this.kareEditItem
			? KARE_BET_SUMM_VALUES.indexOf(this.kareEditItem.betSumLabel)
			: -1;
		indexOfSum === -1
			? this.kcBetsButtons.clearButtons()
			: this.kcBetsButtons.selectButtonByIndex(indexOfSum);

		this.manualSummaInput.value = this.kareEditItem.betSum === 0 ? '' : this.kareEditItem.betSumLabel;

		this.cdr.detectChanges();
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.closeDialog$$.complete();
		this.kareEditItem = undefined;
	}

}
