import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SharedModule } from '@app/shared/shared.module';
import { KareBetTypeOneComponent } from '@app/kare/components/kare-bet-type-one/kare-bet-type-one.component';
import { KareBetTypeTwoComponent } from '@app/kare/components/kare-bet-type-two/kare-bet-type-two.component';
import { TranslateModule } from '@ngx-translate/core';
import { KareSelectedCardsOneComponent } from '@app/kare/components/kare-selected-cards-one/kare-selected-cards-one.component';
import { KareSelectedCardsTwoComponent } from '@app/kare/components/kare-selected-cards-two/kare-selected-cards-two.component';
import { KareCardWithLabelComponent } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { KareEditBetDialogComponent } from '@app/kare/components/kare-edit-bet-dialog/kare-edit-bet-dialog.component';
import { KareGameType } from '@app/core/net/http/api/models/kare-reg-bet';
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('KareEditBetDialogComponent', () => {
	let component: KareEditBetDialogComponent | any;
	let fixture: ComponentFixture<KareEditBetDialogComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				SharedModule,
				TranslateModule.forRoot(),
				HttpClientModule
			],
			declarations: [
				KareEditBetDialogComponent,
				KareBetTypeOneComponent,
				KareBetTypeTwoComponent,
				KareSelectedCardsOneComponent,
				KareSelectedCardsTwoComponent,
				KareCardWithLabelComponent
			],
			providers: [
				LogService,
				AppStoreService,
				HttpService,
				HttpClient
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		TestBed.inject(LogService);

		fixture = TestBed.createComponent(KareEditBetDialogComponent);
		component = fixture.componentInstance;
		component.kareEditItem = {betSumLabel: '', betSum: 0, gameType: KareGameType.Table1_Cards, id: 0, cards: []};
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('check function updateBetSum', () => {
		component.updateBetSum(10);
		expect(component.kareEditItem.betSumLabel).toEqual('10');
		expect(component.kareEditItem.betSum).toEqual(1000);
	});

	it('check function parseSumValue', () => {
		component.parseSumValue('');
		expect(component.kareEditItem.betSumLabel).toEqual('0');
		expect(component.kareEditItem.betSum).toEqual(0);

		component.parseSumValue('100', true);
		expect(component.kareEditItem.betSumLabel).toEqual('100');
		expect(component.kareEditItem.betSum).toEqual(10000);

		component.parseSumValue('123', true);
		expect(component.kareEditItem.betSumLabel).toEqual('123');
		expect(component.kareEditItem.betSum).toEqual(12300);
	});

	it('check function onInputBetSumChangedHandler', () => {
		component.onInputBetSumChangedHandler('987');
		expect(component.kareEditItem.betSumLabel).toEqual('987');
		expect(component.kareEditItem.betSum).toEqual(98700);
	});

	it('check function onSelectBetHandler', () => {
		component.onSelectBetHandler({label: '12', index: 0, disabled: false, selected: true});
		expect(component.kareEditItem.betSumLabel).toEqual('12');
		expect(component.kareEditItem.betSum).toEqual(1200);

		component.onSelectBetHandler(undefined);
		expect(component.kareEditItem.betSumLabel).toEqual('0');
		expect(component.kareEditItem.betSum).toEqual(0);

		component.onSelectBetHandler({label: '12', index: 0, disabled: false, selected: false});
		expect(component.kareEditItem.betSumLabel).toEqual('0');
		expect(component.kareEditItem.betSum).toEqual(0);
	});

	it('check function onClearCardsHandler', () => {
		component.onClearCardsHandler();
		expect(component.kareEditItem.cards.length).toEqual(0);
		expect(component.kareEditItem.betSumLabel).toEqual('0');
		expect(component.kareEditItem.betSum).toEqual(0);
	});

	it('check function onClickCloseHandler', () => {
		component.onClickCloseHandler(true);
		component.onClickCloseHandler(false);
	});

	it('check function onKareBetTypeChangedHandler', () => {
		component.onKareBetTypeChangedHandler([1, 2, 3]);
		expect(component.kareEditItem.cards).toEqual([1, 2, 3]);
	});
});
