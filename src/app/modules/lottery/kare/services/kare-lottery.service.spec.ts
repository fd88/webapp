import {TestBed} from '@angular/core/testing';

import {convertCodeToCardItem, KareLotteryService} from './kare-lottery.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {LogService} from '@app/core/net/ws/services/log/log.service';

import {PrintService} from '@app/core/net/ws/services/print/print.service';
import {Injector} from '@angular/core';
import {HttpService} from '@app/core/net/http/services/http.service';
import {HttpServiceStub} from '@app/core/net/http/services/http.service.spec';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {DialogContainerServiceStub} from '@app/core/dialog/services/dialog-container.service.spec';
import {TransactionService} from '@app/core/services/transaction/transaction.service';
import {TransactionServiceStub} from '@app/core/services/transaction/transaction.service.spec';
import {IKareCardDescriptionItem} from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import {KareCards, KareCardsSuit} from '@app/core/net/http/api/models/kare-reg-bet';
import {PrinterState} from '@app/core/net/ws/api/models/print/print-models';
import {BetDataSource} from '@app/core/game/base-game.service';
import {EsapActions} from '@app/core/configuration/esap';
import {LotteryGameCode} from '@app/core/configuration/lotteries';
import {AppType} from "@app/core/services/store/settings";
import {LogOutService} from "@app/logout/services/log-out.service";
import {LogOutServiceStub} from "@app/logout/services/log-out.service.spec";
import {DRAWS_FOR_GAME_KARE} from "../../../features/mocks/kare-draws";

xdescribe('KareLotteryService', () => {
	let service: KareLotteryService | any;
	let printService: PrintService | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,

				PrintService,
				Injector,
				KareLotteryService,
				{
					provide: LogOutService,
					useValue: LogOutServiceStub,
				},
				{
					provide: HttpService,
					useValue: new HttpServiceStub()
				},
AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				{
					provide: TransactionService,
					useValue: new TransactionServiceStub()
				}
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(KareLotteryService);
		service.initBetData(BetDataSource.Manual);
		service.appStoreService.Settings.appType = AppType.ALTTerminal;
		service.appStoreService.Settings.esapActionToUrlMappingByLottery.set(
			LotteryGameCode.Kare,
			new Map<string, string>([[EsapActions.KareRegBet, 'url']]
		));

		printService = TestBed.inject(PrintService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test convertCodeToCardItem function', () => {
		type KV = {code: number, item: IKareCardDescriptionItem};
		const test: Array<KV> = [{
			code: 0, item: {suit: KareCardsSuit.Spades, cardNumber: 0, card: KareCards.Ace, label: '', shortLabel: ''}
		}, {
			code: 13, item: {suit: KareCardsSuit.Hearts, cardNumber: 13, card: KareCards.Ace, label: '', shortLabel: ''}
		}, {
			code: 26, item: {suit: KareCardsSuit.Diamonds, cardNumber: 26, card: KareCards.Ace, label: '', shortLabel: ''}
		}, {
			code: 39, item: {suit: KareCardsSuit.Clubs, cardNumber: 39, card: KareCards.Ace, label: '', shortLabel: ''}
		}, {
			code: 1, item: {suit: KareCardsSuit.Spades, cardNumber: 1, card: KareCards.Card_2, label: '', shortLabel: ''}
		}];

		test.forEach(c => {
			const result = convertCodeToCardItem(c.code);
			expect(result).toBeDefined();
			expect(result.cardNumber).toEqual(c.item.cardNumber);
			expect(result.suit).toEqual(c.item.suit);
		});
	});

	xit('test buyLottery function', () => {
		const buyLotterySpy = spyOn(service, 'buyLottery').and.callThrough();

		Object.defineProperty(service, 'draws', {
			get: () => DRAWS_FOR_GAME_KARE.draws
		});
		service.betData.drawCount = 1;
		service.betData.kareBetsData = {bets: [{d: 1, a: true, t: 1, s: 5, p: undefined}]};
		service.refreshBets();

		printService.printerState = PrinterState.OffLine;
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();

		printService.printerState = PrinterState.OnLine;
		service.transactionService.executeRequest = (): Promise<void> => new Promise((resolve, reject) => reject({code: 1000}));
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();

		service.transactionService.executeRequest = (): Promise<void> => new Promise(resolve => resolve());
		service.buyLottery();
		expect(buyLotterySpy).toHaveBeenCalled();
	});
});
