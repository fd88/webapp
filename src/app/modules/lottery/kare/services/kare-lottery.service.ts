import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { TranslateService } from '@ngx-translate/core';

import { URL_KARE } from '@app/util/route-utils';
import { Logger } from '@app/core/net/ws/services/log/logger';
import {
	IKareBetItemGameType2,
	KareCards,
	KareCardsSuit,
	KareGameType,
	KarePokerCombinations,
	KareRegBetReq,
	KareRegBetResp
} from '@app/core/net/http/api/models/kare-reg-bet';
import { BaseGameService } from '@app/core/game/base-game.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { ReportsService } from '@app/core/services/report/reports.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { IKareBetDataItem } from '@app/kare/interfaces/ikare-bet-data-item';
import { IKareCardDescriptionItem } from '@app/kare/components/kare-card-with-label/kare-card-with-label.component';
import { enumToValuesArray } from '@app/util/utils';
import { LogOutService } from '@app/logout/services/log-out.service';

/**
 * Возвращает модель с начальными данными для игры "Каре".
 * Вызывается в момент перехода на форму ручного ввода или ввод с купона.
 */
const initBetDataFn = (): IKareBetDataItem => {
	return {
		drawCount: 0,
		kareBetsData: {
			bets: []
		}
	};
};

/**
 * Массив со списком ключей локализации карт для типа ставки №1.
 * Порядок элементов используется в расчете кода карты!
 */
export const CARDS_CODES_TABLE: Array<string> = [
	'lottery.kare.ace',
	'2', '3', '4', '5', '6', '7', '8', '9', '10',
	'lottery.kare.jack',
	'lottery.kare.queen',
	'lottery.kare.king'
];

/**
 * Массив со списком ключей локализации комбинаций карт.
 */
export const POKER_CODES = [
	'lottery.kare.royal_flush',
	'lottery.kare.straight_flush',
	'lottery.kare.square',
	'lottery.kare.full_house',
	'lottery.kare.flash',
	'lottery.kare.street',
	'lottery.kare.threesome',
	'lottery.kare.two_pairs',
	'lottery.kare.pair',
	'lottery.kare.any_combinations'
];

/**
 * Массив значений из списка карт {@link KareCards}.
 */
export const KARE_CARDS_VALUES = enumToValuesArray(KareCards) as Array<KareCards>;

/**
 * Массив значений из списка мастей {@link KareCardsSuit}.
 */
export const KARE_CARDS_SUIT_VALUES = enumToValuesArray(KareCardsSuit) as Array<KareCardsSuit>;

/**
 * Массив значений из списка покерных комбинаций {@link KarePokerCombinations}.
 */
export const KARE_POKER_COMBINATIONS_VALUES = enumToValuesArray(KarePokerCombinations) as Array<KarePokerCombinations>;

/**
 * Список значений сумм ставок на кнопках выбора суммы ставки в гривнах.
 */
export const KARE_BET_SUMM_VALUES: Array<string> = ['5', '10', '20', '50', '100', '200', '500', '1000'];

/**
 * Минимальная сумма ставки в копейках.
 */
export const KARE_MIN_BET_SUM = 500;

/**
 * Максимальная сумма ставки в копейках.
 */
export const KARE_MAX_BET_SUM = 450000;

/**
 * Надписи на кнопках выбора количества тиражей.
 */
export const KARE_DRAWS_LABELS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20];

/**
 * Преобразует код карты в объект с описанием карты.
 * @param {number} cardNumber Код карты.
 * @returns {IKareCardDescriptionItem}
 */
export const convertCodeToCardItem = (cardNumber: number): IKareCardDescriptionItem => {
	const cardsCount = CARDS_CODES_TABLE.length;
	const card = cardNumber % cardsCount;
	const label = CARDS_CODES_TABLE[card];
	const shortLabel = isNaN(+label) ? `${label}-short` : label;

	return {
		suit: Math.floor(cardNumber / cardsCount),
		label,
		shortLabel,
		card,
		cardNumber
	};
};

/**
 * Преобразует объект с описанием карты в код карты.
 * @param {IKareCardDescriptionItem} cardDescriptionItem Объект с описанием карты.
 * @returns {number}
 */
export const convertCardItemToCode = (cardDescriptionItem: IKareCardDescriptionItem): number => {
	const cardsCount = CARDS_CODES_TABLE.length;

	return cardDescriptionItem.suit * cardsCount + cardDescriptionItem.card;
};

/**
 * Сервис лотереи "Каре".
 * Используется как модель-контролер для хранения данных, заполняемых оператором
 * на странице лотереи, и для взаимодействия с ЦС.
 */
@Injectable({
	providedIn: 'root'
})
export class KareLotteryService extends BaseGameService<IKareBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на игру.
	 */
	readonly gameUrl = URL_KARE;

	/**
	 * Название игры.
	 */
	readonly gameName = 'lottery.kare.game_name';

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения.
	 * @param {PrintService} printService Сервис печати.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {HttpService} httpService Сервис HTTP-запросов.
	 * @param {ReportsService} reportsService Сервис отчетов.
	 * @param {StorageService} storageService Сервис хранилища данных в браузере.
	 * @param {TranslateService} translateService Сервис локализации.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {Location} location Объект адресной строки браузера.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly printService: PrintService,
		readonly dialogInfoService: DialogContainerService,
		private readonly transactionService: TransactionService,
		private readonly httpService: HttpService,
		private readonly reportsService: ReportsService,
		private readonly storageService: StorageService,
		protected readonly translateService: TranslateService,
		protected readonly router: Router,
		protected readonly location: Location,
		readonly logoutService: LogOutService
	) {
		super();

		this.currentGameCode = LotteryGameCode.Kare;
		this.currentGroupCode = LotteriesGroupCode.Regular;
		this.initialBetDataFactory = initBetDataFn;
	}

	// -----------------------------
	//  BaseGameService
	// -----------------------------
	/**
	 * Купить лотерею.
	 */
	buyLottery(): void {
		Logger.Log.i('KareLotteryService', 'buyLottery -> start buying Kare lottery')
			.console();

		const request = new KareRegBetReq(this.appStoreService, this.amount, this.betData);
		this.transactionService.buyLottery(KareRegBetResp, LotteryGameCode.Kare, request);
	}

	/**
	 * Обновить ставки.
	 */
	refreshBets(): void {
		let amount = 0;
		if (!!this.betData && !!this.betData.kareBetsData && Array.isArray(this.betData.kareBetsData.bets)) {
			this.betData.kareBetsData.bets
				.forEach(bet => {
					amount += bet.t === KareGameType.Table1_Cards
						? bet.s * bet.d
						: bet.s * bet.d * (bet as IKareBetItemGameType2).p.length;
				});
		}

		this.amount = amount / 100;
	}
}
