import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { URL_INIT, URL_REGISTRY, URL_RESULTS } from '@app/util/route-utils';

import { KareLotteryContentComponent } from '@app/kare/components/kare-lottery-content/kare-lottery-content.component';
import { KareLotteryInitComponent } from '@app/kare/components/kare-lottery-init/kare-lottery-init.component';
import { KareResultsComponent } from '@app/kare/components/kare-results/kare-results.component';
import { KareRegistryResolver } from '@app/kare/kare-registry-resolver';
import { CheckInformationComponent } from '../../features/check-information/check-information/check-information.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Каре".
 */
const routes: Routes = [
	{
		path: '',
		component: KareLotteryContentComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			{
				path: URL_INIT,
				component: KareLotteryInitComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_REGISTRY,
				component: CheckInformationComponent,
				resolve: {
					registry: KareRegistryResolver
				},
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			},
			{
				path: URL_RESULTS,
				component: KareResultsComponent,
				canActivate: [
					AuthGuard
				],
				canDeactivate: [
					NavigationGuard
				]
			}
		]
	}
];

/**
 * Модуль маршрутизации лотереи "Каре".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
		KareRegistryResolver
	]
})
export class KareRoutingModule {}
