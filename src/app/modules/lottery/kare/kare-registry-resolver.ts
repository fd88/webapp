import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { KareGameType, KareInputMode } from '@app/core/net/http/api/models/kare-reg-bet';
import { KareLotteryService } from '@app/kare/services/kare-lottery.service';
import { PARAM_ACTION_BET, PARAM_DEMO_BET } from '@app/util/route-utils';
import { BetDataSource, GameSaleMode } from '@app/core/game/base-game.service';
import { IRegistryResolveData } from '../../features/check-information/check-information/interfaces/iregistry-resolve-data';
import { ICheckInfoRow } from '../../features/check-information/check-information/interfaces/icheck-info-row';

/**
 * Резолвер данных для отображения информации о ручной ставке в "Каре".
 */
@Injectable()
export class KareRegistryResolver implements Resolve<IRegistryResolveData> {

	/**
	 * Конструктор резолвера.
	 *
	 * @param {KareLotteryService} kareLotteryService Сервис игры "Каре".
	 * @param {TranslateService} translateService Сервис для работы с мультиязычностью.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 */
	constructor(
		private readonly kareLotteryService: KareLotteryService,
		private readonly translateService: TranslateService,
		private readonly router: Router
	) {}

	/**
	 * Передает данные для отображения информации о ставке.
	 * @param route Текущий маршрут.
	 * @param state Состояние маршрута.
	 */
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IRegistryResolveData {
		// проверить наличие акционных параметров
		const parsedUrl = this.router.parseUrl(state.url);
		if (parsedUrl.queryParamMap.has(PARAM_ACTION_BET)) {
			// const actionBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_ACTION_BET)) as IActionParameters;
			this.kareLotteryService.initBetData(BetDataSource.Manual);
			this.kareLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			// ... TODO доделать акционные ставки
			this.kareLotteryService.refreshBets();
		}

		// проверить наличие демо-параметров
		if (parsedUrl.queryParamMap.has(PARAM_DEMO_BET)) {
			const demoBet = JSON.parse(parsedUrl.queryParamMap.get(PARAM_DEMO_BET));
			this.kareLotteryService.initBetData(BetDataSource.Manual);
			this.kareLotteryService.betData.saleMode = GameSaleMode.QuickSale;
			this.kareLotteryService.betData.drawCount = 1;
			this.kareLotteryService.betData.kareBetsData.bets = new Array(demoBet.data.betCount);
			this.kareLotteryService.betData.kareBetsData.bets.fill({
				a: true,
				i: KareInputMode.QuickPick,
				t: KareGameType.Table1_Cards,
				s: demoBet.data.betSum * 100,
				d: 1
			});
			this.kareLotteryService.refreshBets();
		}

		if (!this.kareLotteryService.activeDraws || !this.kareLotteryService.betData) {
			return {
				checkInfoRowList: [],
				baseGameService: this.kareLotteryService
			};
		}

		const table1 = this.kareLotteryService.betData.kareBetsData.bets.filter(f => f.t === KareGameType.Table1_Cards);
		const table2 = this.kareLotteryService.betData.kareBetsData.bets.filter(f => f.t === KareGameType.Table2_Poker);
		const c = this.translateService.instant('lottery.kare.cards');
		const p = this.translateService.instant('lottery.kare.combinations');
		let v1: string;
		let v2: string;
		let d1: string;
		let d2: string;

		if (table1.length > 0) {
			v1 = `${table1.length} (${c})`;
			d1 = `${table1[0].d} (${c})`;

			if (table2.length > 0) {
				v2 = `${table2.length} (${p})`;
				d2 = `${table2[0].d} (${p})`;
			}
		} else if (table2.length > 0) {
			v1 = `${table2.length} (${p})`;
			d1 = `${table2[0].d} (${p})`;
		}

		const checkInfoRowList: Array<ICheckInfoRow> = [
			{
				langKeyName: 'lottery.draws_count',
				value: d1
			},
			table1.length > 0 && table2.length > 0
				?
				{
					langKeyName: undefined,
					value: d2
				}
				: undefined,
			{
				langKeyName: 'lottery.kare.bets_count',
				value: v1
			},
			table1.length > 0 && table2.length > 0
				? {
					langKeyName: undefined,
					value: v2
				}
				: undefined,
			{
				langKeyName: 'lottery.sum',
				value: this.kareLotteryService.formattedAmount,
				redColor: true,
				valueIsCurrency: true
			}
		].filter(f => !!f);

		return {
			checkInfoRowList,
			baseGameService: this.kareLotteryService
		};
	}

}
