import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';

import {CoreModule} from '@app/core/core.module';
import {SharedModule} from '@app/shared/shared.module';
import {AuthService} from '@app/core/services/auth.service';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {AuthComponent} from '@app/auth/components/auth/auth.component';
import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {timer} from "rxjs";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../../../../app.module";
import {CommonModule} from "@angular/common";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {MslInputComponent} from "@app/shared/components/msl-input/msl-input.component";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {LogOutService} from "@app/logout/services/log-out.service";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {LogOutModule} from "@app/logout/log-out.module";
import {AuthRoutingModule} from "@app/auth/auth-routing.module";
import {TextMaskModule} from "angular2-text-mask";
import {InitService} from "@app/core/services/init.service";
import {StorageGetResp} from "@app/core/net/ws/api/models/storage/storage-get";
import {ErrorCode} from "@app/core/error/dialog";
import {NetError} from "@app/core/error/types";
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";

describe('AuthComponent', () => {
	let component: AuthComponent | any;
	let fixture: ComponentFixture<AuthComponent>;
	let translateService: TranslateService;
	let initService: InitService;
	let appStoreService: AppStoreService;
	let storageService: StorageService;
	let authService: AuthService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				FormsModule,
				ReactiveFormsModule,
				RouterTestingModule.withRoutes([]),
				CoreModule,
				HttpClientModule,
				SharedModule,
				LogOutModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				}),
				CommonModule,
				AuthRoutingModule,
				TextMaskModule
			],
			providers: [
				AuthService,
				LogService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},
				AppStoreService,
				TranslateService,
				FormBuilder,
				LogOutService,
				StorageService,
				InitService,
				HttpClient,
			],
			declarations: [
				AuthComponent,
				MslInputComponent,
				MslInputPinComponent
			]
		})
		.compileComponents();

		appStoreService = TestBed.inject(AppStoreService);
		initService = TestBed.inject(InitService);
		initService.init();
		await timer(500).toPromise();
		TestBed.inject(LogService);
		translateService = TestBed.inject(TranslateService);
		translateService.use('ru');
		storageService = TestBed.inject(StorageService);
		authService = TestBed.inject(AuthService);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AuthComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		component = fixture.componentInstance;
		// component.mslInputPinComponent = TestBed.createComponent(MslInputPinComponent).componentInstance as MslInputPinComponent;
		(component as any).mslInputPinComponent = {
			setFocus: () => {
			}
		};
		(component as any).inputComponents = {
			first: {
				setFocus: () => {
				}
			},
			last: {
				setFocus: () => {
				}
			}
		};
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('check enabled/disabled status for Login button', () => {
		// check for empty fields
		component.loginForm.setValue({login: '', password: ''});
		expect(component.loginForm.invalid).toBeTruthy();

		// check login for 13 characters
		component.loginForm.setValue({login: '1234567890123', password: '12345678'});
		expect(component.loginForm.invalid).toBeTruthy();

		// check password for 13 characters
		component.loginForm.setValue({login: '12345678', password: '1234567890123'});
		expect(component.loginForm.invalid).toBeTruthy();

		// check for empty password
		component.loginForm.setValue({login: '12345678', password: ''});
		expect(component.loginForm.invalid).toBeTruthy();

		// check for empty login
		component.loginForm.setValue({login: '', password: '12345678'});
		expect(component.loginForm.invalid).toBeTruthy();

		// all fields is correct
		component.loginForm.setValue({login: '12345678', password: '12345678'});
		expect(component.loginForm.valid).toBeTruthy();
	});

	it('Тест обработчика onActionHandler', () => {
		spyOn(component, 'onActionHandler').and.callThrough();
		component.onActionHandler();
		expect(component.onActionHandler).toHaveBeenCalled();
	});

	it('Тест обработчика onResendSMSHandler', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onResendSMSHandler').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		component.onResendSMSHandler();
		expect(component.onResendSMSHandler).toHaveBeenCalled();
	});

	it('Тест обработчика onResendSMSHandler 2', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onResendSMSHandler').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		authService.resendSMS = () => {
			return new Promise((resolve, reject) => {
				reject();
			})
		};
		component.onResendSMSHandler();
		expect(component.onResendSMSHandler).toHaveBeenCalled();
	});

	it('Тест обработчика onResendSMSHandler 3', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onResendSMSHandler').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		authService.resendSMS = () => {
			return new Promise((resolve, reject) => {
				reject(new NetError('Какая-то сетевая ошибка', 'Подробности этой ошибки', 123));
			})
		};
		component.onResendSMSHandler();
		expect(component.onResendSMSHandler).toHaveBeenCalled();
	});


	it('Тест обработчика onResendSMSHandler 4', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onResendSMSHandler').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		authService.resendSMS = () => {
			return new Promise((resolve, reject) => {
				resolve({
					client_trans_id: '12345',
					err_code: 123,
					err_descr: 'Не удалось отправить SMS'
				});
			})
		};
		component.onResendSMSHandler();
		expect(component.onResendSMSHandler).toHaveBeenCalled();
	});


	it('Тест обработчика onStepsSubmit', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onStepsSubmit').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		component.onResendSMSHandler();
		await timer(400).toPromise();
		component.onStepsSubmit();
		await timer(200).toPromise();
		expect(component.onStepsSubmit).toHaveBeenCalled();
	});

	xit('Тест обработчика onSelectTerminal', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onSelectTerminal').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		component.onResendSMSHandler();
		await timer(400).toPromise();
		component.onStepsSubmit();
		await timer(200).toPromise();
		const ev = new MouseEvent('click');
		const target = document.createElement('div');
		target.classList.add('terminal-item');
		target.setAttribute('data-term-index', '0');
		component.onSelectTerminal({...ev, target});
		await timer(400).toPromise();
		expect(component.onSelectTerminal).toHaveBeenCalled();
	});

	it('Тест обработчика onSelectTerminal 2', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onSelectTerminal').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		component.onResendSMSHandler();
		await timer(400).toPromise();
		component.onStepsSubmit();
		await timer(200).toPromise();
		const ev = new MouseEvent('click');
		const target = document.createElement('div');
		target.classList.add('terminal-item');
		target.setAttribute('data-term-index', '0');
		authService.setPos = () => {
			return new Promise((resolve, reject) => {
				reject();
			})
		};
		component.onSelectTerminal({...ev, target});
		await timer(400).toPromise();
		expect(component.onSelectTerminal).toHaveBeenCalled();
	});


	it('Тест обработчика onSelectTerminal 3', async () => {
		component.ngOnInit();
		await timer(200).toPromise();
		spyOn(component, 'onSelectTerminal').and.callThrough();
		component.onStepsSubmit();
		await timer(400).toPromise();
		component.phonesForm.controls['client_phone'].setValue('0681294504');
		component.onResendSMSHandler();
		await timer(400).toPromise();
		component.onStepsSubmit();
		await timer(200).toPromise();
		const ev = new MouseEvent('click');
		const target = document.createElement('div');
		target.classList.add('terminal-item');
		target.setAttribute('data-term-index', '0');
		authService.setPos = () => {
			return new Promise((resolve, reject) => {
				reject(new NetError('Какая-то сетевая ошибка', 'Подробности этой ошибки', 123));
			})
		};
		component.onSelectTerminal({...ev, target});
		await timer(400).toPromise();
		expect(component.onSelectTerminal).toHaveBeenCalled();
	});

	it('Тест геттера isConfirmDisabled', () => {
		component.authStep = 2;
		expect(component.isConfirmDisabled).toBeTruthy();
	});

	it('Тест геттера isConfirmDisabled 2', () => {
		component.authStep = 3;
		expect(component.isConfirmDisabled).toBeTruthy();
	});


	it('Тест обработчика ngOnInit', () => {
		// localStorage.setItem(ApplicationAppId, JSON.stringify(SESS_DATA));
		storageService.getSync = (appId: string, data: Array<string>): StorageGetResp => {
			return {
				requestId: '234',
				errorCode: 0,
				errorDesc: 'success',
				data: [
					{
						key: 'operator',
						value: '{"_userId": 10568, "_sessionId": "3b8b3032-87ed-444f-8290-9fd07abc9c43", "_operCode": "100605", "_access_level": 0}'
					},
					{
						key: 'init_TermCode',
						value: '40005'
					}
				]
			}
		};
		component.ngOnInit();
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('Тест обработчика onPinCodeChange', () => {
		component.onPinCodeChange();
		expect(appStoreService.smsCodeError).toBeFalsy();
	});

	it('Тест метода auth1Fail', () => {
		spyOn(component, 'auth1Fail').and.callThrough();
		component.auth1Fail({
			message: 'Ошибка авторизации',
			messageDetails: 'Описание ошибки авторизации'
		});
		expect(component.auth1Fail).toHaveBeenCalled();
	});

	it('Тест метода auth2Fail', () => {
		spyOn(component, 'auth2Fail').and.callThrough();
		component.auth2Fail({
			code: ErrorCode.Undefined,
			message: 'Ошибка авторизации',
			messageDetails: 'Описание ошибки авторизации'
		});
		expect(component.auth2Fail).toHaveBeenCalled();
	});

	it('Тест метода auth2Success', () => {
		spyOn(component, 'auth2Success').and.callThrough();
		component.auth2Success({
			err_code: 1,
			err_descr: 'Test error',
			user_id: '12345',
			sid: '1234567890',
			pos_list: [],
			client_trans_id: '123'
		});
		expect(component.auth2Success).toHaveBeenCalled();
	});

	it('Тест метода auth2Success 2', () => {
		spyOn(component, 'auth2Success').and.callThrough();
		component.auth2Success({
			err_code: 0,
			err_descr: '',
			user_id: '12345',
			sid: '1234567890',
			pos_list: [],
			client_trans_id: '123'
		});
		expect(component.auth2Success).toHaveBeenCalled();
	});

	it('Тест метода auth2Success 3', async () => {
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		spyOn(component, 'auth2Success').and.callThrough();
		component.auth2Success({
			err_code: 0,
			err_descr: '',
			user_id: '12345',
			sid: '1234567890',
			pos_list: [{
				pos: {
					client_id: 40004,
					access_level: 0,
					pos_code: 'kc2138',
					pos_name: 'ТОВ АйЛОТ, на Симеренка',
					pos_addr: 'Київ, вул. Симиренко, 36, оф. 30'
				}
			}],
			client_trans_id: '123'
		});
		await timer(4100).toPromise();
		expect(component.auth2Success).toHaveBeenCalled();
	});

});
