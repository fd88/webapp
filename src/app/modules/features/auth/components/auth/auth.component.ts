import { Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {interval, Subject, timer} from 'rxjs';
import { environment } from '@app/env/environment';
import { MslInputComponent } from '@app/shared/components/msl-input/msl-input.component';
import { AuthService, SMS_ATTEMPTS, SMS_TIMEOUT } from '@app/core/services/auth.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LogOutService } from '@app/logout/services/log-out.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { ApplicationAppId, AppType } from '@app/core/services/store/settings';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { IError, NetError } from '@app/core/error/types';
import { ResendAuth2Resp } from '@app/core/net/http/api/models/resend-auth-2';
import { Operator } from '@app/core/services/store/operator';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { BOAuth2Resp } from '@app/core/net/http/api/models/bo-auth-2';
import { AccessPosListItemGS, ClientPhoneItemAS, SavedOperator } from '@app/core/net/http/api/types';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { StorageKeys } from '@app/core/net/ws/api/models/storage/storage-models';
import { StorageGetResp } from '@app/core/net/ws/api/models/storage/storage-get';
import { MslInputPinComponent } from '@app/shared/components/msl-input-pin/msl-input-pin.component';
import { BOGetClientListResp } from '@app/core/net/http/api/models/bo-get-client-list';
import { TranslateService } from '@ngx-translate/core';

/**
 * Шаги авторизации
 */
enum AuthStep {
	/**
	 * Шаг 1
	 */
	Step1 = 1,
	/**
	 * Шаг 2
	 */
	Step2 = 2,
	/**
	 * Шаг 3
	 */
	Step3 = 3,
	/**
	 * Шаг 4
	 */
	Step4 = 4
}

/**
 * Компонент авторизации пользователя на терминале.
 */
@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Компоненты для ввода логина и пароля
	 */
	@ViewChildren(MslInputComponent)
	inputComponents: QueryList<MslInputComponent>;

	/**
	 * Компонент ввода СМС-кода
	 */
	@ViewChild('smsControl')
	mslInputPinComponent: MslInputPinComponent;

	/**
	 * Максимальная длина ввода
	 */
	readonly maxInputLength = 12;

	/**
	 * Тип приложения
	 */
	readonly AppType = AppType;

	/**
	 * Шаги авторизации
	 */
	readonly AuthStep = AuthStep;

	/**
	 * Форма логина
	 */
	loginForm: FormGroup;

	/**
	 * Версия ПО
	 */
	buildVersion: string;

	/**
	 * Статус готовности сети
	 */
	communicationReady = true;

	/**
	 * Этап авторизации
	 */
	authStep = AuthStep.Step1;

	/**
	 * Форма ввода кода из СМС
	 */
	smsForm: FormGroup;

	/**
	 * Форма выбора номера телефона
	 */
	phonesForm: FormGroup;

	/**
	 * Оставшееся количество попыток ввода СМС + 1 (плюс попытка сразу после аутентификации)
	 */
	smsRetryCount = SMS_ATTEMPTS + 1;

	/**
	 * Оставшееся время до разблокировки кнопки
	 */
	smsUnlockTimeout = SMS_TIMEOUT;

	/**
	 * Список точек продаж
	 */
	posList: Array<AccessPosListItemGS> = null;

	/**
	 * Выбранная точка из списка
	 */
	termIndex = -1;

	/**
	 * Маска для ввода СМС-кода
	 */
	textMask = [/\d/, /\d/, /\d/, /\d/];

	/**
	 * Список номеров телефонов, по которым можно входить в систему
	 */
	clientList: Array<ClientPhoneItemAS> = [];

	/**
	 * Деактивирована ли кнопка подтверждения входа?
	 */
	get isConfirmDisabled(): boolean {
		if (this.authStep === AuthStep.Step1) {
			return this.loginForm && this.loginForm.invalid;
		}

		if (this.authStep === AuthStep.Step2) {
			return true;
		}

		if (this.authStep === AuthStep.Step3) {
			return (this.smsForm && this.smsForm.invalid) || this.appStoreService.smsCodeError;
		}

		return this.termIndex === -1;
	}

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища данных приложения
	 * @param {AuthService} authService Сервис авторизации
	 * @param {FormBuilder} formBuilder Сервис построения форм
	 * @param {LogOutService} logOutService Сервис выхода из системы
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон
	 * @param {StorageService} storageService Сервис хранилища данных в браузере
	 * @param translateService Сервис перевода
	 */
	constructor(
		readonly appStoreService: AppStoreService,
		private readonly authService: AuthService,
		private readonly formBuilder: FormBuilder,
		private readonly logOutService: LogOutService,
		private readonly dialogInfoService: DialogContainerService,
		private readonly storageService: StorageService,
		private readonly translateService: TranslateService
	) {}

	/**
	 * Выполнить submit на форме при соответствующих действиях пользователя.
	 * По клику на невалидной форме и заполненным полем "логин" перевести фокус на поле "пароль".
	 */
	onActionHandler(): void {
		this.doStepsActions();
	}

	/**
	 * Слушатель отправки первой формы
	 */
	onStepsSubmit(): void {
		this.doStepsActions();
	}

	/**
	 * Слушатель повторной отправки СМС
	 */
	onResendSMSHandler(): void {
		this.authStep = AuthStep.Step3;
		this.appStoreService.Settings.clientID = this.phonesForm.controls['client_phone'].value;
		// повторить отправку СМС
		this.authService.resendSMS()
			.then((resendAuth2Resp: ResendAuth2Resp) => {
				if (resendAuth2Resp.err_code === 0) {
					this.dialogInfoService.hideAll();
					if (environment.mockData) {
						const tmr = timer(200)
							.subscribe(() => {
								this.smsForm.patchValue({
									smscode: environment.sms
								});
								tmr.unsubscribe();
							});
					}
				} else {
					const msg = new DialogError(ErrorCode.OperAuth, resendAuth2Resp.err_descr);
					Logger.Log.e('AuthService', `resendSMS -> ERROR: %s`, msg)
						.console();

					this.dialogInfoService.showOneButtonError(msg, {
						click: () => {},
						text: 'dialog.dialog_button_continue'
					});
				}
			})
			.finally(() => {
				this.waitForResendUnlock();
				this.appStoreService.smsCodeError = false;
				this.smsForm.reset();
				this.pinCodeSetFocus();
			})
			.catch(error => {
				const msg = (error instanceof NetError)
					? { code: error.code, message: error.message, messageDetails: error.messageDetails }
					: new DialogError(ErrorCode.OperAuth, 'dialog.oper_auth_error');

				Logger.Log.e('AuthService', `resendSMS -> ERROR: %s`, msg)
					.console();

				this.dialogInfoService.showOneButtonError(msg, {
					click: () => {},
					text: 'dialog.dialog_button_continue'
				});
			});
	}

	/**
	 * Слушатель изменения поля СМС-кода
	 */
	onPinCodeChange(): void {
		this.appStoreService.smsCodeError = false;
	}

	/**
	 * Событие выбора терминала
	 * @param event Передаваемое событие
	 */
	onSelectTerminal(event: MouseEvent): void {
		const elem: HTMLDivElement = (event.target as HTMLElement).closest('.terminal-item');
		if (!!elem && !!elem.dataset.termIndex) {
			this.termIndex = Number(elem.dataset.termIndex);
			this.appStoreService.Settings.termCode = this.posList[this.termIndex].pos.client_id.toString();
			this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.authorization_wait', {top: 50});
			this.setOperator();
			this.authService.setPos()
				.then(() => {
					this.loginForm.reset();
					this.storageService.putSync(ApplicationAppId, [{
						key: StorageKeys.TermCodeKey,
						value: this.appStoreService.Settings.termCode
					}]);
					this.authService.auth3();
				})
				.finally(() => {
					this.authStep = AuthStep.Step1;
					this.termIndex = -1;
				})
				.catch(error => {
					const msg = (error instanceof NetError)
						? { code: error.code, message: error.message, messageDetails: error.messageDetails }
						: new DialogError(ErrorCode.OperAuth, 'dialog.oper_auth_error');

					Logger.Log.e('AuthService', `SetPos -> ERROR: %s`, msg)
						.console();

					this.dialogInfoService.showOneButtonError(msg, {
						click: () => this.authService.logoutOperator(),
						text: 'dialog.dialog_button_continue'
					});
				});
		}
	}

	/**
	 * Обработчик события фокуса на поле ввода СМС-кода
	 */
	triggerScrollTo(): void {
		// authform_step_2'
		// scrollIntoView
	}

	/**
	 * Функция для отслеживания изменений в списке терминалов
	 * @param index Индекс элемента
	 * @param item Элемент списка
	 */
	trackByField = (index, item: AccessPosListItemGS) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Инициализация формы аутентификации.
	 */
	private initForm(): void {
		this.loginForm = this.formBuilder.group({
			login: [environment.login, [Validators.required, Validators.maxLength(this.maxInputLength)]],
			password: [environment.password, [Validators.required, Validators.maxLength(this.maxInputLength)]]
		});
	}

	/**
	 * Выполнить авторизацию оператора на терминале, согласно введенным данным.
	 */
	private submit(): void {
		this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.authorization_wait', {top: 50});
		this.authService.auth2(this.smsForm.controls['smscode'].value)
			.then(this.auth2Success.bind(this))
			.catch(this.auth2Fail.bind(this));
	}

	/**
	 * Успешная аутентификация на первом шаге
	 * @param response Ответ сервера
	 */
	private auth1Success(response: BOGetClientListResp): void {
		// Logger.Log.i('AuthService', `loginOperator -> OK, access level from response: ${response.access_level}`)
		// 	.console();

		this.authService.loginOperator_val = this.loginForm.controls['login'].value;
		// this.appStoreService.operator.next(
		// 	new Operator(response.user_id,
		// 		response.sid,
		// 		this.loginForm.controls['login'].value,
		// 		response.access_level)
		// );
		this.dialogInfoService.hideAll();
		// this.smsForm.reset();
		this.authStep = AuthStep.Step2;
		this.clientList = response.client_list ? response.client_list : [];
		// this.pinCodeSetFocus();
		// this.waitForResendUnlock();
	}

	/**
	 * Неудачная аутентификация на первом шаге
	 * @param error Ошибка
	 */
	private auth1Fail(error: NetError | IError): void {
		const msg = (error instanceof NetError)
			? { code: error.code, message: error.message, messageDetails: error.messageDetails }
			: new DialogError(ErrorCode.OperAuth, error.message, error.messageDetails);

		Logger.Log.e('AuthService', `auth1 -> ERROR: %s`, msg)
			.console();

		this.dialogInfoService.showOneButtonError(msg, {
			click: () => this.authService.logoutOperator(),
			text: 'dialog.dialog_button_continue'
		});

		this.loginForm.reset();
		this.inputComponents.first.setFocus();
	}

	/**
	 * Успешная авторизация на втором шаге
	 * @param boAuth2Response Ответ сервера
	 */
	private auth2Success(boAuth2Response: BOAuth2Resp): void  {
		this.appStoreService.smsCodeError = boAuth2Response.err_code !== 0;
		if (this.appStoreService.smsCodeError) {
			Logger.Log.e('AuthService', `loginContinue -> ERROR: %s`, boAuth2Response)
				.console();

			const msg =  new DialogError(ErrorCode.OperAuth, boAuth2Response.err_descr);

			this.dialogInfoService.showOneButtonError(msg, {
				click: () => {},
				text: 'dialog.dialog_button_continue'
			});
		} else {
			this.smsForm.reset();
			this.authStep = AuthStep.Step4;
			this.dialogInfoService.hideAll();
			// del
			// delete boAuth2Response.pos_list;
			// boAuth2Response.pos_list = [boAuth2Response.pos_list[0]];
			// del
			this.posList = boAuth2Response.pos_list ? boAuth2Response.pos_list : [];
			// this.hideAndroidKeyboard();
			if (this.posList.length === 1) {
				this.termIndex = 0;
				this.appStoreService.Settings.termCode = this.posList[this.termIndex].pos.client_id.toString();
				this.setOperator();
				const tmr = timer(4000)
					.subscribe(() => {
						tmr.unsubscribe();
						this.storageService.putSync(ApplicationAppId, [{
							key: StorageKeys.TermCodeKey,
							value: this.appStoreService.Settings.termCode
						}]);
						this.authService.auth3();
					});
			} else if (this.posList.length === 0) {
				this.phonesForm.controls['client_phone'].setValue(null);
				this.authStep = AuthStep.Step1;
			}
		}
	}

	/**
	 * Неудачная авторизация на втором шаге
	 * @param error Ошибка
	 */
	private auth2Fail(error: NetError | IError): void {
		this.dialogInfoService.hideAll();

		Logger.Log.e('AuthService', `loginContinue -> ERROR: %s`, error)
			.console();

		if (error.code === ErrorCode.IncorrectSMS) {
			this.appStoreService.smsCodeError = true;
		} else {
			const msg = (error instanceof NetError)
				? { code: error.code, message: error.message, messageDetails: error.messageDetails }
				: new DialogError(ErrorCode.OperAuth, error.message, error.messageDetails);

			Logger.Log.e('AuthService', `auth1 -> ERROR: %s`, msg)
				.console();

			this.dialogInfoService.showOneButtonError(msg, {
				click: () => {},
				text: 'dialog.dialog_button_continue'
			});
		}

	}

	/**
	 * Ждем, пока не разблокируется кнопка повтора СМС
	 */
	private waitForResendUnlock(): void {
		if (this.smsRetryCount > 0) {
			this.smsRetryCount--;
			this.smsUnlockTimeout = SMS_TIMEOUT;
			const tmr = timer(SMS_TIMEOUT)
				.subscribe(() => {
					this.smsUnlockTimeout = 0;
					tmr.unsubscribe();
				});
		}
	}

	/**
	 * Установить фокус на вводе пин-кода
	 */
	private pinCodeSetFocus(): void {
		const tmr = interval(300)
			.subscribe(() => {
				if (!!this.mslInputPinComponent) {
					this.mslInputPinComponent.setFocus();
					tmr.unsubscribe();
				}
			});
	}

	/**
	 * Обработчик отправки данных на разных шагах
	 */
	private doStepsActions(): void {
		if (this.authStep === AuthStep.Step1) {
			if (this.loginForm.valid) {
				this.smsForm.reset();

				this.authService.auth1(this.loginForm.controls['login'].value, this.loginForm.controls['password'].value)
					.then(this.auth1Success.bind(this))
					.catch(this.auth1Fail.bind(this));
			} else {
				if (!!this.loginForm.controls.login.value) {
					this.inputComponents.last.setFocus();
				}
			}
		} else if (this.authStep === AuthStep.Step2) {
			this.onResendSMSHandler();
		} else if (this.authStep === AuthStep.Step3) {
			if (this.smsForm.valid) {
				this.submit();
			}
		}
	}

	/**
	 * Установить оператора
	 * @private
	 */
	private setOperator(): void {
		const op = new Operator(
			this.appStoreService.operator.value.userId,
			this.appStoreService.operator.value.sessionId,
			this.authService.loginOperator_val,
			this.posList[this.termIndex].pos.access_level);
		this.appStoreService.operator.next(op);
		this.storageService.putSync(ApplicationAppId, [{
			key: StorageKeys.Operator,
			value: JSON.stringify(op)
		}]);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.appStoreService.smsCodeError = false;
		this.buildVersion = environment.version;
		this.initForm();

		this.phonesForm = this.formBuilder.group({
			client_phone: ['', [Validators.required]]
		});

		this.smsForm = this.formBuilder.group({
			smscode: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]]
		});

		// Пытаемся получить версию, оператора и код терминала из localStorage
		const sessionData: StorageGetResp = this.storageService.getSync(
			ApplicationAppId,
			[StorageKeys.Operator, StorageKeys.TermCodeKey]
		);

		// если у на всё есть, то можно считать, что мы залогинены
		if (!!sessionData.data[0] && (sessionData.data[0].key === StorageKeys.Operator)
			&& !!sessionData.data[1] && (sessionData.data[1].key === StorageKeys.TermCodeKey)) {
			const savedOp: SavedOperator = JSON.parse(sessionData.data[0].value);
			this.appStoreService.operator.next(new Operator(
				savedOp._userId, savedOp._sessionId, savedOp._operCode, savedOp._access_level
			));
			this.appStoreService.Settings.termCode = sessionData.data[1].value;
			this.appStoreService.isLoggedIn$$.next(true);
			this.authService.auth3();
		}
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		if (!!this.unsubscribe$$) {
			this.unsubscribe$$.next();
			this.unsubscribe$$.complete();
		}
	}

}
