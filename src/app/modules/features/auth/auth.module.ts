import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@app/shared/shared.module';

import { AuthRoutingModule } from '@app/auth/auth-routing.module';
import { AuthComponent } from '@app/auth/components/auth/auth.component';
import { TextMaskModule } from 'angular2-text-mask';

/**
 * Модуль авторизации.
 */
@NgModule({
	imports: [
		CommonModule,
		AuthRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule,
		TextMaskModule
	],
	declarations: [
		AuthComponent
	]
})
export class AuthModule {}
