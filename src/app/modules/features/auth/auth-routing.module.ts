import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля авторизации.
 */
const routes: Routes = [
	{
		path: '',
		component: AuthComponent,
		children: [],
		canDeactivate: [
			NavigationGuard
		]
	}
];

/**
 * Модуль маршрутизации для функционала авторизации.
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
	]
})
export class AuthRoutingModule {}
