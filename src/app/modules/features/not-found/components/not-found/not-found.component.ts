import { Component } from '@angular/core';
import { Location } from '@angular/common';

/**
 * Компонент "Страница не найдена".
 */
@Component({
	selector: 'app-not-found',
	templateUrl: './not-found.component.html',
	styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent  {

	/**
	 * Конструктор компонента.
	 * @param location Сервис для работы с адресной строкой.
	 */
	constructor(
		private readonly location: Location
	) {}

	/**
	 * Обработчик нажатия на кнопку "Назад".
	 */
	onClickBackHandler(): void {
		this.location.back();
	}
}
