import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля игры "Страница не найдена".
 */
const routes: Routes = [
	{
		path: '',
		component: NotFoundComponent,
		children: [
		],
		canDeactivate: [
			NavigationGuard
		]
	}
];

/**
 * Модуль "Страница не найдена".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
	]
})
export class NotFoundRoutingModule {}
