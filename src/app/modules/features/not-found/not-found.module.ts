import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotFoundComponent } from '@app/not-found/components/not-found/not-found.component';
import { NotFoundRoutingModule } from '@app/not-found/not-found-routing.module';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
	imports: [
		CommonModule,
		NotFoundRoutingModule,
		SharedModule
	],
	declarations: [
		NotFoundComponent
	]
})
export class NotFoundModule {}
