import {UpdateDrawInfoLottery} from "@app/core/net/http/api/models/update-draw-info";

/**
 * Mock-тиражи для электронных моментальных лотерей.
 */
export const DRAWS_FOR_INSTANT: UpdateDrawInfoLottery | any = {
	'lottery': {
		"lott_extra": {"group_id":"1"},
		"lott_name": "Святкова",
		"lott_code":"107",
		"currency":"",
		"draws":[{"draw":{"bet_sum":"10.00","code":"2319","data":{"bet_sum_max":"22500.00","bet_sum_min":"100.00","max_win_sum":"20000.00"},"dr_bdate":"2019-09-12T22:01:00","dr_edate":"2026-08-22T23:59:59","num":"17","sale_bdate":"2019-09-12T22:00:00","sale_edate":"2026-08-22T23:59:59","serie_code":"685","serie_num":"До Дня Незалежності","status":"Продажа и выплата выигрышей","status_code":"SALES AND PAYS","version":"16","win_bdate":"2019-09-12T22:00:00","win_edate":"2026-08-22T23:59:59"}}]
	}
};
