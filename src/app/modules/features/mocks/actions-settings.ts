import {IActionsSettings} from "@app/actions/interfaces/iactions-settings";

/**
 * Mock-настройки для акций.
 */
export const ACTIONS_SETTINGS: IActionsSettings = {
	"bannersList": [
		{
			"buttons": [
				{
					"imgUrl": "assets/img/instant/actions/banner_labirynt.jpg",
					"gameCode": 157,
					"groupCode": 1,
					"data": {
						"betCount": 1
					}
				}
			]
		}
	],
	"actionsList": [
		{
			"buttons": [
				{
					"labelKey": "actions.zabava_ticket",
					"gameCode": 3,
					"groupCode": 0,
					"data": {
						"summ": 20,
						"drawCount": 1,
						"betCount": 1,
						"pairs": 0
					}
				},
				{
					"labelKey": "actions.zabava_tickets_pair",
					"gameCode": 3,
					"groupCode": 0,
					"data": {
						"summ": 50,
						"drawCount": 1,
						"betCount": 2,
						"pairs": 1
					}
				}
			]
		},

		{
			"buttons": [
				{
					"labelKey": "actions.megalot_bet_megaball",
					"gameCode": 1,
					"groupCode": 0,
					"data": {
						"summ": 20,
						"drawCount": 1,
						"betCount": 4,
						"ballCount": 6,
						"megaBallCount": 1
					}
				},
				{
					"labelKey": "actions.megalot_bets_megaball",
					"gameCode": 1,
					"groupCode": 0,
					"data": {
						"summ": 50,
						"drawCount": 1,
						"betCount": 10,
						"ballCount": 6,
						"megaBallCount": 1
					}
				}
			]
		},

		{
			"buttons": [
				{
					"labelKey": "actions.tiptop_tip_tickets",
					"gameCode": 71,
					"groupCode": 0,
					"data": {
						"summ": 20,
						"betCount": 10
					}
				},
				{
					"labelKey": "actions.tiptop_top_tickets",
					"gameCode": 72,
					"groupCode": 0,
					"data": {
						"summ": 50,
						"betCount": 10
					}
				}
			]
		},

		{
			"buttons": [
				{
					"labelKey": "4 шт, “Весела скарбничка”",
					"gameCode": 140,
					"groupCode": 1,
					"data": {
						"summ": 20,
						"betCount": 4
					}
				},
				{
					"labelKey": "5 шт, “Морський бій”",
					"gameCode": 107,
					"groupCode": 1,
					"data": {
						"summ": 50,
						"betCount": 5
					}
				}
			]
		},

		{
			"buttons": [
				{
					"labelKey": "2 шт, “Товстий гаманець”",
					"gameCode": 124,
					"groupCode": 1,
					"data": {
						"summ": 20,
						"betCount": 2
					}
				},
				{
					"labelKey": "2 шт, “Максі лото”",
					"gameCode": 134,
					"groupCode": 1,
					"data": {
						"summ": 50,
						"betCount": 2
					}
				}
			]
		}
	]
};
