import {UpdateDrawInfoLottery} from "@app/core/net/http/api/models/update-draw-info";

/**
 * Mock-тиражи для бумажных моментальных лотерей.
 */
export const DRAWS_FOR_GAME_100: UpdateDrawInfoLottery | any = {
	'lott_name': 'Моментальна',
	'lott_code': '100',
	'currency': '',
	'draws': [{
		'draw': {
			'bet_sum': '200.00',
			'code': '1581',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2007-10-29T11:56:10',
			'dr_edate': '2007-10-29T11:56:10',
			'inst_lott_name': 'Test',
			'num': 'HubaBuba1',
			'sale_bdate': '2007-10-01T10:00:00',
			'sale_edate': '2007-11-30T11:00:00',
			'serie_code': '47',
			'serie_name': 'Test',
			'serie_num': 'Test',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '3',
			'win_bdate': '2007-10-01T10:00:00',
			'win_edate': '2007-11-30T11:30:00'
		}
	}, {
		'draw': {
			'bet_sum': '2.00',
			'code': '1003',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2005-10-11T12:17:56',
			'dr_edate': '2005-10-11T12:17:56',
			'inst_lott_name': 'Амур',
			'num': 'ГРАН-ПРІ, серия 1',
			'sale_bdate': '2005-10-12T00:00:00',
			'sale_edate': '2007-12-30T00:00:00',
			'serie_code': '7',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '10',
			'win_bdate': '2005-10-12T00:00:00',
			'win_edate': '2008-02-28T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '2.00',
			'code': '1001',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2005-10-11T11:09:06',
			'dr_edate': '2005-10-11T11:09:06',
			'inst_lott_name': 'Доміно',
			'num': 'Доміно, серия 1',
			'sale_bdate': '2005-10-12T00:00:00',
			'sale_edate': '2007-12-31T00:00:00',
			'serie_code': '9',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '7',
			'win_bdate': '2005-10-12T00:00:00',
			'win_edate': '2008-02-28T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '2.00',
			'code': '1132',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2006-03-26T13:07:39',
			'dr_edate': '2006-03-26T13:07:39',
			'inst_lott_name': 'Супер казино',
			'num': 'Гол!',
			'sale_bdate': '2006-04-03T00:00:00',
			'sale_edate': '2007-12-31T00:00:00',
			'serie_code': '13',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '10',
			'win_bdate': '2006-04-03T00:00:00',
			'win_edate': '2008-02-28T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '2.00',
			'code': '1246',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2006-08-16T15:13:13',
			'dr_edate': '2006-08-16T15:13:13',
			'inst_lott_name': 'Щасливий номер',
			'num': 'Щасливий номер',
			'sale_bdate': '2006-09-01T00:00:00',
			'sale_edate': '2007-12-31T00:00:00',
			'serie_code': '14',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '8',
			'win_bdate': '2006-09-01T00:00:00',
			'win_edate': '2008-02-28T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '1.00',
			'code': '1671',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2008-09-29T12:45:51',
			'dr_edate': '2008-09-29T12:45:51',
			'inst_lott_name': 'test',
			'num': '45425',
			'sale_bdate': '2008-10-29T12:41:00',
			'sale_edate': '2008-10-31T12:41:00',
			'serie_code': '34',
			'serie_name': '45425',
			'serie_num': '45425',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2008-10-29T12:41:00',
			'win_edate': '2008-11-03T12:41:00'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '1002',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2005-10-11T11:22:41',
			'dr_edate': '2005-10-11T11:22:41',
			'inst_lott_name': 'Морський бій.',
			'num': 'Морський бій, серия 1',
			'sale_bdate': '2005-10-12T00:00:00',
			'sale_edate': '2008-12-31T00:00:00',
			'serie_code': '10',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '9',
			'win_bdate': '2005-10-12T00:00:00',
			'win_edate': '2009-02-28T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '1654',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2008-05-21T10:14:13',
			'dr_edate': '2008-05-21T10:14:13',
			'inst_lott_name': 'Авто-лото',
			'num': '33',
			'sale_bdate': '2008-06-21T09:29:00',
			'sale_edate': '2009-06-21T09:29:00',
			'serie_code': '33',
			'serie_name': '33',
			'serie_num': '33',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '4',
			'win_bdate': '2008-06-21T09:29:00',
			'win_edate': '2010-05-21T10:50:00'
		}
	}, {
		'draw': {
			'bet_sum': '0.50',
			'code': '1674',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2008-09-30T15:59:25',
			'dr_edate': '2008-09-30T15:59:25',
			'inst_lott_name': 'New momemt Lottery',
			'num': '23',
			'sale_bdate': '2008-09-03T15:59:48',
			'sale_edate': '2009-09-26T15:59:54',
			'serie_code': '23',
			'serie_name': '23',
			'serie_num': '23',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '3',
			'win_bdate': '2008-09-03T15:59:48',
			'win_edate': '2009-10-14T16:00:02'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '907',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2005-05-26T10:27:44',
			'dr_edate': '2005-05-26T10:27:44',
			'inst_lott_name': 'Навколо світу',
			'num': '1',
			'sale_bdate': '2005-05-28T00:00:00',
			'sale_edate': '2012-12-30T00:00:00',
			'serie_code': '301',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '16',
			'win_bdate': '2005-05-28T00:00:00',
			'win_edate': '2013-02-23T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '2.00',
			'code': '640',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2004-05-25T14:15:51',
			'dr_edate': '2004-05-25T14:15:51',
			'inst_lott_name': 'Снайпер',
			'num': '01',
			'sale_bdate': '2004-05-27T00:00:00',
			'sale_edate': '2012-12-31T23:55:00',
			'serie_code': '6',
			'serie_name': '1',
			'serie_num': '1',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '9',
			'win_bdate': '2004-05-27T00:00:00',
			'win_edate': '2013-02-23T23:55:00'
		}
	}, {
		'draw': {
			'bet_sum': '10.00',
			'code': '61668',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2013-06-12T15:09:13',
			'dr_edate': '2013-06-12T15:09:13',
			'inst_lott_name': 'Вибори 2015',
			'num': 'Вибори 2015 (016)',
			'sale_bdate': '2013-06-12T00:00:00',
			'sale_edate': '2015-06-30T00:00:00',
			'serie_code': '16',
			'serie_name': 'Вибори 2015 (003)',
			'serie_num': 'Вибори 2015 (003)',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2013-06-12T00:00:00',
			'win_edate': '2015-12-31T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '10.00',
			'code': '61669',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2013-06-12T17:20:05',
			'dr_edate': '2013-06-12T17:20:05',
			'inst_lott_name': 'Вибори 2015',
			'num': 'Вибори 2015 (017)',
			'sale_bdate': '2013-06-12T00:00:00',
			'sale_edate': '2015-06-30T00:00:00',
			'serie_code': '17',
			'serie_name': 'Вибори 2015 (004)',
			'serie_num': 'Вибори 2015 (004)',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2013-06-12T00:00:00',
			'win_edate': '2015-12-31T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '10.00',
			'code': '61670',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2013-06-13T14:01:34',
			'dr_edate': '2013-06-13T14:01:34',
			'inst_lott_name': 'Вибори 2015',
			'num': 'Вибори 2015 (005)',
			'sale_bdate': '2013-06-12T00:00:00',
			'sale_edate': '2015-06-30T00:00:00',
			'serie_code': '20',
			'serie_name': 'Вибори 2015 (005)',
			'serie_num': 'Вибори 2015 (005)',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2013-06-12T00:00:00',
			'win_edate': '2015-12-31T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '10.00',
			'code': '61666',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2013-06-12T14:32:10',
			'dr_edate': '2013-06-12T14:32:10',
			'inst_lott_name': 'Вибори 2015',
			'num': 'Вибори 2015 (988)',
			'sale_bdate': '2013-06-12T00:00:00',
			'sale_edate': '2015-06-30T00:00:00',
			'serie_code': '988',
			'serie_name': 'Вибори 2015 (001)',
			'serie_num': 'Вибори 2015 (001)',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '2',
			'win_bdate': '2013-06-12T00:00:00',
			'win_edate': '2015-12-31T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '10.00',
			'code': '61667',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2013-06-12T14:48:05',
			'dr_edate': '2013-06-12T14:48:05',
			'inst_lott_name': 'Вибори 2015',
			'num': 'Вибори 2015 (989)',
			'sale_bdate': '2013-06-12T00:00:00',
			'sale_edate': '2015-06-30T00:00:00',
			'serie_code': '989',
			'serie_name': 'Вибори 2015 (002)',
			'serie_num': 'Вибори 2015 (002)',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2013-06-12T00:00:00',
			'win_edate': '2015-12-31T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '61582',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2007-10-18T14:39:02',
			'dr_edate': '2017-03-16T16:27:42',
			'inst_lott_name': 'HubaBuba ',
			'num': 'Huba',
			'sale_bdate': '2007-10-10T14:41:00',
			'sale_edate': '2017-12-06T16:27:42',
			'serie_code': '44',
			'serie_name': 'Huba',
			'serie_num': 'Huba',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '10',
			'win_bdate': '2007-10-10T14:41:00',
			'win_edate': '2017-03-16T16:27:42'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '61583',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2007-10-18T14:51:10',
			'dr_edate': '2017-03-16T16:27:42',
			'inst_lott_name': 'HubaBuba ',
			'num': 'Buba',
			'sale_bdate': '2007-09-12T14:53:00',
			'sale_edate': '2017-12-06T16:27:42',
			'serie_code': '45',
			'serie_name': 'Buba',
			'serie_num': 'Buba',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '9',
			'win_bdate': '2007-09-12T14:53:00',
			'win_edate': '2017-03-16T16:27:42'
		}
	}, {
		'draw': {
			'bet_sum': '10.00',
			'code': '1458',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2018-09-10T19:56:27',
			'dr_edate': '2018-09-10T19:56:27',
			'inst_lott_name': 'Справжнє золото',
			'num': '11',
			'sale_bdate': '2018-09-10T19:09:00',
			'sale_edate': '2019-11-14T19:09:42',
			'serie_code': '636',
			'serie_name': '11',
			'serie_num': '11',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2018-09-10T19:09:00',
			'win_edate': '2019-10-17T19:09:34'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '1435',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2018-05-31T19:06:03',
			'dr_edate': '2018-05-31T19:06:03',
			'inst_lott_name': 'Щаслива 13 ',
			'num': 'карпати',
			'sale_bdate': '2018-05-31T20:04:10',
			'sale_edate': '2020-01-28T19:04:00',
			'serie_code': '7777',
			'serie_name': 'карпати',
			'serie_num': 'карпати',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '25',
			'win_bdate': '2018-05-31T20:04:10',
			'win_edate': '2020-02-28T19:04:00'
		}
	}, {
		'draw': {
			'bet_sum': '5.00',
			'code': '62008',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2015-05-21T14:52:21',
			'dr_edate': '2015-05-21T14:52:21',
			'inst_lott_name': 'Test',
			'num': 'Test997',
			'sale_bdate': '2015-05-20T00:00:00',
			'sale_edate': '2020-05-31T00:00:00',
			'serie_code': '997',
			'serie_name': 'Test997',
			'serie_num': 'Test997',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '20',
			'win_bdate': '2015-05-20T00:00:00',
			'win_edate': '2020-05-31T00:00:00'
		}
	}, {
		'draw': {
			'bet_sum': '19.65',
			'code': '1445',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2018-07-11T17:11:01',
			'dr_edate': '2018-07-11T17:11:01',
			'inst_lott_name': 'Твій сейф',
			'num': 'Серія 11',
			'sale_bdate': '2018-07-11T17:11:10',
			'sale_edate': '2026-01-31T17:09:00',
			'serie_code': '663',
			'serie_name': 'Серія 11',
			'serie_num': 'Серія 11',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '5',
			'win_bdate': '2018-07-11T17:11:10',
			'win_edate': '2026-08-22T17:09:47'
		}
	}, {
		'draw': {
			'bet_sum': '9.80',
			'code': '1625',
			'data': {
				'bet_sum_max': '20000.00',
				'bet_sum_min': '100.00'
			},
			'dr_bdate': '2019-06-11T13:56:09',
			'dr_edate': '2019-06-11T13:56:09',
			'inst_lott_name': 'Справжнє золото',
			'num': 'Серія 12',
			'sale_bdate': '2019-06-11T13:54:37',
			'sale_edate': '2029-06-13T13:54:39',
			'serie_code': '679',
			'serie_name': 'Серія 12',
			'serie_num': 'Серія 12',
			'status': 'Продажа и выплата выигрышей',
			'status_code': 'SALES AND PAYS',
			'version': '3',
			'win_bdate': '2019-06-11T13:54:37',
			'win_edate': '2026-06-11T13:54:45'
		}
	},
		{
			'draw': {
				'bet_sum': '200.00',
				'code': '12345',
				'data': {
					'bet_sum_max': '20000.00',
					'bet_sum_min': '100.00'
				},
				'dr_bdate': '2007-10-29T11:56:10',
				'dr_edate': '2007-10-29T11:56:10',
				'inst_lott_name': 'Test',
				'num': 'HubaBuba1',
				'sale_bdate': '2007-10-01T10:00:00',
				'sale_edate': '2007-11-30T11:00:00',
				'serie_code': '662',
				'serie_name': 'Test',
				'serie_num': 'Test',
				'status': 'Продажа и выплата выигрышей',
				'status_code': 'SALES AND PAYS',
				'version': '3',
				'win_bdate': '2007-10-01T10:00:00',
				'win_edate': '2007-11-30T11:30:00'
			}
		}
	]
};
