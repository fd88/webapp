import {Component, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subscription, timer} from "rxjs";
import {
	MslInputWithKeyboardComponent
} from "@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component";
import {
	AbstractControl,
	ControlValueAccessor,
	NG_VALIDATORS,
	NG_VALUE_ACCESSOR,
	ValidationErrors,
	Validator
} from "@angular/forms";
import {CameraService} from "@app/core/services/camera.service";

@Component({
  selector: 'app-scanner-input',
  templateUrl: './scanner-input.component.html',
  styleUrls: ['./scanner-input.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => ScannerInputComponent),
			multi: true
		},
		{
			provide: NG_VALIDATORS,
			useExisting: forwardRef(() => ScannerInputComponent),
			multi: true
		}
	]
})
export class ScannerInputComponent implements OnInit, OnDestroy, ControlValueAccessor, Validator {

	private isInitialized = false;

	private isInitializing = false;

	private isEnabledCamera = false;

	currentBarcode = '';

	@Input() minLength = 0;

	@Input() maxLength = 0;

	@Input() expanded = false;

	@Input() showBarcodeField = true;

	@Input() showScannerButton = true;

	@Input() set enabledCamera(state: boolean) {
		this.isEnabledCamera = state;
		if (state && this.expanded) {
			this.showScanner();
		} else {
			this.hideScanner();
		}
	}

	get enabledCamera(): boolean {
		return this.isEnabledCamera;
	}

	@Output() barcodeChange: EventEmitter<string> = new EventEmitter<string>();

	@Output() inputFieldFocus: EventEmitter<void> = new EventEmitter<void>();

	@Output() stateChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

	@ViewChild('codesInput') codesInput: MslInputWithKeyboardComponent;

	constructor(private readonly cameraService: CameraService) { }

	private initialize(): void {
		this.isInitializing = true;
		const targetElement = document.getElementById('scanner');
		this.cameraService.attachTo(targetElement);
		this.cameraService.changeState(this.expanded);
		this.cameraService.scannerState$.next(true);
		this.isInitialized = true;
		this.cameraService.scanSuccess$.subscribe((bc: string) => {
			this.currentBarcode = bc;
			this.barcodeChange.emit(this.currentBarcode);
		});
		timer(3000)
			.subscribe(() => {
				this.isInitializing = false;
			}
		);
	}

	private deinitialize(): void {
		this.cameraService.changeState(false);
		this.cameraService.scannerState$.next(false);
		this.isInitialized = false;
		this.isInitializing = false;
	}

	private switchScanner(): void {
		if (this.cameraService.deinitSubscription && !this.cameraService.deinitSubscription.closed) {
			this.cameraService.deinitSubscription.unsubscribe();
		}
		if (this.expanded && this.enabledCamera && !this.isInitialized && !this.isInitializing) {
			this.initialize();
		} else {
			this.cameraService.changeState(this.expanded);
			if (this.expanded) {
				// this.codesInput.removeFocus();
			} else {
				// this.codesInput.setFocus();
				this.cameraService.deinitSubscription = timer(3000)
					.subscribe(() => {
						this.deinitialize();
					});
			}
		}
	}

	setFocus(): void {
		this.codesInput.setFocus();
	}

	removeFocus(): void {
		this.codesInput.removeFocus();
	}

	onValueChanged(bc: string): void {
		this.writeValue(bc);
		this.barcodeChange.emit(this.currentBarcode);
	}

	changeState(): void {
		this.expanded = !this.expanded;
		this.switchScanner();
		this.stateChanged.emit(this.expanded);
	}

	showScanner(): void {
		this.expanded = true;
		this.switchScanner();
	}

	hideScanner(): void {
		this.expanded = false;
		this.switchScanner();
	}

	ngOnInit(): void {
		if (this.cameraService.deinitSubscription && !this.cameraService.deinitSubscription.closed) {
			this.cameraService.deinitSubscription.unsubscribe();
		}
		timer(300)
			.subscribe(() => {
				if (this.expanded && this.enabledCamera && !this.isInitialized && !this.isInitializing) {
					this.initialize();
				} else {
					this.codesInput.setFocus();
				}
			});
	}

	// ControlValueAccessor Implementation
	onChange = (value: string) => {};
	onTouched = () => {};

	writeValue(value: string): void {
		this.currentBarcode = value || '';
		this.onChange(this.currentBarcode);
	}

	registerOnChange(fn: (value: string) => void): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: () => void): void {
		this.onTouched = fn;
	}

	setDisabledState(isDisabled: boolean): void {
		// Handle component disable state if necessary
	}

	// Validator Implementation
	validate(control: AbstractControl): ValidationErrors | null {
		const value = control.value || '';
		if (this.minLength && value.length < this.minLength) {
			return { minLength: { requiredLength: this.minLength, actualLength: value.length } };
		}
		if (this.maxLength && value.length > this.maxLength) {
			return { maxLength: { requiredLength: this.maxLength, actualLength: value.length } };
		}
		return null;
	}

	ngOnDestroy(): void {
		this.deinitialize();
	}

}
