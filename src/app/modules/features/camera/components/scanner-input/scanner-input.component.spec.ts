import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerInputComponent } from './scanner-input.component';
import {CoreModule} from "@app/core/core.module";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {CameraService} from "@app/core/services/camera.service";

describe('ScannerInputComponent', () => {
  let component: ScannerInputComponent;
  let fixture: ComponentFixture<ScannerInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			SharedModule,
			TranslateModule.forRoot({}),
			HttpClientTestingModule,
			RouterTestingModule.withRoutes(ROUTES)
		],
      	declarations: [ ScannerInputComponent ],
		providers: [
			CameraService,
			BarcodeReaderService,
			AppStoreService,
			HttpService,
			HttpClient
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannerInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test ngOnInit', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test ngOnInit 2', () => {
		component.expanded = true;
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test onValueChanged handler', () => {
		component.onValueChanged('123456789123456789');
		expect(component.currentBarcode).toEqual('123456789123456789');
	});

	it('test onChangeState handler', () => {
		component.expanded = false;
		expect(component.expanded).toBeFalsy();
	});

	it('test onChangeState handler 2', () => {
		component.expanded = true;
		expect(component.expanded).toBeTruthy();
	});

});
