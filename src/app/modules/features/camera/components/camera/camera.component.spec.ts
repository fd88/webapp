import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CameraComponent } from './camera.component';
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {HttpClientModule} from "@angular/common/http";
import {HttpService} from "@app/core/net/http/services/http.service";
import {Router, RouterModule} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {CoreModule} from "@app/core/core.module";

xdescribe('CameraComponent', () => {
  let component: CameraComponent;
  let fixture: ComponentFixture<CameraComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
		imports: [
			HttpClientModule,
			RouterTestingModule,
			CoreModule
		],
		declarations: [ CameraComponent ],
		providers: [
			AppStoreService,
			BarcodeReaderService,
			HttpService
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
