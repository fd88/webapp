import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CamerasSelectorComponent } from './cameras-selector.component';

describe('CamerasSelectorComponent', () => {
  let component: CamerasSelectorComponent;
  let fixture: ComponentFixture<CamerasSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CamerasSelectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CamerasSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
