import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CameraComponent} from "./components/camera/camera.component";
import {CamerasSelectorComponent} from "./components/cameras-selector/cameras-selector.component";
import {SharedModule} from "@app/shared/shared.module";
import {ScannerInputComponent} from "./components/scanner-input/scanner-input.component";

@NgModule({
	declarations: [
		CameraComponent,
		CamerasSelectorComponent,
		ScannerInputComponent
	],
	imports: [
		CommonModule,
		SharedModule
	],
	exports: [
		CameraComponent,
		ScannerInputComponent
	]
})
export class CameraModule { }
