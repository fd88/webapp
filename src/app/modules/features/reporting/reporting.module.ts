import {CommonModule, NgOptimizedImageModule} from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@app/shared/shared.module';
import { TmlBmlInputModule } from '@app/tml-bml-input/tml-bml-input.module';

import { ReportingRoutingModule } from '@app/reporting/reporting-routing.module';
import { ChangeOperationsComponent } from '@app/reporting/components/change-operations/change-operations.component';
import { InputOperationFieldComponent } from '@app/reporting/components/input-operation-field/input-operation-field.component';
import { ReportingDataService } from '@app/reporting/services/reporting-data.service';
import { SanitizePipe } from '@app/reporting/pipes/sanitize.pipe';
import { UserPaymentOperationsComponent } from './components/user-payment-operations/user-payment-operations.component';
import { UPORefillComponent } from './components/uporefill/uporefill.component';
import { UPOWithdrawalComponent } from './components/upowithdrawal/upowithdrawal.component';
import { UserPaymentOperationsService } from '@app/reporting/services/user-payment-operations.service';
import { RequestCodePageComponent } from './components/request-code-page/request-code-page.component';
import { NotAllowedPageComponent } from './components/not-allowed-page/not-allowed-page.component';
import { EnterSmsPageComponent } from './components/enter-sms-page/enter-sms-page.component';
import { TmlBmlLotteryService } from '@app/tmlbml/services/tmlbml-lottery.service';
import { ReportingComponent } from './components/reporting/reporting.component';
import { OtherComponent } from './components/other/other.component';
import { LoyaltyCardInfoComponent } from './components/loyalty-card-info/loyalty-card-info.component';
import { ActiveChallengeComponent } from './components/active-challenge/active-challenge.component';
import {EmitLoyaltyCardComponent} from "@app/reporting/components/emit-loyalty-card/emit-loyalty-card.component";
import {CoreModule} from "@app/core/core.module";
import {UserLoyaltyAuthModule} from "../user-loyalty-auth/user-loyalty-auth.module";
import { BonusesWithdrawalComponent } from './components/bonuses-withdrawal/bonuses-withdrawal.component';
import {CameraModule} from "../camera/camera.module";

/**
 * Модуль для работы с секцией отчетов (в основном меню - "Отчеты" и "Другое").
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ReportingRoutingModule,
        SharedModule,
        TmlBmlInputModule,
        NgOptimizedImageModule,
        UserLoyaltyAuthModule,
        CameraModule
    ],
	declarations: [
		SanitizePipe,
		ChangeOperationsComponent,
		InputOperationFieldComponent,
		UserPaymentOperationsComponent,
		UPORefillComponent,
		UPOWithdrawalComponent,
		RequestCodePageComponent,
		NotAllowedPageComponent,
		EnterSmsPageComponent,
		ReportingComponent,
		OtherComponent,
		LoyaltyCardInfoComponent,
		ActiveChallengeComponent,
		EmitLoyaltyCardComponent,
  BonusesWithdrawalComponent
	],
	providers: [
		ReportingDataService,
		UserPaymentOperationsService,
		TmlBmlLotteryService
	]
})
export class ReportingModule {}
