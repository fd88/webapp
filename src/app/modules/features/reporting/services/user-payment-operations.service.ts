import { Injectable, Injector } from '@angular/core';

import { BehaviorSubject, from, Subject } from 'rxjs';
import { finalize, mergeMap } from 'rxjs/operators';

import { WithdrawalStatus } from '@app/reporting/enums/withdrawal-status.enum';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { BuyVoucherReq, BuyVoucherResp, IBVTicket } from '@app/core/net/http/api/models/buy-voucher';
import { CheckCashOutReq, CheckCashOutResp } from '@app/core/net/http/api/models/check-cash-out';
import { CheckCashSmsReq, CheckCashSmsResp } from '@app/core/net/http/api/models/check-cash-sms';
import { PayCashReq, PayCashResp } from '@app/core/net/http/api/models/pay-cash';

import { parseUrl } from '@app/util/utils';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ResponseCacheService } from '@app/core/services/response-cache.service';
import { ApiError, IError, NetError } from '@app/core/error/types';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogError } from '@app/core/error/dialog';
import { ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { HamburgerMenuService } from '@app/hamburger/services/hamburger-menu.service';
import { Template } from '@app/tickets-print/parser/template';

/**
 * Сервис для работы с функционалом по пополнению счета и вывода денег в кастомных отчетах.
 */
@Injectable({
	providedIn: 'root'
})
export class UserPaymentOperationsService {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Текущее состояние формы вывода денег {@link UPOWithdrawalComponent}.
	 */
	currentWithdrawalStatus$$ = new BehaviorSubject<WithdrawalStatus>(WithdrawalStatus.RequestCode);

	/**
	 * Признак загрузки данных.
	 */
	isLoadingData$$ = new Subject<boolean>();

	/**
	 * Последний полученный ответ на ввод кода.
	 */
	checkCashOutResp: CheckCashOutResp;

	/**
	 * Ошибка ответа на ввод кода подтверждения из СМС
	 */
	smsError: IError;

	/**
	 * Сумма квитанции в виде строки
	 */
	paySum: string;

	/**
	 * Последняя дата выплаты по квитанции в виде строки
	 */
	payDateTime: string;

	/**
	 * Валидный ли код квитанции в поле ввода
	 */
	isValidCodeInput = true;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {DialogContainerService} dialogContainerService Сервис контейнера диалогов.
	 * @param {HttpService} httpService Сервис для работы с HTTP-запросами.
	 * @param {Injector} injector Инжектор зависимостей.
	 * @param {HamburgerMenuComponent} hamburgerMenuService Сервис для работы с гамбургер-меню.
	 */
	constructor(
		private readonly dialogContainerService: DialogContainerService,
		private readonly httpService: HttpService,
		private readonly injector: Injector,
		private readonly hamburgerMenuService: HamburgerMenuService
	) {}

	/**
	 * Выполнить покупку ваучера {@link BuyVoucherReq} для пополнения счета игрока.
	 *
	 * @param {number} summa Сумма пополнения в копейках.
	 */
	buyVoucher(summa: number): void {
		Logger.Log.i('UserPaymentOperationsService', `buyVoucher -> refill amount: "${summa}"`)
			.console();

		const request = new BuyVoucherReq(this.injector, summa);
		this.launchService('dialog.register_refill');
		from(this.httpService.sendApi(request))
			.pipe(finalize(this.finalizeServiceCall.apply(this)))
			.subscribe({
				next: this.buyVoucherResponseHandler.bind(this),
				error: this.buyVoucherErrorHandler.bind(this)
			});
	}

	/**
	 * Проверить квитанцию {@link CheckCashOutReq} для выплаты.
	 *
	 * @param {string} code Код квитанции.
	 */
	checkCashOut(code: string): void {
		Logger.Log.i('UserPaymentOperationsService', `checkCashOut -> code to check: "${code}"`)
			.console();

		const request = new CheckCashOutReq(this.injector, code);
		this.launchService('dialog.request_loading');
		from(this.httpService.sendApi(request))
			.pipe(finalize(this.finalizeServiceCall.apply(this)))
			.subscribe({
				next: this.checkCashOutResponseHandler.bind(this),
				error: this.checkCashOutErrorHandler.bind(this)
			});
	}

	/**
	 * Отправить СМС {@link CheckCashSmsReq} с кодом подтверждения.
	 *
	 * @param {string} code Код квитанции.
	 */
	checkCashSms(code: string): void {
		Logger.Log.i('UserPaymentOperationsService', `checkCashSms -> send SMS with code: "${code}"`)
			.console();

		const request = new CheckCashSmsReq(this.injector, code);
		this.launchService('dialog.request_loading');
		from(this.httpService.sendApi(request))
			.pipe(finalize(this.finalizeServiceCall.apply(this)))
			.subscribe({
				next: this.checkCashSmsResponseHandler.bind(this),
				error: this.checkCashSmsErrorHandler.bind(this)
			});
	}

	/**
	 * Зарегистрировать выплату по квитанции {@link PayCashReq}.
	 *
	 * @param {number} summa Сумма пополнения в копейках.
	 * @param {string} sms Код подтверждения, введенный игроком из СМС.
	 * @param {string} code Код квитанции.
	 */
	payCashReq(summa: number, sms: string, code: string): void {
		Logger.Log.i('UserPaymentOperationsService', `payCashReq -> register payment for "${summa}" with SMS: "${sms}"`)
			.console();

		const request = new PayCashReq(this.injector, summa, sms, code);
		this.launchService('dialog.request_loading');
		from(this.httpService.sendApi(request))
			.pipe(finalize(this.finalizeServiceCall.apply(this)))
			.subscribe({
				next: this.payCashReqResponseHandler.bind(this),
				error: this.payCashReqErrorHandler.bind(this)
			});
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Показать диалоговое окно с информацией о процессе выполнения запроса.
	 * @param dialogText Текст диалогового окна.
	 * @private
	 */
	private launchService(dialogText: string): void {
		this.dialogContainerService.showNoneButtonsInfo('dialog.in_progress', dialogText);
		this.isLoadingData$$.next(true);
	}

	/**
	 * Завершить выполнение запроса.
	 * @param isLoadingData Флаг, указывающий на необходимость отображения индикатора загрузки.
	 * @param deactivateCancelButton Флаг, указывающий на необходимость деактивации кнопки "Отмена".
	 * @private
	 */
	private finalizeServiceCall(isLoadingData = false, deactivateCancelButton = true): void {
		this.isLoadingData$$.next(isLoadingData);

		if (deactivateCancelButton) {
			this.hamburgerMenuService.deactivateCancelButton();
		}
	}

	/**
	 * Обработчик успешного совершения запроса покупки ваучера пополнения {@link BuyVoucherReq}.
	 * Распечатывает чек на основе полученных данных.
	 *
	 * @param {BuyVoucherResp} response Модель ответа на запрос {@link BuyVoucherReq}.
	 */
	private buyVoucherResponseHandler(response: BuyVoucherResp): void {
		Logger.Log.i('UserPaymentOperationsService', `buyVoucherResponseHandler -> response is OK, will print data: %s`, response)
			.console();
		this.dialogContainerService.hideAll();

		const appStoreService = this.injector.get(AppStoreService);
		const responseCacheService = this.injector.get(ResponseCacheService);
		const printService = this.injector.get(PrintService);

		if (!response || !response.ticket_templ_url) {
			return;
		}

		let path = response.ticket_templ_url;

		// подготовить URL шаблона для печати чека
		if (path.indexOf('/') === 0) {
			path = path.substring(1);
		}

		const requestUrl = parseUrl(appStoreService.Settings.csEnvironmentUrl);
		const port = requestUrl.port ? `:${requestUrl.port}` : '';
		const url = `${requestUrl.origin}${port}/${path}`;

		this.dialogContainerService.showNoneButtonsInfo('dialog.in_progress', 'dialog.ticket_printing_wait_info');

		const parsedResponse = {...response};
		parsedResponse.ticket = [response.ticket] as Array<IBVTicket>;

		const template = new Template(appStoreService, 0, responseCacheService, this.httpService);
		const ticketTemplate = {path, url};
		from(template.formatTicket(parsedResponse, response.ticket as ITransactionTicket, ticketTemplate))
			.pipe(
				mergeMap(printContent => {
					return printService.printDocument(printContent, 15000);
				})
			)
			.subscribe(() => {
				this.dialogContainerService.hideAll();
			}, error => {
				const msg = error instanceof ApiError || error instanceof NetError
					? error
					: new DialogError(0, 'dialog.cant_print_refill_voucher');
				this.dialogContainerService.showOneButtonError(msg, {text: 'dialog.dialog_button_continue'});
			});
	}

	/**
	 * Обработчик ошибки при совершении операции пополнения счета.
	 *
	 * @param {NetError | DialogError} error
	 */
	private buyVoucherErrorHandler(error: NetError | DialogError): void {
		Logger.Log.e('UserPaymentOperationsService', `buyVoucherErrorHandler -> response ERROR: %s`, error)
			.console();
		this.dialogContainerService.showOneButtonError(error, {text: 'dialog.dialog_button_continue'});
	}

	/**
	 * Обработчик успешного совершения запроса на проверку возможности снятия средств {@link CheckCashOutReq}.
	 * @param {CheckCashOutResp} response Модель ответа на запрос {@link CheckCashOutReq}.
	 */
	private checkCashOutResponseHandler(response: CheckCashOutResp): void {
		Logger.Log.i('UserPaymentOperationsService', `checkCashOutResponseHandler -> response OK: %s`, response)
			.console();
		this.dialogContainerService.hideAll();

		if (!response) {
			return;
		}

		this.checkCashOutResp = response;
		this.isLoadingData$$.next(true);

		// если пришла хоть какая-то не нулевая сумма, то
		if (response.pay_sum) {
			// преобразовываем ее в текущую локаль
			this.paySum = (response.pay_sum / 100).toLocaleString();
			// и проверяем возможность выплаты
			if (response.pay_enable) {
				// формируем дату
				const payDateObj = new Date(response.pay_expire_date);
				const payDate = payDateObj.toLocaleDateString();
				const payTimeParts = payDateObj.toLocaleTimeString()
					.split(':');
				this.payDateTime = `${payDate}, ${payTimeParts[0]}:${payTimeParts[1]}`;
				this.currentWithdrawalStatus$$.next(WithdrawalStatus.WithdrawalAllowedEnterSMS);
			} else {
				this.currentWithdrawalStatus$$.next(WithdrawalStatus.NoWithdrawalAllowed);
			}
		} else {
			// если сумма равна нулю или вообще не пришла, то ошибку показываем в попапе
			this.dialogContainerService.showOneButtonInfo(`dialog.error_title`, response.reason_detail, {
				click: () => this.isLoadingData$$.next(false),
				text: 'dialog.dialog_button_continue'
			});
			// делаем форму запроса не валидной
			this.isValidCodeInput = false;
		}
	}

	/**
	 * Обработчик ошибки при проверке возможности снятия средств {@link CheckCashOutReq}.
	 * @param error
	 */
	private checkCashOutErrorHandler(error: NetError | DialogError): void {
		Logger.Log.e('UserPaymentOperationsService', `checkCashOutErrorHandler -> response ERROR: %s`, error)
			.console();
		this.dialogContainerService.showOneButtonError(error, {
			click: () => this.isLoadingData$$.next(false),
			text: 'dialog.dialog_button_continue'
		});
	}

	/**
	 * Обработчик успешного совершения запроса на проверку кода смс {@link CheckCashSmsReq}.
	 * @param response Модель ответа на запрос {@link CheckCashSmsReq}.
	 * @private
	 */
	private checkCashSmsResponseHandler(response: CheckCashSmsResp): void {
		Logger.Log.i('UserPaymentOperationsService', `checkCashSms -> response OK: %s`, response)
		.console();
		this.dialogContainerService.hideAll();
		this.isLoadingData$$.next(false);
		// this.paymentBtnDisabled = false;
		this.currentWithdrawalStatus$$.next(WithdrawalStatus.WithdrawalAllowedEnterSMS);
	}

	/**
	 * Обработчик ошибки при проверке кода смс {@link CheckCashSmsReq}.
	 * @param error Ошибка.
	 * @private
	 */
	private checkCashSmsErrorHandler(error: NetError | DialogError): void {
		Logger.Log.e('UserPaymentOperationsService', `checkCashSms -> response ERROR: %s`, error)
			.console();
		this.dialogContainerService.showOneButtonError(error, {text: 'dialog.dialog_button_continue'});
	}

	/**
	 * Обработчик успешного совершения запроса на снятие средств {@link PayCashReq}.
	 * @param response Модель ответа на запрос {@link PayCashReq}.
	 * @private
	 */
	private payCashReqResponseHandler(response: PayCashResp): void {
		Logger.Log.i('UserPaymentOperationsService', `payCashReq -> response OK: %s`, response)
		.console();
		this.dialogContainerService.hideAll();
		this.currentWithdrawalStatus$$.next(WithdrawalStatus.WithdrawalFinal);
	}

	/**
	 * Обработчик ошибки при снятии средств {@link PayCashReq}.
	 * @param error Ошибка.
	 * @private
	 */
	private payCashReqErrorHandler(error: NetError | DialogError): void {
		Logger.Log.e('UserPaymentOperationsService', `payCashReq -> response ERROR: %s`, error)
		.console();
		this.smsError = error;
		this.dialogContainerService.hideAll();
	}
}
