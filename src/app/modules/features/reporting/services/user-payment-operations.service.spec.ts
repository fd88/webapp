import { TestBed, waitForAsync } from '@angular/core/testing';

import { UserPaymentOperationsService } from './user-payment-operations.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { HamburgerMenuService } from '@app/hamburger/services/hamburger-menu.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { Injector } from '@angular/core';
import { BuyVoucherResp } from '@app/core/net/http/api/models/buy-voucher';
import { timer } from 'rxjs';


xdescribe('UserPaymentOperationsService', () => {
	let service: UserPaymentOperationsService | any;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientModule
			],
			providers: [
				Injector,

				UserPaymentOperationsService,
				HttpService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				HamburgerMenuService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(UserPaymentOperationsService);
		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		(appStoreService.Settings as any)._csEnvironmentUrl = '';
		appStoreService.Settings.populateEsapActionsMapping(csConfig);

	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test buyVoucher', async () => {
		const hms: HamburgerMenuService = TestBed.inject(HamburgerMenuService);
		hms.activateCancelButton();
		expect(hms.cancelButtonDisabled).toBeFalsy();
		service.buyVoucher(100);
		await timer(20).toPromise();
		expect(hms.cancelButtonDisabled).toBeTruthy()
	});

	it('test checkCashOut',() => {
		service.checkCashOut('12345678');
	});

	it('test checkCashSms',() => {
		service.checkCashSms('12345678');
	});

	it('test payCashReq', () => {
		service.payCashReq(100, 'sms123', '12345678');
	});

	it('test buyVoucherResponseHandler function', () => {
		const response: BuyVoucherResp | any = {
			client_id: 40000,
			client_trans_id: '400393299460',
			err_code: 0,
			err_descr: 'Операція виконана успішно.',
			lang: 'ua',
			slogan: 'Интернет счета WEB портала МСЛ',
			ticket: {
				bet_sum: '55.00',
				date_reg: '2019-07-25 09:32:07',
				description: 'Ваучер пополнения',
				hash: '2000059616',
				id: 2277874876,
				number: '82',
				pay_sum: '5500',
				in_code: '200-0059-616',
				valid_until: '2019-08-24 09:32:07'
			},
			ticket_templ_url: 'esap/templates/site-voucher-utf8-01.xml',
			time_stamp: '2019-07-25 09:32:07'
		};

		service.buyVoucherResponseHandler(response);

		response.ticket_templ_url = '/esap/templates/site-voucher-utf8-01.xml';
		service.buyVoucherResponseHandler(response);

		service.buyVoucherResponseHandler(response);
	});

	it('test buyVoucherErrorHandler function', () => {
		const response = {
			client_trans_id: '400396922372',
			err_code: 827,
			err_descr: 'Сума виплати/поповнення невірна.',
			lang: 'ua',
			time_stamp: '2019-07-25 13:28:00',
			user_id: 40000
		};

		service.buyVoucherErrorHandler(response);
	});

	it('test checkCashOutResponseHandler function', () => {
		service.checkCashOutResponseHandler();
		service.checkCashOutResponseHandler({pay_sum: 10000});
		service.checkCashOutResponseHandler({pay_sum: 10000, pay_enable: true});
	});

	it('test checkCashOutErrorHandler function', () => {
		service.checkCashOutErrorHandler();
	});

	it('test checkCashSmsErrorHandler function', () => {
		service.checkCashSmsErrorHandler();
	});

	it('test payCashReqErrorHandler function', () => {
		service.payCashReqErrorHandler();
	});
});
