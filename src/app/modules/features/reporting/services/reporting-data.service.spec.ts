import { TestBed } from '@angular/core/testing';

import { ReportingDataService } from './reporting-data.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ReportsService } from '@app/core/services/report/reports.service';
import { RouterTestingModule } from '@angular/router/testing';

import { LogService } from '@app/core/net/ws/services/log/log.service';
import { Operator, TerminalRoles } from '@app/core/services/store/operator';
import { CASH_OPERATIONS } from '@app/reporting/interfaces/ioperation-menu-item';
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ReportingRoutingModule} from "@app/reporting/reporting-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {TmlBmlInputModule} from "@app/tml-bml-input/tml-bml-input.module";
import {UserPaymentOperationsService} from "@app/reporting/services/user-payment-operations.service";
import {TmlBmlLotteryService} from "@app/tmlbml/services/tmlbml-lottery.service";
import {CoreModule} from "@app/core/core.module";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateModule} from "@ngx-translate/core";
import {SESS_DATA} from "../../mocks/session-data";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_100} from "../../mocks/game100-draws";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

describe('ReportingDataService', () => {
	let service: ReportingDataService | any;
	let appStoreService: AppStoreService;
	let reportsService: ReportsService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				ReportingRoutingModule,
				SharedModule,
				TmlBmlInputModule,
				RouterTestingModule,
				CoreModule,
				HttpClientModule,
				TranslateModule.forRoot({})
			],
			providers: [
				ReportingDataService,
				UserPaymentOperationsService,
				TmlBmlLotteryService,
				AppStoreService,
				HttpService,
				HttpClient,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				}
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(ReportingDataService);

		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		appStoreService.isLoggedIn$$.next(true);

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);

		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);

		reportsService  = TestBed.inject(ReportsService);
		await reportsService.reload();

		TestBed.inject(LogService);

	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	xit('test createTopMenu method', () => {
		expect(service.createTopMenu()).toEqual([
			{ label: 'reporting.menu.1', path: '1', isActive: false },
			{ label: 'reporting.menu.2', path: '2', isActive: false }
		]);
	});

	it('test addUserPaymentOperations method', () => {
		let op = new Operator('1', '1', '1', TerminalRoles.RETAILER);
		service.appStoreService.operator.next(op);
		expect(service.addUserPaymentOperations()).toBeUndefined();

		op = new Operator('1', '1', '1', TerminalRoles.OPERATOR);
		service.appStoreService.operator.next(op);
		expect(service.addUserPaymentOperations()).toEqual(CASH_OPERATIONS);
	});

	it('test selectMenuItem method', () => {
		service.fullMenuItems = [
			{
				label: 'reporting.menu.1',
				path: '1',
				isActive: false,
				subMenuItems: [
					{ label: 'reporting.menu.2', path: '2', isActive: false }
				]
			}
		];
		service.selectMenuItem(0, 0);
		expect(service.currentMenuIndex).toEqual(0);
	});

});
