import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { Operator, TerminalRoles } from '@app/core/services/store/operator';
import { ReportsId, ReportsService } from '@app/core/services/report/reports.service';
import { INavigationMenuItem } from '@app/shared/components/navigation-menu.component';
import { IOperationListItem } from '@app/reporting/interfaces/ioperation-list-item';
import {CASH_OPERATIONS, IOperationMenuItem, LOYALTY_CARD} from '@app/reporting/interfaces/ioperation-menu-item';

/**
 * Сервис модуля "Другое".
 */
@Injectable({
	providedIn: 'root'
})
export class ReportingDataService {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Раскрыт ли компонент списка штрих-кодов?
	 */
	readonly bcComponentListExpanded$ = new BehaviorSubject<boolean>(true);

	/**
	 * Признак. указывающий, что модуль будет использоваться только для отчетов.
	 */
	readonly onlyReports$$ = new BehaviorSubject<boolean>(false);

	/**
	 * Список вкладок.
	 */
	readonly topMenu$$ = new BehaviorSubject<Array<INavigationMenuItem>>([]);

	/**
	 * Список операций.
	 */
	readonly operationList$$ = new BehaviorSubject<Array<IOperationListItem>>([]);

	/**
	 * Текущий отчет.
	 */
	readonly currentReport$$ = new BehaviorSubject<string>(undefined);

	/**
	 * Текущая операция.
	 */
	readonly currentOperation$$ = new BehaviorSubject<string>(undefined);

	/**
	 * Модель выбранной кастомной операции.
	 */
	readonly customOperation$$ = new BehaviorSubject<IOperationListItem | undefined>(undefined);

	/**
	 * Признак показа результата запроса.
	 */
	readonly inResult$$ = new BehaviorSubject<boolean>(false);

	/**
	 * Список отчетов со списками операций.
	 */
	fullMenuItems: Array<IOperationMenuItem> = [];

	/**
	 * Индекс предыдущего меню.
	 */
	previousMenuIndex = 0;

	/**
	 * Индекс предыдущего подменю.
	 */
	previousSubMenuIndex = 0;

	/**
	 * Индекс текущего меню.
	 */
	currentMenuIndex = 0;

	/**
	 * Индекс текущего подменю.
	 */
	currentSubMenuIndex = 0;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {ReportsService} reportsService Сервис по загрузке (обновлению), обработке, хранению отчетов из ЦС
	 * @param {AppStoreService} appStoreService Сервис-хранилище данных приложения
	 */
	constructor(
		private readonly reportsService: ReportsService,
		private readonly appStoreService: AppStoreService
	) {}

	/**
	 * Создать элементы верхнего меню.
	 */
	createTopMenu(): Array<INavigationMenuItem> {
		const arr = this.reportsService.reportResponses
			.map(m => {
				return {
					label: `reporting.menu.${m.report.id}`,
					path: m.report.id,
					isActive: false
				};
			});

		this.topMenu$$.next(arr);

		return arr;
	}

	/**
	 * Построить список операций на базе выбранного отчета.
	 * Алгоритм разбора тэгов ACCESS_...
	 * 1. Анализ тэга ACCESS_LEVEL
	 * 1.1. Если отсутствует тэг ACCESS_LEVEL, то перейти к (2)
	 * 1.2. Если в тэге ACCESS_LEVEL атрибут "role" имеет значение "оператор", то перейти к (2)
	 * 1.3. Отчет НЕ доступен. Конец.
	 *
	 * 2. Анализ тэга ACCESS_LIST
	 * 2.1. Если отсутствует тэг ACCESS_LIST, то перейти к (3)
	 * 2.2. Если в тэге ACCESS_LIST атрибут "roles" содержит в списке текущий уровень доступа оператора, то перейти к (3)
	 * 2.3. Отчет НЕ доступен. Конец.
	 * 3. Отчет доступен.
	 *
	 * @param {string} reportId Идентификатор отчета (путь).
	 */
	makeOperationsListByReportId(reportId: string): Array<IOperationListItem> {
		Logger.Log.i('ReportingListComponent', `makeOperationsListByReportId -> by ID: "${reportId}"`)
			.console();

		const operName = Operator.userRolesMap.get(TerminalRoles.OPERATOR);
		const operAccessLevelId = this.appStoreService.operator.value.accessLevel;
		const operAccessLevelName = Operator.userRolesMap.get(operAccessLevelId);

		const reportList = this.reportsService.getReportsList(reportId);
		if (reportList) {
			const arr = reportList
				.filter(p => {
					if (!p.accessibility.access_level || (p.accessibility.access_level && p.accessibility.access_level.role === operName)) {
						if (p.accessibility.access_list) {
							const roles = p.accessibility.access_list.roles.split(',');

							return roles.indexOf(operAccessLevelName) !== -1;
						}

						return true;
					}

					return false;
				})
				.map(m => {
					return {
						label: m.visible_name,
						path: m.id,
						isActive: false,
						autoprint: m.output.printer.autoprint
					};
				});
			this.operationList$$.next(arr);

			Logger.Log.i('ReportingDataService', `for current user role "${operAccessLevelName}" will be available ${arr.length} report(s)`)
				.console();

			return arr;
		}

		return [];
	}

	/**
	 * Добавить меню для пополнения счета игрока и вывода денег со счета для роли "Оператор".
	 */
	addUserPaymentOperations(): IOperationMenuItem | undefined {
		if (this.appStoreService.operator.value.isOperator) {
			return {...CASH_OPERATIONS};
		}

		return undefined;
	}

	/**
	 * Добавить меню для карт лояльности.
	 */
	addLoyaltyCardMenu(): IOperationMenuItem | undefined {
		if (this.appStoreService.operator.value.isOperator) {
			return {...LOYALTY_CARD};
		}

		return undefined;
	}

	/**
	 * Выбрать элемент меню
	 * @param parentIndex Индекс родительского элемента
	 * @param itemIndex Индекс выбранного элемента
	 */
	selectMenuItem(parentIndex: number, itemIndex: number): void {
		for (const [i, menuItem] of this.fullMenuItems.entries()) {
			menuItem.isActive = i === parentIndex;
			if (menuItem.isActive) {
				this.currentReport$$.next(menuItem.path);
			} else {
				menuItem.label = menuItem.defaultLabel;
			}
			for (const [j, subMenuItem] of menuItem.subMenuItems.entries()) {
				subMenuItem.isActive = menuItem.isActive && (j === itemIndex);
				if (subMenuItem.isActive) {
					if (menuItem.path === 'custom-report-user-payments') {
						this.customOperation$$.next(subMenuItem);
					} else {
						this.currentOperation$$.next(subMenuItem.path);
						this.customOperation$$.next(null);
					}
				}
			}
		}
		this.fullMenuItems[parentIndex].expanded = false;
		this.fullMenuItems[parentIndex].isActive = true;
		this.fullMenuItems[parentIndex].label = this.fullMenuItems[parentIndex].subMenuItems[itemIndex].label;
		this.previousMenuIndex = this.currentMenuIndex;
		this.previousSubMenuIndex = this.currentSubMenuIndex;
		this.currentMenuIndex = parentIndex;
		this.currentSubMenuIndex = itemIndex;
	}

}
