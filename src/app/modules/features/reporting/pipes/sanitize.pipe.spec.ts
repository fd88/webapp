import { SanitizePipe } from './sanitize.pipe';
import {TestBed} from "@angular/core/testing";
import {DomSanitizer} from "@angular/platform-browser";
import {CommonModule} from "@angular/common";

xdescribe('SanitizePipe', () => {
	let pipe: SanitizePipe;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				CommonModule
			],
			providers: [
				DomSanitizer,
				SanitizePipe
			]
		})
			.compileComponents();

		pipe = TestBed.inject(SanitizePipe);
	});

  it('test transform method 1', () => {
    expect(pipe.transform('<style>.test{}</style>', 'style')).toEqual('<style>.test{}</style>');
  });
});
