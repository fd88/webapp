import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeValue } from '@angular/platform-browser';

/**
 * Пайп для безопасного использования HTML, CSS, JavaScript, и подключенных ресурсов.
 */
@Pipe({
	name: 'sanitize'
})
export class SanitizePipe implements PipeTransform {

	/**
	 * Конструктор пайпа.
	 *
	 * @param {DomSanitizer} sanitizer Сервис для безопасного использования HTML, CSS, JavaScript, и подключенных ресурсов.
	 */
	constructor(
		private readonly sanitizer: DomSanitizer
	) {}

	/**
	 * Функция преобразования html-кода в безопасный для использования.
	 * @param value html-код
	 * @param type Тип выводимого контента.
	 */
	transform(value: any, type: string): SafeValue {
		switch (type) {
			case 'html':
				return this.sanitizer.bypassSecurityTrustHtml(value);
			case 'style':
				return this.sanitizer.bypassSecurityTrustStyle(value);
			case 'script':
				return this.sanitizer.bypassSecurityTrustScript(value);
			case 'url':
				return this.sanitizer.bypassSecurityTrustUrl(value);
			case 'resource':
				return this.sanitizer.bypassSecurityTrustResourceUrl(value);
			default:
				throw new Error(`Invalid safe type specified: ${type}`);
		}
	}
}
