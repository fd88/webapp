import { INavigationMenuItem } from '@app/shared/components/navigation-menu.component';
import { IOperationListItem } from '@app/reporting/interfaces/ioperation-list-item';

/**
 * Список кастомных идентификаторов отчетов.
 * - {@link UserPayments} - для операций по пополнению счета игрока и вывода денег
 */
export enum CustomReport {
	UserPayments = 'custom-report-user-payments'
}

/**
 * Список операций по аккаунту юзера, которые используются в кастомном отчете {@link CustomReport.UserPayments UserPayments}.
 * - {@link Refill} - пополнение счета
 * - {@link Withdrawal} - вывод денег со счета
 * - {@link Instruction} - инструкция по выводу денег
 */
export enum UserPaymentOperation {
	Refill = 'refill',
	Withdrawal = 'withdrawal',
	Instruction = 'instruction'
}

/**
 * Меню операций для пополнения и вывода денег со счета игрока.
 * Данный пункт меню (отчета) не относится к отчетам из конфигурации.
 */
export const CASH_OPERATIONS: IOperationMenuItem = {
	defaultLabel: 'reporting.menu.player_accounts_operations',
	expanded: false,
	label: 'reporting.menu.player_accounts_operations',
	path: CustomReport.UserPayments,
	subMenuItems: [
		// {
		// 	label: 'reporting.menu.refill',
		// 	path: UserPaymentOperation.Refill,
		// 	isActive: false,
		// 	autoprint: false
		// },
		{
			label: 'reporting.menu.withdrawal',
			path: UserPaymentOperation.Withdrawal,
			isActive: false,
			autoprint: false
		},
		{
			label: 'reporting.menu.help',
			isActive: false,
			autoprint: false,
			extraClass: 'rp-menu-help',
			path: UserPaymentOperation.Instruction
		}
	]
};

/**
 * Меню операций для карт лояльности
 */
export const LOYALTY_CARD: IOperationMenuItem = {
	defaultLabel: 'other.loyalty_card',
	expanded: false,
	label: 'other.loyalty_card',
	path: 'loyalty-card',
	subMenuItems: [
		{
			label: 'other.loyalty_card_info',
			path: 'lc-info',
			isActive: false,
			autoprint: false
		},
		{
			label: 'other.loyalty_card_emission',
			path: 'add-lc-info',
			isActive: false,
			autoprint: false
		},
		// {
		// 	label: 'other.active-challenge',
		// 	path: 'challenges',
		// 	isActive: false,
		// 	autoprint: false
		// }
		// {
		// 	label: 'other.bonuses-withdrawal',
		// 	path: 'bonuses-withdrawal',
		// 	isActive: false,
		// 	autoprint: false
		// }
	]
};

/**
 * Модель элемента меню (отчета).
 */
export interface IOperationMenuItem extends INavigationMenuItem {
	/**
	 * Список подменю.
	 */
	subMenuItems: Array<IOperationListItem>;
	/**
	 * Название пункта меню по умолчанию.
	 */
	defaultLabel?: string;
	/**
	 * Раскрыто ли меню.
	 */
	expanded?: boolean;
}
