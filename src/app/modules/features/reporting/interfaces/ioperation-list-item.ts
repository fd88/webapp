/**
 * Интерфейс для элемента списка операций.
 */
export interface IOperationListItem {
	/**
	 * Название операции.
	 */
	label: string;

	/**
	 * Путь к компоненту операции.
	 */
	path: string;

	/**
	 * Признак, указывающий на активность операции.
	 */
	isActive: boolean;

	/**
	 * Признак, указывающий на необходимость {@link OutputTag.printer автопечати}.
	 */
	autoprint: boolean;

	/**
	 * Дополнительный класс для элемента списка.
	 */
	extraClass?: string;

	/**
	 * Признак, указывающий на видимость операции.
	 */
	isVisible?: boolean;
}

