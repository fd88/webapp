import { inject, TestBed } from '@angular/core/testing';

import { ExitReportingGuard } from './exit-reporting.guard';
import { BehaviorSubject } from 'rxjs';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PARAM_MANUAL_MENU_CLICK } from '@app/util/route-utils';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import {ReportsId} from "@app/core/services/report/reports.service";
import {Operator} from "@app/core/services/store/operator";
import {ReportingComponent} from "@app/reporting/components/reporting/reporting.component";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";

describe('ExitReportingGuard', () => {
	let appStoreService: AppStoreService | any;
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				CoreModule,
				HttpClientTestingModule
			],
			providers: [
				LogService,
				ExitReportingGuard,
				AppStoreService,
				AppStoreService,
				HttpService,
				HttpClient
			]
		});

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
	});

	it('should be created', inject([ExitReportingGuard], (guard: ExitReportingGuard) => {
		expect(guard).toBeTruthy();
	}));

	it('check ExitReportingGuard guard', inject([ExitReportingGuard], (guard: ExitReportingGuard | any) => {
		guard.canDeactivate(
			{
				tmlBmlInputStoreService: {
					ticketsList$$: new BehaviorSubject<Array<ITmlBmlListItem>>([])
				},
				reportingDataService: {
					currentReport$$: new BehaviorSubject<string>('')
				}
			} as ReportingComponent,
			{} as ActivatedRouteSnapshot,
			{} as RouterStateSnapshot,
			{
				url: `xxx?${PARAM_MANUAL_MENU_CLICK}`
			} as RouterStateSnapshot
		);

		guard.canDeactivate(
			{
				tmlBmlInputStoreService: {
					ticketsList$$: new BehaviorSubject<Array<ITmlBmlListItem>>([{} as ITmlBmlListItem])
				},
				reportingDataService: {
					currentReport$$: new BehaviorSubject<string>('')
				}
			} as ReportingComponent,
			{} as ActivatedRouteSnapshot,
			{} as RouterStateSnapshot,
			{
				url: ''
			} as RouterStateSnapshot
		);

		appStoreService.operator.next(new Operator('10568', 'session', '100603', 1));
		guard.canDeactivate(
			{
				tmlBmlInputStoreService: {
					ticketsList$$: new BehaviorSubject<Array<ITmlBmlListItem>>([{} as ITmlBmlListItem])
				},
				reportingDataService: {
					currentReport$$: new BehaviorSubject<string>(''),
					currentOperation$$: new BehaviorSubject<string>('1')
				}
			} as ReportingComponent,
			{} as ActivatedRouteSnapshot,
			{} as RouterStateSnapshot,
			{
				url: ''
			} as RouterStateSnapshot
		);

		guard.canDeactivate(
			{
				tmlBmlInputStoreService: {
					ticketsList$$: new BehaviorSubject<Array<ITmlBmlListItem>>([{} as ITmlBmlListItem])
				},
				reportingDataService: {
					currentReport$$: new BehaviorSubject<string>(ReportsId.PaperInstant),
					currentOperation$$: new BehaviorSubject<string>('1')
				}
			} as ReportingComponent,
			{} as ActivatedRouteSnapshot,
			{} as RouterStateSnapshot,
			{
				url: ''
			} as RouterStateSnapshot
		);

		guard.canDeactivate(
			{
				tmlBmlInputStoreService: {
					ticketsList$$: new BehaviorSubject<Array<ITmlBmlListItem>>([{} as ITmlBmlListItem])
				},
				reportingDataService: {
					currentReport$$: new BehaviorSubject<string>(''),
					currentOperation$$: new BehaviorSubject<string>('22')
				}
			} as ReportingComponent,
			{} as ActivatedRouteSnapshot,
			{} as RouterStateSnapshot,
			{
				url: ''
			} as RouterStateSnapshot
		);
	}));
});
