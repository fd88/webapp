import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PARAM_BARCODE, PARAM_MANUAL_MENU_CLICK, URL_AUTH } from '@app/util/route-utils';
import { ReportsId } from '@app/core/services/report/reports.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { OtherComponent } from '@app/reporting/components/other/other.component';

/**
 * Гард для проверки возможности выхода из раздела "Другое".
 */
@Injectable({
	providedIn: 'root'
})
export class ExitReportingGuard implements CanDeactivate<OtherComponent> {

	/**
	 * Конструктор класса.
	 * @param router Объект для работы с маршрутизацией.
	 * @param appStoreService Сервис хранилища приложения.
	 */
	constructor(
		private readonly router: Router,
		private readonly appStoreService: AppStoreService
	) {}

	/**
	 * Метод для проверки возможности выхода из раздела "Другое".
	 * @param component Компонент, с которого происходит выход.
	 * @param currentRoute Текущий маршрут.
	 * @param currentState Текущее состояние маршрута.
	 * @param nextState Следующее состояние маршрута.
	 */
	canDeactivate(
		component: OtherComponent,
		currentRoute: ActivatedRouteSnapshot,
		currentState: RouterStateSnapshot,
		nextState?: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		const parsedUrl = this.router.parseUrl(nextState.url);
		const isEmptyBarcodesList = component.tmlBmlInputStoreService && component.tmlBmlInputStoreService.ticketsList$$.value.length === 0;
		const isManualClickOnMenu = parsedUrl.queryParamMap.has(PARAM_MANUAL_MENU_CLICK);
		const isBarcode = parsedUrl.queryParamMap.has(PARAM_BARCODE);
		const isLogout = nextState.url.includes(URL_AUTH);

		const isTMLInventory = component.reportingDataService
			&& component.reportingDataService.currentReport$$.value
			&& component.reportingDataService.currentOperation$$.value
			&& component.reportingDataService.currentReport$$.value === ReportsId.PaperInstant
			&& (component.reportingDataService.currentOperation$$.value === '22' || this.appStoreService.operator.value.isManager);

		const noAllowExit = (isTMLInventory && isBarcode) || (isTMLInventory && !isBarcode && !isEmptyBarcodesList);
		const canDeactivate = !noAllowExit || isManualClickOnMenu || isLogout;

		Logger.Log.i('ExitReportingGuard', `canDeactivate -> it's possible to exit outside ReportingListComponent? (${canDeactivate})`)
			.console();

		return canDeactivate;
	}

}
