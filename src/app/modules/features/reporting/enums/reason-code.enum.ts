/**
 * Список причин запрета выплаты.
 * - {@link ReceiptNotFound} - квитанция не найдена
 * - ...
 * - ...
 */
export enum ReasonCode {
	ReceiptNotFound = 70009
}
