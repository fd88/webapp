/**
 * Список состояний на форме вывода денег.
 * - {@link RequestCode} - запросить выплату по коду
 * - ...
 * - ...
 */
export enum WithdrawalStatus {
	RequestCode						= 'request_code',
	IncorrectCode					= 'incorrect_code',
	WithdrawalAllowedSendSMS		= 'withdrawal_allowed_send_sms',
	WithdrawalAllowedWithoutSMS		= 'withdrawal_allowed_without_sms',
	WithdrawalAllowedEnterSMS		= 'withdrawal_allowed_enter_sms',
	WithdrawalAllowedIncorrectSMS	= 'withdrawal_allowed_incorrect_sms',
	NoWithdrawalAllowed				= 'no_withdrawal_allowed',
	WithdrawalFinal					= 'withdrawal_final'
}
