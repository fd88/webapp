import { Component } from '@angular/core';
import { UserPaymentOperationsService } from '@app/reporting/services/user-payment-operations.service';

/**
 * Компонент страницы "Вывод денег не разрешен".
 */
@Component({
	selector: 'app-not-allowed-page',
	templateUrl: './not-allowed-page.component.html',
	styleUrls: ['./not-allowed-page.component.scss']
})
export class NotAllowedPageComponent {
	/**
	 * Конструктор компонента.
	 * @param userPaymentOperationsService Сервис для работы с финансовыми операциями.
	 */
	constructor(readonly userPaymentOperationsService: UserPaymentOperationsService) { }
}
