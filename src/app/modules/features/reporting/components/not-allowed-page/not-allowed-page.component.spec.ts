import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NotAllowedPageComponent } from './not-allowed-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from '@app/core/net/http/services/http.service';

describe('NotAllowedPageComponent', () => {
  let component: NotAllowedPageComponent;
  let fixture: ComponentFixture<NotAllowedPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NotAllowedPageComponent ],
	  imports:[
			TranslateModule.forRoot(),
			RouterTestingModule.withRoutes([]),
			SharedModule,
			FormsModule,
			ReactiveFormsModule,
		  	HttpClientTestingModule
	  ],
		providers: [
			HttpService
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotAllowedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
