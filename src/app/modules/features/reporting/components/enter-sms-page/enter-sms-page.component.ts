import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UserPaymentOperationsService } from '@app/reporting/services/user-payment-operations.service';
import { FormGroup } from '@angular/forms';

/**
 * Компонент ввода СМС-кода для вывода денег.
 */
@Component({
	selector: 'app-enter-sms-page',
	templateUrl: './enter-sms-page.component.html',
	styleUrls: ['./enter-sms-page.component.scss']
})
export class EnterSmsPageComponent  {
	/**
	 * Длина СМС-кода.
	 */
	@Input() pinCodeLength = 4;

	/**
	 * Текст кнопки отправки СМС.
	 */
	@Input() sendSMSButtonText: string;

	/**
	 * Форма ввода СМС-кода для вывода денег.
	 */
	@Input() smsForm: FormGroup;

	/**
	 * Событие нажатия на кнопку отправки СМС.
	 */
	@Output() readonly clickSendSMSButton = new EventEmitter<undefined>();

	/**
	 * Конструктор компонента.
	 * @param userPaymentOperationsService Сервис для работы с финансовыми операциями.
	 */
	constructor(readonly userPaymentOperationsService: UserPaymentOperationsService) { }

	/**
	 * Обработчик нажатия на кнопку отправки СМС.
	 */
	onClickSendSMSButtonHandler(): void {
		this.clickSendSMSButton.emit();
	}
}
