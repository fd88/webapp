import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EnterSmsPageComponent } from './enter-sms-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from '@app/core/net/http/services/http.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import {UserPaymentOperationsService} from "@app/reporting/services/user-payment-operations.service";
import {ReportingModule} from "@app/reporting/reporting.module";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";

describe('EnterSmsPageComponent', () => {
  let component: EnterSmsPageComponent;
  let fixture: ComponentFixture<EnterSmsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			RouterTestingModule.withRoutes([]),
			SharedModule,
			FormsModule,
			ReactiveFormsModule,
			HttpClientTestingModule,
			ReportingModule
		],
		declarations: [
			EnterSmsPageComponent,
			MslInputPinComponent
		],
		providers: [
			HttpService,
			LogService,
			UserPaymentOperationsService
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
  	TestBed.inject(LogService);
    fixture = TestBed.createComponent(EnterSmsPageComponent);
    component = fixture.componentInstance;
	  const formBuilder = new FormBuilder();
	  component.smsForm = formBuilder.group({
		  confirmCode: ['', [
			  Validators.required,
			  Validators.minLength(4),
			  Validators.maxLength(4)
		  ]]
	  });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickSendSMSButtonHandler', () => {
		spyOn(component, 'onClickSendSMSButtonHandler').and.callThrough();
		component.onClickSendSMSButtonHandler();
		expect(component.onClickSendSMSButtonHandler).toHaveBeenCalled();
	});
});
