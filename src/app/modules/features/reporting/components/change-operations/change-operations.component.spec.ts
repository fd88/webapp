import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeOperationsComponent } from './change-operations.component';
import {ReportsService} from "@app/core/services/report/reports.service";
import {CoreModule} from "@app/core/core.module";
import {TranslateModule} from "@ngx-translate/core";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('ChangeOperationsComponent', () => {
  let component: ChangeOperationsComponent;
  let fixture: ComponentFixture<ChangeOperationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			TranslateModule.forRoot(),
			RouterTestingModule,
			HttpClientModule
		],
      declarations: [ ChangeOperationsComponent ],
		providers: [
			ReportsService,
			HttpClient
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
