import { Component, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { ReportsService } from '@app/core/services/report/reports.service';

/**
 * Компонент операций со сменами.
 */
@Component({
	selector: 'app-change-operations',
	templateUrl: './change-operations.component.html',
	styleUrls: ['./change-operations.component.scss']
})
export class ChangeOperationsComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на контейнер для вывода результата.
	 */
	@ViewChild('result', {read: ViewContainerRef, static: true})
	resultRef: ViewContainerRef;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ReportsService} reportsService Сервис для работы с отчетами.
	 * @param {Router} router Сервис для работы с маршрутизацией.
	 */
	constructor(
		readonly reportsService: ReportsService,
		private readonly router: Router
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.reportsService.lastScreenData$
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(v => this.resultRef.element.nativeElement.innerHTML = v);
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
