import { Component, Input } from '@angular/core';
import { UserPaymentOperationsService } from '@app/reporting/services/user-payment-operations.service';
import { FormGroup } from '@angular/forms';

/**
 * Требуемая длина штрих-кода.
 */
const REQ_CODE_LEN = 16;

/**
 * Компонент ввода штрих-кода выигрышного билета для вывода денег.
 */
@Component({
	selector: 'app-request-code-page',
	templateUrl: './request-code-page.component.html',
	styleUrls: ['./request-code-page.component.scss']
})
export class RequestCodePageComponent  {

	/**
	 * Форма ввода штрих-кода для вывода денег.
	 */
	@Input() withdrawForm: FormGroup;

	/**
	 * Конструктор компонента.
	 * @param userPaymentOperationsService Сервис для работы с финансовыми операциями.
	 */
	constructor(readonly userPaymentOperationsService: UserPaymentOperationsService) { }

	/**
	 * Обработчик изменения введенного штрих-кода.
	 * @param reqCode Введенный штрих-код.
	 */
	onReqCodeChange(reqCode: string): void {
		this.userPaymentOperationsService.isValidCodeInput = reqCode ? (reqCode.length <= REQ_CODE_LEN) : true;
	}

	/**
	 * Очистка введенного штрих-кода (сброс формы)
	 */
	clearReqCodeInput(): void {
		this.withdrawForm.reset();
	}
}
