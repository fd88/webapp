import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequestCodePageComponent } from './request-code-page.component';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpService } from '@app/core/net/http/services/http.service';

describe('RequestCodePageComponent', () => {
  let component: RequestCodePageComponent;
  let fixture: ComponentFixture<RequestCodePageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestCodePageComponent ],
		imports:[
			TranslateModule.forRoot(),
			RouterTestingModule.withRoutes([]),
			SharedModule,
			FormsModule,
			ReactiveFormsModule,
			HttpClientTestingModule
		],
		providers: [
			FormBuilder,
			LotteriesService,
			HttpService
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestCodePageComponent);
    component = fixture.componentInstance;
    const formBuilder = new FormBuilder();
    component.withdrawForm = formBuilder.group({
		rcptBarcode: ['1234567890123456', [
			Validators.required,
			Validators.minLength(16),
			Validators.maxLength(16)
		]],
	});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check onReqCodeChange method', () => {
	  component.onReqCodeChange('');
	  expect(component.userPaymentOperationsService.isValidCodeInput).toBeTruthy();

	  component.onReqCodeChange('12345678');
	  expect(component.userPaymentOperationsService.isValidCodeInput).toBeTruthy();

	  component.onReqCodeChange('1234567890123456');
	  expect(component.userPaymentOperationsService.isValidCodeInput).toBeTruthy();

	  component.onReqCodeChange('12345678901234560');
	  expect(component.userPaymentOperationsService.isValidCodeInput).toBeFalsy();
  });

	it('check clearReqCodeInput method', () => {
		expect(component.withdrawForm.controls['rcptBarcode'].value).toEqual('1234567890123456');
		component.clearReqCodeInput();
		expect(component.withdrawForm.controls['rcptBarcode'].value).toBeNull();
	});


});
