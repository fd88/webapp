import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPaymentOperationsComponent } from './user-payment-operations.component';
import {ReportingDataService} from "@app/reporting/services/reporting-data.service";
import {UPORefillComponent} from "@app/reporting/components/uporefill/uporefill.component";
import {UPOWithdrawalComponent} from "@app/reporting/components/upowithdrawal/upowithdrawal.component";
import {TranslateModule} from "@ngx-translate/core";
import {SharedModule} from "@app/shared/shared.module";
import {OneButtonCustomComponent} from "@app/shared/components/one-button-custom/one-button-custom.component";
import {CustomScrollComponent} from "@app/shared/components/custom-scroll/custom-scroll.component";
import {ButtonsGroupComponent} from "@app/shared/components/buttons-group/buttons-group.component";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {UserPaymentOperation} from "@app/reporting/interfaces/ioperation-menu-item";

describe('UserPaymentOperationsComponent', () => {
  let component: UserPaymentOperationsComponent;
  let fixture: ComponentFixture<UserPaymentOperationsComponent>;
  let reportingDataService: ReportingDataService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			SharedModule,
			HttpClientModule,
			RouterTestingModule,
			CoreModule
		],
      declarations: [
		  UserPaymentOperationsComponent,
		  UPORefillComponent,
		  UPOWithdrawalComponent,
		  OneButtonCustomComponent,
		  CustomScrollComponent,
		  ButtonsGroupComponent
	  ],
		providers: [
			ReportingDataService,
			HttpClient,
			HttpService
		]
    })
    .compileComponents();

	reportingDataService = TestBed.inject(ReportingDataService);
	reportingDataService.fullMenuItems = [
		  {
			  label: 'reporting.menu.1',
			  path: '1',
			  isActive: false,
			  subMenuItems: [
				  { label: 'reporting.menu.2', path: '2', isActive: false, autoprint: false }
			  ]
		  }
	];

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPaymentOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onCloseDialog handler', () => {
		component.onCloseDialog();
		expect(component.opHelpType).toEqual(UserPaymentOperation.Refill);
	});

	it('test onCloseDialog handler', () => {
		component.onCloseDialog();
		expect(component.opHelpType).toEqual(UserPaymentOperation.Refill);
	});

	it('test onSelectHelpType handler', () => {
		const childFixture = TestBed.createComponent(ButtonsGroupComponent);
		component.helpType = childFixture.componentInstance;
		childFixture.detectChanges();
		component.helpType.buttons = [{
			index: 0,
			label: 'test',
			selected: false,
			disabled: false
		}];
		component.onSelectHelpType({
			index: 0,
			label: 'test',
			selected: false,
			disabled: false
		});
		expect(component.opHelpType).toBeUndefined();
	});
});
