import { Component, OnInit, ViewChild } from '@angular/core';
import { IOperationListItem } from '@app/reporting/interfaces/ioperation-list-item';
import { ReportingDataService } from '@app/reporting/services/reporting-data.service';
import {
	ButtonGroupMode,
	ButtonGroupStyle,
	ButtonsGroupComponent,
	IButtonGroupItem
} from '@app/shared/components/buttons-group/buttons-group.component';
import { UserPaymentOperation } from '@app/reporting/interfaces/ioperation-menu-item';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

/**
 * Компонент пользовательских финансовых операций.
 */
@Component({
	selector: 'app-user-payment-operations',
	templateUrl: './user-payment-operations.component.html',
	styleUrls: ['./user-payment-operations.component.scss']
})
export class UserPaymentOperationsComponent implements OnInit {

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Предыдущая операция.
	 * @private
	 */
	private previousOperation: IOperationListItem;

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список операций по аккаунту юзера
	 */
	readonly UserPaymentOperation = UserPaymentOperation;

	/**
	 * Перечисление режимов работы группы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	/**
	 * Перечисление стилей группы кнопок.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Ссылка на компонент переключателя для показа справочной информации.
	 */
	@ViewChild('helpType', {static: false}) helpType: ButtonsGroupComponent;

	/**
	 * Текущая операция.
	 */
	currentOperation: IOperationListItem;

	/**
	 * Тип справочной информации.
	 */
	opHelpType: UserPaymentOperation = UserPaymentOperation.Refill;

	/**
	 * Конструктор компонента.
	 * @param reportingDataService Сервис данных для отчетов.
	 * @param http Клиент для работы с http-запросами.
	 * @param translateService Сервис для работы с мультиязычностью.
	 */
	constructor(
		readonly reportingDataService: ReportingDataService,
		readonly http: HttpClient,
		readonly translateService: TranslateService) {
	}

	/**
	 * Обработчик события закрытия диалога.
	 */
	onCloseDialog(): void {
		this.reportingDataService.currentMenuIndex = this.reportingDataService.previousMenuIndex;
		this.reportingDataService.currentSubMenuIndex = this.reportingDataService.previousSubMenuIndex;
		this.reportingDataService.selectMenuItem(this.reportingDataService.currentMenuIndex, this.reportingDataService.currentSubMenuIndex);
		this.opHelpType = UserPaymentOperation.Refill;
	}

	/**
	 * Обработчик события выбора типа справочной информации.
	 * @param button Выбранная кнопка.
	 */
	onSelectHelpType(button: IButtonGroupItem): void {
		this.opHelpType = this.helpType.getValueByButton(button) as UserPaymentOperation;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.reportingDataService.customOperation$$.subscribe((op: IOperationListItem) => {
			this.previousOperation = {...this.currentOperation};
			this.currentOperation = op;
		});
	}

}
