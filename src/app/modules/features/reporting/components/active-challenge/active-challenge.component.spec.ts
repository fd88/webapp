import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveChallengeComponent } from './active-challenge.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {UserLoyaltyAuthModule} from "../../../user-loyalty-auth/user-loyalty-auth.module";
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('ActiveChallengeComponent', () => {
  let component: ActiveChallengeComponent;
  let fixture: ComponentFixture<ActiveChallengeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			UserLoyaltyAuthModule,
			HttpClientTestingModule
		],
    	declarations: [ ActiveChallengeComponent ],
		providers: [
			TranslateService,
			UserLoyaltyAuthService,
			AppStoreService,
			HttpService
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
