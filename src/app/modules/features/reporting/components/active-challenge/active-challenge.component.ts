import { Component, OnInit } from '@angular/core';
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";

@Component({
  selector: 'app-active-challenge',
  templateUrl: './active-challenge.component.html',
  styleUrls: ['./active-challenge.component.scss']
})
export class ActiveChallengeComponent implements OnInit {

  constructor(
	  private readonly userLoyaltyAuthService: UserLoyaltyAuthService
  ) { }

  ngOnInit(): void {

  }

}
