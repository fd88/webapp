import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EmitLoyaltyCardComponent } from './emit-loyalty-card.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {TranslateModule, TranslateService, TranslateStore} from "@ngx-translate/core";
import {MaskedInputComponent} from "@app/shared/components/masked-input/masked-input.component";
import {
	MslInputWithKeyboardComponent
} from "@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {
	UserLoyaltyAuthComponent
} from "../../../user-loyalty-auth/components/user-loyalty-auth/user-loyalty-auth.component";
import {SharedModule} from "@app/shared/shared.module";
import {RouterTestingModule} from "@angular/router/testing";
import {CameraModule} from "../../../camera/camera.module";
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {of} from "rxjs";
import {ErrorHandler} from "@angular/core";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";

describe('EmitLoyaltyCardComponent', () => {
  let component: EmitLoyaltyCardComponent;
  let fixture: ComponentFixture<EmitLoyaltyCardComponent>;
  let userLoyaltyAuthService: UserLoyaltyAuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			FormsModule,
			ReactiveFormsModule,
			HttpClientTestingModule,
			SharedModule,
			RouterTestingModule,
			CameraModule
		],
      	declarations: [
			EmitLoyaltyCardComponent,
			MaskedInputComponent,
			MslInputWithKeyboardComponent,
			MslInputPinComponent,
			UserLoyaltyAuthComponent,
			ScannerInputComponent
		],
		providers: [
			AppStoreService,
			HttpService,
			TranslateService,
			TranslateStore,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			}
		]
    })
    .compileComponents();
	 userLoyaltyAuthService = TestBed.inject(UserLoyaltyAuthService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitLoyaltyCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

	it('should create', () => {
	expect(component).toBeTruthy();
	});

	it('test ngOnInit lifecycle hook', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test ngOnInit lifecycle hook 2', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test onGoForward handler', () => {
		userLoyaltyAuthService.step = 2;
		component.onGoForward();
		expect(userLoyaltyAuthService.step).toBe(3);

		userLoyaltyAuthService.step = 3;
		component.onGoForward();
		expect(userLoyaltyAuthService.step).toBe(4);
	});

	it('test onSMSCodeSubmit handler', () => {
		userLoyaltyAuthService.step = 1;
		component.onSMSCodeSubmit(111);
		expect(userLoyaltyAuthService.step).toBe(2);
	});

	it('test onSubmitCardBarcode handler', () => {
		userLoyaltyAuthService.assignCard = () => of({
			phone: '+380681234567',
			user_id: 1234,
			card_num: '11111111',
			err_code: '0',
			err_descr: '',
			requestId: '12345',
			errorCode: 0,
			errorDesc: ''
		});
		spyOn(component, 'onSubmitCardBarcode').and.callThrough();
		component.onSubmitCardBarcode();
		expect(component.onSubmitCardBarcode).toHaveBeenCalled();
	});

	it('test onSubmitCardBarcode handler 2', () => {
		userLoyaltyAuthService.assignCard = () => of({
			phone: '+380681234567',
			user_id: 1234,
			card_num: '11111111',
			err_code: '1122',
			err_descr: 'Test error',
			requestId: '12345',
			errorCode: 1122,
			errorDesc: 'Test error'
		});
		spyOn(component, 'onSubmitCardBarcode').and.callThrough();
		component.onSubmitCardBarcode();
		expect(component.onSubmitCardBarcode).toHaveBeenCalled();
	});
});
