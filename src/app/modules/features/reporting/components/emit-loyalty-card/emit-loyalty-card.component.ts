import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GreenButtonStyle} from "@app/util/utils";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {timer} from "rxjs";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {IResponse} from "@app/core/net/http/api/types";
import {IError} from "@app/core/error/types";
import {DialogError} from "@app/core/error/dialog";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {GetCardUserResp} from "@app/core/net/http/api/models/get-card-user";
import {concatMap, delay, finalize, tap} from "rxjs/operators";
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";
import {ILoyaltyStepResult} from "../../../user-loyalty-auth/interfaces/i-loyalty-step-result";
import {CameraService} from "@app/core/services/camera.service";

@Component({
  selector: 'app-emit-loyalty-card',
  templateUrl: './emit-loyalty-card.component.html',
  styleUrls: ['./emit-loyalty-card.component.scss']
})
export class EmitLoyaltyCardComponent implements OnInit, OnDestroy {
	readonly GreenButtonStyle = GreenButtonStyle;

	/**
	 * Маска ввода тел. номера
	 */
	readonly phoneMask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

	/**
	 * Форма
	 */
	form: FormGroup;

	userInfo: ILoyaltyStepResult | null = null;

	/**
	 * Показана ли камера.
	 */
	isCameraShown = true;

	/**
	 * Инициализируется ли камера
	 */
	isCameraShowing = false;

	step = 1;

	@ViewChild('smsControl') smsControl: MslInputPinComponent;

	@ViewChild('scannerInput') scannerInput: ScannerInputComponent;

	constructor(
		private readonly formBuilder: FormBuilder,
		readonly appStoreService: AppStoreService,
		private readonly cameraService: CameraService,
		readonly userLoyaltyAuthService: UserLoyaltyAuthService,
		private readonly dialogInfoService: DialogContainerService
	) { }

	onAuthResult(result: ILoyaltyStepResult): void {
		this.step = result.step;
		if (result.error) {
			this.errorsHandler(result.error);
		} else if (this.step === 3) {
			this.userInfo = result;
		}
		timer(200)
			.subscribe(() => {
				if (this.scannerInput) {
					this.scannerInput.setFocus();
				}
			});
		this.disableCancel();
	}

	ngOnInit(): void {
		this.form = this.formBuilder.group({
			cardNum: ['', [Validators.required]]
		});
	}

	ngOnDestroy(): void {
		this.onGoToStart();
	}

	onGoToStart(): void {
		this.step = 1;
		this.userLoyaltyAuthService.lastEnteredPhone = '';
		this.userLoyaltyAuthService.errorText = '';
		this.cameraService.changeState(false);
		if (this.form?.controls['cardNum']) {
			this.form.controls['cardNum'].setValue('');
		}
	}

	onGoForward(): void {
		this.step++;
		if (this.step === 4) {
			timer(200)
				.subscribe(() => {
					if (this.scannerInput) {
						this.scannerInput.setFocus();
					}
				});
			this.disableCancel();
		} else {
			this.cameraService.changeState(false);
		}
	}

	onSMSCodeSubmit(event: any): void {
		this.step++;
		console.log(event);
	}

	disableCancel(): void {
		this.isCameraShowing = true;
		timer(2000)
			.subscribe(() => {
				this.isCameraShowing = false;
			});
	}


	/**
	 * Обработчик изменения поля ввода пин-кода.
	 */
	onPinCodeChange(): void {
		this.appStoreService.smsCodeError = false;
	}

	/**
	 * Обработчик нажатия кнопки "Повторить СМС".
	 */
	onRepeatSMS(): void {
		this.sendSMS();
	}

	sendSMS(): void {
		// this.step++;
	}

	onSubmitCardBarcode(): void {
		this.userLoyaltyAuthService.isLoading = true;
		this.cameraService.changeState(false);
		const cardNum = this.form.controls['cardNum'].value;
		this.form.controls['cardNum'].setValue('');
		this.userLoyaltyAuthService.assignCard(cardNum)
			.pipe(
				finalize(() => {
					this.userLoyaltyAuthService.isLoading = false;
				})
			)
			.subscribe({
				next: (resp) => {
					if (+resp.err_code === 0) {
						this.step = 5;
					} else {
						this.errorsHandler(resp as unknown as IResponse);
					}
				},
				error: err => {
					this.errorsHandler(err);
				}
			});
	}

	private errorsHandler(error: IResponse | IError): void {
		const errCode = +(error as IResponse).err_code || +(error as IError).code;
		const message = (error as IResponse).err_descr || (error as IError).message;
		const details = (error as IError).messageDetails || '';
		const dialogError = new DialogError(errCode, message, details);
		this.dialogInfoService.showOneButtonError(dialogError, {
			text: 'dialog.dialog_button_continue'
		});
		if (this.step >= 3) {
			this.cameraService.changeState(true);
		}
	}
}
