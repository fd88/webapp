import { Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ReportingDataService } from '@app/reporting/services/reporting-data.service';
import {
	getDefaultValue,
	getFieldControlName,
	IReportField,
	ReportInputField,
	ReportsService,
	ReportsId
} from '@app/core/services/report/reports.service';
import { ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { TmlBmlInputStoreService } from '@app/tml-bml-input/services/tml-bml-input-store.service';
import { AuthService } from '@app/core/services/auth.service';
import { InputOperationFieldComponent } from '@app/reporting/components/input-operation-field/input-operation-field.component';
import { ButtonsGroupComponent } from '@app/shared/components/buttons-group/buttons-group.component';
import { DurationDay } from '@app/util/utils';
import { combineLatest, Observable, Subject, timer } from 'rxjs';
import { filter, finalize, map, takeUntil } from 'rxjs/internal/operators';
import { GetReportDataResp } from '@app/core/net/http/api/models/get-report-data';
import { NetError } from '@app/core/error/types';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { INavigationMenuItem } from '@app/shared/components/navigation-menu.component';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';
import { InputTag } from '@app/core/services/report/tags';
import { LogOutService } from '@app/logout/services/log-out.service';
import { debounceTime } from 'rxjs/operators';
import { PrintData } from '@app/core/net/ws/api/models/print/print-models';
import { PrintService } from '@app/core/net/ws/services/print/print.service';

/**
 * Функция валидации формы.
 */
const validateForm = (): { [key: string]: boolean } => undefined;

/**
 * Компонент отчетов.
 */
@Component({
	selector: 'app-reporting',
	templateUrl: './reporting.component.html',
	styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ReportingDataService} reportingDataService Сервис данных для отчетов.
	 * @param {ReportsService} reportsService Сервис отчетов.
	 * @param {ActivatedRoute} route Активный маршрут.
	 * @param {FormBuilder} formBuilder Сервис для создания форм.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {DialogContainerService} dialogInfoService Сервис для отображения диалогов.
	 * @param {TmlBmlInputStoreService} tmlBmlInputStoreService Сервис хранилища ввода ТМЛ/БМЛ.
	 * @param {AuthService} authService Сервис авторизации.
	 * @param logoutService Сервис выхода из системы.
	 * @param printService Сервис печати.
	 */
	constructor(
		readonly reportingDataService: ReportingDataService,
		readonly reportsService: ReportsService,
		private readonly route: ActivatedRoute,
		private readonly formBuilder: FormBuilder,
		readonly appStoreService: AppStoreService,
		private readonly dialogInfoService: DialogContainerService,
		readonly tmlBmlInputStoreService: TmlBmlInputStoreService,
		private readonly authService: AuthService,
		private readonly logoutService: LogOutService,
		private readonly printService: PrintService
	) {}

	/**
	 * Подготовить строку с параметрами для выполнения запроса в ЦС.
	 *
	 * @returns {string} Строка с параметрами, разделенными символом "|".
	 */
	private get requestData(): string {
		const resultArr = [];

		[...this.inputsChildren.toArray()]
			.sort((a, b) =>  a.field.inputIndex - b.field.inputIndex)
			.forEach(item => {
				let data: string;
				if (item.field.fieldTag.type === ReportInputField.DATE_EDIT) {
					data = ReportingComponent.localeIndependentDate(item.value as Date);
				} else if (item.field.fieldTag.type === ReportInputField.TICKET_LIST) {
					data = this.tmlBmlInputStoreService.ticketsList$$.value.map(elem => {
						return elem.bcFrom.barcode === elem.bcTo.barcode
							? elem.bcFrom.barcode
							: `${elem.bcFrom.barcode}-${elem.bcTo.barcode}`;
					})
						.join(';');
				} else {
					data = item.value ? (item.value as string).trim() : '';
				}
				resultArr.push(data);
			});

		return resultArr.join('|');
	}

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Массив дочерних компонентов ввода.
	 */
	@ViewChildren(InputOperationFieldComponent)
	inputsChildren: QueryList<InputOperationFieldComponent>;

	/**
	 * Кнопки выбора периодов.
	 */
	@ViewChild(ButtonsGroupComponent, {static: false})
	datePeriods: ButtonsGroupComponent;

	/**
	 * Компоненты формы.
	 */
	@ViewChildren(InputOperationFieldComponent)
	inputOperationField: QueryList<InputOperationFieldComponent>;

	/**
	 * Перечень вариантов отчетов
	 */
	readonly ReportsId = ReportsId;

	/**
	 * Текущая дата.
	 */
	readonly currDate = Date.now();

	/**
	 * Дата за 30 дней назад
	 */
	readonly monthAgoDate = this.currDate - DurationDay * 30;

	/**
	 * Видимость кнопок периодов для операций и ролей.
	 */
	readonly periodsVisible = {
		4: {
			0: true
		},
		17: {
			0: true
		},
		7: {
			1: true
		}
	};

	/**
	 * Массив полей ввода.
	 */
	inputFields: Array<IReportField> = [];

	/**
	 * Форма для ввода данных и запроса отчета.
	 */
	reportForm: FormGroup;

	/**
	 * Наблюдаемый признак, указывающий на наличие результата по запросу данных и необходимости их отображения.
	 */
	showResultsButtons$: Observable<boolean>;

	/**
	 * Грузится ли отчет
	 */
	isReportLoading = false;

	/**
	 * Генерирует последнюю связку типа: "reportId"-"operationId"
	 */
	currentReportOperationKey$: Observable<string>;

	/**
	 * Валидный ли диапазон дат?
	 */
	isValidPeriod = true;

	/**
	 * Флаг, указывающий на возможность выполнить запрос отчета.
	 */
	canRequest = false;

	/**
	 * Выводить ли кнопки?
	 */
	buttonsExists = false;

	/**
	 * Класс для текущей роли юзера.
	 */
	currentRole: string;

	/**
	 * Признак центрирования результата по вертикали.
	 */
	centeredResult: boolean;

	/**
	 * Признак, указывающий, что элементы формы нужно вывести в обратном порядке.
	 */
	columnReverse = false;

	/**
	 * Счетчик текстовых полей в форме.
	 * Если одно, то не скрываем клавиатуру по клику на свободной области.
	 */
	textFieldsCount = 0;

	/**
	 * Признак того, что мы находимся под менеджером в инвентаризации билетов моментальных лотерей.
	 */
	managerInvent = false;

	/**
	 * Начальная дата периода
	 */
	periodStartDate = this.currDate;

	/**
	 * Открыто ли меню на 360
	 */
	opMenuOpened = false;

	/**
	 * Признак того, что кнопка печати отчета неактивна.
	 */
	disabledPrintButton$: Observable<boolean>;

	/**
	 * Последние данные для печати.
	 */
	lastPrintData: Array<PrintData>;

	// -----------------------------
	//  Private properties
	// -----------------------------

	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Возвращает не зависящую от локали дату, разделенную точками.
	 * @param data Дата
	 */
	private static localeIndependentDate(data: Date): string {
		const day = data.getDate()
			.toString();
		const intMonth = data.getMonth() + 1;
		const month = (intMonth > 9) ? intMonth.toString() : `0${intMonth.toString()}`;
		const year = data.getFullYear()
			.toString();

		return `${day}.${month}.${year}`;
	}

	/**
	 * Обработчик нажатия на операцию в выбранном отчете.
	 *
	 * @param path Путь к операции
	 */
	onClickOperationHandler(path: string): void {
		this.buttonsExists = false;
		this.reportingDataService.currentOperation$$.next(path);
		this.opMenuOpened = !this.opMenuOpened;
	}

	/**
	 * Обработчик нажатия кнопки "Запросить".
	 * Выполняет запрос для получения первичных данных.
	 */
	onClickRequestHandler(): void {
		if (this.isReportLoading || !this.canRequest) {
			return;
		}

		this.isReportLoading = true;
		this.checkCanRequest();

		this.reportsService.getReportByParams(this.requestData)
			.pipe(takeUntil(this.unsubscribe$$))
			.pipe(finalize(() => this.isReportLoading = false))
			.subscribe((content: GetReportDataResp) => {

				this.reportsService.lastReportResponse$$.next(content);

				// пропарсить данные для экрана
				const screenData = this.reportsService.parseReport('screen', content, this.reportsService.currentReportId) as string;
				this.reportsService.lastScreenData$.next(screenData);

				this.reportingDataService.inResult$$.next(true);
			}, error => {
				this.reportsService.lastReportResponse$$.next(undefined);
				this.reportsService.lastScreenData$.next('');
				const err = (error instanceof NetError) ? error : new DialogError(ErrorCode.GetReport, 'dialog.get_report_result_error');
				this.dialogInfoService.showOneButtonError(err, {
					click: () => {
						if (error.code && (error.code === 4313) || (error.code === 4318)) {
							this.logoutService.logoutOperator();
						} else {
							this.tmlBmlInputStoreService.ticketsList$$.next([]);
							this.reportForm.reset({'dateedit-1': new Date(), 'dateedit-2': new Date()});
							this.updateButtonsState();
							this.goOnFirstScreen();
						}
					},
					text: 'dialog.dialog_button_continue'
				});
				Logger.Log.e('ReportsService', 'getReportByParams ERROR -> %s', error.message)
					.console();
			});
	}

	/**
	 * Слушатель обновления области с прокруткой.
	 * Определяет нужно ли центрировать результат по вертикали.
	 * @param needsScroll Нужна ли прокрутка
	 */
	onScrollUpdated(needsScroll: boolean): void {
		const tmr = timer(0)
			.subscribe(() => {
				this.centeredResult = !needsScroll;
				tmr.unsubscribe();
			});
	}

	/**
	 * Обработчик клавиши Enter на поле ввода.
	 *
	 * @param {IReportField} field Поле ввода
	 */
	onCatchEnterHandler(field: IReportField): void {
		if (this.reportForm.valid) {
			this.onClickRequestHandler();
		} else {
			const arr = this.inputsChildren.toArray();
			const item = arr.find(f => f.field.controlName === field.controlName);
			if (item) {
				const oldIdx = arr.indexOf(item);
				const newIdx = (oldIdx === arr.length - 1 ? 0 : oldIdx + 1);
				arr[newIdx].setFocus();
			}
		}
	}


	/**
	 * Слушатель изменения составного контрола в реактивной форме
	 *
	 * @param $event Передаваемое значение
	 */
	onInputChanged($event: string | Date | Array<ITmlBmlListItem>): void {
		this.updateButtonsState();
		this.checkCanRequest();
	}

	/**
	 * Вернуться назад на форму запроса
	 */
	goOnFirstScreen(): void {
		this.reportingDataService.inResult$$.next(false);
		this.updateButtonsState();
		this.checkCanRequest();
	}

	/**
	 * Обработчик выбора периода.
	 * В зависимости от периода выставляет нужный интервал дат - день или месяц.
	 *
	 * @param {number} startDate Начальная дата
	 */
	onSelectPeriod(startDate: number): void {
		this.reportForm.controls['dateedit-1'].setValue(new Date(startDate));
		this.reportForm.controls['dateedit-2'].setValue(new Date(this.currDate));
		this.isValidPeriod = true;
		this.checkCanRequest();
	}

	/**
	 * Слушатель нажатия на кнопку "Печать".
	 */
	onClickPrintHandler(): void {
		if (this.printService.isReady()) {
			this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.report_printing_wait_info');
			this.reportsService.printReport(this.lastPrintData)
				.then(() => {
					this.dialogInfoService.hideActive();

					// если разрешен признак автопечати, то остаемся в состоянии печати
					// для возможности сделать еще одну возможность печати
					this.reportsService.lastReportResponse$$.next(undefined);
					this.tmlBmlInputStoreService.ticketsList$$.next([]);
					this.reportForm.reset({'dateedit-1': new Date(), 'dateedit-2': new Date()});
					this.goOnFirstScreen();
				})
				.catch(err => {
					Logger.Log.e('ReportingListComponent', 'printLastRequestedReport() -> %s', err.message)
						.console();
					this.dialogInfoService.hideActive();
				});
		} else {
			Logger.Log.i('ReportingComponent', 'onClickPrintHandler -> printer not ready')
				.console();

			this.dialogInfoService.showOneButtonInfo('dialog.attention', 'dialog.printer_not_ready_info', {
				text: 'dialog.dialog_button_continue'
			});
		}
	}

	/**
	 * Вспомогательная функция для отслеживания изменений в массиве элементов.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByField = (index, item: IReportField) => item;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Проверка на видимость кнопок периодов.
	 */
	private checkPeriodsVisible(): void {
		this.buttonsExists = this.periodsVisible[this.reportingDataService.currentOperation$$.value]
			&& this.periodsVisible[this.reportingDataService.currentOperation$$.value][this.appStoreService.operator.value.accessLevel];
	}

	/**
	 * Очистить форму от элементов ввода.
	 */
	private clearFormControls(): void {
		Object.keys(this.reportForm.controls)
			.forEach(k => this.reportForm.removeControl(k));
		this.inputFields.splice(0);
	}

	/**
	 * Сбросить контент и ответы сервиса.
	 */
	private resetLastData(): void {
		this.reportsService.lastReportResponse$$.next(undefined);
		this.reportsService.lastScreenData$.next('');
	}

	/**
	 * Обновить состояние кнопок выбора периода.
	 */
	private updateButtonsState(): void {
		if (this.inputsChildren) {
			this.inputsChildren.forEach((elem, i) => {
				const firstElem = elem;
				const secondElem = this.inputsChildren.toArray()[i + 1] ? this.inputsChildren.toArray()[i + 1] : undefined;

				if (firstElem.field.fieldTag.type === ReportInputField.DATE_EDIT
					&& secondElem
					&& secondElem.field.fieldTag.type === ReportInputField.DATE_EDIT
				) {
					const currDate = (new Date()).toLocaleDateString();
					const localeDate1 = firstElem.value ? (firstElem.value as Date).toLocaleDateString() : currDate;
					const localeDate2 = secondElem.value ? (secondElem.value as Date).toLocaleDateString() : currDate;
					const m30Date = new Date((new Date()).getTime() - DurationDay * 30).toLocaleDateString();
					if (this.buttonsExists) {
						this.periodStartDate = ((localeDate1 === localeDate2) && (localeDate1 === currDate)) ? this.currDate :
							((localeDate1 === m30Date) && (localeDate2 === currDate)) ? this.monthAgoDate : 0;
					}
				}
			});
		}
	}

	/**
	 * Построить массив с полями ввода типа {@link inputFields} и FormControl для формы.
	 * @param {Array<number>} customSortArr Массив для порядка вывода элементов
	 */
	private makeFormControls(customSortArr?: Array<number>): void {
		const arr = this.reportsService.getInputs();
		// счетчик добавленных полей
		let inputIndex = 0;
		const tempFields = [];
		for (let i = 0; i < arr.length; i += 2) {
			// ожидается 2 элемента: метка и поле ввода
			const labelTag = arr[i];
			const fieldTag = arr[i + 1];
			if (labelTag && fieldTag) {
				const controlName: string = getFieldControlName(fieldTag);
				const hidden = this.parseHiddenProperty(fieldTag);
				const enableBarcodeInput = this.parseEnableBarcodeInputProperty(fieldTag);
				const field: IReportField = {
					controlName,
					labelTag,
					fieldTag,
					hidden,
					enableBarcodeInput,
					index: customSortArr ? customSortArr[inputIndex] : inputIndex,
					inputIndex,
					placeholder: ''
				};
				inputIndex++;
				tempFields.push(field);

				// добавить новый FormControl
				const fc = new FormControl(getDefaultValue(field, undefined), this.parseValidators(field));
				const fcName = getFieldControlName(field.fieldTag);
				this.reportForm.addControl(fcName, fc);
			} else {
				console.log('+++ WARNING!!!!', labelTag, fieldTag);
			}

			if ((arr[i] && (arr[i].type === ReportInputField.DATE_EDIT)) ||
				(arr[i + 1] && (arr[i + 1].type === ReportInputField.DATE_EDIT))) {
				this.buttonsExists = true;
			}

			if ((arr[i] && (arr[i].type === ReportInputField.TICKET_LIST)) ||
				(arr[i + 1] && (arr[i + 1].type === ReportInputField.TICKET_LIST))) {
				this.tmlBmlInputStoreService.ticketsList$$.next([]);
			}
		}

		// сортировка массива полей по индексам
		tempFields.sort((a: IReportField, b: IReportField) => a.index - b.index);
		// подсчет ТЕКСТОВЫХ полей ввода
		this.textFieldsCount = tempFields.filter(elem => (elem.fieldTag.type === ReportInputField.LINE_EDIT
			|| elem.fieldTag.type === ReportInputField.TICKET_LIST)).length;
		this.inputFields = tempFields;

		// проставить признак автофокуса на первом (текстовом) элементе ввода
		const rf = this.inputFields.find(f => f.fieldTag.type === ReportInputField.LINE_EDIT && !f.hidden);
		if (rf) {
			rf.autofocus = true;
		}
	}

	/**
	 * Пропарсить поле ввода и определить свойство {@link IReportField.enableBarcodeInput enableBarcodeInput},
	 * для дальнейшей блокировки или разрешение использования сканера штрих-кодов на данном поле.
	 *
	 * @param {InputTag} fieldTag Поле ввода.
	 */
	private parseEnableBarcodeInputProperty(fieldTag: InputTag): boolean {
		const operationId = +this.reportsService.operationId;

		return (this.reportsService.currentReportId === ReportsId.PaperInstant)
			&& (operationId === 9 || operationId === 14)
			&& (fieldTag.param_number !== '1');
	}

	/**
	 * Проверить, скрытое ли поле.
	 *
	 * @param {InputTag} fieldTag Поле ввода.
	 */
	private parseHiddenProperty(fieldTag: InputTag): boolean {
		return +this.reportsService.operationId === 4
			&& this.appStoreService.operator.getValue().isOperator
			&& fieldTag.type === ReportInputField.LINE_EDIT;
	}

	/**
	 * Определить кастомный набор валидаторов для формы отчета по ИД отчета {@link ReportsId}
	 * и ИД операции {@link reportsService.operationId}.
	 * По умолчанию все поля обязательны.
	 *
	 * @param {IReportField} field Поле, для которого необходимо определить кастомный набор валидаторов.
	 */
	private parseValidators(field: IReportField): Array<((control: AbstractControl) => (ValidationErrors | null))> {
		const validators = [Validators.required];
		const operationId = +this.reportsService.operationId;

		if (operationId === 4) {
			// у отчета "звіт за період по лотереям" код распространителя необязателен
			if (field.fieldTag.type === ReportInputField.LINE_EDIT) {
				validators.splice(0);
			}
		} else if (operationId === 2) {
			validators.splice(0);
		}

		return validators;
	}

	/**
	 * Проверка возможности выполнить запрос.
	 */
	private checkCanRequest(): void {
		this.canRequest = !(this.reportForm.disabled || this.reportForm.invalid || this.isReportLoading);
	}

	/**
	 * Настроить форму.
	 */
	private initForm(): void {
		this.reportForm = this.formBuilder.group({}, {validator: validateForm});
		this.reportForm.valueChanges
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(() => {
				if (this.reportingDataService.currentOperation$$.value !== '22') {
					this.resetLastData();
				}
			});
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.initForm();

		// определить видимость контейнера с кнопоками НАЗАД и ПЕЧАТЬ
		this.showResultsButtons$ = combineLatest([
			this.reportsService.lastReportResponse$$,
			this.reportingDataService.inResult$$
		])
			.pipe(
				takeUntil(this.unsubscribe$$),
				map(v => !!v[0] && v[1])
			);

		this.currentRole = `role-${this.appStoreService.operator.value.accessLevel}`;
		this.reportingDataService.inResult$$.next(false);

		// подписаться на изменение операции
		this.reportingDataService.currentOperation$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				filter(f => !!f),
			)
			.subscribe(operation => {
				this.reportsService.operationId = operation;
				this.columnReverse = this.appStoreService.operator.value.isManager && operation === '4';
				const sortArr = this.columnReverse ? [1, 2, 0] : undefined;
				this.clearFormControls();
				this.makeFormControls(sortArr);
				this.checkPeriodsVisible();
				this.goOnFirstScreen();
			});

		// подписаться на изменение типа отчета
		this.reportingDataService.currentReport$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				filter(f => !!f)
			)
			.subscribe(report => {
				this.reportsService.currentReportId = report;

				const ol = this.reportingDataService.makeOperationsListByReportId(report);
				if (ol.length) {
					const oper = ol[0];
					oper.isActive = true;
					this.reportingDataService.currentOperation$$.next(oper.path);
					this.checkPeriodsVisible();
				} else {
					this.clearFormControls();
				}
			});

		// подписаться на загруженные данные отчета для подготовки их к печати и валидации кнопки печати
		this.disabledPrintButton$ = this.reportsService.lastReportResponse$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				debounceTime(100),
				map(resp => {
					if (resp) {
						this.lastPrintData = this.reportsService.parseLastLoadedReportToPrint();
						if (this.lastPrintData && this.lastPrintData.length > 0) {
							return false;
						}
					}

					return true;
				})
			);

		// проверить на активацию только секции "ОТЧЕТЫ"
		this.route.data
			.subscribe(() => {
				this.reportingDataService.onlyReports$$.next(true);

				// приготовить первый видимы элемент верхнего меню
				const topMenu = this.reportingDataService.createTopMenu();

				let firstItem: INavigationMenuItem;
				firstItem =  topMenu.find(p => p.path === ReportsId.Reports);

				// если необходимый пункт не найден - выбрать первый попавшийся
				if (!firstItem && topMenu.length > 0) {
					firstItem = topMenu[0];
				}
				//
				// firstItem.isActive = true;
				this.reportingDataService.currentReport$$.next(firstItem.path);
			});

		// подписаться на изменение типа отчета или операции
		this.currentReportOperationKey$ = this.reportingDataService.currentOperation$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				map(currentOp => {
					return `report-${currentOp}`;
				}));

		this.checkCanRequest();

		// подписка на сканер штрихкодов, на случай, если отсканировали штрихкод,
		// стоя на поле "Штрихкод первого билета", сразу переходить на второе поле
		this.reportingDataService.currentReport$$.next(null);
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.reportingDataService.currentReport$$.next(undefined);
		this.reportingDataService.currentOperation$$.next(undefined);
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
