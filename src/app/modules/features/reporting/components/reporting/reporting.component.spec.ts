import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportingComponent } from './reporting.component';
import {ReportingDataService} from "@app/reporting/services/reporting-data.service";
import {ReportsService} from "@app/core/services/report/reports.service";
import {SharedModule} from "@app/shared/shared.module";
import {RouterTestingModule} from "@angular/router/testing";
import {TranslateModule} from "@ngx-translate/core";
import {ROUTES} from "../../../../../app-routing.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {SanitizePipe} from "@app/reporting/pipes/sanitize.pipe";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {TmlBmlInputStoreService} from "@app/tml-bml-input/services/tml-bml-input-store.service";
import {AuthService} from "@app/core/services/auth.service";
import {LogOutService} from "@app/logout/services/log-out.service";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {TmlBmlInputModule} from "@app/tml-bml-input/tml-bml-input.module";
import {LogOutModule} from "@app/logout/log-out.module";
import {CustomScrollComponent} from "@app/shared/components/custom-scroll/custom-scroll.component";
import {
	InputOperationFieldComponent
} from "@app/reporting/components/input-operation-field/input-operation-field.component";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_100} from "../../../mocks/game100-draws";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {Logger} from "@app/core/net/ws/services/log/logger";

describe('ReportingComponent', () => {
  let component: ReportingComponent;
  let fixture: ComponentFixture<ReportingComponent>;
  let appStoreService: AppStoreService;
  let reportsService: ReportsService;
  let reportingDataService: ReportingDataService;
  let printService: PrintService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			SharedModule,
			RouterTestingModule.withRoutes(ROUTES),
			HttpClientModule,
			TranslateModule.forRoot({}),
			CoreModule,
			FormsModule,
			ReactiveFormsModule,
			TmlBmlInputModule,
			LogOutModule
		],
      	declarations: [
			ReportingComponent,
			SanitizePipe,
			CustomScrollComponent,
			InputOperationFieldComponent
		],
		providers: [
			ReportingDataService,
			ReportsService,
			HttpClient,
			HttpService,
			FormBuilder,
			AppStoreService,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			},
			TmlBmlInputStoreService,
			AuthService,
			LogOutService,
			PrintService,
			Logger
		]
    })
    .compileComponents();

	  const op = JSON.parse(SESS_DATA.data[0].value);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
	  appStoreService.isLoggedIn$$.next(true);

	  const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);

	  appStoreService.Draws = new LotteriesDraws();
	  appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);

	  reportsService  = TestBed.inject(ReportsService);
	  await reportsService.reload();

	  TestBed.inject(LogService);
	  TestBed.inject(Logger);

	  reportingDataService = TestBed.inject(ReportingDataService);
	  printService = TestBed.inject(PrintService);

    fixture = TestBed.createComponent(ReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickRequestHandler', () => {
		component.ngOnInit();
		spyOn(component, 'onClickRequestHandler').and.callThrough();
		component.onClickRequestHandler();
		expect(component.onClickRequestHandler).toHaveBeenCalled();
	});

	it('test onClickOperationHandler', () => {
		component.ngOnInit();
		component.onClickOperationHandler('22');
		expect(reportingDataService.currentOperation$$.value).toEqual('22');
	});

	it('test onCatchEnterHandler', () => {
		component.ngOnInit();
		spyOn(component, 'onCatchEnterHandler').and.callThrough();
		component.onCatchEnterHandler({
			controlName: 'date-1',
			labelTag: {
				type: 'dateedit',
				param_number: '0',
				caption: {
					text: 'date',
					parent_tag: 'caption'
				},
				parent_tag: 'inputs',
				class_name: ''
			},
			fieldTag: {
				type: 'dateedit',
				param_number: '0',
				caption: {
					text: 'date',
					parent_tag: 'caption'
				},
				parent_tag: 'inputs',
				class_name: ''
			},
			index: 0,
			inputIndex: 0
		});
		expect(component.onCatchEnterHandler).toHaveBeenCalled();
	});

	it('test onInputChanged Handler', () => {
		component.ngOnInit();
		spyOn(component, 'onInputChanged').and.callThrough();
		component.onInputChanged(new Date());
		expect(component.onInputChanged).toHaveBeenCalled();
	});

	it('test onSelectPeriod Handler', () => {
		component.ngOnInit();
		component.onSelectPeriod(Date.now() - 5 * 24 * 60 * 60 * 1000);
		expect(component.isValidPeriod).toBeTruthy();
	});

	it('test onClickPrintHandler', () => {
		component.ngOnInit();
		spyOn(component, 'onClickPrintHandler').and.callThrough();
		component.onClickPrintHandler();
		expect(component.onClickPrintHandler).toHaveBeenCalled();
	});

	it('test onClickPrintHandler 2', () => {
		component.ngOnInit();
		component.lastPrintData = [];
		printService.isReady = () => true;
		spyOn(component, 'onClickPrintHandler').and.callThrough();
		component.onClickPrintHandler();
		expect(component.onClickPrintHandler).toHaveBeenCalled();
	});
});
