import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';
import { Subject } from 'rxjs';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { IReportField, ReportInputField } from '@app/core/services/report/reports.service';
import { ButtonGroupStyle } from '@app/shared/components/buttons-group/buttons-group.component';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';

/**
 * Компонент ввода на форме отчетов. Тип возможных данных, которые можно задать с помощью этого компонента
 * ограничивается списком {@link ReportInputField}.
 * Данный компонент позволяет создавать реактивные формы.
 */
@Component({
	selector: 'app-input-operation-field',
	templateUrl: './input-operation-field.component.html',
	styleUrls: ['./input-operation-field.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: InputOperationFieldComponent,
			multi: true
		},
		{
			provide: NG_VALIDATORS,
			useExisting: InputOperationFieldComponent,
			multi: true
		}
	]
})
export class InputOperationFieldComponent implements OnInit, OnDestroy, ControlValueAccessor, Validator {

	// -----------------------------
	//  Constants
	// -----------------------------
	/**
	 * Перечисление стилей кнопок, доступных к использованию в данном компоненте.
	 */
	readonly ButtonGroupStyle = ButtonGroupStyle;

	/**
	 * Перечень типов входных полей в отчетах.
	 */
	readonly FIELD = ReportInputField;

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Модель поля ввода.
	 */
	@Input()
	field: IReportField;

	/**
	 * Значение данного поля ввода.
	 */
	@Input()
	value: string | Date | Array<ITmlBmlListItem>;

	/**
	 * Плейсхолдер для полей ввода.
	 */
	@Input()
	placeholder = '';

	/**
	 * Максимальная длина ввода.
	 */
	@Input()
	maxlength = 524288;

	/**
	 * Отступ клавиатуры от низа
	 */
	@Input()
	vkBottomPadding = 150;

	/**
	 * Зафиксировать фокус на поле ввода
	 */
	@Input()
	vkLockFocusOnInput = false;

	/**
	 * Показывать клавиатуру на поле ввода при возврате на форму запроса
	 */
	@Input()
	focusOnReturn = true;

	/**
	 * Показывать иконку клавиатуры на полях ввода
	 */
	@Input()
	showKeyboardIcon = true;

	// -----------------------------
	//  Output properties
	// -----------------------------
	/**
	 * Событие вызывается при нажатии Enter в поле ввода.
	 */
	@Output()
	readonly catchEnter = new EventEmitter();

	/**
	 * Событие вызывается при нажатии на кнопку с иконкой клавиатуры.
	 */
	@Output()
	readonly keyboardIconClick: EventEmitter<MouseEvent> = new EventEmitter();

	/**
	 * Событие вызывается при фокусе на поле ввода.
	 */
	@Output()
	readonly onfocus: EventEmitter<MouseEvent> = new EventEmitter();

	/**
	 * Событие вызывается при потере фокуса с поля ввода.
	 */
	@Output()
	readonly onblur: EventEmitter<MouseEvent> = new EventEmitter();

	/**
	 * Событие изменения даты в одном из календарей
	 */
	@Output()
	readonly dateChanged: EventEmitter<{ newDate: Date; controlName: string; }> = new EventEmitter();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список тиражей.
	 */
	draws: Array<UpdateDrawInfoDraws>;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {ElementRef} elementRef Ссылка на элемент компонента.
	 */
	constructor(
		private readonly appStoreService: AppStoreService,
		private readonly elementRef: ElementRef,
	) {}

	/**
	 * Слушатель события <code>input</code> на поле ввода данных.
	 *
	 * @param {Event | Date | Array<ITmlBmlListItem>} inputData Данные, введенные в поле ввода.
	 */
	onInputHandler(inputData: Event | Date | Array<ITmlBmlListItem>): void {
		const cmp = (arr: Array<ITmlBmlListItem>): string => {
			return Array.isArray(arr)
				? arr
					.map(v => v.bcFrom.barcode + v.bcTo.barcode)
					.reduce((p, c) => p + c, '')
				: '';
		};

		if (Array.isArray(inputData)) {
			if (cmp(inputData) !== cmp(this.value as Array<ITmlBmlListItem>)) {
				this.value = [...inputData];
			} else {
				return;
			}
		} else if (inputData instanceof Date && inputData.getTime() !== (this.value as Date).getTime()) {
			this.value = inputData;
		} else if ((inputData as Event).target) {
			const newValue = ((inputData as Event).target as HTMLInputElement).value;
			if (this.value as string !== newValue) {
				this.value = newValue;
			} else {
				return;
			}
		} else {
			return;
		}

		this.onChange(this.value);
	}

	/**
	 * Обработчик нажатия кнопки Enter на поле ввода
	 */
	onEnterPressedHandler(): void {
		this.catchEnter.emit(this.field);
	}

	/**
	 * Обработчик клика на иконке клавиатуры
	 */
	onKeyboardIconClickHandler(event: MouseEvent): void {
		this.keyboardIconClick.emit(event);
	}

	/**
	 * Устанавливает фокус на поле ввода
	 */
	setFocus(): void {
		const elements = this.elementRef.nativeElement.getElementsByTagName('input');
		const input = elements.item(0) as HTMLInputElement;
		if (input) {
			input.focus();
		}
	}

	/**
	 * Слушатель события фокуса на поле ввода данных.
	 * @param event Передаваемое событие.
	 */
	onFocusHandler(event): void {
		this.onfocus.emit(event);
	}

	/**
	 * Слушатель события потери фокуса с поля ввода данных.
	 * @param event Передаваемое событие.
	 */
	onBlurHandler(event): void {
		this.onblur.emit(event);
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Унаследованное событие от базового класса.
	 * Вызывается в коллбеке при изменении значения UI-элемента
	 * @param value Передаваемое значение
	 */
	private onChange = (value: string | Date | Array<ITmlBmlListItem>) => {};

	/**
	 * Унаследованное событие от базового класса.
	 */
	private onTouched = () => {};

	// -----------------------------
	//  ControlValueAccessor
	// -----------------------------
	/**
	 * Метод, который вызывается при изменении значения UI-элемента
	 * @param fn Передаваемая callback-функция
	 */
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	/**
	 * Метод, который вызывается при касании к UI-элементу
	 * @param fn Передаваемая callback-функция
	 */
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

	/**
	 * Метод для установки недоступности компонента
	 * @param isDisabled Флаг недоступности
	 */
	setDisabledState(isDisabled: boolean): void {
	}

	/**
	 * Метод для программного присвоения значения компоненту
	 * @param obj Значение
	 */
	writeValue(obj: any): void {
		this.value = obj;
	}

	// -----------------------------
	//  Validator
	// -----------------------------
	/**
	 * Регистрирует функцию обратного вызова для выполнения при изменении входных данных валидатора
	 * @param fn Функция обратного вызова
	 */
	registerOnValidatorChange(fn: () => void): void {
	}

	/**
	 * Валидация компонента
	 * @param control Компонент
	 */
	validate(control: AbstractControl): ValidationErrors | null {
		return control.valid ? undefined : control.errors;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.draws = this.appStoreService.Draws.getDraws(LotteryGameCode.TML_BML);
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
