import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputOperationFieldComponent } from './input-operation-field.component';
import {HttpClientModule} from "@angular/common/http";
import {HttpService} from "@app/core/net/http/services/http.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_100} from "../../../mocks/game100-draws";
import {TranslateModule} from "@ngx-translate/core";
import {ITmlBmlListItem} from "@app/tml-bml-input/interfaces/itml-bml-list-item";
import {TML_BML_LIST} from "../../../mocks/mocks";
import {
	CalendarDateInputComponent
} from "../../../calendar/components/calendar-date-input/calendar-date-input.component";
import {CalendarModule} from "../../../calendar/calendar.module";
import {ReactiveFormsModule} from "@angular/forms";

describe('InputOperationFieldComponent', () => {
  let component: InputOperationFieldComponent;
  let fixture: ComponentFixture<InputOperationFieldComponent>;
  let appStoreService: AppStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			HttpClientModule,
			TranslateModule.forRoot(),
			CalendarModule,
			ReactiveFormsModule
		],
      	declarations: [
			  InputOperationFieldComponent,
			  CalendarDateInputComponent
		],
		providers: [
			HttpService
		]
    })
    .compileComponents();

	appStoreService = TestBed.inject(AppStoreService);
	appStoreService.Draws = new LotteriesDraws();
	appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputOperationFieldComponent);
    component = fixture.componentInstance;
	component.field = {
		controlName: 'date-1',
		labelTag: {
			type: 'dateedit',
			param_number: '0',
			caption: {
				text: 'date',
				parent_tag: 'caption'
			},
			parent_tag: 'inputs',
			class_name: ''
		},
		fieldTag: {
			type: 'dateedit',
			param_number: '0',
			caption: {
				text: 'date',
				parent_tag: 'caption'
			},
			parent_tag: 'inputs',
			class_name: ''
		},
		index: 0,
		inputIndex: 0
	};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onInputHandler', () => {
		const ev = new Event('onInputHandler');
		const target = document.createElement('input');
		target.value = '123456';
		component.onInputHandler({...ev, target});
		expect(component.value).toEqual(target.value);
	});

	it('test onInputHandler 2', () => {
		const date = new Date();
		component.value = new Date(Date.now() - 100000);
		component.onInputHandler(date);
		expect((component.value as Date).getTime()).toEqual(date.getTime());
	});

	it('test onInputHandler 3', () => {
		component.onInputHandler([TML_BML_LIST[0]]);
		expect((component.value as Array<ITmlBmlListItem>)).toEqual([TML_BML_LIST[0]]);
	});

	it('test onEnterPressedHandler', () => {
		spyOn(component, 'onEnterPressedHandler').and.callThrough();
		component.onEnterPressedHandler();
		expect(component.onEnterPressedHandler).toHaveBeenCalled();
	});

	it('test onKeyboardIconClickHandler', () => {
		spyOn(component, 'onKeyboardIconClickHandler').and.callThrough();
		component.onKeyboardIconClickHandler(new MouseEvent('onKeyboardIconClickHandler'));
		expect(component.onKeyboardIconClickHandler).toHaveBeenCalled();
	});

	it('test onFocusHandler', () => {
		spyOn(component, 'onFocusHandler').and.callThrough();
		component.onFocusHandler(new FocusEvent('onFocusHandler'));
		expect(component.onFocusHandler).toHaveBeenCalled();
	});

	it('test onBlurHandler', () => {
		spyOn(component, 'onBlurHandler').and.callThrough();
		component.onBlurHandler(new Event('onBlurHandler'));
		expect(component.onBlurHandler).toHaveBeenCalled();
	});

	it('test registerOnChange method', () => {
		spyOn(component, 'registerOnChange').and.callThrough();
		component.registerOnChange(() => {});
		expect(component.registerOnChange).toHaveBeenCalled();
	});

	it('test registerOnTouched method', () => {
		spyOn(component, 'registerOnTouched').and.callThrough();
		component.registerOnTouched(() => {});
		expect(component.registerOnTouched).toHaveBeenCalled();
	});

	it('test setDisabledState method', () => {
		spyOn(component, 'setDisabledState').and.callThrough();
		component.setDisabledState(false);
		expect(component.setDisabledState).toHaveBeenCalled();
	});

	it('test writeValue method', () => {
		component.writeValue('123456');
		expect(component.value).toEqual('123456');
	});

	it('test registerOnValidatorChange method', () => {
		spyOn(component, 'registerOnValidatorChange').and.callThrough();
		component.registerOnValidatorChange(() => {});
		expect(component.registerOnValidatorChange).toHaveBeenCalled();
	});

	it('test setFocus method', () => {
		spyOn(component, 'setFocus').and.callThrough();
		component.setFocus();
		expect(component.setFocus).toHaveBeenCalled();
	});
});
