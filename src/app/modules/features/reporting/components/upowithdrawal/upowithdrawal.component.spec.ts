import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UPOWithdrawalComponent } from './upowithdrawal.component';
import {TranslateModule} from "@ngx-translate/core";
import {UserPaymentOperationsService} from "@app/reporting/services/user-payment-operations.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {CoreModule} from "@app/core/core.module";
import {RequestCodePageComponent} from "@app/reporting/components/request-code-page/request-code-page.component";
import {NotAllowedPageComponent} from "@app/reporting/components/not-allowed-page/not-allowed-page.component";
import {EnterSmsPageComponent} from "@app/reporting/components/enter-sms-page/enter-sms-page.component";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {
	MslInputWithKeyboardComponent
} from "@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {WithdrawalStatus} from "@app/reporting/enums/withdrawal-status.enum";
import {timer} from "rxjs";

describe('UPOWithdrawalComponent', () => {
  let component: UPOWithdrawalComponent;
  let fixture: ComponentFixture<UPOWithdrawalComponent>;
  let appStoreService: AppStoreService;
  let userPaymentOperationsService: UserPaymentOperationsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			CoreModule,
			HttpClientModule,
			ReactiveFormsModule,
			RouterTestingModule
		],
      declarations: [
		  UPOWithdrawalComponent,
		  RequestCodePageComponent,
		  NotAllowedPageComponent,
		  EnterSmsPageComponent,
		  MslInputWithKeyboardComponent
	  ],
		providers: [
			UserPaymentOperationsService,
			BarcodeReaderService,
			HttpClient,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			}
		]
    })
    .compileComponents();
	  const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);
	  userPaymentOperationsService = TestBed.inject(UserPaymentOperationsService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UPOWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickSendSMSButtonHandler', () => {
		component.onClickSendSMSButtonHandler();
		expect(component.sendSMSButtonText).toEqual('reporting.repeat_sms');
	});

	it('test onClickPaymentButtonHandler', () => {
		userPaymentOperationsService.checkCashOutResp = {
			client_trans_id: '12345',
			err_code: 0,
			err_descr: '',
			pay_sum: 1000,
			pay_expire_date: (new Date(Date.now() + 60000)).toISOString(),
			pay_enable: 1,
			reason_code: 123,
			reason_detail: 'Test',
			pay_reg_date: (new Date(Date.now() - 60000)).toISOString(),
			pay_reg_terminal: 123
		};
		spyOn(component, 'onClickPaymentButtonHandler').and.callThrough();
		component.onClickPaymentButtonHandler();
		expect(component.onClickPaymentButtonHandler).toHaveBeenCalled();
	});

	it('test onClickRequestButtonHandler', () => {
		component.onClickRequestButtonHandler();
		expect(component.sendSMSButtonText).toEqual('reporting.send_sms');
	});

	it('test onClickContinueButtonHandler', () => {
		component.onClickContinueButtonHandler();
		expect(userPaymentOperationsService.currentWithdrawalStatus$$.value).toEqual(WithdrawalStatus.RequestCode);
	});

	it('test onGoBack method', () => {
		component.onGoBack();
		expect(userPaymentOperationsService.currentWithdrawalStatus$$.value).toEqual(WithdrawalStatus.RequestCode);
	});
});
