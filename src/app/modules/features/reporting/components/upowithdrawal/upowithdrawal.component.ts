import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MslInputPinComponent } from '@app/shared/components/msl-input-pin/msl-input-pin.component';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { UserPaymentOperationsService } from '@app/reporting/services/user-payment-operations.service';
import { WithdrawalStatus } from '@app/reporting/enums/withdrawal-status.enum';

/**
 * Компонент, предоставляющий функционал для вывода денег со счета игрока.
 * {@link https://igra.msl.ua/cabinet/help/show/65?language=ru Описание процедуры вывода денег со счета игрока}
 */
@Component({
	selector: 'app-upowithdrawal',
	templateUrl: './upowithdrawal.component.html',
	styleUrls: [
		'./upowithdrawal.component.scss',
	]
})
export class UPOWithdrawalComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на компонент ввода пин-кода.
	 */
	@ViewChild(MslInputPinComponent, {static: false})
	mslInputPinComponent: MslInputPinComponent;

	/**
	 * Список состояний на форме вывода денег
	 */
	readonly WithdrawalStatus = WithdrawalStatus;

	/**
	 * Количество цифр в пин-коде.
	 */
	readonly PinCodeLength = 4;

	/**
	 * Флаг видимости кнопки "Запросить код".
	 */
	isVisibleRequestButton$: Observable<boolean>;

	/**
	 * Флаг видимости кнопки выплаты
	 */
	isVisiblePaymentButton$: Observable<boolean>;

	/**
	 * Флаг видимости кнопки "Продолжить".
	 */
	isVisibleContinueButton$: Observable<boolean>;

	/**
	 * Форма для вывода денег.
	 */
	withdrawForm: FormGroup;

	/**
	 * Форма для ввода кода подтверждения.
	 */
	smsForm: FormGroup;

	/**
	 * Текст кнопки "Отправить SMS".
	 */
	sendSMSButtonText = 'reporting.send_sms';

	/**
	 * Флаг видимости иконки клавиатуры.
	 */
	@Input() showKeyboardIcon = true;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {UserPaymentOperationsService} userPaymentOperationsService Сервис для работы с функционалом по пополнению счета и вывода денег в кастомных отчетах
	 * @param {FormBuilder} formBuilder Сервис для создания форм
	 * @param {BarcodeReaderService} barcodeReaderService Сервис для работы с баркод-сканером
	 */
	constructor(
		readonly userPaymentOperationsService: UserPaymentOperationsService,
		private readonly formBuilder: FormBuilder,
		private readonly barcodeReaderService: BarcodeReaderService
	) {}

	/**
	 * Обработчик нажатия на кнопку отправки СМС
	 */
	onClickSendSMSButtonHandler(): void {
		this.sendSMSButtonText = 'reporting.repeat_sms';
		this.userPaymentOperationsService.smsError = undefined;
		this.userPaymentOperationsService.checkCashSms(this.withdrawForm.controls.rcptBarcode.value);
	}

	/**
	 * Обработчик нажатия на кнопку выплаты
	 */
	onClickPaymentButtonHandler(): void {
		this.userPaymentOperationsService.payCashReq(this.userPaymentOperationsService.checkCashOutResp.pay_sum,
			this.smsForm.controls.confirmCode.value,
			this.withdrawForm.controls.rcptBarcode.value);
	}

	/**
	 * Обработчик нажатия на кнопку запроса кода в СМС
	 */
	onClickRequestButtonHandler(): void {
		this.sendSMSButtonText = 'reporting.send_sms';
		this.userPaymentOperationsService.smsError = undefined;
		this.smsForm.reset();
		this.userPaymentOperationsService.checkCashOut(this.withdrawForm.controls.rcptBarcode.value);
	}

	/**
	 * Обработчик нажатия на кнопку "Продолжить"
	 */
	onClickContinueButtonHandler(): void {
		this.userPaymentOperationsService.currentWithdrawalStatus$$.next(WithdrawalStatus.RequestCode);
		this.withdrawForm.reset();
		this.smsForm.reset();
	}

	/**
	 * Обработчик нажатия на кнопку возврата на начальный экран
	 */
	onGoBack(): void {
		this.userPaymentOperationsService.currentWithdrawalStatus$$.next(WithdrawalStatus.RequestCode);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.userPaymentOperationsService.currentWithdrawalStatus$$.next(WithdrawalStatus.RequestCode);

		this.isVisibleRequestButton$ = this.userPaymentOperationsService.currentWithdrawalStatus$$
			.pipe(map(status => {
				return status === WithdrawalStatus.RequestCode
					|| status === WithdrawalStatus.IncorrectCode;
			}));

		this.isVisiblePaymentButton$ = this.userPaymentOperationsService.currentWithdrawalStatus$$
			.pipe(map(status => {
				return status === WithdrawalStatus.WithdrawalAllowedEnterSMS;
			}));

		this.isVisibleContinueButton$ = this.userPaymentOperationsService.currentWithdrawalStatus$$
			.pipe(map(status => (status === WithdrawalStatus.WithdrawalFinal ||
				status === WithdrawalStatus.NoWithdrawalAllowed)));

		this.withdrawForm = this.formBuilder.group({
			rcptBarcode: ['', [
				Validators.required,
				Validators.minLength(16),
				Validators.maxLength(16)
			]],
		});

		this.smsForm = this.formBuilder.group({
			confirmCode: ['', [
				Validators.required,
				Validators.minLength(this.PinCodeLength),
				Validators.maxLength(this.PinCodeLength)
			]]
		});

		this.barcodeReaderService.barcodeSubject$$.subscribe(objBarcode => {
			if (this.userPaymentOperationsService.currentWithdrawalStatus$$.value === WithdrawalStatus.RequestCode) {
				this.withdrawForm.controls['rcptBarcode'].setValue(objBarcode.barcode);
			}
		});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.userPaymentOperationsService.isValidCodeInput = true;
	}

}
