import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoyaltyCardInfoComponent } from './loyalty-card-info.component';
import {
	UserLoyaltyAuthComponent
} from "../../../user-loyalty-auth/components/user-loyalty-auth/user-loyalty-auth.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {SharedModule} from "@app/shared/shared.module";
import {RouterTestingModule} from "@angular/router/testing";
import {TranslateService, TranslateStore} from "@ngx-translate/core";
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {of} from "rxjs";
import {GetCardInfoResp} from "@app/core/net/http/api/models/get-card-info";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";
import {CameraModule} from "../../../camera/camera.module";
import {UserLoyaltyAuthModule} from "../../../user-loyalty-auth/user-loyalty-auth.module";

describe('LoyaltyCardInfoComponent', () => {
  let component: LoyaltyCardInfoComponent | any;
  let fixture: ComponentFixture<LoyaltyCardInfoComponent>;

	let userLoyaltyAuthService: UserLoyaltyAuthService;
	let appStoreService: AppStoreService;
	let dialogInfoService: DialogContainerService;
	let printService: PrintService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			FormsModule,
			HttpClientTestingModule,
			SharedModule,
			RouterTestingModule,
			ReactiveFormsModule,
			UserLoyaltyAuthModule
		],
      	declarations: [
		  LoyaltyCardInfoComponent,
		  UserLoyaltyAuthComponent
	  	],
		providers: [
			HttpService,
			AppStoreService,
			TranslateService,
			TranslateStore,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			},
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyaltyCardInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

	  userLoyaltyAuthService = TestBed.inject(UserLoyaltyAuthService);
	  appStoreService = TestBed.inject(AppStoreService);
	  dialogInfoService = TestBed.inject(DialogContainerService);
	  printService = TestBed.inject(PrintService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('should go to start screen on onGoToStart', () => {
		component.onGoToStart();
		expect(userLoyaltyAuthService.step).toBe(1);
		expect(component.showStartScreen).toBeTrue();
	});


	it('should reset user loyalty auth service on destroy', () => {
		component.ngOnDestroy();
		expect(userLoyaltyAuthService.step).toBe(1);
		expect(userLoyaltyAuthService.errorText).toBe('');
		expect(userLoyaltyAuthService.lastEnteredPhone).toBe('');
	});

	it('test onPrint handler', () => {
		printService.isReady = () => false;
		spyOn(component, 'onPrint').and.callThrough();
		component.onPrint();
		expect(component.onPrint).toHaveBeenCalled();
	});

	it('test onPrint handler 2', () => {
		printService.isReady = () => true;
		printService.getPrinterInfo = () => of({
			printerInfo: {
				paperWidth: 70,
				pixelsPerLine: 24,
				charsPerLine: 30
			},
			errorCode: 0,
			errorDesc: '',
			requestId: '123123123'
		});
		spyOn(component, 'onPrint').and.callThrough();
		component.onPrint();
		expect(component.onPrint).toHaveBeenCalled();
	});

	it('test onAuthResult handler', () => {
		userLoyaltyAuthService.getCardInfo = (v) => of({
				client_trans_id: '111',
				"err_code": 0,
				"err_descr": "Операція успішна",
				"total_bonus_sum": 640,
				"card_limits": {
					"max_bonus_use_limit": {
						"current_value": 0,
						"limit_value": 160000
					},
					"withdraw_limit": {
						"current_value": 0,
						"limit_value": 150000
					},
					"max_winpay_limit": {
						"current_value": 0,
						"limit_value": 100000
					},
					"min_bonus_sum_with_auth": {
						"limit_value": 5000
					}
				},
				"challenges": [
					{
						"id": 4,
						"code": 4,
						"name": "Купити білети Лото-Забава",
						"action_type": "ACTION",
						"date_begin": "2024-03-25T00:00:00+02:00",
						"date_end": "2024-03-31T00:00:00+02:00",
						"amount_total": 0,
						"count_total": 0,
						"bonus_type_code": "PERCENT_OF_AMOUNT",
						"bonus_value": 15,
						"action_condition": "Купити білети Лото-Забава в період від 25.03.2024 по 31.03.2024 і отримати бонус, що дорівнює вартості всіх куплених білетів * 2",
						"participating_lotteries": [
							{
								"id": 169954,
								"code": 3,
								"name": "Лото-Забава",
								"type": "I",
								"is_accrual_bonuses": true,
								"is_use_bonuses": false
							}
						]
					}
				]
			});
		spyOn(component, 'onAuthResult').and.callThrough();
		component.onAuthResult('11111111');
		expect(component.onAuthResult).toHaveBeenCalled();
	});

	it('test errorsHandler handler', () => {
		spyOn(component, 'errorsHandler').and.callThrough();
		(component as any).errorsHandler({
			err_code: 2345,
			err_descr: 'Тестовая ошибка',
			client_trans_id: '12345'
		});
		expect((component as any).errorsHandler).toHaveBeenCalled();
	});
});
