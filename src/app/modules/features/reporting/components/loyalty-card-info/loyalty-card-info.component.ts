import {Component, OnDestroy} from '@angular/core';
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {GetCardInfoResp} from "@app/core/net/http/api/models/get-card-info";
import {IResponse} from "@app/core/net/http/api/types";
import {IError} from "@app/core/error/types";
import {DialogError} from "@app/core/error/dialog";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {Logger} from "@app/core/net/ws/services/log/logger";
import {Format, PrintData, Text} from "@app/core/net/ws/api/models/print/print-models";
import {concatMap} from "rxjs/operators";
import {from} from "rxjs";
import {twoCol, ukrDateTime} from "@app/util/utils";
import {ILoyaltyStepResult} from "../../../user-loyalty-auth/interfaces/i-loyalty-step-result";

@Component({
  selector: 'app-loyalty-card-info',
  templateUrl: './loyalty-card-info.component.html',
  styleUrls: ['./loyalty-card-info.component.scss']
})
export class LoyaltyCardInfoComponent implements OnDestroy {

	bonusInfoResult: GetCardInfoResp | null = null;

	currentDate = '';

	startDate = '';

	endDate = '';

	step = 1;

  	constructor(
		  readonly appStoreService: AppStoreService,
		  readonly userLoyaltyAuthService: UserLoyaltyAuthService,
		  private readonly dialogInfoService: DialogContainerService,
		  private readonly printService: PrintService
	) { }

	onGoToStart(): void {
		this.step = 1;
		this.userLoyaltyAuthService.errorText = '';
	}

	onPrint(): void {
		if (this.printService.isReady()) {
			this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.report_printing_wait_info');
			this.printService.getPrinterInfo()
				.pipe(
					concatMap((printInfo) => {
						let receipt = [
							new Format('center'),
							new Text(`КВИТАНЦІЯ\n\n`),
							new Text(`${this.currentDate}\n\n`),
							new Format('left'),
							new Text(`Термінал: ${this.appStoreService.Settings.termCode}\n`),
							new Text(' \n\n')
						];

						receipt.push(new Text(twoCol(
							'Доступні Фортуналі:',
							this.bonusInfoResult.total_bonus_sum.toFixed(2),
							printInfo.printerInfo.charsPerLine
						) + '\n\n'));

						receipt.push(new Text(' \n\n'));

						const monthUsed = (this.bonusInfoResult.card_limits.max_bonus_use_limit.current_value / 100).toFixed(2)
						receipt.push(new Text(`Використано фортуналів за поточний (календарний) місяць: ${monthUsed}\n\n`));

						const monthLimit = (this.bonusInfoResult.card_limits.max_bonus_use_limit.limit_value / 100).toFixed(2);
						receipt.push(new Text(`Ліміт фортуналів, що можуть бути використані за поточний (календарний) місяць: ${monthLimit}\n\n`));

						receipt.push(new Text('='.repeat(printInfo.printerInfo.charsPerLine) + '\n\n'));

						if (this.bonusInfoResult.challenges[0]) {
							receipt = [
								...receipt,
								new Format('center'),
								new Text(this.bonusInfoResult.challenges[0].name + '\n\n'),
								new Format('left'),
								new Text(`З ${this.startDate} до ${this.endDate}\n\n`),
								new Text(`Умови участі:\n\n`),
								new Text(this.bonusInfoResult.challenges[0].action_condition + '\n\n'),
								new Text(twoCol('Придбано білетів на:', `${this.bonusInfoResult.challenges[0].amount_total} грн`, printInfo.printerInfo.charsPerLine) + '\n\n'),
								new Format('left'),
								new Text(`Залишилось до виконання умов:\n`),
								new Format('right'),
								new Text(`${this.bonusInfoResult.challenges[0].amount_total} грн\n\n`),
							];
						}
						receipt.push(new Format('center'));
						receipt.push(new Text(`Удача чекає на вас!\n\n`));

						return from(this.printService.printDocument(receipt as Array<PrintData>));
					})
				)
				.subscribe({
					next: () => {
						this.dialogInfoService.hideActive();
					},
					error: err => {
						Logger.Log.e('LoyaltyCardInfo', 'onPrint() -> %s', err.message)
							.console();
						this.dialogInfoService.hideActive();
					}
				});
		} else {
			Logger.Log.i('LoyaltyCardInfo', 'onPrint -> printer not ready')
				.console();

			this.dialogInfoService.showOneButtonInfo('dialog.attention', 'dialog.printer_not_ready_info', {
				text: 'dialog.dialog_button_continue'
			});
		}
	}

	onAuthResult(ans: ILoyaltyStepResult): void {
		if (ans.step === 3) {
			if (ans.error) {
				this.errorsHandler(ans.error);
			} else {
				this.step = ans.step;
				this.currentDate = ukrDateTime(new Date());
				this.bonusInfoResult = ans.data;
				if (ans.data.challenges[0]) {
					this.startDate = ukrDateTime(new Date(ans.data.challenges[0].date_begin));
					this.endDate = ukrDateTime(new Date(ans.data.challenges[0].date_end));
				}
			}
		}
	}

	private errorsHandler(error: IResponse | IError): void {
		const errCode = +(error as IResponse).err_code || +(error as IError).code;
		const message = (error as IResponse).err_descr || (error as IError).message;
		const details = (error as IError).messageDetails || '';
		const dialogError = new DialogError(errCode, message, details);
		this.dialogInfoService.showOneButtonError(dialogError, {
			text: 'dialog.dialog_button_continue'
		});
	}

	ngOnDestroy(): void {
		  this.userLoyaltyAuthService.lastEnteredPhone = '';
	}

}
