import { Component, Input, OnInit } from '@angular/core';
import { UserPaymentOperationsService } from '@app/reporting/services/user-payment-operations.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';

/**
 * Компонент, предоставляющий функционал для пополнения счета игрока.
 * {@link https://igra.msl.ua/cabinet/help/show/64?language=ru Описание процедуры пополнения}
 */
@Component({
	selector: 'app-uporefill',
	templateUrl: './uporefill.component.html',
	styleUrls: [
		'./uporefill.component.scss'
	]
})
export class UPORefillComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Минимальная сумма пополнения (грн).
	 */
	readonly MIN = 20;

	/**
	 * Максимальная сумма пополнения (грн).
	 */
	readonly MAX = 1000;

	/**
	 * Форма для ввода суммы пополнения.
	 */
	refillForm: FormGroup;

	/**
	 * Флаг, указывающий, что первый элемент ввода должен быть активным.
	 */
	@Input() firstFocused = true;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {UserPaymentOperationsService} userPaymentOperationsService Сервис для работы с платежными операциями.
	 * @param {FormBuilder} formBuilder Сервис для создания форм.
	 * @param {PrintService} printService Сервис для работы с принтером.
	 * @param {DialogContainerService} dialogContainerService Сервис для работы с диалоговыми окнами.
	 */
	constructor(
		readonly userPaymentOperationsService: UserPaymentOperationsService,
		private readonly formBuilder: FormBuilder,
		private readonly printService: PrintService,
		private readonly dialogContainerService: DialogContainerService
	) {}

	/**
	 * Обработчик кнопки запроса кода пополнения.
	 */
	onClickRegistryButtonHandler(): void {
		// проверить готовность принтера
		if (!this.printService.isReady()) {
			this.dialogContainerService.showOneButtonInfo('dialog.attention', 'dialog.printer_not_ready_info', {
				text: 'dialog.dialog_button_continue'
			});

			return;
		}

		const value = +this.refillForm.controls.summa.value;
		if (isNaN(value)) {
			// ...
		} else {
			this.refillForm.reset();
			this.userPaymentOperationsService.buyVoucher(value * 100);
		}
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Инициализация формы ввода суммы.
	 */
	private initForm(): void {
		this.refillForm = this.formBuilder.group({
			summa: [undefined, [
				Validators.required,
				Validators.min(this.MIN),
				Validators.max(this.MAX)
			]],
		});
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.initForm();
	}

}
