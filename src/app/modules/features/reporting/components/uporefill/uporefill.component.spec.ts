import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UPORefillComponent } from './uporefill.component';
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@app/core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {UserPaymentOperationsService} from "@app/reporting/services/user-payment-operations.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {
	MslInputWithKeyboardComponent
} from "@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";

describe('UPORefillComponent', () => {
  let component: UPORefillComponent;
  let fixture: ComponentFixture<UPORefillComponent>;
  let printService: PrintService;
  let appStoreService: AppStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			CoreModule,
			HttpClientModule,
			ReactiveFormsModule,
			RouterTestingModule
		],
      declarations: [
		  UPORefillComponent,
		  MslInputWithKeyboardComponent
	  ],
		providers: [
			AppStoreService,
			UserPaymentOperationsService,
			BarcodeReaderService,
			HttpClient,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			}
		]
    })
    .compileComponents();
	  printService = TestBed.inject(PrintService);
	  const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UPORefillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickRegistryButtonHandler', () => {
		spyOn(component, 'onClickRegistryButtonHandler').and.callThrough();
		component.onClickRegistryButtonHandler();
		expect(component.onClickRegistryButtonHandler).toBeTruthy();
	});

	it('test onClickRegistryButtonHandler 2', () => {
		printService.isReady = () => true;
		spyOn(component, 'onClickRegistryButtonHandler').and.callThrough();
		component.onClickRegistryButtonHandler();
		expect(component.onClickRegistryButtonHandler).toBeTruthy();
	});

	it('test onClickRegistryButtonHandler 3', () => {
		printService.isReady = () => true;
		component.refillForm.controls['summa'].setValue('test');
		spyOn(component, 'onClickRegistryButtonHandler').and.callThrough();
		component.onClickRegistryButtonHandler();
		expect(component.onClickRegistryButtonHandler).toBeTruthy();
	});
});
