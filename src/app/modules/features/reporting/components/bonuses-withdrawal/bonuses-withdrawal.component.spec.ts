import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BonusesWithdrawalComponent } from './bonuses-withdrawal.component';
import {UserLoyaltyAuthModule} from "../../../user-loyalty-auth/user-loyalty-auth.module";
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ROUTES} from "../../../../../app-routing.module";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";

describe('BonusesWithdrawalComponent', () => {
	let component: BonusesWithdrawalComponent;
	let fixture: ComponentFixture<BonusesWithdrawalComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				UserLoyaltyAuthModule,
				HttpClientModule,
				SharedModule,
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes(ROUTES)
			],
			declarations: [ BonusesWithdrawalComponent ],
			providers: [
				UserLoyaltyAuthService,
				AppStoreService,
				HttpService,
				HttpClient
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		TestBed.inject(BarcodeReaderService);
		appStoreService = TestBed.inject(AppStoreService);

		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		(appStoreService.Settings as any)._csEnvironmentUrl = '';
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(BonusesWithdrawalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onCancel handler', () => {
		component.userLoyaltyAuthService.step = 2;
		component.step = 2;
		component.onCancel();
		expect(component.userLoyaltyAuthService.step).toBe(1);
		expect(component.step).toBe(1);
	});

	it('should set errorState to true for invalid input', () => {
		component.minValue = 0;
		component.maxValue = 1000;

		component.onInputChange('-1');
		expect(component.errorState).toBeTrue();

		component.onInputChange('1001');
		expect(component.errorState).toBeTrue();

		component.onInputChange('abc');
		expect(component.errorState).toBeTrue();
	});

	it('should set errorState to false for valid input', () => {
		component.minValue = 0;
		component.maxValue = 1000;

		component.onInputChange('500');
		expect(component.errorState).toBeFalse();
	});

	it('should increment step on goNext', () => {
		component.step = 1;
		component.onGoNext();
		expect(component.step).toBe(4);
	});

	it('should handle error with code 702', () => {
		const errorResponse = { err_code: 702, err_descr: 'Verification Error' };
		(component as any).errorsHandler(errorResponse);
		expect(component.errorText).toBe('');
	});

	it('should handle error with other codes', () => {
		const errorResponse = { err_code: 500, err_descr: 'Internal Server Error' };
		(component as any).errorsHandler(errorResponse);
		expect(component.errorText).toBe('');
	});

	it('test ngOnInit lifecycle hook', () => {
		component.step = 1;

		expect(component.userLoyaltyAuthService.step).toBe(1);
		expect(component.step).toBe(1);
	});

	xit('test onFinish handler', () => {
		component.cardNum = '11111111';
		component.withdrawalSum = '123';
		spyOn(component, 'onFinish').and.callThrough();
		component.onFinish();
		expect(component.onFinish).toHaveBeenCalled();
	});

});
