import {Component, OnDestroy, ViewChild} from '@angular/core';
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {concatMap, finalize} from "rxjs/operators";
import {getShortPlayerPhone} from "@app/util/utils";
import {IResponse} from "@app/core/net/http/api/types";
import {IError} from "@app/core/error/types";
import {error} from "protractor";
import {Subject, timer} from "rxjs";
import {
	UserLoyaltyAuthComponent
} from "../../../user-loyalty-auth/components/user-loyalty-auth/user-loyalty-auth.component";
import {ILoyaltyStepResult} from "../../../user-loyalty-auth/interfaces/i-loyalty-step-result";

@Component({
  selector: 'app-bonuses-withdrawal',
  templateUrl: './bonuses-withdrawal.component.html',
  styleUrls: ['./bonuses-withdrawal.component.scss']
})
export class BonusesWithdrawalComponent implements  OnDestroy {

	/**
	 * Маска ввода тел. номера
	 */
	readonly phoneMask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

	userInfo: any = {};

	errorState = false;

	minValue = 0;
	maxValue = 1000;

	tmpPhoneNum = '+38(0__) ___-__-__';

	tmpPhonePin = '';

	errorText = '';

	step = 1;

	withdrawalSum = '';

	cardNum = '';

	@ViewChild('loyalty1') loyalty1: UserLoyaltyAuthComponent;

	@ViewChild('loyalty2') loyalty2: UserLoyaltyAuthComponent;

	constructor(readonly userLoyaltyAuthService: UserLoyaltyAuthService) { }

	onInputChange(input: string): void {
		const value = parseFloat(input);

		this.errorState = isNaN(value) || value < this.minValue || value > this.maxValue;
	}

	onCancel(): void {
		this.loyalty1.reset();
		this.loyalty2.reset();
		this.errorText = '';
		this.errorState = false;
		this.resetParams();
	}

	onGoNext(): void {
		this.step = 1;
		this.step = 4;
	}

	private errorsHandler(error: IResponse | IError): void {
		const errCode = +(error as IResponse).err_code || +(error as IError).code;
		const message = (error as IResponse).err_descr || (error as IError).message;
		console.log('error =', error);
		this.userLoyaltyAuthService.errorText = errCode === 702 ? 'other.verification-error-2' : message;
		this.resetParams();
	}

	private resetParams(): void {
		this.userLoyaltyAuthService.lastEnteredPhone = '';
		this.step = 1;
		this.step = 1;
		this.withdrawalSum = '';
		this.userLoyaltyAuthService.isLoading = false;
	}

	onAuthResult(result: ILoyaltyStepResult): void {
		if (result.step === 3) {
			this.userLoyaltyAuthService.errorText = '';
			this.userLoyaltyAuthService.isLoading = true;
			this.cardNum = result.cardNum;
			this.step = 3;
			this.step = 3;
			this.userLoyaltyAuthService.getWithdrawBonus(result.cardNum)
				.subscribe({
					next: (bonusInfo: any)=> {
						if (bonusInfo.err_code === 0) {
							this.userInfo = {...this.userInfo, ...bonusInfo};
							this.minValue = this.userInfo.min_available_bonus_sum_money / 100;
							this.maxValue = this.userInfo.max_available_bonus_sum_money / 100;
							this.userLoyaltyAuthService.isLoading = false;
						} else {
							this.errorsHandler(bonusInfo);
						}},
					error: (error) => {
						this.errorsHandler(error);
					}
				});
		}
	}

	onFinish(): void {
		this.userLoyaltyAuthService.errorText = '';
		this.userLoyaltyAuthService.isLoading = true;
		this.userLoyaltyAuthService.withdrawBonus(this.cardNum, parseFloat(this.withdrawalSum) * 100)
			.subscribe({
				next: (bonusInfo: any)=> {
					if (bonusInfo.err_code === 0) {
						this.step = 5;
						this.userLoyaltyAuthService.isLoading = false;
					} else {
						this.errorsHandler(bonusInfo);
					}},
				error: (error) => {
					this.errorsHandler(error);
				}
			});
	}



	ngOnDestroy(): void {
		this.step = 1;
		this.userLoyaltyAuthService.errorText = '';
		this.userLoyaltyAuthService.lastEnteredPhone = '';
	}

}
