import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OtherComponent} from './other.component';
import {ReportingDataService} from "@app/reporting/services/reporting-data.service";
import {RouterTestingModule} from "@angular/router/testing";
import {ReportsService} from "@app/core/services/report/reports.service";
import {FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {TmlBmlInputStoreService} from "@app/tml-bml-input/services/tml-bml-input-store.service";
import {AuthService} from "@app/core/services/auth.service";
import {LogOutService} from "@app/logout/services/log-out.service";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {CoreModule} from "@app/core/core.module";
import {SharedModule} from "@app/shared/shared.module";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ROUTES} from "../../../../../app-routing.module";
import {TranslateModule} from "@ngx-translate/core";
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_100} from "../../../mocks/game100-draws";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {Logger} from "@app/core/net/ws/services/log/logger";
import {SanitizePipe} from "@app/reporting/pipes/sanitize.pipe";
import {ContentScrolledDirective} from "@app/shared/directives/content-scrolled.directive";
import {
	InputOperationFieldComponent
} from "@app/reporting/components/input-operation-field/input-operation-field.component";
import {
	UserPaymentOperationsComponent
} from "@app/reporting/components/user-payment-operations/user-payment-operations.component";
import {PrintData, Text} from "@app/core/net/ws/api/models/print/print-models";
import {of, throwError} from "rxjs";
import {concatMap, map, tap} from "rxjs/operators";

describe('OtherComponent', () => {
  let component: OtherComponent | any;
  let fixture: ComponentFixture<OtherComponent>;
  let appStoreService: AppStoreService;
  let reportsService: ReportsService;
  let reportingDataService: ReportingDataService;
  let printService: PrintService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			SharedModule,
			RouterTestingModule.withRoutes(ROUTES),
			TranslateModule.forRoot({}),
			HttpClientModule,
			ReactiveFormsModule
		],
      	declarations: [
			OtherComponent,
			InputOperationFieldComponent,
			SanitizePipe,
			ContentScrolledDirective,
			UserPaymentOperationsComponent
		],
		providers: [
			ReportingDataService,
			ReportsService,
			FormBuilder,
			AppStoreService,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			},
			TmlBmlInputStoreService,
			AuthService,
			LogOutService,
			PrintService,
			HttpService,
			HttpClient,
			LogService,
			Logger
		]
    })
    .compileComponents();

	  TestBed.inject(LogService);
	  TestBed.inject(Logger);

	  const op = JSON.parse(SESS_DATA.data[0].value);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
	  appStoreService.isLoggedIn$$.next(true);

	  const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);

	  appStoreService.Draws = new LotteriesDraws();
	  appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);

	  reportsService  = TestBed.inject(ReportsService);
	  await reportsService.reload();

	  reportingDataService = TestBed.inject(ReportingDataService);
	  printService = TestBed.inject(PrintService);
  });

  beforeEach(() => {
	fixture = TestBed.createComponent(OtherComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
  });

  it('should create', () => {
	expect(component).toBeTruthy();
  });

	it('test onClickRequestHandler', () => {
		spyOn(component, 'onClickRequestHandler').and.callThrough();
		component.onClickRequestHandler();
		expect(component.onClickRequestHandler).toHaveBeenCalled();
	});

	it('test onClickRequestHandler 2', () => {
		component.reportForm.controls['lineedit-1'].setValue('123');
		component.reportForm.controls['lineedit-2'].setValue('123');
		component.canRequest = true;
		spyOn(component, 'onClickRequestHandler').and.callThrough();
		component.onClickRequestHandler();
		expect(component.onClickRequestHandler).toHaveBeenCalled();
	});

	it('test onClickRequestHandler 3', () => {
		component.reportForm.controls['lineedit-1'].setValue('123');
		component.reportForm.controls['lineedit-2'].setValue('123');
		component.canRequest = true;
		reportsService.getReportByParams = () => of(null).pipe(
			concatMap(() => throwError('test'))
		);
		spyOn(component, 'onClickRequestHandler').and.callThrough();
		component.onClickRequestHandler();
		expect(component.onClickRequestHandler).toHaveBeenCalled();
	});

	it('test onClickPrintHandler', () => {
		spyOn(component, 'onClickPrintHandler').and.callThrough();
		component.onClickPrintHandler();
		expect(component.onClickPrintHandler).toHaveBeenCalled();
	});

	it('test onClickPrintHandler 2', () => {
		spyOn(component, 'onClickPrintHandler').and.callThrough();
		printService.isReady = () => true;
		component.lastPrintData = [{
			control: 'text'
		} as Text];
		reportsService.printReport = (report: Array<PrintData>) => Promise.resolve('');
		component.onClickPrintHandler();
		expect(component.onClickPrintHandler).toHaveBeenCalled();
	});

	it('test onClickPrintHandler 3', () => {
		spyOn(component, 'onClickPrintHandler').and.callThrough();
		printService.isReady = () => true;
		component.lastPrintData = [{
			control: 'text'
		} as Text];
		reportsService.printReport = (report: Array<PrintData>) => Promise.reject({
			message: 'Some fake error for tests'
		});
		component.onClickPrintHandler();
		expect(component.onClickPrintHandler).toHaveBeenCalled();
	});

	it('test onCatchEnterHandler', () => {
		spyOn(component, 'onCatchEnterHandler').and.callThrough();
		component.onCatchEnterHandler({
			controlName: 'date-1',
			labelTag: {
				type: 'dateedit',
				param_number: '0',
				caption: {
					text: 'date',
					parent_tag: 'caption'
				},
				parent_tag: 'inputs',
				class_name: ''
			},
			fieldTag: {
				type: 'dateedit',
				param_number: '0',
				caption: {
					text: 'date',
					parent_tag: 'caption'
				},
				parent_tag: 'inputs',
				class_name: ''
			},
			index: 0,
			inputIndex: 0
		});
		expect(component.onCatchEnterHandler).toHaveBeenCalled();
	});

	it('test onClickMenuHandler', () => {
		component.onClickMenuHandler({
			label: 'test',
			path: '22'
		});
		expect(reportingDataService.currentReport$$.value).toEqual('22');
	});

	it('test onClickOperationHandler', () => {
		component.onClickOperationHandler(0, 0);
		expect(reportingDataService.currentMenuIndex).toEqual(0);
	});

	it('test onTitleClickHandler', () => {
		reportingDataService.fullMenuItems[0].expanded = true;
		component.onTitleClickHandler(new MouseEvent('click'), 0);
		expect(reportingDataService.fullMenuItems[0].expanded).toBeFalsy();
	});

	it('test onTitleClickHandler 2', () => {
		component.isReportLoading = false;
		reportingDataService.fullMenuItems[0].expanded = false;
		component.onTitleClickHandler(new MouseEvent('click'), 0);
		expect(reportingDataService.fullMenuItems[0].expanded).toBeTruthy();
	});

	it('test onShowGraphicalBCs', () => {
		component.isReportLoading = true;
		spyOn(component, 'onShowGraphicalBCs').and.callThrough();
		component.onShowGraphicalBCs();
		expect(component.onShowGraphicalBCs).toHaveBeenCalled();
	});

	it('test resetLastData', () => {
		component.resetLastData();
		expect(reportsService.lastReportResponse$$.value).toBeUndefined();
	});
});
