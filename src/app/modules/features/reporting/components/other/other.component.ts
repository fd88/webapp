import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import {combineLatest, Observable, of, Subject} from 'rxjs';
import { filter, finalize, map, takeUntil } from 'rxjs/internal/operators';
import { AuthService } from '@app/core/services/auth.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { InputTag } from '@app/core/services/report/tags';
import {
	getDefaultValue,
	getFieldControlName,
	IReportField,
	ReportInputField,
	ReportsId,
	ReportsService
} from '@app/core/services/report/reports.service';
import { INavigationMenuItem } from '@app/shared/components/navigation-menu.component';
import { ReportingDataService } from '@app/reporting/services/reporting-data.service';
import { InputOperationFieldComponent } from '@app/reporting/components/input-operation-field/input-operation-field.component';
import { TmlBmlInputStoreService } from '@app/tml-bml-input/services/tml-bml-input-store.service';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';
import { GetReportDataResp } from '@app/core/net/http/api/models/get-report-data';
import { NetError } from '@app/core/error/types';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { LogOutService } from '@app/logout/services/log-out.service';
import { IOperationListItem } from '@app/reporting/interfaces/ioperation-list-item';
import { IOperationMenuItem } from '@app/reporting/interfaces/ioperation-menu-item';
import { ISOtoCSDate } from '@app/util/utils';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { PrintData } from '@app/core/net/ws/api/models/print/print-models';
import { debounceTime } from 'rxjs/operators';

// -----------------------------
//  Static functions
// -----------------------------

/**
 * Функция для валидации формы.
 */
const validateForm = (): { [key: string]: boolean } => undefined;

/**
 * Номера операция, для которых доступна печать.
 */
const PRINT_OPERATIONS = ['10', '15', '16', '22', '5', '6', '7', '9', '11', '14'];

/**
 * Компонент для раздела "Другое"
 */
@Component({
	selector: 'app-other',
	templateUrl: './other.component.html',
	styleUrls: ['./other.component.scss']
})
export class OtherComponent implements OnInit, OnDestroy {

	/**
	 * Деактивирована ли кнопка "Печать".
	 */
	disabledPrintButton$: Observable<boolean>;

	/**
	 * Последние данные для печати.
	 */
	lastPrintData: Array<PrintData>;

	/**
	 * Видимость кнопки "Печать".
	 */
	printButtonVisible = false;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ReportingDataService} reportingDataService Сервис для работы с данными отчетов.
	 * @param {ReportsService} reportsService Сервис для работы с отчетами.
	 * @param {ActivatedRoute} route Активный маршрут.
	 * @param {FormBuilder} formBuilder Сервис для работы с формами.
	 * @param {AppStoreService} appStoreService Сервис для работы с хранилищем приложения.
	 * @param {DialogContainerService} dialogInfoService Сервис для работы с диалогами.
	 * @param {TmlBmlInputStoreService} tmlBmlInputStoreService Сервис для работы с хранилищем ввода ТМЛ/БМЛ.
	 * @param {AuthService} authService Сервис для работы с авторизацией.
	 * @param logoutService Сервис выхода из системы.
	 * @param printService Сервис печати.
	 */
	constructor(
		readonly reportingDataService: ReportingDataService,
		readonly reportsService: ReportsService,
		private readonly route: ActivatedRoute,
		private readonly formBuilder: FormBuilder,
		readonly appStoreService: AppStoreService,
		private readonly dialogInfoService: DialogContainerService,
		readonly tmlBmlInputStoreService: TmlBmlInputStoreService,
		private readonly authService: AuthService,
		private readonly logoutService: LogOutService,
		private readonly printService: PrintService
	) {}

	/**
	 * Подготовить строку с параметрами для выполнения запроса в ЦС.
	 *
	 * @returns {string} Строка с параметрами, разделенными символом "|".
	 */
	private get requestData(): string {
		const resultArr = [];

		[...this.inputsChildren.toArray()]
			.sort((a, b) =>  a.field.inputIndex - b.field.inputIndex)
			.forEach(item => {
				let data: string;
				if (item.field.fieldTag.type === ReportInputField.TICKET_LIST) {
					data = this.tmlBmlInputStoreService.ticketsList$$.value.map(elem => {
						return elem.bcFrom.barcode === elem.bcTo.barcode
							? elem.bcFrom.barcode
							: `${elem.bcFrom.barcode}-${elem.bcTo.barcode}`;
					})
						.join(';');
				} else if (item.field.fieldTag.type === ReportInputField.DATE_EDIT) {
					data = ISOtoCSDate((item.value as Date).toISOString());
				} else {
					data = item.value ? (item.value as string).trim() : '';
				}
				resultArr.push(data);
			});

		return resultArr.join('|');
	}

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Коллекция компонентов ввода.
	 */
	@ViewChildren(InputOperationFieldComponent)
	inputsChildren: QueryList<InputOperationFieldComponent>;

	/**
	 * Компоненты формы.
	 */
	@ViewChildren(InputOperationFieldComponent)
	inputOperationField: QueryList<InputOperationFieldComponent>;

	/**
	 * Перечень вариантов отчетов.
	 */
	readonly ReportsId = ReportsId;

	/**
	 * Массив полей ввода.
	 */
	inputFields: Array<IReportField> = [];

	/**
	 * Форма для ввода данных.
	 */
	reportForm: FormGroup;

	/**
	 * Наблюдаемый признак, указывающий на наличие результата по запросу данных и необходимости их отображения.
	 */
	showResultsButtons$: Observable<boolean>;

	/** Грузится ли отчет */
	isReportLoading = false;

	/** Генерирует последнюю связку типа: "reportId"-"operationId" */
	currentReportOperationKey$: Observable<string>;

	/**
	 * Классы для секции
	 */
	sectionClasses$: Observable<string> = of('');

	/**
	 * Флаг, указывающий на возможность выполнить запрос отчета.
	 */
	canRequest = false;

	/** Выводить ли кнопки? */
	buttonsExists = false;

	/** Класс для текущей роли юзера. */
	currentRole: string;

	/**
	 * Признак центрирования результата по вертикали.
	 */
	centeredResult: boolean;

	/**
	 * Признак того, что мы находимся под менеджером в инвентаризации билетов моментальных лотерей.
	 */
	managerInvent = false;

	/**
	 * Признак того, что введены одинаковые штрих-коды
	 */
	equalBarcodes = false;

	/**
	 * Индекс текущего выбранного отчета, содержащего операции.
	 */
	private currentReportIndex = 0;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Признак того, что мы находимся под менеджером в выдаче или возврате билетов моментальных лотерей
	 * (там где есть диапазон штрих-кодов)
	 */
	private barcodesInterval = false;

	/**
	 * Обработчик нажатия кнопки "Запросить".
	 * Выполняет запрос для получения первичных данных.
	 */
	onClickRequestHandler(): void {
		if (this.isReportLoading || !this.canRequest) {
			return;
		}

		this.isReportLoading = true;
		this.checkCanRequest();

		this.reportsService.getReportByParams(this.requestData)
			.pipe(takeUntil(this.unsubscribe$$))
			.pipe(finalize(() => this.isReportLoading = false))
			.subscribe((content: GetReportDataResp) => {

				this.reportsService.lastReportResponse$$.next(content);

				// пропарсить данные для экрана
				const screenData = this.reportsService.parseReport('screen', content, this.reportsService.currentReportId) as string;
				this.reportsService.lastScreenData$.next(screenData);

				this.reportingDataService.inResult$$.next(true);
			}, error => {
				this.reportsService.lastReportResponse$$.next(undefined);
				this.reportsService.lastScreenData$.next('');
				const err = (error instanceof NetError) ? error : new DialogError(ErrorCode.GetReport, 'dialog.get_report_result_error');
				this.dialogInfoService.showOneButtonError(err, {
					click: () => {
						if (error.code && (error.code === 4313) || (error.code === 4318)) {
							this.logoutService.logoutOperator();
						} else {
							this.tmlBmlInputStoreService.ticketsList$$.next([]);
							this.reportForm.reset();
							this.goOnFirstScreen();
						}
					},
					text: 'dialog.dialog_button_continue'
				});
				Logger.Log.e('ReportsService', 'getReportByParams ERROR -> %s', error.message)
					.console();
			});
	}

	/**
	 * Слушатель нажатия на кнопку "Печать".
	 */
	onClickPrintHandler(): void {
		if (this.printService.isReady()) {
			this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.report_printing_wait_info');
			this.reportsService.printReport(this.lastPrintData)
				.then(() => {
					this.dialogInfoService.hideActive();

					// если разрешен признак автопечати, то остаемся в состоянии печати
					// для возможности сделать еще одну возможность печати
					// this.reportsService.lastReportResponse$$.next(undefined);
					this.tmlBmlInputStoreService.ticketsList$$.next([]);
					this.reportingDataService.inResult$$.next(true);
					this.reportForm.reset({'dateedit-1': new Date(), 'dateedit-2': new Date()});
					// this.goOnFirstScreen();
				})
				.catch(err => {
					Logger.Log.e('ReportingListComponent', 'printLastRequestedReport() -> %s', err.message)
						.console();
					this.dialogInfoService.hideActive();
				});
		} else {
			Logger.Log.i('ReportingComponent', 'onClickPrintHandler -> printer not ready')
				.console();

			this.dialogInfoService.showOneButtonInfo('dialog.attention', 'dialog.printer_not_ready_info', {
				text: 'dialog.dialog_button_continue'
			});
		}
	}


	/**
	 * Обработчик клавиши Enter на поле ввода.
	 *
	 * @param {IReportField} field Поле ввода.
	 */
	onCatchEnterHandler(field: IReportField): void {
		if (this.reportForm.valid) {
			this.onClickRequestHandler();
		} else {
			const arr = this.inputsChildren.toArray();
			const item = arr.find(f => f.field.controlName === field.controlName);
			if (item) {
				const oldIdx = arr.indexOf(item);
				const newIdx = (oldIdx === arr.length - 1 ? 0 : oldIdx + 1);
				arr[newIdx].setFocus();
			}
		}
	}

	/**
	 * Обработчик выбора отчета.
	 * После выбора отчета будет сформирован список операций.
	 *
	 * @param {INavigationMenuItem} item Выбранный пункт меню.
	 */
	onClickMenuHandler(item: INavigationMenuItem): void {
		this.reportingDataService.currentReport$$.next(item.path);
	}

	/**
	 * Обработчик нажатия на операцию.
	 *
	 * @param parentIndex Индекс родительского меню
	 * @param {number} itemIndex Индекс меню, по которому был совершен клик.
	 */
	onClickOperationHandler(parentIndex: number, itemIndex: number): void {
		if (!this.isReportLoading) {
			this.reportingDataService.selectMenuItem(parentIndex, itemIndex);
		}
	}

	/**
	 * Слушатель изменения составного контрола в реактивной форме
	 *
	 * @param $event Новое значение
	 */
	onInputChanged($event: string | Date | Array<ITmlBmlListItem>): void {
		if (this.barcodesInterval) {
			this.equalBarcodes = this.inputsChildren.toArray()[1].value &&
				this.inputsChildren.toArray()[1].value === this.inputsChildren.toArray()[2].value;
		}
		this.checkCanRequest();
	}

	/**
	 * Обработчик нажатия на титульное меню с операциями.
	 *
	 * @param {MouseEvent} event Событие нажатия.
	 * @param {number} itemIndex Индекс меню, по которому был совершен клик.
	 */
	onTitleClickHandler(event: MouseEvent, itemIndex: number): void {
		if (!this.isReportLoading) {
			if (this.reportingDataService.fullMenuItems[itemIndex].expanded) {
				this.reportingDataService.fullMenuItems[itemIndex].expanded = false;
				const activeSubMenuItem = this.reportingDataService.fullMenuItems[itemIndex].subMenuItems.find(elem => elem.isActive);
				this.reportingDataService.fullMenuItems[itemIndex].label = activeSubMenuItem ? activeSubMenuItem.label :
					this.reportingDataService.fullMenuItems[itemIndex].defaultLabel;
			} else {
				this.reportingDataService.fullMenuItems.forEach(menu => {
					menu.expanded = false;
					if (menu.isActive) {
						const activeSubItem = menu.subMenuItems.find(elem => elem.isActive);
						menu.label = activeSubItem ? activeSubItem.label : null;
					}
				});
				this.reportingDataService.fullMenuItems[itemIndex].expanded = true;
				this.reportingDataService.fullMenuItems[itemIndex].label = null;
				this.currentReportIndex = itemIndex;
			}
		}
	}

	/**
	 * Обработчик нажатия на кнопку показа штрих-кодов в графическом виде
	 */
	onShowGraphicalBCs(): void {
		if (this.isReportLoading) {
			return;
		}

		// пропарсить данные для экрана
		const screenData = this.reportsService.parseReport('screen', this.reportsService.lastReportResponse$$.value, `${this.reportsService.currentReportId}_graphical_bcs`) as string;
		this.reportsService.lastScreenData$.next(screenData);

		this.reportingDataService.inResult$$.next(true);
	}

	/**
	 * Вернуться назад на форму запроса
	 */
	goOnFirstScreen(): void {
		this.reportingDataService.inResult$$.next(false);
		this.checkCanRequest();
	}

	/**
	 * Вспомогательная функция для отслеживания изменений в массиве элементов.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByField = (index, item: IReportField) => item;

	/**
	 * Вспомогательная функция для отслеживания изменений в массиве элементов.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByIndex = (index, item: any) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Очистить форму от элементов ввода.
	 */
	private clearFormControls(): void {
		Object.keys(this.reportForm.controls)
			.forEach(k => this.reportForm.removeControl(k));
		this.inputFields.splice(0);
	}

	/**
	 * Сбросить контент и ответы сервиса.
	 */
	private resetLastData(): void {
		this.reportsService.lastReportResponse$$.next(undefined);
		this.reportsService.lastScreenData$.next('');
	}

	/**
	 * Построить массив с полями ввода типа {@link inputFields} и FormControl для формы.
	 * @param {Array<number>} customSortArr Массив для порядка вывода элементов
	 */
	private makeFormControls(customSortArr?: Array<number>): void {
		const arr = this.reportsService.getInputs();
		if (arr) {
			// счетчик добавленных полей
			let inputIndex = 0;
			const tempFields = [];
			for (let i = 0; i < arr.length; i += 2) {
				// ожидается 2 элемента: метка и поле ввода
				const labelTag = arr[i];
				const fieldTag = arr[i + 1];
				if (labelTag && fieldTag) {
					const controlName: string = getFieldControlName(fieldTag);
					const hidden = this.parseHiddenProperty(fieldTag);
					const enableBarcodeInput = this.parseEnableBarcodeInputProperty(fieldTag);
					const field: IReportField = {
						controlName,
						labelTag,
						fieldTag,
						hidden,
						enableBarcodeInput,
						index: customSortArr ? customSortArr[inputIndex] : inputIndex,
						inputIndex,
						placeholder: this.barcodesInterval && (inputIndex === 1 || inputIndex === 2) ? 'reporting.scan_barcode' : ''
					};
					inputIndex++;
					tempFields.push(field);

					// добавить новый FormControl
					this.managerInvent = this.appStoreService.operator.value.isManager
						&& this.reportingDataService.currentOperation$$.value === '22';
					const defaultString = this.managerInvent ? this.authService.loginOperator_val : undefined;
					const fc = new FormControl(getDefaultValue(field, defaultString), this.parseValidators(field));
					const fcName = getFieldControlName(field.fieldTag);
					this.reportForm.addControl(fcName, fc);
				} else {
					console.log('+++ WARNING!!!!', labelTag, fieldTag);
				}

				if ((arr[i] && (arr[i].type === ReportInputField.DATE_EDIT)) ||
					(arr[i + 1] && (arr[i + 1].type === ReportInputField.DATE_EDIT))) {
					this.buttonsExists = true;
				}

				if ((arr[i] && (arr[i].type === ReportInputField.TICKET_LIST)) ||
					(arr[i + 1] && (arr[i + 1].type === ReportInputField.TICKET_LIST))) {
					this.tmlBmlInputStoreService.ticketsList$$.next([]);
				}
			}

			// сортировка массива полей по индексам
			tempFields.sort((a: IReportField, b: IReportField) => a.index - b.index);
			// подсчет ТЕКСТОВЫХ полей ввода
			this.inputFields = tempFields;

			// проставить признак автофокуса на первом (текстовом) элементе ввода
			const rf = this.inputFields.find(f => f.fieldTag.type === ReportInputField.LINE_EDIT && !f.hidden);
			if (rf) {
				rf.autofocus = true;
			}
		}
	}

	/**
	 * Пропарсить поле ввода и определить свойство {@link IReportField.enableBarcodeInput enableBarcodeInput},
	 * для дальнейшей блокировки или разрешение использования сканера штрих-кодов на данном поле.
	 *
	 * @param {InputTag} fieldTag Поле ввода.
	 */
	private parseEnableBarcodeInputProperty(fieldTag: InputTag): boolean {
		const operationId = +this.reportsService.operationId;

		return (this.reportsService.currentReportId === ReportsId.PaperInstant)
			&& (operationId === 9 || operationId === 14)
			&& (fieldTag.param_number !== '1');
	}

	/**
	 * Проверить, скрытое ли поле.
	 *
	 * @param {InputTag} fieldTag Поле ввода.
	 */
	private parseHiddenProperty(fieldTag: InputTag): boolean {
		const operationId = +this.reportsService.operationId;

		if (this.reportsService.currentReportId === ReportsId.PaperInstant) {
			return operationId === 22
				&& this.appStoreService.operator.getValue().isOperator
				&& fieldTag.type === ReportInputField.LINE_EDIT;
		}

		return false;
	}

	/**
	 * Определить кастомный набор валидаторов для формы отчета по ИД отчета {@link ReportsId}
	 * и ИД операции {@link reportsService.operationId}.
	 * По умолчанию все поля обязательны.
	 *
	 * @param {IReportField} field Поле, для которого необходимо определить кастомный набор валидаторов.
	 */
	private parseValidators(field: IReportField): Array<((control: AbstractControl) => (ValidationErrors | null))> {
		const validators = [Validators.required];
		const operationId = +this.reportsService.operationId;

		switch (this.reportsService.currentReportId) {
			case ReportsId.Reports:
				if (operationId === 4) {
					// у отчета "звіт за період по лотереям" код распространителя необязателен
					if (field.fieldTag.type === ReportInputField.LINE_EDIT) {
						validators.splice(0);
					}
				} else if (operationId === 2) {
					validators.splice(0);
				}
				break;

			case ReportsId.Finance:
				break;

			case ReportsId.OperatorShift:
				break;

			case ReportsId.PaperInstant:
				// инвентаризация ТМЛ/БМЛ
				if (operationId === 22) {
					// параметр "код оператора" необязателен
					if (field.fieldTag.type === ReportInputField.LINE_EDIT || field.fieldTag.type === ReportInputField.TICKET_LIST) {
						validators.splice(0);
					}
				}
				break;

			default:
				break;
		}

		return validators;
	}

	/**
	 * Проверка возможности выполнить запрос.
	 */
	private checkCanRequest(): void {
		this.canRequest = !(this.reportForm.disabled || this.reportForm.invalid || this.isReportLoading);
	}

	/**
	 * Настроить форму.
	 */
	private initForm(): void {
		this.reportForm = this.formBuilder.group({}, {validator: validateForm});
		this.reportForm.valueChanges
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(() => {
				if (PRINT_OPERATIONS.indexOf(this.reportingDataService.currentOperation$$.value) === -1) {
					this.resetLastData();
				}
			});
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {

		this.initForm();

		// определить видимость контейнера с кнопками НАЗАД и ПЕЧАТЬ
		this.showResultsButtons$ = combineLatest([
			this.reportsService.lastReportResponse$$,
			this.reportingDataService.inResult$$
		])
			.pipe(
				takeUntil(this.unsubscribe$$),
				map(v => !!v[0] && v[1])
			);

		this.currentRole = `role-${this.appStoreService.operator.value.accessLevel}`;
		this.reportingDataService.inResult$$.next(false);

		// подписаться на изменение операции
		this.reportingDataService.currentOperation$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				filter(f => !!f),
			)
			.subscribe(operation => {
				this.reportsService.operationId = operation;
				this.equalBarcodes = false;
				this.barcodesInterval = this.appStoreService.operator.value.isManager && (operation === '9' || operation === '14');
				this.printButtonVisible = PRINT_OPERATIONS.indexOf(operation) > -1;
				const sortArr = undefined;
				this.clearFormControls();
				this.makeFormControls(sortArr);
				this.goOnFirstScreen();
			});

		// подписаться на изменение типа отчета
		this.reportingDataService.currentReport$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				filter(f => !!f)
			)
			.subscribe(report => {
				this.reportsService.currentReportId = report;

				const ol = this.reportingDataService.makeOperationsListByReportId(report);
				if (ol.length) {
					const oper = ol[0];
					oper.isActive = true;
					this.reportingDataService.currentOperation$$.next(oper.path);
				} else {
					this.clearFormControls();
				}
			});

		this.reportingDataService.createTopMenu();
		this.reportingDataService.onlyReports$$.next(false);
		// подписаться на изменение типа отчета или операции
		this.currentReportOperationKey$ = combineLatest([
			this.reportingDataService.currentReport$$,
			this.reportingDataService.currentOperation$$
		])
			.pipe(
				takeUntil(this.unsubscribe$$),
				map(m => {
					if (!m[0] || !m[1]) {
						return null;
					}

					return `${m[0]}_${m[1]}`;
				})
			);

		this.sectionClasses$ = this.currentReportOperationKey$.pipe(
			takeUntil(this.unsubscribe$$),
			map((opKey: string) => {
				if (opKey) {
					return `reporting-right-section_${opKey} ${opKey} ${this.currentRole}`;
				}

				return 'reporting-right-section_custom';
			})
		);

		this.reportingDataService.topMenu$$.subscribe(topMenu => {
			this.reportingDataService.fullMenuItems = [];
			// строим полное меню отчетов для секции "Другое"
			const items = topMenu.filter(elem => elem.path !== ReportsId.Reports)
				.map(elem => {
					const subMenuItems: Array<IOperationListItem> = this.reportingDataService.makeOperationsListByReportId(elem.path);

					return {...elem, defaultLabel: elem.label, label: null, expanded: false, subMenuItems};
				})
				.filter(elem => elem.subMenuItems.length !== 0);

			// добавить меню ввод-вывод денег только оператору
			const arr: Array<IOperationMenuItem> = [...items];
			const upo = this.reportingDataService.addUserPaymentOperations();
			if (upo) {
				arr.push(upo);
			}
			// const loyalty = this.reportingDataService.addLoyaltyCardMenu();
			// arr.push(loyalty);
			this.reportingDataService.fullMenuItems = arr;

			// https://jira.emict.net/browse/CS-4524
			if (this.appStoreService.operator.value.accessLevel === 1) {
				const bmlIndex = this.reportingDataService.fullMenuItems.findIndex(elem => elem.path === ReportsId.PaperInstant);
				if (bmlIndex > -1) {
					const invIndex = this.reportingDataService.fullMenuItems[bmlIndex].subMenuItems.findIndex(elem => elem.path === '22');
					if (invIndex > -1) {
						this.reportingDataService.selectMenuItem(bmlIndex, invIndex);
					}
				}
			} else {
				this.reportingDataService.selectMenuItem(0, 0);
			}
		});

		// подписаться на загруженные данные отчета для подготовки их к печати и валидации кнопки печати
		this.disabledPrintButton$ = this.reportsService.lastReportResponse$$
			.pipe(
				takeUntil(this.unsubscribe$$),
				debounceTime(100),
				map(resp => {
					if (resp) {
						this.lastPrintData = this.reportsService.parseLastLoadedReportToPrint();
						if (this.lastPrintData && this.lastPrintData.length > 0) {
							return false;
						}
					}

					return true;
				})
			);

		this.reportingDataService.customOperation$$.next(undefined);
		this.checkCanRequest();
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.reportingDataService.currentReport$$.next(undefined);
		this.reportingDataService.currentOperation$$.next(undefined);
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}
}
