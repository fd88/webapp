import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { ExitReportingGuard } from '@app/reporting/guards/exit-reporting.guard';
import { NavigationGuard } from '@app/core/guards/navigation.guard';
import { URL_OTHER } from '@app/util/route-utils';
import { ReportingComponent } from '@app/reporting/components/reporting/reporting.component';
import { OtherComponent } from '@app/reporting/components/other/other.component';

const routes: Routes = [
	{
		path: '',
		component: ReportingComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		]
	},
	{
		path: URL_OTHER,
		component: OtherComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			ExitReportingGuard,
			NavigationGuard
		]
	}
];

/**
 * Модуль маршрутизации.
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	]
})
export class ReportingRoutingModule {}
