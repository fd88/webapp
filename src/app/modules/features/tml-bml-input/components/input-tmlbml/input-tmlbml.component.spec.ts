import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputTmlbmlComponent } from './input-tmlbml.component';
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TmlBmlInputStoreService} from "@app/tml-bml-input/services/tml-bml-input-store.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {TmlBmlListComponent} from "@app/tml-bml-input/components/tml-bml-list/tml-bml-list.component";
import {TwoStepPanelComponent} from "@app/tml-bml-input/components/two-step-panel/two-step-panel.component";
import {SESS_DATA} from "../../../mocks/session-data";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {Operator} from "@app/core/services/store/operator";
import {CoreModule} from "@app/core/core.module";
import {MslInputBarcodeComponent} from "@app/tml-bml-input/components/msl-input-barcode/msl-input-barcode.component";
import {timer} from "rxjs";
import {DRAWS_FOR_GAME_100} from "../../../mocks/game100-draws";

xdescribe('InputTmlbmlComponent', () => {
  let component: InputTmlbmlComponent;
  let fixture: ComponentFixture<InputTmlbmlComponent>;
  let appStoreService: AppStoreService;
  let barcodeReaderService: BarcodeReaderService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			RouterTestingModule.withRoutes(ROUTES),
			SharedModule,
			TranslateModule.forRoot({
				loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
				}
			}),
			HttpClientModule,
			CoreModule
		],
      	declarations: [
			InputTmlbmlComponent,
			TmlBmlListComponent,
			TwoStepPanelComponent,
			MslInputBarcodeComponent
		],
		providers: [
			TmlBmlInputStoreService,
			BarcodeReaderService,
			HttpService,
			HttpClient,
			AppStoreService
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
	  const op = JSON.parse(SESS_DATA.data[0].value);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
	  appStoreService.isLoggedIn$$.next(true);
	  barcodeReaderService = TestBed.inject(BarcodeReaderService);
	fixture = TestBed.createComponent(InputTmlbmlComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test ngOnInit lifecycle hook', async () => {
		component.draws = DRAWS_FOR_GAME_100.draws;
		component.isNeededDrawsCodeCheck = true;
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		barcodeReaderService.barcodeSubject$$.next({
			barcode: '0631123412341234',
			isCorrectBarcode: true
		});
		await timer(200).toPromise();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test onEnteredBarcodeHandler', async () => {
		spyOn(component, 'onEnteredBarcodeHandler').and.callThrough();
		component.onEnteredBarcodeHandler('1111222233334444');
		expect(component.onEnteredBarcodeHandler).toHaveBeenCalled();
	});

	it('test onChangeCameraState', async () => {
		spyOn(component, 'onChangeCameraState').and.callThrough();
		component.onChangeCameraState(false);
		expect(component.onChangeCameraState).toHaveBeenCalled();
	});
});
