import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { debounceTime, map, takeUntil } from 'rxjs/operators';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { TmlBmlInputStoreService } from '@app/tml-bml-input/services/tml-bml-input-store.service';
import { ITmlBmlListItem } from '@app/tml-bml-input/interfaces/itml-bml-list-item';
import { TmlBmlListComponent } from '@app/tml-bml-input/components/tml-bml-list/tml-bml-list.component';

/**
 * Компонент ввода баркодов для лотереи "Стирачки".
 */
@Component({
	selector: 'app-input-tmlbml',
	templateUrl: './input-tmlbml.component.html',
	styleUrls: ['./input-tmlbml.component.scss']
})
export class InputTmlbmlComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Задает список тиражей, по которым возможен ввод билетов.
	 */
	@Input()
	set draws(value: Array<UpdateDrawInfoDraws>) {
		this.tmlBmlInputStoreService.draws = value;
	}

	/**
	 * Необходимость контролировать код тиража в списке.
	 * Если контроль включен и количество кодов будет более одного, будет сгенерирована ошибка.
	 */
	@Input()
	set isNeededDrawsCodeCheck(value: boolean) {
		this.tmlBmlInputStoreService.isNeededDrawsCodeCheck = value;
	}


	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие генерируется после ввода нового диапазона и изменении общего списка диапазонов.
	 */
	@Output()
	readonly inputListChanged = new EventEmitter<Array<ITmlBmlListItem>>();

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Ссылка на компонент списка введенных билетов.
	 */
	@ViewChild('tmlBmlList') tmlBmlList: TmlBmlListComponent;

	/**
	 * Наблюдаемая переменная для ручного ввода баркода.
	 */
	manualBarcode$$ = new Subject<string>();

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TmlBmlInputStoreService} tmlBmlInputStoreService Сервис хранения данных ввода билетов БМЛ.
	 * @param {BarcodeReaderService} barcodeReaderService Сервис чтения баркодов.
	 */
	constructor(
		readonly tmlBmlInputStoreService: TmlBmlInputStoreService,
		private readonly barcodeReaderService: BarcodeReaderService,
	) {}

	/**
	 * Слушатель события {@link TwoStepPanelComponent.barcodeEntered barcodeEntered}.
	 *
	 * @param {string} barcode Введенный вручную баркод.
	 */
	onEnteredBarcodeHandler(barcode: string): void {
		this.manualBarcode$$.next(barcode);
	}

	/**
	 * Обработчик события изменения состояния камеры.
	 * @param camState Состояние камеры.
	 */
	onChangeCameraState(camState: boolean): void {
		this.tmlBmlList.scrollToBottom();
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.tmlBmlInputStoreService.init();

		this.tmlBmlInputStoreService.ticketsList$$
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(next => {
				this.inputListChanged.emit(next);
			});

		// запустить прослушивание сервиса чтения баркодов и ручного ввода
		// предотвращает дублирование ввода при фокусе на поле ввода и сканировании штрих-кода сканером
		merge(
			this.barcodeReaderService.barcodeSubject$$.pipe(map(m => m.barcode)),
			this.manualBarcode$$
		)
			.pipe(
				takeUntil(this.unsubscribe$$),
				debounceTime(100)
			)
			.subscribe(bc => {
				this.tmlBmlInputStoreService.parseNewBarcode(bc);
				// Может перепрыгивать в конец, если неправильный штрихкод
				this.tmlBmlList.scrollToBottom();
			});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
