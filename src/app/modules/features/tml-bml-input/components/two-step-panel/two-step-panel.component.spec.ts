import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TwoStepPanelComponent} from './two-step-panel.component';
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../../app.module";
import {SESS_DATA} from "../../../mocks/session-data";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {Operator} from "@app/core/services/store/operator";
import {MslInputBarcodeComponent} from "../msl-input-barcode/msl-input-barcode.component";
import {SpoilerComponent} from "@app/shared/components/spoiler/spoiler.component";
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {timer} from "rxjs";
import {TmlBmlInputStoreService} from "../../services/tml-bml-input-store.service";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {TmlBmlInputMode} from "../../enums/tml-bml-input-mode.enum";
import {UserInputMode} from "@app/util/utils";
import {StepNumber} from "../../enums/step-number.enum";
import {CameraComponent} from "../../../camera/components/camera/camera.component";

describe('TwoStepPanelComponent', () => {
  let component: TwoStepPanelComponent;
  let fixture: ComponentFixture<TwoStepPanelComponent>;
  let appStoreService: AppStoreService;
  let tmlBmlInputStoreService: TmlBmlInputStoreService;

  beforeEach(waitForAsync(() => {
	  TestBed.configureTestingModule({
		  imports: [
			  CoreModule,
			  HttpClientModule,
			  TranslateModule.forRoot({
				  loader: {
					  provide: TranslateLoader,
					  useFactory: HttpLoaderFactory,
					  deps: [HttpClient]
				  }
			  }),
			  RouterTestingModule.withRoutes(ROUTES)
		  ],
		  declarations: [
			  TwoStepPanelComponent,
			  CameraComponent,
			  MslInputBarcodeComponent,
			  SpoilerComponent
		  ],
		  providers: [
			  HttpService,
			  HttpClient,
			  AppStoreService,
			  TmlBmlInputStoreService,
			  StorageService
		  ]
	  })
		  .compileComponents();
  }));

  beforeEach(() => {

	  const op = JSON.parse(SESS_DATA.data[0].value);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
	  appStoreService.isLoggedIn$$.next(true);
	  tmlBmlInputStoreService = TestBed.inject(TmlBmlInputStoreService);

	fixture = TestBed.createComponent(TwoStepPanelComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
  });

  it('should create', async () => {
	await timer(100).toPromise();
	expect(component).toBeTruthy();
  });

	it('test onRangeClickHandler', () => {
		component.onRangeClickHandler();
		expect(tmlBmlInputStoreService.currentRangeMode$$.value).toEqual(TmlBmlInputMode.TwoStep);
	});

	it('test onRangeClickHandler 2', () => {
		tmlBmlInputStoreService.currentRangeMode$$.next(TmlBmlInputMode.TwoStep);
		component.onRangeClickHandler();
		expect(tmlBmlInputStoreService.currentRangeMode$$.value).toEqual(TmlBmlInputMode.OneStep);
	});

	it('test onClearClickHandler', () => {
		tmlBmlInputStoreService.currentErrorForStep2$$.next({
			errorText: 'Error'
		});
		component.onClearClickHandler();
		expect(tmlBmlInputStoreService.currentErrorForStep2$$.value).toBeUndefined();
	});

	it('test onCameraClose event', () => {
		component.onCameraClose();
		expect(component.userInputMode).toEqual(UserInputMode.Manual);
	});

	it('test onManualEnterMode event', () => {
		component.ngOnInit();
		component.manualEnter = false;
		component.bc1 = new MslInputBarcodeComponent(tmlBmlInputStoreService);
		component.bc1.setFocus = () => {};
		component.onManualEnterMode();
		expect(component.userInputMode).toEqual(UserInputMode.Manual);
	});

	it('test onManualEnterMode event 2', () => {
		component.ngOnInit();
		component.manualEnter = true;
		tmlBmlInputStoreService.currentStep$$.next(StepNumber.Two);
		component.bc2 = new MslInputBarcodeComponent(tmlBmlInputStoreService);
		component.bc2.setFocus = () => {};
		component.onManualEnterMode();
		expect(component.userInputMode).toEqual(UserInputMode.Scanner);
	});

	it('test onChangeCameraState event', () => {
		component.ngOnInit();
		component.manualEnter = true;
		tmlBmlInputStoreService.currentStep$$.next(StepNumber.Two);
		component.bc1 = new MslInputBarcodeComponent(tmlBmlInputStoreService);
		component.bc1.setFocus = () => {};
		component.onChangeCameraState();
		expect(component.userInputMode).toEqual(UserInputMode.Scanner);
	});

	it('test onValueChanges event', async () => {
		component.ngOnInit();
		component.bc1 = new MslInputBarcodeComponent(tmlBmlInputStoreService);
		component.bc1.setFocus = () => {};
		component.onValueChanges('1111222233334444');
		expect(component.barcodeValue).toEqual('1111222233334444');
	});

});
