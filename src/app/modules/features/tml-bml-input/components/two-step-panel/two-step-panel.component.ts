import { Component, EventEmitter, OnDestroy, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Subject, timer } from 'rxjs';
import { StepNumber } from '../../enums/step-number.enum';
import { TmlBmlInputMode } from '../../enums/tml-bml-input-mode.enum';
import { TmlBmlInputStoreService } from '../../services/tml-bml-input-store.service';
import { MslInputBarcodeComponent } from '../msl-input-barcode/msl-input-barcode.component';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApplicationAppId } from '@app/core/services/store/settings';
import { UserInputMode } from '@app/util/utils';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { TmlBmlInputFieldStates } from '@app/tml-bml-input/enums/tml-bml-input-field-states.enum';
import {delay, takeUntil, tap} from 'rxjs/operators';
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";

/**
 * Компонент для ввода диапазона билетов в БМЛ
 */
@Component({
	selector: 'app-two-step-panel',
	templateUrl: './two-step-panel.component.html',
	styleUrls: ['./two-step-panel.component.scss']
})
export class TwoStepPanelComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие генерируется при получении аналогичного события {@link MslInputBarcodeComponent.barcodeEntered barcodeEntered}
	 * из компонента ввода {@link MslInputBarcodeComponent}.
	 */
	@Output()
	readonly barcodeEntered = new EventEmitter<string>();

	/**
	 * Событие смены состояния камеры
	 */
	@Output()
	readonly changedCameraState = new EventEmitter<boolean>(false);

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Коллекция компонентов ввода {@link MslInputBarcodeComponent}.
	 */
	@ViewChildren(MslInputBarcodeComponent)
	inputComponents: QueryList<MslInputBarcodeComponent>;

	/**
	 * Ссылка на первый компонент ввода штрих-кода.
	 */
	@ViewChild('bc1') bc1: MslInputBarcodeComponent;

	/**
	 * Ссылка на второй компонент ввода штрих-кода.
	 */
	@ViewChild('bc2') bc2: MslInputBarcodeComponent;

	@ViewChild('scannerInput') scannerInput: ScannerInputComponent;

	/**
	 * Режим работы модуля ввода "Стирачек"
	 */
	readonly TwoStepInputMode = TmlBmlInputMode;

	/**
	 * Список состояний поля ввода
	 */
	readonly TmlBmlInputFieldStates = TmlBmlInputFieldStates;

	/**
	 * Список шагов при вводе баркода
	 */
	readonly StepNumber = StepNumber;

	/**
	 * Текущее значение штрих-кода.
	 */
	barcodeValue: string;

	/**
	 * Флаг ручного режима ввода
	 */
	manualEnter = false;

	/**
	 * Хранилище режимов ввода для каждого пользователя
	 */
	userInputStorage: {[userID: number]: UserInputMode};

	/**
	 * Текущий режим ввода
	 */
	userInputMode: UserInputMode = 0;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Предыдущее значение отсканированного штрих-кода.
	 */
	prevBc = '';

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TmlBmlInputStoreService} service Сервис хранилища данных модуля ввода "Стирачек".
	 * @param appStoreService Сервис хранилища данных приложения.
	 * @param storageService Сервис хранилища данных в LocalStorage.
	 */
	constructor(
		readonly service: TmlBmlInputStoreService,
		readonly appStoreService: AppStoreService,
		private readonly storageService: StorageService
	) {}

	/**
	 * Слушатель нажатия кнопки изменения режима ввода.
	 */
	onRangeClickHandler(): void {
		const rm = this.service.currentRangeMode$$.value === TmlBmlInputMode.OneStep
			? TmlBmlInputMode.TwoStep
			: TmlBmlInputMode.OneStep;

		this.service.currentRangeMode$$.next(rm);
	}

	/**
	 * Слушатель нажатия кнопки "Очистить".
	 */
	onClearClickHandler(): void {
		this.inputComponents.forEach(f => f.onClearClickHandler());
		this.service.currentStep$$.next(StepNumber.One);
		this.service.stepOneValue$$.next(undefined);
		this.service.stepTwoValue$$.next(undefined);
		this.service.currentErrorForStep1$$.next(undefined);
		this.service.currentErrorForStep2$$.next(undefined);
	}

	/**
	 * Обработчик события закрытия камеры
	 */
	onCameraClose(): void {
		this.manualEnter = true;
		this.userInputMode = this.manualEnter ? UserInputMode.Manual : UserInputMode.Scanner;
		this.putIntoStorage();
		this.changedCameraState.emit(this.manualEnter);
		const tmr = timer(50)
			.subscribe(() => {
				this.bc1.setFocus();
				tmr.unsubscribe();
			});
		this.scannerInput.hideScanner();
	}

	/**
	 * Обработчик события переключения режима ввода
	 */
	onManualEnterMode(): void {
		this.manualEnter = !this.manualEnter;
		if (this.service.currentStep$$.value === StepNumber.Two) {
			this.bc2.setFocus();
		} else {
			this.bc1.setFocus();
		}
		this.changedCameraState.emit(this.manualEnter);
		this.userInputMode = this.manualEnter ? UserInputMode.Manual : UserInputMode.Scanner;
		this.putIntoStorage();
		if (this.manualEnter) {
			this.scannerInput.hideScanner();
		} else {
			this.scannerInput.showScanner();
		}
	}

	/**
	 * Обработчик события изменения значения в поле ввода штрих-кода.
	 * @param result Значение штрих-кода.
	 */
	onValueChanges(result: string): void {
		if (result !== this.prevBc) {
			this.barcodeValue = result;
			this.prevBc = result;
			this.barcodeEntered.emit(this.barcodeValue);
			const tmr = timer(1500)
				.subscribe(() => {
					this.prevBc = '';
					tmr.unsubscribe();
				});
			if (this.service.currentStep$$.value === StepNumber.Two) {
				this.manualEnter = true;
			}
			if (this.service.currentRangeMode$$.value === TmlBmlInputMode.OneStep) {
				const tmr2 = timer(50)
					.subscribe(() => {
						this.bc1.setFocus();
						tmr2.unsubscribe();
					});
			}
		}
	}

	/**
	 * Обработчик события изменения состояния камеры.
	 */
	onChangeCameraState(): void {
		this.manualEnter = !this.manualEnter;
		this.changedCameraState.emit(this.manualEnter);
		// занести в локальное хранилище
		if (!this.manualEnter) {
			this.userInputMode = UserInputMode.Scanner;
			this.putIntoStorage();
		}
		const tmr = timer(50)
			.subscribe(() => {
				this.bc1.setFocus();
				tmr.unsubscribe();
			});
		if (this.manualEnter) {
			this.scannerInput.hideScanner();
		} else {
			this.scannerInput.showScanner();
		}
	}

	/**
	 * Обработчик события ввода штрих-кода в первом поле.
	 * @param $event Передаваемое событие.
	 */
	onBarcodeStep1Entered($event): void {
		this.barcodeEntered.emit($event);
		if (!!this.bc2) {
			this.bc2.setFocus();
		}
	}

	/**
	 * Обработчик получения фокуса в первом поле ввода штрих-кода.
	 */
	onBC1Focus(): void {
		// this.userInputMode = UserInputMode.Manual;
		// this.putIntoStorage();
	}

	/**
	 * Функция сохранения режима ввода в локальное хранилище.
	 * @private
	 */
	private putIntoStorage(): void {
		this.userInputStorage[this.appStoreService.operator.value.userId] = this.userInputMode;
		this.storageService.putSync(ApplicationAppId, [{
			key: 'user_input_mode',
			value: JSON.stringify(this.userInputStorage)
		}]);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		const uInputStorage = this.storageService.getSync(ApplicationAppId, ['user_input_mode']);
		this.userInputStorage = uInputStorage && uInputStorage.data && (uInputStorage.data.length > 0) ?
			JSON.parse(uInputStorage.data[0].value) : {};
		this.userInputMode = this.userInputStorage[this.appStoreService.operator.value.userId] ?
			this.userInputStorage[this.appStoreService.operator.value.userId] : UserInputMode.Scanner;
		this.manualEnter = this.userInputMode === UserInputMode.Manual;
		const tmr = timer(300)
			.pipe(
				tap(() => {
					this.bc1.setReadonly(true);
				}),
				delay(300),
				tap(() => {
					this.bc1.setFocus();
				}),
				delay(300)
			)
			.subscribe(() => {
				this.bc1.setReadonly(false);
				tmr.unsubscribe();
			});
		this.appStoreService.showCameraComponent$$
			.pipe(
				takeUntil(this.unsubscribe$$)
			)
			.subscribe((showCameraComponent) => {
				if (showCameraComponent) {
					if (!this.manualEnter) {
						this.scannerInput.showScanner();
					}
				} else {
					this.scannerInput.hideScanner();
				}
			});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
