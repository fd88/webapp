import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TmlBmlListComponent } from './tml-bml-list.component';
import {CoreModule} from "@app/core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../../app.module";
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {TmlBmlInputStoreService} from "@app/tml-bml-input/services/tml-bml-input-store.service";
import {TML_BML_LIST} from "../../../mocks/mocks";

describe('TmlBmlListComponent', () => {
  let component: TmlBmlListComponent;
  let fixture: ComponentFixture<TmlBmlListComponent>;
  let tmlBmlInputStoreService: TmlBmlInputStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			HttpClientModule,
			TranslateModule.forRoot({
				loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
				}
			}),
			RouterTestingModule.withRoutes(ROUTES)
		],
      declarations: [
		  TmlBmlListComponent
	  ],
	  providers: [
		  TmlBmlInputStoreService
	  ]
    })
    .compileComponents();
  });

  beforeEach(() => {
	tmlBmlInputStoreService = TestBed.inject(TmlBmlInputStoreService);
    fixture = TestBed.createComponent(TmlBmlListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test ngOnInit lifecycle hook', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		tmlBmlInputStoreService.ticketsList$$.next([...TML_BML_LIST]);
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test onDelete event', () => {
		component.ngOnInit();
		tmlBmlInputStoreService.ticketsList$$.next([...TML_BML_LIST]);
		component.onDelete('0662542857123006');
		expect(tmlBmlInputStoreService.ticketsList$$.value.length).toEqual(11);
	});


});
