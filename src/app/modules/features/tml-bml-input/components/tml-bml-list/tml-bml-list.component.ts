import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ITmlBmlListItem } from '../../interfaces/itml-bml-list-item';
import { TmlBmlInputStoreService } from '../../services/tml-bml-input-store.service';
import { Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from '@app/env/environment';
import { TML_BML_LIST } from '../../../mocks/mocks';

/**
 * Интерфейс для группировки списка билетов по номеру игры.
 */
interface GroupedTicketsList {
	/**
	 * Список билетов по номеру игры.
	 */
	[intGameNumber: number]: Array<ITmlBmlListItem>;
}

/**
 * Компонент для отображения списка билетов.
 */
@Component({
	selector: 'app-tml-bml-list',
	templateUrl: './tml-bml-list.component.html',
	styleUrls: ['./tml-bml-list.component.scss']
})
export class TmlBmlListComponent implements OnInit, OnDestroy {

	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Ссылка на элемент для прокрутки списка.
	 */
	@ViewChild('bmlScroll') bmlScroll: ElementRef;

	/**
	 * Список номеров игр.
	 */
	intGameNumbers: Array<number> = [];

	/**
	 * Сгруппированный список билетов.
	 */
	groupedTicketsList: GroupedTicketsList = {};

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TmlBmlInputStoreService} tmlBmlInputStoreService Сервис для работы со списком билетов.
	 */
	constructor(
		readonly tmlBmlInputStoreService: TmlBmlInputStoreService
	) {}

	/**
	 * Функция для прокрутки списка вниз.
	 */
	scrollToBottom(): void {
		const tmr = timer(50)
			.subscribe(() => {
				(this.bmlScroll.nativeElement as HTMLDivElement).scrollTo(0, this.bmlScroll.nativeElement.scrollHeight);
				tmr.unsubscribe();
			});
	}

	/**
	 * Функция для удаления билета из списка.
	 * @param bc Штрих-код билета.
	 */
	onDelete(bc: string): void {
		const index = this.tmlBmlInputStoreService.ticketsList$$.value.findIndex(ticket => ticket.bcFrom.barcode === bc);
		const arr = this.tmlBmlInputStoreService.ticketsList$$.value;
		arr.splice(index, 1);
		this.tmlBmlInputStoreService.ticketsList$$.next(arr);
	}

	/**
	 * Функция для отслеживания изменений в списке билетов.
	 * @param index Индекс элемента.
	 * @param item Элемент списка.
	 */
	trackByTicketFn = (index, item: ITmlBmlListItem) => index;

	/**
	 * Функция для отслеживания изменений в списке номеров игр.
	 * @param index Индекс элемента.
	 * @param item Элемент списка.
	 */
	trackByGameNumber = (index, item: number) => index;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		if (environment.mockData) {
			const tmr = timer(1000)
				.subscribe(() => {
					this.tmlBmlInputStoreService.ticketsList$$.next(TML_BML_LIST);
					tmr.unsubscribe();
				});
		}
		this.tmlBmlInputStoreService.ticketsList$$
			.pipe(takeUntil(this.unsubscribe$$))
			.subscribe(tList => {
				this.groupedTicketsList = {};
				// this.intGameNumbers = [];
				for (const bmlItem of tList) {
					if (!this.groupedTicketsList[bmlItem.bcFrom.intGameNumber]) {
						this.groupedTicketsList[bmlItem.bcFrom.intGameNumber] = [];
						// this.intGameNumbers.push(bmlItem.bcFrom.intGameNumber);
					}
					this.groupedTicketsList[bmlItem.bcFrom.intGameNumber].push(bmlItem);
				}
				// Ищем, было ли что-то удалено
				this.intGameNumbers = this.intGameNumbers.filter(elem => !!this.groupedTicketsList[elem]);
				// Ищем, было ли что-то добавлено, что увеличилось кол. номеров игр
				const objGameNums = Object.keys(this.groupedTicketsList);
				if (objGameNums.length > this.intGameNumbers.length) {
					for (const objGameNum of objGameNums) {
						const intObjGameNum = parseInt(objGameNum, 10);
						if (this.intGameNumbers.indexOf(intObjGameNum) === -1) {
							this.intGameNumbers.push(intObjGameNum);
						}
					}
				}
				// this.scrollToBottom();
		});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}
}
