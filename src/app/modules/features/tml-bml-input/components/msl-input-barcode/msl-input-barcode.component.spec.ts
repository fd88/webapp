import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MslInputBarcodeComponent} from './msl-input-barcode.component';
import {CoreModule} from "@app/core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpLoaderFactory} from "../../../../../app.module";
import {RouterTestingModule} from "@angular/router/testing";
import {ROUTES} from "../../../../../app-routing.module";
import {ElementRef} from "@angular/core";
import {TmlBmlInputStoreService} from "@app/tml-bml-input/services/tml-bml-input-store.service";
import {TmlBmlInputFieldStates} from "@app/tml-bml-input/enums/tml-bml-input-field-states.enum";
import {BehaviorSubject} from "rxjs";

describe('MslInputBarcodeComponent', () => {
  let component: MslInputBarcodeComponent;
  let fixture: ComponentFixture<MslInputBarcodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			HttpClientModule,
			TranslateModule.forRoot({
				loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
				}
			}),
			RouterTestingModule.withRoutes(ROUTES)
		],
      declarations: [ MslInputBarcodeComponent ],
		providers: [
			TmlBmlInputStoreService
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MslInputBarcodeComponent);
    component = fixture.componentInstance;
    const elem = document.createElement('input');
	elem.value = '0662123412341';
	component.inputElement = new ElementRef<HTMLInputElement>(elem);
	component.status = new BehaviorSubject<TmlBmlInputFieldStates>(TmlBmlInputFieldStates.OkInput);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onInputHandler', () => {
		component.ngOnInit();
		const ev = new Event('onInputHandler');
		const target = document.createElement('input');
		target.value = '0662123412341';
		spyOn(component, 'onInputHandler').and.callThrough();
		component.onInputHandler({...ev, target});
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 2', () => {
		component.ngOnInit();
		const ev = new Event('onInputHandler');
		const target = document.createElement('input');
		target.value = '';
		spyOn(component, 'onInputHandler').and.callThrough();
		component.onInputHandler({...ev, target});
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 3', () => {
		component.ngOnInit();
		const ev = new Event('onInputHandler');
		const target = document.createElement('input');
		target.value = '0662123412341234';
		spyOn(component, 'onInputHandler').and.callThrough();
		component.onInputHandler({...ev, target});
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onClickKeyboardHandler', () => {
		spyOn(component, 'onClickKeyboardHandler').and.callThrough();
		component.onClickKeyboardHandler();
		expect(component.onClickKeyboardHandler).toHaveBeenCalled();
	});

	it('test onChangeFocus', () => {
		spyOn(component, 'onChangeFocus').and.callThrough();
		component.onChangeFocus(false);
		expect(component.onChangeFocus).toHaveBeenCalled();
	});

});
