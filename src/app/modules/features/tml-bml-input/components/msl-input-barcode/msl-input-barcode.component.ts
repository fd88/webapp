import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { BehaviorSubject, combineLatest, Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { StepNumber } from '@app/tml-bml-input/enums/step-number.enum';
import { TmlBmlInputFieldStates } from '@app/tml-bml-input/enums/tml-bml-input-field-states.enum';
import { TmlBmlInputStoreService } from '@app/tml-bml-input/services/tml-bml-input-store.service';

/**
 * Компонент ввода штрих-кода.
 * Представляет собой input с элементами подсчета количества символов
 * и индикатором текущего состояния типа {@link TmlBmlInputFieldStates}.
 */
@Component({
	selector: 'app-msl-input-barcode',
	templateUrl: './msl-input-barcode.component.html',
	styleUrls: ['./msl-input-barcode.component.scss']
})
export class MslInputBarcodeComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Определяет {@link StepNumber шаг}, для которого данный элемент ввода предназначен.
	 */
	@Input()
	step: StepNumber;

	/**
	 * Задать объект наблюдения за статусом компонента ввода.
	 */
	@Input()
	status: BehaviorSubject<TmlBmlInputFieldStates>;

	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие генерируется при получении 16 символов с предполагаемым баркодом.
	 */
	@Output()
	readonly barcodeEntered = new EventEmitter<string>();

	/**
	 * Событие, которое генерируется при получении фокуса в поле ввода.
	 */
	@Output()
	readonly focused = new EventEmitter<null>();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на элемент ввода.
	 */
	@ViewChild('inputElement', { static: true })
	inputElement: ElementRef;

	/**
	 * Признак доступности поля ввода только для чтения.
	 */
	inputReadonly = false;

	/**
	 * Список состояний поля ввода:
	 */
	readonly TmlBmlInputFieldStates = TmlBmlInputFieldStates;

	/**
	 * Минимальное количество символов в поле ввода штрих-кода.
	 */
	readonly MinLength = 13;

	/**
	 * Максимальное количество символов в поле ввода штрих-кода.
	 */
	readonly MaxLength = 16;

	/**
	 * Отступ от нижней границы экрана до нижней границы виртуальной клавиатуры.
	 */
	bottomPadding: number;

	// -----------------------------
	//  Private properties
	// -----------------------------

	/**
	 * Признак наличия фокуса в поле ввода.
	 */
	private readonly focus$$ = new BehaviorSubject<boolean>(false);

	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TmlBmlInputStoreService} service Сервис хранения состояния ввода.
	 */
	constructor(
		readonly service: TmlBmlInputStoreService
	) {}

	/**
	 * Метод устанавливает фокус в поле ввода.
	 */
	setFocus(): void {
		const tmr = timer(200)
			.subscribe(() => {
				(this.inputElement.nativeElement as HTMLInputElement).focus();
				tmr.unsubscribe();
			});
	}

	/**
	 * Метод устанавливает доступность поля ввода только для чтения.
	 * @param isReadonly Признак доступности поля ввода только для чтения.
	 */
	setReadonly(isReadonly: boolean): void {
		this.inputReadonly = isReadonly;
	}

	/**
	 * Обработчик события {@link input}.
	 * По результату ввода определяет текущее состояние ввода - {@link TmlBmlInputFieldStates}.
	 *
	 * @param {Event} event Передаваемое событие.
	 */
	onInputHandler(event: Event): void {
		if (event.target && event.target instanceof HTMLInputElement && event.target.value) {
			const barcode = event.target.value;
			if (barcode.length === 13 || barcode.length === 16) {
				// писк
				this.barcodeEntered.emit(barcode);
			}
		} else if (event.target && event.target instanceof HTMLInputElement && event.target.value === '') {
			this.clearInputField();
		}
	}

	/**
	 * Слушатель нажатия на кнопку очистки поля ввода.
	 */
	onClearClickHandler(): void {
		this.clearInputField();
	}

	/**
	 * Слушатель нажатия на кнопку активации виртуальной клавиатуры.
	 */
	onClickKeyboardHandler(): void {
		(this.inputElement.nativeElement as HTMLInputElement).focus();
	}

	/**
	 * Слушатель изменения фокуса на элементе ввода.
	 *
	 * @param {boolean} value Признак наличия фокуса.
	 */
	onChangeFocus(value: boolean): void {
		this.focus$$.next(value);
		this.focused.emit();
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Очистить поле ввода.
	 */
	private clearInputField(): void {
		this.inputElement.nativeElement.value = '';

		if (this.step === StepNumber.One) {
			this.service.inputStatusForStep1$$.next(TmlBmlInputFieldStates.Clear);
			this.service.currentErrorForStep1$$.next(undefined);
		} else {
			this.service.inputStatusForStep2$$.next(TmlBmlInputFieldStates.Clear);
			this.service.currentErrorForStep2$$.next(undefined);
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		// обновить поля ввода по последним значениям
		combineLatest([this.service.stepOneValue$$, this.service.stepTwoValue$$])
			.pipe(
				takeUntil(this.unsubscribe$$)
			)
			.subscribe(v => {
				const bcObj = this.step === StepNumber.One ? v[0] : v[1];
				(this.inputElement.nativeElement as HTMLInputElement).value = bcObj ? bcObj.barcode : '';
			});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.focus$$.complete();
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
