/**
 * Список шагов при вводе баркода.
 * - {@link One} - ввод первого баркода из диапазона
 * - {@link Two} - ввод второго баркода из диапазона
 */
export enum StepNumber {
	One = 'first',
	Two = 'second'
}
