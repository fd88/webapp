/**
 * Режим работы модуля ввода "Стирачек":
 * - {@link OneStep} - ввод кодов поштучно
 * - {@link TwoStep} - ввод кодов диапазоном (первый и последний номер)
 */
export enum TmlBmlInputMode {
	OneStep = 'one-step',
	TwoStep = 'two-step'
}
