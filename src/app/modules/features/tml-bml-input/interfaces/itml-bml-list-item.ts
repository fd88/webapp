import { BarcodeObject } from '../../../core/barcode/barcode-object';
import { UpdateDrawInfoDraws } from '../../../core/net/http/api/models/update-draw-info';

/**
 * Модель элемента списка "Стирачек".
 */
export interface ITmlBmlListItem {
	/**
	 * Элемент начала диапазона.
	 */
	bcFrom: BarcodeObject;

	/**
	 * Элемент окончания диапазона.
	 */
	bcTo: BarcodeObject;

	/**
	 * Тираж, к которому принадлежит диапазон.
	 */
	draws: UpdateDrawInfoDraws;
}
