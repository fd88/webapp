import {TestBed} from '@angular/core/testing';

import {TmlBmlInputStoreService} from './tml-bml-input-store.service';
import {DRAWS_FOR_GAME_100} from "../../mocks/game100-draws";
import {TmlBmlInputMode} from "@app/tml-bml-input/enums/tml-bml-input-mode.enum";
import {StepNumber} from "@app/tml-bml-input/enums/step-number.enum";
import {TML_BML_LIST} from "../../mocks/mocks";

describe('TmlBmlInputStoreService', () => {
  let service: TmlBmlInputStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TmlBmlInputStoreService);
	service.draws = DRAWS_FOR_GAME_100.draws;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

	it('test parseNewBarcode method', () => {
		service.isNeededDrawsCodeCheck = true;
		service.ticketsList$$.next([...TML_BML_LIST]);
		service.currentRangeMode$$.next(TmlBmlInputMode.TwoStep);
		service.currentStep$$.next(StepNumber.One);
		service.parseNewBarcode('0662542857001');
		service.currentStep$$.next(StepNumber.Two);
		service.parseNewBarcode('0662542857005');
		expect(service.ticketsList$$.value.length).toEqual(12);
	});

	it('test parseNewBarcode method 2', () => {
		service.ticketsList$$.next([...TML_BML_LIST]);
		service.currentRangeMode$$.next(TmlBmlInputMode.OneStep);
		service.currentStep$$.next(StepNumber.One);
		service.parseNewBarcode('0047123412341234');
		expect(service.ticketsList$$.value.length).toEqual(13);
	});

	it('test parseNewBarcode method 3', () => {
		service.ticketsList$$.next([...TML_BML_LIST]);
		service.currentRangeMode$$.next(TmlBmlInputMode.TwoStep);
		service.currentStep$$.next(StepNumber.One);
		service.parseNewBarcode('0047542857001');
		service.currentStep$$.next(StepNumber.Two);
		service.parseNewBarcode('0047642857005');
		expect(service.ticketsList$$.value.length).toEqual(12);
	});

	it('test parseNewBarcode method 4', () => {
		service.isNeededDrawsCodeCheck = true;
		service.ticketsList$$.next([...TML_BML_LIST]);
		service.currentRangeMode$$.next(TmlBmlInputMode.TwoStep);
		service.currentStep$$.next(StepNumber.One);
		service.parseNewBarcode('0047542857001');
		service.currentStep$$.next(StepNumber.Two);
		service.parseNewBarcode('0047542857005');
		expect(service.ticketsList$$.value.length).toEqual(12);
	});

	it('test parseNewBarcode method 5', () => {
		service.isNeededDrawsCodeCheck = false;
		service.ticketsList$$.next([...TML_BML_LIST]);
		service.currentRangeMode$$.next(TmlBmlInputMode.TwoStep);
		service.currentStep$$.next(StepNumber.One);
		service.parseNewBarcode('0662542857001006');
		service.currentStep$$.next(StepNumber.Two);
		service.parseNewBarcode('0662542857001006');
		expect(service.ticketsList$$.value.length).toEqual(12);
	});
});
