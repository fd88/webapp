import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LotteriesComponent } from '@app/lotteries/components/lotteries/lotteries.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля отображения списка лотерей.
 */
const routes: Routes = [
	{
		path: '',
		component: LotteriesComponent,
		children: [],
		canDeactivate: [
			NavigationGuard
		]
	}
];

/**
 * Модуль маршрутизации для модуля отображения списка лотерей.
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
	]
})
export class LotteriesRoutingModule {}
