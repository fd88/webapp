import { Component, Input } from '@angular/core';
import { IGameItem } from '@app/lotteries/components/game-list/game-list.component';

/**
 * Компонент для отображения игры в списке игр
 */
@Component({
	selector: 'app-game-item',
	templateUrl: './game-item.component.html',
	styleUrls: ['./game-item.component.scss']
})
export class GameItemComponent  {

	/**
	 * Элемент списка игр
	 */
	@Input()
	gameItem: IGameItem;
}
