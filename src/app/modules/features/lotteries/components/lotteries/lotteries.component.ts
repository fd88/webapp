import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, merge, Subject, timer } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import {
	URL_GONKA,
	URL_INIT,
	URL_INSTANT,
	URL_KARE,
	URL_LOTTERIES,
	URL_MEGALOT,
	URL_RESULTS,
	URL_SPORTPROGNOZ,
	URL_TIPTOP,
	URL_TMLBML,
	URL_ZABAVA
} from '@app/util/route-utils';
import { IGameItem } from '@app/lotteries/components/game-list/game-list.component';
import { TotalCheckListSize } from '@app/total-check/components/total-check-info-panel/total-check-info-panel.component';
import { OneButtonCustomComponent } from '@app/shared/components/one-button-custom/one-button-custom.component';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { ApplicationAppId } from '@app/core/services/store/settings';
import { StorageGetResp } from '@app/core/net/ws/api/models/storage/storage-get';
import { LogoViewStyle } from '@app/shared/components/msl-game-logo/msl-game-logo.component';
import { selectActualDraws } from '@app/util/utils';
import { DOCUMENT } from '@angular/common';

/**
 * Интерфейс информации о джек-поте
 */
interface JackpotInfo {
	/**
	 * Информация о лотерее.
	 */
	lottery: IGameItem;
	/**
	 * Джекпот.
	 */
	jackpot?: string;
	/**
	 * Мегаприз.
	 */
	megaprize?: string;
}

/**
 * Компонент выбора лотерей из списка разрешенных.
 */
@Component({
	selector: 'app-lotteries',
	templateUrl: './lotteries.component.html',
	styleUrls: ['./lotteries.component.scss']
})
export class LotteriesComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Перечисление возможной высоты списка операций.
	 */
	readonly TotalCheckListSize = TotalCheckListSize;

	/**
	 * Перечисление возможных стилей отображения логотипа.
	 */
	readonly LogoViewStyle = LogoViewStyle;

	/**
	 * Признак того, что отображаемый элемент есть лотереей
	 */
	isLottery: boolean;

	/**
	 * Наблюдаемая переменная, содержащая список лотерей.
	 */
	gameList$$ = new BehaviorSubject<Array<IGameItem>>([]);

	/**
	 * Количество отображаемых лотерей.
	 */
	visibleLotteriesCount = 9;

	/**
	 * Текущая дата.
	 */
	currentDateStr: string;

	/**
	 * Информация о джек-потах.
	 */
	jackpotsInfo: Array<JackpotInfo> = [];

	/**
	 * Ссылка на диалого-окно с информацией о джек-потах.
	 */
	@ViewChild('jackpotinfo', {static: false}) jackpotinfo: OneButtonCustomComponent;


	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Текущий префикс маршрута для стартовой страницы лотереи
	 * @private
	 */
	private currentPrefix: string;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {Router} router Объект маршрутизатора.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {ActivatedRoute} route Активный маршрут.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {StorageService} storageService Сервис хранилища данных в localStorage.
	 * @param document Объект документа.
	 */
	constructor(
		private readonly router: Router,
		private readonly appStoreService: AppStoreService,
		private readonly route: ActivatedRoute,
		readonly transactionService: TransactionService,
		private readonly storageService: StorageService,
		@Inject(DOCUMENT) private readonly document: Document
	) {}


	/**
	 * Слушатель клика на лотереи.
	 * По нажатию маршрутизатор активирует окно инициализации соответствующей лотереи.
	 *
	 * @param {IGameItem} game Выбранная лотерея.
	 */
	onClickLotteryHandler(game: IGameItem): void {
		this.router.navigateByUrl(game.route)
			.catch(err => Logger.Log.e('LotteriesComponent', `can't navigate to path: ${err}`)
				.console());
	}

	/**
	 * Слушатель клика по области логотипа.
	 *
	 * @param {string} route Маршрут к лотерее.
	 */

	goToLottery(route: string): void {
		this.jackpotinfo.hide();
		this.router.navigate([`/${route}`])
			.then()
			.catch();
	}

	/**
	 * Функция для отслеживания изменений в массиве лотерей.
	 * @param index Индекс элемента в массиве.
	 * @param item Элемент массива.
	 */
	trackByFn(index: number, item): number {
		return index;
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Создание списка лотерей.
	 * @private
	 */
	private createLotteryList(): Array<IGameItem> {
		return [
			{
				name: 'lottery.zabava.loto_zabava',
				enabled: this.appStoreService.Draws?.hasLottery(LotteryGameCode.Zabava),
				styleClass: 'zabava',
				route: `${URL_LOTTERIES}/${URL_ZABAVA}/${this.currentPrefix}`
			},
			{
				name: 'lottery.megalot.megalot_name',
				enabled: this.appStoreService.Draws?.hasLottery(LotteryGameCode.MegaLot),
				styleClass: 'megalot',
				route: `${URL_LOTTERIES}/${URL_MEGALOT}/${this.currentPrefix}`
			},
			{
				name: 'lottery.instant.loto_momentary',
				enabled: this.appStoreService.Draws?.hasInstantLotteries(LotteriesGroupCode.EInstant),
				styleClass: 'instant-1',
				route: `${URL_LOTTERIES}/${URL_INSTANT}/${this.currentPrefix}/${LotteriesGroupCode.EInstant}`,
				hideResults: this.currentPrefix === URL_RESULTS
			},
			{
				name: 'lottery.instant.loto_golden_triumph',
				enabled: this.appStoreService.Draws?.hasInstantLotteries(LotteriesGroupCode.VBL),
				styleClass: 'instant-2',
				route: `${URL_LOTTERIES}/${URL_INSTANT}/${this.currentPrefix}/${LotteriesGroupCode.VBL}`,
				hideResults: this.currentPrefix === URL_RESULTS
			},
			{
				name: 'lottery.tiptop.game_name',
				enabled: (this.appStoreService.Draws?.hasLottery(LotteryGameCode.Tip)
					|| this.appStoreService.Draws.hasLottery(LotteryGameCode.Top)),
				styleClass: 'tiptop',
				route: `${URL_LOTTERIES}/${URL_TIPTOP}/${this.currentPrefix}`
			},
			{
				name: 'lottery.race_for_money.loto_gonka',
				enabled: this.appStoreService.Draws?.hasLottery(LotteryGameCode.Gonka),
				styleClass: 'gonka',
				route: `${URL_LOTTERIES}/${URL_GONKA}/${this.currentPrefix}`
			},
			{
				name: 'lottery.tmlbml.game_name',
				enabled: this.appStoreService.Draws?.hasLottery(LotteryGameCode.TML_BML),
				styleClass: 'tmlbml',
				route: `${URL_LOTTERIES}/${URL_TMLBML}/${this.currentPrefix}`,
				hideResults: this.currentPrefix === URL_RESULTS
			},
			{
				name: 'lottery.sportprognoz.game_name',
				enabled: this.appStoreService.Draws?.hasLottery(LotteryGameCode.Sportprognoz),
				styleClass: 'sportprognoz',
				route: `${URL_LOTTERIES}/${URL_SPORTPROGNOZ}/${this.currentPrefix}`
			},
			{
				name: 'lottery.kare.game_name',
				enabled: this.appStoreService.Draws?.hasLottery(LotteryGameCode.Kare),
				styleClass: 'kare',
				route: `${URL_LOTTERIES}/${URL_KARE}/${this.currentPrefix}`
			},
			// TODO: лотереи в процессе разработки -----------------------------------------------------
			{
				name: 'Спорт Ліга',
				enabled: this.appStoreService.Draws?.hasLottery(0) && false,
				styleClass: 'sportliga',
				route: ``
			}
		];
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		const o1$ = this.route.data
			.pipe(
				takeUntil(this.unsubscribe$$),
				tap(v => {
					this.isLottery = !!v && v.mode === URL_LOTTERIES;
					this.currentPrefix = this.isLottery ? URL_INIT : URL_RESULTS;
					this.document.getElementsByTagName('html')[0].classList
						.add('overflow-hidden');
					this.document.getElementsByTagName('body')[0].classList
						.add('overflow-hidden');
				})
			);

		const o2$ = this.appStoreService.Draws?.lotteryList$
			.pipe(takeUntil(this.unsubscribe$$));

		merge(o1$, o2$)
			.subscribe(() => {
				const lottList = this.createLotteryList();
				this.gameList$$.next(lottList);
				this.visibleLotteriesCount = lottList.filter(elem => !!elem.route && elem.enabled).length;
			});

		const dateObj = new Date();
		const currentDate = dateObj.toLocaleDateString();
		const currentTime = dateObj.toLocaleTimeString();
		const timeParts = currentTime.split(':');
		this.currentDateStr = `${currentDate}, ${timeParts[0]}:${timeParts[1]}`;

		// ДЖЕК-ПОТЫ
		// Забава
		const zabavaDraws = selectActualDraws(this.appStoreService.Draws, LotteryGameCode.Zabava);
		if (zabavaDraws && zabavaDraws.length && zabavaDraws[0].draw.jackpot) {
			this.jackpotsInfo.push({
				lottery: {
					name: 'lottery.zabava.loto_zabava',
					enabled: this.appStoreService.Draws.hasLottery(LotteryGameCode.Zabava),
					styleClass: 'zabava-icon',
					route: `${URL_LOTTERIES}/${URL_ZABAVA}/${this.currentPrefix}`,
					gameCode: LotteryGameCode.Zabava
				},
				jackpot: zabavaDraws[0].draw.jackpot
			});
		}
		// Мегалот
		const megalotDraws = selectActualDraws(this.appStoreService.Draws, LotteryGameCode.MegaLot);
		if (megalotDraws && megalotDraws.length &&
			(megalotDraws[0].draw.jackpot || (megalotDraws[0].draw.data && megalotDraws[0].draw.data.mega_prize))) {
			const megalotItem: JackpotInfo = {
				lottery: {
					name: 'lottery.megalot.megalot_name',
					enabled: this.appStoreService.Draws.hasLottery(LotteryGameCode.MegaLot),
					styleClass: 'megalot-icon',
					route: `${URL_LOTTERIES}/${URL_MEGALOT}/${this.currentPrefix}`,
					gameCode: LotteryGameCode.MegaLot
				}
			};

			if (megalotDraws[0].draw.jackpot) {
				megalotItem.jackpot = megalotDraws[0].draw.jackpot;
			}

			if (megalotDraws[0].draw.data && megalotDraws[0].draw.data.mega_prize) {
				megalotItem.megaprize = megalotDraws[0].draw.data.mega_prize;
			}

			this.jackpotsInfo.push(megalotItem);
		}
		// Спортпрогноз
		const sportpDraws = selectActualDraws(this.appStoreService.Draws, LotteryGameCode.Sportprognoz);
		if (sportpDraws && sportpDraws.length &&
			(sportpDraws[0].draw.jackpot || (sportpDraws[0].draw.data && sportpDraws[0].draw.data.mega_prize))) {
			const sportpItem: JackpotInfo = {
				lottery: {
					name: 'lottery.sportprognoz.game_name',
					enabled: this.appStoreService.Draws.hasLottery(LotteryGameCode.Sportprognoz),
					styleClass: 'sportprognoz-icon',
					route: `${URL_LOTTERIES}/${URL_SPORTPROGNOZ}/${this.currentPrefix}`,
					gameCode: LotteryGameCode.Sportprognoz
				}
			};
			if (sportpDraws[0].draw.jackpot) {
				sportpItem.jackpot = sportpDraws[0].draw.jackpot;
			}
			this.jackpotsInfo.push(sportpItem);
		}

		this.storageService.get(ApplicationAppId, ['jackpotinfotime'])
			.then((stResp: StorageGetResp) => {
				const showingsVal = stResp.data.find(elem => elem.key === 'jackpotinfotime');
				let showings = !!showingsVal ? JSON.parse(showingsVal.value) : null;
				const currStDate = (new Date()).toLocaleDateString();

				if (!showings || !showings[this.appStoreService.operator.value.userId]
					|| showings[this.appStoreService.operator.value.userId] !== currStDate) {
					const tmr = timer(100, 100)
						.pipe(filter(() => !!this.jackpotinfo))
						.subscribe(() => {
							if (this.jackpotsInfo.length > 0) {
								this.jackpotinfo.show();
								if (!showings) {
									showings = {};
								}
								if (!showings[this.appStoreService.operator.value.userId]
									|| showings[this.appStoreService.operator.value.userId] !== currStDate) {
									showings[this.appStoreService.operator.value.userId] = currStDate;
								}
								this.storageService.put(ApplicationAppId, [{
									key: 'jackpotinfotime',
									value: JSON.stringify(showings)
								}])
									.then()
									.catch();
							}
							tmr.unsubscribe();
						});
				}
			})
			.catch(err => console.log(err));
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
		this.document.getElementsByTagName('html')[0].classList
			.remove('overflow-hidden');
		this.document.getElementsByTagName('body')[0].classList
			.remove('overflow-hidden');
	}
}
