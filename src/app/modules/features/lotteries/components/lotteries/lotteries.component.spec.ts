import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LotteriesComponent } from '@app/lotteries/components/lotteries/lotteries.component';
import { GameListComponent } from '@app/lotteries/components/game-list/game-list.component';
import { TotalCheckModule } from '@app/total-check/total-check.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from '@app/core/core.module';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LotteriesDraws } from '@app/core/services/store/draws';
import { SharedModule } from '@app/shared/shared.module';
import {ApplicationAppId} from "@app/core/services/store/settings";
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_ZABAVA} from "../../../mocks/zabava-draws";
import {ROUTES} from "../../../../../app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {DRAWS_FOR_GAME_MEGALOT} from "../../../mocks/megalot-draws";
import {URL_INIT, URL_LOTTERIES, URL_ZABAVA} from "@app/util/route-utils";
import {timer} from "rxjs";
import {HttpLoaderFactory} from "../../../../../app.module";
import {HttpClient} from "@angular/common/http";
import {LogService} from "@app/core/net/ws/services/log/log.service";

describe('LotteriesComponent', () => {
	let component: LotteriesComponent;
	let fixture: ComponentFixture<LotteriesComponent>;
	let appStoreService: AppStoreService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes(ROUTES),
				HttpClientTestingModule,
				TranslateModule.forRoot(),
				TotalCheckModule,
				CoreModule,
				SharedModule,
				BrowserAnimationsModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				})
			],
			declarations: [
				LotteriesComponent,
				GameListComponent
			],
			providers: [
				AppStoreService
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		localStorage.setItem(ApplicationAppId, JSON.stringify(SESS_DATA));
		TestBed.inject(LogService);
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		appStoreService.isLoggedIn$$.next(true);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Zabava, {...DRAWS_FOR_GAME_ZABAVA.lottery});
		appStoreService.Draws.setLottery(LotteryGameCode.MegaLot, {...DRAWS_FOR_GAME_MEGALOT.lottery});
		fixture = TestBed.createComponent(LotteriesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test ngOnInit lifecycle hook', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test onClickLotteryHandler', () => {
		spyOn(component, 'onClickLotteryHandler').and.callThrough();
		component.onClickLotteryHandler({
			name: 'Лото Забава',
			enabled: true,
			styleClass: '',
			route: `${URL_LOTTERIES}/${URL_ZABAVA}/${URL_INIT}`
		});
		expect(component.onClickLotteryHandler).toHaveBeenCalled();
	});

	xit('test goToLottery method', () => {
		spyOn(component, 'goToLottery').and.callThrough();
		component.goToLottery(`${URL_LOTTERIES}/${URL_ZABAVA}/${URL_INIT}`);
		expect(component.goToLottery).toHaveBeenCalled();
	});
});
