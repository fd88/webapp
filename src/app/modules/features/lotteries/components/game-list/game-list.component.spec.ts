import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameListComponent } from './game-list.component';
import {LotteriesGroupCode } from "@app/core/configuration/lotteries";
import {
	URL_GONKA,
	URL_INSTANT, URL_KARE,
	URL_LOTTERIES,
	URL_MEGALOT,
	URL_SPORTPROGNOZ,
	URL_TIPTOP, URL_TMLBML,
	URL_ZABAVA
} from "@app/util/route-utils";
import {TranslateModule} from "@ngx-translate/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('GameListComponent', () => {
  const currentPrefix = 'init';
  let component: GameListComponent;
  let fixture: ComponentFixture<GameListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			BrowserAnimationsModule
		],
      declarations: [ GameListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameListComponent);
    component = fixture.componentInstance;
	component.gameList = [
		{
			name: 'lottery.zabava.loto_zabava',
			enabled: true,
			styleClass: 'zabava',
			route: `${URL_LOTTERIES}/${URL_ZABAVA}/${currentPrefix}`
		},
		{
			name: 'lottery.megalot.megalot_name',
			enabled: true,
			styleClass: 'megalot',
			route: `${URL_LOTTERIES}/${URL_MEGALOT}/${currentPrefix}`
		},
		{
			name: 'lottery.instant.loto_momentary',
			enabled: true,
			styleClass: 'instant-1',
			route: `${URL_LOTTERIES}/${URL_INSTANT}/${currentPrefix}/${LotteriesGroupCode.EInstant}`,
			hideResults: false
		},
		{
			name: 'lottery.instant.loto_golden_triumph',
			enabled: true,
			styleClass: 'instant-2',
			route: `${URL_LOTTERIES}/${URL_INSTANT}/${currentPrefix}/${LotteriesGroupCode.VBL}`,
			hideResults: false
		},
		{
			name: 'lottery.tiptop.game_name',
			enabled: true,
			styleClass: 'tiptop',
			route: `${URL_LOTTERIES}/${URL_TIPTOP}/${currentPrefix}`
		},
		{
			name: 'lottery.race_for_money.loto_gonka',
			enabled: true,
			styleClass: 'gonka',
			route: `${URL_LOTTERIES}/${URL_GONKA}/${currentPrefix}`
		},
		{
			name: 'lottery.tmlbml.game_name',
			enabled: true,
			styleClass: 'tmlbml',
			route: `${URL_LOTTERIES}/${URL_TMLBML}/${currentPrefix}`,
			hideResults: false
		},
		{
			name: 'lottery.sportprognoz.game_name',
			enabled: true,
			styleClass: 'sportprognoz',
			route: `${URL_LOTTERIES}/${URL_SPORTPROGNOZ}/${currentPrefix}`
		},
		{
			name: 'lottery.kare.game_name',
			enabled: true,
			styleClass: 'kare',
			route: `${URL_LOTTERIES}/${URL_KARE}/${currentPrefix}`
		},
		// TODO: лотереи в процессе разработки -----------------------------------------------------
		{
			name: 'Спорт Ліга',
			enabled: false,
			styleClass: 'sportliga',
			route: ``
		}
	];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickGameHandler', () => {
		spyOn(component, 'onClickGameHandler').and.callThrough();
		const ev = new MouseEvent('onClickGameHandler');
		const target = document.createElement('li');
		(target as any).value = 0;
		component.onClickGameHandler({ ...ev, target });
		expect(component.onClickGameHandler).toHaveBeenCalled();
	});
});
