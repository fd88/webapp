import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { scaleAnimation } from '@app/util/animations';
import { LotteryGameCode } from '@app/core/configuration/lotteries';

/**
 * Модель элемента лотереи на экране списка лотерей.
 */
export interface IGameItem {
	/**
	 * Название игры.
	 */
	name: string;

	/**
	 * Признак доступности игры.
	 */
	enabled: boolean;

	/**
	 * Класс для отображения иконки игры.
	 */
	styleClass: string;

	/**
	 * Путь к игре.
	 */
	route: string;

	/**
	 * Скрыть элемент на экране с результатами.
	 */
	hideResults?: boolean;

	/**
	 * Код игры.
	 */
	gameCode?: LotteryGameCode;
}

/**
 * Компонент для отрисовки списка игр на экране.
 */
@Component({
	selector: 'app-game-list',
	templateUrl: './game-list.component.html',
	styleUrls: ['./game-list.component.scss'],
	animations: [
		scaleAnimation
	]
})
export class GameListComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Список игр, отображаемых на экране.
	 */
	@Input()
	gameList: Array<IGameItem>;

	/**
	 * Количество доступных лотерей.
	 */
	enabledLotteries = 0;

	/**
	 * Признак лотереи (иначе - это результат).
	 */
	@Input()
	isLottery = true;

	// -----------------------------
	//  Output properties
	// -----------------------------
	/**
	 * Событие выбора игры.
	 */
	@Output()
	readonly gameSelected = new EventEmitter<IGameItem>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Слушатель клика на лотереи.
	 * По нажатию маршрутизатор активирует окно инициализации соответствующей лотереи.
	 *
	 * @param {MouseEvent} event Передаваемое событие.
	 */
	onClickGameHandler(event: MouseEvent): void {
		const target = event.target as HTMLLIElement;
		const val = target.value >= 0
			? target.value
			: ((event.target as HTMLElement).parentElement as HTMLLIElement).value;
		if (!isNaN(val)) {
			const game = this.gameList[val];
			this.gameSelected.emit(game);
		}
	}

	/**
	 * Функция для отслеживания изменений в массиве игр.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByGame = (index, item: IGameItem) => index;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.enabledLotteries = this.gameList.filter(lottery => lottery.enabled && !lottery.hideResults).length;
	}

}
