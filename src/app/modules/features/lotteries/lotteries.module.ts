import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LotteriesComponent } from '@app/lotteries/components/lotteries/lotteries.component';
import { LotteriesRoutingModule } from '@app/lotteries/lotteries-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { TotalCheckModule } from '@app/total-check/total-check.module';

import { GameListComponent } from '@app/lotteries/components/game-list/game-list.component';
import { GameItemComponent } from '@app/lotteries/components/game-item/game-item.component';
import { CheckInformationModule } from '../check-information/check-information.module';

/**
 * Модуль отображения списка лотерей.
 */
@NgModule({
	imports: [
		CommonModule,
		LotteriesRoutingModule,
		SharedModule,
		CheckInformationModule,
		TotalCheckModule.forChild()
	],
	declarations: [
		LotteriesComponent,
		GameListComponent,
		GameItemComponent
	],
	providers: [
	]
})
export class LotteriesModule {}
