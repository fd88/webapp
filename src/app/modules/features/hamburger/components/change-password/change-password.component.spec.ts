import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ChangePasswordComponent, ChangeStatus } from './change-password.component';
import {TranslateModule} from "@ngx-translate/core";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "@app/shared/shared.module";
import {HttpService} from "@app/core/net/http/services/http.service";
import {ChangePasswordService} from "@app/hamburger/services/change-password.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {AuthService} from "@app/core/services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { TransactionServiceStub } from '@app/core/services/transaction/transaction.service.spec';
import { Observable } from 'rxjs';
import { BOChangePasswordResp } from '@app/core/net/http/api/models/bo-change-password';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import {LotteriesService} from "@app/core/services/lotteries.service";
import {MslInputWithEyeComponent} from "@app/shared/components/msl-input-with-eye/msl-input-with-eye.component";
import {OneButtonCustomComponent} from "@app/shared/components/one-button-custom/one-button-custom.component";
import {CloseButtonComponent} from "@app/shared/components/close-button/close-button.component";
import {HamburgerModule} from "@app/hamburger/hamburger.module";

describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			RouterTestingModule.withRoutes([]),
			SharedModule,
			FormsModule,
			ReactiveFormsModule,
			HamburgerModule
		],
      	declarations: [
      		ChangePasswordComponent,
			MslInputWithEyeComponent,
			OneButtonCustomComponent,
			CloseButtonComponent
	  	],
		providers: [
		  	FormBuilder,
			{
				provide: ChangePasswordService,
				useValue: {
					changePassword: (oldPasswd: string, newPasswd: string): Observable<BOChangePasswordResp> => {
						return new Observable(observer => {
							if (oldPasswd === newPasswd) {
								observer.error({
									client_id: '123',
									client_trans_id: '12345',
									err_code: 1,
									err_descr: 'test error'
								});
							} else if (!oldPasswd && !newPasswd) {
								observer.next({
									client_id: '123',
									client_trans_id: '12345',
									err_code: 1,
									err_descr: 'test error'
								});
							}

							else {
								observer.next({
									client_id: '123',
									client_trans_id: '12345',
									err_code: 0,
									err_descr: 'test success'
								});
							}
							observer.complete();
						});
					}
				}
			},
		  	DialogContainerService,
			AuthService,
			HttpService,
			HttpClient,
			HttpHandler,
			{
				provide: TransactionService,
				useValue: new TransactionServiceStub()
			},
			LogService,
			LotteriesService
		]
    })
    .compileComponents();

    TestBed.inject(LogService);
  }));

  	beforeEach(() => {
    	fixture = TestBed.createComponent(ChangePasswordComponent);
    	component = fixture.componentInstance;
    	fixture.detectChanges();
  	});

  	it('should create', () => {
    	expect(component).toBeTruthy();
  	});

  	it('test showDialog method', () => {
  		spyOn(component, 'showDialog').and.callThrough();
		component.showDialog();
  		expect(component.showDialog).toHaveBeenCalled();
  	});

	xit('test onConfirmOldPass method', () => {
		component.onConfirmOldPass();
		expect(component.newPass.isDisabled).toBeFalsy();
	});

	xit('test onConfirmNewPass method', () => {
		component.onConfirmNewPass();
		expect(component.newPass2.isDisabled).toBeFalsy();
	});

	it('test onConfirmRepeatPass method', () => {
		spyOn(component, 'onConfirmRepeatPass').and.callThrough();
		component.onConfirmRepeatPass();
		expect(component.onConfirmRepeatPass).toHaveBeenCalled();
	});

	it('test onDialogAction handler', () => {
		spyOn(component, 'onDialogAction').and.callThrough();

		component.onDialogAction();
		expect(component.onDialogAction).toHaveBeenCalled();

		component.form.controls['oldpass'].setValue('121212');
		component.form.controls['newpass'].setValue('131313');
		component.form.controls['newpass2'].setValue('131313');
		component.changeStatus = ChangeStatus.Initial;
		component.onDialogAction();
		expect(component.onDialogAction).toHaveBeenCalled();

		component.form.controls['oldpass'].setValue('121212');
		component.form.controls['newpass'].setValue('121212');
		component.form.controls['newpass2'].setValue('131313');
		component.changeStatus = ChangeStatus.Initial;
		component.onDialogAction();
		expect(component.onDialogAction).toHaveBeenCalled();
	});

	it('test getPassLabel getter', () => {
		expect(component.newpassLabel).toEqual('hamburger.enter_new_pass');
	});

	it('test getPassLabel getter 2', () => {
		component.form.controls['newpass'].setValue('test');
		component.form.controls['newpass'].setErrors({});
		expect(component.newpassLabel).toEqual('hamburger.incorrect_password_entered');
	});

	it('test getPassLabel getter 3', () => {
		component.form.controls['newpass'].setValue('101605');
		component.form.controls['newpass2'].setValue('102605');
		component.form.setErrors({
			newPasswordsNotSame: true
		});
		expect(component.newpassLabel).toEqual('hamburger.passwords_not_same');
	});

	it('test newpass2Label getter', () => {
		expect(component.newpass2Label).toEqual('hamburger.repeat_new_pass');
	});

	it('test onConfirmOldPass handler', () => {
		const fixture2 = TestBed.createComponent(MslInputWithEyeComponent);
		const child = fixture2.componentInstance;
		fixture2.detectChanges();
		component.newPass = child;
		spyOn(component, 'onConfirmOldPass').and.callThrough();
		component.onConfirmOldPass();
		expect(component.onConfirmOldPass).toHaveBeenCalled();
	});

	it('test onConfirmNewPass handler', () => {
		const fixture2 = TestBed.createComponent(MslInputWithEyeComponent);
		const child = fixture2.componentInstance;
		fixture2.detectChanges();
		component.newPass2 = child;
		spyOn(component, 'onConfirmNewPass').and.callThrough();
		component.onConfirmNewPass();
		expect(component.onConfirmNewPass).toHaveBeenCalled();
	});

	it('test onFocus handler', () => {
		spyOn(component, 'onFocus').and.callThrough();
		component.onFocus(new Event('onFocus'), 'test');
		expect(component.onFocus).toHaveBeenCalled();
	});

	it('test onChangePass handler', () => {
		spyOn(component, 'onChangePass').and.callThrough();
		component.onChangePass('test', 'test 2');
		expect(component.onChangePass).toHaveBeenCalled();
	});

	it('test onCancelHandler', () => {
		component.onCancelHandler();
		expect(component.changeStatus).toEqual(ChangeStatus.Initial);
	});

	it('test onClickHelp', () => {
		component.onClickHelp();
		expect(component.rulesDialogShown).toBeTruthy();
	});

	it('test onCloseRulesDialog', () => {
		component.onCloseRulesDialog();
		expect(component.rulesDialogShown).toBeFalsy();
	});
});
