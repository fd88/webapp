import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HelpComponent } from './help.component';
import { TranslateModule } from '@ngx-translate/core';
import { SafeHtmlPipe } from '@app/shared/pipes/safe-html.pipe';
import { SharedModule } from '@app/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {OneButtonCustomComponent} from "@app/shared/components/one-button-custom/one-button-custom.component";
import {CustomScrollComponent} from "@app/shared/components/custom-scroll/custom-scroll.component";
import {HttpClientModule} from "@angular/common/http";

describe('HelpComponent', () => {
  let component: HelpComponent;
  let fixture: ComponentFixture<HelpComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
		imports: [
			HttpClientModule,
			TranslateModule.forRoot(),
			SharedModule,
			BrowserAnimationsModule
		],
      	declarations: [
      		HelpComponent,
			OneButtonCustomComponent,
			CustomScrollComponent
	  	],
		providers: [
			SafeHtmlPipe,
			AppStoreService,
			HttpService
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onSpoilerClick handler', () => {
		const ev = new Event('onSpoilerClick');
		const target = document.createElement('h2');
		target.classList.add('spoiler__header');
		target.setAttribute('data-index', '0');
		component.onSpoilerClick({...ev, target});
		expect(component.spoilers[0].state).toEqual('down');
	});

	it('test onSpoilerClick handler 2', () => {
		const ev = new Event('onSpoilerClick');
		const target = document.createElement('h2');
		target.classList.add('spoiler__header');
		target.setAttribute('data-index', '0');
		component.onSpoilerClick({...ev, target});
		component.onSpoilerClick({...ev, target});
		expect(component.spoilers[0].state).toEqual('up');
	});

	it('test onCloseHelp handler', () => {
		component.onCloseHelp();
		expect(component.spoilers.every(item => item.state === 'up')).toBeTruthy();
	});

});
