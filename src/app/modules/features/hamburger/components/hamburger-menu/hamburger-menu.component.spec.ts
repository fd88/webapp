import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import {TranslateModule, TranslateService} from '@ngx-translate/core';

import { HamburgerMenuComponent } from '@app/hamburger/components/hamburger-menu/hamburger-menu.component';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { ReportsService } from '@app/core/services/report/reports.service';
import { AuthService } from '@app/core/services/auth.service';
import { HamburgerMenuService } from '@app/hamburger/services/hamburger-menu.service';
import { ChangeLogService } from '@app/sys-info/services/change-log.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { TransactionServiceStub } from '@app/core/services/transaction/transaction.service.spec';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { ChangePasswordComponent } from '@app/hamburger/components/change-password/change-password.component';
import { OneButtonCustomComponent } from '@app/shared/components/one-button-custom/one-button-custom.component';
import {FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {CloseButtonComponent} from "@app/shared/components/close-button/close-button.component";
import {SharedModule} from "@app/shared/shared.module";
import {CoreModule} from "@app/core/core.module";
import {HelpComponent} from "@app/hamburger/components/help/help.component";
import {environment} from "@app/env/environment";
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {CancelFlag} from "@app/core/services/transaction/transaction-types";

describe('HamburgerMenuComponent', () => {
	let component: HamburgerMenuComponent | any;
	let fixture: ComponentFixture<HamburgerMenuComponent>;
	let appStoreService: AppStoreService;
	let reportService: ReportsService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				BrowserAnimationsModule,
				RouterTestingModule.withRoutes([]),
				NoopAnimationsModule,
				HttpClientTestingModule,
				TranslateModule.forRoot(),
				ReactiveFormsModule,
				SharedModule,
				CoreModule
			],
			providers: [
				LogService,
				AuthService,
				LotteriesService,

				ChangeLogService,
				HttpService,
				HamburgerMenuService,
				ReportsService,
				StorageService,
				BarcodeReaderService,
				AppStoreService,
				{
					provide: TransactionService,
					useValue: new TransactionServiceStub()
				},
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				FormBuilder,
				TranslateService
			],
			declarations: [
				HamburgerMenuComponent,
				ChangePasswordComponent,
				OneButtonCustomComponent,
				CloseButtonComponent,
				HelpComponent
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		reportService = TestBed.inject(ReportsService);
	}));

	beforeEach(async() => {
		await TestBed.inject(AppStoreService);

		fixture = TestBed.createComponent(HamburgerMenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test ngOnInit lifecycle hook', () => {
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		appStoreService.isLoggedIn$$.next(true);
		component.ngOnInit();
		reportService.ready$.next(true);
		expect(component.buildVersion).toEqual(environment.version);
	});

	it('test onClickHamburgerMenuItemHandler', () => {
		spyOn(component, 'onClickHamburgerMenuItemHandler').and.callThrough();
		const ev = new MouseEvent('onClickHamburgerMenuItemHandler');
		const target = document.createElement('div');
		target.classList.add('hamburger-menu__item');
		(target as any).value = 1;
		component.onClickHamburgerMenuItemHandler({ ...ev, target });
		expect(component.onClickHamburgerMenuItemHandler).toHaveBeenCalled();
	});

	it('test onClickHamburgerMenuItemHandler', () => {
		component.onClickChangeLangHandler('ru');
		expect(appStoreService.Settings.lang).toEqual('ru');
	});

	it('test onClickAboutHandler', () => {
		spyOn(component, 'onClickAboutHandler').and.callThrough();
		component.onClickAboutHandler();
		expect(component.onClickAboutHandler).toHaveBeenCalled();
	});

	it('test onClickChangePasswordHandler', () => {
		spyOn(component, 'onClickChangePasswordHandler').and.callThrough();
		component.onClickChangePasswordHandler();
		expect(component.onClickChangePasswordHandler).toHaveBeenCalled();
	});

	it('test onClickHelpHandler', () => {
		spyOn(component, 'onClickHelpHandler').and.callThrough();
		component.onClickHelpHandler();
		expect(component.onClickHelpHandler).toHaveBeenCalled();
	});

	it('test onCloseMenuHandler', () => {
		spyOn(component, 'onCloseMenuHandler').and.callThrough();
		component.onCloseMenuHandler();
		expect(component.onCloseMenuHandler).toHaveBeenCalled();
	});

	it('test rejectCancelHandler', () => {
		spyOn(component, 'rejectCancelHandler').and.callThrough();
		component.rejectCancelHandler();
		expect(component.rejectCancelHandler).toHaveBeenCalled();
	});

	it('test onClickCancelLast', () => {
		spyOn(component, 'onClickCancelLast').and.callThrough();
		component.onClickCancelLast();
		expect(component.onClickCancelLast).toHaveBeenCalled();
	});

	it('test cancelTransaction method', () => {
		spyOn(component, 'cancelTransaction').and.callThrough();
		component.cancelTransaction({
			label: 'Cancel',
			value: CancelFlag.Manual_NoMoney
		});
		expect(component.cancelTransaction).toHaveBeenCalled();
	});

});
