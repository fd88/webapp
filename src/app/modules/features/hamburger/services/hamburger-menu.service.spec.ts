import { TestBed, waitForAsync } from '@angular/core/testing';

import { timer } from 'rxjs';

import { HamburgerMenuService } from '@app/hamburger/services/hamburger-menu.service';
import { AnimationState } from '@app/util/animations';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

xdescribe('HamburgerMenuService', () => {
	let service: HamburgerMenuService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				AppStoreService,
				HttpService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(HamburgerMenuService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('check menu animation states', () => {
		expect(service.stateExpression).toBeUndefined();
		service.switchMenu(AnimationState.collapsed);
		expect(service.stateExpression).toEqual(AnimationState.collapsed);
		service.switchMenu(AnimationState.expanded);
		expect(service.stateExpression).toEqual(AnimationState.expanded);
	});

	it('check disabling of LAST TRANSACTION CANCEL button', waitForAsync(async() => {
		// проверить деактивацию по таймеру
		expect(service.cancelButtonDisabled).toBeTruthy();
		service.activateCancelButton(2000);
		expect(service.cancelButtonDisabled).toBeFalsy();
		await timer(2000).toPromise();
		expect(service.cancelButtonDisabled).toBeTruthy();

		// проверить деактивацию через функцию
		service.activateCancelButton(2000);
		expect(service.cancelButtonDisabled).toBeFalsy();
		service.deactivateCancelButton();
		expect(service.cancelButtonDisabled).toBeTruthy();
	}));
});
