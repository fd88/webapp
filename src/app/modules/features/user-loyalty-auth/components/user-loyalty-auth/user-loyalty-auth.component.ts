import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	ViewChild
} from '@angular/core';
import {getShortPlayerPhone, GreenButtonStyle} from "@app/util/utils";
import {
	AbstractControl,
	AbstractControlOptions,
	FormBuilder,
	FormGroup,
	ValidationErrors,
	Validators
} from "@angular/forms";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {Observable, of, Subject, timer} from "rxjs";
import {MaskedInputComponent} from "@app/shared/components/masked-input/masked-input.component";
import {UserLoyaltyAuthService} from "../../services/user-loyalty-auth.service";
import {SendPinCodeResp} from "@app/core/net/http/api/models/send-pin-code";
import {catchError, concatMap, takeUntil} from "rxjs/operators";
import {IResponse} from "@app/core/net/http/api/types";
import {IError} from "@app/core/error/types";
import {GetCardUserResp} from "@app/core/net/http/api/models/get-card-user";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {GetCardInfoResp} from "@app/core/net/http/api/models/get-card-info";
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {ILoyaltyStepResult} from "../../interfaces/i-loyalty-step-result";

@Component({
  selector: 'app-user-loyalty-auth',
  templateUrl: './user-loyalty-auth.component.html',
  styleUrls: ['./user-loyalty-auth.component.scss']
})
export class UserLoyaltyAuthComponent implements OnInit, OnDestroy {
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	readonly GreenButtonStyle = GreenButtonStyle;

	/**
	 * Маска ввода тел. номера
	 */
	readonly phoneMask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

	/**
	 * Форма
	 */
	form: FormGroup;
	form2: FormGroup;

	step = 1;

	@Input() showScanVariant = true;

	private authWay = this.showScanVariant ? 'variant_scan' : 'variant_phone';

	@Input() showPhoneVariant = true;

	@Input() scannerActive = false;

	@Input() showValidMark = true;

	@Input() outerSpoilerExpanded = true;

	@Input() playerMustExists = true;

	@Input() showSubmitButtons = true;

	@Input() buttonText = 'reporting.continue';

	@Output() authResult = new EventEmitter<ILoyaltyStepResult>();

	@ViewChild('phoneNumber') phoneNumber: MaskedInputComponent;

	@ViewChild('scannerInput') scannerInput: ScannerInputComponent;

	@ViewChild('smsControl') smsControl: MslInputPinComponent;

	constructor(
		private readonly formBuilder: FormBuilder,
		readonly appStoreService: AppStoreService,
		readonly userLoyaltyAuthService: UserLoyaltyAuthService,
		private readonly barcodeReaderService: BarcodeReaderService,
		private readonly cdr: ChangeDetectorRef
	) { }

	get isScannerExpanded(): boolean {
		return this.scannerInput.expanded;
	}

	isValid(): boolean {
		if (this.step === 1) {
			return this.form.valid;
		}
		if (this.step === 2) {
			return this.form2.valid;
		}
		return true;
	}

	executeStep1(usePrevious = false): Observable<ILoyaltyStepResult> {
		this.userLoyaltyAuthService.isLoading = true;
		this.userLoyaltyAuthService.errorText = '';
		if (this.scannerInput?.expanded) {
			this.scannerInput.changeState();
		}
		if (this.authWay === 'variant_scan') {
			return this.finalHandler(this.form.controls['cardBarcode'].value);
		}
		if (!usePrevious && !this.userLoyaltyAuthService.lastEnteredPhone) {
			this.userLoyaltyAuthService.lastEnteredPhone = '380' + getShortPlayerPhone(this.form.controls['playerPhone'].value);
		}
		return this.sendSMS();
	}

	executeStep2(): Observable<ILoyaltyStepResult> {
		this.userLoyaltyAuthService.isLoading = true;
		const code = this.form2.controls['pinCode'].value;
		this.reset();
		return this.userLoyaltyAuthService.verifyPhone(code)
			.pipe(
				concatMap(() => this.userLoyaltyAuthService.getCardUser()),
				concatMap((cardUser: GetCardUserResp) => this.finalHandler(cardUser.card_num)),
				catchError(this.errorsHandler.bind(this))
			);
	}

	onSubmitForm1(): void {
		this.executeStep1().subscribe();
	}

	onSubmitForm2(): void {
		this.executeStep2().subscribe();
	}

	onVariantChange(event: Event): void {
		this.userLoyaltyAuthService.errorText = '';
		this.changeAuthVariant((event.target as HTMLInputElement).id);
	}

	goBack(): void {
		this.step = 1;
		this.userLoyaltyAuthService.errorText = '';
		timer(100)
			.subscribe(() => {
				this.form.updateValueAndValidity();
				this.cdr.detectChanges();
			});
	}

	onRepeatSMS(): void {
		this.sendSMS().subscribe();
	}

	changeCameraState(): void {
		this.scannerInput.changeState();
	}

	onTextFieldFocused(variant: string): void {
		this.userLoyaltyAuthService.errorText = '';
		this.form.controls['variant'].setValue(variant);
		this.changeAuthVariant(variant);
	}

	ngOnInit(): void {
		this.authWay = this.showScanVariant ? 'variant_scan' : 'variant_phone';
		this.form = this.formBuilder.group({
			variant: [ this.authWay, Validators.required],
			cardBarcode: [''],
			playerPhone: ['']
		},
			{ validator: this.step1Validator() } as AbstractControlOptions
		);

		this.form2 = this.formBuilder.group({
			pinCode: ['', [Validators.required, Validators.pattern(/^\d{4}$/)]]
		});

		// Обработчик изменения значения радио-кнопок
		this.form.get('variant').valueChanges.subscribe(value => {
			this.updateTextFieldsValidators(value);
		});

		if (!!this.userLoyaltyAuthService.lastEnteredPhone) {
			this.step = 2;
			timer(200)
				.subscribe(() => {
					if (this.smsControl) {
						this.smsControl.setFocus();
					}
				});
		}

		if (this.step === 1) {
			timer(100)
				.subscribe(() => {
					// Инициализация валидаторов для текстовых полей
					this.updateTextFieldsValidators(this.form.get('variant').value);
				});
			if (this.showScanVariant) {

				// при сканировании билета на форме обновить поле ввода полученным значением
				this.barcodeReaderService.barcodeSubject$$
					.pipe(
						takeUntil(this.unsubscribe$$)
					)
					.subscribe((bc) => {
						this.form.controls['cardBarcode'].setValue(bc.barcode);
					});
			} else {
				timer(200)
					.subscribe(() => {
						this.phoneNumber.setFocus();
					});
			}
		}
	}

	ngOnDestroy(): void {
		this.userLoyaltyAuthService.errorText = '';
		this.step = 1;
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

	reset(): void {
		this.form.reset();
		this.form2.reset();
		this.authWay = this.showScanVariant ? 'variant_scan' : 'variant_phone';
		this.form.controls['variant'].setValue(this.authWay);
	}

	private errorsHandler(error: IResponse | IError): Observable<ILoyaltyStepResult> {
		const errCode = +(error as IResponse).err_code || +(error as IError).code;
		const message = (error as IResponse).err_descr || (error as IError).message;
		this.userLoyaltyAuthService.errorText = errCode === 702 ? 'other.verification-error' : message;
		if (errCode !== 843) {
			timer(100)
				.subscribe(() => {
					this.form.updateValueAndValidity();
					this.cdr.detectChanges();
				});
		}
		this.reset();
		this.userLoyaltyAuthService.isLoading = false;
		return of({
			error,
			step: this.step
		});
	}

	private sendSMS(): Observable<ILoyaltyStepResult> {
		this.userLoyaltyAuthService.isLoading = true;
		this.userLoyaltyAuthService.errorText = '';
		return this.userLoyaltyAuthService.sendPinCode()
			.pipe(
				concatMap((data: SendPinCodeResp) => {
					if (+data.err_code === 0) {
						this.step = 2;
						const ans: ILoyaltyStepResult = {
							step: this.step
						};
						this.reset();
						this.userLoyaltyAuthService.isLoading = false;
						timer(200)
							.subscribe(() => {
								if (this.smsControl) {
									this.smsControl.setFocus();
								}
							});
						this.authResult.emit(ans);
						return of(ans);
					}
					return this.errorsHandler(data);
				}),
				catchError((error: IResponse | IError) => this.errorsHandler(error))
			);
	}

	private finalHandler(cardNum: string): Observable<ILoyaltyStepResult> {
		return this.userLoyaltyAuthService.getCardInfo(cardNum)
			.pipe(
				concatMap((data: GetCardInfoResp) => {
					if (+data.err_code === 0) {
						const ans: ILoyaltyStepResult = {
							data,
							step: 3,
							cardNum
						};
						this.authResult.emit(ans);
						this.reset();
						this.userLoyaltyAuthService.isLoading = false;
						this.step = 1;
						return of(ans);
					}
					return this.errorsHandler(data);
				}),
				catchError(this.errorsHandler.bind(this))
			);
	}

	private changeAuthVariant(authVar: string): void {
		this.authWay = authVar;
		this.form.updateValueAndValidity();
		if (this.authWay === 'variant_scan') {
			this.scannerInput.setFocus();
		} else {
			this.phoneNumber.setFocus();
		}
	}

	private step1Validator() {
		return (control: AbstractControl): ValidationErrors | null => {
			const cardBarcode = control.get('cardBarcode')?.value;
			const playerPhone = control.get('playerPhone')?.value;

			if (this.authWay === 'variant_scan' && cardBarcode && cardBarcode.length === 8) {
				// Вариант с использованием cardBarcode: значение длиной 8 символов
				return null; // Валидно
			} else if (this.authWay === 'variant_phone' && playerPhone) {
				// Вариант с использованием playerPhone: любое заполненное значение
				return null; // Валидно
			} else {
				return { invalidForm: true }; // Не валидно
			}
		};
	}

	// Функция для обновления валидаторов текстовых полей
	private updateTextFieldsValidators(choice: string): void {
		if (choice === 'variant_scan') {
			this.form.get('cardBarcode').setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(8)]);
			this.form.get('playerPhone').clearValidators();
		} else {
			this.form.get('cardBarcode').clearValidators();
			this.form.get('playerPhone').setValidators([Validators.required]);
		}
		this.form.get('cardBarcode').updateValueAndValidity();
		this.form.get('playerPhone').updateValueAndValidity();
	}
}
