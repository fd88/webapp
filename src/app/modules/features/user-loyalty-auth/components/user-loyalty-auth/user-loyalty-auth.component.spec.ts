import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserLoyaltyAuthComponent } from './user-loyalty-auth.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpService} from "@app/core/net/http/services/http.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateService, TranslateStore} from "@ngx-translate/core";
import {RouterTestingModule} from "@angular/router/testing";
import {of} from "rxjs";
import {UserLoyaltyAuthService} from "../../services/user-loyalty-auth.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {VerifyPhoneResp} from "@app/core/net/http/api/models/verify-phone";
import {SendPinCodeResp} from "@app/core/net/http/api/models/send-pin-code";
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";
import {CameraModule} from "../../../camera/camera.module";

describe('UserLoyaltyAuthComponent', () => {
  let component: UserLoyaltyAuthComponent;
  let fixture: ComponentFixture<UserLoyaltyAuthComponent>;
  let userLoyaltyAuthService: UserLoyaltyAuthService;
  let barcodeReaderService: jasmine.SpyObj<BarcodeReaderService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			FormsModule,
			ReactiveFormsModule,
			HttpClientTestingModule,
			SharedModule,
			RouterTestingModule,
			CameraModule
		],
      	declarations: [
			  	UserLoyaltyAuthComponent,
				ScannerInputComponent
		],
		providers: [
			HttpService,
			AppStoreService,
			TranslateService,
			TranslateStore
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoyaltyAuthComponent);
    component = fixture.componentInstance;

	  userLoyaltyAuthService = TestBed.inject(UserLoyaltyAuthService);

	  userLoyaltyAuthService.verifyPhone = (v: string) => of({
		  client_trans_id: '12345',
		  err_code: 0,
		  err_descr: ''
	  } as VerifyPhoneResp);

	  userLoyaltyAuthService.getCardUser = () => of({
		  	card_num: '11111111',
		  	user_id: 1,
		  	phone: '+380681294504',
		  	err_code: 0,
		  	err_descr: '',
		  	client_trans_id: '111'
		  });

	  userLoyaltyAuthService.sendPinCode = () => of({
		  client_trans_id: '12345',
		  err_code: 0,
		  err_descr: ''
	  } as SendPinCodeResp);

	  barcodeReaderService = TestBed.inject(BarcodeReaderService) as jasmine.SpyObj<BarcodeReaderService>;


	  fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('should initialize the form on ngOnInit', () => {
		component.ngOnInit();
		expect(component.form).toBeDefined();
		expect(component.form2).toBeDefined();
		expect(component.form.get('variant').value).toBe('variant_scan');
	});

	it('should switch camera state correctly', () => {
		expect(component.isScannerExpanded).toBeFalsy();
	});

	it('should update validators when auth variant changes', () => {
		component.form.controls['variant'].setValue('variant_scan');
		(component as any).updateTextFieldsValidators('variant_scan');
		expect(component.form.get('cardBarcode').valid).toBeFalse();

		component.form.controls['variant'].setValue('variant_phone');
		(component as any).updateTextFieldsValidators('variant_phone');
		expect(component.form.get('playerPhone').valid).toBeFalse();
	});


	it('should update validators when auth variant changes', () => {
		component.form.controls['variant'].setValue('variant_scan');
		(component as any).updateTextFieldsValidators('variant_scan');
		expect(component.form.get('cardBarcode').valid).toBeFalse();
		expect(component.form.get('playerPhone').validator).toBeNull();

		component.form.controls['variant'].setValue('variant_phone');
		(component as any).updateTextFieldsValidators('variant_phone');
		expect(component.form.get('cardBarcode').validator).toBeNull();
		expect(component.form.get('playerPhone').valid).toBeFalse();
	});

	// it('test onSMSCodeSubmit handler', () => {
	// 	spyOn(component, 'onSMSCodeSubmit').and.callThrough();
	// 	component.onSMSCodeSubmit(null);
	// 	expect(component.onSMSCodeSubmit).toHaveBeenCalled();
	// });
	//
	// it('test onSMSCodeSubmit handler 2', () => {
	// 	spyOn(component, 'onSMSCodeSubmit').and.callThrough();
	// 	userLoyaltyAuthService.getCardUser = () => of(null);
	// 	component.onSMSCodeSubmit(null);
	// 	expect(component.onSMSCodeSubmit).toHaveBeenCalled();
	// });
	//
	// it('test onSubmitForm handler', () => {
	// 	spyOn(component, 'onSubmitForm').and.callThrough();
	// 	component.onSubmitForm();
	// 	expect(component.onSubmitForm).toHaveBeenCalled();
	// });

	xit('test onSubmitForm handler 2', () => {
		(component as any).authWay === 'variant_phone';
		spyOn(component, 'onSubmitForm').and.callThrough();
		component.onSubmitForm();
		expect(component.onSubmitForm).toHaveBeenCalled();
	});

	it('test onVariantChange handler', () => {
		spyOn(component, 'onVariantChange').and.callThrough();
		component.onVariantChange({
			target: {
				id: 'test'
			}
		} as any);
		expect(component.onVariantChange).toHaveBeenCalled();
	});

	it('test onGoBack handler', () => {
		spyOn(component, 'onGoBack').and.callThrough();
		component.onGoBack();
		expect(component.onGoBack).toHaveBeenCalled();
	});

	it('test onRepeatSMS handler', () => {
		spyOn(component, 'onRepeatSMS').and.callThrough();
		component.onRepeatSMS();
		expect(component.onRepeatSMS).toHaveBeenCalled();
	});

	it('test onInputChangeHandler handler', () => {
		component.onInputChangeHandler('12321');
		expect(component.appStoreService.cardNum).toBe('12321');
	});

	it('test errorsHandler handler', () => {
		(component as any).errorsHandler({
			err_code: 2345,
			err_descr: 'Тестовая ошибка',
			client_trans_id: '12345'
		});
		expect(userLoyaltyAuthService.step).toBe(1);
	});


});
