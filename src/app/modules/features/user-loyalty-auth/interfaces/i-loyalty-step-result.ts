import {GetCardInfoResp} from "@app/core/net/http/api/models/get-card-info";
import {IResponse} from "@app/core/net/http/api/types";
import {IError} from "@app/core/error/types";

export interface ILoyaltyStepResult {
	error?: IResponse | IError;
	step: number;
	cardNum?: string;
	data?: GetCardInfoResp;
}
