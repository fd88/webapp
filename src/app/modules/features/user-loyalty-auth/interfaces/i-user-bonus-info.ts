export interface IUserBonusInfo {
	/**
	 * Номер карты лояльности
	 */
	card_num: string;

	/**
	 * Общая сумма накопленных на карте бонусов (фортуналей) в копейках
	 */
	total_bonus_sum: number;
}
