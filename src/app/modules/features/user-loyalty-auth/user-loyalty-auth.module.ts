import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserLoyaltyAuthComponent} from "./components/user-loyalty-auth/user-loyalty-auth.component";
import {SharedModule} from "@app/shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CameraModule} from "../camera/camera.module";

@NgModule({
	declarations: [
		UserLoyaltyAuthComponent
	],
	exports: [
		UserLoyaltyAuthComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		ReactiveFormsModule,
		FormsModule,
		CameraModule
	]
})
export class UserLoyaltyAuthModule { }
