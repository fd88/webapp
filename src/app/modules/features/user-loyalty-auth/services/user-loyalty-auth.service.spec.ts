import {TestBed} from '@angular/core/testing';
import {UserLoyaltyAuthService} from './user-loyalty-auth.service';
import {HttpService} from "@app/core/net/http/services/http.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {HttpClientModule} from "@angular/common/http";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {Operator} from "@app/core/services/store/operator";

describe('UserLoyaltyAuthService', () => {
  let service: UserLoyaltyAuthService;
  let appStoreService: AppStoreService;
  let csConfig;

  beforeEach(async () => {
    TestBed.configureTestingModule({
		imports: [
			HttpClientModule
		],
		providers: [
			HttpService,
			AppStoreService
		]
	});
    service = TestBed.inject(UserLoyaltyAuthService);
	service.lastEnteredPhone = '+380681234567';

	  TestBed.inject(LogService);
	  appStoreService = TestBed.inject(AppStoreService);
	  csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);
	const operator = new Operator('testInstantUser', `${Date.now()}`, '123456', 1);
	appStoreService.operator.next(operator);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

	it('test getBonusBalance method', () => {
		spyOn(service, 'getBonusBalance').and.callThrough();
		service.getBonusBalance('12345678');
		expect(service.getBonusBalance).toHaveBeenCalled();
	});

	it('test sendPinCode method', () => {
		spyOn(service, 'sendPinCode').and.callThrough();
		service.sendPinCode();
		expect(service.sendPinCode).toHaveBeenCalled();
	});

	it('test verifyPhone method', () => {
		spyOn(service, 'verifyPhone').and.callThrough();
		service.verifyPhone('1111');
		expect(service.verifyPhone).toHaveBeenCalled();
	});

	it('test assignCard method', () => {
		spyOn(service, 'assignCard').and.callThrough();
		service.assignCard('1234578');
		expect(service.assignCard).toHaveBeenCalled();
	});

	it('test getCardUser method', () => {
		spyOn(service, 'getCardUser').and.callThrough();
		service.getCardUser();
		expect(service.getCardUser).toHaveBeenCalled();
	});

	it('test getCardInfo method', () => {
		spyOn(service, 'getCardInfo').and.callThrough();
		service.getCardInfo('11111111');
		expect(service.getCardInfo).toHaveBeenCalled();
	});

	it('test getAvailableBonus method', () => {
		spyOn(service, 'getAvailableBonus').and.callThrough();
		service.getAvailableBonus('11111111', 500, 3, LotteryGameCode.Zabava);
		expect(service.getAvailableBonus).toHaveBeenCalled();
	});

	it('test getWithdrawBonus method', () => {
		spyOn(service, 'getWithdrawBonus').and.callThrough();
		service.getWithdrawBonus('12345678');
		expect(service.getWithdrawBonus).toHaveBeenCalled();
	});

	it('test withdrawBonus method', () => {
		spyOn(service, 'withdrawBonus').and.callThrough();
		service.withdrawBonus('11111111', 500);
		expect(service.withdrawBonus).toHaveBeenCalled();
	});

	it('test getPayedTicketInfo method', () => {
		spyOn(service, 'getPayedTicketInfo').and.callThrough();
		service.getPayedTicketInfo('11111111', 500, LotteryGameCode.Zabava);
		expect(service.getPayedTicketInfo).toHaveBeenCalled();
	});

	xit('test winPayToCard method', () => {
		spyOn(service, 'winPayToCard').and.callThrough();
		service.winPayToCard('06620123456789123456789', '500', '11111111', 3);
		expect(service.winPayToCard).toHaveBeenCalled();
	});
});
