import {EventEmitter, Injectable, Output} from '@angular/core';
import {BehaviorSubject, from, Observable, of, Subject} from "rxjs";
import {GetBonusBalanceReq, GetBonusBalanceResp} from "@app/core/net/http/api/models/get-bonus-balance";
import {HttpService} from "@app/core/net/http/services/http.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {SendPinCodeReq, SendPinCodeResp} from "@app/core/net/http/api/models/send-pin-code";
import {VerifyPhoneReq, VerifyPhoneResp} from "@app/core/net/http/api/models/verify-phone";
import {GetUserByPhoneReq, GetUserByPhoneResp} from "@app/core/net/http/api/models/get-user-by-phone";
import {AssignCardReq, AssignCardResp} from "@app/core/net/http/api/models/assign-card";
import {GetCardUserReq, GetCardUserResp} from "@app/core/net/http/api/models/get-card-user";
import {GetCardInfoReq, GetCardInfoResp} from "@app/core/net/http/api/models/get-card-info";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {GetAvailableBonusReq, GetAvailableBonusResp} from "@app/core/net/http/api/models/get-available-bonus";
import {AuthPhoneReq, AuthPhoneResp} from "@app/core/net/http/api/models/auth-phone";
import {GetWithdrawBonusReq, GetWithdrawBonusResp} from "@app/core/net/http/api/models/get-withdraw-bonus";
import {WithdrawBonusReq, WithdrawBonusResp} from "@app/core/net/http/api/models/withdraw-bonus";
import {WinPayToCardReq, WinPayToCardResp} from "@app/core/net/http/api/models/win-pay-to-card";
import {HttpHeaders} from "@angular/common/http";
import {GetPayedTicketInfoReq, GetPayedTicketInfoResp} from "@app/core/net/http/api/models/get-payed-ticket-info";

@Injectable({
  providedIn: 'root'
})
export class UserLoyaltyAuthService {

	lastEnteredPhone: string = '';

	isLoading = false;

	errorText = '';

	constructor(
		private readonly appStoreService: AppStoreService,
		private readonly httpService: HttpService
	) { }

	getBonusBalance(cardNum: string): Observable<GetBonusBalanceResp> {
		const getBonusBalanceReq = new GetBonusBalanceReq(this.appStoreService, cardNum);
		return from(this.httpService.sendApi(getBonusBalanceReq)) as Observable<GetBonusBalanceResp>;
	}

	sendPinCode(): Observable<SendPinCodeResp> {
		const sendPinCodeReq = new SendPinCodeReq(this.appStoreService, this.lastEnteredPhone);
		return from(this.httpService.sendApi(sendPinCodeReq)) as Observable<SendPinCodeResp>;
	}

	verifyPhone(pin: string): Observable<VerifyPhoneResp> {
		const verifyPhoneReq = new VerifyPhoneReq(this.appStoreService, this.lastEnteredPhone, pin);
		return from(this.httpService.sendApi(verifyPhoneReq)) as Observable<VerifyPhoneResp>;
	}

	assignCard(cardNum: string): Observable<AssignCardResp> {
		const assignCardReq = new AssignCardReq(this.appStoreService, cardNum, this.lastEnteredPhone);
		assignCardReq.headers = new HttpHeaders({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		return from(this.httpService.sendApi(assignCardReq)) as unknown as Observable<AssignCardResp>;
	}

	getCardUser(): Observable<GetCardUserResp> {
		const getCardUserReq = new GetCardUserReq(this.appStoreService, this.lastEnteredPhone);
		return from(this.httpService.sendApi(getCardUserReq)) as Observable<GetCardUserResp>;
	}

	getCardInfo(cardNum: string): Observable<GetCardInfoResp> {
		const getCardInfoReq = new GetCardInfoReq(this.appStoreService, cardNum);
		getCardInfoReq.headers = new HttpHeaders({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		return from(this.httpService.sendApi(getCardInfoReq)) as Observable<GetCardInfoResp>;
	}

	getAvailableBonus(
		cardNum: string,
		totalBetSum: number,
		ticketCount: number,
		lotteryCode: LotteryGameCode
	): Observable<GetAvailableBonusResp> {
		const getAvailableBonusReq = new GetAvailableBonusReq(
			this.appStoreService,
			cardNum,
			totalBetSum * 100,
			ticketCount,
			lotteryCode
		);
		getAvailableBonusReq.headers = new HttpHeaders({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		return from(this.httpService.sendApi(getAvailableBonusReq)) as Observable<GetAvailableBonusResp>;
	}

	getWithdrawBonus(cardNum: string): Observable<GetWithdrawBonusResp> {
		const getWithdrawBonusReq = new GetWithdrawBonusReq(this.appStoreService, cardNum);
		getWithdrawBonusReq.headers = new HttpHeaders({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		return from(this.httpService.sendApi(getWithdrawBonusReq)) as Observable<GetWithdrawBonusResp>;
	}

	withdrawBonus(cardNum: string, bonusWithdrawSum: number): Observable<WithdrawBonusResp> {
		const withdrawBonusReq = new WithdrawBonusReq(this.appStoreService, cardNum, bonusWithdrawSum);
		withdrawBonusReq.headers = new HttpHeaders({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		return from(this.httpService.sendApi(withdrawBonusReq)) as Observable<WithdrawBonusResp>;
	}

	getPayedTicketInfo(cardNumber: string, winSum: number, lotteryCode: LotteryGameCode): Observable<GetPayedTicketInfoResp> {
		const getPayedTicketInfoReq = new GetPayedTicketInfoReq(this.appStoreService, cardNumber, winSum * 100, lotteryCode);
		getPayedTicketInfoReq.headers = new HttpHeaders({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		return from(this.httpService.sendApi(getPayedTicketInfoReq)) as Observable<GetPayedTicketInfoResp>;
	}

	winPayToCard(barcode: string, winSum: string, cardNum: string, gameCode: number): Observable<WinPayToCardResp> {
		const winPayToCardReq = new WinPayToCardReq(this.appStoreService, barcode, winSum, cardNum, gameCode);
		return from(this.httpService.sendApi(winPayToCardReq)) as Observable<WinPayToCardResp>;
	}
}
