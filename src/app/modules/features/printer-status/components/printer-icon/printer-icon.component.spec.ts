import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PrinterIconComponent} from './printer-icon.component';
import {CoreModule} from "@app/core/core.module";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {TopBarModule} from "@app/top-bar/top-bar.module";
import {TopBarService} from "@app/top-bar/services/top-bar.service";
import {PrinterState} from "@app/core/net/ws/api/models/print/print-models";
import {HttpClientModule} from "@angular/common/http";
import {StatusIconType} from "@app/top-bar/enums/status-icon-type.enum";

describe('PrinterIconComponent', () => {
  let component: PrinterIconComponent;
  let fixture: ComponentFixture<PrinterIconComponent>;
  let printService: PrintService;
  let topBarService: TopBarService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			TopBarModule,
			HttpClientModule
		],
      	declarations: [ PrinterIconComponent ],
		providers: [
			PrintService,
			TopBarService
		]
    })
    .compileComponents();

	printService = TestBed.inject(PrintService);
	topBarService = TestBed.inject(TopBarService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrinterIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test ngOnInit lifecycle hook', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		topBarService.statusPanelItem$$.next({
			iconType: StatusIconType.Printer,
			statusText: 'Не готов',
			iconTarget: null
		});
		component.ngOnInit();
		printService.statusEvent$$.next(PrinterState.Disconnected);
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test ngOnInit lifecycle hook 2', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		topBarService.statusPanelItem$$.next({
			iconType: StatusIconType.Printer,
			statusText: 'Не готов',
			iconTarget: null
		});
		printService.getStatus = () => PrinterState.OnLine;
		component.ngOnInit();
		printService.statusEvent$$.next(PrinterState.OnLine);
		expect(component.ngOnInit).toHaveBeenCalled();
	});

	it('test ngOnInit lifecycle hook 3', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		topBarService.statusPanelItem$$.next({
			iconType: StatusIconType.Printer,
			statusText: 'Не готов',
			iconTarget: null
		});
		printService.getStatus = () => PrinterState.OffLine;
		component.ngOnInit();
		printService.statusEvent$$.next(PrinterState.OnLine);
		expect(component.ngOnInit).toHaveBeenCalled();
	});
});
