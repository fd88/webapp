/**
 * Модель одного дня в месячном списке.
 */
export interface IDayItem {

	/**
	 * Отображаемый номер дня.
	 */
	label: number;

	/**
	 * Дата в формате "2019-06-11T12:25:52.022Z".
	 */
	date?: string;

	/**
	 * День из текущего месяца.
	 */
	activeMonth: boolean;

	/**
	 * Текущий ден.
	 */
	currentDate: boolean;

	/**
	 * Выбранный день.
	 */
	selectedDate: boolean;

	/**
	 * Запрещенный к выбору день.
	 */
	disabledDay: boolean;
}
