import { CalendarTheme } from '../enums/calendar-theme.enum';

/**
 * Модель конфигурации компонента ввода данных.
 */
export interface ICalendarConfiguration {

	/**
	 * Идентификатор конфигурации (timestamp).
	 */
	id: number;

	/**
	 * Начальная дата.
	 */
	initDate: Date;

	/**
	 * Выбранная дата. Подсвечивает на календаре эту дату как выбранную.
	 */
	selectedDate?: Date;

	/**
	 * Цветовая темау календаря.
	 */
	calendarTheme: CalendarTheme;

	/**
	 * Элемент, относительно которого будет открыт компонент ввода даты.
	 */
	targetElement: HTMLElement;

	/**
	 * Сдвиг сверху.
	 */
	top: number;

	/**
	 * Сдвиг слева.
	 */
	left: number;

	/**
	 * Показывать или нет оверлей полупрзрачный фон под календарем.
	 */
	showOverlay: boolean;

	/**
	 * Отобразить титульную кнопку с заданным текстом.
	 * Если текст пустой, то кнопка будет скрыта.
	 */
	showTitleText: string;

	/**
	 * Отключить даты в окне выбора, если они позднее текущей даты.
	 */
	disableDatesLaterThanCurrent: boolean;

}
