import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendarDateSelectorComponent} from './calendar-date-selector.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {startOfDay} from "@app/util/utils";
import {CalendarTheme} from "@app/calendar/enums/calendar-theme.enum";
import {CalendarService} from "@app/calendar/services/calendar.service";

describe('CalendarDateSelectorComponent', () => {
  let component: CalendarDateSelectorComponent;
  let fixture: ComponentFixture<CalendarDateSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
	  imports: [
		  TranslateModule.forRoot()
	  ],
      declarations: [ CalendarDateSelectorComponent ],
	  providers: [
		  TranslateService
	  ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarDateSelectorComponent);
    component = fixture.componentInstance;
	component.calendarService = TestBed.inject(CalendarService);
	component.calendarService.calendarConfiguration$$.next({
		id: Date.now(),
		initDate: startOfDay(),
		selectedDate: undefined,
		calendarTheme: CalendarTheme.Green,
		targetElement: null,
		top: 0,
		left: 0,
		showOverlay: true,
		showTitleText: 'test',
		disableDatesLaterThanCurrent: true
	});
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickMonthHandler', () => {
		spyOn(component, 'onClickMonthHandler').and.callThrough();
		const ev = new MouseEvent('onClickMonthHandler');
		ev.stopPropagation = () => {};
		component.onClickMonthHandler(ev);
		expect(component.onClickMonthHandler).toHaveBeenCalled();
	});

	it('test onClickMonthHandler 2', () => {
		spyOn(component, 'onClickMonthHandler').and.callThrough();
		const ev = new MouseEvent('onClickMonthHandler');
		const target = document.createElement('div');
		target.setAttribute('direction', '1');
		ev.stopPropagation = () => {};
		component.onClickMonthHandler({...ev, target});
		expect(component.onClickMonthHandler).toHaveBeenCalled();
	});

	it('test onClickDayHandler', () => {
		spyOn(component, 'onClickDayHandler').and.callThrough();
		const ev = new MouseEvent('onClickDayHandler');
		ev.stopPropagation = () => {};
		component.onClickDayHandler(ev);
		expect(component.onClickDayHandler).toHaveBeenCalled();
	});

	it('test onClickDayHandler 2', () => {
		spyOn(component, 'onClickDayHandler').and.callThrough();
		const ev = new MouseEvent('onClickDayHandler');
		const target = document.createElement('div');
		target.setAttribute('date', Date.toString());
		ev.stopPropagation = () => {};
		component.onClickDayHandler({...ev, target});
		expect(component.onClickDayHandler).toHaveBeenCalled();
	});

	it('test onClickDayHandler 3', () => {
		spyOn(component, 'onClickDayHandler').and.callThrough();
		const ev = new MouseEvent('onClickDayHandler');
		const target = document.createElement('div');
		target.setAttribute('date', '2019-06-11T12:25:52.022Z');
		ev.stopPropagation = () => {};
		component.days = [{
			label: 11,
			date: '2019-06-11T12:25:52.022Z',
			activeMonth: false,
			currentDate: false,
			selectedDate: false,
			disabledDay: false
		}];
		component.onClickDayHandler({...ev, target});
		expect(component.onClickDayHandler).toHaveBeenCalled();
	});

	it('test onClickSelectHandler', () => {
		spyOn(component, 'onClickSelectHandler').and.callThrough();
		component.onClickSelectHandler();
		expect(component.onClickSelectHandler).toHaveBeenCalled();
	});
});
