import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendarDateInputComponent} from './calendar-date-input.component';
import {TranslateModule} from "@ngx-translate/core";
import {CalendarService} from "@app/calendar/services/calendar.service";
import {startOfDay} from "@app/util/utils";
import {CalendarTheme} from "@app/calendar/enums/calendar-theme.enum";
import {CalendarDateInputDirective} from "@app/calendar/directives/calendar-date-input.directive";

describe('CalendarDateInputComponent', () => {
  let component: CalendarDateInputComponent;
  let fixture: ComponentFixture<CalendarDateInputComponent>;
  let calendarService: CalendarService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot()
		],
      declarations: [
		  CalendarDateInputComponent,
		  CalendarDateInputDirective
	  ],
		providers: [
			CalendarService
		]
    })
    .compileComponents();
	calendarService = TestBed.inject(CalendarService);
	calendarService.calendarConfiguration$$.next({
		id: Date.now(),
		initDate: startOfDay(),
		selectedDate: new Date(),
		calendarTheme: CalendarTheme.Green,
		targetElement: undefined,
		top: 0,
		left: 0,
		showOverlay: true,
		showTitleText: 'test',
		disableDatesLaterThanCurrent: true
	});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarDateInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onDateChangedHandler', () => {
		spyOn(component, 'onDateChangedHandler').and.callThrough();
		component.onDateChangedHandler(new Date());
		expect(component.onDateChangedHandler).toHaveBeenCalled();
	});

	it('test onClickClearHandler', () => {
		component.onClickClearHandler();
		expect(component.value).toBeUndefined();
	});
});
