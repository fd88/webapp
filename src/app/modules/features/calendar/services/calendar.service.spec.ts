import { TestBed } from '@angular/core/testing';

import { CalendarService } from './calendar.service';
import {startOfDay} from "@app/util/utils";
import {CalendarTheme} from "@app/calendar/enums/calendar-theme.enum";
import {TranslateModule, TranslateService} from "@ngx-translate/core";

const CALENDAR_DATA = {
	id: Date.now(),
	initDate: startOfDay(),
	selectedDate: undefined,
	calendarTheme: CalendarTheme.Green,
	targetElement: null,
	top: 0,
	left: 0,
	showOverlay: true,
	showTitleText: 'test',
	disableDatesLaterThanCurrent: true
};

describe('CalendarService', () => {
  let service: CalendarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot()
		],
		providers: [
			TranslateService
		]
	});
    service = TestBed.inject(CalendarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

	it('test showCalendar', () => {
		spyOn(service, 'showCalendar').and.callThrough();
		service.showCalendar(CALENDAR_DATA);
		service.showCalendar(CALENDAR_DATA);
		expect(service.showCalendar).toHaveBeenCalled();
	});

	it('test showCalendar 2', () => {
		spyOn(service, 'showCalendar').and.callThrough();
		service.showCalendar(CALENDAR_DATA);
		document.body.click();
		expect(service.showCalendar).toHaveBeenCalled();
	});
});
