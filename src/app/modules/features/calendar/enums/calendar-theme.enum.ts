/**
 * Цветовая схема календаря.
 */
export enum CalendarTheme {
	Green	= 'green',
	Yellow	= 'yellow'
}
