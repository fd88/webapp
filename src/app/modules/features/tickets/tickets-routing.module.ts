import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {URL_BONUS_PAYMENT, URL_CHECK, URL_PAYMENT, URL_PAYMENT_FINISH} from '@app/util/route-utils';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { TicketCheckComponent } from '@app/tickets/components/ticket-check/ticket-check.component';
import { TicketPaymentComponent } from '@app/tickets/components/ticket-payment/ticket-payment.component';
import { TicketsComponent } from '@app/tickets/components/tickets.component';
import { TicketBonusPaymentComponent } from '@app/tickets/components/ticket-bonus-payment/ticket-bonus-payment.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';
import {
	TicketLoyaltyPaymentComponent
} from "@app/tickets/components/ticket-loyalty-payment/ticket-loyalty-payment.component";

const routes: Routes = [{
	path: '',
	component: TicketsComponent,
	canActivate: [
		AuthGuard
	],
	canDeactivate: [
		NavigationGuard
	],
	children: [
		{
			path: URL_CHECK,
			component: TicketCheckComponent,
			canActivate: [
				AuthGuard
			],
			canDeactivate: [
				NavigationGuard
			]
		},
		{
			path: URL_PAYMENT,
			component: TicketPaymentComponent,
			canActivate: [
				AuthGuard
			],
			canDeactivate: [
				NavigationGuard
			]
		},
		{
			path: URL_PAYMENT_FINISH,
			component: TicketLoyaltyPaymentComponent,
			canActivate: [
				AuthGuard
			],
			canDeactivate: [
				NavigationGuard
			]
		},
		{
			path: URL_BONUS_PAYMENT,
			component: TicketBonusPaymentComponent,
			canActivate: [
				AuthGuard
			],
			canDeactivate: [
				NavigationGuard
			]
		}
	]
}];

/**
 * Модуль маршрутизации для функционала проверки, отмены и выплаты.
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	]
})
export class TicketsRoutingModule {}
