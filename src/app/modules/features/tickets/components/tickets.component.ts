import { Component, OnDestroy } from '@angular/core';
import { AppStoreService } from '@app/core/services/store/app-store.service';

/**
 * Компонент-контейнер для модуля работы с билетами.
 */
@Component({
	template: `<router-outlet></router-outlet>`
})
export class TicketsComponent implements OnDestroy {

	/**
	 * Конструктор компонента.
	 * @param appStoreService Сервис хранилища приложения.
	 */
	constructor(private readonly appStoreService: AppStoreService) {
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.appStoreService.mobileKeyboardShown = false;
	}

}
