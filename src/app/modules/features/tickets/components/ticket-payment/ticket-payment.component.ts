import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { TicketsService } from '@app/tickets/services/tickets.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { AppType } from '@app/core/services/store/settings';
import { SmsCheckService } from '@app/core/services/sms-check.service';
import { SendAuthCodeResp } from '@app/core/net/http/api/models/send-auth-code';
import { IResponse } from '@app/core/net/http/api/types';
import { IError, NetError } from '@app/core/error/types';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { MslInputPinComponent } from '@app/shared/components/msl-input-pin/msl-input-pin.component';
import { from, timer } from 'rxjs';
import { GetDocumentResp } from '@app/core/net/http/api/models/get-document';
import { Template } from '@app/tickets-print/parser/template';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { ResponseCacheService } from '@app/core/services/response-cache.service';
import { concatMap } from 'rxjs/internal/operators';
import { PrintData } from '@app/core/net/ws/api/models/print/print-models';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { HttpService } from '@app/core/net/http/services/http.service';

/**
 * Шаг выплаты
 */
enum PayStep {
	/**
	 * Шаг 1
	 */
	Step1 = 1,
	/**
	 * Шаг 2
	 */
	Step2 = 2
}

/**
 * Компонент, отрисовывающий состояние билета во время проверки на выигрышность.
 * Билет может быть:
 * - выигрышный
 * - невыигрышный
 * - выигрышный, но уже оплаченный
 */
@Component({
	selector: 'app-ticket-payment',
	templateUrl: './ticket-payment.component.html',
	styleUrls: ['./ticket-payment.component.scss']
})
export class TicketPaymentComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Тип приложения
	 */
	readonly AppType = AppType;

	/**
	 * Шаг выплаты выигрыша
	 */
	readonly PayStep = PayStep;

	/**
	 * Шаг выплаты выигрыша
	 */
	payStep = PayStep.Step1;

	/**
	 * Признак того, что пин-код в фокусе
	 */
	pinFocused = false;

	/**
	 * Поле ввода СМС
	 */
	@ViewChild('smsControl') smsControl: MslInputPinComponent;

	/**
	 * Недоступна ли кнопка выплаты?
	 */
	get isPayBtnDisabled(): boolean {

		if ((this.payStep === PayStep.Step1) && this.ticketsService.winCheck.sms_required) {
			return true;
		}

		if (this.payStep === PayStep.Step2) {
			return this.ticketsService.isRequestButtonDisabled
				|| this.appStoreService.smsCodeError || !this.smsControl || this.smsControl.value.length !== 4;
		}

		return false;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TicketsService} ticketsService Сервис работы с билетами.
	 * @param {AppStoreService} appStoreService Сервис работы с хранилищем приложения.
	 * @param {SmsCheckService} smsCheckService Сервис работы с СМС.
	 * @param {DialogContainerService} dialogInfoService Сервис работы с диалоговыми окнами.
	 * @param responseCacheService Сервис кэширования ответов.
	 * @param printService Сервис печати.
	 * @param httpService Сервис работы с HTTP-запросами и ответами.
	 */
	constructor(
		readonly ticketsService: TicketsService,
		readonly appStoreService: AppStoreService,
		private readonly smsCheckService: SmsCheckService,
		private readonly dialogInfoService: DialogContainerService,
		private readonly responseCacheService: ResponseCacheService,
		private readonly printService: PrintService,
		private readonly httpService: HttpService
	) {}

	/**
	 * Обработчик клика по документу.
	 * @param event Передаваемое событие.
	 */
	@HostListener('document:click', ['$event'])
	documentClick(event: MouseEvent): void {
		const cList = (event.target as Element).classList;
		this.pinFocused = cList.contains('pin__input') || cList.contains('pin__clear');
	}

	/**
	 * Обработчик нажатия кнопки "Отмена".
	 */
	onClickCancelHandler(): void {
		Logger.Log.i('TicketPaymentComponent', `onClickCancelHandler -> "Cancel" button clicked`)
			.console();

		this.ticketsService.goToCheckPage();
	}

	/**
	 * Обработчик нажатия кнопки "Повторить СМС".
	 */
	onRepeatSMS(): void {
		this.sendSMS();
	}

	/**
	 * Обработчик нажатия кнопки "Выплата".
	 */
	onClickWinPayHandler(): void {
		this.winPayAction();
	}

	/**
	 * Обработчик изменения поля ввода пин-кода.
	 */
	onPinCodeChange(): void {
		this.appStoreService.smsCodeError = false;
	}

	/**
	 * Обработчик отправки формы
	 */
	onSMSCodeSubmit(): void {
		if (!this.isPayBtnDisabled) {
			this.winPayAction();
		}
	}

	/**
	 * Обработчик фокуса на поле ввода пин-кода.
	 */
	onPinFocused(): void {
		this.pinFocused = true;
	}

	/**
	 * Обработчик клика по кнопке "Печать".
	 */
	onClickPrintHandler(): void {
		if (this.printService.isReady()) {
			const getDocSub = this.ticketsService.getDocument()
				.pipe(
					concatMap((getDocResp: GetDocumentResp) => {
						const tpl = new Template(this.appStoreService, LotteryGameCode.Undefined, this.responseCacheService, this.httpService);
						const fakeTicket = {
							id: '1',
							bet_sum: '0',
							description: ''
						};

						return from(tpl.formatTicket({
							client_trans_id: getDocResp.client_trans_id,
							err_code: getDocResp.err_code,
							err_descr: getDocResp.err_descr,
							ticket_templ_url: getDocResp.doc.doc_templ_url,
							ticket: [fakeTicket]
						}, fakeTicket, {
							url: getDocResp.doc.doc_templ_url,
							path: `${Math.round(Math.random() * 100000)}-${Date.now()}-${getDocResp.doc.doc_templ_url}`
						}));
					}),
					concatMap((printData: Array<PrintData>) => {
						this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.doc_printing_wait_info');

						return from(this.printService.printDocument(printData));
					})
				)
				.subscribe({
					next: (docID: string) => {
						console.log('docID =', docID);
						this.ticketsService.goToCheckPage();
						this.dialogInfoService.hideAll();
						getDocSub.unsubscribe();
					},
					error: (err: IError) => {
						console.log('err =', err);
						this.dialogInfoService.showOneButtonError(err as NetError, {
							click: this.ticketsService.goToCheckPage.bind(this),
							text: 'dialog.dialog_button_continue'
						});
					}
				});
		} else {
			this.dialogInfoService.showOneButtonInfo('dialog.attention', 'dialog.printer_not_ready_info', {
				text: 'dialog.dialog_button_continue'
			});
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.appStoreService.smsCodeError = false;
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.appStoreService.mobileKeyboardShown = false;
	}

	/**
	 * Функция отправки запроса на получение СМС.
	 * @private
	 */
	private sendSMS(): void {
		this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.sending_sms');
		this.smsCheckService.sendAuthCode(this.ticketsService.winCheck.player_phone)
			.then(this.afterSendAuthCode.bind(this))
			.catch(this.errorSMSHandler.bind(this));
	}

	/**
	 * Функция выплаты выигрыша.
	 * @private
	 */
	private winPay(): void {
		Logger.Log.i('TicketPaymentComponent', `onClickWinPayHandler -> "Payment" button clicked`)
			.console();

		this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.win_payment');

		this.ticketsService.isRequestButtonDisabled = true;

		const smsValue = !!this.smsControl ? this.smsControl.value : null;

		if (this.ticketsService.winCheck.isPaper) {
			this.ticketsService.ticketWinPayPaper(smsValue);
		} else {
			this.ticketsService.ticketWinPay(smsValue);
		}
	}

	/**
	 * Обработчик ответа от сервера после запроса на отправку СМС.
	 * @param sendAuthCodeResp Ответ от сервера.
	 * @private
	 */
	private afterSendAuthCode(sendAuthCodeResp: SendAuthCodeResp): void {
		if (sendAuthCodeResp.err_code === 0) {
			this.payStep = PayStep.Step2;
			this.dialogInfoService.hideAll();
			const tmr = timer(200)
				.subscribe(() => {
					this.smsControl.setFocus();
					tmr.unsubscribe();
				});
		} else {
			this.errorSMSHandler(sendAuthCodeResp);
		}
	}

	/**
	 * Обработчик ошибки при отправке СМС.
	 * @param errResp Ответ от сервера.
	 * @private
	 */
	private errorSMSHandler(errResp: IResponse | IError): void {
		let msg: DialogError;
		if ((errResp as IResponse).err_code) {
			const resp = errResp as IResponse;
			msg = { code: resp.err_code, message: 'lottery.error_sending_sms', messageDetails: resp.err_descr };
		} else {
			msg = (errResp instanceof NetError)
				? { code: errResp.code, message: errResp.message, messageDetails: errResp.messageDetails }
				: new DialogError(ErrorCode.TicketRegister, 'lottery.error_sending_sms');
		}

		this.dialogInfoService.showOneButtonError(msg, {
			click: () => {},
			text: 'dialog.dialog_button_continue'
		});
	}

	/**
	 * Функция выплаты выигрыша с бонусом или отправки СМС.
	 * @private
	 */
	private winPayAction(): void {
		if (this.ticketsService.winCheck.sms_required && this.payStep === PayStep.Step1) {
			this.sendSMS();
		} else {
			this.winPay();
		}
	}
}
