import { Component, OnInit, ViewChild } from '@angular/core';
import { TicketsService } from '@app/tickets/services/tickets.service';
import { MslInputPinComponent } from '@app/shared/components/msl-input-pin/msl-input-pin.component';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { SendAuthCodeResp } from '@app/core/net/http/api/models/send-auth-code';
import { timer } from 'rxjs';
import { IResponse } from '@app/core/net/http/api/types';
import { IError, NetError } from '@app/core/error/types';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { SmsCheckService } from '@app/core/services/sms-check.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';

/**
 * Шаг бонусной выплаты
 */
enum PayStep {
	/**
	 * Шаг 1
	 */
	Step1 = 1,
	/**
	 * Шаг 2
	 */
	Step2 = 2
}

/**
 * Компонент выплаты бонуса.
 */
@Component({
	selector: 'app-ticket-bonus-payment',
	templateUrl: './ticket-bonus-payment.component.html',
	styleUrls: ['./ticket-bonus-payment.component.scss']
})
export class TicketBonusPaymentComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Шаги выплаты бонуса
	 */
	readonly PayStep = PayStep;

	/**
	 * Текущий шаг выплаты бонуса
	 */
	payStep = PayStep.Step1;

	/**
	 * Признак фокуса на поле ввода СМС
	 */
	pinFocused = false;

	/**
	 * Поле ввода СМС
	 */
	@ViewChild('smsControl') smsControl: MslInputPinComponent;

	/**
	 * Недоступна ли кнопка выплаты?
	 */
	get isPayBtnDisabled(): boolean {

		if (this.payStep === PayStep.Step2) {
			return this.ticketsService.isRequestButtonDisabled
				|| this.appStoreService.smsCodeError || !this.smsControl || this.smsControl.value.length !== 4;
		}

		return false;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TicketsService} ticketsService Сервис работы с билетами.
	 * @param appStoreService Сервис работы с хранилищем приложения.
	 * @param smsCheckService Сервис проверки СМС.
	 * @param dialogInfoService Сервис диалоговых окон.
	 */
	constructor(
		readonly ticketsService: TicketsService,
		readonly appStoreService: AppStoreService,
		private readonly smsCheckService: SmsCheckService,
		private readonly dialogInfoService: DialogContainerService,
	) {}

	/**
	 * Обработчик нажатия кнопки "Назад".
	 */
	onClickBackHandler(): void {
		this.ticketsService.goToCheckPage();
	}

	/**
	 * Обработчик нажатия кнопки "Отмена".
	 */
	onClickCancelHandler(): void {
		Logger.Log.i('TicketPaymentComponent', `onClickCancelHandler -> "Cancel" button clicked`)
			.console();

		this.ticketsService.goToCheckPage();
	}

	/**
	 * Обработчик нажатия кнопки "Повторить СМС".
	 */
	onRepeatSMS(): void {
		this.sendSMS();
	}

	/**
	 * Обработчик нажатия кнопки "Выплата".
	 */
	onClickWinPayHandler(): void {
		this.winPayAction();
	}

	/**
	 * Обработчик изменения поля ввода пин-кода.
	 */
	onPinCodeChange(): void {
		this.appStoreService.smsCodeError = false;
	}

	/**
	 * Обработчик отправки формы
	 */
	onSMSCodeSubmit(): void {
		if (!this.isPayBtnDisabled) {
			this.winPayAction();
		}
	}

	/**
	 * Обработчик фокуса на поле ввода пин-кода.
	 */
	onPinFocused(): void {
		this.pinFocused = true;
	}

	/**
	 * Функция запроса на отправку СМС.
	 * @private
	 */
	private sendSMS(): void {
		this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.sending_sms');
		this.smsCheckService.sendAuthCode(this.ticketsService.winCheck.player_phone)
			.then(this.afterSendAuthCode.bind(this))
			.catch(this.errorSMSHandler.bind(this));
	}

	/**
	 * Функция выплаты выигрыша с бонусом.
	 * @private
	 */
	private winPay(): void {
		Logger.Log.i('TicketPaymentComponent', `onClickWinPayHandler -> "Payment" button clicked`)
			.console();

		this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.win_payment');

		this.ticketsService.isRequestButtonDisabled = true;

		const smsValue = !!this.smsControl ? this.smsControl.value : null;

		this.ticketsService.ticketBonusPay(smsValue);
	}

	/**
	 * Обработчик ответа от сервера после запроса на отправку СМС.
	 * @param sendAuthCodeResp Ответ от сервера.
	 * @private
	 */
	private afterSendAuthCode(sendAuthCodeResp: SendAuthCodeResp): void {
		if (sendAuthCodeResp.err_code === 0) {
			this.payStep = PayStep.Step2;
			this.dialogInfoService.hideAll();
			const tmr = timer(200)
				.subscribe(() => {
					this.smsControl.setFocus();
					tmr.unsubscribe();
				});
		} else {
			this.errorSMSHandler(sendAuthCodeResp);
		}
	}

	/**
	 * Обработчик ошибки при отправке СМС.
	 * @param errResp Ответ от сервера.
	 * @private
	 */
	private errorSMSHandler(errResp: IResponse | IError): void {
		let msg: DialogError;
		if ((errResp as IResponse).err_code) {
			const resp = errResp as IResponse;
			msg = { code: resp.err_code, message: 'lottery.error_sending_sms', messageDetails: resp.err_descr };
		} else {
			msg = (errResp instanceof NetError)
				? { code: errResp.code, message: errResp.message, messageDetails: errResp.messageDetails }
				: new DialogError(ErrorCode.TicketRegister, 'lottery.error_sending_sms');
		}

		this.dialogInfoService.showOneButtonError(msg, {
			click: () => {},
			text: 'dialog.dialog_button_continue'
		});
	}

	/**
	 * Функция выплаты выигрыша с бонусом или отправки СМС.
	 * @private
	 */
	private winPayAction(): void {
		if (this.payStep === PayStep.Step1) {
			this.sendSMS();
		} else {
			this.winPay();
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.ticketsService.isRequestButtonDisabled = false;
	}

}
