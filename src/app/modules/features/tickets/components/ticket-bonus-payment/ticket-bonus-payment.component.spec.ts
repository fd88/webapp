import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketBonusPaymentComponent } from '@app/tickets/components/ticket-bonus-payment/ticket-bonus-payment.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared/shared.module';
import { HttpService } from '@app/core/net/http/services/http.service';

import { LotteriesService } from '@app/core/services/lotteries.service';
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {TicketsService} from "@app/tickets/services/tickets.service";
import {TicketsModule} from "@app/tickets/tickets.module";
import {NetError} from "@app/core/error/types";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {IDialog} from "@app/core/dialog/types";

describe('TicketBonusPaymentComponent', () => {
	let component: TicketBonusPaymentComponent | any;
	let fixture: ComponentFixture<TicketBonusPaymentComponent>;
	let ticketsService: TicketsService;
	let appStoreService: AppStoreService;
	let printService: PrintService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot(),
				SharedModule,
				TicketsModule
			],
			declarations: [
				TicketBonusPaymentComponent,
				MslInputPinComponent
			],
			providers: [
				LogService,
				HttpService,
				LotteriesService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				}
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		ticketsService = TestBed.inject(TicketsService);
		ticketsService.winCheck = {
			isPaper: false,
			win_sum: '5',
			win_ticket_sum: '6',
			win_allow: true,
			is_win: true,
			reason: '',
			barcode: '1111222233334444',
			winBarCode: '1111222233334444',
			sms_required: false,
			document_id: 2
		};
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		printService = TestBed.inject(PrintService);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(TicketBonusPaymentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
		component.smsControl = {
			setFocus: (): void => {
			}
		} as MslInputPinComponent;
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onClickCancelHandler', () => {
		spyOn(component, 'onClickCancelHandler').and.callThrough();
		component.onClickCancelHandler();
		expect(component.onClickCancelHandler).toBeTruthy();
	});

	it('test onClickWinPayHandler', () => {
		spyOn(component, 'onClickWinPayHandler').and.callThrough();

		ticketsService.winCheck = {
			isPaper: false,
			win_sum: '5',
			win_ticket_sum: '6',
			win_allow: true,
			is_win: true,
			reason: '',
			barcode: '1111222233334444',
			winBarCode: '1111222233334444'
		};

		(ticketsService as any).dialogInfoService.showOneButtonError = (): IDialog => undefined;
		component.onClickWinPayHandler();
		expect(component.onClickWinPayHandler).toBeTruthy();

		ticketsService.winCheck.isPaper = true;
		component.onClickWinPayHandler();
		expect(component.onClickWinPayHandler).toBeTruthy();
	});

	it('test afterSendAuthCode', () => {
		spyOn(component, 'afterSendAuthCode').and.callThrough();
		component.afterSendAuthCode({
			client_trans_id: '123456789',
			err_code: 700,
			err_descr: 'Тестовая ошибка'
		});
		expect(component.afterSendAuthCode).toHaveBeenCalled();
	});

	it('test afterSendAuthCode 2', () => {
		spyOn(component, 'afterSendAuthCode').and.callThrough();
		component.afterSendAuthCode(new NetError('Тестовая ошибка', 'Сообщение для тестовой ошибки', 1234));
		expect(component.afterSendAuthCode).toHaveBeenCalled();
	});

	it('test errorSMSHandler', () => {
		spyOn(component, 'errorSMSHandler').and.callThrough();
		component.errorSMSHandler({
			client_trans_id: '123456789',
			err_code: 0,
			err_descr: 'Тестовая ошибка'
		});
		expect(component.errorSMSHandler).toHaveBeenCalled();
	});

	it('test winPayAction', () => {
		spyOn(component, 'winPayAction').and.callThrough();
		ticketsService.winCheck.sms_required = true;
		component.payStep = 1;
		component.winPayAction();
		expect(component.winPayAction).toHaveBeenCalled();
	});

	it('test winPayAction 2', () => {
		spyOn(component, 'winPayAction').and.callThrough();
		ticketsService.winCheck.sms_required = true;
		component.payStep = 2;
		component.winPayAction();
		expect(component.winPayAction).toHaveBeenCalled();
	});

	it('test isPayBtnDisabled getter', () => {
		component.payStep = 2;
		ticketsService.isRequestButtonDisabled = true;
		expect(component.isPayBtnDisabled).toBeTruthy();
	});

	it('test isPayBtnDisabled getter 2', () => {
		component.payStep = 2;
		appStoreService.smsCodeError = true;
		expect(component.isPayBtnDisabled).toBeTruthy();
	});

	it('test isPayBtnDisabled getter 3', () => {
		component.payStep = 2;
		component.smsControl = undefined;
		expect(component.isPayBtnDisabled).toBeTruthy();
	});

	it('test isPayBtnDisabled getter 4', () => {
		component.payStep = 2;
		component.smsControl = {
			value: '123'
		} as MslInputPinComponent;
		expect(component.isPayBtnDisabled).toBeTruthy();
	});

	it('test onRepeatSMS handler', () => {
		spyOn(component, 'onRepeatSMS').and.callThrough();
		component.onRepeatSMS();
		expect(component.onRepeatSMS).toHaveBeenCalled();
	});

	it('test onPinCodeChange handler', () => {
		component.onPinCodeChange();
		expect(appStoreService.smsCodeError).toBeFalsy();
	});

	it('test onSMSCodeSubmit handler', () => {
		spyOn(component, 'onSMSCodeSubmit').and.callThrough();
		component.onSMSCodeSubmit();
		expect(component.onSMSCodeSubmit).toHaveBeenCalled();
	});

	it('test onPinFocused handler', () => {
		component.onPinFocused();
		expect(component.pinFocused).toBeTruthy();
	});

	it('test onClickBackHandler', () => {
		spyOn(component, 'onClickBackHandler').and.callThrough();
		component.onClickBackHandler();
		expect(component.onClickBackHandler).toHaveBeenCalled();
	});
});
