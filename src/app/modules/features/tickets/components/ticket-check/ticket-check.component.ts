import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subject, timer} from 'rxjs';
import {BarcodeReaderService} from '@app/core/barcode/barcode-reader.service';
import {LotteryGameCode} from '@app/core/configuration/lotteries';
import {Logger} from '@app/core/net/ws/services/log/logger';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {MslInputComponent} from '@app/shared/components/msl-input/msl-input.component';
import {
	MslInputWithKeyboardComponent
} from '@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component';
import {TicketsService} from '@app/tickets/services/tickets.service';
import {ApplicationAppId, AppType} from '@app/core/services/store/settings';
import {StorageService} from '@app/core/net/ws/services/storage/storage.service';
import {UserInputMode} from '@app/util/utils';
import {distinctUntilChanged, filter, takeUntil} from "rxjs/operators";
import {BarcodeObject} from "@app/core/barcode/barcode-object";
import {ScannerInputComponent} from "../../../camera/components/scanner-input/scanner-input.component";

/**
 * Компонент проверки билетов.
 */
@Component({
	selector: 'app-ticket-check',
	templateUrl: './ticket-check.component.html',
	styleUrls: ['./ticket-check.component.scss']
})
export class TicketCheckComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Ссылка на поле ввода штрих-кода.
	 */
	@ViewChild('barcodeInput', { static: true })
	barcodeInput: MslInputWithKeyboardComponent;

	/**
	 * Ссылка на поле ввода проверочного номера.
	 */
	@ViewChild('secondBarcodeInput', { static: false })
	secondBarcodeInput: MslInputComponent;

	/**
	 * Ссылка на кнопку проверки.
	 */
	@ViewChild('checkButton', { static: true })
	checkButton: ElementRef;

	@ViewChild('scannerInput', { static: true }) scannerInput: ScannerInputComponent;

	/**
	 * Ссылка на компонент камеры.
	 */
	// @ViewChild('camera', { static: false }) cameraComponent: CameraComponent;

	/**
	 * Тип приложения.
	 */
	readonly AppType = AppType;

	/**
	 * Показана ли камера.
	 */
	isCameraShown = true;

	/**
	 * Сохраненный режим ввода для каждого пользователя (вручную или со сканера).
	 */
	userInputStorage: {[userID: number]: UserInputMode};

	/**
	 * Текущий режим ввода (вручную или со сканера).
	 */
	userInputMode: UserInputMode = 0;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	/**
	 * Сканировать ли проверочный номер.
	 */
	scanCheckNum = false;

	/**
	 * Предыдущий штрих-код.
	 * @private
	 */
	private prevBC = '';

	private isFocused = false;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {BarcodeReaderService} barcodeReaderService Сервис чтения штрих-кодов.
	 * @param {TicketsService} ticketsService Сервис работы с билетами.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {ActivatedRoute} route Активный маршрут.
	 * @param storageService Сервис хранилища в браузере.
	 */
	constructor(
		readonly barcodeReaderService: BarcodeReaderService,
		readonly ticketsService: TicketsService,
		readonly appStoreService: AppStoreService,
		private readonly route: ActivatedRoute,
		private readonly storageService: StorageService
	) {}

	/**
	 * Обработчик клика по документу.
	 *
	 * @param event Переданный объект события.
	 */
	@HostListener('document:click', ['$event'])
	documentClick(event: MouseEvent): void {
		const cList = (event.target as Element).classList;
		this.appStoreService.mobileKeyboardShown = cList.contains('ticket-check__item') || cList.contains('input-field');
	}

	/**
	 * Обработчик нажатия кнопки проверки выигрышности по билету.
	 */
	onClickWinCheckHandler(): void {
		if (this.checkButton.nativeElement.hasAttribute('disabled')) {
			Logger.Log.i('TicketCheckComponent', `onClickWinCheckHandler -> button is disabled`)
				.console();

			return;
		}

		Logger.Log.i('TicketCheckComponent', `onClickWinCheckHandler -> button was clicked`)
			.console();

		const bc = this.barcodeInput.value.trim();
		if ((this.ticketsService.isPaperLottery) ||
			((bc.length === 16) && (!!this.secondBarcodeInput) && (this.secondBarcodeInput.value.length > 0))) {
			const sbc = this.secondBarcodeInput.value.trim();
			Logger.Log.i('TicketCheckComponent', `onClickWinCheckHandler -> will check ticket [${bc}/${sbc}] as TML...`)
				.console();

			this.ticketsService.ticketWinCheckPaper(bc, sbc)
				.subscribe(this.cleanAndFocus.bind(this));
		} else {
			Logger.Log.i('TicketCheckComponent', `onClickWinCheckHandler -> will check ticket [${bc}] as EML...`)
				.console();

			this.ticketsService.ticketWinCheck(bc)
				.subscribe(this.cleanAndFocus.bind(this));
		}
	}

	/**
	 * Слушатель изменения значения элемента ввода баркода.
	 *
	 * @param {string} value Значение элемента ввода.
	 */
	onInputChangeHandler(value: string): void {
		// this.scanCheckNum = false;
		this.checkBarcode(value);
		if (this.ticketsService.isPaperLottery && value.length === 16) {
			const tmr = timer(50)
				.subscribe(() => {
					if (this.secondBarcodeInput) {
						this.secondBarcodeInput.setFocus();
					}
					tmr.unsubscribe();
				});
		}
	}

	/**
	 * Обработчик нажатия клавиши Enter.
	 */
	onEnterPressedHandler(): void {
		this.ticketsService.isPaperLottery ? this.secondBarcodeInput.setFocus() : this.onClickWinCheckHandler();
	}

	/**
	 * Слушатель успешного сканирования штрих-кода.
	 *
	 * @param {string} bc Сканированный штрих-код.
	 */
	onScanSuccessHandler(bc: string): void {
		if (this.isCameraShown) {
			if (bc.length === 16 && !this.scanCheckNum) {
				this.barcodeInput.value = bc;
				this.checkBarcode(bc);
			} else if (this.scanCheckNum && (this.barcodeInput.value !== bc) && (bc.length === 6)) {
				if (this.secondBarcodeInput) {
					this.secondBarcodeInput.value = bc;
				}
				this.scanCheckNum = false;
				this.isCameraShown = false;
			} else if (!this.scanCheckNum) {
				this.barcodeInput.value = bc;
				this.isCameraShown = false;
				this.checkBarcode(bc);
			}
			if (this.prevBC !== bc) {
				this.prevBC = bc;
			}
		}
	}

	/**
	 * Обработчик переключения режима ввода штрих-кода.
	 */
	onSwitchCamera(): void {
		this.isCameraShown = !this.isCameraShown;
		this.userInputMode = this.isCameraShown ? UserInputMode.Scanner : UserInputMode.Manual;
		this.putIntoStorage();
		if (this.isCameraShown) {
			this.scannerInput.showScanner();
		} else {
			this.scannerInput.hideScanner();
		}
		const tmr = timer(100)
			.subscribe(() => {
				this.barcodeInput.setFocus();
				tmr.unsubscribe();
			});
	}

	/**
	 * Обработчик фокуса на поле ввода штрих-кода.
	 */
	onInputFieldFocus(): void {
		// this.isCameraShown = false;
		// this.cameraComponent.changeState(this.isCameraShown);
		// this.appStoreService.mobileKeyboardShown = true;
		// // занести в локальное хранилище
		// this.userInputMode = UserInputMode.Manual;
		// this.putIntoStorage();
		this.isFocused = true;
	}

	onInputFieldBlur(): void {
		this.isFocused = false;
	}

	onBarcodeChangeHandler(bc: string): void {
		this.setBarcode(new BarcodeObject(bc));
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Выполнить очистку полей и разрешить нажатие кнопки запроса.
	 *
	 * @param {boolean} isFocusNeeded Флаг, указывающий, нужно ли установить фокус на поле ввода штрих-кода.
	 */
	private cleanAndFocus(isFocusNeeded: boolean = true): void {
		if (this.secondBarcodeInput) {
			this.secondBarcodeInput.value = '';
		}

		this.barcodeInput.value = '';
		this.ticketsService.isPaperLottery = false;
		// штрихкод может быть вообще не валидным
		this.ticketsService.isUnknownCorrectLottery = false;

		if (isFocusNeeded) {
			this.barcodeInput.setFocus();
		}
	}

	/**
	 * Проверяет введенный штрих-код.
	 *
	 * @param {string} barcode Введенный штрих-код.
	 */
	private checkBarcode(barcode: string): void {
		// если баркод неверный прекратить проверку
		const ba = this.barcodeReaderService.determineBarcodeType(barcode);

		// проверить, принадлежит ли баркод к БМЛ
		this.ticketsService.isPaperLottery = ba.isCorrectBarcode && ba.detectedGameCode === LotteryGameCode.TML_BML;
		this.ticketsService.isUnknownCorrectLottery = ba.isCorrectBarcode && !Number.isInteger(ba.detectedGameCode);
		this.ticketsService.gameCode = ba.detectedGameCode;
		this.scanCheckNum = this.ticketsService.isPaperLottery && barcode.length === 16;

		if (!ba.isCorrectBarcode && barcode.length !== 16 && barcode.length < 24) {
			const tmr = timer(100)
				.subscribe(() => {
					this.barcodeInput.setFocus();
					tmr.unsubscribe();
				});
		} else if (ba.isCorrectBarcode && barcode.length === 16) {
			const tmr = timer(100)
				.subscribe(() => {
					if (this.secondBarcodeInput) {
						this.secondBarcodeInput.setFocus();
					}
					tmr.unsubscribe();
				});
		}

		Logger.Log.i(
			'TicketCheckComponent', `checkBarcode -> is paper ticket? (%s), is unknown correct ticket? (%s)`,
			this.ticketsService.isPaperLottery,
			this.ticketsService.isUnknownCorrectLottery
		)
			.console();
	}

	private setBarcode(ba: BarcodeObject): void {
		if (!!this.barcodeInput.value && this.ticketsService.isPaperLottery) {
			// если поле ввода штрихкода заполнено кодом ТМЛ (показано второе поле)
			// то заполнить второе поле (если заполнено, то перезаполнить)
			this.secondBarcodeInput.value = ba.barcode;
		} else {
			// во всех остальных случаях - заполнить или перезаполнить первое поле
			this.barcodeInput.value = ba.barcode;
			this.checkBarcode(ba.barcode);
		}
	}

	/**
	 * Сохранить режим ввода штрих-кода в браузерное хранилище.
	 * @private
	 */
	private putIntoStorage(): void {
		this.userInputStorage[this.appStoreService.operator.value.userId] = this.userInputMode;
		this.storageService.putSync(ApplicationAppId, [{
			key: 'user_input_mode',
			value: JSON.stringify(this.userInputStorage)
		}]);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		// разблокировать кнопку запроса
		this.ticketsService.isRequestButtonBusy = false;

		this.ticketsService.isPaperLottery = false;
		this.ticketsService.isUnknownCorrectLottery = false;

		// при наличии в url параметра "barcode" заполнить им поле баркода
		if (this.route.snapshot.queryParamMap.has('barcode')) {
			const bc = this.route.snapshot.queryParamMap.get('barcode');
			this.barcodeInput.value = bc ? bc : '';
		}

		// при сканировании билета на форме обновить поле ввода полученным значением
		this.barcodeReaderService.barcodeSubject$$
			.pipe(
				filter(() => !this.isFocused),
				distinctUntilChanged((prev, curr) => prev.barcode === curr.barcode),
				takeUntil(this.unsubscribe$$)
			)
			.subscribe(this.setBarcode.bind(this));

		const uInputStorage = this.storageService.getSync(ApplicationAppId, ['user_input_mode']);
		this.userInputStorage = uInputStorage && uInputStorage.data && (uInputStorage.data.length > 0) ?
			JSON.parse(uInputStorage.data[0].value) : {};
		this.userInputMode = this.userInputStorage[this.appStoreService.operator.value.userId] ?
			this.userInputStorage[this.appStoreService.operator.value.userId] : UserInputMode.Scanner;
		this.isCameraShown = this.userInputMode === UserInputMode.Scanner;
		// const tmr = timer(50)
		// 	.subscribe(() => {
		// 		this.barcodeInput.setFocus();
		// 		tmr.unsubscribe();
		// 	});
		this.ticketsService.focusInput = this.barcodeInput;

		this.appStoreService.showCameraComponent$$
			.pipe(
				takeUntil(this.unsubscribe$$)
			)
			.subscribe((showCameraComponent) => {
				if (showCameraComponent) {
					if (this.isCameraShown) {
						this.scannerInput.showScanner();
					}
				} else {
					this.scannerInput.hideScanner();
				}
			});

	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
