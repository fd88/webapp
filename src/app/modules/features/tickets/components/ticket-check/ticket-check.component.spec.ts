import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '@app/shared/shared.module';

import { AppStoreService } from '@app/core/services/store/app-store.service';

import { LogService } from '@app/core/net/ws/services/log/log.service';

import { TicketsService } from '@app/tickets/services/tickets.service';
import { TicketCheckComponent } from '@app/tickets/components/ticket-check/ticket-check.component';
import { HttpService } from '@app/core/net/http/services/http.service';

import { MslInputWithKeyboardComponent } from '@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component';
import { MslInputComponent } from '@app/shared/components/msl-input/msl-input.component';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import {SESS_DATA} from "../../../mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {CoreModule} from "@app/core/core.module";
import {HttpClientModule} from "@angular/common/http";

describe('TicketCheckComponent', () => {
	let component: TicketCheckComponent | any;
	let fixture: ComponentFixture<TicketCheckComponent>;
	let appStoreService: AppStoreService;
	let ticketsService: TicketsService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot(),
				SharedModule,
				CoreModule
			],
			declarations: [
				TicketCheckComponent
			],
			providers: [
				LogService,
				TicketsService,
				HttpService,
				BarcodeReaderService,
				LotteriesService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				}
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		TestBed.inject(BarcodeReaderService);
		appStoreService = TestBed.inject(AppStoreService);

		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		(appStoreService.Settings as any)._csEnvironmentUrl = ''
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		ticketsService = TestBed.inject(TicketsService);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(TicketCheckComponent);
		component = fixture.debugElement.componentInstance;
		// component.route.snapshot.queryParams = {'barcode': '11112222333444'};
		fixture.detectChanges();
	});

	it('should create the TicketCheckComponent', () => {
		expect(component).toBeTruthy();
	});

	it('check min/max range of entered barcodes', () => {
		component.barcodeInput.value = '';
		fixture.detectChanges();
		expect(component.checkButton.nativeElement.hasAttribute('disabled')).toBeTruthy();

		component.barcodeInput.value = '123456789012345';
		fixture.detectChanges();
		expect(component.checkButton.nativeElement.hasAttribute('disabled')).toBeTruthy();

		component.barcodeInput.value = '123456789012345678901234567';
		fixture.detectChanges();
		expect(component.checkButton.nativeElement.hasAttribute('disabled')).toBeTruthy();

		component.barcodeInput.value = '1234567890123456';
		fixture.detectChanges();
		expect(component.checkButton.nativeElement.hasAttribute('disabled')).toBeFalsy();

		component.barcodeInput.value = '12345678901234567890123456';
		fixture.detectChanges();
		expect(component.checkButton.nativeElement.hasAttribute('disabled')).toBeFalsy();

		component.barcodeInput.value = '12345678901234567890';
		fixture.detectChanges();
		expect(component.checkButton.nativeElement.hasAttribute('disabled')).toBeFalsy();
	});

	it('test onClickWinCheckHandler method', () => {

		spyOn(component, 'onClickWinCheckHandler').and.callThrough();
		component.checkButton.disabled = true;
		component.onClickWinCheckHandler();
		expect(component.onClickWinCheckHandler).toHaveBeenCalled();

		component.checkButton.nativeElement.removeAttribute('disabled');
		ticketsService.isPaperLottery = true;
		component.barcodeInput = {
			value: '111122223333',
			setFocus: () => {
			}
		} as MslInputWithKeyboardComponent;
		component.secondBarcodeInput = {
			value: '12345'
		} as MslInputComponent;
		component.onClickWinCheckHandler();
		expect(component.onClickWinCheckHandler).toHaveBeenCalled();

		ticketsService.isPaperLottery = false;
		component.onClickWinCheckHandler();
		expect(component.onClickWinCheckHandler).toHaveBeenCalled();
	});

	it('test onEnterPressedHandler method', () => {
		spyOn(component, 'onEnterPressedHandler').and.callThrough();
		component.secondBarcodeInput = {
			setFocus: () => {}
		} as MslInputComponent;
		ticketsService.isPaperLottery = true;
		component.onEnterPressedHandler();
		expect(component.onEnterPressedHandler).toHaveBeenCalled();

		component.checkButton.disabled = false;
		ticketsService.isPaperLottery = false;
		component.onEnterPressedHandler();
		expect(component.onEnterPressedHandler).toHaveBeenCalled();
	});

	it('test onInputChangeHandler method', () => {
		component.onInputChangeHandler('111122223333');
		expect(ticketsService.isPaperLottery).toBeFalsy();
	});

	it('test onInputChangeHandler method 2', () => {
		ticketsService.isPaperLottery = true;
		component.secondBarcodeInput = {
			setFocus: () => {}
		};
		spyOn(component, 'onInputChangeHandler').and.callThrough();
		component.onInputChangeHandler('0662123412341234');
		expect(component.onInputChangeHandler).toHaveBeenCalled();
	});

	it('test cleanAndFocus method', () => {

		ticketsService.isPaperLottery = true;
		ticketsService.isUnknownCorrectLottery = true;

		component.cleanAndFocus(false);
		expect(ticketsService.isPaperLottery).toBeFalsy();
		expect(ticketsService.isUnknownCorrectLottery).toBeFalsy();

		component.barcodeInput = {
			value: '12345',
			setFocus: () => {}
		} as MslInputWithKeyboardComponent;
		component.secondBarcodeInput = {
			value: '12345'
		} as MslInputComponent;

		ticketsService.isPaperLottery = true;
		component.cleanAndFocus();
		expect(ticketsService.isPaperLottery).toBeFalsy();
	});

	it('test checkBarcode method', () => {
		component.barcodeReaderService.determineBarcodeType = () => {
			return {
				isCorrectBarcode: true,
				detectedGameCode: LotteryGameCode.TML_BML
			}
		};
		component.secondBarcodeInput = {
			setFocus: () => {
			}
		} as MslInputComponent;
		component.checkBarcode('0000000000000000');
		expect(ticketsService.isPaperLottery).toBeTruthy();
	});

	it('test ngOnDestroy handler', () => {
		spyOn(component, 'ngOnDestroy').and.callThrough();
		component.ngOnDestroy();
		expect(component.ngOnDestroy).toHaveBeenCalled();
	});

	it('test onScanSuccessHandler', () => {
		component.scanCheckNum = false;
		component.isCameraShown = true;
		spyOn(component, 'onScanSuccessHandler').and.callThrough();
		component.onScanSuccessHandler('0662012312341234');
		expect(component.onScanSuccessHandler).toHaveBeenCalled();
	});

	it('test onScanSuccessHandler 2', () => {
		component.isCameraShown = true;
		spyOn(component, 'onScanSuccessHandler').and.callThrough();
		component.onScanSuccessHandler('0662012312341');
		expect(component.onScanSuccessHandler).toHaveBeenCalled();
	});

	it('test onScanSuccessHandler 3', () => {
		component.isCameraShown = true;
		component.scanCheckNum = true;
		component.barcodeInput = {
			value: ''
		};
		component.secondBarcodeInput = {
			value: ''
		};
		spyOn(component, 'onScanSuccessHandler').and.callThrough();
		component.onScanSuccessHandler('123456');
		expect(component.onScanSuccessHandler).toHaveBeenCalled();
	});

	it('test onSwitchCamera', () => {
		spyOn(component, 'onSwitchCamera').and.callThrough();
		component.onSwitchCamera();
		expect(component.onSwitchCamera).toHaveBeenCalled();
	});

	it('test onInputFieldFocus handler', () => {
		spyOn(component, 'onInputFieldFocus').and.callThrough();
		component.onInputFieldFocus();
		expect(component.onInputFieldFocus).toHaveBeenCalled();
	});
});
