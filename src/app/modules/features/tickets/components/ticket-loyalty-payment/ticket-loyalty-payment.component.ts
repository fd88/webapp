import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {concatMap} from "rxjs/operators";
import {TicketsService} from "@app/tickets/services/tickets.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {URL_CHECK, URL_TICKETS} from "@app/util/route-utils";
import {Logger} from "@app/core/net/ws/services/log/logger";
import {Router} from "@angular/router";
import {of} from "rxjs";
import {DialogError, ErrorCode} from "@app/core/error/dialog";
import {GetCardInfoResp} from "@app/core/net/http/api/models/get-card-info";
import {WinPayToCardResp} from "@app/core/net/http/api/models/win-pay-to-card";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {ILoyaltyStepResult} from "../../../user-loyalty-auth/interfaces/i-loyalty-step-result";

@Component({
  selector: 'app-ticket-loyalty-payment',
  templateUrl: './ticket-loyalty-payment.component.html',
  styleUrls: ['./ticket-loyalty-payment.component.scss']
})
export class TicketLoyaltyPaymentComponent implements OnInit, OnDestroy {

	showProposal = true;

	needsVerification = false;

	playersCardNum = '';

	isLoading = false;

	finalObject = {
		next: (resp: WinPayToCardResp | null) => {
			if (resp) {
				this.dialogInfoService.showOneButtonInfo('dialog.complete', 'dialog.win_pay_to_card_done', {
					click: this.goToCheckPage.bind(this),
					text: 'dialog.dialog_button_continue'
				});
			}
			this.userLoyaltyAuthService.isLoading = false;
			this.isLoading = false;
		},
		error: (error) => {
			this.dialogInfoService.showOneButtonError(error, {
				text: 'dialog.dialog_button_continue'
			});
			this.userLoyaltyAuthService.isLoading = false;
			this.isLoading = false;
		}
	};

  	constructor(
		  private readonly appStoreService: AppStoreService,
		  readonly userLoyaltyAuthService: UserLoyaltyAuthService,
		  private readonly ticketsService: TicketsService,
		  private readonly dialogInfoService: DialogContainerService,
		  private readonly router: Router
	) { }

	onWinPayToCard(): void {
		this.showProposal = false;
	}

	/**
	 * Перейти на страницу проверки доступности выплаты по чеку.
	 */
	goToCheckPage(): void {
		this.router.navigate([URL_TICKETS, URL_CHECK])
			.catch(err => Logger.Log.e('TicketsService', `goToCheckPage -> can't navigate to check page: ${err}`)
				.console());
	}

	onCardNumEntered(result: ILoyaltyStepResult): void {
		this.playersCardNum = result.cardNum;
		const kopSum = +this.ticketsService.winCheck.win_sum * 100;
		this.needsVerification = result.data.card_limits.verification_needed_min_win_sum
			? kopSum >= result.data.card_limits.verification_needed_min_win_sum.limit_value
			: false;
		if (!this.needsVerification) {
			this.isLoading = true;
			this.userLoyaltyAuthService.winPayToCard(
				this.ticketsService.winCheck.barcode,
				kopSum.toString(),
				result.cardNum,
				this.ticketsService.gameCode
			).subscribe(this.finalObject);
		}
	}

	onPayAndFinish(): void {
		this.userLoyaltyAuthService.isLoading = true;
		this.isLoading = false;
		this.userLoyaltyAuthService.winPayToCard(
			this.ticketsService.winCheck.barcode,
			(+this.ticketsService.winCheck.win_sum * 100).toString(),
			this.playersCardNum,
			this.ticketsService.gameCode
		).subscribe(this.finalObject);
	}

	ngOnInit(): void {
		this.appStoreService.cardNum = '';
		this.userLoyaltyAuthService.lastEnteredPhone = '';
	}

  	ngOnDestroy(): void {
	  this.userLoyaltyAuthService.lastEnteredPhone = '';
	  this.userLoyaltyAuthService.isLoading = false;
	  this.userLoyaltyAuthService.errorText = '';
  	}

}
