import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TicketLoyaltyPaymentComponent} from './ticket-loyalty-payment.component';
import {RouterTestingModule} from "@angular/router/testing";
import {SharedModule} from "@app/shared/shared.module";
import {UserLoyaltyAuthModule} from "../../../user-loyalty-auth/user-loyalty-auth.module";
import {
	UserLoyaltyAuthComponent
} from "../../../user-loyalty-auth/components/user-loyalty-auth/user-loyalty-auth.component";
import {UserLoyaltyAuthService} from "../../../user-loyalty-auth/services/user-loyalty-auth.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TicketsService} from "@app/tickets/services/tickets.service";
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@app/core/core.module";
import {LotteriesService} from "@app/core/services/lotteries.service";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {Operator} from "@app/core/services/store/operator";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

describe('TicketLoyaltyPaymentComponent', () => {
  let component: TicketLoyaltyPaymentComponent;
  let fixture: ComponentFixture<TicketLoyaltyPaymentComponent>;
  let ticketsService: TicketsService;
  let userLoyaltyAuthService: UserLoyaltyAuthService;
  let appStoreService: AppStoreService;
  let csConfig: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			SharedModule,
			RouterTestingModule,
			SharedModule,
			UserLoyaltyAuthModule,
			HttpClientModule,
			TranslateModule.forRoot(),
			CoreModule
		],
      	declarations: [
			TicketLoyaltyPaymentComponent,
			UserLoyaltyAuthComponent
		],
		providers: [
			UserLoyaltyAuthService,
			HttpService,
			AppStoreService,
			TicketsService,
			HttpClient,
			LotteriesService,
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			}
		]
    })
    .compileComponents();

	  ticketsService = TestBed.inject(TicketsService);
	  userLoyaltyAuthService = TestBed.inject(UserLoyaltyAuthService);

	  userLoyaltyAuthService.lastEnteredPhone = '+380681234567';

	  TestBed.inject(LogService);
	  appStoreService = TestBed.inject(AppStoreService);
	  csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);
	  const operator = new Operator('testInstantUser', `${Date.now()}`, '123456', 1);
	  appStoreService.operator.next(operator);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketLoyaltyPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onWinPayToCard handler', () => {
		component.showProposal = true;
		component.onWinPayToCard();
		expect(component.showProposal).toBeFalsy();
	});

	it('test goToCheckPage method', () => {
		spyOn(component, 'goToCheckPage').and.callThrough();
		component.goToCheckPage();
		expect(component.goToCheckPage).toHaveBeenCalled();
	});

	it('test ngOnInit lifecycle hook', () => {
		ticketsService.winCheck = {
			barcode: '1111222233334444',
			additional_info: {
				seller_hint: 'Його сума більша, ніж ліміт виплати на точці продажу. Тому порадьте щасливчику звернутись на Гарячу лінію МСЛ \u2013 0 800 21 00 65. Дзвінки безкоштовні',
				seller_notice: 'Вітаємо гравця з великим виграшем!'
			},
			win_sum: '2415000',
			win_ticket_sum: '3000000',
			win_allow: true,
			is_win: true,
			isPaper: false,
			winBarCode: '1111222233334444',
			reason: 'test'
		};
		ticketsService.gameCode = LotteryGameCode.Zabava;
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(component.ngOnInit).toHaveBeenCalled();
	});
});
