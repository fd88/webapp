import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';

import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { TicketsService } from '@app/tickets/services/tickets.service';

import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { ResponseCacheService } from '@app/core/services/response-cache.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';

import { LotteriesService } from '@app/core/services/lotteries.service';
import { EsapActions } from '@app/core/configuration/esap';
import { Observable } from 'rxjs';
import { WinBonusType } from '@app/core/net/http/api/models/win-check';
import {HttpClientModule} from "@angular/common/http";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

xdescribe('TicketsService', () => {
	let service: TicketsService | any;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				TicketsService,
				AppStoreService,
				StorageService,
				HttpService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},

				LogService,
				ResponseCacheService,
				PrintService,

				LotteriesService,
				TransactionService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(TicketsService);

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test cleanUp method', () => {
		service.winCheck = {};
		service.isRequestButtonBusy = true;
		service.cleanUp();
		expect(service.winCheck).toBeUndefined();
		expect(service.isRequestButtonBusy).toBeFalsy();
	});

	it('test ticketWinCheck method', () => {
		spyOn(service, 'ticketWinCheck').and.callThrough();

		service.ticketWinCheck('0');
		expect(service.ticketWinCheck).toHaveBeenCalled();

		service.ticketWinCheck('0000000000000000');
		expect(service.ticketWinCheck).toHaveBeenCalled();

		service.httpService.sendApi = (winCheckReq) => {
			return new Promise(resolve => resolve(null));
		};
		service.ticketWinCheck('1111222233334444');
		expect(service.ticketWinCheck).toHaveBeenCalled();
	});

	it('test ticketWinPay method', () => {
		service.winCheck = {
			barcode: '1111222233334444',
			win_sum: '5'
		};

		spyOn(service, 'ticketWinPay').and.callThrough();

		service.ticketWinPay();
		expect(service.ticketWinPay).toHaveBeenCalled();

		service.ticketWinPay();
		expect(service.ticketWinPay).toHaveBeenCalled();
	});

	it('test ticketWinPayPaper method', () => {
		service.winCheck = {
			barcode: '1111222233334444',
			win_sum: '5'
		};

		spyOn(service, 'ticketWinPayPaper').and.callThrough();

		service.ticketWinPayPaper();
		expect(service.ticketWinPayPaper).toHaveBeenCalled();

		service.ticketWinPayPaper();
		expect(service.ticketWinPayPaper).toHaveBeenCalled();
	});

	it('test ticketBonusPay method', () => {
		service.winCheck = {
			barcode: '1111222233334444',
			win_sum: '5'
		};

		spyOn(service, 'ticketBonusPay').and.callThrough();
		service.ticketBonusPay();
		expect(service.ticketBonusPay).toHaveBeenCalled();

		service.printService = {
			isReady: () => true
		};

		service.bonusPayTransactionService.executeBonusRequest = (barcode: string, bonusId: number) => {
			return new Observable(observer => {
				observer.next('test');
			});
		};
		service.ticketBonusPay();
		expect(service.ticketBonusPay).toHaveBeenCalled();

		service.bonusPayTransactionService.executeBonusRequest = (barcode: string, bonusId: number) => {
			return new Observable(observer => {
				observer.error('test');
			});
		};

		service.ticketBonusPay();
		expect(service.ticketBonusPay).toHaveBeenCalled();
	});

	it('test goToPaymentPage method', () => {
		spyOn(service, 'goToPaymentPage').and.callThrough();
		service.goToPaymentPage();
		expect(service.goToPaymentPage).toHaveBeenCalled();
	});

	it('test goToBonusPaymentPage method', () => {
		spyOn(service, 'goToBonusPaymentPage').and.callThrough();
		service.goToBonusPaymentPage();
		expect(service.goToBonusPaymentPage).toHaveBeenCalled();
	});

	it('test requestTicketPayment method', () => {
		spyOn(service, 'requestTicketPayment').and.callThrough();

		const request = {
			"headers": {
				"normalizedNames": {},
				"lazyUpdate": null,
				"headers": {}
			},
			"params": {
				"map": {}
			},
			"trans_id": "402340407050",
			"url": EsapActions.ZabavaRegBet
		};
		const barcode = '0659000027089640';
		service.winCheck = {
			barcode: '1111222233334444',
			win_sum: '5'
		};

		service.httpService.sendApi = (request) => {
			return new Promise(resolve => resolve({}));
		};
		service.requestTicketPayment(request, barcode);
		expect(service.requestTicketPayment).toHaveBeenCalled();

		service.httpService.sendApi = (request) => {
			return new Promise((resolve, reject ) => reject({}));
		};
		service.requestTicketPayment(request, barcode);
		expect(service.requestTicketPayment).toHaveBeenCalled();

	});


	it('test ticketWinCheckPaper method', () => {
		service.winCheck = {
			barcode: '1111222233334444',
			win_sum: '5'
		};

		spyOn(service, 'ticketWinCheckPaper').and.callThrough();

		service.httpService.sendApi = (winCheckReq) => {
			return new Promise(resolve => resolve({
				client_trans_id: "402340407050",
				err_code: 0,
				err_descr: '',
				user_id: 1,
				sid: '111',
				win_ticket_sum: 10,
				win_sum: 8,
				win_allow: 1,
				reason_descr: 'test',
				extra_win: [{
					id: "195",
					pay_instantly: "1",
					type: WinBonusType.Bonus,
					win_allow: "1",
					win_sum: "10000",
					win_summary: "Подарунок - лотерейні білети на сумму"
				}]
			}));
		};
		service.ticketWinCheckPaper();
		expect(service.ticketWinCheckPaper).toHaveBeenCalled();

		service.httpService.sendApi = (winCheckReq) => {
			return new Promise(resolve => resolve({
				client_trans_id: "402340407050",
				err_code: 0,
				err_descr: '',
				user_id: 1,
				sid: '111',
				win_ticket_sum: 10,
				win_sum: 8,
				win_allow: '1',
				reason_descr: 'test',
				extra_win: []
			}));
		};
		service.ticketWinCheckPaper();
		expect(service.ticketWinCheckPaper).toHaveBeenCalled();


		service.httpService.sendApi = (winCheckReq) => {
			return new Promise(resolve => resolve({
				client_trans_id: "402340407050",
				err_code: 0,
				err_descr: '',
				user_id: 1,
				sid: '111',
				win_ticket_sum: 10,
				win_sum: 8,
				win_allow: '0',
				reason_descr: 'test',
				extra_win: []
			}));
		};
		service.ticketWinCheckPaper();
		expect(service.ticketWinCheckPaper).toHaveBeenCalled();

		service.httpService.sendApi = (winCheckReq) => {
			return new Promise(resolve => resolve({
				client_trans_id: "402340407050",
				err_code: 0,
				err_descr: '',
				user_id: 1,
				sid: '111',
				win_ticket_sum: 10,
				win_sum: 8,
				win_allow: '0',
				reason_descr: 'test',
				extra_win: [{
					id: "195",
					pay_instantly: "1",
					type: WinBonusType.Bonus,
					win_allow: "1",
					win_sum: "10000",
					win_summary: "Подарунок - лотерейні білети на сумму"
				}]
			}));
		};
		service.ticketWinCheckPaper();
		expect(service.ticketWinCheckPaper).toHaveBeenCalled();

		service.httpService.sendApi = (winCheckReq) => {
			return new Promise(resolve => resolve({}));
		};
		service.ticketWinCheckPaper();
		expect(service.ticketWinCheckPaper).toHaveBeenCalled();

	});

});
