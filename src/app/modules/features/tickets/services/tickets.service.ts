import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {from, Observable, Subject, timer} from 'rxjs';
import { finalize } from 'rxjs/operators';
import {
	URL_BONUS_PAYMENT,
	URL_CHECK,
	URL_LOTTERIES,
	URL_PAYMENT,
	URL_PAYMENT_FINISH,
	URL_TICKETS
} from '@app/util/route-utils';
import { numberFromStringCurrencyFormat, stringFromNumberCurrencyFormat } from '@app/util/utils';
import { validateResponseSync } from '@app/util/validator';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { IError, NetError } from '@app/core/error/types';
import {
	IExtraWin,
	PlayerAuth,
	WinCheckPaperReq,
	WinCheckPaperResp,
	WinCheckReq,
	WinCheckResp
} from '@app/core/net/http/api/models/win-check';
import { WinPayPaperReq, WinPayReq } from '@app/core/net/http/api/models/win-pay';
import { HttpService } from '@app/core/net/http/services/http.service';
import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';
import { ResponseCacheService } from '@app/core/services/response-cache.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { BonusPayTransactionService } from '@app/core/services/transaction/bonus-pay-transaction.service';
import { TotalCheckStorageService } from '@app/total-check/services/total-check-storage.service';
import { WinCheckData } from '@app/tickets/interfaces/win-check-data';
import { LogOutService } from '@app/logout/services/log-out.service';
import { MslInputWithKeyboardComponent } from '@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component';
import { GetDocumentReq, GetDocumentResp } from '@app/core/net/http/api/models/get-document';
import {LotteryGameCode} from "@app/core/configuration/lotteries";

/**
 * Сервис проверки билетов.
 * Используется как модель-контролер для хранения данных, заполняемых оператором на странице проверки билетов.
 */
@Injectable({
	providedIn: 'root'
})
export class TicketsService {

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Данные для проверки выигрыша.
	 */
	winCheck: WinCheckData;

	/**
	 * Признак, указывающий, что в строку проверки введен баркод бумажной лотереи.
	 */
	isPaperLottery: boolean;

	/**
	 * Признак занятости кнопки запроса.
	 */
	isRequestButtonBusy = false;

	/**
	 * Признак, указывающий, что в строку проверки введен корректный баркод, но тиражи по нему отсутствуют.
	 */
	isUnknownCorrectLottery = false;

	/**
	 * Недоступна ли кнопка запроса?
	 */
	isRequestButtonDisabled = false;

	/**
	 * Поле ввода с фокусировкой
	 */
	focusInput: MslInputWithKeyboardComponent;

	/**
	 * Код игры из последнего проверенного билета
	 */
	gameCode: LotteryGameCode;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {Router} router Объект маршрутизации.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {HttpService} httpService Сервис HTTP-запросов.
	 * @param {DialogContainerService} dialogInfoService Сервис диалоговых окон.
	 * @param {TranslateService} translate Сервис локализации.
	 * @param {TotalCheckStorageService} totalCheckService Сервис хранилища данных чека.
	 * @param {TransactionService} transactionService Сервис транзакций.
	 * @param {ResponseCacheService} responseCacheService Сервис кэша ответов.
	 * @param {PrintService} printService Сервис печати.
	 * @param {BonusPayTransactionService} bonusPayTransactionService Сервис транзакций бонус-платежей.
	 * @param logoutService Сервис выхода из системы.
	 */
	constructor(
		private readonly router: Router,
		private readonly appStoreService: AppStoreService,
		private readonly httpService: HttpService,
		private readonly dialogInfoService: DialogContainerService,
		private readonly translate: TranslateService,
		private readonly totalCheckService: TotalCheckStorageService,
		private readonly transactionService: TransactionService,
		private readonly responseCacheService: ResponseCacheService,
		private readonly printService: PrintService,
		private readonly bonusPayTransactionService: BonusPayTransactionService,
		private readonly logoutService: LogOutService
	) {}

	/**
	 * Очищает контейнер с хранимыми данными и разблокирует кнопку запроса.
	 */
	cleanUp(): void {
		this.winCheck = undefined;
		this.isRequestButtonBusy = false;
		if (this.focusInput) {
			this.focusInput.setFocus();
		}
	}

	/**
	 * Запускает процедуру проверки билета лотереи в ЦС.
	 *
	 * @param {string} barcode Проверяемый баркод.
	 */
	ticketWinCheck(barcode: string): Subject<boolean> {
		const result = new Subject<boolean>();

		// заблокировать кнопку запроса
		this.isRequestButtonBusy = true;

		// создать запрос для проверки и обработать ошибку некорректного запроса
		let winCheckReq;
		try {
			winCheckReq = new WinCheckReq(this.appStoreService, barcode, this.translate.currentLang);
		} catch (e) {
			this.dialogInfoService.showOneButtonError(new DialogError(ErrorCode.BarcodeRegexp, 'dialog.barcode_regexp_error'),
			{
				click: () => {
					result.next(true);
					this.cleanUp();
				},
				text: 'dialog.dialog_button_continue'
			});

			return result;
		}

		// отправить запрос на проверку в ЦС
		this.transactionService.setLastUnCanceled();
		this.httpService.sendApi(winCheckReq)
			.then((response: WinCheckResp) => {
				Logger.Log.i('TicketsService', 'ticketWinCheck -> WinCheckResp response: %s', response)
					.console();

				// заблокировать кнопку запроса
				this.isRequestButtonBusy = true;

				return this.validateAndMoveToPayment<WinCheckResp>(
					WinCheckResp,
					barcode,
					undefined,
					response,
					result,
					false
				);
			})
			.catch((error: IError) => {
				Logger.Log.e('TicketsService', `ticketWinCheck -> WinCheckResp got ERROR code: ${error.code}, message: ${error.message}`)
					.console();

				// заблокировать кнопку запроса
				this.isRequestButtonBusy = true;

				if (error.code && ((error.code === 4313) || (error.code === 4318))) {
					this.dialogInfoService.showOneButtonError(new DialogError(error.code, error.message, error.messageDetails), {
						click: () => this.logoutService.logoutOperator(),
						text: 'dialog.dialog_button_continue'
					});
				} else {
					let err: NetError | DialogError;
					err = (error instanceof NetError)
						? error
						: new DialogError(ErrorCode.TicketChecking, 'dialog.ticket_checking_error');
					this.dialogInfoService.showOneButtonError(err, {
						click: () => this.cleanUp(),
						text: 'dialog.dialog_button_continue'
					});
					result.next(true);
				}
			});

		return result;
	}

	/**
	 * Запускает процедуру проверки билета ТМЛ/БМЛ в ЦС.
	 *
	 * @param {string} barcode Проверяемый баркод.
	 * @param {string} winBarCode Проверочный код.
	 */
	ticketWinCheckPaper(barcode: string, winBarCode: string): Subject<boolean> {
		const result = new Subject<boolean>();

		// заблокировать кнопку запроса
		this.isRequestButtonBusy = true;

		this.transactionService.setLastUnCanceled();

		// создаем запрос
		const request = new WinCheckPaperReq(this.appStoreService, barcode, winBarCode, this.translate.currentLang);
		this.httpService.sendApi(request)
			.then((response: WinCheckResp) => {
				Logger.Log.i('TicketsService', 'WinCheckPaperReq response: %s', response)
					.console();

				// разаблокировать кнопку запроса
				this.isRequestButtonBusy = true;

				return this.validateAndMoveToPayment<WinCheckPaperResp>(
					WinCheckPaperResp,
					barcode,
					winBarCode,
					response,
					result,
					true
				);
			})
			.catch(error => {
				// разаблокировать кнопку запроса
				this.isRequestButtonBusy = true;

				if (error.code && ((error.code === 4313) || (error.code === 4318))) {
					this.dialogInfoService.showOneButtonError(new DialogError(error.code, error.message, error.messageDetails), {
						click: () => this.logoutService.logoutOperator(),
						text: 'dialog.dialog_button_continue'
					});
				} else {
					let err: NetError | DialogError;
					err = (error instanceof NetError)
						? error
						: new DialogError(ErrorCode.TicketChecking, 'dialog.ticket_checking_error');
					this.dialogInfoService.showOneButtonError(err, {
						click: () => {
							this.cleanUp();
							result.next(false);
						},
						text: 'dialog.dialog_button_continue'
					});
				}
			});

		return result;
	}

	/**
	 * Запускает процедуру выплаты по билету обычной лотереи в ЦС.
	 * @param smsCode Код подтверждения из СМС.
	 */
	ticketWinPay(smsCode: string): void {
		let winPayReq: WinPayReq;
		try {
			winPayReq = new WinPayReq(
				this.appStoreService,
				this.winCheck.barcode,
				`${numberFromStringCurrencyFormat(this.winCheck.win_sum)}`,
				smsCode
			);
		} catch (e) {
			this.dialogInfoService.showOneButtonError(
				new DialogError(ErrorCode.BarcodeRegexp, 'dialog.barcode_regexp_error'), {
					click: this.goToCheckPage.bind(this),
					text: 'dialog.dialog_button_continue'
				});

			return;
		}

		this.requestTicketPayment(winPayReq, this.winCheck.barcode);
	}

	/**
	 * Запускает процедуру выплаты по билету лотереи ТМЛ/БМЛ в ЦС.
	 * @param smsCode Код подтверждения из СМС.
	 */
	ticketWinPayPaper(smsCode: string): void {
		let request;
		try {
			request = new WinPayPaperReq(
				this.appStoreService,
				this.winCheck.barcode,
				this.winCheck.winBarCode,
				`${numberFromStringCurrencyFormat(this.winCheck.win_sum)}`,
				smsCode
			);
		} catch (e) {
			this.dialogInfoService.showOneButtonError(
				new DialogError(ErrorCode.TicketPayment, 'dialog.ticket_payment_error'), {
					click: this.goToCheckPage.bind(this),
					text: 'dialog.dialog_button_continue'
				});

			return;
		}

		this.requestTicketPayment(request, this.winCheck.barcode);
	}

	/**
	 * Зарегистрировать бонус
	 * @param smsValue Код подтверждения из СМС.
	 */
	ticketBonusPay(smsValue: string): void {
		Logger.Log.i('TicketsService', `ticketBonusPay -> ticket: ${this.winCheck.barcode}, bonusId: ${this.winCheck.bonusId}`)
			.console();

		// this.ticketsService.winCheck.player_phone
		this.bonusPayTransactionService.executeBonusRequest(this.winCheck.barcode, this.winCheck.bonusId,
			this.winCheck.player_phone, smsValue)
			.pipe(
				finalize(() => {
					console.log('+++finalize');
					this.goToCheckPage();
				})
			)
			.subscribe(
				next => {
					console.log('++++++!!!!!!!!!!!!!!!!!state', next);
				},
				error => {
					console.log('++++++error', error);
					Logger.Log.e('TicketsService', `ticketBonusPay -> transaction ERROR: %s`, error)
						.console();

					let errMsg: NetError | DialogError;
					errMsg = error instanceof NetError
						? error
						: !!error.message
							? {message: error.message, code: error.code, messageDetails: error.messageDetails}
							: new DialogError(ErrorCode.Transaction, !!error.message ? error.message : 'dialog.transaction_error');

					this.dialogInfoService.showOneButtonError(errMsg, {
						click: this.goToCheckPage.bind(this),
						text: 'dialog.dialog_button_continue'
					});
				}
			);
	}

	/**
	 * Перейти на страницу проверки доступности выплаты по чеку.
	 */
	goToCheckPage(): void {
		this.router.navigate([URL_TICKETS, URL_CHECK])
			.catch(err => Logger.Log.e('TicketsService', `goToCheckPage -> can't navigate to check page: ${err}`)
				.console());
	}

	/**
	 * Запрос на документ
	 */
	getDocument(): Observable<GetDocumentResp> {
		const request = new GetDocumentReq(this.appStoreService, this.winCheck.document_id);

		return from(this.httpService.sendApi(request) as Promise<GetDocumentResp>);
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Выполнить выплату по чеку.
	 *
	 * @param {WinPayReq | WinPayPaperReq} request Запрос на выплату.
	 * @param {string} barcode Штрих-код билета.
	 */
	private requestTicketPayment(request: WinPayReq | WinPayPaperReq, barcode: string): void {
		const amount = numberFromStringCurrencyFormat(this.winCheck.win_sum);

		const trsMode = +localStorage.getItem('TRS_MODE') || 0;

		this.transactionService.setLastUnCanceled();
		this.httpService.sendApi(request)
			.then(response => {

				this.appStoreService.smsCodeError = false;

				Logger.Log.i('TicketsService', 'WinPayResp response: %s', response)
					.console();


				// if (trsMode === 3) {
				// 	this.appStoreService.showBrowserPrint = true;
				// 	this.appStoreService.winPaid = parseFloat(this.winCheck.win_ticket_sum);
				//
				// 	this.dialogInfoService.showConfirmTwoButtons(
				// 		'dialog.complete',
				// 		'dialog.win_pay_ticket_ok_info',
				// 		'',
				// 		{
				// 			first: {
				// 				text: 'tickets.print-check',
				// 				click: () => {
				// 					window.print();
				// 				}
				// 			},
				// 			second: {
				// 				text: 'tickets.to-start',
				// 				click: () => {
				// 					this.appStoreService.showBrowserPrint = false;
				// 					this.appStoreService.winPaid = 0;
				// 					this.goToCheckPage();
				// 					this.dialogInfoService.hideAll();
				// 				}
				// 			}
				// 		}, {
				// 			hideOnClick: false
				// 		}
				// 	);
				// } else {
				// 	this.dialogInfoService.showOneButtonInfo('dialog.complete', 'dialog.win_pay_ticket_ok_info', {
				// 		click: this.goToCheckPage.bind(this),
				// 		text: 'dialog.dialog_button_continue'
				// 	});
				//
				// }

				this.totalCheckService.payment(request, barcode, amount);
				// this.dialogInfoService.hideAll();
				//
				// this.router.navigate([URL_LOTTERIES]).then();
				this.dialogInfoService.showOneButtonInfo('dialog.complete', 'dialog.win_pay_ticket_ok_info', {
					click: this.goToCheckPage.bind(this),
					text: 'dialog.dialog_button_continue'
				});
			})
			.finally(() => {
				this.isRequestButtonDisabled = false;
			})
			.catch((error: IError) => {
				Logger.Log.e('TicketsService', 'WinPayResp got error: code: %s, message: ', error.code, error.message)
					.console();
				if ((error.code === 4313) || (error.code === 4318)) {
					this.dialogInfoService.showOneButtonError(new DialogError(error.code, error.message, error.messageDetails), {
						click: () => this.logoutService.logoutOperator(),
						text: 'dialog.dialog_button_continue'
					});
				} else {
					if (error.code === ErrorCode.IncorrectSMS) {
						this.appStoreService.smsCodeError = true;
						this.dialogInfoService.hideAll();
					} else {
						let err: NetError | DialogError;
						err = (error instanceof NetError)
							? error
							: new DialogError(ErrorCode.TicketPayment, 'dialog.ticket_payment_error');
						this.dialogInfoService.showOneButtonError(err, {
							click: this.goToCheckPage.bind(this),
							text: 'dialog.dialog_button_continue'
						});
					}
				}
			});
	}

	/**
	 * Выполнить валидацию билета и в случае успешного исхода перейти на формы выплаты выигрыша.
	 *
	 * @param T Тип запроса.
	 * @param barcode Штрихкод билета.
	 * @param winBarCode Штрихкод выигрыша.
	 * @param response Ответ от сервера.
	 * @param result Результат валидации.
	 * @param isPaper Признак бумажной лотереи.
	 */
	private validateAndMoveToPayment<T>(
		T,
		barcode: string,
		winBarCode: string,
		response: WinCheckResp | WinCheckPaperResp,
		result: Subject<boolean>,
		isPaper: boolean
	): Subject<boolean> {
		Logger.Log.i('TicketsService', `validateAndMoveToPayment -> response will be validated`)
			.console();

		validateResponseSync<T>(T, response, {
			onSucceed: () => {
				this.winCheck = {
					win_ticket_sum: stringFromNumberCurrencyFormat(response.win_ticket_sum),
					win_sum: stringFromNumberCurrencyFormat(response.win_sum),
					win_allow: response.win_allow === 1,
					is_win: response.win_ticket_sum > 0,
					barcode,
					winBarCode,
					reason: response.reason_descr,
					isPaper,
					additional_info: response.additional_info,
					document_id: response.document_id || undefined
				};

				// Может потребоваться СМС-проверка
				this.winCheck.sms_required = response.player_auth === PlayerAuth.SMS;
				// И может прийти номер телефона игрока
				this.winCheck.player_phone = response.player_phone ? response.player_phone : '';

				// проверить на наличие бонусной выплаты (по чеку "Авто Лото")
				if (Array.isArray(response.extra_win) && response.extra_win.length > 0) {
					const ew: IExtraWin = response.extra_win[0];

					if (ew.win_allow === '1') {
						this.winCheck.bonusId = ew.id;
						this.goToBonusPaymentPage();
						result.next(true);

						return;
					}

					this.winCheck.win_allow = false;
				}

				// Может прийти детализация выигрыша по билетам
				if (response.win_tickets_details) {
					this.winCheck.win_tickets_details = response.win_tickets_details.map(v => {
						v.win_sum = v.win_sum / 100;
						if (!!v.win_ticket_sum) {
							v.win_ticket_sum = v.win_ticket_sum / 100;
						}
						return v;
					});
				}

				// перейти на форму выплаты
				this.goToPaymentPage();
				result.next(true);
			},
			onFailed: (error: IError) => {
				Logger.Log.e('TicketsService', `response validation error: ${error.message}`)
					.console();

				result.next(false);
				throw error;
			}
		});

		return result;
	}

	/**
	 * Перейти на страницу выплаты по чеку.
	 */
	private goToPaymentPage(): void {
		this.router.navigate([URL_TICKETS, URL_PAYMENT])
			.catch(err => Logger.Log.e('TicketsService', `Can't navigate to payment page: ${err}`)
				.console());
	}

	/**
	 * Перейти на страницу регистрации бонуса по чеку "Авто Лото".
	 */
	private goToBonusPaymentPage(): void {
		this.router.navigate([URL_TICKETS, URL_BONUS_PAYMENT])
			.catch(err => Logger.Log.e('TicketsService', `Can't navigate to bonus payment page: ${err}`)
				.console());
	}

}
