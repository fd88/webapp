import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '@app/shared/shared.module';

import { TicketCheckComponent } from '@app/tickets/components/ticket-check/ticket-check.component';
import { TicketPaymentComponent } from '@app/tickets/components/ticket-payment/ticket-payment.component';
import { TicketsComponent } from '@app/tickets/components/tickets.component';
import { TicketsService } from '@app/tickets/services/tickets.service';
import { TicketsRoutingModule } from '@app/tickets/tickets-routing.module';
import { TicketBonusPaymentComponent } from './components/ticket-bonus-payment/ticket-bonus-payment.component';
import {CameraModule} from "../camera/camera.module";
import { TicketLoyaltyPaymentComponent } from './components/ticket-loyalty-payment/ticket-loyalty-payment.component';
import {UserLoyaltyAuthModule} from "../user-loyalty-auth/user-loyalty-auth.module";

/**
 * Модуль предназначен для работы с билетами (проверка, отмена, выплата и т.д.).
 * Загружается по схеме с ленивой загрузкой.
 */
@NgModule({
	imports: [
		CommonModule,
		TicketsRoutingModule,
		SharedModule,
		FormsModule,
		CameraModule,
		UserLoyaltyAuthModule
	],
	declarations: [
		TicketsComponent,
		TicketCheckComponent,
		TicketPaymentComponent,
		TicketBonusPaymentComponent,
		TicketLoyaltyPaymentComponent
	],
	providers: [
		TicketsService
	]
})
export class TicketsModule {}
