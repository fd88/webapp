/**
 * Модель изменения, содержащая текст изменения на разных языках.
 */
export interface IChangeLogRowItem {
	/**
	 * Текст изменения на русском языке.
	 */
	ru: string;
	/**
	 * Текст изменения на украинском языке.
	 */
	ua: string;
	/**
	 * Текст изменения на английском языке.
	 */
	en: string;
}

/**
 * Модель со списком изменений по определенной версии.
 */
export interface IChangeLogItem {
	/**
	 * Версия
	 */
	version: string;
	/**
	 * Дата
	 */
	date: string;
	/**
	 * Что было добавлено
	 */
	added: Array<IChangeLogRowItem>;
	/**
	 * Что было изменено
	 */
	changed: Array<IChangeLogRowItem>;
	/**
	 * Что было исправлено
	 */
	fixed: Array<IChangeLogRowItem>;
}

/**
 * Модель объекта, содержащего список изменений от версии к версии.
 */
export interface IChangeLog {
	/**
	 * Массив записей
	 */
	items: Array<IChangeLogItem>;
}
