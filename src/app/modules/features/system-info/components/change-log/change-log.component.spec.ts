import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TranslateModule } from '@ngx-translate/core';

import { ChangeLogComponent } from '@app/sys-info/components/change-log/change-log.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { SharedModule } from '@app/shared/shared.module';
import { ChangeLogService } from '@app/sys-info/services/change-log.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from '@app/core/net/http/services/http.service';

describe('ChangeLogComponent', () => {
	let component: ChangeLogComponent;
	let fixture: ComponentFixture<ChangeLogComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
				ChangeLogComponent
			],
			providers: [
				LogService,
				ChangeLogService,
				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(async() => {
		fixture = TestBed.createComponent(ChangeLogComponent);
		component = fixture.componentInstance;
		component.changeLogService = TestBed.inject(ChangeLogService);
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onClickCloseHandler', () => {
		spyOn(component, 'onClickCloseHandler').and.callThrough();
		component.isOnlyLatestChanges = true;
		component.onClickCloseHandler(true);
		expect(component.onClickCloseHandler).toHaveBeenCalled();
	});

	it('test onClickCloseHandler 2', () => {
		spyOn(component, 'onClickCloseHandler').and.callThrough();
		component.isOnlyLatestChanges = true;
		component.onClickCloseHandler(false);
		expect(component.onClickCloseHandler).toHaveBeenCalled();
	});

	it('test onClickCloseHandler 3', () => {
		spyOn(component, 'onClickCloseHandler').and.callThrough();
		component.onClickCloseHandler();
		expect(component.onClickCloseHandler).toHaveBeenCalled();
	});

	it('test onTRSModeChange', () => {
		const ev = new Event('onTRSModeChange');
		const target = document.createElement('input');
		target.value = '2';
		component.onTRSModeChange({...ev, target});
		expect(component.trsMode).toEqual(2);
	});


});
