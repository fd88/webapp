import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { TranslateModule } from '@ngx-translate/core';

import { ChangeLogService } from '@app/sys-info/services/change-log.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { AuthService } from '@app/core/services/auth.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import {HttpClientModule} from "@angular/common/http";

describe('ChangeLogService', () => {
	let service: ChangeLogService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				AuthService,
				HttpService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(ChangeLogService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test showChangeLog method', () => {
		spyOn(service, 'showChangeLog').and.callThrough();
		service.showChangeLog();
		expect(service.showChangeLog).toHaveBeenCalled();
	});

	it('test hideDialog method', () => {
		spyOn(service, 'hideDialog').and.callThrough();
		service.hideDialog();
		expect(service.hideDialog).toHaveBeenCalled();
	});

	it('test isNewSoftwareVersions method', () => {
		expect(service.isNewSoftwareVersions('1.1.2', '1.1.1')).toBeTruthy();
	});

	it('test isNewSoftwareVersions method 2', () => {
		expect(service.isNewSoftwareVersions('1.1.1', '1.1.1')).toBeFalsy();
	});
});
