export enum StatusIconType {
	Display = 'display-icon',
	Printer = 'printer-icon',
	Scanner = 'scanner-icon'
}
