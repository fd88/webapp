import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusPanelComponent } from './status-panel.component';

describe('StatusPanelComponent', () => {
  let component: StatusPanelComponent;
  let fixture: ComponentFixture<StatusPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatusPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickPanelHandler', () => {
		spyOn(component, 'onClickPanelHandler').and.callThrough();
		component.onClickPanelHandler();
		expect(component.onClickPanelHandler).toHaveBeenCalled();
	});

	it('test iconTarget setter', () => {
		component.iconTarget = {
			nativeElement: document.createElement('div')
		};
		expect(component.leftPosition).toEqual(60);
	});
});
