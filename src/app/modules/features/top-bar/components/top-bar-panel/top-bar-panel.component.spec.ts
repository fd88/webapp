import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { TopBarPanelComponent } from "@app/top-bar/components/top-bar-panel/top-bar-panel.component";
import { PrinterIconComponent } from "../../../printer-status/components/printer-icon/printer-icon.component";
import { LogOutButtonComponent } from "@app/logout/components/log-out-button/log-out-button.component";
import { StatusPanelComponent } from "@app/top-bar/components/status-panel/status-panel.component";
import { HttpService } from "@app/core/net/http/services/http.service";
import { HttpClient } from "@angular/common/http";
import { TopBarService } from "@app/top-bar/services/top-bar.service";
import { HamburgerMenuService } from "@app/hamburger/services/hamburger-menu.service";
import { LotteriesService } from "@app/core/services/lotteries.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe('TopBarPanelComponent', () => {
	let component: TopBarPanelComponent | any;
	let fixture: ComponentFixture<TopBarPanelComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				TranslateModule.forRoot(),
				HttpClientTestingModule
			],
			declarations: [
				TopBarPanelComponent,
				PrinterIconComponent,
				LogOutButtonComponent,
				StatusPanelComponent
			],
			providers: [
				LogService,
				TopBarService,
				HamburgerMenuService,
				HttpService,
				LotteriesService,
				HttpClient
			]
		})
			.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TopBarPanelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onClickGoHomeHandler', () => {
		spyOn(component, 'onClickGoHomeHandler').and.callThrough();
		component.onClickGoHomeHandler();
		expect(component.onClickGoHomeHandler).toHaveBeenCalled();
	});

	it('test onClickToggleHamburgerMenuHandler', () => {
		spyOn(component, 'onClickToggleHamburgerMenuHandler').and.callThrough();
		component.onClickToggleHamburgerMenuHandler();
		expect(component.onClickToggleHamburgerMenuHandler).toHaveBeenCalled();
	});

	it('test onClickCommunicationIconHandler', () => {
		spyOn(component, 'onClickCommunicationIconHandler').and.callThrough();
		component.onClickCommunicationIconHandler();
		expect(component.onClickCommunicationIconHandler).toHaveBeenCalled();

		component.topBarService.statusPanelItem$$.next({});
		component.onClickCommunicationIconHandler();
		expect(component.onClickCommunicationIconHandler).toHaveBeenCalled();
	});


});
