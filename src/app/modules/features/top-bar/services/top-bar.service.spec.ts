import { TestBed } from '@angular/core/testing';
import { TopBarService } from '@app/top-bar/services/top-bar.service';

describe('TopBarService', () => {
	let service: TopBarService;
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
			],
			providers: [
			]
		});
		service = TestBed.inject(TopBarService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test hideStatusPanel', () => {
		service.hideStatusPanel();
		expect(service.statusPanelItem$$.value).toBeUndefined();
	});

});
