import { StatusIconType } from '../enums/status-icon-type.enum';
import { ElementRef } from '@angular/core';

/**
 * Интерфейс для элемента панели статуса.
 */
export interface StatusPanelItem {
	/**
	 * Тип иконки.
	 */
	iconType: StatusIconType;
	/**
	 * Текст статуса.
	 */
	statusText: string;
	/**
	 * Ссылка на элемент, в котором будет отображаться статус.
	 */
	iconTarget: ElementRef;
}
