import { Pipe, PipeTransform } from '@angular/core';

/**
 * Пайп для преобразования числа в положительное.
 */
@Pipe({
	name: 'makePositive'
})
export class MakePositivePipe implements PipeTransform {

	/**
	 * Преобразование числа в положительное.
	 * @param value число
	 */
	transform(value: number | string): number {
		let result = typeof value === 'string' ? +value : Math.abs(value);
		if (isNaN(result)) {
			result = 0;
		}

		return result;
	}

}
