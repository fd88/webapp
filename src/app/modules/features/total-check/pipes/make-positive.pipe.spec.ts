import { TestBed } from '@angular/core/testing';
import { MakePositivePipe } from "@app/total-check/pipes/make-positive.pipe";

describe('TotalCheckInfoPanelComponent', () => {
	let pipe: MakePositivePipe;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				MakePositivePipe
			]
		});
		pipe = TestBed.inject(MakePositivePipe);
	});

	it('should create', () => {
		expect(pipe).toBeTruthy();
	});

	it('test transform method', () => {
		expect(pipe.transform(-1)).toEqual(1);
		expect(pipe.transform('-1')).toEqual(-1);
		expect(pipe.transform('ololotrololo')).toEqual(0);
	});

});
