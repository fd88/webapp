import { TestBed, waitForAsync } from '@angular/core/testing';
import { TotalCheckStorageService } from '@app/total-check/services/total-check-storage.service';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ICancelableRequest } from '@app/core/net/http/api/types';
import { CancelableApiClient, URLSearchParams } from '@app/core/net/http/api/api-client';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { CancelFlag } from '@app/core/services/transaction/transaction-types';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import {DRAWS_FOR_GAME_ZABAVA} from "../../mocks/zabava-draws";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {LotteriesDraws} from "@app/core/services/store/draws";

describe('TotalCheckStorageService', () => {
	let service: TotalCheckStorageService | any;
	let appStoreService: AppStoreService | any;
	let request: ICancelableRequest;
	let response;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				CoreModule,
				HttpClientTestingModule
			],
			providers: [
				LogService,
				BarcodeReaderService,
				AppStoreService,
				HttpService,
				HttpClient
			]
		});

		TestBed.inject(LogService);

		service = TestBed.inject(TotalCheckStorageService);

		request = {
			"headers": new HttpHeaders(),
			"params": new URLSearchParams(),
			"trans_id": "402229668100",
			"url": EsapActions.ZabavaRegBet,
			"user_id": "10568",
			"sess_id": "9d443ad5-57c7-4a1a-863e-0286c447fde5",
			"gameCode": 3
		};

		response = {
			"client_id": 40000,
			"client_trans_id": "402229668100",
			"err_code": 0,
			"err_descr": "Операція виконана успішно.",
			"lang": "ua",
			"sid": "9d443ad5-57c7-4a1a-863e-0286c447fde5",
			"slogan": "ПРОСТО ГРАТИ - ЛЕГКО ВИГРАВАТИ",
			"ticket": [
				{
					"addgame": [
						{
							"code": 1,
							"comb": "251124091263134238532637",
							"name": "Парочка",
							"couples_idx": "1",
							"row0": "zabava/ticket8-c1-r64",
							"row1": "zabava/ticket8-c1-r65",
							"row2": "zabava/ticket8-c1-r56",
							"row3": "zabava/ticket8-c1-r67",
							"row4": "zabava/ticket8-c1-r68",
							"row5": "zabava/ticket8-c1-r69",
							"row6": "zabava/ticket8-c1-r70",
							"row7": "zabava/ticket8-c1-r71",
							"row8": "zabava/ticket8-c1-r72",
							"row9": "zabava/ticket8-c1-r73",
							"cd": [
								{
									"n": "2"
								},
								{
									"n": "5"
								},
								{
									"n": "1"
								},
								{
									"n": "1"
								},
								{
									"n": "2"
								},
								{
									"n": "4"
								},
								{
									"n": "0"
								},
								{
									"n": "9"
								},
								{
									"n": "1"
								},
								{
									"n": "2"
								},
								{
									"n": "6"
								},
								{
									"n": "3"
								},
								{
									"n": "1"
								},
								{
									"n": "3"
								},
								{
									"n": "4"
								},
								{
									"n": "2"
								},
								{
									"n": "3"
								},
								{
									"n": "8"
								},
								{
									"n": "5"
								},
								{
									"n": "3"
								},
								{
									"n": "2"
								},
								{
									"n": "6"
								},
								{
									"n": "3"
								},
								{
									"n": "7"
								}
							]
						},
						{
							"code": 2,
							"comb": "0085531287",
							"name": "Студія",
							"dig": [
								{
									"v": "0"
								},
								{
									"v": "0"
								},
								{
									"v": "8"
								},
								{
									"v": "5"
								},
								{
									"v": "5"
								},
								{
									"v": "3"
								},
								{
									"v": "1"
								},
								{
									"v": "2"
								},
								{
									"v": "8"
								},
								{
									"v": "7"
								}
							]
						}
					],
					"bet_count": 0,
					"bet_sum": "17.00",
					"description": "Билет Лото-Забава N 241",
					"draw_date": "2019-11-01 13:35:23",
					"draw_num": 1305,
					"game_comb": [
						{
							"game_field": "03283846631226445673131700606600193954731117385971",
							"row": [
								{
									"data": "0328384663",
									"num": [
										{
											"v": "03",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "28",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "8"
												}
											]
										},
										{
											"v": "38",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "8"
												}
											]
										},
										{
											"v": "46",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "6"
												}
											]
										},
										{
											"v": "63",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "3"
												}
											]
										}
									]
								},
								{
									"data": "1226445673",
									"num": [
										{
											"v": "12",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "2"
												}
											]
										},
										{
											"v": "26",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "6"
												}
											]
										},
										{
											"v": "44",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "4"
												}
											]
										},
										{
											"v": "56",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "6"
												}
											]
										},
										{
											"v": "73",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "3"
												}
											]
										}
									]
								},
								{
									"data": "1317006066",
									"num": [
										{
											"v": "13",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "17",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "00",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "60",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "66",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "6"
												}
											]
										}
									]
								},
								{
									"data": "0019395473",
									"num": [
										{
											"v": "00",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "19",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "9"
												}
											]
										},
										{
											"v": "39",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "9"
												}
											]
										},
										{
											"v": "54",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "4"
												}
											]
										},
										{
											"v": "73",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "3"
												}
											]
										}
									]
								},
								{
									"data": "1117385971",
									"num": [
										{
											"v": "11",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "1"
												}
											]
										},
										{
											"v": "17",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "38",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "8"
												}
											]
										},
										{
											"v": "59",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "9"
												}
											]
										},
										{
											"v": "71",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "1"
												}
											]
										}
									]
								}
							]
						},
						{
							"game_field": "09304357680625375561042100586102270053741427415375",
							"row": [
								{
									"data": "0930435768",
									"num": [
										{
											"v": "09",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "9"
												}
											]
										},
										{
											"v": "30",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "43",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "57",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "68",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "8"
												}
											]
										}
									]
								},
								{
									"data": "0625375561",
									"num": [
										{
											"v": "06",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "6"
												}
											]
										},
										{
											"v": "25",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "5"
												}
											]
										},
										{
											"v": "37",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "55",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "5"
												}
											]
										},
										{
											"v": "61",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "1"
												}
											]
										}
									]
								},
								{
									"data": "0421005861",
									"num": [
										{
											"v": "04",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "4"
												}
											]
										},
										{
											"v": "21",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "1"
												}
											]
										},
										{
											"v": "00",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "58",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "8"
												}
											]
										},
										{
											"v": "61",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "1"
												}
											]
										}
									]
								},
								{
									"data": "0227005374",
									"num": [
										{
											"v": "02",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "2"
												}
											]
										},
										{
											"v": "27",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "00",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "53",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "74",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "4"
												}
											]
										}
									]
								},
								{
									"data": "1427415375",
									"num": [
										{
											"v": "14",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "4"
												}
											]
										},
										{
											"v": "27",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "41",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "1"
												}
											]
										},
										{
											"v": "53",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "75",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "5"
												}
											]
										}
									]
								}
							]
						},
						{
							"game_field": "10003247700820405169052300496401163349721016404867",
							"row": [
								{
									"data": "1000324770",
									"num": [
										{
											"v": "10",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "00",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "32",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "2"
												}
											]
										},
										{
											"v": "47",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "7"
												}
											]
										},
										{
											"v": "70",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "0"
												}
											]
										}
									]
								},
								{
									"data": "0820405169",
									"num": [
										{
											"v": "08",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "8"
												}
											]
										},
										{
											"v": "20",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "40",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "51",
											"dig": [
												{
													"v": "5"
												},
												{
													"v": "1"
												}
											]
										},
										{
											"v": "69",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "9"
												}
											]
										}
									]
								},
								{
									"data": "0523004964",
									"num": [
										{
											"v": "05",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "5"
												}
											]
										},
										{
											"v": "23",
											"dig": [
												{
													"v": "2"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "00",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "49",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "9"
												}
											]
										},
										{
											"v": "64",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "4"
												}
											]
										}
									]
								},
								{
									"data": "0116334972",
									"num": [
										{
											"v": "01",
											"dig": [
												{
													"v": "0"
												},
												{
													"v": "1"
												}
											]
										},
										{
											"v": "16",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "6"
												}
											]
										},
										{
											"v": "33",
											"dig": [
												{
													"v": "3"
												},
												{
													"v": "3"
												}
											]
										},
										{
											"v": "49",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "9"
												}
											]
										},
										{
											"v": "72",
											"dig": [
												{
													"v": "7"
												},
												{
													"v": "2"
												}
											]
										}
									]
								},
								{
									"data": "1016404867",
									"num": [
										{
											"v": "10",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "16",
											"dig": [
												{
													"v": "1"
												},
												{
													"v": "6"
												}
											]
										},
										{
											"v": "40",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "0"
												}
											]
										},
										{
											"v": "48",
											"dig": [
												{
													"v": "4"
												},
												{
													"v": "8"
												}
											]
										},
										{
											"v": "67",
											"dig": [
												{
													"v": "6"
												},
												{
													"v": "7"
												}
											]
										}
									]
								}
							]
						}
					],
					"id": "629396707",
					"mac_code": "003013050000024168725234",
					"portion_id": 0,
					"regdate": "2019-10-16 10:07:21",
					"ticket_num": 241,
					"ticket_snum": 68725234,
					"couples_cnt": "1",
					"bgimg": {
						"p1": "zabava/ticket8-f3",
						"p2": "s5",
						"p3": "r1-r.png"
					},
					"field": "3"
				}
			],
			"ticket_templ_url": "/esap/templates/zabava-ticket-0-utf8-21.xml",
			"time_stamp": "2019-10-16 10:07:21",
			"user_id": "10568"
		};

	});

	beforeEach(waitForAsync(async() => {
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.Zabava, DRAWS_FOR_GAME_ZABAVA.lottery);
	}));

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test register, clearAmount and clearHistory methods', () => {
		service.register(request, response);
		expect(service.operationsList$$.value.length).toEqual(1);

		const transaction = {
			getNumberOfPrinted: () => 1
		};
		service.register(request, response, transaction);
		expect(service.operationsList$$.value.length).toEqual(2);

		response.ticket = undefined;
		service.register(request, response);
		expect(service.operationsList$$.value.length).toEqual(2);

		service.register(request as CancelableApiClient, response);
		service.clearAmount();
		expect(service.operationsList$$.value.length).toEqual(0);

		service.clearHistory();
		expect(service.operationsList$$.value.length).toEqual(0);
	});

	it('test cancel method', () => {
		service.register(request, response);
		service.cancel({
			cancelFlag: CancelFlag.Manual_PrintError,
			store: {
				params: {
					trans_id: "402229668100"
				}
			}
		});
		expect(service.operationsList$$.value.length).toEqual(2);

		service.register(request, response);
		service.cancel({
			cancelFlag: CancelFlag.Unknown_1,
			store: {
				params: {
					trans_id: "402229668101"
				}
			}
		});
		expect(service.operationsList$$.value.length).toEqual(3);

		service.register(request, response);
		service.cancel({
			cancelFlag: CancelFlag.Manual_PrintError,
			store: {
				params: {
					trans_id: "402229668101"
				}
			}
		});
		expect(service.operationsList$$.value.length).toEqual(4);
	});

	it('test detectGameNameAndCode method', () => {
		expect(service.detectGameNameAndCode(response.ticket)).toEqual({
			gameName: undefined, gameCode: 0
		});

		service.barcodeReaderService = {
			determineBarcodeType: () => {
				return {
					detectedLotteryInfo: {
						lott_name: 'Zabava',
					},
					detectedGameCode: LotteryGameCode.Zabava
				}
			}
		};
		expect(service.detectGameNameAndCode(response.ticket)).toEqual({
			gameName: 'Zabava', gameCode: LotteryGameCode.Zabava
		});

	});

	it('test paymentAndRegisterBonus method', () => {
		const req = {
			"headers": {
				"normalizedNames": {},
				"lazyUpdate": null,
				"headers": {}
			},
			"params": new Map([[EsapParams.BARCODE, '0729003104039950']]),
			"trans_id": "402236082948",
			"url": "https://dev.cs.emict.net/EInstantRegSrvDev",
			"user_id": 10568,
			"sess_id": "971367a8-a6c0-4aed-b31d-8c3c7cd035f2",
			"gameCode": 162
		};
		const resp = {
			"client_id": 40000,
			"client_trans_id": "402236082948",
			"err_code": 0,
			"err_descr": "Операція виконана успішно.",
			"lang": "ua",
			"oper_code": 100605,
			"term_code": 40000,
			"ticket": [
				{
					"bet_sum": "100.00",
					"date_reg": "2019-10-16 17:04:59",
					"description": "Білет N 0729-002225-010",
					"game_name": "АВТО-ЛОТО",
					"gfield": [
						{
							"img": "",
							"sign": "14"
						},
						{
							"img": "",
							"sign": "16"
						},
						{
							"img": "",
							"sign": "01"
						},
						{
							"img": "",
							"sign": "07"
						},
						{
							"img": "",
							"sign": "06"
						},
						{
							"img": "",
							"sign": "20"
						},
						{
							"img": "",
							"sign": "15"
						},
						{
							"img": "",
							"sign": "04"
						},
						{
							"img": "",
							"sign": "13"
						},
						{
							"img": "",
							"sign": "17"
						},
						{
							"img": "",
							"sign": "06"
						},
						{
							"img": "",
							"sign": "02"
						},
						{
							"img": "",
							"sign": "04"
						},
						{
							"img": "",
							"sign": "15"
						},
						{
							"img": "",
							"sign": "20"
						},
						{
							"img": "",
							"sign": "05"
						},
						{
							"img": "",
							"sign": "16"
						},
						{
							"img": "",
							"sign": "17"
						},
						{
							"img": "",
							"sign": "11"
						},
						{
							"img": "",
							"sign": "01"
						},
						{
							"img": "G2T1",
							"sign": "15"
						},
						{
							"img": "G2T1",
							"sign": "06"
						},
						{
							"img": "G2T1",
							"sign": "02"
						},
						{
							"img": "G2T1",
							"sign": "17"
						},
						{
							"img": "G2T1",
							"sign": "03"
						},
						{
							"img": "G2T1",
							"sign": "11"
						},
						{
							"img": "G2T1",
							"sign": "08"
						},
						{
							"img": "G2T1",
							"sign": "10"
						},
						{
							"img": "G2T1",
							"sign": "07"
						},
						{
							"img": "G2T2",
							"sign": "18"
						},
						{
							"img": "G2T2",
							"sign": "16"
						},
						{
							"img": "G2T2",
							"sign": "07"
						},
						{
							"img": "G2T2",
							"sign": "05"
						},
						{
							"img": "G2T2",
							"sign": "19"
						},
						{
							"img": "G2T2",
							"sign": "02"
						},
						{
							"img": "G2T2",
							"sign": "09"
						},
						{
							"img": "G2T2",
							"sign": "12"
						},
						{
							"img": "G2T2",
							"sign": "11"
						}
					],
					"gfield_cnt": 19,
					"id": 298334580,
					"interactive": {
						"code": ""
					},
					"mac_code": "0729002225010223",
					"next_game": "",
					"portion_id": 0,
					"series_name": "01",
					"series_num": 1,
					"slogan": "",
					"ticket_num": "0729-002225-010",
					"wfield": [
						{
							"img": "",
							"sign": "16"
						},
						{
							"img": "",
							"sign": "08"
						},
						{
							"img": "",
							"sign": "18"
						},
						{
							"img": "",
							"sign": "15"
						},
						{
							"img": "",
							"sign": "02"
						},
						{
							"img": "",
							"sign": "10"
						},
						{
							"img": "",
							"sign": "12"
						},
						{
							"img": "",
							"sign": "04"
						},
						{
							"img": "",
							"sign": "05"
						},
						{
							"img": "",
							"sign": "11"
						},
						{
							"img": "",
							"sign": "20"
						},
						{
							"img": "",
							"sign": "01"
						}
					],
					"wfield_cnt": 6
				}
			],
			"ticket_templ_url": "esap/templates/auto-lotto-ticket-utf8-04.xml",
			"time_stamp": "2019-10-16 17:04:59"
		};
		service.paymentAndRegisterBonus(req, resp);
		expect(service.operationsList$$.value.length).toEqual(2);

	});

	it('test paymentAndRegisterBonus method', () => {
		request.trans_id = '402250826545';
		service.payment(request, '0626000595083686', 1000);
		expect(service.operationsList$$.value.length).toEqual(1);
	});
});
