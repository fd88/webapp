import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TotalCheckInfoPanelComponent } from './total-check-info-panel.component';
import {
	TotalCheckListState,
	TotalCheckStorageService
} from '@app/total-check/services/total-check-storage.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ElementRef } from "@angular/core";
import { ICancelableRequest } from "@app/core/net/http/api/types";
import { LotteryGameCode } from "@app/core/configuration/lotteries";
import {TransactionAction} from "@app/util/utils";
import {OPERATIONS_LIST} from "../../../mocks/mocks";
import {environment} from "@app/env/environment";

describe('TotalCheckInfoPanelComponent', () => {
	let component: TotalCheckInfoPanelComponent;
	let fixture: ComponentFixture<TotalCheckInfoPanelComponent>;
	let tcsService: TotalCheckStorageService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot(),
				SharedModule
			],
			declarations: [
				TotalCheckInfoPanelComponent
			],
			providers: [
				LogService,
				BarcodeReaderService,
				TotalCheckStorageService,
				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService)
		tcsService = TestBed.inject(TotalCheckStorageService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TotalCheckInfoPanelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test trackByOperationFn method', () => {
		expect(component.trackByOperationFn(1, {
			timestamp: 12345678,
			gameName: 'test',
			gameCode: LotteryGameCode.Zabava,
			operationType: TransactionAction.PAYMENT,
			operationTypeLabel: 'ru',
			count: 1,
			summa: 5,
			operationResult: 'ru',
			request: {} as ICancelableRequest
		})).toEqual('112345678');
	});

	it('test onClearAmountHandler handler', () => {
		component.tcsService.totalCheckAmount$$.next(123);
		component.onClearAmountHandler();
		expect(component.tcsService.totalCheckAmount$$.value).toEqual(0);
	});

	it('test onClickUpDownHandler handler', () => {
		tcsService.operationListState$$.next(TotalCheckListState.Expanded);
		component.onClickUpDownHandler();
		expect(tcsService.operationListState$$.value).toEqual(TotalCheckListState.Collapsed);
	});

	it('test onClickUpDownHandler handler 2', () => {
		tcsService.operationListState$$.next(TotalCheckListState.Collapsed);
		component.onClickUpDownHandler();
		expect(tcsService.operationListState$$.value).toEqual(TotalCheckListState.Expanded);
	});

	it('test onSwipeHandler', () => {
		component.onSwipeHandler({
			direction: Hammer.DIRECTION_DOWN
		});
		expect(tcsService.operationListState$$.value).toEqual(TotalCheckListState.Collapsed);
	});

	it('test onSwipeHandler 2', () => {
		component.onSwipeHandler({
			direction: Hammer.DIRECTION_UP
		});
		expect(tcsService.operationListState$$.value).toEqual(TotalCheckListState.Expanded);
	});

	it('test onSwipeHandler 2', () => {
		environment.mockData = false;
		tcsService.operationsList$$.next([]);
		component.ngOnInit();
		environment.mockData = true;
		expect(tcsService.operationListState$$.value).toEqual(TotalCheckListState.Collapsed);
	});
});
