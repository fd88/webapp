import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { DATE_TEMPLATE_HH_MM_SS } from '@app/util/utils';
import {
	ITotalCheckOperation,
	TotalCheckListState,
	TotalCheckStorageService
} from '@app/total-check/services/total-check-storage.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { environment } from '@app/env/environment';
import { OPERATIONS_LIST } from '../../../mocks/mocks';
import { URL_CHECK, URL_TICKETS } from '@app/util/route-utils';
import { TransactionAction } from '@app/util/utils';

/**
 * Перечисление возможной высоты списка операций.
 */
export enum TotalCheckListSize {
	/**
	 * Малый размер списка.
	 */
	Small = 120,
	/**
	 * Большой размер списка.
	 */
	Big = 242
}

/**
 * Компонент для отображения баланса по выполненным операциям на терминале.
 * Состоит из списка операций, итоговой суммы и кнопки сброса.
 */
@Component({
	selector: 'app-total-check-info-panel',
	templateUrl: './total-check-info-panel.component.html',
	styleUrls: ['./total-check-info-panel.component.scss']
})
export class TotalCheckInfoPanelComponent implements OnInit, OnDestroy {
	/**
	 * Список возможных действий с лотереями
	 */
	readonly TransactionAction = TransactionAction;
	/**
	 * Список состояний списка операций
	 */
	readonly TotalCheckListState  = TotalCheckListState;
	/**
	 * Формат даты для отображения в списке операций
	 */
	readonly OPERATION_DATE = DATE_TEMPLATE_HH_MM_SS;
	/**
	 * Ссылка на страницу проверки билетов
	 */
	readonly ticketsCheck = `/${URL_TICKETS}/${URL_CHECK}`;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Наблюдаемая переменная для уничтожения всех подписок
	 */
	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {TotalCheckStorageService} tcsService Сервис для работы с хранилищем итогового чека.
	 * @param appStoreService Сервис для работы с хранилищем приложения.
	 */
	constructor(
		readonly tcsService: TotalCheckStorageService,
		private readonly appStoreService: AppStoreService
	) {}

	/**
	 * Развернут ли список операций.
	 */
	get isExpanded(): boolean {
		return this.tcsService.operationListState$$.value === TotalCheckListState.Expanded;
	}

	/**
	 * Слушатель нажатия на кнопку очистки итоговой суммы чека.
	 */
	onClearAmountHandler(): void {
		this.tcsService.clearAmount();
	}

	/**
	 * Слушатель нажатия на кнопку свернуть/развернуть журнал операций.
	 */
	onClickUpDownHandler(): void {
		if (this.tcsService.operationListState$$.value === TotalCheckListState.Collapsed) {
			this.tcsService.operationListState$$.next(TotalCheckListState.Expanded);
		} else {
			this.tcsService.operationListState$$.next(TotalCheckListState.Collapsed);
		}
	}

	/**
	 * Обработчик свайпа вверх/вниз.
	 * @param event Событие свайпа.
	 */
	onSwipeHandler(event): void {
		if (event.direction === Hammer.DIRECTION_DOWN) {
			this.tcsService.operationListState$$.next(TotalCheckListState.Collapsed);
		} else if (event.direction === Hammer.DIRECTION_UP) {
			this.tcsService.operationListState$$.next(TotalCheckListState.Expanded);
		}
	}

	/**
	 * Функция для отслеживания изменений в списке операций.
	 * @param index Индекс элемента в списке.
	 * @param item Элемент списка.
	 */
	trackByOperationFn = (index, item: ITotalCheckOperation) => `${index}${item.timestamp}`;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		// Автологин - чисто для себя, для верстки
		// Если он есть, то показать тестовые данные в калькуляторе
		// if (environment.mockData) {
		// 	this.tcsService.operationsList$$.next(OPERATIONS_LIST);
		// }

		if (!this.tcsService.operationsList$$.value.length) {
			this.tcsService.operationListState$$.next(TotalCheckListState.Collapsed);
		}
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}

}
