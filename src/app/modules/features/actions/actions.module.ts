import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';

import { ActionsRoutingModule } from '@app/actions/actions-routing.module';
import { ActionsComponent } from '@app/actions/components/actions/actions.component';
import { ActionsService } from '@app/actions/services/actions.service';
import { ActionItemComponent } from '@app/actions/components/action-item/action-item.component';
import { ActionButtonComponent } from '@app/actions/components/action-button/action-button.component';
import { MegalotLotteryService } from '@app/megalot/services/megalot-lottery.service';
import { ActionBannerRowComponent } from '@app/actions/components/action-banner-row/action-banner-row.component';
import { ActionBannerComponent } from '@app/actions/components/action-banner/action-banner.component';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';

/**
 * Модуль раздела "Акции".
 */
@NgModule({
	imports: [
		CommonModule,
		ActionsRoutingModule,
		SharedModule
	],
	declarations: [
		ActionsComponent,
		ActionItemComponent,
		ActionButtonComponent,
		ActionBannerRowComponent,
		ActionBannerComponent
	],
	providers: [
		ActionsService,
		MegalotLotteryService,
		InstantLotteryService
	]
})
export class ActionsModule {}
