import { TestBed } from '@angular/core/testing';
import { ActionsService } from '@app/actions/services/actions.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import {IActionParameters, IActionsButton, IActionsListItem} from '@app/actions/interfaces/iactions-settings';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { TranslateModule } from '@ngx-translate/core';
import { LogOutService } from '@app/logout/services/log-out.service';
import { LotteriesService } from '@app/core/services/lotteries.service';

describe('ActionsService', () => {
	let service: ActionsService | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				AppStoreService,
				InstantLotteryService,
				HttpService,
				LogOutService,
				LotteriesService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(ActionsService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test navigateBySelectedAction method', () => {
		let action: IActionsListItem;
		let button: IActionsButton;
		spyOn(service, 'navigateBySelectedAction').and.callThrough();

		action = {
			buttons: [
				{
					labelKey: "actions.zabava_ticket_pairs",
					gameCode: LotteryGameCode.Zabava,
					groupCode: 0,
					data: {
						summ: 20,
						drawCount: 1,
						betCount: 1,
						pairs: 2
					}
				},
				{
					labelKey: "actions.zabava_tickets_pairs",
					gameCode: LotteryGameCode.Zabava,
					groupCode: 0,
					data: {
						summ: 50,
						drawCount: 1,
						betCount: 2,
						pairs: 3
					}
				}
			]
		};
		service.navigateBySelectedAction(action, action.buttons[0]);
		expect(service.navigateBySelectedAction).toHaveBeenCalled();

		action = {
			buttons: [
				{
					labelKey: "actions.megalot_bet_megaball",
					gameCode: LotteryGameCode.MegaLot,
					groupCode: 0,
					data: {
						summ: 20,
						drawCount: 1,
						betCount: 4,
						ballCount: 6,
						megaBallCount: 1
					}
				},
				{
					labelKey: "actions.megalot_bets_megaball",
					gameCode: LotteryGameCode.MegaLot,
					groupCode: 0,
					data: {
						summ: 50,
						drawCount: 1,
						betCount: 10,
						ballCount: 6,
						megaBallCount: 1
					}
				}
			]
		};
		service.navigateBySelectedAction(action, action.buttons[0]);
		expect(service.navigateBySelectedAction).toHaveBeenCalled();

		action = {
			buttons: [
				{
					labelKey: "actions.tiptop_tip_tickets",
					gameCode: LotteryGameCode.Tip,
					groupCode: 0,
					data: {
						summ: 20,
						betCount: 10
					}
				},
				{
					labelKey: "actions.tiptop_top_tickets",
					gameCode: LotteryGameCode.Top,
					groupCode: 0,
					data: {
						summ: 50,
						betCount: 10,
					}
				}
			]
		};
		service.navigateBySelectedAction(action, action.buttons[0]);
		expect(service.navigateBySelectedAction).toHaveBeenCalled();

		action = {
			buttons: [
				{
					labelKey: "4 шт, “Весела Скарбничка”",
					groupCode: LotteriesGroupCode.EInstant,
					gameCode: 140,
					data: {
						summ: 20,
						betCount: 4
					}
				},
				{
					labelKey: "5 шт, “Морской бой”",
					groupCode: LotteriesGroupCode.EInstant,
					gameCode: 107,
					data: {
						summ: 50,
						betCount: 5
					}
				}
			]
		};
		service.navigateBySelectedAction(action, action.buttons[0]);
		expect(service.navigateBySelectedAction).toHaveBeenCalled();
	});

	it('test requestActionList method', () => {
		spyOn(service, 'requestActionList');
		service.actionsSettings$$.next({
			bannersList: [],
			actionsList: []
		});
		service.requestActionList();
		expect(service.requestActionList).toHaveBeenCalled();

		service.actionsSettings$$.next(undefined);
		service.requestActionList();
		expect(service.requestActionList).toHaveBeenCalled();
	});

	it('test requestActionList method', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery(undefined);
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 2', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.Zabava,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 3', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.MegaLot,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 4', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.Tip,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 5', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.Top,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	xit('test requestActionList method 6', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.Gonka,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 7', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.TML_BML,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 8', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.Sportprognoz,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});

	it('test requestActionList method 9', () => {
		spyOn(service, 'navigateByLottery').and.callThrough();
		service.navigateByLottery({
			gameCode: LotteryGameCode.Kare,
			groupCode: LotteriesGroupCode.Regular,
			data: {
				summ: 10000,
				drawCount: 1,
				betCount: 1,
				pairs: 1,
				ticketSumm: 10000
			}
		});
		expect(service.navigateByLottery).toHaveBeenCalled();
	});
});
