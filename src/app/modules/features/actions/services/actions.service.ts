import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';

import { Logger } from '@app/core/net/ws/services/log/logger';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';

import { selectActualDraws } from '@app/util/utils';
import {
	createUrlParam,
	PARAM_ACTION_BET,
	URL_GONKA,
	URL_INIT,
	URL_INSTANT,
	URL_KARE,
	URL_LOTTERIES,
	URL_MEGALOT,
	URL_REGISTRY,
	URL_SPORTPROGNOZ,
	URL_TIPTOP,
	URL_TMLBML,
	URL_ZABAVA
} from '@app/util/route-utils';

import { IActionsButton, IActionsListItem, IActionsSettings } from '@app/actions/interfaces/iactions-settings';
import { IInstantGameData, InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import {environment} from "@app/env/environment";
import {ACTIONS_SETTINGS} from "../../mocks/actions-settings";

/**
 * Сервис модуля "Акции".
 */
@Injectable({
	providedIn: 'root'
})
export class ActionsService {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Конфигурация акций, на основе которой они будут отображены в соответствующем разделе меню.
	 */
	readonly actionsSettings$$ = new BehaviorSubject<IActionsSettings>(undefined);

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор сервиса.
	 *
	 * @param {HttpClient} httpClient Сервис для выполнения HTTP-запросов.
	 * @param {Router} router Сервис маршрутизации.
	 * @param {AppStoreService} appStoreService Сервис хранилища приложения.
	 * @param {InstantLotteryService} instantLotteryService Сервис для работы с ЭМЛ.
	 * @param {DialogContainerService} dialogInfoService Сервис для работы с диалоговыми окнами.
	 */
	constructor(
		private readonly httpClient: HttpClient,
		private readonly router: Router,
		private readonly appStoreService: AppStoreService,
		private readonly instantLotteryService: InstantLotteryService,
		private readonly dialogInfoService: DialogContainerService
	) {}

	/**
	 * Запросить список акций.
	 */
	requestActionList(): void {
		Logger.Log.i('ActionsService', `requestActionList`)
			.console();

		if (!this.actionsSettings$$.value) {
			if (environment.mockData) {
				this.processActSettingsResponse(ACTIONS_SETTINGS);
			} else {
				this.httpClient.get('assets/actions-settings.json')
					.subscribe(this.processActSettingsResponse.bind(this));
			}
		} else {
			this.actionsSettings$$.next({...this.actionsSettings$$.value});
		}
	}

	/**
	 * Перейти в окно регистрации ставки для выбранной акции.
	 *
	 * @param {IActionsListItem} action Модель выбранной лотереи с акциями.
	 * @param {IActionsButton} button Модель выбранной на кнопке акции.
	 * @param {boolean} navigationFromBanner Признак навигации по клику на банере.
	 */
	navigateBySelectedAction(action: IActionsListItem, button: IActionsButton, navigationFromBanner?: boolean): void {
		Logger.Log.i('ActionsService', `navigateBySelectedAction -> try to navigate for button: %s`, button)
			.console();

		if (!button) {
			return;
		}

		let bannerLottery: IInstantGameData;
		// определить URL игры
		let gameRegistryUrl: string;
		if (button.groupCode === LotteriesGroupCode.Regular) {
			if (Number.isInteger(button.gameCode)) {
				if (button.gameCode === LotteryGameCode.Zabava) {
					gameRegistryUrl = `${URL_ZABAVA}`;
				} else if (button.gameCode === LotteryGameCode.MegaLot) {
					gameRegistryUrl = `${URL_MEGALOT}`;
				} else if (button.gameCode === LotteryGameCode.Tip || button.gameCode === LotteryGameCode.Top) {
					gameRegistryUrl = `${URL_TIPTOP}`;
				}
			}
		} else if (button.groupCode === LotteriesGroupCode.EInstant || button.groupCode === LotteriesGroupCode.VBL) {
			gameRegistryUrl = `${URL_INSTANT}`;
		}

		if (!!gameRegistryUrl && !!button.data) {
			const param = {queryParams: createUrlParam(PARAM_ACTION_BET, JSON.stringify(button))};
			let path: Array<string>;
			path = [URL_LOTTERIES, gameRegistryUrl, URL_REGISTRY];

			if (navigationFromBanner) {
				path = [URL_LOTTERIES, gameRegistryUrl, URL_INIT, `${button.groupCode}`];
				this.instantLotteryService.setGroupId(LotteriesGroupCode.EInstant);
				bannerLottery = this.instantLotteryService.lotteries.find(f => f.code === button.gameCode);
			}

			if (!navigationFromBanner || (navigationFromBanner && bannerLottery)) {
				this.router.navigate(path, param)
					.catch(err => Logger.Log.e('ActionsService', `navigateBySelectedAction -> can't navigate to lotteries screen: ${err}`)
						.console());
			} else {
				const msg = new DialogError(ErrorCode.LotteryNotAssigned, 'dialog.lottery_not_assigned');
				this.dialogInfoService.showOneButtonError(msg, {text: 'dialog.dialog_button_continue'});
			}
		}
	}

	/**
	 * Перейти в окно регистрации лотереи.
	 *
	 * @param {IActionsButton} button Модель выбранной на кнопке акции (любой, 1 из 2х).
	 */
	navigateByLottery(button: IActionsButton): void {
		Logger.Log.i('ActionsService', `navigateByLottery -> try to navigate from logo`)
			.console();

		if (!button) {
			return;
		}

		// определить URL игры
		let gameRegistryUrl: string;
		if (button.groupCode === LotteriesGroupCode.Regular) {
			if (Number.isInteger(button.gameCode)) {
				if (button.gameCode === LotteryGameCode.Zabava) {
					gameRegistryUrl = `${URL_ZABAVA}`;
				} else if (button.gameCode === LotteryGameCode.MegaLot) {
					gameRegistryUrl = `${URL_MEGALOT}`;
				} else if (button.gameCode === LotteryGameCode.Tip || button.gameCode === LotteryGameCode.Top) {
					gameRegistryUrl = `${URL_TIPTOP}`;
				} else if (button.gameCode === LotteryGameCode.Gonka) {
					gameRegistryUrl = `${URL_GONKA}`;
				} else if (button.gameCode === LotteryGameCode.TML_BML) {
					gameRegistryUrl = `${URL_TMLBML}`;
				} else if (button.gameCode === LotteryGameCode.Sportprognoz) {
					gameRegistryUrl = `${URL_SPORTPROGNOZ}`;
				} else if (button.gameCode === LotteryGameCode.Kare) {
					gameRegistryUrl = `${URL_KARE}`;
				}
			}
		} else if (button.groupCode === LotteriesGroupCode.EInstant || button.groupCode === LotteriesGroupCode.VBL) {
			gameRegistryUrl = `${URL_INSTANT}`;
		}

		if (!!gameRegistryUrl) {
			const path = [URL_LOTTERIES, gameRegistryUrl, URL_INIT];
			if (button.groupCode !== LotteriesGroupCode.Regular) {
				path.push(button.groupCode.toString());
			}

			this.router.navigate(path)
				.catch(err => Logger.Log.e('ActionsService', `navigateByLottery -> can't navigate to lotteries screen: ${err}`)
					.console());
		}
	}

	/**
	 * Обработать ответ на запрос списка акций.
	 * @param response Ответ на запрос списка акций.
	 * @private
	 */
	private processActSettingsResponse(response: IActionsSettings): void {
		Logger.Log.i('ActionsService', `requestActionList -> response: %s`, response)
			.console();

		const actions = this.parseConfiguration(response);
		this.actionsSettings$$.next(actions);
	}

	// -----------------------------
	//  Private functions
	// -----------------------------
	/**
	 * Разобрать конфигурацию в ответе на запрос списка акций.
	 * @param response Ответ на запрос списка акций.
	 * @private
	 */
	private parseConfiguration(response: IActionsSettings): IActionsSettings {
		Logger.Log.i('ActionsService', `parseConfiguration`)
			.console();

		const a = response.actionsList
			.map(m => m.buttons)
			.reduce((p, c) => p.concat(c), []);
		const b = response.bannersList
			.map(m => m.buttons)
			.reduce((p, c) => p.concat(c), []);
		const buttons = [...a, ...b];
		buttons.forEach(f => {
			const draws = selectActualDraws(this.appStoreService.Draws, f.gameCode);

			if (f.data) {
				const betSumm = Number.isFinite(f.data.summ) && Number.isFinite(f.data.betCount) ? f.data.summ / f.data.betCount : 0;
				const draw = Array.isArray(draws) ? draws.find(ff => +ff.draw.bet_sum === betSumm) : undefined;
				f.disabled = !Array.isArray(draws) || draws.length === 0 || (f.groupCode !== LotteriesGroupCode.Regular && !draw);
			}
		});

		return {...response};
	}
}
