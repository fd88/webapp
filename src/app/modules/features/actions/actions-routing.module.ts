import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '@app/core/guards/auth.guard';

import { ActionsComponent } from '@app/actions/components/actions/actions.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

/**
 * Список маршрутов для модуля "Акции".
 */
const routes: Routes = [
	{
		path: '',
		component: ActionsComponent,
		canActivate: [
			AuthGuard
		],
		canDeactivate: [
			NavigationGuard
		],
		children: [
			// {
			// 	path: URL_INIT,
			// 	component: KareLotteryInitComponent,
			// 	canActivate: [
			// 		AuthGuardService
			// 	]
			// },
			// {
			// 	path: URL_SCANNER,
			// 	component: KareCouponComponent,
			// 	canActivate: [
			// 		AuthGuardService
			// 	]
			// },
			// {
			// 	path: URL_REGISTRY,
			// 	component: CheckInformationComponent,
			// 	resolve: {
			// 		registry: KareRegistryResolver
			// 	},
			// 	canActivate: [
			// 		AuthGuardService
			// 	]
			// },
			// {
			// 	path: URL_RESULTS,
			// 	component: KareResultsComponent,
			// 	canActivate: [
			// 		AuthGuardService
			// 	]
			// }
		]
	}
];

/**
 * Модуль маршрутизации для раздела "Акции".
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	],
	providers: [
	]
})
export class ActionsRoutingModule {}
