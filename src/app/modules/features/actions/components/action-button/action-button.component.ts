import { Component, Input } from '@angular/core';
import { IActionsButton, IActionsListItem } from '@app/actions/interfaces/iactions-settings';
import { ActionsService } from '@app/actions/services/actions.service';

/**
 * Компонент для отображения кнопки в элементе списка акций
 */
@Component({
	selector: 'app-action-button',
	templateUrl: './action-button.component.html',
	styleUrls: ['./action-button.component.scss']
})
export class ActionButtonComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Акция, которая привязана к кнопке
	 */
	@Input()
	action: IActionsListItem;

	/**
	 * Кнопка в элементе списка акций
	 */
	@Input()
	button: IActionsButton;

	// -----------------------------
	//  Public properties
	// -----------------------------

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Конструктор компонента
	 * @param actionsService Сервис для работы с акциями
	 */
	constructor(
		readonly actionsService: ActionsService
	) {}

	/**
	 * Обработчик клика по кнопке акции
	 */
	onActionClickHandler(): void {
		this.actionsService.navigateBySelectedAction(this.action, this.button);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
