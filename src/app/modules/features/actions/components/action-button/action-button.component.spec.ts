import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionButtonComponent } from './action-button.component';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ActionsService } from '@app/actions/services/actions.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { LogService } from '@app/core/net/ws/services/log/log.service';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {OneButtonErrorComponent} from "@app/core/dialog/components/one-button-error/one-button-error.component";
import {DialogInfo} from "@app/core/error/dialog";
import {DialogConfig, IDialog} from "@app/core/dialog/types";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {InstantModule} from "@app/instant/instant.module";
import {CoreModule} from "@app/core/core.module";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {DRAWS_FOR_INSTANT} from "../../../mocks/instant-draws";
import {BetDataSource} from "@app/core/game/base-game.service";

describe('ActionButtonComponent', () => {
	let component: ActionButtonComponent;
	let fixture: ComponentFixture<ActionButtonComponent>;
	let actionsService: ActionsService;
	let appStoreService: AppStoreService;
	let instantLotteryService: InstantLotteryService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				HttpClientModule,
				TranslateModule.forRoot(),
				InstantModule,
				CoreModule
			],
			declarations: [
				ActionButtonComponent
			],
			providers: [
				LogService,
				ActionsService,
				AppStoreService,
				InstantLotteryService,
				LotteriesService,
				HttpService,
				// {
				// 	provide: DialogContainerService,
				// 	useClass: DialogContainerServiceStub
				// }
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		// TestBed.inject(DialogContainerService);
		// lotteriesService = TestBed.inject(LotteriesService);
		// await lotteriesService.reload();
		appStoreService = TestBed.inject(AppStoreService);

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);

		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(107, DRAWS_FOR_INSTANT.lottery);
		appStoreService.Draws.setLottery(124, { ...DRAWS_FOR_INSTANT.lottery, lott_code: 124});
		instantLotteryService = TestBed.inject(InstantLotteryService);
		instantLotteryService.initBetData(BetDataSource.Manual);

		actionsService = TestBed.inject(ActionsService);

		fixture = TestBed.createComponent(ActionButtonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test onActionClickHandler', () => {
		spyOn(component, 'onActionClickHandler').and.callThrough();
		actionsService.requestActionList();
		// component.button = component.action.buttons[0];
		component.onActionClickHandler();
		expect(component.onActionClickHandler).toHaveBeenCalled();
	});

});
