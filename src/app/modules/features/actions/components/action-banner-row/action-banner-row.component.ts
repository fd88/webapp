import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IActionsBanners, IActionsButton } from '@app/actions/interfaces/iactions-settings';
import { ActionsService } from '@app/actions/services/actions.service';

/**
 * Компонент для отображения строки баннеров.
 */
@Component({
	selector: 'app-action-banner-row',
	templateUrl: './action-banner-row.component.html',
	styleUrls: ['./action-banner-row.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionBannerRowComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Список баннеров.
	 */
	@Input()
	actionBanners: IActionsBanners;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ActionsService} actionsService Сервис для работы с акциями.
	 */
	constructor(
		readonly actionsService: ActionsService
	) {}

	/**
	 * Функция для отслеживания изменений в массиве.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByFn = (index, item: IActionsButton) => `${index}${item.gameCode}`;

	/**
	 * Обработчик клика по банеру.
	 *
	 * @param {MouseEvent} event Событие клика.
	 */
	onBannerClickHandler(event: MouseEvent): void {
		const target = event.target as HTMLElement;
		if (target) {
			const col = parseInt(target.getAttribute('column'), 10);
			if (!isNaN(col)) {
				const button = this.actionBanners.buttons[col];
				this.actionsService.navigateBySelectedAction(undefined, button, true);
			}
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
