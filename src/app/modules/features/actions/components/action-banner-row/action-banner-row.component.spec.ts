import {ComponentFixture, TestBed } from '@angular/core/testing';

import {ActionBannerRowComponent} from './action-banner-row.component';
import {ActionBannerComponent} from '@app/actions/components/action-banner/action-banner.component';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '@app/shared/shared.module';
import {ActionsService} from '@app/actions/services/actions.service';
import {HttpService} from '@app/core/net/http/services/http.service';
import {InstantLotteryService} from '@app/instant/services/instant-lottery.service';
import {LotteriesService} from '@app/core/services/lotteries.service';
import {LotteriesGroupCode, LotteryGameCode} from "@app/core/configuration/lotteries";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {DRAWS_FOR_INSTANT} from "../../../mocks/instant-draws";
import {LotteriesDraws} from "@app/core/services/store/draws";

const BUTTON = {
	labelKey: "actions.zabava_ticket_pairs",
	data: {
		summ: 20,
		drawCount: 1,
		betCount: 1,
		pairs: 2
	},
	gameCode: LotteryGameCode.Zabava,
	groupCode: LotteriesGroupCode.Regular
};

describe('ActionBannerRowComponent', () => {
	let component: ActionBannerRowComponent;
	let fixture: ComponentFixture<ActionBannerRowComponent>;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				BrowserAnimationsModule,
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot(),
				SharedModule
			],
			declarations: [
				ActionBannerRowComponent,
				ActionBannerComponent
			],
			providers: [
				LogService,
				HttpService,
				ActionsService,
				InstantLotteryService,
				LotteriesService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},
				AppStoreService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);

		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);

		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(107, DRAWS_FOR_INSTANT.lottery);
		appStoreService.Draws.setLottery(124, { ...DRAWS_FOR_INSTANT.lottery, lott_code: 124});

	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ActionBannerRowComponent);
		component = fixture.componentInstance;
		component.actionBanners = {
			buttons: [ BUTTON ]
		};
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test trackByFn method', () => {
		expect(component.trackByFn(0, BUTTON)).toEqual('03');
	});

	it('test onBannerClickHandler', () => {
		const ev = new MouseEvent('onBannerClickHandler');
		const target = document.createElement('div');
		target.setAttribute('column', '0');
		spyOn(component, 'onBannerClickHandler').and.callThrough();
		component.onBannerClickHandler({...ev, target});
		expect(component.onBannerClickHandler).toHaveBeenCalled();
	});
});
