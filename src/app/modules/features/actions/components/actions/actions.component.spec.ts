import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActionsComponent } from './actions.component';
import { ActionsService } from '@app/actions/services/actions.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { ActionItemComponent } from '@app/actions/components/action-item/action-item.component';
import { SharedModule } from '@app/shared/shared.module';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ActionBannerRowComponent } from '@app/actions/components/action-banner-row/action-banner-row.component';
import { ActionButtonComponent } from '@app/actions/components/action-button/action-button.component';
import { ActionBannerComponent } from '@app/actions/components/action-banner/action-banner.component';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from '@app/core/net/http/services/http.service';
import {LotteriesDraws} from "@app/core/services/store/draws";


describe('ActionsComponent', () => {
	let component: ActionsComponent;
	let fixture: ComponentFixture<ActionsComponent>;
	let appStoreService: AppStoreService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				BrowserAnimationsModule,
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot(),
				SharedModule,
				HttpClientTestingModule
			],
			declarations: [
				ActionsComponent,
				ActionItemComponent,
				ActionButtonComponent,
				ActionBannerRowComponent,
				ActionBannerComponent
			],
			providers: [
				ActionsService,
				LogService,
				AppStoreService,
				InstantLotteryService,

				LotteriesService,
				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.Draws = new LotteriesDraws();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ActionsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test trackByFn function', () => {
		expect(component.trackByFn(0, 1)).toEqual(1);
	});

	it('test onBannerClickHandler', () => {
		spyOn(component, 'onBannerClickHandler').and.callThrough();
		const ev = new Event('onBannerClickHandler');
		component.onBannerClickHandler(ev, 	{
				"buttons": [
					{
						"imgUrl": "assets/img/instant/actions/banner_labirynt.jpg",
						"gameCode": 157,
						"groupCode": 1,
						"data": {
							"betCount": 1
						}
					}
				]
			}
		);
		expect(component.onBannerClickHandler).toHaveBeenCalled();
	});
});
