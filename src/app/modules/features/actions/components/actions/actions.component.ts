import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActionsService } from '@app/actions/services/actions.service';
import { IActionsBanners } from '@app/actions/interfaces/iactions-settings';

/**
 * Компонент акций.
 */
@Component({
	selector: 'app-actions',
	templateUrl: './actions.component.html',
	styleUrls: ['./actions.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionsComponent implements OnInit {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ActionsService} actionsService Сервис акций.
	 */
	constructor(
		readonly actionsService: ActionsService
	) {}

	/**
	 * Функция для отслеживания изменений в массиве акций.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByFn = (index, item) => item;

	/**
	 * Обработчик клика на баннере.
	 *
	 * @param {Event} event Передаваемое событие.
	 * @param {IActionsBanners} banner Баннер.
	 */
	onBannerClickHandler(event: Event, banner: IActionsBanners): void {
		this.actionsService.navigateBySelectedAction(banner, banner.buttons[0]);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.actionsService.requestActionList();
	}

}
