import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionBannerComponent } from './action-banner.component';
import { ActionsService } from '@app/actions/services/actions.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared/shared.module';
import {HttpService} from "@app/core/net/http/services/http.service";

xdescribe('ActionBannerComponent', () => {
	let component: ActionBannerComponent;
	let fixture: ComponentFixture<ActionBannerComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				BrowserAnimationsModule,
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot(),
				SharedModule
			],
			declarations: [
				ActionBannerComponent,
				ActionBannerComponent
			],
			providers: [
				LogService,
				ActionsService,
				HttpService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		fixture = TestBed.createComponent(ActionBannerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
