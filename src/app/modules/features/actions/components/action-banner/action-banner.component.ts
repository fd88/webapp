import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IActionsButton } from '@app/actions/interfaces/iactions-settings';

/**
 * Компонент для отображения баннера на странице акций
 */
@Component({
	selector: 'app-action-banner',
	templateUrl: './action-banner.component.html',
	styleUrls: ['./action-banner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionBannerComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Объект с настройками баннера
	 */
	@Input()
	banner: IActionsButton;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
