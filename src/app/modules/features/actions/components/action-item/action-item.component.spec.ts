import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionItemComponent } from './action-item.component';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ActionButtonComponent } from '@app/actions/components/action-button/action-button.component';
import { RouterTestingModule } from '@angular/router/testing';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { LotteryGameCode } from "@app/core/configuration/lotteries";
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpService } from '@app/core/net/http/services/http.service';
import { InstantLotteryService } from '@app/instant/services/instant-lottery.service';
import { LotteriesService } from '@app/core/services/lotteries.service';

describe('ActionItemComponent', () => {
	let component: ActionItemComponent | any;
	let fixture: ComponentFixture<ActionItemComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
				ActionItemComponent,
				ActionButtonComponent
			],
			providers: [
				LogService,
				HttpClient,
				HttpHandler,
				HttpService,
				InstantLotteryService,
				LotteriesService
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ActionItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test trackByFn function', () => {
		expect(component.trackByFn(0, 1)).toEqual(1);
	});

	it('test action setter', () => {
		component.action = {
			gameCode: LotteryGameCode.Zabava,
			groupCode: 0,
			buttons: [
				{
					labelKey: "actions.zabava_ticket_pairs",
					data: {
						summ: 20,
						drawCount: 1,
						betCount: 1,
						pairs: 2
					}
				},
				{
					labelKey: "actions.zabava_tickets_pairs",
					data: {
						summ: 50,
						drawCount: 1,
						betCount: 2,
						pairs: 3
					}
				}
			]
		};
		expect(component._action).toEqual(component.action);
	});

});
