import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IActionsListItem } from '@app/actions/interfaces/iactions-settings';
import { LogoViewStyle } from '@app/shared/components/msl-game-logo/msl-game-logo.component';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { ActionsService } from '@app/actions/services/actions.service';

/**
 * Компонент для отображения элемента списка акций
 */
@Component({
	selector: 'app-action-item',
	templateUrl: './action-item.component.html',
	styleUrls: ['./action-item.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionItemComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------
	/**
	 * Сеттер для элемента списка акций
	 * @param value Элемент списка акций
	 */
	@Input()
	set action(value: IActionsListItem) {
		this._action = value;

		if (!!value && Array.isArray(value.buttons) && value.buttons.length > 0) {
			this.gameCode = value.buttons[0].gameCode;
			this.groupCode = value.buttons[0].groupCode;
		}
	}

	/**
	 * Геттер для элемента списка акций
	 */
	get action(): IActionsListItem {
		return this._action;
	}

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Список вариантов отображения
	 */
	readonly LogoViewStyle = LogoViewStyle;

	/**
	 * Код игры
	 */
	gameCode: LotteryGameCode;

	/**
	 * Код группы игр для ЭМЛ
	 */
	groupCode: LotteriesGroupCode = LotteriesGroupCode.Regular;

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Элемент списка акций
	 * @private
	 */
	private _action: IActionsListItem;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента
	 * @param actionsService Сервис для работы с акциями
	 */
	constructor(readonly actionsService: ActionsService) {}

	/**
	 * Функция для отслеживания изменений в массиве
	 * @param index Индекс элемента
	 * @param item Элемент
	 */
	trackByFn = (index, item) => item;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
