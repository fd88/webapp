import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';

/**
 * Интерфейс настроек акций.
 */
export interface IActionsSettings {
	/**
	 * Список баннеров.
	 */
	bannersList: Array<IActionsBanners>;
	/**
	 * Список кнопок акций.
	 */
	actionsList: Array<IActionsListItem>;
}

/**
 * Модель одного баннера из списка баннеров {@link IActionsSettings.bannersList bannersList}.
 */
export interface IActionsBanners extends IActionsListItem {

}

/**
 * Модель одной акции из списка акций {@link IActionsSettings.actionsList actionsList}.
 */
export interface IActionsListItem {

	/**
	 * Список кнопок.
	 */
	buttons: Array<IActionsButton>;

}

/**
 * Модель одной кнопки внутри одной акции.
 */
export interface IActionsButton {

	/**
	 * Ключ для локализации метки на кнопке.
	 * В качестве параметров для локализации будет передан параметр {@link data},
	 * соответственно строка локализации должна быть сделана с учетом этих параметров.
	 */
	labelKey?: string;

	/**
	 * Ссылка на изображение.
	 */
	imgUrl?: string;

	/**
	 * Код игры.
	 */
	gameCode: LotteryGameCode;

	/**
	 * Код группы.
	 */
	groupCode: LotteriesGroupCode;

	/**
	 * Данные акционной ставки, отображаемые на кнопке.
	 */
	data: IActionParameters;

	/**
	 * Признак, указывающий, на то, что кнопка должна быть отключена (по причине отсутствия тиражей).
	 */
	disabled?: boolean;

}

/**
 * Модель различных параметров для кнопок.
 * Данные параметры будут отражены на кнопках и будут использоваться в параметрах локализации.
 */
export interface IActionParameters {

	/**
	 * Сумма акционной ставки в гривнах.
	 */
	summ?: number;

	/**
	 * Количество тиражей.
	 */
	drawCount?: number;

	/**
	 * Количество ставок.
	 */
	betCount?: number;

	/**
	 * Количество шаров (в случае Мегалота)
	 */
	ballCount?: number;

	/**
	 * Количество мега-шаров (в случае Мегалота)
	 */
	megaBallCount?: number;

	/**
	 * Количество парочек (в случае Забавы)
	 */
	pairs?: number;

	/**
	 * Сумма ставок по билету
	 */
	ticketSumm?: number;
}
