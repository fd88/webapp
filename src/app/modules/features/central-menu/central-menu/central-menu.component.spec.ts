import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CentralMenuComponent } from './central-menu.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { ReportsService } from '@app/core/services/report/reports.service';
import { ReportsServiceStub } from '@app/core/services/report/reports.service.spec';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import {CoreModule} from "@app/core/core.module";
import {HttpService} from "@app/core/net/http/services/http.service";
import {ReportingModule} from "@app/reporting/reporting.module";

describe('CentralMenuComponent', () => {
	let component: CentralMenuComponent;
	let fixture: ComponentFixture<CentralMenuComponent>;
	let reportService: ReportsService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				BrowserAnimationsModule,
				RouterTestingModule.withRoutes([]),
				NoopAnimationsModule,
				HttpClientModule,
				TranslateModule.forRoot(),
				CoreModule,
				ReportingModule
			],
			providers: [
				LogService,
				AppStoreService,
				ReportsService,
				HttpService
			],
			declarations: [
				CentralMenuComponent
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
		reportService = TestBed.inject(ReportsService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CentralMenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test ngOnInit lifecycle hook', () => {
		spyOn(component, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		reportService.ready$.next(true);
		expect(component.ngOnInit).toHaveBeenCalled();
	});
});
