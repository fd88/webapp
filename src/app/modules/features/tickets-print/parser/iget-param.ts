/**
 * Интерфейс модели для получения параметров.
 */
export interface IGetParam {
	/**
	 * Получить параметр по имени.
	 * @param name Имя параметра.
	 */
	getParam(name: string): any;

	/**
	 * Получить параметр секции.
	 */
	getSectionParam(): any;

	/**
	 * Получить глобальные параметры.
	 */
	getGlobalParams(): any;
}
