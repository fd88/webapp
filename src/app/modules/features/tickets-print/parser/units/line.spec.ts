import {LineUnit} from "@app/tickets-print/parser/units/line";
import {TestBed} from "@angular/core/testing";

xdescribe('LineUnit', () => {
	let lineUnit: LineUnit;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				{
					provide: LineUnit,
					useValue: new LineUnit([
						{
							nolf: '0',
							align: 'left',
							width: 100,
							spacing: '',
						}
					], null)
				}
			]
		});

		lineUnit = TestBed.inject(LineUnit);
		lineUnit.clean();
	});

	it('test setText method', () => {
		spyOn(lineUnit, 'setText').and.callThrough();
		lineUnit.setText('some text');
		expect(lineUnit.setText).toHaveBeenCalled();
	});

	it('test setParam method', () => {
		spyOn(lineUnit, 'setParam').and.callThrough();
		lineUnit.setParam('some parameter');
		expect(lineUnit.setParam).toHaveBeenCalled();
	});


});
