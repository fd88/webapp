import { BarcodeUnit } from "@app/tickets-print/parser/units/barcode";
import { TestBed } from "@angular/core/testing";

describe('BarcodeUnit', () => {
	let barcodeUnit: BarcodeUnit | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				{
					provide: BarcodeUnit,
					useValue: new BarcodeUnit([], undefined)
				}
			]
		});

		barcodeUnit = TestBed.inject(BarcodeUnit);
		barcodeUnit.clean();
	});

	it('test setParam method', () => {
		barcodeUnit.setParam('ololo');
		expect(barcodeUnit.barcode.getBarcodeData()).toEqual(['ololo']);
	});

	it('test setText method', () => {
		barcodeUnit.setText('trololo');
		expect(barcodeUnit.barcode.getBarcodeData()).toEqual(['trololo']);
	});
});
