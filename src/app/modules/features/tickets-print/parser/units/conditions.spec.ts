import {ConditionsUnit} from "@app/tickets-print/parser/units/conditions";
import {TestBed} from "@angular/core/testing";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {HttpClientModule} from "@angular/common/http";
import {HttpService} from "@app/core/net/http/services/http.service";

describe('ConditionsUnit', () => {
	let conditionsUnit: ConditionsUnit;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				{
					provide: ConditionsUnit,
					useValue: new ConditionsUnit()
				},
				LogService,
				HttpService
			]
		});
		TestBed.inject(LogService);
		conditionsUnit = TestBed.inject(ConditionsUnit);
		conditionsUnit.clean();
	});

	it('test test method', () => {
		conditionsUnit.test({
			getParam: (p: string) => 'test',
			getSectionParam: () => null,
			getGlobalParams: () => null
		}, null);
		expect(conditionsUnit.condition).toBeTruthy();
	});

	it('test test method 2', () => {
		conditionsUnit.test({
			getParam: (p: string) => 5,
			getSectionParam: () => null,
			getGlobalParams: () => null
		}, null);
		expect(conditionsUnit.condition).toBeTruthy();
	});

	it('test test method 3', () => {
		conditionsUnit.test({
			getParam: (p: string) => ({test: 5, test2: 10}),
			getSectionParam: () => null,
			getGlobalParams: () => null
		}, null);
		expect(conditionsUnit.condition).toBeTruthy();
	});
});
