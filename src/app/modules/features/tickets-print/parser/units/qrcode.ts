import { IFlowUnit } from '@app/tickets-print/parser/iflow-unit';
import { PrintTag } from '@app/tickets-print/parser/print';
import { QRMapTag } from '@app/tickets-print/parser/tags/qrmap';

/**
 * Элемент, описывающий QR код в потоке данных.
 */
export class QRCodeUnit implements IFlowUnit {
	/**
	 * Объект описывающий QR код.
	 * @private
	 */
	private readonly qrMap: QRMapTag;

	/**
	 * Конструктор класса.
	 * @param print Массив тегов для отправки на печать.
	 * @param attributes Атрибуты.
	 */
	constructor(print: Array<PrintTag>, attributes: NamedNodeMap) {
		this.qrMap = new QRMapTag(attributes);
		print.push(this.qrMap);
	}

	/**
	 * Очистка потока данных (пустой, унаследован).
	 */
	clean(): void {
	}
}
