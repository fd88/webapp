/**
 * Интерфейс моделирующий поток данных.
 */
export interface IFlowUnit {
	/**
	 * Очистка потока данных.
	 */
	clean(): void;
}
