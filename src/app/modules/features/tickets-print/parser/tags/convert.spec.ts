import { ConvertTag } from "@app/tickets-print/parser/tags/convert";
import { TestBed } from "@angular/core/testing";

xdescribe('BarcodeUnit', () => {
	let convertTag: ConvertTag | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				{
					provide: ConvertTag,
					useValue: new ConvertTag({
						getParam: (name) => name,
						getSectionParam: () => 'test',
						getGlobalParams: () => 'test'
					}, undefined)
				}
			]
		});

		convertTag = TestBed.inject(ConvertTag);
	});

	xit('test concatToken method', () => {
		expect(convertTag.concatToken('test')).toEqual('test');
	});

});
