import { init, Tag } from '@app/tickets-print/parser/tag';

/**
 * Модель xml тега <qrmap>
 */
export class QRMapTag extends Tag {
	/**
	 * Создание и инициализация класса на основе xml атрибутов
	 * @param attributes xml-атрибуты
	 */
	constructor(attributes: NamedNodeMap) {
		super();
		init(this, attributes);
	}
}
