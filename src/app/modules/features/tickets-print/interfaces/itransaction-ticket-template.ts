/**
 * Модель, обеспечивающая доступ к шаблону билета для печати.
 */
export interface ITransactionTicketTemplate {

	/**
	 * Путь к экшену.
	 */
	path: string;

	/**
	 * Полный URL для загрузки ресурса из ЦС.
	 */
	url: string;

}
