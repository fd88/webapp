import { PrintData } from '@app/core/net/ws/api/models/print/print-models';
import { ITransactionTicket } from '@app/core/services/transaction/transaction-types';

/**
 * Интерфейс модели для хранения данных о процессе печати билета
 */
export interface PrintTicketInfo {

	/**
	 * Обрабатываемая модель билета из ответа.
	 */
	ticket: ITransactionTicket;

	/**
	 * Ошибка обработки билета.
	 */
	error?: any;

	/**
	 * Данные для печати.
	 */
	result?: Array<PrintData>;

}
