import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import {TranslateModule, TranslateService} from '@ngx-translate/core';

import { LogOutService } from '@app/logout/services/log-out.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { AuthService } from '@app/core/services/auth.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { LotteriesService } from '@app/core/services/lotteries.service';



import {AppStoreService} from "@app/core/services/store/app-store.service";
import {TotalCheckStorageService} from "@app/total-check/services/total-check-storage.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {Router} from "@angular/router";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";

xdescribe('LogOutService', () => {
	let service: LogOutService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule
			],
			providers: [
				LogService,
				LogOutService,
				AuthService,
				DialogContainerService,
				HttpService,
				LotteriesService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(LogOutService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

// -----------------------------
//  Static functions
// -----------------------------

export class LogOutServiceStub extends LogOutService {
	constructor() {
		super(
			{} as TranslateService,
			{} as DialogContainerService,
			{} as AppStoreService,
			{} as TotalCheckStorageService,
			{} as BarcodeReaderService,
			{} as
			{} as LotteriesService,
			{} as Router,
			{} as StorageService
		);
	}

	logoutOperator(): void {

	}
}
