import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LogOutButtonComponent } from '@app/logout/components/log-out-button/log-out-button.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { TranslateModule } from '@ngx-translate/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { LogOutService } from '@app/logout/services/log-out.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LotteriesService } from '@app/core/services/lotteries.service';

xdescribe('LogOutButtonComponent', () => {
	let component: LogOutButtonComponent;
	let fixture: ComponentFixture<LogOutButtonComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				HttpClientTestingModule,
				RouterTestingModule.withRoutes([])
			],
			declarations: [
				LogOutButtonComponent
			],
			providers: [
				LogService,
				LogOutService,
				HttpService,
				DialogContainerService,
				LotteriesService
			]
		})
		.compileComponents();

		TestBed.inject(LogService)
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(LogOutButtonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
