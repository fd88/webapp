import { BaseGameService, IBetDataItem } from '@app/core/game/base-game.service';
import { ICheckInfoRow } from './icheck-info-row';

/**
 * Интерфейс резолвера, используемого в регистрации чека.
 */
export interface IRegistryResolveData {

	/**
	 * Список строк для отображения на странице регистрации чека.
	 * Содержит массив элементов типа {@link ICheckInfoRow}.
	 */
	checkInfoRowList: Array<ICheckInfoRow>;

	/**
	 * Ссылка на базовый сервис.
	 */
	baseGameService: BaseGameService<IBetDataItem>;

}
