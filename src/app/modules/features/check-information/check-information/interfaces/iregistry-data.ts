import { ICheckInfoRow } from './icheck-info-row';

/**
 * Интерфейс компонента регистрации чека.
 */
export interface IRegistryData {

	/**
	 * Список строк для отображения на странице регистрации чека.
	 * Содержит массив элементов типа {@link ICheckInfoRow}.
	 */
	checkInfoRowList: Array<ICheckInfoRow>;

	/**
	 * Слушатель нажатия кнопки "РЕГИСТРАЦИЯ" ставки.
	 */
	onConfirmClickHandler(): void;

	/**
	 * Слушатель нажатия кнопки "НАЗАД".
	 */
	onBackClickHandler(): void;

}
