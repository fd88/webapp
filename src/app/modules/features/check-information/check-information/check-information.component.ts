import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	HostListener,
	Input,
	OnDestroy,
	OnInit,
	Output,
	ViewChild
} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {interval, MonoTypeOperatorFunction, Observable, of, Subject, timer} from 'rxjs';
import {catchError, concatMap, delay, finalize, map, repeat, takeUntil, tap} from 'rxjs/operators';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { BaseGameService } from '@app/core/game/base-game.service';
import { ICheckInfoRow } from './interfaces/icheck-info-row';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { AppType } from '@app/core/services/store/settings';
import { SendAuthCodeResp } from '@app/core/net/http/api/models/send-auth-code';
import { IResponse } from '@app/core/net/http/api/types';
import { DialogError, ErrorCode } from '@app/core/error/dialog';
import { IError, NetError } from '@app/core/error/types';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { SmsCheckService } from '@app/core/services/sms-check.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MaskedInputComponent } from '@app/shared/components/masked-input/masked-input.component';
import { MslInputPinComponent } from '@app/shared/components/msl-input-pin/msl-input-pin.component';
import {getShortPlayerPhone, GreenButtonStyle} from '@app/util/utils';
import { environment } from '@app/env/environment';
import { LogOutService } from '@app/logout/services/log-out.service';
import {ZabavaLotteryService} from "@app/zabava/services/zabava-lottery.service";
import {TransactionService} from "@app/core/services/transaction/transaction.service";
import {Logger} from "@app/core/net/ws/services/log/logger";
import {URL_LOTTERIES} from "@app/util/route-utils";
import {ITransactionResponse, ITransactionTicket} from "@app/core/services/transaction/transaction-types";
import {
	UserLoyaltyAuthComponent
} from "../../user-loyalty-auth/components/user-loyalty-auth/user-loyalty-auth.component";
import {UserLoyaltyAuthService} from "../../user-loyalty-auth/services/user-loyalty-auth.service";
import {ILoyaltyStepResult} from "../../user-loyalty-auth/interfaces/i-loyalty-step-result";
import {HttpClient} from "@angular/common/http";

/**
 * Шаг регистрации
 */
enum RegStep {
	/**
	 * Шаг 1
	 */
	Step1 = 1,
	/**
	 * Шаг 2
	 */
	Step2 = 2,
	/**
	 * Шаг 3
	 */
	Step3 = 3
}

/**
 * Компонент регистрации чека с отображением информацией по чеку.
 * Выводит всю необходимую информацию по чеку и показывает кнопку
 * подтверждения регистрации данного чек.
 * По нажатию кнопки подтверждения сгенерирует событие <code>confirm</code>,
 * по которому разработчик сможет продолжить регистрацию данного чека.
 */
@Component({
	selector: 'app-check-information',
	templateUrl: './check-information.component.html',
	styleUrls: ['./check-information.component.scss']
})
export class CheckInformationComponent implements OnInit, OnDestroy {

	private readonly unsubscribe$$ = new Subject<never>();

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Группа лоттерей (пока-что нужна только для триумфа).
	 */
	@Input()
	groupId = LotteriesGroupCode.Regular;

	/**
	 * Список информационных строк, которые будут выведены на экране регистрации чека.
	 * Порядок вывода строк соотетствует порядку элементов в массиве.
	 */
	@Input()
	checkInfoRowList: Array<ICheckInfoRow>;

	/**
	 * Код лотереи
	 */
	@Input()
	gameCode: number;

	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие генерируется, когда пользователь подтвердил регистрацию.
	 */
	@Output()
	readonly confirm = new EventEmitter<boolean>();

	/**
	 * Событие генерируется, когда пользователь нажал кнопку "Назад".
	 */
	@Output()
	readonly back = new EventEmitter();

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Маска ввода тел. номера
	 */
	readonly phoneMask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

	/**
	 * Шаги регистрации
	 */
	readonly RegStep = RegStep;

	/**
	 * Тип приложения
	 */
	readonly AppType = AppType;

	/**
	 * Стили кнопок
	 */
	readonly GreenButtonStyle = GreenButtonStyle;

	/**
	 * Шаг регистрации билета
	 */
	regStep: RegStep = RegStep.Step1;

	/**
	 * Базовый класс игры
	 */
	baseGameService: BaseGameService<any>;

	/**
	 * Форма
	 */
	form: FormGroup;

	/**
	 * В фокусе ли номер телефона?
	 */
	telFocused = false;

	/**
	 * Не нужна ли СМС-проверка?
	 */
	noSMSCheck = false;

	spoilerExpanded = true;

	userInfo: any = null;

	totalBonuses = 0;

	canWithdraw = 0;

	canWithdrawMoney = 0;

	betSumBonuses = 0;

	usedBonusesBonuses = 0;

	willAddBonuses = 0;

	step = 1;

	errorText = '';

	useFortunals = false;

	totalSum = '';

	verificationThreshold = 0;

	showButtons = false;

	isLoading = false;

	latency = 0;

	maxLatency = 300;

	/**
	 * Неактивна дли кнопка регистрации?
	 */
	get isRegBtnDisabled(): boolean {
		if (!this.noSMSCheck) {
			if (this.regStep === RegStep.Step1 && this.form.valid) {
				return false;
			}

			return !(this.regStep === RegStep.Step2 && !this.appStoreService.smsCodeError && this.form.valid);
		}

		if (this.step === 1) {
			return this.loyaltyAuth?.form.invalid;
		}

		if (this.step === 2) {
			return this.loyaltyAuth?.form2.invalid;
		}

		return false;
	}

	get lotteryWithLoyaltyCard(): boolean {
		// return this.gameCode === LotteryGameCode.Sportprognoz;
		return false;
	}

	/**
	 * Телефон юзера
	 */
	@ViewChild('userTel', {static: false}) userTel: MaskedInputComponent;

	/**
	 * Поле ввода СМС
	 */
	@ViewChild('smsControl', {static: false}) smsControl: MslInputPinComponent;

	@ViewChild('loyaltyAuth') loyaltyAuth: UserLoyaltyAuthComponent;

	@ViewChild('loyaltyAuth2') loyaltyAuth2: UserLoyaltyAuthComponent;

	// -----------------------------
	//  Public functions
	// -----------------------------
	/**
	 * Конструктор компонента
	 * @param route Объект для работы с маршрутами
	 * @param router
	 * @param appStoreService
	 * @param smsCheckService
	 * @param dialogInfoService
	 * @param formBuilder
	 * @param logoutService
	 * @param transactionService
	 * @param userLoyaltyAuthService
	 * @param httpClient
	 */
	constructor(
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		readonly appStoreService: AppStoreService,
		private readonly smsCheckService: SmsCheckService,
		private readonly dialogInfoService: DialogContainerService,
		private readonly formBuilder: FormBuilder,
		private readonly logoutService: LogOutService,
		readonly transactionService: TransactionService,
		readonly userLoyaltyAuthService: UserLoyaltyAuthService,
		private readonly httpClient: HttpClient
	) {}

	/**
	 * Обработчик события клика по документу.
	 * @param event Передаваемое событие
	 */
	@HostListener('document:click', ['$event'])
	documentClick(event: MouseEvent): void {
		this.telFocused = (event.target as Element).classList.contains('masked-input');
	}

	/**
	 * Слушатель нажатия кнопки "НАЗАД".
	 * Отправляет событие <code>back</code>.
	 */
	onClickBackButtonHandler(): void {
		switch (this.step) {
			case 1:
				this.back.emit();

				if (!!this.baseGameService) {
					this.baseGameService.goBackFromRegistrationForm();
				}
			break;
			case 2:
				this.loyaltyAuth.goBack();
				this.step = 1;
				this.userLoyaltyAuthService.lastEnteredPhone = '';
			break;
			case 3:
				this.step = 1;
			break;
			case 4:
				this.step = 3;
				this.useFortunals = false;
			break;
			case 5:
				this.step = 4;
				this.loyaltyAuth2.goBack();
			break;
		}
	}

	onClickRegWithCard(): void {
		this.showButtons = false;
		this.isLoading = true;

		switch (this.step) {
			case 1:
				this.loyaltyAuth.executeStep1()
					.subscribe({
						next: (v) => {
							this.step = v.step;
						},
						error: this.errorsHandler.bind(this)
					});
				break;
			case 2:
				this.loyaltyAuth.executeStep2()
					.subscribe({
						next: (v) => {
							this.step = v.step;
							this.isLoading = false;
						},
						error: this.errorsHandler.bind(this)
					});
				break;
			case 3:
				if ((this.canWithdraw >= this.verificationThreshold) && this.useFortunals) {
					this.userLoyaltyAuthService.errorText = '';
					if (this.userLoyaltyAuthService.lastEnteredPhone) {
						this.loyaltyAuth2.executeStep1(true)
							.subscribe({
								next: (v) => {
									this.step = 5;
									this.isLoading = false;
								},
								error: this.errorsHandler.bind(this)
							});
					} else {
						this.step = 4;
					}
				} else {
					this.onClickActionButtonHandler();
				}
				this.isLoading = false;
				break;
			case 4:
				this.loyaltyAuth2.executeStep1()
					.subscribe({
						next: (v) => {
							this.step = 5;
							this.isLoading = false;
						},
						error: this.errorsHandler.bind(this)
					});
				break;
			case 5:
				this.loyaltyAuth2.executeStep2()
					.subscribe({
						next: (v) => {
							if (!v.error) {
								this.onClickActionButtonHandler();
								this.step = 3;
								this.userLoyaltyAuthService.lastEnteredPhone = '';
								this.userLoyaltyAuthService.errorText = '';
							}
							this.isLoading = false;
						},
						error: this.errorsHandler.bind(this)
					});
				break;
		}
		timer(100)
			.subscribe(() => {
				this.showButtons = true;
			})
	}

	onClickRegWithoutCard(): void {
		this.appStoreService.cardNum = '';
		this.appStoreService.bonusPaySum = 0;
		this.onClickActionButtonHandler();
	}

	/**
	 * Слушатель нажатия кнопки "РЕГИСТРАЦИЯ".
	 * Отправляет событие <code>confirm</code>.
	 */
	onClickActionButtonHandler(): void {
		if (this.noSMSCheck) {
			if (this.gameCode === LotteryGameCode.Zabava && this.baseGameService.betInputMode === 2) {
				(this.baseGameService as ZabavaLotteryService).buyLotteryWithBlanks();
			} else {
				this.baseGameService.buyLottery();
			}
		} else {
			if (this.regStep === RegStep.Step1 && this.form.controls['playerPhone'].valid) {
				this.sendSMS();
			} else if (this.regStep === RegStep.Step2 && !this.appStoreService.smsCodeError
				&& this.smsControl.value.length === 4) {
				this.confirm.emit(true);

				if (!!this.baseGameService) {
					this.appStoreService.playerInfo = {
						playerPhone: getShortPlayerPhone(this.form.controls['playerPhone'].value),
						playerAuthCode: this.smsControl.value
					};
					this.baseGameService.buyLottery();
				}
			}
		}
	}

	isRegButtonEnabled(): boolean {
		if (!!this.errorText) {
			return false;
		}
		if ((this.step === 1) || (this.step === 2)) {
			return this.loyaltyAuth?.isValid();
		}
		if ((this.step === 4) || (this.step === 5)) {
			return this.loyaltyAuth2?.isValid();
		}
		return true;
	}

	/**
	 * Обработчик печати билетов в браузере
	 */
	onBrowserPrintHandler(): void {
		window.print();
	}

	onApplyDiscount(): void {
		const fullSum = parseFloat(this.checkInfoRowList[this.checkInfoRowList.length - 1].value);
		this.totalSum = this.useFortunals ? (fullSum - this.canWithdraw).toFixed(2) : fullSum.toFixed(2);
		this.appStoreService.bonusPaySum = this.useFortunals ? this.canWithdraw * 100 : 0;
		this.willAddBonuses = this.useFortunals ? this.usedBonusesBonuses : this.betSumBonuses;
	}

	onGoToMain(): void {
		this.router.navigate([`/${URL_LOTTERIES}`])
			.catch(err => {
				Logger.Log.e('Ошибка перехода на главную страницу', err)
					.console();
			});
	}

	/**
	 * Обработчик события нажатия на кнопку "Отправить СМС".
	 */
	onSendSMSHandler(): void {
		this.sendSMS();
	}

	/**
	 * Обработчик события изменения значения в поле ввода СМС.
	 */
	onPinCodeChange(): void {
		this.appStoreService.smsCodeError = false;
	}

	/**
	 * Обработчик события фокуса на поле ввода телефона.
	 */
	onUserTelFocused(): void {
		this.telFocused = true;
	}

	/**
	 * Обработчик события потери фокуса полем ввода СМС.
	 */
	onUserTelBlured(): void {
		this.telFocused = false;
	}

	onMoreLessClick(): void {
		this.spoilerExpanded = !this.spoilerExpanded;
		if (!this.spoilerExpanded && this.loyaltyAuth.isScannerExpanded) {
			this.loyaltyAuth.changeCameraState();
		}
	}

	/**
	 * Функция для отслеживания изменений в массиве.
	 * @param index Индекс элемента
	 * @param item Элемент
	 */
	trackByFn = (index, item: ICheckInfoRow) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Функция обновления данных при изменении роута, преобразована в оператор.
	 * @private
	 */
	private updateDataWhenRouteChanges(): MonoTypeOperatorFunction<Data> {
		return (source: Observable<Data>) => source.pipe(
			tap(this.updateDataCb.bind(this))
		);
	}

	/**
	 * Функция обновления данных при изменении роута.
	 * @param data
	 * @private
	 */
	private updateDataCb(data: Data): void {
		if (!!data && !!data.registry) {
			this.checkInfoRowList = data.registry.checkInfoRowList;
			this.baseGameService = data.registry.baseGameService;
			this.gameCode = this.baseGameService.currentGameCode;
			this.groupId = this.baseGameService.currentGroupCode;
		}
	}

	/**
	 * Функция отправки СМС.
	 * @private
	 */
	private sendSMS(): void {
		this.dialogInfoService.showNoneButtonsInfo('dialog.in_progress', 'dialog.sending_sms');
		this.smsCheckService.sendAuthCode(getShortPlayerPhone(this.form.controls['playerPhone'].value))
			.then(this.sendAuthCodeHandler.bind(this))
			.catch(this.errorSMSHandler.bind(this));
	}

	/**
	 * Функция отправки проверочного кода.
	 * @param sendAuthCodeResp Ответ от сервера.
	 * @private
	 */
	private sendAuthCodeHandler(sendAuthCodeResp: SendAuthCodeResp): void {
		if (sendAuthCodeResp.err_code === 0) {
			this.regStep = RegStep.Step2;
			this.form.controls['smsCode'].setValidators([Validators.required, Validators.pattern(/^\d{4}$/)]);
			this.dialogInfoService.hideAll();
			const tmr = timer(200)
				.subscribe(() => {
					if (environment.mockData) {
						this.form.controls['smsCode'].setValue('1234');
					}
					this.smsControl.setFocus();
					tmr.unsubscribe();
				});
		} else {
			this.errorSMSHandler(sendAuthCodeResp);
		}
	}

	/**
	 * Функция обработки ошибок при отправке СМС.
	 * @param errResp Ответ от сервера.
	 * @private
	 */
	private errorSMSHandler(errResp: IResponse | IError): void {
		let msg: DialogError;
		if ((errResp as IResponse).err_code) {
			const resp = errResp as IResponse;
			msg = { code: resp.err_code, message: 'lottery.error_sending_sms', messageDetails: resp.err_descr };
		} else {
			msg = (errResp instanceof NetError)
				? { code: errResp.code, message: errResp.message, messageDetails: errResp.messageDetails }
				: new DialogError(ErrorCode.TicketRegister, 'lottery.error_sending_sms');
		}

		this.dialogInfoService.showOneButtonError(msg, {
			click: () => {
				if ((msg.code === 4313) || (msg.code === 4318)) {
					this.logoutService.logoutOperator();
				}
			},
			text: 'dialog.dialog_button_continue'
		});
	}

	private errorsHandler(error: IResponse | IError): void {
		const errCode = +(error as IResponse).err_code || +(error as IError).code;
		const message = (error as IResponse).err_descr || (error as IError).message;
		console.log('error =', error);
		this.errorText = errCode === 702 ? 'other.verification-error-2' : message;
		this.isLoading = false;
	}

	onAuthResult(result: ILoyaltyStepResult): void {
		if (result.step === 3) {
			this.appStoreService.cardNum = result.cardNum;
			const ticketsCount = +this.checkInfoRowList[this.checkInfoRowList.length - 2].value;
			const totalSum = parseFloat(this.checkInfoRowList[this.checkInfoRowList.length - 1].value);
			this.userLoyaltyAuthService.isLoading = true;
			this.totalBonuses = result.data.total_bonus_sum / 100;
			this.verificationThreshold = result.data.card_limits.min_bonus_sum_with_auth.limit_value / 100;

			this.userLoyaltyAuthService.getAvailableBonus(
				result.cardNum,
				totalSum,
				ticketsCount,
				this.gameCode
			).pipe(
				finalize(() => {
					this.userLoyaltyAuthService.isLoading = false;
				})
			).subscribe({
				next: (v) => {
					this.userInfo = v;
					this.canWithdraw = v.available_bonus_sum / 100;
					this.canWithdrawMoney = v.available_bonus_sum_money / 100;
					this.betSumBonuses = v.bet_sum_bonuses / 100;
					this.usedBonusesBonuses = v.used_bonuses_bonuses / 100;
					this.willAddBonuses = this.betSumBonuses;
					this.isLoading = false;
				},
				error: (err) => {
					this.errorsHandler(err);
				}
			});
		} else {
			this.isLoading = false;
		}
	}

	onAuthResult2(result: ILoyaltyStepResult): void {
		if (result.step === 3) {
			this.appStoreService.cardNum = result.cardNum;
			this.onClickActionButtonHandler();
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.appStoreService.smsCodeError = false;
		this.route.data
			.pipe(this.updateDataWhenRouteChanges())
			.subscribe();

		this.form = this.formBuilder.group({
			playerPhone: ['', [Validators.required]],
			// smsCode: ['', [Validators.required, Validators.pattern(/^\d{4}$/)]],
			smsCode: ['']
		});
		const sellMethod = this.appStoreService.Settings.sellMethods.get(this.baseGameService.currentGameCode);
		this.noSMSCheck = !(sellMethod && sellMethod.sms);

		this.step = 1;
		this.userLoyaltyAuthService.lastEnteredPhone = '';

		this.appStoreService.printGameName = this.baseGameService.gameName;
		this.transactionService.lastResponse$.subscribe((response: ITransactionResponse) => {
			this.appStoreService.showBrowserPrint = !!response && (this.transactionService.trsMode === 3);
			if (this.appStoreService.showBrowserPrint) {
				this.appStoreService.tickets = ('length' in response.ticket) ? response.ticket : [response.ticket];
			}
		});

		timer(100)
			.subscribe(() => {
				this.showButtons = true;
			});

		this.testPing()
			.pipe(
				delay(1000),
				repeat(),
				takeUntil(this.unsubscribe$$)
			)
			.subscribe((v) => {
				this.latency = v;
			});

	}

	private testPing(): Observable<number> {
		let start: number;
		return of(null)
			.pipe(
				concatMap(() => {
					start = performance.now();
					return this.httpClient.get('/favicon.ico?' + Math.random() * 10000000);
				}),
				catchError((err) => {
					console.log('err =', err);
					return of(null);
				}),
				map(() => Math.round(performance.now() - start))
			);
	}

	ngOnDestroy(): void {
		this.transactionService.lastResponse$.next(null);
		this.appStoreService.showBrowserPrint = false;
		this.appStoreService.tickets = [];
		this.appStoreService.cardNum = '';
		this.step = 1;
		this.userLoyaltyAuthService.lastEnteredPhone = '';
		this.unsubscribe$$.next();
		this.unsubscribe$$.complete();
	}
}
