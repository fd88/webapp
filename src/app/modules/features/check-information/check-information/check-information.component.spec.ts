import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CheckInformationComponent} from './check-information.component';
import {RouterTestingModule} from "@angular/router/testing";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {CoreModule} from "@app/core/core.module";
import {TranslateModule} from "@ngx-translate/core";
import {LogOutModule} from "@app/logout/log-out.module";
import {SharedModule} from "@app/shared/shared.module";
import {MslGameLogoComponent} from "@app/shared/components/msl-game-logo/msl-game-logo.component";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {SmsCheckService} from "@app/core/services/sms-check.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {LogOutService} from "@app/logout/services/log-out.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {BetDataSource} from "@app/core/game/base-game.service";
import {ZabavaLotteryService} from "@app/zabava/services/zabava-lottery.service";
import {MaskedInputComponent} from "@app/shared/components/masked-input/masked-input.component";
import {LotteriesDraws} from "@app/core/services/store/draws";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DRAWS_FOR_GAME_ZABAVA} from "../../mocks/zabava-draws";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {NetError} from "@app/core/error/types";
import {UserLoyaltyAuthModule} from "../../user-loyalty-auth/user-loyalty-auth.module";

describe('CheckInformationComponent', () => {
  let component: CheckInformationComponent | any;
  let fixture: ComponentFixture<CheckInformationComponent>;
  let appStoreService: AppStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			RouterTestingModule,
			CoreModule,
			TranslateModule.forRoot(),
			LogOutModule,
			SharedModule,
			ReactiveFormsModule,
			HttpClientModule,
			UserLoyaltyAuthModule
		],
		declarations: [
			CheckInformationComponent,
			MslGameLogoComponent,
			MslInputPinComponent,
			MaskedInputComponent
		],
		providers: [
		  AppStoreService,
		  SmsCheckService,
		  {
			provide: DialogContainerService,
			useClass: DialogContainerServiceStub
		  },
		  FormBuilder,
		  LogOutService,
		  HttpClient,
		  ZabavaLotteryService,
		  LogService
		]
    })
    .compileComponents();

	TestBed.inject(LogService);
	appStoreService = TestBed.inject(AppStoreService);
	appStoreService.Draws = new LotteriesDraws();
	appStoreService.Draws.setLottery(LotteryGameCode.Zabava, DRAWS_FOR_GAME_ZABAVA.lottery);
	const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	appStoreService.Settings.populateEsapActionsMapping(csConfig);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInformationComponent);
    component = fixture.componentInstance;
	component.baseGameService = TestBed.inject(ZabavaLotteryService);
	component.baseGameService.initBetData(BetDataSource.Manual);
	const zabavaGameService = component.baseGameService as ZabavaLotteryService;
    zabavaGameService.init();
	zabavaGameService.betData.betsCount = 10;
	zabavaGameService.drawIndex = 0;
	component.checkInfoRowList = [
		{
			"langKeyName": "lottery.selected_draw",
			"value": "1305"
		},
		{
			"langKeyName": "lottery.zabava.rich_and_famous",
			"value": "dialog.no"
		},
		{
			"langKeyName": "lottery.zabava.pair",
			"value": "5"
		},
		{
			"langKeyName": "lottery.all_tickets_number",
			"value": "10"
		},
		{
			"langKeyName": "lottery.sum",
			"value": "350.00",
			"redColor": true,
			"valueIsCurrency": true
		}
	];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickActionButtonHandler', () => {
		component.noSMSCheck = true;
		spyOn(component, 'onClickActionButtonHandler').and.callThrough();
		component.onClickActionButtonHandler();
		expect(component.onClickActionButtonHandler).toHaveBeenCalled();
	});

	it('test onClickActionButtonHandler 2', () => {
		component.noSMSCheck = false;
		component.regStep = 1;
		component.form.controls['playerPhone'].setValue('+380681234567');
		component.form.controls['playerPhone'].updateValueAndValidity();
		spyOn(component, 'onClickActionButtonHandler').and.callThrough();
		component.onClickActionButtonHandler();
		component.regStep = 2;
		component.smsControl = {
			setFocus: () => {
			},
			value: '1234'
		} as MslInputPinComponent;
		component.onClickActionButtonHandler();
		expect(component.onClickActionButtonHandler).toHaveBeenCalled();
	});

	it('test onClickBackButtonHandler', () => {
		spyOn(component, 'onClickBackButtonHandler').and.callThrough();
		component.onClickBackButtonHandler();
		expect(component.onClickBackButtonHandler).toHaveBeenCalled();
	});

	it('test onSendSMSHandler', () => {
		spyOn(component, 'onSendSMSHandler').and.callThrough();
		component.smsControl = {
			setFocus: () => {
			},
			value: '1234'
		} as MslInputPinComponent;
		component.onSendSMSHandler();
		expect(component.onSendSMSHandler).toHaveBeenCalled();
	});

	it('test onPinCodeChange handler', () => {
		component.onPinCodeChange();
		expect(appStoreService.smsCodeError).toBeFalsy();
	});

	it('test onUserTelFocused handler', () => {
		component.onUserTelFocused();
		expect(component.telFocused).toBeTruthy();
	});

	it('test onUserTelBlured handler', () => {
		component.onUserTelBlured();
		expect(component.telFocused).toBeFalsy();
	});

	it('test sendAuthCodeHandler', () => {
		spyOn(component, 'sendAuthCodeHandler').and.callThrough();
		component.sendAuthCodeHandler({
			client_trans_id: '123456789',
			err_code: 700,
			err_descr: 'Тестовая ошибка'
		});
		expect(component.sendAuthCodeHandler).toHaveBeenCalled();
	});

	it('test sendAuthCodeHandler 2', () => {
		spyOn(component, 'sendAuthCodeHandler').and.callThrough();
		component.sendAuthCodeHandler(new NetError('Тестовая ошибка', 'Сообщение для тестовой ошибки', 1234));
		expect(component.sendAuthCodeHandler).toHaveBeenCalled();
	});

	it('test errorSMSHandler', () => {
		spyOn(component, 'errorSMSHandler').and.callThrough();
		component.errorSMSHandler({
			client_trans_id: '123456789',
			err_code: 0,
			err_descr: 'Тестовая ошибка'
		});
		expect(component.errorSMSHandler).toHaveBeenCalled();
	});

	it('test isRegBtnDisabled getter', () => {
		component.noSMSCheck = false;
		expect(component.isRegBtnDisabled).toBeTruthy();
	});

	it('test isRegBtnDisabled getter 2', () => {
		component.noSMSCheck = false;
		appStoreService.smsCodeError = false;
		expect(component.isRegBtnDisabled).toBeTruthy();
	});

	it('test isRegBtnDisabled getter 3', () => {
		component.noSMSCheck = false;
		appStoreService.smsCodeError = false;
		component.regStep = 2;
		component.form.controls['playerPhone'].setValue('+380681234567');
		component.form.controls['smsCode'].setValue('1234');
		component.form.controls['playerPhone'].updateValueAndValidity();
		expect(component.isRegBtnDisabled).toBeFalsy();
	});

	xit('test isRegBtnDisabled getter 4', () => {
		component.noSMSCheck = false;
		component.regStep = 1;
		component.form.controls['playerPhone'].setValue('+380681234567');
		component.form.controls['smsCode'].setValue('1234');
		component.form.controls['playerPhone'].updateValueAndValidity();
		expect(component.isRegBtnDisabled).toBeFalsy();
	});
});
