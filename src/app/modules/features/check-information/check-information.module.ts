import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImageModule} from '@angular/common';
import { CheckInformationComponent } from './check-information/check-information.component';
import { SharedModule } from '@app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import {UserLoyaltyAuthModule} from "../user-loyalty-auth/user-loyalty-auth.module";

@NgModule({
	declarations: [
		CheckInformationComponent
	],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        TextMaskModule,
        NgOptimizedImageModule,
		UserLoyaltyAuthModule
    ],
	exports: [
		CheckInformationComponent
	]
})
export class CheckInformationModule { }
