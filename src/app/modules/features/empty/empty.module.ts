import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { EmptyComponentComponent } from './empty-component/empty-component.component';
import { EmptyRoutingModule } from './empty-routing.module';

@NgModule({
	imports: [
		CommonModule,
		EmptyRoutingModule,
		SharedModule
	],
	providers: [
	],
	declarations: [
		EmptyComponentComponent
	]
})
export class EmptyModule {}
