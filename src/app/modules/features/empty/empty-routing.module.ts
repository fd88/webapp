import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmptyComponentComponent } from './empty-component/empty-component.component';
import { NavigationGuard } from '@app/core/guards/navigation.guard';

const routes: Routes = [
	{
		path: '',
		component: EmptyComponentComponent,
		canDeactivate: [
			NavigationGuard
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [RouterModule]
})
export class EmptyRoutingModule {}
