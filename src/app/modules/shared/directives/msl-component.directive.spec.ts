import { ElementRef, Renderer2, RendererStyleFlags2 } from '@angular/core';
import { TestBed } from "@angular/core/testing";
import { HttpClientModule } from "@angular/common/http";
import { MslComponentDirective } from "@app/shared/directives/msl-component.directive";

xdescribe('MslComponentDirective', () => {
	let directive: MslComponentDirective | any;

	beforeEach(() => {
		directive = new MslComponentDirective({} as ElementRef, {} as Renderer2);

		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				Renderer2
			]
		});

		directive.renderer = {
			setStyle: (el: any, style: string, value: any, flags?: RendererStyleFlags2): void => {
			}
		};
	});

	it('should create an instance', () => {
		expect(directive).toBeTruthy();
	});

	it('test onFocusHandler', () => {
		spyOn(directive, 'onFocusHandler').and.callThrough();
		directive.inputWidthOnFocus = 100;
		directive.onFocusHandler();
		expect(directive.onFocusHandler).toHaveBeenCalled();

	});

	it('test onBlurHandler', () => {
		spyOn(directive, 'onBlurHandler').and.callThrough();
		directive.onBlurHandler();
		expect(directive.onBlurHandler).toHaveBeenCalled();
	});



});
