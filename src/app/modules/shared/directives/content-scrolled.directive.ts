import { Directive, ElementRef, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

/**
 * Директива для определения наличия прокрутки внутри элемента.
 */
@Directive({
	selector: '[appContentScrolled]'
})
export class ContentScrolledDirective implements OnInit, OnDestroy {

	/**
	 * Подписка на изменение высоты элемента.
	 * @private
	 */
	private elSubscription: Subscription;

	/**
	 * Конструктор директивы.
	 *
	 * @param {ElementRef} elementRef Ссылка на элемент, к которому применяется директива
	 * @param {Renderer2} renderer Объект для работы с DOM в Angular
	 */
	constructor(
		private readonly elementRef: ElementRef,
		private readonly renderer: Renderer2
	) {}

	/**
	 * Обработчик события инициализации директивы
	 */
	ngOnInit(): void {
		this.elSubscription = interval(0)
			.pipe(
				map(() => {
					const elem: HTMLElement = this.elementRef.nativeElement;

					return elem.clientHeight >= elem.scrollHeight;
				}),
				distinctUntilChanged((v1, v2) => v1 === v2)
			)
			.subscribe((v: boolean) => {
				if (v) {
					this.renderer.removeClass(this.elementRef.nativeElement, 'content-scrolled');
				} else {
					this.renderer.addClass(this.elementRef.nativeElement, 'content-scrolled');
				}
			});
	}

	/**
	 * Обработчик события уничтожения директивы
	 */
	ngOnDestroy(): void {
		if (this.elSubscription) {
			this.elSubscription.unsubscribe();
			this.elSubscription = null;
		}
	}
}
