import { Directive, ElementRef, Inject, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

/**
 * Еще одна версия директивы для наложения одного элемента поверх на другого.
 * Создана для библиотеки Scandit в разделе "Другое"
 */
@Directive({
	selector: '[appLayover2]'
})
export class Layover2Directive implements OnInit, OnDestroy {

	/**
	 * Идентификатор элемента, поверх которого будет наложен текущий элемент.
	 */
	@Input() appLayover2 = '';

	/**
	 * Подписка на изменение размеров или положения элемента, поверх которого будет наложен текущий элемент.
	 * @private
	 */
	private elContSubscription: Subscription;

	/**
	 * Конструктор директивы.
	 *
	 * @param {ElementRef} elementRef Ссылка на элемент, к которому применяется директива
	 * @param {Renderer2} renderer Объект для работы с DOM в Angular
	 * @param document Ссылка на объект DOM Document
	 */
	constructor(
		private readonly elementRef: ElementRef,
		private readonly renderer: Renderer2,
		@Inject(DOCUMENT) private readonly document: Document
	) {}

	/**
	 * Пересчет позиции и размеров элемента, поверх которого будет наложен текущий элемент.
	 * @param el Элемент, поверх которого будет наложен текущий элемент
	 * @private
	 */
	private static recalcArea(el: HTMLElement): Array<number> {
		const rect = el.getBoundingClientRect();
		const cLeft = rect.left;
		const cTop = el.offsetTop;
		const cWidth = rect.width;
		const cHeight = rect.height;

		return [cLeft, cTop, cWidth, cHeight];
	}

	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.elContSubscription = interval(0)
			.pipe(
				map(() => this.document.getElementById(this.appLayover2)),
				filter(el => {
					this.renderer.setStyle(this.elementRef.nativeElement, 'visibility', el ? 'visible' : 'hidden');

					return !!el;
				}),
				map(() => Layover2Directive.recalcArea(this.document.getElementById(this.appLayover2))),
				distinctUntilChanged((v1, v2) => v1[0] === v2[0] && v1[1] === v2[1] && v1[2] === v2[2] && v1[3] === v2[3])
			)
			.subscribe((v: Array<number>) => {
				this.renderer.setStyle(this.elementRef.nativeElement, 'top', `${v[1]}px`);
				this.renderer.setStyle(this.elementRef.nativeElement, 'z-index', 16);
			});
	}

	/**
	 * Обработчик события уничтожения компонента
	 */
	ngOnDestroy(): void {
		if (this.elContSubscription) {
			this.elContSubscription.unsubscribe();
			this.elContSubscription = null;
		}
	}
}
