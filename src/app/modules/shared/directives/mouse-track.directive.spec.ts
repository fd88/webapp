import { ElementRef, Renderer2, RendererStyleFlags2 } from '@angular/core';
import { TestBed } from "@angular/core/testing";
import { HttpClientModule } from "@angular/common/http";
import { MouseTrackDirective } from "@app/shared/directives/mouse-track.directive";

xdescribe('MouseTrackDirective', () => {
	let directive: MouseTrackDirective | any;

	beforeEach(() => {
		directive = new MouseTrackDirective({} as ElementRef, {} as Renderer2);

		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				Renderer2
			]
		});
	});

	it('should create an instance', () => {
		expect(directive).toBeTruthy();
	});

	it('test onMouseDownHandler', () => {
		spyOn(directive, 'onMouseDownHandler').and.callThrough();

		directive.onMouseDownHandler({clientX: 100, clientY: 200} as MouseEvent);
		expect(directive.onMouseDownHandler).toHaveBeenCalled();

		directive.isActive = true;
		directive._element = {};
		directive.renderer = {
			setStyle: (el: any, style: string, value: any, flags?: RendererStyleFlags2): void => {
			}
		};
		directive.onMouseDownHandler({clientX: 100, clientY: 200} as MouseEvent);
		expect(directive.onMouseDownHandler).toHaveBeenCalled();

	});

	it('test onMouseUpHandler', () => {
		spyOn(directive, 'onMouseUpHandler').and.callThrough();

		directive.onMouseUpHandler({} as MouseEvent);
		expect(directive.onMouseUpHandler).toHaveBeenCalled();

		directive.isActive = true;
		directive._element = {};
		directive.renderer = {
			setStyle: (el: any, style: string, value: any, flags?: RendererStyleFlags2): void => {
			}
		};
		directive.onMouseUpHandler({} as MouseEvent);
		expect(directive.onMouseUpHandler).toHaveBeenCalled();

	});



});
