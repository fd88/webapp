import { DeclensionWords, DeclineWordPipe } from '@app/shared/pipes/decline-word.pipe';

describe('DeclineWordPipe', () => {

	it('create an instance', () => {
		const pipe = new DeclineWordPipe();
		expect(pipe).toBeTruthy();
	});

	it('check words', () => {
		const pipe = new DeclineWordPipe();
		expect(pipe.transform(DeclensionWords.Event, 0)).toEqual('lottery.decline_words.event_2');
		expect(pipe.transform(DeclensionWords.Event, 1)).toEqual('lottery.decline_words.event_0');
		expect(pipe.transform(DeclensionWords.Event, 2)).toEqual('lottery.decline_words.event_1');
		expect(pipe.transform(DeclensionWords.Event, 5)).toEqual('lottery.decline_words.event_2');

		expect(pipe.transform(DeclensionWords.Mark, 0)).toEqual('lottery.decline_words.mark_2');
		expect(pipe.transform(DeclensionWords.Mark, 1)).toEqual('lottery.decline_words.mark_0');
		expect(pipe.transform(DeclensionWords.Mark, 2)).toEqual('lottery.decline_words.mark_1');
		expect(pipe.transform(DeclensionWords.Mark, 5)).toEqual('lottery.decline_words.mark_2');

		expect(pipe.transform(DeclensionWords.Mark2, 0)).toEqual('lottery.decline_words.mark2_2');
		expect(pipe.transform(DeclensionWords.Mark2, 1)).toEqual('lottery.decline_words.mark2_0');
		expect(pipe.transform(DeclensionWords.Mark2, 2)).toEqual('lottery.decline_words.mark2_1');
		expect(pipe.transform(DeclensionWords.Mark2, 5)).toEqual('lottery.decline_words.mark2_2');

		expect(pipe.transform('xxx', 5)).toEqual('xxx');
	});

});
