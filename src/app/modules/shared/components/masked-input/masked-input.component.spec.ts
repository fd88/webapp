import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MaskedInputComponent } from './masked-input.component';

describe('MaskedInputComponent', () => {
  let component: MaskedInputComponent | any;
  let fixture: ComponentFixture<MaskedInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MaskedInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaskedInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onFocusHandler', () => {
		spyOn(component, 'onFocusHandler').and.callThrough();
		const ev = new FocusEvent('onFocusHandler')
		const target = document.createElement('input')
		target.value = '1234';
		component.onFocusHandler({...ev, target});
		expect(component.onFocusHandler).toHaveBeenCalled();
	});

	it('test onBlurHandler', () => {
		spyOn(component, 'onBlurHandler').and.callThrough();
		component.onBlurHandler(new FocusEvent('onBlurHandler'));
		expect(component.onBlurHandler).toHaveBeenCalled();
	});

	it('test onClickHandler', () => {
		spyOn(component, 'onClickHandler').and.callThrough();
		const ev = new MouseEvent('onClickHandler');
		const target = document.createElement('input');
		component.onClickHandler({...ev, target});
		expect(component.onClickHandler).toHaveBeenCalled();
	});

	it('test onInputHandler', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new Event('onInputHandler');
		const target = document.createElement('input');
		target.value = '+38(012) 345-67-89';
		component.mask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
		component.onInputHandler({...ev, target});
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test registerOnChange', () => {
		spyOn(component, 'registerOnChange').and.callThrough();
		component.registerOnChange(null);
		expect(component.registerOnChange).toHaveBeenCalled();
	});

	it('test registerOnTouched', () => {
		spyOn(component, 'registerOnTouched').and.callThrough();
		component.registerOnTouched(null);
		expect(component.registerOnTouched).toHaveBeenCalled();
	});

	it('test setDisabledState', () => {
		spyOn(component, 'setDisabledState').and.callThrough();
		component.setDisabledState(true);
		expect(component.setDisabledState).toHaveBeenCalled();
	});

	it('test writeValue', () => {
		component.writeValue('+38(012) 345-67-89');
		expect(component.value).toEqual('+38(012) 345-67-89');
	});

	it('test registerOnValidatorChange', () => {
		spyOn(component, 'registerOnValidatorChange').and.callThrough();
		component.registerOnValidatorChange(() => {});
		expect(component.registerOnValidatorChange).toHaveBeenCalled();
	});

	it('test checkValidation method', () => {
		component.mask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
		expect(component.checkValidation('+38(012) A45-67-89')).toBeFalsy();
	});

	it('test value setter', () => {
		component.mask = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
		component.value = '+38(012) A45-67-89';
		expect(component._value).toEqual('+38(012) A45-67-89');
	});
});
