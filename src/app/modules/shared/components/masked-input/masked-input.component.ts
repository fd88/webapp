import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { timer } from 'rxjs';
import {
	AbstractControl,
	ControlValueAccessor,
	NG_VALIDATORS,
	NG_VALUE_ACCESSOR,
	ValidationErrors
} from '@angular/forms';

/**
 * Компонент маскированного ввода.
 */
@Component({
	selector: 'app-masked-input',
	templateUrl: './masked-input.component.html',
	styleUrls: ['./masked-input.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: MaskedInputComponent,
			multi: true
		},
		{
			provide: NG_VALIDATORS,
			useExisting: MaskedInputComponent,
			multi: true,
		}
	]
})
export class MaskedInputComponent implements  ControlValueAccessor {

	/**
	 * Геттер значения компонента.
	 */
	@Input()
	get value(): string {
		return this._value;
	}

	/**
	 * Сеттер значения компонента.
	 * @param v Значение.
	 */
	set value(v: string) {
		this._value = v;
	}

	/**
	 * Подпись к полю ввода
	 */
	@Input()
	label: string;

	/**
	 * Маска ввода
	 */
	@Input()
	mask: Array<string | RegExp>;

	/**
	 * Флаг состояния ошибочного ввода
	 */
	@Input()
	errorState = false;

	/**
	 * Флаг недоступности ввода
	 */
	@Input()
	isDisabled = false;

	/**
	 * Показывать маркер валидности
	 */
	@Input()
	showValidMark = false;

	/**
	 * Это поле пин-кода?
	 */
	@Input()
	isPinCode = false;

	/**
	 * Флаг валидности
	 */
	isValid = false;

	/**
	 * Ссылка на элемент ввода
	 */
	@ViewChild('txtInput', {static: false}) txtInput: ElementRef;

	/**
	 * Событие установки фокуса на поле ввода
	 */
	@Output() readonly focused = new EventEmitter<FocusEvent>();

	/**
	 * Событие потери фокуса полем ввода
	 */
	@Output() readonly blured = new EventEmitter<FocusEvent>();

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Значение компонента.
	 * @private
	 */
	private _value: string;

	/**
	 * Установить позицию курсора в поле ввода
	 * @param target Поле ввода
	 * @param caretPos Позиция курсора
	 * @private
	 */
	private static setCaretPosition(target: HTMLInputElement, caretPos: number): void {
		if (target['createTextRange']) {
			const range = target['createTextRange']();
			range.move('character', caretPos);
			range.select();
		} else if (target.selectionStart) {
			target.setSelectionRange(caretPos, caretPos);
		}
	}

	/**
	 * Унаследованное событие от базового класса.
	 * Вызывается в коллбеке при изменении значения UI-элемента
	 * @param value Передаваемое значение
	 */
	private onChange = (value: string) => {};

	/**
	 * Унаследованное событие от базового класса.
	 */
	private onTouched = () => {};


	// -----------------------------
	//  ControlValueAccessor
	// -----------------------------
	/**
	 * Метод, который вызывается при изменении значения UI-элемента
	 * @param fn Передаваемая callback-функция
	 */
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	/**
	 * Метод, который вызывается при касании к UI-элементу
	 * @param fn Передаваемая callback-функция
	 */
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

	/**
	 * Метод для установки недоступности компонента
	 * @param isDisabled Флаг недоступности
	 */
	setDisabledState(isDisabled: boolean): void {
	}

	/**
	 * Метод для программного присвоения значения компоненту
	 * @param obj Значение
	 */
	writeValue(obj: string): void {
		this._value = obj;
	}

	// -----------------------------
	//  Validator
	// -----------------------------
	/**
	 * Регистрирует функцию обратного вызова для выполнения при изменении входных данных валидатора
	 * @param fn Функция обратного вызова
	 */
	registerOnValidatorChange(fn: () => void): void {
	}

	/**
	 * Валидация компонента
	 * @param control Компонент
	 */
	validate(control: AbstractControl): ValidationErrors | null {
		return this.isValid ? null : {not_matched: true};
	}

	/**
	 * Установить фокус на поле ввода
	 */
	setFocus(): void {
		(this.txtInput.nativeElement as HTMLInputElement).focus();
	}

	/**
	 * Обработчик события установки фокуса на поле ввода
	 * @param event Передаваемое событие
	 */
	onFocusHandler(event: FocusEvent): void {
		this.fixCursorPos(event.target as HTMLInputElement);
		this.focused.emit(event);
	}

	/**
	 * Обработчик события потери фокуса полем ввода
	 * @param event Передаваемое событие
	 */
	onBlurHandler(event: FocusEvent): void {
		this.blured.emit(event);
	}

	/**
	 * Обработчик события нажатия кнопки мыши на поле ввода
	 * @param event Передаваемое событие
	 */
	onClickHandler(event: MouseEvent): void {
		this.fixCursorPos(event.target as HTMLInputElement);
	}

	/**
	 * Обработчик события ввода в поле ввода
	 * @param event Передаваемое событие
	 */
	onInputHandler(event: Event): void {
		const val = (event.target as HTMLInputElement).value;
		this.isValid = this.checkValidation(val);
		this.onChange(val);
		this.onTouched();
	}

	/**
	 * Проверка валидности введенного значения
	 * @param st Введенное значение
	 * @private
	 */
	private checkValidation(st: string): boolean {
		if (!st) {
			return false;
		}
		const chars = st.split('');
		for (let i = 0; i < this.mask.length; i++) {
			if ((this.mask[i] !== chars[i]) && !((this.mask[i] as RegExp).test(chars[i]))) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Исправить позицию курсора в поле ввода
	 * @param inputElem Поле ввода
	 * @private
	 */
	private fixCursorPos(inputElem: HTMLInputElement): void {
		const txtValue = inputElem.value;
		const txtValueArr = txtValue.split('')
			.map(elem => isNaN(parseInt(elem, 10)) ? '0' : '1');
		const cursorPos = txtValueArr.lastIndexOf('1') + 1;
		const tmr = timer(200)
			.subscribe(() => {
				MaskedInputComponent.setCaretPosition(inputElem, cursorPos);
				tmr.unsubscribe();
			});
	}

	ngOnInit(): void {
		timer(100)
			.subscribe(() => {
				this.isValid = this.checkValidation(this.value);
				if (!!this.value) {
					this.onChange(this.value);
				}
			})
	}
}
