import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropDownListComponent } from './drop-down-list.component';
import {TranslateModule} from "@ngx-translate/core";
import {CUSTOM} from "@app/core/dialog/components/two-buttons-with-dropdown/two-buttons-with-dropdown.component";
import {TerminalRoles} from "@app/core/services/store/operator";

describe('DropDownListComponent', () => {
  let component: DropDownListComponent;
  let fixture: ComponentFixture<DropDownListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
	  imports: [
		  TranslateModule.forRoot()
	  ],
      declarations: [ DropDownListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropDownListComponent);
    component = fixture.componentInstance;
	component.dataProvider = [
		{label: 'header.shutdown_message_0', value: CUSTOM},
		{label: 'header.shutdown_message_1', value: '1', rolesList: [TerminalRoles.OPERATOR]},
		{label: 'header.shutdown_message_2', value: '2', rolesList: [TerminalRoles.OPERATOR]},
		{label: 'header.shutdown_message_3', value: '3', rolesList: [TerminalRoles.OPERATOR]}
	];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test onClickTitleHandler', () => {
	  component.isExpandedList = false;
	  component.onClickTitleHandler();
	  expect(component.isExpandedList).toBeTruthy();
  });

  it('test onClickListItemHandler', () => {
	component.isExpandedList = false;
	const ev = new MouseEvent('onClickListItemHandler');
	const target = document.createElement('div')
	target.setAttribute('value', '3');
	component.onClickListItemHandler({ ...ev, target });
	expect(component.selectedItem).toEqual(component.dataProvider[3]);
  });

	it('test selectItem method', () => {
		component.selectItem(component.dataProvider[2]);
		expect(component.selectedItem).toEqual(component.dataProvider[2]);
	});

	it('test trackByFn method', () => {
		expect(component.trackByFn(2, component.dataProvider[2])).toEqual('2');
	});

	it('test onDocumentClickHandler', () => {
		const ev = new MouseEvent('onDocumentClickHandler');
		const target = document.createElement('div');
		component.closeOnLostFocus = true;
		component.isExpandedList = true;
		component.onDocumentClickHandler({...ev, target});
		expect(component.isExpandedList).toBeFalsy();
	});


});
