import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { OneButtonCustomComponent } from '@app/shared/components/one-button-custom/one-button-custom.component';
import { MslComponentDirective } from '@app/shared/directives/msl-component.directive';
import { CloseButtonComponent } from '@app/shared/components/close-button/close-button.component';


describe('OneButtonCustomComponent', () => {
	let fixture: ComponentFixture<OneButtonCustomComponent>;
	let component: OneButtonCustomComponent;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot(),
			],
			declarations: [
				OneButtonCustomComponent,
				MslComponentDirective,
				CloseButtonComponent,
			],
			providers: [
			]
		})
			.compileComponents()
			.catch(err => {
				console.error(err);
			});

		fixture = TestBed.createComponent(OneButtonCustomComponent);
		component = fixture.debugElement.componentInstance as OneButtonCustomComponent;

	}));

	it('test hide function', () => {
		component.isVisible = true;
		component.onConfirm(null);
		expect(component.isVisible).toBeFalsy();
	});

	it('test onConfirm function', () => {
		component.isVisible = true;
		component.onConfirm(null);
		expect(component.isVisible).toBeFalsy();

		component.canHide = false;
		component.onConfirm(null);
		expect(component.isVisible).toBeFalsy();

	});

	it('test onCancel function', () => {
		component.isVisible = true;
		component.onCancel(null);
		expect(component.isVisible).toBeFalsy();
	});




});
