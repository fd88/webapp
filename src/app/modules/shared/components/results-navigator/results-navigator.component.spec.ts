import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ResultNavigationDirection, ResultsNavigatorComponent} from './results-navigator.component';
import {SharedModule} from "@app/shared/shared.module";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {TranslateModule} from "@ngx-translate/core";
import {CommonModule} from "@angular/common";
import {CoreModule} from "@app/core/core.module";
import {GameResultsService} from "@app/core/services/results/game-results.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {LogService} from "@app/core/net/ws/services/log/log.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CalendarDateInputComponent} from "@app/calendar/components/calendar-date-input/calendar-date-input.component";

describe('ResultsNavigatorComponent', () => {
  let component: ResultsNavigatorComponent | any;
  let fixture: ComponentFixture<ResultsNavigatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
	imports: [
		SharedModule,
		RouterTestingModule.withRoutes([]),
		HttpClientModule,
		TranslateModule.forRoot({}),
		CommonModule,
		CoreModule
	],
      declarations: [
		  ResultsNavigatorComponent,
		  CalendarDateInputComponent
	  ],
		providers: [
			{
				provide: DialogContainerService,
				useClass: DialogContainerServiceStub
			},
			LogService,
			HttpClient
		]
    })
    .compileComponents();

	TestBed.inject(LogService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsNavigatorComponent);
    component = fixture.componentInstance;
	component.service = TestBed.inject(GameResultsService);
	component.viewResults = {
		drawNumber:'1246',
		drawDate:'2019-02-25 14:44:32',
		extraInfo:'Джекпот наступн.тиражу - 3,000,000 грн.'
	};
	component.gameCode = LotteryGameCode.Zabava;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onClickPrintResultsHandler', () => {
		spyOn(component, 'onClickPrintResultsHandler').and.callThrough();
		component.onClickPrintResultsHandler({ copiesCount: 1 });
		expect(component.onClickPrintResultsHandler).toHaveBeenCalled();
	});

	it('test onDateSelectedHandler', () => {
		spyOn(component, 'onDateSelectedHandler').and.callThrough();
		component.onDateSelectedHandler(new Date('2019-02-25 14:44:32'));
		expect(component.onDateSelectedHandler).toHaveBeenCalled();
	});

	it('test onDateSelectedHandler 2', () => {
		component.onClickNextOrPrevButtonHandler(ResultNavigationDirection.Previous);
		expect(component.currentPage).toEqual(2);
	});

	it('test onSelectedResultHandler', () => {
		spyOn(component, 'onSelectedResultHandler').and.callThrough();
		component.onSelectedResultHandler(0);
		expect(component.onSelectedResultHandler).toHaveBeenCalled();
	});

	it('test onSelectedLotteryTypeHandler', () => {
		spyOn(component, 'onSelectedLotteryTypeHandler').and.callThrough();
		const ev = new Event('onSelectedLotteryTypeHandler');
		const target = document.createElement('input');
		target.value = LotteryGameCode.Zabava.toString();
		component.onSelectedLotteryTypeHandler({ ...ev, target });
		expect(component.onSelectedLotteryTypeHandler).toHaveBeenCalled();
	});
});
