import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsSetComponent } from '@app/shared/components/buttons-set/buttons-set.component';
import {TranslateModule} from '@ngx-translate/core';
import {ButtonGroupMode} from "@app/shared/components/buttons-group/buttons-group.component";

xdescribe('ButtonsSetComponent', () => {
	let component: ButtonsSetComponent | any;
	let fixture: ComponentFixture<ButtonsSetComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				ButtonsSetComponent
			]
		})
			.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ButtonsSetComponent);
		component = fixture.componentInstance;
		component.buttonsCount = 1;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test #1 onButtonClickHandler', () => {
		spyOn(component, 'onButtonClickHandler').and.callThrough();
		const ev = new Event('onButtonClickHandler');
		const target = document.createElement('button');
		target.setAttribute('bg-index', '0');
		component.onButtonClickHandler({...ev, target});
		expect(component.onButtonClickHandler).toHaveBeenCalled();
	});

	it('test #2 onButtonClickHandler', () => {
		component.disabled = true;
		spyOn(component, 'onButtonClickHandler').and.callThrough();
		const ev = new Event('onButtonClickHandler');
		const target = document.createElement('button');
		target.setAttribute('bg-index', '0');
		component.onButtonClickHandler({...ev, target});
		expect(component.onButtonClickHandler).toHaveBeenCalled();
	});

	it('test clearButtons method', () => {
		component.buttonsCount = 0;
		spyOn(component, 'clearButtons').and.callThrough();
		component.clearButtons();
		expect(component.clearButtons).toHaveBeenCalled();
	});

	it('test getValueByButton method #1', () => {
		component.buttonsCount = 1;
		component.buttons[0].label = '1';
		component.valuesArray = ['1'];
		component.enabledButtonsCount = 1;
		expect(component.getValueByButton(component.buttons[0])).toEqual('1');
	});

	it('test getValueByButton method #2', () => {
		component.buttonsCount = 1;
		component.buttons[0].label = '1';
		expect(component.getValueByButton(component.buttons[0])).toEqual('1');
	});

	it('test getValueByButton method #3', () => {
		component.buttons = [];
		expect(component.getValueByButton({label: '1', index: 5, selected: false, disabled: false})).toBeUndefined();
	});

	it('test changeButtonState method #4', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.changeButtonState(0, true);
		expect(component.buttons[0].selected).toBeTruthy();
	});

	it('test changeButtonState method #5', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.workMode = ButtonGroupMode.range_deselect_2;
		component.changeButtonState(0, false);
		expect(component.buttons[0].selected).toBeFalsy();
	});

	it('test changeButtonState method #6', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.countSelectedButtons = 1;
		component.workMode = ButtonGroupMode.custom;
		component.changeButtonState(0, false);
		expect(component.buttons[0].selected).toBeFalsy();
	});

	it('test changeButtonState method #7', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.countSelectedButtons = 1;
		component.workMode = ButtonGroupMode.radio;
		component.changeButtonState(0, false);
		expect(component.buttons[0].selected).toBeTruthy();
	});

	it('test changeButtonState method #8', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.countSelectedButtons = 1;
		component.workMode = ButtonGroupMode.radio_deselect;
		component.changeButtonState(0, false);
		expect(component.buttons[0].selected).toBeFalsy();
	});

	it('test changeButtonState method #9', () => {
		component.buttons = [{label: '1', index: 0, selected: false, disabled: false}];
		component.countSelectedButtons = 0;
		component.workMode = ButtonGroupMode.no_selection;
		component.changeButtonState(0, false);
		expect(component.buttons[0].selected).toBeFalsy();
	});

	it('test changeButtonState method #10', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.countSelectedButtons = 1;
		component.workMode = ButtonGroupMode.range_deselect;
		component.changeButtonState(0, false);
		expect(component.buttons[0].selected).toBeFalsy();
	});

	it('test initSelectedProperty', () => {
		component.buttons = [{label: '1', index: 0, selected: true, disabled: false}];
		component.countSelectedButtons = 1;

		expect(component.initSelectedProperty(0)).toBeFalsy();
	});

});
