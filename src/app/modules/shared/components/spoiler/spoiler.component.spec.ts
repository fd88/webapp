import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoilerComponent } from './spoiler.component';

describe('SpoilerComponent', () => {
  let component: SpoilerComponent;
  let fixture: ComponentFixture<SpoilerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoilerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test closeClick handler', () => {
		spyOn(component, 'closeClick').and.callThrough();
		component.closeClick();
		expect(component.closeClick).toHaveBeenCalled();
	});

	it('test changeState method', () => {
		component.opened = false;
		component.changeState();
		expect(component.opened).toBeTruthy();
	});

	it('test changeState method 2', () => {
		component.opened = true;
		component.changeState();
		expect(component.opened).toBeFalsy();
	});
});
