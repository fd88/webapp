import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';

/**
 * Компонент раскрывающегося блока с заголовком.
 */
@Component({
	selector: 'app-spoiler',
	templateUrl: './spoiler.component.html',
	styleUrls: ['./spoiler.component.scss']
})
export class SpoilerComponent  {

	/**
	 * Заголовок блока.
	 */
	@Input() title = '';

	/**
	 * Флаг, отвечающий за состояние блока.
	 */
	@Input() @HostBinding('class.opened') opened = false;

	/**
	 * Флаг, отвечающий за отображение крестика закрытия в заголовке.
	 */
	@Input() crossMark = false;

	/**
	 * Событие, которое будет вызвано при закрытии блока.
	 */
	@Output() readonly closed = new EventEmitter();

	/**
	 * Событие, которое будет вызвано при разворачивании блока.
	 */
	@Output() readonly expanded = new EventEmitter();

	/**
	 * Функция, которая будет вызвана при закрытии блока.
	 */
	closeClick(): void {
		this.closed.emit();
	}

	/**
	 * Функция, которая будет вызвана при разворачивании или сворачивании блока.
	 */
	changeState(): void {
		this.opened = this.crossMark || !this.opened;
		if (this.opened) {
			this.expanded.emit();
		} else {
			this.closed.emit();
		}
	}


}
