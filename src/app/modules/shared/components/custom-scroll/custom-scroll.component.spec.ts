import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomScrollComponent } from './custom-scroll.component';

describe('CustomScrollComponent', () => {
  let component: CustomScrollComponent;
  let fixture: ComponentFixture<CustomScrollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomScrollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test scrollToBottom method', () => {
		spyOn(component, 'scrollToBottom').and.callThrough();
		component.scrollToBottom();
		expect(component.scrollToBottom).toHaveBeenCalled();
	});

	it('test onClickButtonHandler method', () => {
		spyOn(component, 'onClickButtonHandler').and.callThrough();

		component.onClickButtonHandler(100);
		expect(component.onClickButtonHandler).toHaveBeenCalled();
	});
});
