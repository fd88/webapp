import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiscalReceiptComponent } from './fiscal-receipt.component';

describe('FiscalReceiptComponent', () => {
  let component: FiscalReceiptComponent;
  let fixture: ComponentFixture<FiscalReceiptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiscalReceiptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
