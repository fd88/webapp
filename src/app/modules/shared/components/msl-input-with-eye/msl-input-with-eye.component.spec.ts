import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MslInputWithEyeComponent } from './msl-input-with-eye.component';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { HttpService } from '@app/core/net/http/services/http.service';
import {HttpClientModule} from "@angular/common/http";

describe('MslInputWithEyeComponent', () => {
  let component: MslInputWithEyeComponent | any;
  let fixture: ComponentFixture<MslInputWithEyeComponent>;

  	beforeEach(waitForAsync(() => {
    	TestBed.configureTestingModule({
			imports: [
				SharedModule,
				TranslateModule.forRoot(),
				HttpClientModule
			],
      		declarations: [  ],
			providers: [
				LogService,
				AppStoreService,
				HttpService
			]
    	})
    	.compileComponents();

		TestBed.inject(LogService);
  	}));

  	beforeEach(() => {
    	fixture = TestBed.createComponent(MslInputWithEyeComponent);
    	component = fixture.componentInstance;
    	fixture.detectChanges();
  	});

	it('test onChange method', () => {
		spyOn(component, 'onChange').and.callThrough();
		component.onChange('test');
		expect(component.onChange).toHaveBeenCalled();
	});

	it('test onTouched method', () => {
		spyOn(component, 'onTouched').and.callThrough();
		component.onTouched();
		expect(component.onTouched).toHaveBeenCalled();
	});

	it('test onInputHandler method', () => {
		const input = document.createElement('input');
		spyOn(component, 'onInputHandler').and.callThrough();

		input.value = '14739458';
		component.onInputHandler({
			target: input
		});
		expect(component.onInputHandler).toHaveBeenCalled();

		input.value = '';
		component.onInputHandler({
			target: input
		});
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onFocus method', () => {
		spyOn(component, 'onFocus').and.callThrough();
		component.onFocus(null);
		expect(component.onFocus).toHaveBeenCalled();
	});

	it('test checkAndEmitValueChangeEvent method', () => {
		component.value = '12345';
		component.checkAndEmitValueChangeEvent('12345');
		expect(component.value).toEqual('12345');
	});

	it('test setDisabledState method', () => {
		spyOn(component, 'setDisabledState').and.callThrough();
		component.setDisabledState(true);
		expect(component.setDisabledState).toHaveBeenCalled();
	});

	it('test validate method', () => {
		const errors = {'test': 'Тест'};
		expect(component.validate({valid: false, errors})).toEqual(errors);
	});

	it('test onEyeIconClickHandler method', () => {
		component.value = '';
		component.secured = true;
		component.onEyeIconClickHandler();
		expect(component.secured).toBeTruthy();

		component.value = '121212';
		component.secured = true;
		component.onEyeIconClickHandler();
		expect(component.secured).toBeFalsy();

		component.value = '121212';
		component.secured = false;
		component.onEyeIconClickHandler();
		expect(component.secured).toBeTruthy();
	});

	it('test onKeyDownHandler method', () => {
		spyOn(component, 'onKeyDownHandler').and.callThrough();
		const ev = new KeyboardEvent('onKeyDownHandler');
		component.onKeyDownHandler({...ev, key: 'Enter'});
		expect(component.onKeyDownHandler).toHaveBeenCalled();
	});
});
