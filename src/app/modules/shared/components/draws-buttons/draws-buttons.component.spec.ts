import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DrawsButtonsComponent } from '@app/shared/components/draws-buttons/draws-buttons.component';
import {DRAWS_FOR_GAME_ZABAVA} from "../../../features/mocks/zabava-draws";

describe('DrawsButtonsComponent', () => {
	let component: DrawsButtonsComponent | any;
	let fixture: ComponentFixture<DrawsButtonsComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			declarations: [
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DrawsButtonsComponent);
		component = fixture.componentInstance;
		component.drawsList = [...DRAWS_FOR_GAME_ZABAVA.lottery.draws];
		fixture.detectChanges();
	});

	it('should create', () => {
		component.results = [{
			draw_code:'1246',
			draw_name: '1246',
			drawing_date_begin: '2019-02-25 14:44:32',
			drawing_date_end: '2029-02-25 14:44:32',
			winning_date_end: '2039-02-25 14:44:32',
			drawing:[
				{
					extra_info:'Джекпот наступн.тиражу - 3,000,000 грн.',
					name:'Основний розiграш',
					win_comb:'-',
					winComb:[
						'-'
					],
					win_cat: [],
					data: {
						events: []
					}
				},
				{
					extra_info:'',
					name:'Конкурс Парочка',
					win_cat:[
						{
							'name':'Повний збiг',
							'win_cnt':'0',
							'win_sum':'300000.00'
						},
						{
							'name':'Будь-який кут з 5-ти',
							'win_cnt':'1',
							'win_sum':'7500.00'
						},
						{
							'name':'Будь-яка лiнiя з 3-х',
							'win_cnt':'8',
							'win_sum':'100.00'
						},
						{
							'name':'Верхня вершина',
							'win_cnt':'364',
							'win_sum':'5.00'
						}
					],
					win_comb:'02 05 19 08 11 37 42 50 72',
					winComb:[
						'02',
						'05',
						'19',
						'08',
						'11',
						'37',
						'42',
						'50',
						'72'
					],
					data: {
						events: []
					}
				},
				{
					extra_info:'',
					name:'Конкурс Багаті та відомі',
					win_comb:'202',
					winComb:[
						'202'
					],
					win_cat: [],
					data: {
						events: []
					}
				}
			]
		}];
		expect(component).toBeTruthy();
	});

	it('test onSelectedDrawHandler', () => {
		component.onSelectedDrawHandler(null, 0);
		expect(component.selectedDrawIndex).toEqual(0);
	});

	it('test activeDrawCount getter', () => {
		expect(component.activeDrawCount).toEqual(0);
	});

	it('test updateButtons', () => {
		component.drawsList[0].isDrawActive = true;
		spyOn(component, 'updateButtons').and.callThrough();
		component.updateButtons();
		expect(component.updateButtons).toHaveBeenCalled();
	});


});
