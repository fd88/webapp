import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LogoViewStyle, MslGameLogoComponent} from './msl-game-logo.component';
import {TranslateModule} from '@ngx-translate/core';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {LotteriesGroupCode, LotteryGameCode} from "@app/core/configuration/lotteries";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('MslGameLogoComponent', () => {
	let component: MslGameLogoComponent | any;
	let fixture: ComponentFixture<MslGameLogoComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot(),
				CoreModule,
				HttpClientModule
			],
			declarations: [
				MslGameLogoComponent
			],
			providers: [
				LogService,
				HttpService,
				HttpClient
			]
		})
		.compileComponents();

		TestBed.inject(LogService);
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MslGameLogoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test parseCode method #1', () => {
		const realAnswer = component.parseCode(LotteryGameCode.Gonka);
		const expectedAnswer = {code: LotteryGameCode.Gonka, imgUrl: '/assets/img/pic/logo_gg.png',
			title: 'lottery.race_for_money.loto_gonka', groupCode: 0, logoItemClass: `game-logo__image_game-code_${LotteryGameCode.Gonka}`};
		expect(realAnswer).toEqual(expectedAnswer);
	});

	it('test parseCode method #2', () => {
		const realAnswer = component.parseCode(LotteryGameCode.Tip);
		const expectedAnswer = {code: LotteryGameCode.Tip, imgUrl: '/assets/img/pic/logo_tiptop.png',
			title: 'lottery.tiptop.game_name', groupCode: 0, logoItemClass: `game-logo__image_game-code_${LotteryGameCode.Tip}`};
		expect(realAnswer).toEqual(expectedAnswer);
	});

	it('test parseCode method #3', () => {
		const realAnswer = component.parseCode(LotteryGameCode.Top);
		const expectedAnswer = {code: LotteryGameCode.Top, imgUrl: '/assets/img/pic/logo_tiptop.png',
			title: 'lottery.tiptop.game_name', groupCode: 0, logoItemClass: `game-logo__image_game-code_${LotteryGameCode.Top}`};
		expect(realAnswer).toEqual(expectedAnswer);
	});

	it('test parseCode method #4', () => {
		const realAnswer = component.parseCode(LotteryGameCode.TML_BML);
		const expectedAnswer = {code: LotteryGameCode.TML_BML, imgUrl: '/assets/img/pic/logo_styrachka.png',
			title: 'lottery.tmlbml.game_name', groupCode: 0, logoItemClass: `game-logo__image_game-code_${LotteryGameCode.TML_BML}`};
		expect(realAnswer).toEqual(expectedAnswer);
	});

	it('test parseCode method #5', () => {
		const realAnswer = component.parseCode(LotteryGameCode.Zabava);
		const expectedAnswer = {code: LotteryGameCode.Zabava, imgUrl: '/assets/img/pic/logo_zabava.svg',
			title: 'lottery.zabava.loto_zabava', groupCode: 0, logoItemClass: `game-logo__image_game-code_${LotteryGameCode.Zabava}`};
		expect(realAnswer).toEqual(expectedAnswer);
	});

	it('test parseCode method #6', () => {
		expect(component.parseCode(LotteryGameCode.Undefined)).toBeUndefined();
	});

	it('test groupCode setter #1', () => {
		component.groupCode = LotteriesGroupCode.EInstant;
		const expected = {code: 0, groupCode: LotteriesGroupCode.EInstant, imgUrl:'/assets/img/pic/logo_instant.png',
			title: 'lottery.instant.loto_momentary', logoItemClass: `group-code-${LotteriesGroupCode.EInstant}`};
		expect(component.model).toEqual(expected);
	});

	it('test groupCode setter #2', () => {
		component.groupCode = LotteriesGroupCode.VBL;
		const expected = {code: 0, groupCode: LotteriesGroupCode.VBL, imgUrl:'/assets/img/pic/golden-triumph_logo.png',
			title: 'lottery.instant.loto_golden_triumph', logoItemClass: `group-code-${LotteriesGroupCode.VBL}`};
		expect(component.model).toEqual(expected);
	});

	it('test parseGroupCode method', () => {
		expect(component.parseGroupCode(undefined)).toBeUndefined();
	});

	it('test parseGroupCode method 2', () => {
		component.logoViewStyle = LogoViewStyle.DemoList;
		expect(component.parseGroupCode(LotteriesGroupCode.EInstant)).toEqual({
			code: 0,
			groupCode: 1,
			imgUrl: '/assets/img/pic/logo_instant.png',
			title: 'lottery.instant.loto_momentary',
			logoItemClass: 'group-code-1'
		});
	});
});
