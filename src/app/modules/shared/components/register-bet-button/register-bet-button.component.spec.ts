import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RegisterBetButtonComponent } from '@app/shared/components/register-bet-button/register-bet-button.component';
import { TranslateModule } from '@ngx-translate/core';
import { MslCurrencyPipe } from '@app/shared/pipes/msl-currency.pipe';

xdescribe('RegisterBetButtonComponent', () => {
	let component: RegisterBetButtonComponent;
	let fixture: ComponentFixture<RegisterBetButtonComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				TranslateModule.forRoot()
			],
			declarations: [
				RegisterBetButtonComponent,
				MslCurrencyPipe
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(RegisterBetButtonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
