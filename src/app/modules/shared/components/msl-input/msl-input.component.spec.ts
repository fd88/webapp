import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MslInputComponent } from './msl-input.component';

describe('MslInputComponent', () => {
  let component: MslInputComponent;
  let fixture: ComponentFixture<MslInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MslInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MslInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onInputChangeHandler', () => {
		const ev = new Event('onInputChangeHandler');
		const target = document.createElement('input');
		target.value = 'test';
		component.onInputChangeHandler({ ...ev, target });
		expect(component.value).toEqual('test');
	});

	it('test updateValue', () => {
		component.updateValue('test');
		expect(component.value).toEqual('test');
	});

	it('test onEnterPressedHandler', () => {
		spyOn(component, 'onEnterPressedHandler').and.callThrough();
		const ev = new Event('onInputChangeHandler');
		component.onEnterPressedHandler(ev);
		expect(component.onEnterPressedHandler).toHaveBeenCalled();
	});

	it('test setDisabledState', () => {
		spyOn(component, 'setDisabledState').and.callThrough();
		component.setDisabledState(true);
		expect(component.setDisabledState).toHaveBeenCalled();
	});
});
