import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MslInputPinComponent } from './msl-input-pin.component';

describe('MslInputPinComponent', () => {
  let component: MslInputPinComponent;
  let fixture: ComponentFixture<MslInputPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MslInputPinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MslInputPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test onModelChange', () => {
		spyOn(component, 'onModelChange').and.callThrough();
		component.onModelChange('1234');
		expect(component.onModelChange).toHaveBeenCalled();
	});

	it('test onClearHandler', () => {
		component.onClearHandler();
		expect(component.value).toEqual('');
	});

	it('test onFocusHandler', () => {
		spyOn(component, 'onFocusHandler').and.callThrough();
		const ev = new FocusEvent('onFocusHandler')
		const target = document.createElement('input')
		target.value = '1234';
		component.onFocusHandler({...ev, target});
		expect(component.onFocusHandler).toHaveBeenCalled();
	});

	it('test onBlurHandler', () => {
		spyOn(component, 'onBlurHandler').and.callThrough();
		component.onBlurHandler(new FocusEvent('onBlurHandler'));
		expect(component.onBlurHandler).toHaveBeenCalled();
	});

	it('test onClickHandler', () => {
		spyOn(component, 'onClickHandler').and.callThrough();
		const ev = new MouseEvent('onClickHandler');
		const target = document.createElement('input');
		component.onClickHandler({...ev, target});
		expect(component.onClickHandler).toHaveBeenCalled();
	});

	it('test onInputHandler', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		component.onInputHandler(new Event('onInputHandler'));
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test registerOnChange', () => {
		spyOn(component, 'registerOnChange').and.callThrough();
		component.registerOnChange(null);
		expect(component.registerOnChange).toHaveBeenCalled();
	});

	it('test registerOnTouched', () => {
		spyOn(component, 'registerOnTouched').and.callThrough();
		component.registerOnTouched(null);
		expect(component.registerOnTouched).toHaveBeenCalled();
	});

	it('test setDisabledState', () => {
		spyOn(component, 'setDisabledState').and.callThrough();
		component.setDisabledState(true);
		expect(component.setDisabledState).toHaveBeenCalled();
	});

	it('test writeValue', () => {
		component.writeValue('1234');
		expect(component.value).toEqual('1234');
	});

	it('test registerOnValidatorChange', () => {
		spyOn(component, 'registerOnValidatorChange').and.callThrough();
		component.registerOnValidatorChange(() => {});
		expect(component.registerOnValidatorChange).toHaveBeenCalled();
	});
});
