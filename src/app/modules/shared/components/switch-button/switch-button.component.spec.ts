import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SwitchButtonComponent } from './switch-button.component';
import {TranslateModule} from "@ngx-translate/core";

xdescribe('SwitchButtonComponent', () => {
  let component: SwitchButtonComponent;
  let fixture: ComponentFixture<SwitchButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    	declarations: [ SwitchButtonComponent ],
		imports: [
			TranslateModule.forRoot({})
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
