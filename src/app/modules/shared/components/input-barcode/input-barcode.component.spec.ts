import {ComponentFixture, TestBed} from '@angular/core/testing';

import {InputBarcodeComponent, InputBarcodeType} from './input-barcode.component';
import {TranslateModule} from "@ngx-translate/core";
import {DRAWS_FOR_GAME_100} from "../../../features/mocks/game100-draws";
import {ButtonsGroupComponent} from "@app/shared/components/buttons-group/buttons-group.component";
import {SharedModule} from "@app/shared/shared.module";
import {TMLBarcode} from "@app/core/barcode/tml-barcode";

describe('InputBarcodeComponent', () => {
  let component: InputBarcodeComponent;
  let fixture: ComponentFixture<InputBarcodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			SharedModule
		],
      declarations: [
		  InputBarcodeComponent,
		  ButtonsGroupComponent
	  ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputBarcodeComponent);
    component = fixture.componentInstance;
	component.draws = DRAWS_FOR_GAME_100.draws;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test setBarcode method', () => {
		spyOn(component, 'setBarcode').and.callThrough();
		component.isManualInput = false;
		component.setBarcode('0661123412341234');
		expect(component.setBarcode).toBeTruthy();
	});

	it('test setBarcode method 2', () => {
		spyOn(component, 'setBarcode').and.callThrough();
		component.isManualInput = true;
		component.setBarcode('0661123412341234');
		expect(component.setBarcode).toBeTruthy();
	});

	it('test onChangeInputModeHandler', () => {
		spyOn(component, 'onChangeInputModeHandler').and.callThrough();
		component.onChangeInputModeHandler({
			index: 1,
			label: 'test',
			selected: false,
			disabled: false
		});
		expect(component.onChangeInputModeHandler).toBeTruthy();
	});

	it('test onClickManualEnterHandler', () => {
		spyOn(component, 'onClickManualEnterHandler').and.callThrough();
		component.isRangeMode = true;
		component.onClickManualEnterHandler({
			index: 1,
			label: 'test',
			selected: true,
			disabled: false
		});
		expect(component.onClickManualEnterHandler).toBeTruthy();
	});

	it('test onClickManualEnterHandler 2', () => {
		spyOn(component, 'onClickManualEnterHandler').and.callThrough();
		component.isRangeMode = false;
		component.onClickManualEnterHandler({
			index: 1,
			label: 'test',
			selected: true,
			disabled: false
		});
		expect(component.onClickManualEnterHandler).toBeTruthy();
	});

	it('test onClickManualEnterHandler 3', () => {
		spyOn(component, 'onClickManualEnterHandler').and.callThrough();
		component.isRangeMode = false;
		component.onClickManualEnterHandler({
			index: 1,
			label: 'test',
			selected: false,
			disabled: false
		});
		expect(component.onClickManualEnterHandler).toBeTruthy();
	});

	it('test onManualBarcodeEnteredHandler', () => {
		spyOn(component, 'onManualBarcodeEnteredHandler').and.callThrough();
		component.onManualBarcodeEnteredHandler('0661123412341234', document.createElement('input'));
		expect(component.onManualBarcodeEnteredHandler).toHaveBeenCalled();
	});

	it('test onInputHandler', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		target.value = '0661123412341234';
		component.barcodeType = InputBarcodeType.TMLBML;
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 2', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047123412341234';
		component.isRangeMode = true;
		component.isManualInput = true;
		component.kbdPhase1 = {
			nativeElement: {

			}
		};
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 3', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047123412351234';
		component.currentInputPhase = [new TMLBarcode('0047123412341234')]
		component.isRangeMode = true;
		component.isManualInput = true;
		component.kbdPhase1 = {
			nativeElement: {

			}
		};
		component.kbdPhase2 = {
			nativeElement: {

			}
		};
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});


	it('test onInputHandler 4', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047123412351234';
		component.currentInputPhase = [new TMLBarcode('0048123412341234')];
		component.isRangeMode = true;
		component.isManualInput = true;
		component.kbdPhase1 = {
			nativeElement: {

			}
		};
		component.kbdPhase2 = {
			nativeElement: {

			}
		};
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 5', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047223412341234';
		component.currentInputPhase = [new TMLBarcode('0047123412341234')];
		component.isRangeMode = true;
		component.isManualInput = true;
		component.kbdPhase1 = {
			nativeElement: {

			}
		};
		component.kbdPhase2 = {
			nativeElement: {

			}
		};
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 6', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047123412341234';
		component.currentInputPhase = [new TMLBarcode('0047123412341234')];
		component.isRangeMode = true;
		component.isManualInput = true;
		component.kbdPhase1 = {
			nativeElement: {

			}
		};
		component.kbdPhase2 = {
			nativeElement: {

			}
		};
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 6', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047123412341234';
		component.currentInputPhase = [new TMLBarcode('0047123412341234')];
		component.isRangeMode = false;
		component.isManualInput = true;
		component.kbdSingle = {
			nativeElement: {

			}
		};
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onInputHandler 7', () => {
		spyOn(component, 'onInputHandler').and.callThrough();
		const ev = new InputEvent('onInputHandler');
		const target = document.createElement('input');
		component.barcodeType = InputBarcodeType.TMLBML;
		target.value = '0047123412341234';
		component.currentInputPhase = [new TMLBarcode('0047123412341234')];
		component.isRangeMode = false;
		component.isManualInput = true;
		component.kbdSingle = {
			nativeElement: {

			}
		};
		component.isNeededDrawsCodeCheck = true;
		component.onInputHandler({...ev, target})
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onClickCancelHandler', () => {
		spyOn(component, 'onClickCancelHandler').and.callThrough();
		component.onClickCancelHandler();
		expect(component.onClickCancelHandler).toHaveBeenCalled();
	});


});
