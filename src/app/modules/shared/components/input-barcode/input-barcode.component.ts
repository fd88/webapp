import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { timer } from 'rxjs';

import { fadeInAnimation } from '@app/util/animations';
import { BarcodeObject } from '@app/core/barcode/barcode-object';
import { createBarcodeObject } from '@app/core/barcode/barcode-reader.service';
import { TMLBarcode } from '@app/core/barcode/tml-barcode';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { ButtonGroupMode, ButtonGroupStyle, ButtonsGroupComponent, IButtonGroupItem } from '../buttons-group/buttons-group.component';

/**
 * Типы баркодов.
 * Возможные типы баркодов:
 * - {@link TMLBML} - бумажная лотерея.
 */
export enum InputBarcodeType {
	TMLBML = 'tmlbml'
}

/**
 * Состояние компонента ввода списка баркодов:
 * - {@link EXPANDED} - Компонент раскрыт. Есть возможность ввода данных.
 * - {@link COLLAPSED} - Компонент закрыт. Все элементы ввода спрятаны.
 */
export enum InputBarcodeComponentState {
	EXPANDED = 'expanded',
	COLLAPSED = 'collapsed'
}

export type ItemListType = [BarcodeObject, BarcodeObject, UpdateDrawInfoDraws];

/**
 * Компонент ввода баркодов.
 */
@Component({
	selector: 'app-input-barcode',
	templateUrl: './input-barcode.component.html',
	styleUrls: ['./input-barcode.component.scss'],
	animations: [
		fadeInAnimation
	]
})
export class InputBarcodeComponent implements OnInit {

	// -----------------------------
	//  Constants
	// -----------------------------
	/**
	 * Режимы работы кнопок.
	 */
	readonly ButtonGroupMode = ButtonGroupMode;

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Задает тип баркода.
	 */
	@Input()
	barcodeType: InputBarcodeType;

	/**
	 * Отобразить список введенных баркодов.
	 */
	@Input()
	showList = true;

	/**
	 * Задает стиль {@link ButtonGroupStyle} отображаемых кнопок.
	 */
	@Input()
	buttonStyle: ButtonGroupStyle = ButtonGroupStyle.outline_square_radial_fill;

	/**
	 * Максимальная длина вводимого баркода.
	 */
	@Input()
	maxLength = 16;

	/**
	 * Минимальная длина вводимого баркода.
	 */
	@Input()
	minLength = 16;

	/**
	 * Необходимость контролировать код тиража в списке.
	 * Если контроль ввключен и количество кодов будет более одного, будет сгенерирована ошибка.
	 */
	@Input()
	isNeededDrawsCodeCheck = false;

	/**
	 * Задает список тиражей, по которым возможен ввод билетов.
	 * Строит хешмап {@link drawsMap}.
	 *
	 * @param {Array<UpdateDrawInfoDraws>} value - список тиражей.
	 */
	@Input()
	set draws(value: Array<UpdateDrawInfoDraws>) {
		this.drawsMap.clear();
		value.forEach(v => this.drawsMap.set(+v.draw.serie_code, v));
	}

	// -----------------------------
	//  Output properties
	// -----------------------------

	/**
	 * Событие генерируется при вводе нового диапазона.
	 */
	@Output()
	readonly newRange = new EventEmitter<[BarcodeObject, BarcodeObject]>();

	/**
	 * Событие генерируется после ввода нового диапазона и и зменении общего списка диапазонов.
	 */
	@Output()
	readonly inputListChanged = new EventEmitter<Array<ItemListType>>();

	/**
	 * Событие генерируется при возникновении ошибок добавления билетов в список.
	 */
	@Output()
	readonly parserError = new EventEmitter<string>();

	/**
	 * Событие генерируется при возникновении предупреждений во время добавления билетов в список.
	 */
	@Output()
	readonly parserWarning = new EventEmitter<string>();

	/**
	 * Событие генерируется при изменении состояния компонента.
	 */
	@Output()
	readonly componentStateChanged = new EventEmitter<InputBarcodeComponentState>();

	// -----------------------------
	//  Public properties
	// -----------------------------
	/**
	 * Начальный штрихкод в диапазоне.
	 */
	@ViewChild('kbdPhase1', { static: false })
	kbdPhase1: ElementRef;

	/**
	 * Конечный штрихкод в диапазоне.
	 */
	@ViewChild('kbdPhase2', { static: false })
	kbdPhase2: ElementRef;

	/**
	 * Штрихкод в режиме ввода одного билета.
	 */
	@ViewChild('kbdSingle', { static: false })
	kbdSingle: ElementRef;

	/**
	 * Компонент с кнопками выбора режима ввода.
	 */
	@ViewChild('inputModeComponent', { static: true })
	inputModeComponent: ButtonsGroupComponent;

	/**
	 * Кнопка ручного ввода штрих-кода.
	 */
	@ViewChild('manualInputComponent', { static: true })
	manualInputComponent: ButtonsGroupComponent;

	/**
	 * Активен режим ручного ввода.
	 */
	isManualInput = false;

	/**
	 * Активен режим ввода диапазоном (по первому и последнему билету диапазона).
	 */
	isRangeMode = false;

	/**
	 * Текущая фаза ввода билетов диапазоном.
	 * Должно содержать 2 баркода после сканирования диапазона.
	 */
	currentInputPhase: Array<BarcodeObject> = [];

	/**
	 * Список введенных диапазонов с баркодами.
	 */
	inputBarcodeList: Array<ItemListType> = [];

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Хранит соответствие между номером игры и списком тиражей для этой игры.
	 * @private
	 */
	private readonly drawsMap = new Map<number, UpdateDrawInfoDraws>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Задает новый баркод, полученный из какого-либо источника (в автоматическом режиме).
	 * Производит парсинг баркода с последующим возможным добавлением его в список.
	 *
	 * @param {string} value Передаваемый баркод для парсинга.
	 */
	setBarcode(value: string): void {
		if (this.isManualInput) {
			return;
		}

		if (value) {
			this.parseBarcode(value);
		}
	}

	/**
	 * Обработчик изменения режима ввода (одиночный/диапазон).
	 *
	 * @param {IButtonGroupItem} button Нажатая кнопка.
	 */
	onChangeInputModeHandler(button: IButtonGroupItem): void {
		this.isRangeMode = (button.index === 1);

		// сбросить фазы ввода
		if (this.isRangeMode) {
			this.currentInputPhase = [];
		}

		this.checkComponentStateAndSendEvent();
	}

	/**
	 * Обработчик нажатия кнопки "Ручной ввод".
	 *
	 * @param {IButtonGroupItem} button Нажатая кнопка.
	 */
	onClickManualEnterHandler(button: IButtonGroupItem): void {
		this.isManualInput = button.selected;
		this.checkComponentStateAndSendEvent();
	}

	/**
	 * Обработчик события ручного ввода баркода.
	 *
	 * @param {string} barcode Баркод, введенный с клавиатуры.
	 * @param {HTMLInputElement} nextTarget Следующий элемент для фокуса.
	 */
	onManualBarcodeEnteredHandler(barcode: string, nextTarget?: HTMLInputElement): void {
		// сфокусироваться на следующем элементе (если задан)
		if (nextTarget) {
			timer(300)
				.subscribe(() => nextTarget.focus());
		}

		this.parseBarcode(barcode);
	}

	/**
	 * Обработчик события {@link input}.
	 *
	 * @param event Передаваемое событие.
	 */
	onInputHandler(event): void {
		if (event.target && event.target.value && event.target.value.length === 16) {
			this.parseBarcode(event.target.value);
		}
	}

	/**
	 * Обработчика нажатия кнопки "Отмена ввода".
	 */
	onClickCancelHandler(): void {
		this.resetInputControls();
	}

	/**
	 * Обработчик клика на строке списка баркодов.
	 *
	 * @param event Событие клика.
	 */
	onRemoveClickHandler(event): void {
		if (event.target) {
			const attr = event.target.getAttributeNode('index');
			if (attr) {
				this.inputBarcodeList.splice(attr.value, 1);
				this.inputListChanged.emit(this.inputBarcodeList);
			}
		}
	}

	/**
	 * Вспомогательная функция для отслеживания изменений в массиве элементов.
	 * @param index Индекс элемента.
	 * @param item Элемент.
	 */
	trackByField = (index, item: ItemListType) => item;

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Сгенерировать событие {@link componentStateChanged} при изменении состояния компонента.
	 */
	private checkComponentStateAndSendEvent(): void {
		if (this.isRangeMode || this.isManualInput) {
			this.componentStateChanged.emit(InputBarcodeComponentState.EXPANDED);
		} else {
			this.componentStateChanged.emit(InputBarcodeComponentState.COLLAPSED);
			// setTimeout(() => (document.activeElement as HTMLElement).blur(), 1000);
		}
	}

	/**
	 * Парсер баркодов.
	 * Выполняет основные проверки баркода согласно режима ввода (одиночный или диапазоном).
	 *
	 * @param {string} barcode Исходный баркод в виде строки.
	 */
	private parseBarcode(barcode: string): void {
		// создать объект баркода
		let bcObj: BarcodeObject;
		switch (this.barcodeType) {
			case InputBarcodeType.TMLBML:
				bcObj = new TMLBarcode(barcode);

				// проверить тираж для баркода
				if (!this.drawsMap.get(bcObj.intGameNumber)) {
					this.parserWarning.emit(`[${barcode}] - has undefined draw and will be skipped`);

					return;
				}
				break;

			default:
				this.parserWarning.emit('!!!! UNDEFINED BC !!!!');

				return;
		}

		// проверить режим ввода и валидацировать баркод
		let arr: [BarcodeObject, BarcodeObject];
		if (this.isRangeMode) {
			this.currentInputPhase.push(bcObj);

			// на ручном вводе при получении баркода со сканера
			// прописать полученное значение в поле ввода клавиатуры
			if (this.isManualInput) {
				if (this.currentInputPhase.length === 1) {
					this.kbdPhase1.nativeElement.value = barcode;
				} else {
					this.kbdPhase2.nativeElement.value = barcode;
				}
			}

			// на второй фазе ввода проверить совпадение номера игры и пачки
			if (this.currentInputPhase.length === 2) {
				const b1 = this.currentInputPhase[0];
				const b2 = this.currentInputPhase[1];

				if (b1.gameNumber !== b2.gameNumber) {
					this.parserError.emit('dialog.games_are_different');
					this.clearSecondPhase();
				} else if (b1.ticketPackage !== b2.ticketPackage) {
					this.parserError.emit('dialog.packages_are_different');
					this.clearSecondPhase();
				} else if (b1.ticketNumber === b2.ticketNumber) {
					// TODO test for #ALT-451 ------------------------------------------
					if (this.isManualInput) {
						this.parserError.emit('dialog.identical_ticket_numbers');
					}
					// ------------------------------------------------------------------
					this.clearSecondPhase();
				} else {
					// валидация пройдена, добавить пачку в список
					arr = +b1.ticketNumber > +b2.ticketNumber
						? [b2, b1]
						: [this.currentInputPhase[0], this.currentInputPhase[1]];
					this.newRange.emit(arr);
					this.addPackageToList(arr);
					this.resetInputControls();
				}
			}
		} else {
			arr = [bcObj, bcObj];

			if (this.isManualInput) {
				this.kbdSingle.nativeElement.value = barcode;
			}

			this.newRange.emit(arr);
			this.addPackageToList(arr);
			this.resetInputControls();
		}
	}

	/**
	 * Откатить состояние на фазу 1 по причине некорретного баркода.
	 */
	private clearSecondPhase(): void {
		this.currentInputPhase.splice(1, 1);
		this.kbdPhase2.nativeElement.value = '';
	}

	/**
	 * Инициализировать элементы ввода.
	 */
	private resetInputControls(): void {
		if (this.inputModeComponent) {
			this.inputModeComponent.selectedButtons = [0];
			this.inputModeComponent.clearButtons();
		}

		if (this.manualInputComponent) {
			this.manualInputComponent.selectedButtons = [];
			this.manualInputComponent.clearButtons();
		}

		this.isRangeMode = false;
		this.isManualInput = false;
		this.checkComponentStateAndSendEvent();

		this.currentInputPhase.splice(0);
	}

	/**
	 * Добавить новую пачку (диапазон) билетов в список.
	 *
	 * @param {[BarcodeObject, BarcodeObject]} range Диапазон билетов (первый и последний элемент {@link BarcodeObject}).
	 */
	private addPackageToList(range: [BarcodeObject, BarcodeObject]): void {
		const udi = this.drawsMap.get(range[0].intGameNumber);

		// проверить существует ли такой же код тиража для непустого списка если это необходимо
		if (this.isNeededDrawsCodeCheck) {
			const existCode = this.inputBarcodeList.find(v => v[2].draw.code === udi.draw.code);
			if (this.inputBarcodeList.length > 0 && !existCode) {
				this.parserError.emit('dialog.tickets_from_different_lotteries_cant_be_registered');
				this.clearSecondPhase();

				return;
			}
		}

		// отфильтровать список по игре и пачке
		const filteredPackage = this.inputBarcodeList
			.filter(v => v[0].intGameNumber === range[0].intGameNumber && v[0].intTicketPackage === range[0].intTicketPackage);

		// если по фильтру "игра+пачка" имеются элементы - объединить список
		if (filteredPackage.length > 0) {
			// построить вектор с индексами
			const flatArray = [];
			[...filteredPackage, range]
				.forEach(v => {
					Array.from(Array(v[1].intTicketNumber - v[0].intTicketNumber + 1))
						.forEach((o, i) => flatArray[v[0].intTicketNumber + i] = true);
				});

			// нарезать вектор на новые диапазоны
			let d1 = -1;
			let d2 = -1;
			const newList: Array<ItemListType> = [];
			for (let i = 0; i < flatArray.length; i++) {
				if (!flatArray[i] || (flatArray[i] && i === flatArray.length - 1)) {
					if (flatArray[i] && i === flatArray.length - 1) {
						if (d1 < 0) {
							d1 = i;
						}

						d2 = i;
					}

					if (d1 >= 0 && d2 >= 0) {
						newList.push([
							createBarcodeObject(range[0].intGameNumber, range[0].intTicketPackage, d1),
							createBarcodeObject(range[0].intGameNumber, range[0].intTicketPackage, d2),
							udi
						]);
					}

					d1 = d2 = -1;
				} else {
					if (d1 < 0 && d2 < 0) {
						d1 = d2 = i;
					} else {
						d2 = i;
					}
				}
			}

			// удалить старые элементы
			const arr = this.inputBarcodeList
				.filter(v => !(v[0].intGameNumber === range[0].intGameNumber
					&& v[0].intTicketPackage === range[0].intTicketPackage));
			this.inputBarcodeList = arr ? arr : [];

			this.inputBarcodeList.push(...newList);
			this.inputListChanged.emit(this.inputBarcodeList);
		} else {
			this.inputBarcodeList.push([range[0], range[1], udi]);
			this.inputListChanged.emit(this.inputBarcodeList);
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	/**
	 * Обработчик события инициализации компонента
	 */
	ngOnInit(): void {
		this.inputModeComponent.selectedButtons = this.isRangeMode ? [1] : [0];
	}
}
