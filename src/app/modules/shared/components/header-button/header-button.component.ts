import { Component } from '@angular/core';
import { MslButtonComponent } from '../msl-button.component';

/**
 * Компонент кнопки для шапки.
 */
@Component({
	selector: 'app-header-button',
	styleUrls: ['./header-button.component.scss'],
	templateUrl: './header-button.component.html'
})
export class HeaderButtonComponent extends MslButtonComponent  {

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------





}
