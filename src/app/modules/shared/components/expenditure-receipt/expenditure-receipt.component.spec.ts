import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenditureReceiptComponent } from './expenditure-receipt.component';

describe('ExpenditureReceiptComponent', () => {
  let component: ExpenditureReceiptComponent;
  let fixture: ComponentFixture<ExpenditureReceiptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpenditureReceiptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenditureReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
