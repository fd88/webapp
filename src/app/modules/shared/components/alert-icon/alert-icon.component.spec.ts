import { ComponentFixture, TestBed } from '@angular/core/testing';
import {AlertIconComponent} from "@app/shared/components/alert-icon/alert-icon.component";


describe('Test AlertIconComponent', () => {
  let component: AlertIconComponent;
  let fixture: ComponentFixture<AlertIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
