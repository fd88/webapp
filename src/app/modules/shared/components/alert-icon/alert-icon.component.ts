import { Component, Input } from '@angular/core';

/**
 * Компонент иконки статуса периферийного устройства.
 */
@Component({
	selector: 'app-alert-icon',
	templateUrl: './alert-icon.component.html',
	styleUrls: ['./alert-icon.component.scss']
})
export class AlertIconComponent  {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Признак, указывающий на то, что сработал алерт или устройство неисправно.
	 */
	@Input()
	alertState = false;
}
