import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MslInputWithKeyboardComponent } from '@app/shared/components/msl-input-with-keyboard/msl-input-with-keyboard.component';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { HttpService } from '@app/core/net/http/services/http.service';

describe('MslInputWithKeyboardComponent', () => {
	let component: MslInputWithKeyboardComponent | any;
	let fixture: ComponentFixture<MslInputWithKeyboardComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				SharedModule,
				TranslateModule.forRoot()
			],
			declarations: [
			],
			providers: [
				AppStoreService,
				HttpService
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MslInputWithKeyboardComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('test registerOnChange', () => {
		spyOn(component, 'registerOnChange').and.callThrough();
		component.registerOnChange(null);
		expect(component.registerOnChange).toHaveBeenCalled();
	});

	it('test onInputHandler', () => {
		const target = document.createElement('input');
		const ev = new Event('onInputHandler');
		spyOn(component, 'onInputHandler').and.callThrough();
		component.onInputHandler({...ev, target});
		expect(component.onInputHandler).toHaveBeenCalled();
	});

	it('test onFocusHandler', () => {
		const ev = new FocusEvent('onInputHandler');
		spyOn(component, 'onFocusHandler').and.callThrough();
		component.onFocusHandler(ev);
		expect(component.onFocusHandler).toHaveBeenCalled();
	});

	it('test onKeydownHandler', () => {
		const ev = new KeyboardEvent('onKeydownHandler');
		spyOn(component, 'onKeydownHandler').and.callThrough();
		component.onKeydownHandler({...ev, key: 'Enter'});
		expect(component.onKeydownHandler).toHaveBeenCalled();
	});

	it('test checkAndEmitValueChangeEvent', () => {
		component.value = 'test1';
		component.checkAndEmitValueChangeEvent('test2');
		expect(component.value).toEqual('test2');
	});

	it('test checkAndEmitValueChangeEvent', () => {
		component.value = 'test1';
		component.checkAndEmitValueChangeEvent('test2');
		expect(component.value).toEqual('test2');
	});

	it('test writeValue', () => {
		component.value = 'test1';
		component.writeValue('test2')
		expect(component.value).toEqual('test2');
	});

	it('test validate', () => {
		expect(component.validate({ valid: true })).toBeUndefined();
	});


});
