import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared/shared.module';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { PrintButtonComponent } from '@app/shared/components/print-button/print-button.component';
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {HttpService} from "@app/core/net/http/services/http.service";
import {CoreModule} from "@app/core/core.module";

describe('PrintButtonComponent', () => {
	let component: PrintButtonComponent | any;
	let fixture: ComponentFixture<PrintButtonComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot(),
				SharedModule,
				HttpClientTestingModule,
				CoreModule
			],
			declarations: [
			],
			providers: [
				LogService,
				AppStoreService,
				HttpClient,
				HttpService
			]
		})
			.compileComponents();

		TestBed.inject(LogService);

		fixture = TestBed.createComponent(PrintButtonComponent);
		component = fixture.debugElement.componentInstance;
		fixture.detectChanges();
	}));

	it('should create the PrintButtonComponent', () => {
		expect(component).toBeTruthy();
	});

	it('test onClickCopiesCountHandler', () => {
		component.onClickCopiesCountHandler(5);
		expect(component.copiesCount).toEqual(1);
	});

	it('test onClickCopiesCountHandler 2', () => {
		component.onClickCopiesCountHandler(4);
		expect(component.copiesCount).toEqual(5);
	});

	it('test onClickPrintHandler', () => {
		spyOn(component, 'onClickPrintHandler').and.callThrough();
		component.onClickPrintHandler();
		expect(component.onClickPrintHandler).toHaveBeenCalled();
	});


});
