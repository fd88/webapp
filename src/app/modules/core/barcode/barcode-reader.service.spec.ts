import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { BarcodeReaderService, createBarcodeObject } from '@app/core/barcode/barcode-reader.service';
import { TMLBarcode } from '@app/core/barcode/tml-barcode';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { BarcodeGameKey } from '@app/core/configuration/cs';
import { LotteriesDraws } from '@app/core/services/store/draws';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { Operator, TerminalRoles } from '@app/core/services/store/operator';
import {DRAWS_FOR_GAME_100} from "../../features/mocks/game100-draws";
import {HttpClientModule} from "@angular/common/http";
import {Router, RouterModule} from "@angular/router";

interface ICheckBarcode {
	/**
	 * Штрих-код.
	 */
	bc: string;

	/**
	 * Признак корретности штрих-кода.
	 */
	ok: boolean;

	/**
	 * Ожидаемый код игры.
	 */
	code?: number;

	/**
	 * Признак моменталки.
	 */
	instant?: boolean;
}

describe('BarcodeReaderService', () => {
	let service: BarcodeReaderService | any;
	let appStoreService: AppStoreService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				RouterModule
			],
			providers: [
				LogService,
				StorageService,
				HttpService,
				AppStoreService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(BarcodeReaderService);
		appStoreService = TestBed.inject(AppStoreService);
	});

	beforeEach(waitForAsync(async() => {
		appStoreService = TestBed.inject(AppStoreService);
	}));

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('created barcodes should be equal to check list', () => {
		const bcCheckList = [
			'0663000001001996',
			'0663000001002997',
			'0663000001003998',
			'0663000001004999',
			'0663000001005001',
			'0663000001006002',
			'0663000001007003',
			'0663000001008004',
			'0663000001009005',
			'0663000001010997',
			'0663000001011998',
			'0663000001012999',
			'0663000001013001',
			'0663000001014002'
		];

		bcCheckList.forEach(v => {
			const b = new TMLBarcode(v);
			const c = createBarcodeObject(b.intGameNumber, b.intTicketPackage, b.intTicketNumber);
			expect(v).toEqual(c.barcode);
		});
	});

	it('check service start/stop', () => {
		expect(service.serviceIsActive).toBeFalsy();
		service.startDetector();
		expect(service.serviceIsActive).toBeTruthy();
		service.stopDetector();
		expect(service.serviceIsActive).toBeFalsy();
	});

	it('check parseKeyOnSubscription function', () => {
		const kbd = new KeyboardEvent('keydown', {bubbles : true, cancelable : true, shiftKey : true});
		expect(service['parseKeyOnSubscription']({...kbd, key: 'Enter'})).toBeUndefined();
		expect(service['parseKeyOnSubscription']({...kbd, key: '1'})).toBeUndefined();
	});

	it('check manualBarcodeInput function', () => {
		const testBC = '11112222333344445555';

		service.startDetector();
		expect(service.manualBarcodeInput(testBC)).toBeUndefined();

		service.stopDetector();
		expect(service.manualBarcodeInput(testBC)).toBeUndefined();
	});

	xit('check barcode type detector', waitForAsync(async() => {
		const bcCheckList: Array<ICheckBarcode> = [
			{bc: undefined, ok: false},
			{bc: '', ok: false},
			{bc: '12345', ok: false},

			{bc: '0663000001001996', code: 100, instant: true, ok: true},	// Стирачка
			{bc: '0636008060066077', code: 100, instant: true, ok: true},	// Стирачка

			{bc: '0658003580094249', code: 145, instant: true, ok: true},	// ЭМЛ "Веселик"
			{bc: '0630000433067685', code: 143, instant: true, ok: true},	// ЭМЛ "Виграшні лінії"

			{bc: '00140000002014050000634653', code: 1, ok: true},			// Мегалот
			{bc: '00240000002015250000001773', code: 2, ok: true},			// Спортпрогноз
			{bc: '003012470000241660658676', code: 3, ok: true},			// Забава
			{bc: '07140000002014190000644480', code: 71, ok: true},			// Тип
			{bc: '07240000002622750000320142', code: 72, ok: true}			// Топ
		];

		bcCheckList.forEach(v => {
			console.log(`--> check barcode: "${v.bc}"`);
			const ba = service.determineBarcodeType(v.bc);

			expect(ba).toBeDefined('determineBarcodeType() should return IBarcodeGameTypeAnalysis object');
			expect(ba.isCorrectBarcode === v.ok).toBeTruthy(`barcode [${v.bc}] should be ${v.ok ? '' : 'in'}correct`);
			if (ba.isCorrectBarcode) {
				if (v.instant) {
					const egk = BarcodeGameKey.LotteryInstantLotoMomentary;
					expect(ba.gameKey === egk)
						.toBeTruthy(`by barcode [${v.bc}] was detected game key [${ba.gameKey}] but expected [${egk}]`);
				} else {
					expect(ba.detectedGameCode === v.code)
						.toBeTruthy(`by barcode [${v.bc}] was detected game code [${ba.detectedGameCode}] but expected [${v.code}]`);
				}
			}
		});
	}));

	xit('check barcodeParser function', waitForAsync(async() => {
		appStoreService.operator.next(new Operator('123', 'test_session_id', '12345678', TerminalRoles.OPERATOR));

		appStoreService.Draws = new LotteriesDraws();
		appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);
		appStoreService.Settings.instantLotteryCodes.push(LotteryGameCode.TML_BML);

		service._keyBuffer = '';
		expect(service.barcodeParser()).toBeFalsy();
		service._keyBuffer = '0047000001001996';
		expect(service.barcodeParser()).toBeTruthy();
	}));

	it('check finding code from draws', () => {
		appStoreService.operator.next(new Operator('123', 'test_session_id', '12345678', TerminalRoles.OPERATOR));

		const withDraws = '0663000001001996';
		const woDraws = '0000123456123123';

		// без тиражей
		appStoreService.Draws = undefined;
		appStoreService.Settings.instantLotteryCodes.splice(0);
		expect(service.findDrawsForLotteryByBarcode()).toBeUndefined();
		expect(service.findDrawsForLotteryByBarcode(woDraws)).toBeUndefined();
		expect(service.findDrawsForLotteryByBarcode(withDraws)).toBeUndefined();

		// с пустым массивам тиражей
		appStoreService.Draws = new LotteriesDraws();
		expect(service.findDrawsForLotteryByBarcode()).toBeUndefined();
		expect(service.findDrawsForLotteryByBarcode(woDraws)).toBeUndefined();
		expect(service.findDrawsForLotteryByBarcode(withDraws)).toBeUndefined();

		// с тиражами от ТМЛ/БМЛ
		appStoreService.Draws.setLottery(LotteryGameCode.TML_BML, DRAWS_FOR_GAME_100);
		appStoreService.Settings.instantLotteryCodes.push(LotteryGameCode.TML_BML);
		expect(service.findDrawsForLotteryByBarcode()).toBeUndefined();
		expect(service.findDrawsForLotteryByBarcode(woDraws)).toBeUndefined();
		expect(service.findDrawsForLotteryByBarcode(withDraws)).toBeDefined();
		expect(service.parseGameCodeByDraws({}).detectedGameCode).toBeUndefined();

		// если не задан код, подставиться из тиража
		expect(service.parseGameCodeByDraws({barcode: woDraws}).detectedGameCode).toBeUndefined();
		expect(service.parseGameCodeByDraws({barcode: withDraws}).detectedGameCode).toBe(100);
		expect(service.parseGameCodeByDraws({barcode: withDraws, detectedGameCode: 0}).detectedGameCode).toBe(0);
	});

});
