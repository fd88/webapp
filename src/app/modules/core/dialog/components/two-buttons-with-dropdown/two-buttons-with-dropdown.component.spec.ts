import { ComponentFixture, TestBed } from '@angular/core/testing';

import {CUSTOM, TwoButtonsWithDropdownComponent} from './two-buttons-with-dropdown.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {CoreModule} from "@app/core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {DropDownListComponent, IDropdownListItem} from "@app/shared/components/drop-down-list/drop-down-list.component";
import {SESS_DATA} from "../../../../features/mocks/session-data";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {Operator, TerminalRoles} from "@app/core/services/store/operator";
import {MslInputPinComponent} from "@app/shared/components/msl-input-pin/msl-input-pin.component";
import {DialogContainerComponent} from "@app/core/dialog/components/dialog-container.components";
import {MslInputComponent} from "@app/shared/components/msl-input/msl-input.component";
import {Subject} from "rxjs";

describe('TwoButtonsWithDropdownComponent', () => {
  let component: TwoButtonsWithDropdownComponent;
  let fixture: ComponentFixture<TwoButtonsWithDropdownComponent>;
  let appStoreService: AppStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			CoreModule,
			HttpClientModule
		],
      declarations: [
		  TwoButtonsWithDropdownComponent,
		  DropDownListComponent,
		  MslInputPinComponent,
		  DialogContainerComponent
	  ],
		providers: [
			TranslateService,
			HttpClient
		]
    })
    .compileComponents();

	  const op = JSON.parse(SESS_DATA.data[0].value);
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
	  appStoreService.isLoggedIn$$.next(true);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoButtonsWithDropdownComponent);
    component = fixture.componentInstance;
	  component.buttons = {
		  first: {
			  text: 'yes'
		  },
		  second: {
			  text: 'no'
		  }
	  };
	component.itemsList = [
		{label: 'header.shutdown_message_0', value: CUSTOM},
		{label: 'header.shutdown_message_1', value: '1', rolesList: [TerminalRoles.OPERATOR]},
		{label: 'header.shutdown_message_2', value: '2', rolesList: [TerminalRoles.OPERATOR]},
		{label: 'header.shutdown_message_3', value: '3', rolesList: [TerminalRoles.OPERATOR]}
	];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test isDisabled getter', () => {
	component.currentSelection = {...component.itemsList[0]};
	expect(component.isDisabled).toBeFalsy();
  });

  it('test isDisabled getter 2', () => {
	component.currentSelection = {...component.itemsList[0]};
	component.customMessage = {} as MslInputComponent;
	expect(component.isDisabled).toBeTruthy();
  });

  it('test isDisabled getter 2', () => {
	component.currentSelection = {...component.itemsList[0]};
	component.customMessage = {
		value: ''
	} as MslInputComponent;
	expect(component.isDisabled).toBeTruthy();
  });

	it('test onItemChanged handler', () => {
		component.onItemChanged(component.itemsList[0]);
		expect(component.currentSelection).toEqual(component.itemsList[0]);
	});

	it('test clickFirst method', () => {
		component.result = new Subject<IDropdownListItem>();
		component.result.next(component.itemsList[1]);
		component.customMessage = {
			value: 'test'
		} as MslInputComponent;
		component.clickFirst();
		expect(component.currentSelection).toEqual({
			value: CUSTOM,
			label: 'test'
		});
	});

	it('test clickFirst method 2', () => {
		component.result = new Subject<IDropdownListItem>();
		component.result.next(component.itemsList[1]);
		component.clickFirst();
		expect(component.currentSelection).toBeUndefined();
	});

	it('test clickSecond method', () => {
		component.result = new Subject<IDropdownListItem>();
		component.result.next(component.itemsList[1]);
		spyOn(component, 'clickSecond').and.callThrough();
		component.clickSecond();
		expect(component.clickSecond).toHaveBeenCalled();
	});

	it('test ngOnInit lifecycle hook', () => {
		component.itemsList = [];
		component.ngOnInit();
		expect(component.message).toEqual('header.confirmation');
	});

});
