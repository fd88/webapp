import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoButtonsErrorComponent } from './two-buttons-error.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {DialogContainerComponent} from "../dialog-container.components";
import {CoreModule} from "../../../core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('TwoButtonsErrorComponent', () => {
  let component: TwoButtonsErrorComponent;
  let fixture: ComponentFixture<TwoButtonsErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			CoreModule,
			HttpClientModule
		],
      declarations: [
		  TwoButtonsErrorComponent,
		  DialogContainerComponent
	  ],
		providers: [
			TranslateService,
			HttpClient
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoButtonsErrorComponent);
    component = fixture.componentInstance;
	  component.buttons = {
		  first: {
			  text: 'yes'
		  },
		  second: {
			  text: 'no'
		  }
	  };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
