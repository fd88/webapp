import { ChangeDetectorRef, Component } from '@angular/core';
import { ErrorDialogComponent } from '../../types';

/**
 * Диалог ошибки с двумя кнопками.
 */
@Component({
	selector: 'app-two-buttons-error',
	templateUrl: './two-buttons-error.component.html',
	styleUrls: ['./two-buttons-error.component.scss']
})
export class TwoButtonsErrorComponent extends ErrorDialogComponent  {

	// -----------------------------
	//  Public properties
	// -----------------------------



	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ChangeDetectorRef} changeDetector Детектор обнаружения изменений.
	 */
	constructor(
		readonly changeDetector: ChangeDetectorRef
	) {
		super(changeDetector);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
