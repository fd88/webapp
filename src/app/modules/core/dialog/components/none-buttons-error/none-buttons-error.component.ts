import { ChangeDetectorRef, Component } from '@angular/core';
import { ErrorDialogComponent } from '../../types';

/**
 * Диалог ошибки без кнопок.
 */
@Component({
	selector: 'app-none-button-error',
	templateUrl: './none-buttons-error.component.html',
	styleUrls: ['./none-buttons-error.component.scss']
})
export class NoneButtonsErrorComponent extends ErrorDialogComponent  {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Конструктор компонента.
	 *
	 * @param {ChangeDetectorRef} changeDetector Детектор обнаружения изменений.
	 */
	constructor(
		readonly changeDetector: ChangeDetectorRef
	) {
		super(changeDetector);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------



}
