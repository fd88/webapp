import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoneButtonsErrorComponent } from './none-buttons-error.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {DialogContainerComponent} from "../dialog-container.components";
import {CoreModule} from "../../../core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('NoneButtonsErrorComponent', () => {
  let component: NoneButtonsErrorComponent;
  let fixture: ComponentFixture<NoneButtonsErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			TranslateModule.forRoot(),
			HttpClientModule
		],
      declarations: [
		  NoneButtonsErrorComponent,
		  DialogContainerComponent
	  ],
		providers: [
			TranslateService,
			HttpClient
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoneButtonsErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
