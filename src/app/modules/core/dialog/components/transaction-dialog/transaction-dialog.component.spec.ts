import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionDialogComponent } from './transaction-dialog.component';
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@app/core/core.module";
import {HttpClientModule} from "@angular/common/http";

describe('TransactionDialogComponent', () => {
  let component: TransactionDialogComponent;
  let fixture: ComponentFixture<TransactionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			CoreModule,
			HttpClientModule
		],
      declarations: [ TransactionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('test hideExtraMessage method', () => {
		component.extra = 'test';
		component.hideExtraMessage();
		expect(component.extra).toBeUndefined();
	});
});
