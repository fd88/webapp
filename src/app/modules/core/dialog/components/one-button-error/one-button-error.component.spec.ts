import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneButtonErrorComponent } from './one-button-error.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {DialogContainerComponent} from "../dialog-container.components";
import {CoreModule} from "../../../core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('OneButtonErrorComponent', () => {
  let component: OneButtonErrorComponent;
  let fixture: ComponentFixture<OneButtonErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot(),
			CoreModule,
			HttpClientModule
		],
      declarations: [
		  OneButtonErrorComponent,
		  DialogContainerComponent
	  ],
		providers: [
			TranslateService,
			HttpClient
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneButtonErrorComponent);
    component = fixture.componentInstance;
	  component.buttons = {
		  first: {
			  text: 'Ok'
		  }
	  };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
