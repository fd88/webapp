import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmTwoButtonsComponent } from './confirm-two-buttons.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";

describe('ConfirmTwoButtonsComponent', () => {
  let component: ConfirmTwoButtonsComponent;
  let fixture: ComponentFixture<ConfirmTwoButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
		imports: [
			TranslateModule.forRoot()
		],
      declarations: [ ConfirmTwoButtonsComponent ],
		providers: [
			TranslateService
		]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmTwoButtonsComponent);
    component = fixture.componentInstance;
    component.buttons = {
		first: {
			text: 'yes'
		},
		second: {
			text: 'no'
		}
	};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
