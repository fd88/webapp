import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { DialogContainerService } from './dialog-container.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { DialogContainerComponent } from '@app/core/dialog/components/dialog-container.components';
import { DialogError, DialogInfo } from '@app/core/error/dialog';
import {
	DialogConfig,
	IDialog,
	IDialogBindButton,
	IDialogBindButtons,
	IDialogComponent,
	TransactionDialog
} from '@app/core/dialog/types';
import { NetError } from '@app/core/error/types';
import {Type} from "@angular/core";
import {IDropdownListItem} from "@app/shared/components/drop-down-list/drop-down-list.component";
import {Subject} from "rxjs";

xdescribe('DialogContainerService', () => {
	let service: DialogContainerService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				LogService,
				DialogContainerService
			]
		});
		service = TestBed.inject(DialogContainerService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test showNoneButtonsError', () => {
		spyOn(service, 'showNoneButtonsError').and.callThrough();
		service.showNoneButtonsError(new NetError('test'));
		expect(service.showNoneButtonsError).toHaveBeenCalled();
	});
});

export class DialogContainerServiceStub {

	container = {
		addContainer: (c: DialogContainerComponent) => {},
		next: {
			loadComponent: (component: Type<IDialogComponent>): IDialogComponent => undefined
		}
	}

	showNoneButtonsInfo = (title: string, message: string | DialogInfo, config?: DialogConfig): IDialog => { return; };
	showOneButtonError = (error: NetError | DialogError, button: IDialogBindButton, config?: DialogConfig, extra?: string): IDialog => { return; };
	showOneButtonInfo = (title: string, message: string | DialogInfo, button: IDialogBindButton, config?: DialogConfig): IDialog => { return; };
	hideActive = (): void => {};
	hideAll = (): void => {};
	showTransactionDialog = (title: string, message: string | DialogInfo, config?: DialogConfig): TransactionDialog => { return; };
	updateTransactionDialog = (item: any) => {};
	showTwoButtonsWithDropdown = (title: string,
								  message: string | DialogInfo,
								  list: Array<IDropdownListItem>,
								  defaultItem: IDropdownListItem,
								  buttons: IDialogBindButtons,
								  result: Subject<IDropdownListItem>,
								  config?: DialogConfig): IDialog => {return;};
}
