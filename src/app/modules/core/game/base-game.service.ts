import { Location } from '@angular/common';
import { Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { URL_LOTTERIES, URL_REGISTRY, URL_RESULTS } from '@app/util/route-utils';
import { formattedAmount, selectActualDraws } from '@app/util/utils';

import { Logger } from '@app/core/net/ws/services/log/logger';
import { LotteriesGroupCode, LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { UpdateDrawInfoDraws } from '@app/core/net/http/api/models/update-draw-info';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { LogOutService } from '@app/logout/services/log-out.service';
import {BehaviorSubject} from "rxjs";

/**
 * Список возможных вариантов продажи:
 * - {@link RegularSale} - обычная продажа
 * - {@link QuickSale} - продажа билетов из меню "быстрая продажа"
 * - {@link Blanks} - продажа с помощью бланков
 */
export enum GameSaleMode {
	RegularSale		= 0,
	QuickSale		= 1
}

/**
 * Базовая модель данных по игре, по которым можно совершить ставку.
 * Так же позволяет восстановить всю заполненную информацию по игре при переходе из одного состояние в другое.
 */
export interface IBetDataItem {

	/**
	 * Количество тиражей, в которых сыграет билет.
	 */
	drawCount: number;

	/**
	 * Режим продаж лотерейных билетов.
	 * @see EsapParams.SALE_MODE SALE_MODE
	 */
	saleMode?: GameSaleMode;
}

/**
 * Перечисление используемых источников для создания ставок.
 * Допустимые варианты:
 * - {@link Scanner} - со сканера
 * - {@link Manual} - ручной ввод.
 */
export enum BetDataSource {
	Scanner	= 'scanner',
	Manual	= 'manual'
}

/**
 * Абстрактный класс сервиса лотереи.
 */
export abstract class BaseGameService<T extends IBetDataItem> {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Функция генерирующая модель ставки с начальными данными.
	 */
	initialBetDataFactory: () => T;

	/**
	 * Текущий выбранный код игры.
	 * Если в лотерее допускается выбор родственной игры (Тип и Топ), то код будет соответствовать выбранной игре.
	 * В противном случае содержит единственный код, соответствующей игре.
	 */
	currentGameCode: LotteryGameCode;

	/**
	 * Указывает на группу лотереи.
	 */
	currentGroupCode: LotteriesGroupCode;

	/**
	 * Список активных тиражей по игре.
	 * Список будет наполнен после вызова функции {@link initBetData}
	 */
	activeDraws: Array<UpdateDrawInfoDraws>;

	/**
	 * Ссылка на сервис {@link AppStoreService}.
	 */
	appStoreService: AppStoreService;

	/**
	 * Источник ввода ставок
	 * 1 - вручную, 2 - типографские бланки
	 */
	betInputMode = 1;

	/**
	 * Часть URL, соответствующего модулю имплементируемой игры.
	 */
	abstract readonly gameUrl: string;

	/**
	 * Ключ локализации имени игры.
	 */
	abstract gameName: string;

	/**
	 * Текущая модель ставки.
	 */
	get betData(): T | undefined {
		if (this._currentSource === BetDataSource.Manual) {
			return this._manualBetDataItem;
		} else if (this._currentSource === BetDataSource.Scanner) {
			return this._scannerBetDataItem;
		}

		return undefined;
	}

	/**
	 * Сеттер текущей суммы ставки в грн.
	 *
	 * @param {number} value Новое значение суммы ставки.
	 */
	set amount(value: number) {
		this._amount = value;
	}

	/**
	 * Геттер текущей суммы ставки в грн.
	 */
	get amount(): number {
		return this._amount;
	}

	// ---

	/**
	 * Возвращает форматированное представление суммы ставки.
	 */
	get formattedAmount(): string {
		return formattedAmount(this._amount);
	}

	// ---

	/**
	 * Возвращает полный URL к компоненту отображения результатов по игре.
	 */
	get resultsUrl(): string {
		return `/${URL_LOTTERIES}/${this.gameUrl}/${URL_RESULTS}`;
	}

	// ---

	/**
	 * Возвращает полный URL к компоненту отображения информации о ставке.
	 */
	get registryUrl(): string {
		return `/${URL_LOTTERIES}/${this.gameUrl}/${URL_REGISTRY}`;
	}

	// -----------------------------
	//  Private properties
	// -----------------------------
	/**
	 * Текущая модель ставки (ручной ввод).
	 * @private
	 */
	private _manualBetDataItem: T;

	/**
	 * Текущая модель ставки (ввод со сканера).
	 * @private
	 */
	private _scannerBetDataItem: T;

	/**
	 * Источник данных для текущей ставки.
	 * @private
	 */
	private _currentSource: BetDataSource;

	/**
	 * Текущая сумма ставки в грн.
	 * @protected
	 */
	protected _amount: number;

	/**
	 * Ссылка на сервис маршрутизации.
	 * @protected
	 */
	protected abstract readonly router: Router;

	/**
	 * Ссылка на сервис локализации.
	 * @protected
	 */
	protected abstract readonly translateService: TranslateService;

	/**
	 * Ссылка на сервис диалоговых окон.
	 * @protected
	 */
	protected abstract readonly dialogInfoService: DialogContainerService;

	/**
	 * Ссылка на сервис разлогина.
	 * @protected
	 */
	protected abstract readonly logoutService: LogOutService;

	/**
	 * Ссылка на объект локации.
	 * @protected
	 */
	protected abstract readonly location: Location;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Перейти к регистрации обработки чека.
	 *
	 * @param {Params} queryParams Объект с параметрами для навигации.
	 */
	goToRegistryPage(queryParams?: Params): void {
		Logger.Log.i('BaseGameService', `goToRegistryPage -> will navigate to the registry page`)
			.console();

		this.router.navigate([this.registryUrl], {queryParams})
			.catch(err => {
				Logger.Log.e('BaseGameService', `goToRegistryPage -> can't navigate: %s`, err)
					.console();
			});
	}

	/**
	 * Функция возврата с окна регистрации (кнопка "НАЗАД").
	 * Выполняет переход на шаг назад (к заполненной лоторее).
	 */
	goBackFromRegistrationForm(): void {
		if (!!this.location) {
			Logger.Log.i('BaseGameService', `goBackFromRegistrationForm -> will navigate back`)
				.console();

			// this.location.back();
			this.router.navigate([this.appStoreService.previousURL])
				.catch(err => {
					Logger.Log.e('BaseGameService', `Go Back -> can't go back: %s`, err)
						.console();
				});
		}
	}

	/**
	 * Инициализация ставок на компоненте ручного ввода или с купона.
	 * Задает текущую модель с данными по выбранному источнику {@link BetDataSource}.
	 *
	 * @param {BetDataSource} source Источник данных.
	 */
	initBetData(source: BetDataSource): void {
		this.updateActiveDraws();

		this._currentSource = source;

		if (this._currentSource === BetDataSource.Manual) {
			if (!this._manualBetDataItem) {
				this._manualBetDataItem = this.initialBetDataFactory();
			}
		} else if (this._currentSource === BetDataSource.Scanner) {
			if (!this._scannerBetDataItem) {
				this._scannerBetDataItem = this.initialBetDataFactory();
			}
		}
	}

	/**
	 * Сброс введенных данных по игре.
	 */
	resetBetData(): void {
		this._scannerBetDataItem = undefined;
		this._manualBetDataItem = undefined;

		this.amount = 0;
	}

	/**
	 * Обновить массив активных тиражей.
	 */
	updateActiveDraws(): void {
		this.activeDraws = selectActualDraws(this.appStoreService.Draws, this.currentGameCode);
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Абстрактная функция, которая вызывается при наличии изменений в ставках.
	 * Должна выполнять обсчет параметра {@link BaseGameService.amount amount}.
	 */
	abstract refreshBets(): void;

	/**
	 * Абстрактная функция, вызываемая после нажатия на кнопку подтверждения покупки лотереи.
	 * Должна инициировать транзакцию покупки.
	 */
	abstract buyLottery(): void;

}
