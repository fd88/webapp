import { BehaviorSubject } from 'rxjs';

import { ICouponConfiguration } from '@app/shared/components/coupon/coupon-with-marks/coupon-with-marks.component';
import { ICouponSectionAnalysisItem } from '@app/shared/components/coupon/coupon-section-analysis/coupon-section-analysis.component';
import { ICouponFieldItem } from '@app/shared/components/coupon/coupon-filled-fields/coupon-filled-fields.component';
import { CouponBetBoxStatus } from '@app/shared/directives/coupon-draw.directive';

import { ICouponDrawMark } from '@app/core/coupon/icoupon-draw-mark';
import { ICouponDrawBetBox } from '@app/core/coupon/icoupon-draw-bet-box';
import { MarksType, ScannedTypeCouponOMR } from '@app/core/net/ws/api/models/scanner/scanner-models';

/**
 * Абстрактный класс сервиса сканирования для любой игры.
 */
export abstract class BaseCouponService<C extends ICouponConfiguration> {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Список полей {@link ICouponFieldItem}, отображающих количество отмененных,
	 * ошибочных или корректно заполненых полей с метками.
	 */
	abstract fieldListForRightPart: Array<ICouponFieldItem>;

	/**
	 * Абстрактная функция, генерирующая модель конфигурации купона с начальными данными о изображениях и метках.
	 */
	abstract initialCouponDataFactory: () => C;

	/**
	 * Абстрактный фабричный метод для создания секций {@link ICouponDrawBetBox} и обсчета различных вспомогательных параметров.
	 */
	abstract renderBoxFactoryFn: (box: ICouponDrawBetBox) => ICouponDrawBetBox;

	/**
	 * Абстрактный фабричный метод для обновления модели метки {@link ICouponDrawMark}
	 * параметрами (стилями) визуализации метки.
	 */
	abstract renderMarksFactoryFn: (mark: ICouponDrawMark) => void;

	/**
	 * Конфигурация купона.
	 */
	readonly couponConfiguration$$ = new BehaviorSubject<C>(undefined);

	/**
	 * Список меток "ОТМЕНА".
	 */
	protected abstract readonly cancelMarksByBlock: Array<MarksType>;

	/**
	 * Список меток "АВТО".
	 */
	protected abstract readonly autoMarksByBlock: Array<MarksType>;

	// -----------------------------
	//  Private properties
	// -----------------------------

	/**
	 * Конфигурация купона.
	 * @private
	 */
	private _couponConfiguration: C;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Инициализировать конфигурацию модели купона.
	 */
	initConfiguration(): void {
		this._couponConfiguration = this.initialCouponDataFactory();
		this.couponConfiguration$$.next(this._couponConfiguration);
	}

	/**
	 * Сбрасывает конфигурацию купона вместе с выбранными метками.
	 */
	resetConfiguration(): void {
		this._couponConfiguration = undefined;
		this.couponConfiguration$$.next(undefined);
	}

	/**
	 * Функция создания меток на купоне.
	 * На базе конфигурации купона собирает все возможные метки для данного типа купона
	 * в {@link IMarkConfiguration.allMarks allMarks}.
	 * Для отрисовки меток будет использован фабричный метод {@link renderMarksFactoryFn},
	 * который нужно указать в конечной реализации компонента купона.
	 */
	createAllMarks(): void {
		const allMarks: Array<ICouponDrawMark> = [];
		this._couponConfiguration.blockConfiguration
			.forEach((f, boxId) => {
				f.id = boxId;

				const boxMarks = [...f.mainMarksByBox, ...(f.systemMarks ? f.systemMarks : [])];
				boxMarks.forEach(mark => {
					// продублировать идентификатор блока в метку
					mark.boxId = boxId;

					// пометить метку признаком АВТО или ОТМЕНА
					mark.isAutoMark = !!this.autoMarksByBlock
						&& !!this.autoMarksByBlock.find(m => mark.mark[0] === m[0] && mark.mark[1] === m[1]);
					mark.isCancelMark = !!this.cancelMarksByBlock
						&& !!this.cancelMarksByBlock.find(m => mark.mark[0] === m[0] && mark.mark[1] === m[1]);
				});
				f.allMarksByBox = boxMarks;
				allMarks.push(...boxMarks);
			});

		allMarks.forEach(this.renderMarksFactoryFn.bind(this));

		this._couponConfiguration.markConfiguration = {allMarks};
		this.couponConfiguration$$.next({...this._couponConfiguration});
	}

	/**
	 * Функция создания секций с метками.
	 * Создает все возможные группы с метками: ставки, дополнительные тиражи, сумма ставок и т.д.
	 */
	createAllBetBoxes(): void {
		const allAnalyse: Array<ICouponSectionAnalysisItem> = [];
		this._couponConfiguration.blockConfiguration = this._couponConfiguration.blockConfiguration
			.map(box => {
				const newBox = this.renderBoxFactoryFn(box);
				newBox.selectedMainMarksByBox = box.mainMarksByBox.filter(f => f.selected);
				const analyse = this.analyseCoupon(newBox);
				newBox.analyse = analyse;
				allAnalyse.push(analyse);
				if (newBox.analyse) {
					newBox.betBoxStatus = newBox.analyse.status;
				}

				return newBox;
			});

		// определить общий статус купона
		this._couponConfiguration.overallCouponStatus = !!allAnalyse.find(f => f.status === CouponBetBoxStatus.Error)
			? CouponBetBoxStatus.Error
			: CouponBetBoxStatus.OK;


		// отмененные блоки
		const canceledBIDs = new Map<number, boolean>();
		this._couponConfiguration.blockConfiguration.forEach(block => {
			canceledBIDs.set(block.id, block.betBoxStatus === CouponBetBoxStatus.Canceled);
		});

		// поставить меткам отмененный статус, если весь блок отменен
		this._couponConfiguration.markConfiguration.allMarks.forEach(mark => {
			mark.betBoxStatus = !mark.isCancelMark && canceledBIDs.get(mark.boxId)
			&& mark.selected ? CouponBetBoxStatus.Canceled : undefined;
		});

		this.couponConfiguration$$.next({...this._couponConfiguration});

		// TODO удалить ---------------------------------
		// console.log('+++++marks', this._couponConfiguration.markConfiguration.allMarks.map(m => `${m.id} - (${m.mark.join(',')})`));
		// console.log('+++++marks', this._couponConfiguration.markConfiguration.allMarks
		// 	.filter(f => f.selected)
		// 	.map(m => `[${m.mark[0]},${m.mark[1]}],`)
		// 	.join(''));
		// ----------------------------------------------
	}

	/**
	 * Функция подсветки выбранных меток на купоне из модели последних отсканированных данных {@link ScannedTypeCouponOMR}.
	 * Помечает признак {@link ICouponDrawMark.selected selected} значением true, если метка выбрана.
	 *
	 * @param {Array<Array<number>>} marksListToUpdate
	 */
	updateMarks(marksListToUpdate: Array<Array<number>>): void {
		if (Array.isArray(marksListToUpdate)) {
			const arr = [...this._couponConfiguration.markConfiguration.allMarks];
			arr.forEach(v => v.selected = false);

			marksListToUpdate
				.forEach(v => {
					const mark = arr.find(f => f.mark[0] === v[0] && f.mark[1] === v[1]);
					if (mark) {
						mark.selected = true;
					}
				});

			this._couponConfiguration.markConfiguration.allMarks = arr;
			// TODO удалить ---------------------------------
			// arr.forEach(v => v.selected = true);
			// ----------------------------------------------
			this.couponConfiguration$$.next({...this._couponConfiguration});
		}
	}

	/**
	 * Обновляет конфигурацию новым значением {ICouponConfiguration.grayscaleMode grayscaleMode}.
	 *
	 * @param {boolean} isGrayScale
	 */
	updateGrayScaleProperty(isGrayScale: boolean): void {
		this._couponConfiguration.grayscaleMode = isGrayScale;
		this.couponConfiguration$$.next({...this._couponConfiguration});
	}

	/**
	 * Абстрактная функция анализа отсканированного купона.
	 *
	 * @param {ICouponDrawBetBox} box Секция купона, подлежащая анализу.
	 * @returns Возвращает объект с анализом секции типа {@link ICouponSectionAnalysisItem}.
	 */
	abstract analyseCoupon(box: ICouponDrawBetBox): ICouponSectionAnalysisItem;

	/**
	 * Абстрактная функция обсчета отсканированного купона.
	 */
	abstract calculateBets(): void;

}
