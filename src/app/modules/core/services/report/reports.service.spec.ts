import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { TransactionService } from '@app/core/services/transaction/transaction.service';

import { ReportsService } from '@app/core/services/report/reports.service';
import { Router } from '@angular/router';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { HttpServiceStub } from '@app/core/net/http/services/http.service.spec';
import { TransactionServiceStub } from '@app/core/services/transaction/transaction.service.spec';

import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { ResponseCacheService } from '@app/core/services/response-cache.service';
import { ReportTag } from '@app/core/services/report/tags';
import {LogService} from "@app/core/net/ws/services/log/log.service";

describe('ReportsService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				ReportsService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				{
					provide: HttpService,
					useValue: new HttpServiceStub()
				},
				{
					provide: TransactionService,
					useValue: new TransactionServiceStub()
				},
				LogService
			]
		});
		TestBed.inject(LogService);
	});

	it('should be created', () => {
		const service: ReportsService = TestBed.inject(ReportsService);
		expect(service).toBeTruthy();
	});
});

// -----------------------------
//  Static functions
// -----------------------------

export class ReportsServiceStub extends ReportsService {
	constructor() {
		super(
			{} as Router,
			{} as DialogContainerService,
			{} as HttpService,
			{} as TransactionService,
			{} as AppStoreService,
			{} as PrintService,
			{} as ResponseCacheService,
			{} as TranslateService
		);
	}

	reportResponses = [{
		report: {
			id: '1',
			config: 'test 1',
			version: 1,
			requestAction: undefined,
			requestArgType: undefined,
			requestArgParams: undefined
		},
		parser: undefined,
		error: undefined
	},
		{
			report: {
				id: '2',
				config: 'test 2',
				version: 2,
				requestAction: undefined,
				requestArgType: undefined,
				requestArgParams: undefined
			},
			parser: undefined,
			error: undefined
		}];

	getReportsList(key: string): Array<ReportTag> {
		// accessibility
		const repTag = new ReportTag();
		repTag.output = {
			parent_tag: 'test',
			screen: undefined,
			printer: {
				autoprint: false,
				parent_tag: 'test',
				sections: undefined
			}
		};
		repTag.accessibility = {
			parent_tag: 'test',
			access_level: {
				role: 'operator',
				parent_tag: 'test'
			},
			access_list: {
				roles: 'operator',
				parent_tag: 'test'
			}
		};
		const repTag2 = new ReportTag();
		repTag2.output = {
			parent_tag: 'test',
			screen: undefined,
			printer: {
				autoprint: false,
				parent_tag: 'test',
				sections: undefined
			}
		};
		repTag2.accessibility = {
			parent_tag: 'test',
			access_level: {
				role: 'operator',
				parent_tag: 'test'
			},
			access_list: null
		};

		const repTag3 = new ReportTag();
		repTag3.output = {
			parent_tag: 'test',
			screen: undefined,
			printer: {
				autoprint: false,
				parent_tag: 'test',
				sections: undefined
			}
		};
		repTag3.accessibility = {
			parent_tag: 'test',
			access_level: null,
			access_list: {
				roles: 'operator',
				parent_tag: 'test'
			}
		};
		const repTag4 = new ReportTag();
		repTag4.output = {
			parent_tag: 'test',
			screen: undefined,
			printer: {
				autoprint: false,
				parent_tag: 'test',
				sections: undefined
			}
		};
		repTag4.accessibility = {
			parent_tag: 'test',
			access_level: {
				role: null,
				parent_tag: 'test'
			},

			access_list: {
				roles: 'operator',
				parent_tag: 'test'
			}
		};
		return [
			repTag,
			repTag2,
			repTag3,
			repTag4
		];
	}
}
