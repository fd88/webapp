import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LotteriesService } from './lotteries.service';

import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';

describe('LotteriesService', () => {
	let lotteriesService: LotteriesService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule
			],
			providers: [
				LogService,
				LotteriesService,
				StorageService,
				HttpService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				}
			]
		});
		lotteriesService = TestBed.inject(LotteriesService);
	});

	it('should be created', () => {
		expect(lotteriesService).toBeTruthy();
	});

});
