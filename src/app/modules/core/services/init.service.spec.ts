import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { InitService } from '@app/core/services/init.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { AuthService } from '@app/core/services/auth.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { InitGuard } from '@app/core/guards/init.guard';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { ChangeLogService } from '@app/sys-info/services/change-log.service';
import {HttpClientModule, HttpHeaders} from "@angular/common/http";
import {environment} from "@app/env/environment";
import {SESS_DATA} from "../../features/mocks/session-data";
import {Operator} from "@app/core/services/store/operator";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {IResponse, KeyValue} from "@app/core/net/ws/api/types";
import {AppType} from "@app/core/services/store/settings";
import {StorageGetReq} from "@app/core/net/ws/api/models/storage/storage-get";
import {NetError} from "@app/core/error/types";

describe('InitService', () => {
	let service: InitService | any;
	let appStoreService: AppStoreService | any;
	let storageService: StorageService;
	let httpService: HttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientModule,
				TranslateModule.forRoot()
			],
			providers: [
				InitService,
				InitGuard,
				LogService,
				AuthService,
				LotteriesService,
				ChangeLogService,
				HttpService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				},
				StorageService
			]
		});

		TestBed.inject(LogService);

		service = TestBed.inject(InitService);
		appStoreService = TestBed.inject(AppStoreService);
		storageService = TestBed.inject(StorageService);
		httpService = TestBed.inject(HttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test init method', () => {
		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test init method 2', () => {
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService = TestBed.inject(AppStoreService);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		appStoreService.isLoggedIn$$.next(true);
		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test init method 3', () => {
		environment.printerUrl = 'ws://127.0.0.1:2424';
		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test init method 4', () => {
		appStoreService.Settings._csEnvironmentUrl = '/non-existent-config.json';
		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test init method 5', () => {
		storageService.get = (appId: string, data: Array<string>, owner?: string, timeout?: number, retry?: number): Promise<IResponse> => {
			return new Promise<IResponse>((resolve, reject) => {
				reject({
					message: 'test error message'
				});
			});
		};

		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test init method 6', () => {
		storageService.get = (appId: string, data: Array<string>, owner?: string, timeout?: number, retry?: number): Promise<IResponse> => {
			return new Promise<IResponse>((resolve, reject) => {
				resolve({
					requestId: '123',
					errorCode: 0,
					errorDesc: ''
				});
			});
		};

		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});

	it('test init method 7', () => {
		storageService.put = (appId: string, data: Array<KeyValue>, timeout?: number, retry?: number): Promise<IResponse> => {
			return new Promise<IResponse>((resolve, reject) => {
				reject({
					message: 'Some test error'
				});
			});
		};

		spyOn(service, 'init').and.callThrough();
		service.init();
		expect(service.init).toHaveBeenCalled();
	});


	it('test initAppSettings method', () => {
		appStoreService.Settings.printServiceUrl = 'ws://127.0.0.1:2424';
		service.initPrintService();
		spyOn(service, 'initAppSettings').and.callThrough();
		service.initAppSettings();
		expect(service.initAppSettings).toHaveBeenCalled();
	});

	it('test initAppSettings method 2', () => {
		appStoreService.Settings.printServiceUrl = 'ws://127.0.0.1:2424';
		service.initPrintService();
		spyOn(service, 'initAppSettings').and.callThrough();
		const ev = new Event('initAppSettings');
		service.initAppSettings({...ev, type: 'Some error'});
		expect(service.initAppSettings).toHaveBeenCalled();
	});

	it('test getEsapConfig method', () => {
		httpService.sendGet = (url: string, headers: HttpHeaders, timeout?: number, retryCount?: number): Promise<string> => {
			return new Promise<string>((resolve, reject) => {
				reject({
					message: 'Error getting config'
				});
			});
		};
		spyOn(service, 'getEsapConfig').and.callThrough();
		service.getEsapConfig();
		expect(service.getEsapConfig).toHaveBeenCalled();
	});

	it('test getEsapConfig method 2', () => {
		httpService.sendGet = (url: string, headers: HttpHeaders, timeout?: number, retryCount?: number): Promise<string> => {
			return new Promise<string>((resolve, reject) => {
				reject(new NetError('Error getting config'));
			});
		};
		spyOn(service, 'getEsapConfig').and.callThrough();
		service.getEsapConfig();
		expect(service.getEsapConfig).toHaveBeenCalled();
	});
});
