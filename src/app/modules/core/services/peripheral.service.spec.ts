import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { TranslateModule } from '@ngx-translate/core';

import { PeripheralService } from '@app/core/services/peripheral.service';
import { BarcodeReaderService } from '@app/core/barcode/barcode-reader.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { LotteriesService } from '@app/core/services/lotteries.service';

import { LogOutService } from '@app/logout/services/log-out.service';

xdescribe('PeripheralService', () => {
	let service: PeripheralService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot()
			],
			providers: [
				BarcodeReaderService,
				LogService,
				LogOutService,
				HttpService,
				LotteriesService
			]
		});

		service = TestBed.inject(PeripheralService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
