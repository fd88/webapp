import { LotteriesDraws } from "@app/core/services/store/draws";
import { TestBed } from "@angular/core/testing";
import { LotteriesGroupCode, LotteryGameCode } from "@app/core/configuration/lotteries";

xdescribe('BarcodeUnit', () => {
	let lotteriesDraws: LotteriesDraws | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				LotteriesDraws
			]
		});

		lotteriesDraws = TestBed.inject(LotteriesDraws);
	});

	it('test setLottery method', () => {
		lotteriesDraws.setLottery(LotteryGameCode.Undefined, {
			lott_extra: {
				group_id: LotteriesGroupCode.EInstant
			}
		});
		expect(lotteriesDraws.hasLottery(LotteryGameCode.Undefined)).toBeTruthy();
	});

	xit('test getLottery method', () => {
		lotteriesDraws._lotteries = undefined;
		expect(lotteriesDraws.getLottery(LotteryGameCode.Undefined)).toBeUndefined();
	});

	it('test delLottery method', () => {
		lotteriesDraws._lotteries = new Map();
		lotteriesDraws._lotteries.set([LotteryGameCode.Zabava, {}]);
		lotteriesDraws.delLottery(LotteryGameCode.Zabava);
		lotteriesDraws.lotteryList$.subscribe(val => {
			expect(val.get(LotteryGameCode.Zabava)).toBeUndefined();
		});
	});

	xit('test delLottery method', () => {
		// getLotteriesCodes
	});
});
