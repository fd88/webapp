import {HttpClientModule} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';

import {AppStoreService} from '@app/core/services/store/app-store.service';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {HttpService} from '@app/core/net/http/services/http.service';
import {AuthService} from "@app/core/services/auth.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {GameResultsService} from "@app/core/services/results/game-results.service";
import {LotteriesService} from "@app/core/services/lotteries.service";
import {ReportsService} from "@app/core/services/report/reports.service";
import {PeripheralService} from "@app/core/services/peripheral.service";
import {DatePipe} from "@angular/common";
import {TransactionService} from "@app/core/services/transaction/transaction.service";
import {ResponseCacheService} from "@app/core/services/response-cache.service";
import {ErrorHandler} from "@angular/core";
import {AppErrorHandler} from "@app/core/error/handler";

describe('AppStoreService', () => {
	let service: AppStoreService | any;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				AuthService,
				DialogContainerService,
				AppStoreService,
				StorageService,
				LogService,
				PrintService,
				HttpService,
				BarcodeReaderService,
				GameResultsService,
				LotteriesService,
				ReportsService,
				PeripheralService,

				DatePipe,

				TransactionService,
				ResponseCacheService,
				{
					provide: ErrorHandler,
					useClass: AppErrorHandler
				}
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(AppStoreService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test playerInfo getter', () => {
		service._playerInfo = {
			playerPhone: '+380991234567',
			playerAuthCode: '1234'
		}
		expect(service.playerInfo).toEqual(service._playerInfo);
	});

	it('test playerInfo setter', () => {
		service.playerInfo = {
			playerPhone: '+380991234567',
			playerAuthCode: '1234'
		}
		expect(service._playerInfo).toEqual({
			playerPhone: '+380991234567',
			playerAuthCode: '1234'
		});
	});

});
