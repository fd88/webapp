import { TestBed } from '@angular/core/testing';

import { ResponseCacheService } from './response-cache.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';
import { DialogContainerServiceStub } from '@app/core/dialog/services/dialog-container.service.spec';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DurationDay } from '@app/util/utils';
import { Cache } from '@app/util/cache';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';

xdescribe('ResponseCacheService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule
			],
			providers: [
				LogService,
				HttpService,
				StorageService,
				{
					provide: DialogContainerService,
					useValue: new DialogContainerServiceStub()
				}
			]
		});

		TestBed.inject(LogService);
	});

	it('should be created', () => {
		const service: ResponseCacheService = TestBed.inject(ResponseCacheService);
		expect(service).toBeTruthy();
	});

	it('Проверка вытесняющего кеша', () => {
		const cache = new Cache<string, string>();
		cache.lengthFunction = (value: string) => value.length;

		// check add/remove elements
		const maxLen = 10000;
		cache.maxLength = maxLen;
		cache.maxAge = DurationDay; // set delay to one day to prevent removing elements by time
		Array.from(Array(maxLen)).forEach((v, i) => cache.set(`id:${i}`, 'X'));
		expect(cache.currentLength).toEqual(maxLen);
		Array.from(Array(maxLen)).forEach((v, i) => cache.del(`id:${i}`));
		expect(cache.currentLength).toEqual(0);

		// проверить вытеснение данных по времени
		// cache.maxAge = 5000;
		// cache.clear();
		// timer(0, 1000)
		// 	.pipe(take(14))
		// 	.subscribe(v => {
		// 		// console.log('+++', v);
		// 		cache.set(`${Date.now()}`, `${v}`)
		// 	}, () => {
		// 	}, () => {
		// 		expect(cache.size()).toEqual(5);
		// 	});

		// page.setPause(30000);
	});
});
