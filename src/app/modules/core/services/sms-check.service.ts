import { Injectable } from '@angular/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { SendAuthCodeReq, SendAuthCodeResp } from '@app/core/net/http/api/models/send-auth-code';

/**
 * Сервис для работы с СМС
 */
@Injectable({
	providedIn: 'root'
})
export class SmsCheckService {

	/**
	 * Конструктор сервиса
	 * @param httpService Сервис для работы с HTTP-запросами
	 * @param appStoreService Сервис для работы с хранилищем приложения
	 */
	constructor(private readonly httpService: HttpService, private readonly appStoreService: AppStoreService) { }

	/**
	 * Отправить СМС
	 * @param {string} playerPhone - 9 цифр номера игрока
	 */
	sendAuthCode(playerPhone: string): Promise<SendAuthCodeResp> {
		const sendAuthCodeReq = new SendAuthCodeReq(this.appStoreService, playerPhone);

		return this.httpService.sendApi(sendAuthCodeReq);
	}
}
