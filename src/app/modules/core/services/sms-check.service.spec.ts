import { TestBed } from '@angular/core/testing';
import { SmsCheckService } from './sms-check.service';
import {CoreModule} from "@app/core/core.module";
import {HttpService} from "@app/core/net/http/services/http.service";
import {HttpClientModule} from "@angular/common/http";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LogService} from "@app/core/net/ws/services/log/log.service";

describe('SmsCheckService', () => {
  let service: SmsCheckService;
  let appStoreService: AppStoreService;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
		imports: [
			CoreModule,
			HttpClientModule
		],
		providers: [
			HttpService,
			AppStoreService,
			LogService
		]
	});
	  TestBed.inject(LogService);
	  const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	  appStoreService = TestBed.inject(AppStoreService);
	  appStoreService.Settings.populateEsapActionsMapping(csConfig);
      service = TestBed.inject(SmsCheckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

	it('should be created', async () => {
		const resp = await service.sendAuthCode('681234567');
		expect(resp.err_code).toEqual(0);
	});
});
