import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {TranslateModule } from '@ngx-translate/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { AuthService } from '@app/core/services/auth.service';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import {ROUTES} from "../../../app-routing.module";
import {of, timer} from "rxjs";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {Operator} from "@app/core/services/store/operator";
import {SESS_DATA} from "../../features/mocks/session-data";
import {ReportsService} from "@app/core/services/report/reports.service";

describe('AuthService', () => {
	let appStoreService: AppStoreService;
	let authService: AuthService | any;
	let lotteriesService: LotteriesService | any;
	let reportsService: ReportsService;
	let csConfig;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule.withRoutes(ROUTES),
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				HttpService,
				AuthService,
				LotteriesService,
				PrintService,
				AppStoreService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},
				ReportsService
			]
		});

		TestBed.inject(LogService);
		appStoreService = TestBed.inject(AppStoreService);
		authService = TestBed.inject(AuthService);
		lotteriesService = TestBed.inject(LotteriesService);
		reportsService = TestBed.inject(ReportsService);
		csConfig = await fetch('/config-alt-web.json').then(r => r.json());
	});

	it('should be created', () => {
		expect(authService).toBeTruthy();
	});

	it('test logoutOperator method', () => {
		spyOn(authService, 'logoutOperator').and.callThrough();
		authService.logoutOperator();
		expect(authService.logoutOperator).toHaveBeenCalled();
	});

	it('test presenceControl method', () => {
		appStoreService.isLoggedIn$$.next(true);
		spyOn(authService, 'presenceControl').and.callThrough();
		authService.presenceControl();
		expect(authService.presenceControl).toHaveBeenCalled();
	});

	xit('test auth3 method', async () => {
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		spyOn(authService, 'auth3').and.callThrough();
		(authService as any).loadingDataChain = () => of(true);
		authService.auth3();
		await timer(2000).toPromise();
		expect(authService.auth3).toHaveBeenCalled();
	});

	xit('test auth3 method 2', async () => {
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		spyOn(authService, 'auth3').and.callThrough();
		reportsService.reload = () => {
			return new Promise((resolve) => {
				resolve({
					message: 'Ошибка загрузки отчетов',
					messageDetails: 'Подробности ошибки загрузки отчетов',
					code: 234
				});
			});
		};
		authService.auth3();
		await timer(2000).toPromise();
		expect(authService.auth3).toHaveBeenCalled();
	});

	it('test auth3 method 3', async () => {
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
		const op = JSON.parse(SESS_DATA.data[0].value);
		appStoreService.operator.next(new Operator(op._userId, op._sessionId, op._operCode, op._access_level));
		spyOn(authService, 'auth3').and.callThrough();
		reportsService.reload = () => {
			return new Promise((resolve, reject) => {
				reject({
					message: 'Ошибка загрузки отчетов',
					messageDetails: 'Подробности ошибки загрузки отчетов',
					code: 234
				});
			});
		};
		authService.auth3();
		await timer(2000).toPromise();
		expect(authService.auth3).toHaveBeenCalled();
	});


	it('test lotteriesReload method', () => {
		lotteriesService.reload = () => {
			return new Promise((resolve) => {
				resolve({
					message: 'Ошибка загрузки лотерей',
					code: 123
				});
			});
		};
		spyOn(authService, 'lotteriesReload').and.callThrough();
		authService.lotteriesReload();
		expect(authService.lotteriesReload).toHaveBeenCalled();
	});

	it('test presenceTimerHandler method', () => {
		authService.setPresenceTimer(false);
		spyOn(authService, 'presenceTimerHandler').and.callThrough();
		authService.presenceTimerHandler();
		expect(authService.presenceTimerHandler).toHaveBeenCalled();
	});

	it('test presenceTimerHandler method 2', () => {
		authService.setPresenceTimer(false);
		authService.presenceControlTime = Date.now() + 100000;
		spyOn(authService, 'presenceTimerHandler').and.callThrough();
		authService.presenceTimerHandler();
		expect(authService.presenceTimerHandler).toHaveBeenCalled();
	});
});
