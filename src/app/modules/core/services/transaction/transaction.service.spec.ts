import {Injector} from '@angular/core';
import {Router} from '@angular/router';

import {TestBed} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

import {TransactionService} from '@app/core/services/transaction/transaction.service';
import {HttpClient} from '@angular/common/http';
import {HttpService} from '@app/core/net/http/services/http.service';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {RouterTestingModule} from '@angular/router/testing';
import {ReportsService} from '@app/core/services/report/reports.service';
import {AuthService} from '@app/core/services/auth.service';
import {LotteriesService} from '@app/core/services/lotteries.service';
import {DialogInfo} from '@app/core/error/dialog';
import {DialogConfig} from '@app/core/dialog/types';
import {InitService} from '@app/core/services/init.service';
import {BarcodeReaderService} from '@app/core/barcode/barcode-reader.service';
import {TotalCheckStorageService} from '@app/total-check/services/total-check-storage.service';
import {HamburgerMenuService} from '@app/hamburger/services/hamburger-menu.service';
import {LogService} from '@app/core/net/ws/services/log/log.service';
import {PrintService} from '@app/core/net/ws/services/print/print.service';
import {ResponseCacheService} from '@app/core/services/response-cache.service';
import {StorageTransaction, Transaction} from '@app/core/services/transaction/transaction';
import {InitGuard} from '@app/core/guards/init.guard';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LogOutService} from '@app/logout/services/log-out.service';
import {PeripheralService} from '@app/core/services/peripheral.service';
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {sid} from "../../../features/mocks/mocks";
import {of} from "rxjs";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";

describe('TransactionService', () => {
	let service: TransactionService | any;
	jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			providers: [
				LogService,
				ReportsService,
				HttpService,
				AuthService,
				LotteriesService,
				HttpClient,
				InitService,
				InitGuard,
				Injector,
				HamburgerMenuService,
				LogOutService,
				PeripheralService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},
				AppStoreService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(TransactionService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('check function -> hasSufficientParams', () => {
		const injector = TestBed.inject(Injector);
		const transaction = new Transaction(injector);
		service['transaction'] = transaction;
		expect(transaction.hasSufficientParams()).toBe(false);

		const storageTransaction = new StorageTransaction();
		storageTransaction.params = {
			url: 'https://dev.cs.emict.net/ZabavaRegSrvDev',
			trans_id: '400016531460',
			user_id: '10567',
			sess_id: '346575f0-aebe-484b-845e-0b6f79e9509b',
			game_code: 3
		};
		transaction['_store'] = storageTransaction;
		expect(transaction.hasSufficientParams()).toBe(true);
	});

	it('test printing method', () => {
		spyOn(service, 'printing').and.callThrough();
		service.getPrinterInfo = () => of({
			paperWidth: 80,
			pixelsPerLine: 384,
			charsPerLine: 3
		});
		service.printing(LotteryGameCode.Zabava, {
			ticket: [
				{
					bet_count: 0,
					bet_sum: '20.00',
					description: 'Билет Лото-Забава N 6',
					draw_date: '2020-07-21 06:43:55',
					draw_num: 1742,
					game_comb: [{
						game_field: '01003253631318385165051600486613273753630627374773'
					},          {
						game_field: '02243950621426425500082900557215263560711520395671'
					},          {
						game_field: '10173654670422455474032500596809224549740921340069'
					}],
					id: '4497344957',
					mac_code: '003017420000000692963569',
					portion_id: 0,
					regdate: '2020-05-23 14:34:11',
					ticket_num: 6,
					ticket_snum: 92963569
				}
			],
			ticket_templ_url: '/esap/templates/zabava-ticket-0-utf8-28.xml',
		});
		expect(service.printing).toHaveBeenCalled();
	});
});

// -----------------------------
//  Static functions
// -----------------------------

class HamburgerMenuServiceStub extends HamburgerMenuService {
	constructor() {
		super({} as AppStoreService, {} as HttpService);
	}
}

export class TransactionServiceStub extends TransactionService {
	constructor() {
		super(
			{} as HttpService,
			{} as AppStoreService,
			{} as PrintService,
			{} as DialogContainerService,
			{} as TranslateService,
			{} as Router,
			{} as BarcodeReaderService,
			{} as TotalCheckStorageService,
			new HamburgerMenuServiceStub(),
			{} as Injector,
			{} as ResponseCacheService,
			{} as PeripheralService,
			{} as LogOutService
		);
	}

	setCannotBeUndoneState(): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			reject({
				message: 'Тестовая ошибка',
				messageDetails: 'Ошибка отмены транзакции',
				code: 123
			});
		});
	}
}
