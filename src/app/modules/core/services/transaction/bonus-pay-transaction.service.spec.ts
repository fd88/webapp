import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateModule } from '@ngx-translate/core';

import { BonusPayTransactionService } from '@app/core/services/transaction/bonus-pay-transaction.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { HttpService } from '@app/core/net/http/services/http.service';
import { ResponseCacheService } from '@app/core/services/response-cache.service';
import { PrintService } from '@app/core/net/ws/services/print/print.service';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';


import { TotalCheckStorageService } from '@app/total-check/services/total-check-storage.service';
import { HamburgerMenuService } from '@app/hamburger/services/hamburger-menu.service';
import { LotteriesService } from '@app/core/services/lotteries.service';

xdescribe('BonusPayTransactionService', () => {
	let service: BonusPayTransactionService;
	let appStoreService: AppStoreService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				TranslateModule.forRoot(),
				RouterTestingModule.withRoutes([])
			],
			providers: [
				LogService,
				HamburgerMenuService,
				HttpService,
				ResponseCacheService,
				PrintService,
				StorageService,
				TotalCheckStorageService,
				LotteriesService,

				AppStoreService
			]
		});

		TestBed.inject(LogService);
		service = TestBed.inject(BonusPayTransactionService);
	});

	beforeEach(waitForAsync(async() => {
		appStoreService = TestBed.inject(AppStoreService);
	}));

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
