import {TestBed} from '@angular/core/testing';
import {DatePipe} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';

import {TranslateModule, TranslateService} from '@ngx-translate/core';

import {GameResultsService} from '@app/core/services/results/game-results.service';
import {AppStoreService} from '@app/core/services/store/app-store.service';
import {HttpService} from '@app/core/net/http/services/http.service';
import {TransactionService} from '@app/core/services/transaction/transaction.service';
import {DialogContainerService} from '@app/core/dialog/services/dialog-container.service';
import {ReportsService} from '@app/core/services/report/reports.service';
import {StorageService} from '@app/core/net/ws/services/storage/storage.service';
import {PrintService} from '@app/core/net/ws/services/print/print.service';
import {MslCurrencyPipe} from '@app/shared/pipes/msl-currency.pipe';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LotteriesService} from '@app/core/services/lotteries.service';
import {PrintResultTemplate, PrintResultTypes} from "@app/core/services/results/results-utils";
import {DialogContainerServiceStub} from "@app/core/dialog/services/dialog-container.service.spec";
import {LogService} from "@app/core/net/ws/services/log/log.service";

const printData = [
	{
		"key": PrintResultTypes.LotteryName,
		"value": [
			"lottery.zabava.loto_zabava"
		]
	},
	{
		"key": PrintResultTypes.DrawNumber,
		"value": [
			"1762"
		]
	},
	{
		"key": PrintResultTypes.DrawDate,
		"value": [
			"2020-09-17 15:10:02"
		]
	},
	{
		"key": PrintResultTypes.WinCombWithLines,
		"value": [
			"01",
			"16",
			"32",
			"47",
			"18",
			"19",
			"34",
			"20",
			"21",
			"36",
			"08",
			"07",
			"05",
			"11",
			"25",
			"40",
			"54",
			"22",
			"38",
			"52",
			"13",
			"14",
			"15",
			"29",
			"28",
			"42",
			"57",
			"58",
			"72",
			"73",
			"71",
			"70",
			"69",
			"68",
			"67",
			"66",
			"65",
			"50",
			"45",
			"44",
			"43",
			"30",
			"03",
			"02",
			"04",
			"09",
			"10",
			"06",
			"12",
			"27",
			"26",
			"24",
			"23"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"lottery.category",
			"lottery.win"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"3 горизонт.без підков",
			"5000000.00"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"3 горизонт.з підков.",
			"100000.00"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"2 горизонт.або хрест",
			"24.85"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"1 горизонт.або 1 діаг.",
			"24.85"
		]
	},
	{
		"key": PrintResultTypes.Line
	},
	{
		"key": PrintResultTypes.OneColumnTable,
		"value": [
			"Конкурс Парочка"
		]
	},
	{
		"key": PrintResultTypes.OneColumnTable,
		"value": [
			"lottery.win_combination"
		]
	},
	{
		"key": PrintResultTypes.WinCombWOLines,
		"value": [
			"01",
			"02",
			"22",
			"03",
			"23",
			"46",
			"00",
			"00",
			"00"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"lottery.sub_category",
			"lottery.win"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"Повний збiг",
			"300000.00"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"Будь-який кут з 5-ти",
			"7500.00"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"Будь-яка лiнiя з 3-х",
			"100.00"
		]
	},
	{
		"key": PrintResultTypes.TwoColumnTable,
		"value": [
			"Верхня вершина",
			"6.22"
		]
	},
	{
		"key": PrintResultTypes.OneColumnTable,
		"value": [
			"Джекпот наступн.тиражу -  грн."
		]
	},
	{
		key: PrintResultTypes.DashedLine
	},
	{
		key: PrintResultTypes.DoubleLine
	},
	{
		key: PrintResultTypes.TextCenter,
		value: ['test']
	},
	{
		key: PrintResultTypes.TwoColumnTableFast,
		value: ['test 1', 'test 2']
	},
	{
		key: PrintResultTypes.TwoColumnTableCustom,
		value: ['test 2', 'test 3']
	},
	{
		key: PrintResultTypes.NextDrawJackPot
	},
	{
		key: 999999
	}
];

describe('GameResultsService', () => {
	let service: GameResultsService;
	let appStoreService: AppStoreService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule.withRoutes([]),
				HttpClientTestingModule,
				TranslateModule.forRoot()
			],
			providers: [
				AppStoreService,
				StorageService,
				HttpService,
				TransactionService,
				PrintService,
				{
					provide: DialogContainerService,
					useClass: DialogContainerServiceStub
				},
				TranslateService,
				ReportsService,
				DatePipe,
				MslCurrencyPipe,
				LotteriesService,
				LogService
			]
		});
		TestBed.inject(LogService);
		service = TestBed.inject(GameResultsService);
		appStoreService = TestBed.inject(AppStoreService);
		const csConfig = await fetch('/config-alt-web.json').then(r => r.json());
		appStoreService.Settings.populateEsapActionsMapping(csConfig);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test printLastResults method', () => {
		spyOn(service, 'printLastResults').and.callThrough();
		service.printLastResults({ printData, printTemplate: PrintResultTemplate, copiesCount: 1 });
		expect(service.printLastResults).toHaveBeenCalled();
	});

});
