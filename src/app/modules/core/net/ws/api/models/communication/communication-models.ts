import { ApiRequest } from '../../types';

/**
 * Идентификатор сервиса коммуникаций.
 */
export const COMMUNICATION_SERVICE_OWNER = 'ua.msl.periphery.communication_service';

/**
 * Идентификатор приложения коммуникационного сервиса.
 */
export const COMMUNICATION_SERVICE_APPID = 'ua.msl.alt.service.communication';

/**
 * Абстрактный класс для создания запросов через коммуникационный сервис.
 */
export abstract class CommunicationRequest extends ApiRequest {
	/**
	 * Идентификатор устройства.
	 */
	deviceId: string;
}

/**
 * Модель коммуникационного устройства.
 */
export interface ICommunicationDevice {

	/**
	 * Идентификатор устройства.
	 */
	id: string;

	/**
	 * Тип коммуникационного устройства.
	 */
	type: CommunicationDeviceType;

	/**
	 * Состояние готовности устройства.
	 */
	state: CommunicationDeviceState;

	/**
	 * Состояние сети.
	 */
	net: INetworkStatus;

	/**
	 * Состояние модема.
	 */
	modem: IModemStatus;

}

/**
 * Тип коммуникационного устройства.
 * Существуют следующие типы:
 * - {@link RadioModem}
 * - {@link Ethernet}
 */
export enum CommunicationDeviceType {
	RadioModem			= 'radio-modem',
	Ethernet			= 'ethernet'
}

/**
 * Состояние готовности устройства.
 * Когда устройство готово к передаче данных, параметр имеет значение "ready".
 * Для отключенных устройств значение параметра устанавливается в "off".
 * Остальные значения говорят о промежуточном состоянии.
 * Набор таких состояний и переходы между ними зависит от типа устройства.
 * Существуют следующие статусы:
 * - {@link Off}
 * - {@link Initialization}
 * - {@link SimSwitching}
 * - {@link GsmRegistering}
 * - {@link GsmConnecting}
 * - {@link NetworkConfiguring}
 * - {@link Ready}
 * - {@link ErrorConnect}
 */
export enum CommunicationDeviceState {
	Off					= 'off',
	Initialization		= 'initialization',
	SimSwitching		= 'simSwitching',
	GsmRegistering		= 'gsmRegistrering',
	GsmConnecting		= 'gsmConnecting',
	NetworkConfiguring	= 'networkConfiguring',
	Ready				= 'ready',
	ErrorConnect		= 'errorConnect'
}

/**
 * Объект, описывающий состояние сети.
 */
export interface INetworkStatus {

	/**
	 * Состояние готовности сетевого интерфейса.
	 * Значение true говорит о готовности устройства к передаче данных.
	 */
	up: boolean;

	/**
	 * Текущий IP адрес интерфейса.
	 */
	ipAddress: string;

}

/**
 * Объект, описывающий состояние модема (для типа устройства "radio-modem").
 */
export interface IModemStatus {

	/**
	 * Аппаратная конфигурация модема.
	 */
	hw: ICommunicationHardwareDescription;

	/**
	 * Режим работы сети передачи данных.
	 * "NOSERVICE", "GPRS", "3G"
	 */
	mode: string;

	/**
	 * Информация о регистрации модема в сети провайдера.
	 */
	network: IModemNetworkStatus;

	/**
	 * Уровень (качество) сигнала (проценты от 0 до 100).
	 */
	signal: number;

	/**
	 * Информация о текущей sim карте.
	 */
	sim: IModemSimSlotStatus;

}

/**
 * Объект, описывающий аппаратную конфигурацию модема.
 */
export interface ICommunicationHardwareDescription {

	/**
	 * IMEI модема (для типа устройства "radio-modem").
	 */
	imei: string;

	/**
	 * Массив объектов с информацией об установленных SIM картах (для типа устройства "radio-modem").
	 * Количество элементов соответствует количеству слотов для карт в модеме (2).
	 */
	sim: Array<ISimCard>;

}

/**
 * Модель информации о регистрации модема в сети провайдера.
 */
export interface IModemNetworkStatus {

	/**
	 * Идентификатор провайдера мобильной связи, в сети которого зарегистрирован модем.
	 */
	providerId: string; // "25503"

	/**
	 * Название провайдера мобильной связи, в сети которого зарегистрирован модем.
	 */
	providerName: string; // "KYIVSTAR"

	/**
	 * Дата регистрации в сети. ???
	 */
	registerTime: string; // "2019-04-04 16:18:25.926291958 +0300"

	/**
	 * Состояние регистрации модема в сети:
	 * - true - модем зарегистрирован в сети провайдера
	 * - false - идет процесс поиска сети и регистрации в сети провайдера.
	 * В этом состоянии остальные параметры объекта network не определены.
	 */
	registered: boolean;

}

/**
 * Информация о текущей sim карте.
 */
export interface IModemSimSlotStatus {

	/**
	 * Номер слота текущей активной сим карты.
	 */
	slot: number;

	/**
	 * Состояние sim карты.
	 */
	status: SimSlotStatus;

}

/**
 * Модель объекта с информацией об установленной SIM карте.
 */
export interface ISimCard {

	/**
	 * ICCID карты.
	 */
	iccid: string; // "89380062300052245742"

	/**
	 * Issuer identification number - идентификатор провайдера, выпустившего сим-карту.
	 */
	iin: string ; // "38006"

	/**
	 * Флаг присутствия SIM карты в соответствующем слоте.
	 */
	inserted: boolean;

	/**
	 * Название провайдера, выпустившего сим-карту.
	 */
	issuer: string; // "life:)"

}

/**
 * Состояние sim карты.
 * - {@link Selecting} - означает, что идет процесс переключения sim карты в модеме,
 * в этом состоянии активная сим карта еще не выбрана, остальные поля объекта sim не определены.
 * - {@link Initialization}
 * - {@link Ready}
 * - {@link NotInserted}
 * - {@link Error}
 */
export enum SimSlotStatus {
	Selecting			= 'selecting',
	Initialization		= 'initialization',
	Ready				= 'ready',
	NotInserted			= 'notInserted',
	Error				= 'error'
}

/**
 * Интерфейс объекта, содержащего информацию о состоянии модема.
 */
export interface ICommunicationNotification {

	/**
	 * Тип изменения.
	 * "update" устанавливается, если соответствующий параметр добавляется в структуру состояния или меняет свое значение.
	 * Если параметр удаляется из структуры, например, удаляется информация о текущем провайдере в момент переключения сим-карт,
	 * то "changeType" будет иметь значение "remove"
	 */
	changeType: string; // "update"

	/**
	 * Идентификатор устройства.
	 */
	deviceId: string; // "IRZ"

	/**
	 * Произошедшее событие.
	 */
	event: string; // "devStateChanged"

	/**
	 * Название изменившегося параметра, например, "state", "net.ipAddress", "modem.sim.status"
	 */
	param: string; // "modem.mode"

	/**
	 * Новое значение параметра.
	 * Это поле отсутствует если "changeType" имеет значение "remove".
	 */
	value: string; // "3G"

}
