/**
 * Строковой идентификатор сервиса логирования.
 */
export const LOG_SERVICE_OWNER = 'ua.msl.periphery.log_service';

/**
 * Модель сообщения, которое будет залогировано на терминале.
 */
export interface LogData {

	/**
	 * Unix time stamp (millisecond).
	 */
	timestamp: string;

	/**
	 * Название модуля, сгенерировавшего сообщение.
	 */
	moduleName: string;

	/**
	 * Тип сообщения.
	 * Допускаются следующие типы сообщений:
	 * - DEBUG - сообщения используемые при отладке приложения
	 * - ERROR - сообщения об ошибках
	 * - INFO - информационные сообщения
	 */
	typeMessage: string;

	/**
	 * Текст сообщения.
	 */
	message: string;

}
