import { COMMUNICATION_SERVICE_APPID, CommunicationRequest } from '@app/core/net/ws/api/models/communication/communication-models';
import { IResponse } from '@app/core/net/ws/api/types';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель запроса состояния коммуникационного сервиса.
 */
export class SwitchProviderReq extends CommunicationRequest {

	/**
	 * Конструктор запроса.
	 */
	constructor() {
		super(COMMUNICATION_SERVICE_APPID, 'switchProvider');
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа состояния коммуникационного сервиса.
 */
export interface SwitchProviderResp extends IResponse {
	/**
	 * Поле для хранения любых данных.
	 */
	xxx: any;
}
