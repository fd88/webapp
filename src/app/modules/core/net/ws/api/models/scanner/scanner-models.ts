import { INotification } from '@app/core/net/ws/api/types';

/**
 * Идентификатор сервиса сканирования.
 */
export const SCANNER_SERVICE_OWNER = 'ua.msl.periphery.scanner_service';

/**
 * Состояние сканера.
 * - {@link Connected} - подключен
 * - {@link Disconnected} - отключен
 */
export enum ScannerState {
	Connected		= 'connected',
	Disconnected	= 'disconnected'
}

/**
 * Идентификатор события ("scanned") - "сканирован документ".
 */
export type ScannerEvents = 'scanned';

/**
 * Тип данных, распознанных сканером на сканированном документе:
 * - "barcode" - отсканирован билет со штрих-кодом
 * - "couponOMR" - отсканирован игровой купон с информацией в виде поля меток, отмечаемых игроком
 */
export type ScannedType = 'barcode' | 'couponOMR';

/**
 * Типы штрих-кодов, которые может распознать сканер.
 */
export type BarcodeType = 'EAN-13' | 'CODE128' | 'QR';

/**
 * Массив меток, отмеченных игроком на игровом купоне.
 */
export type MarksType = Array<number>;

/**
 * Смотри описание API Scan Service
 */
export interface ScannedTypeBarcode {
	/**
	 * Значение штрих-кода.
	 */
	value: string;
	/**
	 * Тип штрих-кода.
	 */
	barcodeType?: BarcodeType;
}

/**
 * Модель нотификации от сканера купона, содержащая всю необходимую информацию о купоне и метках.
 */
export interface ScannedTypeCouponOMR {
	/**
	 * Идентификатор купона.
	 */
	couponId: string;
	/**
	 * Количество строк на купоне.
	 */
	rowCount: number;
	/**
	 * Количество столбцов на купоне.
	 */
	columnCount: number;
	/**
	 * Массив меток, отмеченных игроком на игровом купоне.
	 */
	marks: Array<MarksType>;
}

/**
 * Модель уведомлений, смотри описание API Scan Service - scanned
 */
export interface Scanned extends INotification {
	/**
	 * Идентификатор события.
	 */
	event: ScannerEvents;

	/**
	 * Тип данных, распознанных сканером на сканированном документе.
	 */
	type: ScannedType;

	/**
	 * Содержимое сканированного документа. Формат зависит от типа данных.
	 */
	data: ScannedTypeBarcode | ScannedTypeCouponOMR;
}
