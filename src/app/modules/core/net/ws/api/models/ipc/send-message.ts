import { ApiRequest, IResponse } from '@app/core/net/ws/api/types';
import { IPC_SERVICE_APPID } from '@app/core/net/ws/api/models/ipc/ipc-models';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API IPC Service - sendMessage.
 * Сервис отправляет сообщение указанному приложению через механизм нотификаций.
 * Ответ не содержит результат операции - OK, если сообщение передано указанному получателю или код ошибки, если не передано.
 */
export class SendMessageReq extends ApiRequest {

	/**
	 * Конструктор модели
	 */
	constructor() {
		super(IPC_SERVICE_APPID, 'sendMessage');
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API IPC Service - {@link SendMessageReq sendMessage}
 */
export interface SendMessageResp extends IResponse {
	/**
	 * Пустой объект
	 */
	_: string;
}
