import { ApiRequest, IResponse } from '@app/core/net/ws/api/types';
import { IPC_SERVICE_APPID } from '@app/core/net/ws/api/models/ipc/ipc-models';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель запроса регистрации клиента.
 * По данному запросу сервис зарегистрирует нового клиента, которому можно адресовать сообщения.
 * Установленный "appId" будет являться адресом клиента для получения сообщений от других приложений.
 * Ответ не содержит каких-либо дополнительных параметров (только результат операции).
 */
export class RegisterClientReq extends ApiRequest {

	/**
	 * Конструктор запроса.
	 */
	constructor() {
		super(IPC_SERVICE_APPID, 'registerClient');
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API IPC Service - {@link RegisterClientReq registerClient}
 */
export interface RegisterClientResp extends IResponse {
	/**
	 * Поле для хранения любых данных.
	 */
	_: string;
}
