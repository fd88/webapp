import { ApiRequest, IResponse } from '@app/core/net/ws/api/types';
import { HardwareInfoData } from '@app/core/net/ws/api/models/storage/storage-models';

/**
 * Модель для вызова API Storage Service - hardwareInfo
 */
export class StorageHardwareInfoReq extends ApiRequest {
	/**
	 * Конструктор модели
	 * @param appId
	 */
	constructor(appId: string) {
		super(appId, 'hardware_info');
	}
}

/**
 * Модель ответа на запрос API Storage Service - hardwareInfo
 */
export interface StorageHardwareInfoResp extends IResponse {
	/**
	 * Список информации о железе
	 */
	data: Array<HardwareInfoData>;
}
