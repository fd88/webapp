import { IResponse } from '@app/core/net/ws/api/types';
import {
	COMMUNICATION_SERVICE_APPID,
	CommunicationRequest,
	ICommunicationDevice
} from '@app/core/net/ws/api/models/communication/communication-models';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель запроса состояния коммуникационного сервиса.
 */
export class GetStateReq extends CommunicationRequest {

	/**
	 * Конструктор запроса.
	 */
	constructor() {
		super(COMMUNICATION_SERVICE_APPID, 'getState');
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API Communication Service - {@link GetStateReq getState}
 */
export interface GetStateResp extends IResponse {
	/**
	 * Идентификатор текущего устройства, используемого для связи с ЦС.
	 */
	activeDevice: string;

	/**
	 * Массив объектов, описывающих коммуникационные устройства.
	 */
	devices: Array<ICommunicationDevice>;
}
