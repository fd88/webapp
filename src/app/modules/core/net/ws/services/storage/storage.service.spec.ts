import { TestBed } from '@angular/core/testing';
import { StorageService } from '@app/core/net/ws/services/storage/storage.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';

import { AppStoreService } from "@app/core/services/store/app-store.service";
import {HttpClientModule} from "@angular/common/http";
import {HttpService} from "@app/core/net/http/services/http.service";

xdescribe('StorageService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				LogService,
				StorageService,
				AppStoreService,
				HttpService
			]
		});

		TestBed.inject(LogService);
	});

	it('should be created', () => {
		const service = TestBed.inject(StorageService);
		expect(service).toBeTruthy();
	});
});
