import { TestBed } from '@angular/core/testing';
import { LogService } from './log.service';
import {AuthService} from "@app/core/services/auth.service";
import {DialogContainerService} from "@app/core/dialog/services/dialog-container.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {PrintService} from "@app/core/net/ws/services/print/print.service";
import {HttpService} from "@app/core/net/http/services/http.service";
import {BarcodeReaderService} from "@app/core/barcode/barcode-reader.service";
import {GameResultsService} from "@app/core/services/results/game-results.service";
import {LotteriesService} from "@app/core/services/lotteries.service";
import {ReportsService} from "@app/core/services/report/reports.service";
import {PeripheralService} from "@app/core/services/peripheral.service";
import {DatePipe} from "@angular/common";
import {TransactionService} from "@app/core/services/transaction/transaction.service";
import {ResponseCacheService} from "@app/core/services/response-cache.service";
import {ErrorHandler} from "@angular/core";
import {AppErrorHandler} from "@app/core/error/handler";
import {HttpClientModule} from "@angular/common/http";

describe('LogService', () => {
	let service: LogService | any;
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule
			],
			providers: [
				AuthService,
				DialogContainerService,
				AppStoreService,
				StorageService,
				LogService,
				PrintService,
				HttpService,
				BarcodeReaderService,
				GameResultsService,
				LotteriesService,
				ReportsService,
				PeripheralService,

				DatePipe,

				TransactionService,
				ResponseCacheService,
				{
					provide: ErrorHandler,
					useClass: AppErrorHandler
				}
			]
		});
		service = TestBed.inject(LogService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('test Init method', () => {
		spyOn(service, 'Init').and.callThrough();
		service.Init('ws://localhost/', 200);
		expect(service.Init).toHaveBeenCalled();
	});

	it('test flushDeferredLogs method', () => {
		spyOn(service, 'flushDeferredLogs').and.callThrough();
		service.flushDeferredLogs();
		expect(service.flushDeferredLogs).toHaveBeenCalled();
	});

	it('test onResult handler', () => {
		spyOn(service, 'onResult').and.callThrough();
		service.onResult({
			requestId: '1234',
			errorCode: 1234,
			errorDesc: 'Some test error'
		});
		expect(service.onResult).toHaveBeenCalled();
	});

	it('test onError handler', () => {
		spyOn(service, 'onError').and.callThrough();
		service.onError({
			message: 'test',
			messageDetails: 'Fake error for tests',
			code: 1234
		});
		expect(service.onError).toHaveBeenCalled();
	});
});
