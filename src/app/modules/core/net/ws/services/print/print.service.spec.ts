import { TestBed } from '@angular/core/testing';
import { PrintService } from '@app/core/net/ws/services/print/print.service';

xdescribe('PrintService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [],
			providers: []
		});
	});

	it('should be created', () => {
		const service: PrintService = TestBed.inject(PrintService);
		expect(service).toBeTruthy();
	});
});
