import { HttpHeaders } from '@angular/common/http';
import { IError } from '@app/core/error/types';
import { URLSearchParams } from '@app/core/net/http/api/api-client';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { TerminalRoles } from '@app/core/services/store/operator';


/**
 * Интерфейс сохраненного оператора в localStorage.
 */
export interface SavedOperator {
	/**
	 * Идентификатор оператора.
	 */
	_userId: string;
	/**
	 * Идентификатор сессии.
	 */
	_sessionId: string;
	/**
	 * Код оператора.
	 */
	_operCode: string;
	/**
	 * Уровень доступа пользователя.
	 */
	_access_level: TerminalRoles;
}


/**
 * Интерфейс для наблюдения за результатом выполнения запроса.
 */
export interface IClientResponseObserver {
	/**
	 * Вызывается при получении ответа от ЦС.
	 * @param response Ответ от ЦС.
	 */
	onResult(response: PromiseLike<IResponse> | IResponse | PromiseLike<string> | string): void;

	/**
	 * Вызывается при возникновении ошибки.
	 * @param err Ошибка.
	 */
	onError(err: IError): void;
}

/**
 * Интерфейс модели ответа от ЦС.
 */
export interface IMessage {
	/**
	 * Ответ от ЦС.
	 */
	respond?: IResponse;
}

/**
 * Интерфейс модели тела ответа от ЦС.
 */
export interface IResponse {

	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;

	/**
	 * Результат операции.
	 */
	err_code: number;

	/**
	 * Текст ошибки.
	 */
	err_descr: string;

}

/**
 * Интерфейс модели запроса к ЦС.
 */
export interface IRequest {

	/**
	 * URL, по которому производится запрос.
	 */
	url: string;

	/**
	 * Идентификатор транзакции.
	 */
	trans_id: string;

	/**
	 * Контейнер с параметрами запроса.
	 */
	params: URLSearchParams;

	/**
	 * Заголовок запроса.
	 */
	headers: HttpHeaders;

}

/**
 * Интерфейс модели транзакционного запроса в ЦС.
 */
export interface ICancelableRequest extends IRequest {

	/**
	 * Идентификатор пользователя.
	 */
	user_id: string;

	/**
	 * Идентификатор сессии.
	 */
	sess_id: string;

	/**
	 * Код игры.
	 */
	gameCode: LotteryGameCode;

}

/**
 * Интерфейс модели соответствия URL запроса к сервису ЦС имени парметра ACTION.
 */
export interface IActionUrl {

	/**
	 * Имя операции в ЦС.
	 */
	action: string;

	/**
	 * Ссылка, соответствующая имени опреации в ЦС.
	 */
	url: string;

}


/**
 * Интерфейс терминалов и точек продаж на которые назначен пользователь
 */
export interface AccessPosListItemGS {
	/**
	 * Объект терминала или точки продажи
	 */
	pos: {
		/**
		 * Код терминала
		 */
		client_id: number;

		/**
		 * Уровень доступа для пользователя на данном терминале
		 */
		access_level: TerminalRoles;

		/**
		 * Код точки продажи
		 */
		pos_code: string;

		/**
		 * Наименование точки продажи
		 */
		pos_name: string;

		/**
		 * Адрес точки продажи
		 */
		pos_addr: string;
	};
}

/**
 * Типы авторизации на втором этапе
 */
export enum Auth2Type {
	/**
	 * По смс
	 */
	SMS = 'sms'
}

/**
 * Интерфейс операции в бек-офисе
 */
export interface BOOperation {
	/**
	 * Описание операции в бек-офисе
	 */
	descr: string;
	/**
	 * Идентификатор операции в бек-офисе
	 */
	id: string;
	/**
	 * Название операции в бек-офисе
	 */
	name: string;
}

/**
 * Интерфейс операций в бек-офисе
 */
export interface BOOperations {
	/**
	 * Объект операции в бек-офисе
	 */
	o: BOOperation;
}

/**
 * Интерфейс элемента списка клиентских телефонов
 */
export interface ClientPhoneItemAS {
	/**
	 * Идентификатор клиента
	 */
	client_id: number;
	/**
	 * Номер телефона без кода страны и лидирующего нуля
	 */
	login: string;
}
