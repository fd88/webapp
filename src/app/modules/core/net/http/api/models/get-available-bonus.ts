import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetAvailableBonus
 */
export class GetAvailableBonusReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param cardNum
	 * @param totalBetSum
	 * @param ticketCount
	 * @param lotteryCode
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNum: string,
		totalBetSum: number,
		ticketCount: number,
		lotteryCode: LotteryGameCode
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetAvailableBonus, true));

		this.params.append(EsapParams.CARD_NUMBER, cardNum);
		this.params.append(EsapParams.TOTAL_BET_SUM, totalBetSum.toString());
		this.params.append(EsapParams.TICKET_COUNT, ticketCount.toString());
		this.params.append(EsapParams.LOTTERY_CODE, lotteryCode.toString());

		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		this.params.remove(EsapParams.PROTO_TYPE);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetAvailableBonus
 */
export interface GetAvailableBonusResp extends IResponse {
	/**
	 * Количество бонусов, доступное для списания в рамках данной покупки билетов, в сотых долях бонуса (1 бонус = 100 в этом поле)
	 */
	available_bonus_sum: number;

	/**
	 * Денежный эквивалент доступного для списания количества бонусов в копейках
	 */
	available_bonus_sum_money: number;

	/**
	 * Количество бонусов, которое будет начислено на сумму покупки билетов, в сотых долях бонуса (1 бонус = 100 в этом поле)
	 */
	bet_sum_bonuses: number;

	/**
	 * Количество бонусов на сумму потраченных на покупку бонусов, если бонусы будут использованы при покупке, в сотых долях бонуса (1 бонус = 100 в этом поле).
	 */
	used_bonuses_bonuses: number;
}
