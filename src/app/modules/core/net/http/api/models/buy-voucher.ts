import { Injector } from '@angular/core';

import { IResponse } from '@app/core/net/http/api/types';
import { ApiClient } from '@app/core/net/http/api/api-client';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова запроса API {@link EsapParams.BuyVoucher BuyVoucher}.
 */
export class BuyVoucherReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {Injector} injector Инжектор зависимостей.
	 * @param {string} summa Сумма пополнения счета игрока в копейках.
	 */
	constructor(injector: Injector, summa: number) {
		const appStoreService = injector.get(AppStoreService);

		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.BuyVoucher));

		if (appStoreService.hasOperator()) {
			this.params.append(EsapParams.USER_ID, appStoreService.operator.value.userId);
			this.params.append(EsapParams.SID, appStoreService.operator.value.sessionId);
		}

		this.params.append(EsapParams.PAY_SUM, `${summa}`);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API {@link EsapParams.BuyVoucher BuyVoucher}.
 */
export class BuyVoucherResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;
	/**
	 * Код ошибки.
	 */
	err_code: number;
	/**
	 * Описание ошибки.
	 */
	err_descr: string;
	/**
	 * Слоган для печати на ваучере.
	 */
	slogan: string;
	/**
	 * Билет, для которого покупается ваучер.
	 */
	ticket: IBVTicket | Array<IBVTicket> | any;
	/**
	 * URL для печати билета.
	 */
	ticket_templ_url: string;
	/**
	 * Время покупки ваучера.
	 */
	time_stamp: string;
}

/**
 * Модель билета, для которого покупается ваучер.
 */
export class IBVTicket implements ITransactionTicket {
	/**
	 * Сумма ставки.
	 */
	bet_sum: string;		// "0.44",
	/**
	 * Дата регистрации.
	 */
	date_reg: string;		// "2019-07-04 09:07:14",
	/**
	 * "Ваучер пополнения"
	 */
	description: string;
	/**
	 * Хэш билета.
	 */
	hash: string;			// "2000049102",
	/**
	 * Идентификатор билета.
	 */
	id: string;				// 2276701829,
	/**
	 * Номер билета.
	 */
	number: string;			// "7",
	/**
	 * Сумма пополнения.
	 */
	pay_sum: string;		// "44",
	/**
	 * Проверочный код.
	 */
	pin_code: string;		// "200-0049-102",
	/**
	 * Действителен до.
	 */
	valid_until: string;	// "2019-08-03 09:07:14"
	/**
	 * Идентификатор клиента.
	 */
	client_id: string;
}


