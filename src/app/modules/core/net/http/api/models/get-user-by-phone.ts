import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetUserByPhone
 */
export class GetUserByPhoneReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param phone Телефон пользователя
	 * @param pin PIN код пользователя (полученный sms-сообщением)
	 */
	constructor(
		appStoreService: AppStoreService,
		phone: string,
		pin: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetUserByPhone));

		this.params.append(EsapParams.PHONE, phone);
		this.params.append(EsapParams.PIN, pin);
		this.params.remove(EsapParams.PROTO_TYPE);
		// this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetUserByPhone
 */
export interface GetUserByPhoneResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	/**
	 * Идентификатор сессии
	 */
	sid: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: number;

	/**
	 * Номер карты лояльности
	 */
	card_num: string;
}
