import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApiClient } from '@app/core/net/http/api/api-client';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API {@link EsapActions.WinPay} для обычных лотерей.
 */
export class WinPayReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService централизованое хранилище данных
	 * @param {string} barcode Маккод билета.
	 * @param {string} winSum Сумма выплачиваемого выигрыша.
	 * @param {string} smsCode Код подвтерждения из СМС
	 * @param {string} cashierID Для web-кассы - идентификатор авторизованного кассира
	 */
	constructor(appStoreService: AppStoreService, barcode: string, winSum: string, smsCode?: string, cashierID?: string) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(barcode, EsapActions.WinPay));

		// if (appStoreService.hasOperator()) {
		// 	this.params.append(EsapParams.USER_ID, appStoreService.operator.getValue().userId);
		// 	this.params.append(EsapParams.SID, appStoreService.operator.getValue().sessionId);
		// }

		this.params.append(EsapParams.BARCODE, barcode);
		this.params.append(EsapParams.WIN_SUM, winSum);

		if (!!smsCode) {
			this.params.append(EsapParams.PLAYER_AUTH_CODE, smsCode);
		}

		if (!!cashierID) {
			this.params.append(EsapParams.CASHIER_ID, cashierID);
		}
	}

}

/**
 * Модель для вызова API {@link EsapActions.WinPay} для лотерей типа БМЛ/ТМЛ.
 */
export class WinPayPaperReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService централизованое хранилище данных
	 * @param {string} barcode Маккод билета.
	 * @param {string} winBarCode Штрихкод, в котором зашифрован код категории выигрыша (используется только для бумажных МЛ).
	 * @param {string} winSum Сумма выплачиваемого выигрыша.
	 * @param {string} smsCode Код подвтерждения из СМС
	 * @param {string} cashierID Для web-кассы - идентификатор авторизованного кассира
	 */
	constructor(
				appStoreService: AppStoreService, barcode: string, winBarCode: string,
				winSum: string, smsCode?: string, cashierID?: string
				) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.TML_BML, EsapActions.WinPay));

		// if (appStoreService.hasOperator()) {
		// 	this.params.append(EsapParams.USER_ID, appStoreService.operator.getValue().userId);
		// 	this.params.append(EsapParams.SID, appStoreService.operator.getValue().sessionId);
		// }

		this.params.append(EsapParams.BARCODE, barcode);
		this.params.append(EsapParams.WINBARCODE, winBarCode);
		this.params.append(EsapParams.WIN_SUM, winSum);

		if (!!smsCode) {
			this.params.append(EsapParams.PLAYER_AUTH_CODE, smsCode);
		}

		if (!!cashierID) {
			this.params.append(EsapParams.CASHIER_ID, cashierID);
		}
	}

}
