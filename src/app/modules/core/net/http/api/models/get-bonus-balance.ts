import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetBonusBalance
 */
export class GetBonusBalanceReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param cardNum Номер карты лояльности
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNum: string,
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetBonusBalance, true));

		this.params.append(EsapParams.CARD_NUM, cardNum);
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		this.params.remove(EsapParams.PROTO_TYPE);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetBonusBalance
 */
export interface GetBonusBalanceResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	/**
	 * Идентификатор сессии
	 */
	sid?: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: number;

	/**
	 * Номер карты лояльности
	 */
	card_num: string;

	/**
	 * Общая сумма накопленных на карте бонусов (фортуналей) в копейках
	 */
	bonus_balance: number;
}
