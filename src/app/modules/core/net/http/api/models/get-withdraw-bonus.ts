import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetWithdrawBonus
 */
export class GetWithdrawBonusReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param cardNum Номер карты лояльности
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNum: string,
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetWithdrawBonus, true));

		this.params.append(EsapParams.CARD_NUMBER, cardNum);
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		this.params.remove(EsapParams.PROTO_TYPE);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetWithdrawBonus
 */
export interface GetWithdrawBonusResp extends IResponse {
	/**
	 *  Минимальное количество бонусов, которое можно вывести с карты.
	 */
	min_available_bonus_sum: number;

	/**
	 * Денежный эквивалент минимального количества бонусов, которое можно вывести с карты
	 */
	min_available_bonus_sum_money: number;

	/**
	 *  Максимальное количество бонусов, которое можно вывести с карты.
	 */
	max_available_bonus_sum: number;

	/**
	 * Денежный эквивалент максимального количества бонусов, которое можно вывести с карты.
	 */
	max_available_bonus_sum_money: number;
}
