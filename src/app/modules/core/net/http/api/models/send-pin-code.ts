import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API SendPinCode
 */
export class SendPinCodeReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param phone Телефон пользователя
	 */
	constructor(
		appStoreService: AppStoreService,
		phone: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.SendPinCode));

		this.params.append(EsapParams.PHONE, phone);
		this.params.remove(EsapParams.PROTO_TYPE);
		// this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API SendPinCode
 */
export interface SendPinCodeResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;
}
