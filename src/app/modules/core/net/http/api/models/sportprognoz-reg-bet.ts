import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { ISportprognozBetDataItem } from '@app/sportprognoz/services/sportprognoz-lottery.service';

// -----------------------------
//  Request
// -----------------------------

/**
 * Интерфейс модели одного исхода.
 * Свойства модели: {@link o}, {@link s}, {@link b}.
 */
export interface ISportPrognozOutcome {
	/**
	 * Значения кодов исходов встреч. Числа из диапазона 1..7:
	 * - 1 - ничья (0001)
	 * - 2 - победа 1-й команды (0010)
	 * - 3 - ничья или победа 1-й команды (при игре по системе на 2) (0011)
	 * - 4 - победа второй команды (0100)
	 * - 5 - ничья или победа 2-й команды (при игре по системе на 2) (0101)
	 * - 6 - победа 1-й команды или 2-й команды (при игре по системе на 2) (0110)
	 * - 7 - любой исход (система на 3) (0111)
	 * {@link ISportPrognozOutcome}
	 */
	o: number;

	/**
	 * Значения системы. Числа может принимать значение 0, 2, 3.
	 * - 0 - не системная
	 * - 2 - система на 2
	 * - 3 - стстема на 3
	 * {@link ISportPrognozOutcome}
	 */
	s: number;

	/**
	 * Значения блоков. Числа из диапазона 0...2:
	 * - 0 - нет блоков
	 * - 1 - блок на 1
	 * - 2 - блок на 2
	 * {@link ISportPrognozOutcome}
	 */
	b: number;
}

/**
 * Интерфейс с массивом исходов матчей {@link ISportPrognozOutcome} по одной ставке.
 */
export interface ISportPrognozBet {
	/**
	 * Массив исходов матчей {@link ISportPrognozOutcome} по одной ставке.
	 */
	outcomes: Array<ISportPrognozOutcome>;
}

/**
 * Модель массива ставок {@link ISportPrognozBet}, каждая из которых представляет набор
 * из фиксированного числа (сейчас — 12) исходов матчей {@link ISportPrognozOutcome}.
 */
export class ISportPrognozBetData {
	/**
	 * Массив ставок {@link ISportPrognozBet}
	 */
	bets: Array<ISportPrognozBet>;
}

/**
 * Модель для вызова API SportPrognozRegBet.
 */
export class SportPrognozRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param betsBarcode
	 */
	constructor(appStoreService: AppStoreService, betsBarcode: string) {
		super(
			appStoreService,
			appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Sportprognoz, EsapActions.SportPrognozRegBet),
			{
				drawCount: 1
			}
		);

		this.gameCode = LotteryGameCode.Sportprognoz;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.BETS_BARCODE, betsBarcode);
		// this.params.append(EsapParams.PAY_AMOUNT, `${amount * 100}`);
		// this.params.append(EsapParams.BETS, JSON.stringify({bets: betData.betsList}));
		// this.params.append(EsapParams.DRAW_CODE, betData.selectedDraw.draw.code);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Смотри описание API
 */
export class Ticket implements ITransactionTicket {
	/**
	 * Уникальный идентификатор чека для его отмены по 39 команде.
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Маккод (баркод) билета, как строка десятичных цифр.
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Название точки продаж, на которой куплен билет.
	 */
	@IsNotEmpty()
	pos_name: string;

	/**
	 * Дата и время регистрации билета.
	 */
	@IsNotEmpty()
	@IsDateFormat()
	regdate: string;

	/**
	 * Человекопонятное название документа (например, "Чек Спортпрогноз N3434 на тираж 1234").
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Сумма билета (сумма всех ставок в билете).
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Количество ставок.
	 */
	@IsNotEmpty()
	bet_count: number;

	/**
	 * Дата розыгрыша тиража.
	 */
	@IsNotEmpty()
	@IsDateFormat()
	draw_date: string;

	/**
	 * Название (номер) первого тиража.
	 */
	@IsNotEmpty()
	draw_name: string;

	/**
	 * Идентификатор порции документов при регистрации с легких терминалов.
	 */
	portion_id: string;

	/**
	 * Номер билета.
	 */
	@IsNotEmpty()
	ticket_num: number;

	/**
	 * Слоган билета.
	 */
	slogan: string;

	// @IsNotEmpty()
	ticket_templ_url: string;

	/**
	 * Информация о ставках.
	 * Формат данных такой же, как в параметре BETS запроса на регистрацию.
	 */
	@Type(() => ISportPrognozBetData)
	@ValidateNested()
	bets: Array<ISportPrognozBetData>;
}

/**
 * Модель ответа на запрос API SportPrognozRegBet.
 */
export class SportPrognozRegBetResp implements ITransactionResponse {
	/**
	 * Уникальный идентификатор транзакции.
	 */
	client_trans_id: string;

	/**
	 * Код ошибки.
	 */
	err_code: number;

	/**
	 * Описание ошибки.
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя.
	 */
	user_id: string;

	/**
	 * Идентификатор сессии.
	 */
	sid: string;

	/**
	 * Время ответа.
	 */
	time_stamp: string;

	/**
	 * Массив билетов.
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
