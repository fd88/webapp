import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetCardUser
 */
export class GetCardUserReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param phone
	 */
	constructor(
		appStoreService: AppStoreService,
		phone: string,
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetCardUser, true));

		this.params.append(EsapParams.PHONE, phone);
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		this.params.remove(EsapParams.PROTO_TYPE);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetCardUser
 */
export interface GetCardUserResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: number;

	/**
	 * Номер карты лояльности
	 */
	card_num: string;

	/**
	 * Телефон игрока
	 */
	phone: string;
}
