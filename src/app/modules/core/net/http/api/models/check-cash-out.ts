import { IResponse } from '@app/core/net/http/api/types';
import { ApiClient } from '@app/core/net/http/api/api-client';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { Injector } from '@angular/core';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова запроса API {@link EsapParams.CheckCashOut CheckCashOut}.
 */
export class CheckCashOutReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {Injector} injector Инжектор зависимостей.
	 * @param {string} code Код квитанции.
	 */
	constructor(injector: Injector, code: string) {
		const appStoreService = injector.get(AppStoreService);

		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.CheckCashOut));

		if (appStoreService.hasOperator()) {
			this.params.append(EsapParams.USER_ID, appStoreService.operator.value.userId);
			this.params.append(EsapParams.SID, appStoreService.operator.value.sessionId);
		}

		this.params.append(EsapParams.CODE, `${code}`);
	}

}

// -----------------------------
//  Response
// -----------------------------
/**
 * Абстрактная модель ответа на запрос API {@link EsapParams.CheckCashOut CheckCashOut}.
 */
export abstract class CheckCashOutResp implements IResponse {
	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;
	/**
	 * Код ошибки.
	 */
	err_code: number;
	/**
	 * Описание ошибки.
	 */
	err_descr: string;
	/**
	 * Сумма платежа.
	 */
	pay_sum: number;
	/**
	 * Дата истечения платежа.
	 */
	pay_expire_date: string;
	/**
	 * Доступность платежа.
	 */
	pay_enable: number;
	/**
	 * Код причины недоступности платежа.
	 */
	reason_code: number;
	/**
	 * Описание причины недоступности платежа.
	 */
	reason_detail: string;
	/**
	 * Дата регистрации платежа.
	 */
	pay_reg_date: string;
	/**
	 * Номер терминала при регистрации платежа.
	 */
	pay_reg_terminal: number;
}
