// -----------------------------
//  Request
// -----------------------------

import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/ws/api/types";

/**
 * Модель запроса для вызова API AssignCard.
 */
export class AssignCardReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизовано е хранилище данных.
	 * @param cardNumber
	 * @param phone Телефон пользователя
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNumber: string,
		phone: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.AssignCard));

		this.params.append(EsapParams.CARD_NUMBER, cardNumber);
		this.params.append(EsapParams.PHONE, phone);
		this.params.append(EsapParams.OPER_CODE, appStoreService.operator.value.operCode);
		if (!appStoreService.Settings.clientID) {
			this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		}
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API AssignCard
 */
export interface AssignCardResp extends IResponse {

	/**
	 * Идентификатор клиента
	 */
	client_id?: number;

	/**
	 * Идентификатор транзакции. Значение этого параметра соответствует тому, что было в запросе.
	 * Если в запросе параметр CLIENT_TRANS_ID отсутствует, то в ответе его тоже не будет.
	 */
	client_trans_id?: string;

	/**
	 * Телефон пользователя
	 */
	phone: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: number;

	/**
	 * Номер карты лояльности
	 */
	card_num: string;

	/**
	 * Код ошибки
	 */
	err_code: string;

	/**
	 * Описание ошибки
	 */
	err_descr: string;
}
