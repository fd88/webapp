import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApiClient } from '../api-client';
import { IResponse } from '../types';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API BOManageUserRoles.
 */
export class BOManageUserRolesReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {string} userId Идентификатор пользователя.
	 * @param {string} sid Идентификатор сессии.
	 */
	constructor(
		appStoreService: AppStoreService,
		userId: string,
		sid: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.BOManageUserRoles));

		this.params.append(EsapParams.USER_ID, userId);
		this.params.append(EsapParams.TARGET_USER_ID, userId);
		this.params.append(EsapParams.SID, sid);

		this.params.append(EsapParams.ACTION_ID, '0');
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Массив ролей пользователя.
 */
export type BOGetRolesRoles = Array<BOGetRolesRole>;

/**
 * Смотри описание API
 */
export interface BOGetRolesRoleData {
	/**
	 * Идентификатор роли.
	 */
	id: number;
	/**
	 * Название роли.
	 */
	name: string;
	/**
	 * Описание роли.
	 */
	descr: string;
}

/**
 * Смотри описание API
 */
export interface BOGetRolesRole {
	/**
	 * Данные роли пользователя.
	 */
	r: BOGetRolesRoleData;
}

/**
 * Модель ответа на запрос API BOManageUserRoles
 */
export interface BOManageUserRolesResp extends IResponse {
	/**
	 * Идентификатор пользователя.
	 */
	user_id: string;
	/**
	 * Идентификатор сессии.
	 */
	sid: string;
	/**
	 * Массив ролей пользователя.
	 */
	roles: BOGetRolesRoles;
}
