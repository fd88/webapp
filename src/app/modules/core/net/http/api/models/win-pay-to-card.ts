import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApiClient } from '@app/core/net/http/api/api-client';
import {IResponse} from "@app/core/net/http/api/types";

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API {@link EsapActions.WinPayToCard} для обычных лотерей.
 */
export class WinPayToCardReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService централизованое хранилище данных
	 * @param {string} barcode Маккод билета.
	 * @param {string} winSum Сумма выплачиваемого выигрыша.
	 * @param cardNum
	 * @param gameCode
	 */
	constructor(
		appStoreService: AppStoreService,
		barcode: string,
		winSum: string,
		cardNum: string,
		gameCode: number
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(barcode, EsapActions.WinPayToCard));
		this.params.set(EsapParams.PROTO_VER, '2');
		this.params.append(EsapParams.MACCODE, barcode);
		this.params.append(EsapParams.WIN_SUM, winSum);
		this.params.append(EsapParams.CARD_NUM, cardNum);
		this.params.append(EsapParams.OPER_CODE, appStoreService.operator.value.operCode);
		this.params.append(EsapParams.GAME_CODE, gameCode.toString());
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API WinPayToCard
 */
export interface WinPayToCardResp extends IResponse {

}
