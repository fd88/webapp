// -----------------------------
//  Request
// -----------------------------

import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/ws/api/types";

/**
 * Модель запроса для вызова API GetPayedTicketInfo.
 */
export class GetPayedTicketInfoReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизовано е хранилище данных.
	 * @param cardNumber
	 * @param winSum
	 * @param lotteryCode
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNumber: string,
		winSum: number,
		lotteryCode: LotteryGameCode
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetPayedTicketInfo));

		this.params.append(EsapParams.CARD_NUMBER, cardNumber);
		this.params.append(EsapParams.WIN_SUM, winSum.toString());
		this.params.append(EsapParams.LOTTERY_CODE, lotteryCode.toString());

		if (appStoreService.Settings.termCode) {
			this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		}
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetPayedTicketInfo
 */
export interface GetPayedTicketInfoResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	client_trans_id: string;

	/**
	 * Код ошибки. Значение 0 указывает на успешное выполнение операции
	 */
	err_code: number;

	err_descr: string;

	is_win_pay_allowed?: boolean;
}
