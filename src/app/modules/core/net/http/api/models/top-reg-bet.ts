import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IBetDataItem } from '@app/core/game/base-game.service';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API TipTopRegBet для Top
 */
export class TopRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор.
	 *
	 * @param {AppStoreService} appStoreService централизованное хранилище данных
	 * @param {IBetDataItem} betData данные ставки
	 * @param {string} inputMode режим ввода
	 * @param {string} betsCount кол-то ставок
	 * @param {string} drawCount кол-во тиражей
	 * @param {string} isUnique смотри описание API
	 * @param {string} isSingle смотри описание API
	 */
	constructor(
		appStoreService: AppStoreService,
		betData: IBetDataItem,
		inputMode: string,
		betsCount: string,
		drawCount: string,
		isUnique: boolean,
		isSingle: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Top, EsapActions.TipTopRegBet), betData);

		this.gameCode = LotteryGameCode.Top;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.INPUT_MODE, inputMode);
		this.params.append(EsapParams.TOP_DRAW_COUNT, drawCount);
		this.params.append(EsapParams.TOP_BETS_COUNT, betsCount);
		this.params.append(EsapParams.TOP_IS_UNIQUE, isUnique ? '1' : '0');
		this.params.append(EsapParams.TOP_SINGLE_TICKET, isSingle);
	}

	/**
	 * Добавляет параметры в запрос
	 * @param request Запрос
	 * @param betsCount Кол-во ставок
	 * @param drawCount Кол-во тиражей
	 * @param isUnique Признак того, что комбинации в каждом тираже должны быть одинаковые (ТипТоп)
	 * @param isSingle Признак того, что запрашивается единый мультитиражный чек (ТипТоп)
	 * */
	static appendParam(request: CancelableApiClient, betsCount: string, drawCount: string, isUnique: boolean, isSingle: string): void {
		if (request) {
			request.params.append(EsapParams.TOP_DRAW_COUNT, drawCount);
			request.params.append(EsapParams.TOP_BETS_COUNT, betsCount);
			request.params.append(EsapParams.TOP_IS_UNIQUE, isUnique ? '1' : '0');
			request.params.append(EsapParams.TOP_SINGLE_TICKET, isSingle);
		}
	}

}
