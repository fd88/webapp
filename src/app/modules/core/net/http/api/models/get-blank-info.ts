import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetBlankInfo
 */
export class GetBlankInfoReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {number} drawCode Код тиража.
	 * @param {string} macCode Маккод бланка.
	 * @param {string} multiUserID Идентификатор пользователя, который обнаружен на пользовательском компьютере.
	 * Используется для отслеживания нескольких аккаунтов от одного и того же пользователя
	 */
	constructor(
		appStoreService: AppStoreService,
		drawCode: number,
		macCode: string,
		multiUserID?: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetBlankInfo));

		this.params.append(EsapParams.DRAW_CODE, drawCode.toString());
		this.params.append(EsapParams.MACCODE, macCode);
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);

		if (multiUserID) {
			this.params.append(EsapParams.MULTIUSER_ID, multiUserID);
		}
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetBlankInfo
 */
export interface GetBlankInfoResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	/**
	 * Стоимость билета
	 */
	price: number;

	/**
	 * Количество дополнительных игровых комбинаций
	 */
	add_comb_cnt: number;
}
