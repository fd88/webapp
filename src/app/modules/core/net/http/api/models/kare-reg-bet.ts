import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IBetDataItem } from '@app/core/game/base-game.service';
import { IKareBetDataItem } from '@app/kare/interfaces/ikare-bet-data-item';

// -----------------------------
//  Request
// -----------------------------

/**
 * Список вариантов ставок в игре "Каре".
 * - {@link Table1_Cards} - вариант игры, в котором игрок отмечает от 1 до 5 карт.
 * - {@link Table2_Poker} - вариант отмечает комбинации из игры "Покер".
 */
export enum KareGameType {
	Table1_Cards	= 1,
	Table2_Poker	= 2
}

/**
 * Таблица кодов покерных комбинаций.
 */
export enum KarePokerCombinations {
	/**
	 * Код комбинации "Роял Флеш".
	 */
	RoyalFlush		= 1,
	/**
	 * Код комбинации "Стрит Флеш".
	 */
	StraightFlush	= 2,
	/**
	 * Код комбинации "Каре".
	 */
	Square			= 3,
	/**
	 * Код комбинации "Фул Хаус".
	 */
	FullHouse		= 4,
	/**
	 * Код комбинации "Флеш".
	 */
	Flash			= 5,
	/**
	 * Код комбинации "Стрит".
	 */
	Street			= 6,
	/**
	 * Код комбинации "Тройка".
	 */
	Threesome		= 7,
	/**
	 * Код комбинации "Две пары"
	 */
	TwoPairs		= 8,
	/**
	 * Код комбинации "Пара"
	 */
	Pair			= 9,
	/**
	 * Любая комбинация
	 */
	AnyCombinations	= 10
}

/**
 * Список мастей в игре "Каре".
 * - {@link Spades} - Пики
 * - {@link Hearts} - Червы
 * - {@link Diamonds} - Бубны
 * - {@link Clubs} - Трефы
 */
export enum KareCardsSuit {
	Spades			= 0,
	Hearts			= 1,
	Diamonds		= 2,
	Clubs			= 3
}

/**
 * Список карт в игре "Каре".
 * Список расположен в порядке отображения на UI.
 * Значения карт соответствуют таким, по которым можно расчитать код карты из документации.
 * Состоит из:
 * - {@link Card_2} - двойка
 * - {@link Card_3} - тройки
 * - {@link Card_4} - четверка
 * - {@link Card_5} - пятерка
 * - {@link Card_6} - шестерка
 * - {@link Card_7} - семерка
 * - {@link Card_8} - восьмерка
 * - {@link Card_9} - девятка
 * - {@link Card_10} - десятка
 * - {@link Jack} - валет
 * - {@link Queen} - королева
 * - {@link King} - король
 * - {@link Ace} - туз
 */
export enum KareCards {
	Card_2			= 1,
	Card_3			= 2,
	Card_4			= 3,
	Card_5			= 4,
	Card_6			= 5,
	Card_7			= 6,
	Card_8			= 7,
	Card_9			= 8,
	Card_10			= 9,
	Jack			= 10,
	Queen			= 11,
	King			= 12,
	Ace				= 0
}

/**
 * Список режимов ввода данных ставки.
 * - {@link Coupon} - ставка с купона
 * - {@link CouponAuto} - ставка с купона (автогенерация)
 * - {@link Manual} - ставка из интерфейса ручного ввода
 * - {@link QuickPick} - Quick Pick (быстрая ставка автогенерацией)
 */
export enum KareInputMode {
	Coupon = 0,
	CouponAuto = 1,
	Manual = 2,
	QuickPick = 3
}

/**
 * Интерфейс модели одного исхода.
 * Свойства модели: {@link i}, {@link t}, {@link s}, {@link d}.
 */
export interface IKareBetItem {

	/**
	 * Режим ввода данных ставки.
	 */
	i: KareInputMode;

	/**
	 * Тип ставки (соответствует номеру стола) - число, может принимать значение 1 или 2.
	 * Обязательный параметр.
	 */
	t: KareGameType;

	/**
	 * Сумма ставки (для типа 2 стоимость каждой из выбранных комбинаций) в копейках.
	 * Обязательный параметр.
	 */
	s: number;

	/**
	 * Количество тиражей (опционально), в которых принимает участие ставка, по умолчанию: 1.
	 */
	d: number;

}

/**
 * Модель типа ставки 1.
 */
export interface IKareBetItemGameType1 extends IKareBetItem {

	/**
	 * Признак автогенерации ставки (опционально).
	 * Если true, то для ставки сервер сам генерирует 5 карт случайным образом.
	 * По умолчанию: false
	 */
	a: boolean;

	/**
	 * Массив кодов карт (обязательно, если a == false), количество карт - от 1 до 5.
	 * Если установлен признак автогенерации, то этот параметр игнорируется.
	 * Коды карт соответствуют таблице кодов карт ниже.
	 */
	c?: Array<number>;

}

/**
 * Модель типа ставки 2.
 */
export interface IKareBetItemGameType2 extends IKareBetItem {

	/**
	 * Массив кодов покерных комбинаций.
	 * Коды комбинаций указаны в таблице кодов комбинаций.
	 */
	p?: Array<KarePokerCombinations>;

}

/**
 * Комбинированная модель ставки в игре "Каре".
 * Содержит массив {@link bets} элементов типа 1 или 2.
 */
export interface IKareBetsData {
	/**
	 * Массив ставок в игре "Каре".
	 */
	bets: Array<IKareBetItemGameType1 | IKareBetItemGameType2>;
}

/**
 * Модель для вызова API KareRegBetReq.
 */
export class KareRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизовано е хранилище данных.
	 * @param {string} amount Сумма ставки в грн.
	 * @param {IKareBetDataItem} betData Данные по текущей ставке в игре.
	 */
	constructor(appStoreService: AppStoreService, amount: number, betData?: IKareBetDataItem) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Kare, EsapActions.KareRegBet), betData);

		this.gameCode = LotteryGameCode.Kare;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.AMOUNT, `${amount * 100}`);
		this.params.append(EsapParams.BETS, JSON.stringify(betData.kareBetsData));
	}

}

// -----------------------------
//  Response
// -----------------------------
/**
 * Ставка в билете
 */
export class TicketBet {

	/**
	 * Тип ставки (соответствует номеру стола) - число, может принимать значение 1 или 2.
	 */
	type: number;

	/**
	 * Сумма ставки, строка в формате "123.00" (гривни, копейки, разделитель точка).
	 */
	sum: string;

	/**
	 * Для типа ставки 1 - массив карт.
	 */
	card: {code: number, name: string};

	/**
	 * Для типа ставки 2 - массив выбранных комбинаций.
	 */
	comb: {code: number, name: string};

}

/**
 * Смотри описание API
 */
export class Ticket implements ITransactionTicket {

	/**
	 * Уникальный идентификатор чека для его отмены по 39 команде.
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Человекопонятное название документа (например, "Чек Каре N3434 на тираж 1234").
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Сумма билета (сумма всех ставок в билете), строка в формате "123.00" (гривни, копейки, разделитель точка).
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Идентификатор порции документов при регистрации с легких терминалов (опционально).
	 */
	portion_id: string;

	/**
	 * Маккод (баркод) билета, как строка десятичных цифр.
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Номер билета.
	 */
	@IsNotEmpty()
	ticket_num: number;

	/**
	 * Название тиража.
	 */
	@IsNotEmpty()
	draw_name: string;

	/**
	 * Дата и время розыгрыша тиража.
	 */
	@IsNotEmpty()
	@IsDateFormat()
	draw_date: string;

	/**
	 * Количество ставок.
	 */
	@IsNotEmpty()
	bet_count: number;

	/**
	 * Массив ставок.
	 */
	bet: Array<TicketBet>;

	/**
	 * Дата и время регистрации билета.
	 */
	@IsNotEmpty()
	@IsDateFormat()
	regdate: string;

	/**
	 * Слоган билета.
	 */
	slogan: string;

	/**
	 * Код терминала, с которого продан билет
	 */
	term_code: number;

	/**
	 * Ссылка на шаблон билета.
	 */
	ticket_templ_url?: string;

}

/**
 * Модель ответа на запрос API KareRegBet.
 */
export class KareRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции
	 */
	client_trans_id: string;

	/**
	 * Код ошибки
	 */
	err_code: number;

	/**
	 * Описание ошибки
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: string;

	/**
	 * Идентификатор сессии
	 */
	sid: string;

	/**
	 * Дата и время транзакции
	 */
	time_stamp: string;

	/**
	 * Массив билетов
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
