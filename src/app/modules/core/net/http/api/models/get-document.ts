import { ApiClient } from '@app/core/net/http/api/api-client';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { IResponse } from '@app/core/net/http/api/types';

/**
 * Интерфейс полученного документа
 */
export interface DocObject {
	/**
	 * Ссылка на шаблон документа
	 */
	doc_templ_url: string;
	/**
	 * Данные документа
	 */
	doc_data: any;
}

/**
 * Модель запроса для вызова API GetDocument.
 */
export class GetDocumentReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param documentId Идентификатор запрашиваемого документа
	 * @param documentParams Параметры документа. Опциональный параметр. Для каждого типа документа набор параметров может отличаться.
	 * @param {string} multiUserID Идентификатор пользователя, который обнаружен на пользовательском компьютере.
	 * Используется для отслеживания нескольких аккаунтов от одного и того же пользователя
	 */
	constructor(
		appStoreService: AppStoreService,
		documentId: number,
		documentParams?: any,
		multiUserID?: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetDocument));

		this.params.append(EsapParams.DOCUMENT_ID, documentId.toString());
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);

		if (documentParams) {
			this.params.append(EsapParams.DOCUMENT_PARAMS, JSON.stringify(documentParams));
		}

		if (multiUserID) {
			this.params.append(EsapParams.MULTIUSER_ID, multiUserID);
		}
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetDocument
 */
export interface GetDocumentResp extends IResponse {

	/**
	 * Идентификатор клиента
	 */
	client_id?: number;

	/**
	 * Данные отправляемого документа. Для каждого типа документа формат данных может быть определен свой.
	 */
	doc: DocObject;
}
