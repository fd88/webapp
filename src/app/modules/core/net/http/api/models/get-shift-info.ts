import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApiClient } from '../api-client';
import { IResponse } from '../types';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API GetShiftInfo
 */
export class GetShiftInfo extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService централизованое хранилище данных
	 * @param {string} actionName Имя экшена для данного отчета.
	 */
	constructor(
		appStoreService: AppStoreService,
		actionName = EsapActions.GetShiftInfo
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, actionName));

		if (appStoreService.hasOperator()) {
			this.params.append(EsapParams.USER_ID, appStoreService.operator.getValue().userId);
			this.params.append(EsapParams.SID, appStoreService.operator.getValue().sessionId);
		}

	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetShiftInfoResp
 */
export class GetShiftInfoResp implements IResponse {

	/** Идентификатор клиента */
	client_id: string;

	/** Идентификатор транзакции */
	client_trans_id: string;

	/** Результат операции */
	err_code: number;

	/** Текст ошибки */
	err_descr: string;

	/** Код точки продажи, к которой привязан терминал, с которого пришел запрос */
	current_pos_code: string;

	/** Статус текущей смены */
	shift_status: string;

	/** Код точки продажи, к которой привязана текущая смена */
	pos_code: string;

	/** Название точки продажи, к которой привязана текущая смена */
	pos_name: string;

	/** Дата время начала текущей смены */
	start_date: string;

	/** Дата время окончания текущей смены */
	end_date: string;

	/** Текущий баланс по смене (в копейках) */
	amount: number;

	/** Непогашенный долг по предыдущей смене (в копейках) */
	debt: number;

	/** Предельная дата погашения долга */
	debt_pay_expire: string;
}
