import { IsNotEmpty } from 'class-validator';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApiClient } from '@app/core/net/http/api/api-client';
import { IResponse } from '@app/core/net/http/api/types';
import {AdditionalInfo, IWinTicketsDetails} from '@app/tickets/interfaces/win-check-data';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API {@link EsapActions.WinCheck} для обычных лотерей.
 */
export class WinCheckReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {string} barcode Мак-код билета.
	 * @param {string} lang Язык для получения инфы с бекенда.
	 */
	constructor(
		appStoreService: AppStoreService,
		barcode: string,
		lang?: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(barcode, EsapActions.WinCheck));
		this.params.append(EsapParams.BARCODE, barcode);
		this.params.set(EsapParams.LANG, lang || appStoreService.Settings.lang);
		// параметры пока только для продавца
		this.params.append(EsapParams.ACCEPT_ADDITIONAL_INFO, 'seller_notice,seller_hint');
		// this.params.append(EsapParams.GAME_CODE, '10001');//// Джим просил добавить, но пока работает без костыля
	}
}

/**
 * Модель для вызова API {@link EsapActions.WinCheck} для лотерей типа БМЛ/ТМЛ.
 */
export class WinCheckPaperReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param appStoreService Централизованное хранилище данных.
	 * @param barcode Мак-код билета.
	 * @param winBarCode Проверочный код.
	 * @param {string} lang Язык для получения инфы с бекенда.
	 */
	constructor(appStoreService: AppStoreService, barcode: string, winBarCode: string, lang?: string) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.TML_BML, EsapActions.WinCheck));
		this.params.append(EsapParams.BARCODE, barcode);
		this.params.append(EsapParams.WINBARCODE, winBarCode);
		this.params.set(EsapParams.LANG, lang || appStoreService.Settings.lang);
		// параметры пока только для продавца
		this.params.append(EsapParams.ACCEPT_ADDITIONAL_INFO, 'seller_notice,seller_hint');
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Типы дополнительных выигрышей.
 */
export enum WinBonusType {
	Bonus = 'bonus'
}

/**
 * Типы авторизации.
 */
export enum PlayerAuth {
	SMS = 'sms'
}

/**
 * Интерфейс дополнительных выигрышей.
 */
export interface IExtraWin {
	/**
	 * Идентификатор выигрыша.
	 */
	id: string;				// "195"
	/**
	 * Выплачивать ли выигрыш сразу.
	 */
	pay_instantly: string;	// "1"

	/**
	 * Тип выигрыша.
	 */
	type: WinBonusType;		// "bonus"

	/**
	 * Разрешена ли выплата выигрыша.
	 */
	win_allow: string;		// "1"

	/**
	 * Сумма выигрыша.
	 */
	win_sum: string;		// "10000"

	/**
	 * Примечание к выигрышу.
	 */
	win_summary: string;	// "Подарунок - лотерейні білети на сумму"
}

/**
 * Модель ответа на запрос API {@link EsapActions.WinCheck} для обычной лотереи.
 */
export class WinCheckResp implements IResponse {
	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;
	/**
	 * Код ошибки.
	 */
	err_code: number;
	/**
	 * Описание ошибки.
	 */
	err_descr: string;
	/**
	 * Идентификатор пользователя.
	 */
	user_id: string;

	/**
	 * Идентификатор сессии.
	 */
	sid: string;

	/**
	 * Сумма выигрыша по билету.
	 */
	@IsNotEmpty()
	win_ticket_sum: number;

	/**
	 * Сумма выигрыша, доступная к выплате (с учетом всех налоговых сборов).
	 */
	@IsNotEmpty()
	win_sum: number;

	/**
	 * Признак разрешения оплаты.
	 * - 1 - оплата разрешена
	 * - 0 – оплата запрещена
	 */
	@IsNotEmpty()
	win_allow: number;

	/**
	 * Причина запрета выплаты выигрыша.
	 */
	reason_descr: string;

	/**
	 * Опциональная часть с описанием дополнительных выигрышей по билету.
	 */
	extra_win: Array<IExtraWin>;

	/**
	 * Опциональная часть с дополнительной информцией.
	 */
	additional_info?: AdditionalInfo;

	/**
	 * Признак необходимости дополнительной проверки при выплате выиграша с помощью отправки проверочного кода через СМС.
	 */
	player_auth?: PlayerAuth;

	/**
	 * Телефон игрока.
	 */
	player_phone?: string;

	/**
	 * Идентификатор документа в случае большого выигрыша (1 или 2)
	 */
	document_id?: number;

	/**
	 * Детализация выигрыша по билетам. Присутствует в ответах проверки выигрыша по бланкам,
	 * которым может соответствовать несколько выигрышных билетов. К таким бланкам относятся
	 * - типографские бланки "Лото Забава"
	 */
	win_tickets_details?: Array<IWinTicketsDetails>;
}

/**
 * Модель ответа на запрос API {@link EsapActions.WinCheck} для БМЛ/ТМЛ.
 */
export class WinCheckPaperResp extends WinCheckResp {
	/**
	 * Код игры.
	 */
	game_code: number;
}
