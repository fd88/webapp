import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ApiClient } from '@app/core/net/http/api/api-client';
import { TerminalRoles } from '@app/core/services/store/operator';
import { AccessPosListItemGS, Auth2Type, BOOperations, IResponse } from '@app/core/net/http/api/types';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель запроса для вызова API BOAuthenticate.
 */
export class BOAuthenticateReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизовано е хранилище данных.
	 * @param {string} login логин
	 * @param {string} pass пароль
	 */
	constructor(
		appStoreService: AppStoreService,
		login: string,
		pass: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.BOAuthenticate));

		this.params.append(EsapParams.LOGIN, login);
		this.params.append(EsapParams.PASSWD, pass);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API BOAuthenticate
 */
export interface BOAuthenticateResp extends IResponse {

	/**
	 * Идентификатор пользователя
	 */
	user_id: string;

	/**
	 * Идентификатор созданной сессии
	 */
	sid: string;

	/**
	 * Уровень доступа пользователя:
	 * 0 - продавец
	 * 1 - менеджер
	 * 2 - инженер
	 * 3 - кассир
	 * 6 - оператор "базового терминала" Лото-Забава
	 * 10 - продавец, смена которого заблокирована
	 * 13 - специальный уровень доступа, определяющий требование сменить пароль (вся остальная функциональность для оператора заблокирована)
	 */
	access_level: TerminalRoles;

	/**
	 * Указывает на способ и необходимость второй фазы аутентификации (для двухфакторной аутентификации).
	 * Возможные значения:
	 * sms - указывает на то, что система требует пройти аутентификацию через указание полученного SMS-кода.
	 * Сервис на телефон пользователя отправит 4-значный SMS-код для аутентификации'
	 */
	auth2?: Auth2Type;

	/**
	 * Массив терминалов и точек продаж
	 */
	pos_list?: Array<AccessPosListItemGS>;

	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	/**
	 * Массив операций, доступных для пользователя
	 */
	operations?: Array<BOOperations>;
}
