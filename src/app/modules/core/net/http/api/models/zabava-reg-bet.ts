import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IZabavaBetDataItem } from '@app/zabava/interfaces/izabava-bet-data-item';

// -----------------------------
//  Request
// -----------------------------

/**
 * Абстрактная модель ставки.
 * Содержит параметры: {@link IZabavaBets.t t}, {@link IZabavaBets.f f}
 */
export interface IZabavaBets {
	/**
	 * Количество билетов.
	 * {@link IZabavaBets}
	 */
	t: number;

	/**
	 * Количество игровых полей.
	 * {@link IZabavaBets}
	 */
	f: number;
}

/**
 * Модель дополнительной комбинации.
 * Содержит параметры: {@link IZabavaAdditionalCombinations.c c}, {@link IZabavaAdditionalCombinations.n n}
 */
export interface IZabavaAdditionalCombinations {
	/**
	 * Код дополнительной игры.
	 * {@link IZabavaAdditionalCombinations}
	 */
	c: number;

	/**
	 * Количество комбинаций.
	 * {@link IZabavaAdditionalCombinations}
	 */
	n: number;
}

/**
 * Интерфейс упрощенной ставки.
 * Содержит параметры: {@link IZabavaBetsSimple.a a} в дополнение к базовому интерфейсу {@link IZabavaBets}.
 */
export interface IZabavaBetsSimple extends IZabavaBets {
	/**
	 * Количество игровых комбинаций.
	 * {@link IZabavaBetsSimple}
	 */
	a: number;
}

/**
 * Альтернативный вариант ставки.
 * Содержит параметры: {@link IZabavaBetsExtend.e e} в дополнение к базовому интерфейсу {@link IZabavaBets}.
 */
export interface IZabavaBetsExtend extends IZabavaBets {
	/**
	 * Список объектов, каждый из которых описывает дополнительные комбинации.
	 * {@link IZabavaBetsExtend}
	 */
	e: Array<IZabavaAdditionalCombinations>;
}

/**
 * Для SALE_TYPE=1 параметр BETS_DATA, имеет следующий формат:
 * {@link IZabavaBetsSimple}: {“tickets“:[{“t“:n,“f“:m, “a“:k},…]}
 * или
 * {@link IZabavaBetsExtend}: {“tickets“:[{“t“:n,“f“:m, "e":[{"c":x,"n":y},...]},…]}
 * где bets - содержит массив требований к конфигурации запрашиваемых билетов.
 * Каждый элемент массива представляется структурой {t:n,f:m, ...}, где:
 * - t содержит количество билетов n
 * - f содержит количество игровых полей m
 * - a содержит количество k игровых комбинаций для дополнительной игры (напр., комбинации «Щасливый номер»)
 * - e альтернативный вариант запроса дополнительных комбинаций.
 * Позволяет указывать несколько разных типов дополнительных игр, если это поддерживается системой.
 * Содержит массив объектов, каждый из которых описывает доп. комбинации и включает поля:
 * <br>
 * - c - код дополнительной игры
 * - n - количество комбинаций
 * <br>
 * Пример для запроса 1 билета с 3 игровыми полями и 2 билетов с 2 игровыми полями:
 * BETS_DATA={“tickets“: [{“t“:1, “f“:3, “a“: 1}, {“t“:2, “f“:2, “a“: 0}]}
 */
export interface ZabavaBetsData {
	/**
	 * Массив билетов.
	 */
	tickets: Array<IZabavaBetsSimple> | Array<IZabavaBetsExtend>;
}

/**
 * Модель для вызова запроса API {@link EsapParams.ZabavaRegBet ZabavaRegBet}.
 * В качестве параметра ставки используется модель {@link ZabavaBetsData}.
 */
export class ZabavaRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {number} amount  Стоимость ставок в гривнах.
	 * @param {IZabavaBetDataItem} betData Параметры ставки.
	 * @param {ZabavaBetsData} zabavaBetsData
	 * @param {string} drawNum Номер тиража.
	 * @param {string} macCode Опциональное поле для регистрации билетов на основании полиграфских бланков. Список маккодов бланков в формате '023498653212;023498653212;023498653212'
	 */
	constructor(
		appStoreService: AppStoreService,
		amount: number,
		betData: IZabavaBetDataItem,
		zabavaBetsData: ZabavaBetsData,
		drawNum: string,
		macCode?: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Zabava, EsapActions.ZabavaRegBet), betData);

		this.gameCode = LotteryGameCode.Zabava;
		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.BET_SUM, `${amount * 100}`);
		this.params.append(EsapParams.BETS_COUNT, `${betData.betsCount}`);
		this.params.append(EsapParams.BETS_DATA, JSON.stringify(zabavaBetsData));
		this.params.append(EsapParams.DRAW_NUM, drawNum);
		this.params.append(EsapParams.DRAW_COUNT, `${betData.drawCount}`);
		this.params.append(EsapParams.SALE_TYPE, '1');
		if (!!macCode) {
			this.params.append(EsapParams.MACCODE, macCode);
		}
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Смотри описание API
 */
export class ZabavaAddGame {
	/**
	 * Комбинация
	 */
	comb: string;

	/**
	 * Название дополнительной игры
	 */
	name: string;

	/**
	 * Код дополнительной игры
	 */
	code: number;
}

/**
 * Смотри описание API
 */
export class ZabavaGameComb {
	/**
	 * Комбинация на игровом поле
	 */
	game_field: string;
}

/**
 * Смотри описание API
 */
export class ZabavaGameAction {
	/**
	 * Код действия
	 */
	code: string;
}

/**
 * Смотри описание API.
 */
export class Ticket implements ITransactionTicket {
	/**
	 * Идентификатор порции билетов
	 */
	portion_id: number;

	/**
	 * Массив игровых действий
	 */
	action: Array<ZabavaGameAction>;

	/**
	 * Массив дополнительных игр
	 */
	addgame: Array<ZabavaAddGame>;

	/**
	 * Идентификатор билета
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Штрих-код билета
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Дата регистрации билета
	 */
	@IsNotEmpty()
	@IsDateFormat()
	regdate: string;

	/**
	 * Описание билета
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Сумма ставки
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Количество ставок
	 */
	@IsNotEmpty()
	bet_count: number;

	/**
	 * Дата тиража
	 */
	@IsNotEmpty()
	@IsDateFormat()
	draw_date: string;

	/**
	 * Номер тиража
	 */
	@IsNotEmpty()
	draw_num: string;

	/**
	 * Номер билета
	 */
	@IsNotEmpty()
	ticket_num: number;

	/**
	 * Номер билета в порции
	 */
	@IsNotEmpty()
	ticket_snum: number;

	/**
	 * Массив игровых комбинаций
	 */
	@Type(() => ZabavaGameComb)
	@ValidateNested()
	game_comb: Array<ZabavaGameComb>;
}

/**
 * Модель ответа на запрос API ZabavaRegBet.
 */
export class ZabavaRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции
	 */
	client_trans_id: string;

	/**
	 * Код ошибки
	 */
	err_code: number;

	/**
	 * Описание ошибки
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: string;

	/**
	 * Идентификатор сессии
	 */
	sid: string;

	/**
	 * Время ответа от сервера
	 */
	time_stamp: string;

	/**
	 * Идентификатор клиента
	 */
	client_id: string;

	/**
	 * Слоган билета
	 */
	slogan: string;

	/**
	 * Ссылка на шаблон билета
	 */
	@IsNotEmpty()
	ticket_templ_url: string;

	/**
	 * Массив билетов
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
