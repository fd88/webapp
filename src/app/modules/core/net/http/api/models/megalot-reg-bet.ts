import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IMegalotBetDataItem } from '@app/megalot/interfaces/imegalot-bet-data-item';

// -----------------------------
//  Request
// -----------------------------

/**
 * Интерфейс модели одной ставки.
 * Свойства модели: {@link balls}, {@link megaballs}, {@link input_mode}.
 * Пример ставок в формате JSON:
 * BETS_DATA=[
 *   {"balls": [1,2,9,11,42], "megaballs": [0,3], "input_mode": 2},
 *   {"balls": [5,8,9,11,41], "megaballs": [7], "input_mode": 2}
 * ]
 */
export interface IMegalotBetsBalls {
	/**
	 * Массив шаров основной ставки.
	 * {@link IMegalotBetsBalls}
	 */
	balls: Array<number>;

	/**
	 * Массив номеров мегакулек.
	 * {@link IMegalotBetsBalls}
	 */
	megaballs: Array<number>;

	/**
	 * Режим ввода ставки, возможные значения:
	 * - 0 - ставка с купона
	 * - 1 - с купона с автозаполнением
	 * - 2 - ручной ввод
	 * - 3 - QuickPick (автогенерация по кнопке из GUI)
	 * {@link IMegalotBetsBalls}
	 */
	input_mode: number;
}

/**
 * Модель для вызова API MegalotRegBet.
 */
export class MegalotRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {string} amount Сумма ставки в копейках.
	 * @param {IMegalotBetDataItem} betData Данные по ставке.
	 */
	constructor(appStoreService: AppStoreService, amount: string, betData: IMegalotBetDataItem) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.MegaLot, EsapActions.MegalotRegBet), betData);

		this.gameCode = LotteryGameCode.MegaLot;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.AMOUNT, amount);
		this.params.append(EsapParams.BETS_COUNT, `${betData.bets.length}`);
		this.params.append(EsapParams.BETS_DATA, JSON.stringify(betData.bets));
		this.params.append(EsapParams.DRAW_COUNT, `${betData.drawCount}`);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ставки в игре MegaLot.
 */
export class Bet {
	/**
	 * Строка с номерами шариков в диапазоне 1-42, разделенных запятыми
	 */
	@IsNotEmpty()
	balls: string;

	/**
	 * Строка с номерами мегакулек в диапазоне 0-9, разделенных запятыми
	 */
	@IsNotEmpty()
	megaballs: string;

	/**
	 * Режим ввода данных ставки, возможные значения:
	 * - 0 - ставка с купона
	 * - 1 - с купона с автозаполнением
	 * - 2 - ручной ввод
	 * - 3 - QuickPick (автогенерация по кнопке из GUI)
	 */
	@IsNotEmpty()
	input_mode: string;
}

/**
 * Данные лотерейных билетов
 */
export class Ticket implements ITransactionTicket {
	/**
	 * Идентификатор билета
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Маккод (баркод) билета, как строка десятичных цифр
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Название точки продажи
	 */
	@IsNotEmpty()
	pos_name: string;

	/**
	 * Дата и время регистрации билета
	 */
	@IsNotEmpty()
	@IsDateFormat()
	regdate: string;

	/**
	 * Человекопонятное название документа (например, "Чек Мегалот N3434 на тираж 1234")
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Сумма билета (сумма всех ставок в билете), строка в формате "123.00" (гривны, копейки, разделитель точка)
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Количество ставок в билете
	 */
	@IsNotEmpty()
	bet_count: number;

	/**
	 * Дата и время розыгрыша тиража
	 */
	@IsNotEmpty()
	@IsDateFormat()
	draw_date: string;

	/**
	 * Номер тиража
	 */
	@IsNotEmpty()
	draw_num: string;

	/**
	 * Название игры
	 */
	@IsNotEmpty()
	game_name: string;

	/**
	 * Номер билета
	 */
	@IsNotEmpty()
	ticket_num: number;

	/**
	 * Стоимость билета (сумма всех ставок в билете) в копейках
	 */
	@IsNotEmpty()
	ticket_sum: string;

	/**
	 * Ссылка на шаблон билета
	 */
	@IsNotEmpty()
	ticket_templ_url: string;

	/**
	 * Массив ставок в билете
	 */
	@Type(() => Bet)
	@ValidateNested()
	bet: Array<Bet>;
}

/**
 * Модель ответа на запрос API MegalotRegBet
 */
export class MegalotRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции. Значение этого параметра соответствует тому, что было в запросе.
	 * Если в запросе параметр CLIENT_TRANS_ID отсутствует, то в ответе его тоже не будет.
	 */
	client_trans_id: string;

	/**
	 * Код ошибки. Если ошибки нет, то значение равно 0.
	 */
	err_code: number;

	/**
	 * Описание ошибки. Текст ошибки возвращается на том языке, который был указан в запросе в параметре LANG
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: string;

	/**
	 * Идентификатор сессии
	 */
	sid: string;

	/**
	 * Дата и время регистрации транзакции
	 */
	time_stamp: string;

	/**
	 * Данные лотерейных билетов
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
