import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IBetDataItem } from '@app/core/game/base-game.service';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API TipTopRegBet для лотереи Tip
 */
export class TipRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор класса
	 *
	 * @param {AppStoreService} appStoreService централизованное хранилище данных
	 * @param {IBetDataItem} betData Данные ставки
	 * @param {string} inputMode режим ввода
	 * @param {string} betsCount кол-то ставок
	 * @param {string} drawCount кол-во тиражей
	 * @param {string} isUnique смотри описание API
	 * @param {string} isSingle смотри описание API
	 */
	constructor(
		appStoreService: AppStoreService,
		betData: IBetDataItem,
		inputMode: string,
		betsCount: string,
		drawCount: string,
		isUnique: boolean,
		isSingle: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Tip, EsapActions.TipTopRegBet), betData);

		this.gameCode = LotteryGameCode.Tip;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.INPUT_MODE, inputMode);
		this.params.append(EsapParams.TIP_DRAW_COUNT, drawCount);
		this.params.append(EsapParams.TIP_BETS_COUNT, betsCount);
		this.params.append(EsapParams.TIP_IS_UNIQUE, isUnique ? '1' : '0');
		this.params.append(EsapParams.TIP_SINGLE_TICKET, isSingle);
	}

	/**
	 * Добавляет параметры в запрос
	 * @param request Запрос
	 * @param betsCount	Кол-во ставок
	 * @param drawCount Кол-во тиражей
	 * @param isUnique Признак того, что комбинации в каждом тираже должны быть одинаковые (ТипТоп)
	 * @param isSingle Признак того, что запрашивается единый мультитиражный чек (ТипТоп)
	 */
	static appendParam(request: CancelableApiClient, betsCount: string, drawCount: string, isUnique: boolean, isSingle: string): void {
		if (request) {
			request.params.append(EsapParams.TIP_DRAW_COUNT, drawCount);
			request.params.append(EsapParams.TIP_BETS_COUNT, betsCount);
			request.params.append(EsapParams.TIP_IS_UNIQUE, isUnique ? '1' : '0');
			request.params.append(EsapParams.TIP_SINGLE_TICKET, isSingle);
		}
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Смотри описание API
 */
export class Bet {

	/**
	 * Номер тиража
	 */
	@IsNotEmpty()
	number: string;

	/**
	 * Выигрышная комбинация
	 */
	@IsNotEmpty()
	comb: string;
}

/**
 * Смотри описание API
 */
export class Ticket implements ITransactionTicket {
	/**
	 * Номер билета
	 */
	@IsNotEmpty()
	ticket_num: number;

	/**
	 * Номер первого тиража
	 */
	@IsNotEmpty()
	first_draw_num: string;

	/**
	 * Дата первого тиража
	 */
	@IsNotEmpty()
	@IsDateFormat()
	first_draw_date: string;

	/**
	 * Номер последнего тиража
	 */
	last_draw_num: string;

	/**
	 * Дата последнего тиража
	 */
	@IsDateFormat()
	last_draw_date: string;

	/**
	 * Кол-во тиражей
	 */
	@IsNotEmpty()
	draw_count: number;

	/**
	 * Кол-во ставок
	 */
	@IsNotEmpty()
	bet_count: number;

	/**
	 * Сумма ставки
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Дата покупки билета
	 */
	@IsNotEmpty()
	@IsDateFormat()
	regdate: string;

	/**
	 * Мак-код билета
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Подпись билета
	 */
	@IsNotEmpty()
	mac_code_sign: string;

	/**
	 * Идентификатор билета
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Идентификатор порции билетов
	 */
	@IsNotEmpty()
	portion_id: string;

	/**
	 * Описание билета
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Ссылка на шаблон билета
	 */
	@IsNotEmpty()
	ticket_templ_url: string;

	/**
	 * Слоган в билете
	 */
	@IsNotEmpty()
	slogan: string;

	/**
	 * Ставка в билете
	 */
	@Type(() => Bet)
	@ValidateNested()
	bet: Array<Bet>;
}

/**
 * Модель ответа на запрос API TipTopRegBet
 */
export class TipTopRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции
	 */
	client_trans_id: string;

	/**
	 * Код ошибки
	 */
	err_code: number;

	/**
	 * Описание ошибки
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: string;

	/**
	 * Идентификатор сессии
	 */
	sid: string;

	/**
	 * Время ответа
	 */
	time_stamp: string;

	/**
	 * Идентификатор клиентcкого приложения
	 */
	client_id: string;

	/**
	 * Билеты в запросе регистрации ставки
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
