import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API WithdrawBonus
 */
export class WithdrawBonusReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param cardNum Номер карты лояльности
	 * @param bonusWithdrawSum
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNum: string,
		bonusWithdrawSum: number
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.WithdrawBonus, true));

		this.params.append(EsapParams.CARD_NUMBER, cardNum);
		this.params.append(EsapParams.OPER_CODE, appStoreService.operator.value.operCode);
		this.params.append(EsapParams.BONUS_WITHDRAW_SUM, bonusWithdrawSum.toString());
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		this.params.remove(EsapParams.PROTO_TYPE);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API WithdrawBonus
 */
export interface WithdrawBonusResp extends IResponse {

}
