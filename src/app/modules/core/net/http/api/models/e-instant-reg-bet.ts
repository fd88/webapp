import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IInstantBetDataItem } from '@app/instant/interfaces/iinstant-bet-data-item';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова API EInstantRegBet
 */
export class EInstantRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор.
	 *
	 * @param {AppStoreService} appStoreService централизованное хранилище данных
	 * @param {LotteryGameCode} gameCode код лотереи
	 * @param {string} betsCount кол-во ставок
	 * @param {string} drawCode код тиража
	 * @param {IInstantBetDataItem} betData данные ставки
	 */
	constructor(
		appStoreService: AppStoreService,
		gameCode: LotteryGameCode,
		betsCount: number,
		drawCode: string,
		betData: IInstantBetDataItem
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(gameCode, EsapActions.EInstantRegBet), betData);

		this.gameCode = gameCode;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.DRAW_CODE, drawCode);
		this.params.append(EsapParams.TICKETS_COUNT, `${betsCount}`);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Смотри описание API
 */
export class EInstantIWfield {
	/**
	 * Фрагмент картинки билета
	 */
	img: string;

	/**
	 * Текстовый фрагмент билета
	 */
	sign: string;
}

/**
 * Смотри описание API
 */
export class EInstantIGfield {
	/**
	 * Фрагмент картинки билета
	 */
	img: string;

	/**
	 * Текстовый фрагмент билета
	 */
	sign: string;
}

/**
 * Интерфейс участия в интерактивном розыгрыше
 */
export class EInstantInteractive {
	/**
	 * Код участия в интерактивном розыгрыше
	 */
	code: string;
}

/**
 * Данные лотерейных билетов
 */
export class Ticket implements ITransactionTicket {
	/**
	 * Идентификатор порции документов при регистрации с легких терминалов
	 */
	portion_id: number;

	/**
	 * Тег, в котором передается дата и время розыгрыша (в случае, если по лотерее проводятся интерактивные розыгрыши)
	 */
	next_game: string;

	/**
	 * Слоган билета
	 */
	slogan: string;

	/**
	 * Опциональный параметр, определяющий участие билета в интерактивном розыгрыше.
	 */
	interactive?: EInstantInteractive;

	/**
	 * Уникальный идентификатор билета (чека). Используется для ссылки на билет в случае необходимости его отмены
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Название игры
	 */
	@IsNotEmpty()
	game_name: string;

	/**
	 * Сумма ставки
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Дата регистрации билета
	 */
	@IsNotEmpty()
	@IsDateFormat()
	date_reg: string;

	/**
	 * Описание билета
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Штрих-код билета
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Название серии билета
	 */
	@IsNotEmpty()
	series_name: string;

	/**
	 * Номер серии билета
	 */
	@IsNotEmpty()
	series_num: number;

	/**
	 * Номер билета
	 */
	@IsNotEmpty()
	ticket_num: string;

	/**
	 * Количество игровых полей с игровой комбинацией
	 */
	@IsNotEmpty()
	gfield_cnt: number;

	/**
	 * Массив объектов, содержащих данные игровых полей. Игровые поля могут содержать в себе коды печатаемых картинок и/или текст.
	 */
	@Type(() => EInstantIGfield)
	@ValidateNested()
	gfield: Array<EInstantIGfield>;

	/**
	 * Количество игровых полей с выигрышной комбинацией
	 */
	@IsNotEmpty()
	wfield_cnt: number;

	/**
	 * Массив объектов, содержащих игровые поля с выпавшей игровой комбинацией. Игровые поля могут содержать в себе коды печатаемых картинок и/или текст.
	 */
	@Type(() => EInstantIWfield)
	@ValidateNested()
	wfield: Array<EInstantIWfield>;
}

/**
 * Модель ответа на запрос API EInstantRegBet
 */
export class EInstantRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции
	 */
	client_trans_id: string;

	/**
	 * Код ошибки
	 */
	err_code: number;

	/**
	 * Описание ошибки
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя
	 */
	user_id: string;

	/**
	 * Идентификатор сессии
	 */
	sid: string;

	/**
	 * Время ответа от сервера
	 */
	time_stamp: string;

	/**
	 * Идентификатор клиента
	 */
	client_id: string;

	/**
	 * Слоган
	 */
	slogan: string;

	/**
	 * Ссылка на шаблон билета
	 */
	@IsNotEmpty()
	ticket_templ_url: string;

	/**
	 * Массив объектов, содержащих данные билетов
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
