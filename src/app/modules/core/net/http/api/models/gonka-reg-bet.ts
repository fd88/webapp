import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IsDateFormat } from '@app/util/decorators';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { IGonkaBetDataItem } from '@app/gonka/interfaces/igonka-bet-data-item';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель ставки для лотереи "Гонка на гроши".
 */
export interface IGonkaBet {

	/**
	 * Массив победителей (1..10) в заездах (1..4). Т.е. это массив из 4х элементов.
	 */
	R: Array<Array<number>>;

	/**
	 * Множитель ставки. Например, для суммы ставки 4грн он равен 2.
	 */
	S: number;

	/**
	 * Тип игры (1..5).
	 */
	T: number;

	/**
	 * Массив победителей в заездах.
	 * Массив до 4х элементов.
	 */
	RN?: Array<number>;

}

/**
 * Модель для вызова API GonkaRegBet.
 */
export class GonkaRegBetReq extends CancelableApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {number} betsCount Кол-во ставок.
	 * @param {Array<IGonkaBet>} betsList Список ставок.
	 * @param {number} drawCount Кол-во тиражей.
	 * @param {IGonkaBetDataItem} betData Данные ставки.
	 */
	constructor(
		appStoreService: AppStoreService,
		betsCount: number,
		betsList: Array<IGonkaBet>,
		drawCount: number,
		betData: IGonkaBetDataItem
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Gonka, EsapActions.GonkaRegBet), betData);

		const betsData = {bets: betsList};
		this.gameCode = LotteryGameCode.Gonka;

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.BETS_COUNT, `${betsCount}`);
		this.params.append(EsapParams.BETS_DATA, JSON.stringify(betsData));
		this.params.append(EsapParams.DRAW_COUNT, `${drawCount}`);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Смотри описание API
 */
export class Bet {
	/**
	 * Неиспользуемое поле в игре "Гонка на деньги".
	 */
	@IsNotEmpty()
	balls: string;

	// @IsNotEmpty()
	// count: number;
	/**
	 * Тип ставки.
	 */
	@IsNotEmpty()
	type: string;
}

/**
 * Смотри описание API
 */
export class Ticket implements ITransactionTicket {
	/**
	 * Данные ставки.
	 */
	@Type(() => Bet)
	@ValidateNested()
	bet: Array<Bet>;

	/**
	 * Сумма ставки.
	 */
	@IsNotEmpty()
	bet_cost: number;

	/**
	 * Количество ставок.
	 */
	@IsNotEmpty()
	bet_count: number;

	/**
	 * Сумма ставки в строковом формате.
	 */
	@IsNotEmpty()
	bet_sum: string;

	/**
	 * Описание билета.
	 */
	@IsNotEmpty()
	description: string;

	/**
	 * Дата и время розыгрыша тиража, как строка формата "dd.mm.yyyy hh:mi".
	 */
	@IsNotEmpty()
	@IsDateFormat()
	draw_date: string;

	/**
	 * Номер тиража.
	 */
	@IsNotEmpty()
	draw_num: string;

	/**
	 * Идентификатор билета.
	 */
	@IsNotEmpty()
	id: string;

	/**
	 * Маккод (26-символьный баркод) билета, как строка десятичных цифр.
	 */
	@IsNotEmpty()
	mac_code: string;

	/**
	 * Дата и время регистрации билета.
	 */
	@IsNotEmpty()
	@IsDateFormat()
	regdate: string;

	/**
	 * Номер чека (билета).
	 */
	@IsNotEmpty()
	ticket_num: number;

	/**
	 * Ссылка на шаблон билета.
	 */
	@IsNotEmpty()
	ticket_templ_url: string;
}

/**
 * Модель ответа на запрос API GonkaRegBet.
 */
export class GonkaRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;

	/**
	 * Код ошибки.
	 */
	err_code: number;

	/**
	 * Описание ошибки.
	 */
	err_descr: string;

	/**
	 * Идентификатор пользователя.
	 */
	user_id: string;

	/**
	 * Идентификатор сессии.
	 */
	sid: string;

	/**
	 * Дата и время запроса.
	 */
	time_stamp: string;

	/**
	 * Список билетов.
	 */
	@Type(() => Ticket)
	@ValidateNested()
	ticket: Array<Ticket>;
}
