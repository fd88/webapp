import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { ITransactionResponse, ITransactionTicket } from '@app/core/services/transaction/transaction-types';
import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { ITmlBmlBetDataItem } from '@app/tmlbml/services/tmlbml-lottery.service';

// -----------------------------
//  Request
// -----------------------------

/**
 * Интерфейс модели одной ставки для бумажной лотереи.
 * Содержит два параметра одного диапазона - {@link b1} и {@link b2}.
 * В одном диапазоне могут продаваться только билеты из одной пачки.
 */
export interface IPaperInstantBet {

	/**
	 * Содержит штрих-код первого билета из продаваемого диапазона.
	 * {@link IPaperInstantBet}
	 */
	b1: string;

	/**
	 * Содержит штрих-код последнего билета из продаваемого диапазона.
	 * {@link IPaperInstantBet}
	 */
	b2: string;

}

/**
 * Список диапазонов.
 */
export interface PaperTicketsData {
	/**
	 * Массив билетов в диапазоне.
	 */
	tickets: Array<IPaperInstantBet>;
}

/**
 * Модель для вызова запроса API {@link EsapParams.PaperInstantRegBet PaperInstantRegBet}.
 * В качестве параметра ставки используется модель {@link ZabavaBetsData}.
 */
export class PaperInstantRegBetReq extends CancelableApiClient {
	/**
	 * Конструктор.
	 *
	 * @param {AppStoreService} appStoreService Централизованое хранилище данных.
	 * @param {string} drawCode Код тиража.
	 * @param {ITmlBmlBetDataItem} betData Список ставок.
	 */
	constructor(appStoreService: AppStoreService, drawCode: string, betData: ITmlBmlBetDataItem) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.TML_BML, EsapActions.PaperInstantRegBet), betData);

		this.gameCode = LotteryGameCode.TML_BML;

		const ticketsData: PaperTicketsData = {
			tickets: betData.ticketList
				.map(m => {
					return {
						b1: m.bcFrom.barcode,
						b2: m.bcTo.barcode
					};
				})
		};

		this.params.append(EsapParams.GAME_CODE, `${this.gameCode}`);
		this.params.append(EsapParams.DRAW_CODE, drawCode);
		this.params.append(EsapParams.TICKETS_DATA, JSON.stringify(ticketsData));
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель параметра {@link PaperInstantRegBetResp.ticket}.
 */
export class PaperInstantTicket implements ITransactionTicket {
	/**
	 * Идентификатор билета.
	 */
	id: string;
	/**
	 * Сумма ставки.
	 */
	bet_sum: string;
	/**
	 * Описсание билета.
	 */
	description: string;
	/**
	 * Ссылка на шаблон билета.
	 */
	ticket_templ_url: string;
	/**
	 * Массив серий билета.
	 */
	serie: Array<any>;
}

/**
 * Модель ответа на запрос API PaperInstantRegBet.
 */
export class PaperInstantRegBetResp implements ITransactionResponse {
	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;

	/**
	 * Код ошибки.
	 */
	err_code: number;

	/**
	 * Описание ошибки.
	 */
	err_descr: string;

	// user_id: string;
	// sid: string;
	// time_stamp: string;
	// client_id: string;

	/**
	 * Массив билетов.
	 */
	@Type(() => PaperInstantTicket)
	@ValidateNested()
	ticket: Array<PaperInstantTicket>;

	/**
	 * Ссылка на шаблон билета.
	 */
	ticket_templ_url: string;
}
