// -----------------------------
//  Request
// -----------------------------

import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/ws/api/types";

/**
 * Модель запроса для вызова API AuthPhone.
 */
export class AuthPhoneReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизовано е хранилище данных.
	 * @param phone Телефон пользователя
	 */
	constructor(
		appStoreService: AppStoreService,
		phone: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.AuthPhone));

		this.params.append(EsapParams.PHONE, phone);
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API AuthPhone
 */
export interface AuthPhoneResp extends IResponse {

	/**
	 * Идентификатор клиента
	 */
	client_id?: string;

	client_trans_id: string;

	/**
	 * Код ошибки. Значение 0 указывает на успешное выполнение операции
	 */
	err_code: number;

	err_descr: string;
}
