import { ApiClient } from '@app/core/net/http/api/api-client';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import { IResponse } from '@app/core/net/http/api/types';

/**
 * Модель запроса для вызова API SendAuthCode
 */
export class SendAuthCodeReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param {string} playerPhone Номер телефона игрока в формате 9 цифр.
	 * @param {string} multiUserID Идентификатор пользователя, который обнаружен на пользовательском компьютере.
	 * Используется для отслеживания нескольких аккаунтов от одного и того же пользователя
	 */
	constructor(
		appStoreService: AppStoreService,
		playerPhone: string,
		multiUserID?: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.SendAuthCode));

		this.params.append(EsapParams.PLAYER_PHONE, playerPhone);

		if (multiUserID) {
			this.params.append(EsapParams.MULTIUSER_ID, multiUserID);
		}
	}

}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API SendAuthCode
 */
export interface SendAuthCodeResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;
}
