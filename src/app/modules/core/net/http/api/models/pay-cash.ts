import { Injector } from '@angular/core';
import { IResponse } from '@app/core/net/http/api/types';
import { ApiClient } from '@app/core/net/http/api/api-client';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { EsapActions, EsapParams } from '@app/core/configuration/esap';
import { LotteryGameCode } from '@app/core/configuration/lotteries';

// -----------------------------
//  Request
// -----------------------------

/**
 * Модель для вызова запроса API {@link EsapParams.PayCash PayCash}.
 */
export class PayCashReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {Injector} injector Инжектор зависимостей.
	 * @param {number} summa Сумма пополнения счета игрока в копейках.
	 * @param {string} sms Код подтверждения, введенный игроком из СМС.
	 * @param {string} rcptCode Код квитанции.
	 */
	constructor(injector: Injector, summa: number, sms: string, rcptCode: string) {
		const appStoreService = injector.get(AppStoreService);

		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.PayCash));

		if (appStoreService.hasOperator()) {
			this.params.append(EsapParams.USER_ID, appStoreService.operator.value.userId);
			this.params.append(EsapParams.SID, appStoreService.operator.value.sessionId);
		}

		this.params.append(EsapParams.CODE, `${rcptCode}`);
		this.params.append(EsapParams.PAY_SUM, `${summa}`);
		this.params.append(EsapParams.SMS_CODE, `${sms}`);
	}

}

// -----------------------------
//  Response
// -----------------------------
/**
 * Абстрактная модель ответа на запрос API {@link EsapParams.PayCash PayCash}.
 */
export abstract class PayCashResp implements IResponse {
	/**
	 * Идентификатор транзакции.
	 */
	client_trans_id: string;
	/**
	 * Код ошибки.
	 */
	err_code: number;
	/**
	 * Описание ошибки.
	 */
	err_descr: string;
}
