import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API VerifyPhone
 */
export class VerifyPhoneReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param phone Телефон пользователя
	 * @param pin PIN код пользователя (полученный sms-сообщением)
	 */
	constructor(
		appStoreService: AppStoreService,
		phone: string,
		pin: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.VerifyPhone, true));

		this.params.append(EsapParams.PHONE, phone);
		this.params.append(EsapParams.PIN, pin);
		this.params.remove(EsapParams.PROTO_TYPE);
		// this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API VerifyPhone
 */
export interface VerifyPhoneResp extends IResponse {
	/**
	 * Идентификатор клиента
	 */
	client_id?: string;
}
