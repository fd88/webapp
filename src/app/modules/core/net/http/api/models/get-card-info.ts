import {ApiClient} from "@app/core/net/http/api/api-client";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {LotteryGameCode} from "@app/core/configuration/lotteries";
import {EsapActions, EsapParams} from "@app/core/configuration/esap";
import {IResponse} from "@app/core/net/http/api/types";

/**
 * Модель запроса для вызова API GetCardInfo
 */
export class GetCardInfoReq extends ApiClient {

	/**
	 * Конструктор запроса.
	 *
	 * @param {AppStoreService} appStoreService Централизованное хранилище данных.
	 * @param cardNum
	 */
	constructor(
		appStoreService: AppStoreService,
		cardNum: string
	) {
		super(appStoreService, appStoreService.Settings.getEsapActionUrl(LotteryGameCode.Undefined, EsapActions.GetCardInfo, true));

		this.params.append(EsapParams.CARD_NUMBER, cardNum);
		this.params.append(EsapParams.CLIENT_ID, appStoreService.Settings.termCode);
		this.params.remove(EsapParams.PROTO_TYPE);
	}
}

// -----------------------------
//  Response
// -----------------------------

/**
 * Модель ответа на запрос API GetCardInfo
 */
export interface GetCardInfoResp extends IResponse {

	total_bonus_sum: number;
	card_limits: {
		max_bonus_use_limit: {
			current_value: number;
			limit_value: number;
		};

		withdraw_limit: {
			current_value: number;
			limit_value: number;
		};

		max_winpay_limit: {
			current_value: number;
			limit_value: number;
		};

		min_bonus_sum_with_auth: {
			limit_value: number;
		};

		verification_needed_min_win_sum: {
			limit_value: number;
		}
	};

	challenges: Array<{
		id: number;
		code: number;
		name: string;
		action_type: string;
		date_begin: string;
		date_end: string;
		amount_total: number;
		count_total: number;
		bonus_type_code: string;
		bonus_value: number;
		action_condition: string;
		participating_lotteries: [
			{
				id: number;
				code: number;
				name: string;
				type: string;
				is_accrual_bonuses: true,
				is_use_bonuses: false
			}
		]
	}>;

}
