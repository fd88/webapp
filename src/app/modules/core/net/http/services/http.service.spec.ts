import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HttpService } from './http.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { IRequest, IResponse } from '@app/core/net/http/api/types';

xdescribe('HttpService', () => {
	let httpTestingController: HttpTestingController;
	let service: HttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				HttpClientTestingModule
			],
			providers: [
				HttpService
			]
		});

		httpTestingController = TestBed.inject(HttpTestingController);
		service = TestBed.inject(HttpService);
	});

	afterEach(() => {
		// console.log('+++++++++++++++++++aaaaaaaaaaaaaaaa');
		// httpTestingController.verify();
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

// -----------------------------
//  Static functions
// -----------------------------

export class HttpServiceStub /*extends HttpService*/ {

	sendApi(request: IRequest, timeout?: number, retryCount?: number): Promise<IResponse> {
		return Promise.resolve({} as IResponse);
	}

	sendGet(url: string, headers: HttpHeaders, timeout?: number, retryCount?: number): Promise<string> {
		return Promise.resolve('ok response');
	}

	constructor() {
		// super(
		// 	{
		// 		post: (...arg) => {console.log('++++++++', arg)}
		// 	} as HttpClient
		// );
		// super({} as HttpClient);
	}
}
