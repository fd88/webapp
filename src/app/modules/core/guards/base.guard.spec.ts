import { TestBed } from '@angular/core/testing';

import { BaseGuard } from './base.guard';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';

xdescribe('BaseGuard', () => {
  let guard: BaseGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
		imports: [
			HttpClientTestingModule,
		],
		providers: [
			HttpService,
			AppStoreService
		]
	});
    guard = TestBed.inject(BaseGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
