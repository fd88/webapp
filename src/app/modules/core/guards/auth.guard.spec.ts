import { inject, TestBed } from '@angular/core/testing';

import { AuthGuard } from '@app/core/guards/auth.guard';
import { AuthService } from '@app/core/services/auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { LogService } from '@app/core/net/ws/services/log/log.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';
import { Operator } from '@app/core/services/store/operator';
import { LotteriesService } from '@app/core/services/lotteries.service';
import { DialogContainerService } from '@app/core/dialog/services/dialog-container.service';


xdescribe('AuthGuard', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot()
			],
			providers: [
				AuthGuard,
				AuthService,
				HttpService,
				LotteriesService,
				DialogContainerService,

				AppStoreService
			]
		});
	});

	it('should be created', inject([AuthGuard], (guard: AuthGuard) => {
		expect(guard).toBeTruthy();
	}));

	it('check guard function',
		inject([
			LogService,
			AppStoreService,
			AuthGuard,
			AuthService
		], (log: LogService, store: AppStoreService, service: AuthGuard, auth: AuthService) => {
		expect(service.canActivate(undefined, undefined)).toBeFalsy();

		store.operator.next(new Operator('123', 'test_session_id', '12345678', 0));
		store.isLoggedIn$$.next(true);
		expect(service.canActivate(undefined, undefined)).toBeTruthy();
	}));
});
