import { TestBed } from '@angular/core/testing';

import { NavigationGuard } from './navigation.guard';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {CoreModule} from "@app/core/core.module";

xdescribe('NavigationGuard', () => {
  let guard: NavigationGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
		imports: [
			HttpClientTestingModule,
			CoreModule
		],
		declarations: [

		],
		providers: [
			HttpClient,
			AppStoreService
		]
	});
    guard = TestBed.inject(NavigationGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
