import { inject, TestBed } from '@angular/core/testing';

import { InitGuard } from '@app/core/guards/init.guard';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpService } from '@app/core/net/http/services/http.service';
import { AppStoreService } from '@app/core/services/store/app-store.service';


xdescribe('InitGuard', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				RouterTestingModule.withRoutes([]),
				TranslateModule.forRoot()
			],
			providers: [
				InitGuard,
				HttpService,
				AppStoreService
			]
		});
	});

	it('should be created', inject([InitGuard], (guard: InitGuard) => {
		expect(guard).toBeTruthy();
	}));

	it('check guard function', inject([InitGuard], (guard: InitGuard | any) => {
		guard.isInitialized = false;
		expect(guard.canActivate(undefined, undefined)).toBeFalsy();

		guard.isInitialized = true;
		expect(guard.canActivate(undefined, undefined)).toBeTruthy();
	}));
});
