import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';
import { TopBarModule } from '@app/top-bar/top-bar.module';
import { HamburgerModule } from '@app/hamburger/hamburger.module';
import { CentralMenuModule } from '@app/central-menu/central-menu.module';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {environment} from "@app/env/environment";
import {HttpLoaderFactory} from "./app.module";
import {CustomPreloadingStrategy, ROUTES} from "./app-routing.module";
import {HAMMER_GESTURE_CONFIG} from "@angular/platform-browser";
import {HammerConfig} from "@app/util/hammer-config";
import {DialogContainerComponent} from "@app/core/dialog/components/dialog-container.components";
import {LayoverDirective} from "@app/shared/directives/layover.directive";
import {MouseTrackDirective} from "@app/shared/directives/mouse-track.directive";
import {HamburgerMenuComponent} from "@app/hamburger/components/hamburger-menu/hamburger-menu.component";
import {TopBarPanelComponent} from "@app/top-bar/components/top-bar-panel/top-bar-panel.component";
import {CentralMenuComponent} from "@app/central-menu/central-menu/central-menu.component";
import {timer} from "rxjs";
import {InitService} from "@app/core/services/init.service";
import {AuthService} from "@app/core/services/auth.service";
import {AppStoreService} from "@app/core/services/store/app-store.service";
import {StorageService} from "@app/core/net/ws/services/storage/storage.service";
import {CameraComponent} from "./modules/features/camera/components/camera/camera.component";

describe('AppComponent', () => {
	let fixture: ComponentFixture<AppComponent>;
	let app: AppComponent;
	let appStoreService: AppStoreService;
	let authService: AuthService;
	let storageService: StorageService;
	let initService: InitService;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			imports: [
				BrowserAnimationsModule,
				RouterTestingModule.withRoutes(ROUTES),
				HttpClientModule,
				CoreModule,
				SharedModule,
				TopBarModule,
				HamburgerModule,
				CentralMenuModule,
				TranslateModule.forRoot({
					loader: {
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps: [HttpClient]
					}
				})
			],
			declarations: [
				AppComponent,
				DialogContainerComponent,
				LayoverDirective,
				MouseTrackDirective,
				HamburgerMenuComponent,
				TopBarPanelComponent,
				CentralMenuComponent,
				CameraComponent
			],
			providers: [
				CustomPreloadingStrategy,
				{
					provide: HAMMER_GESTURE_CONFIG,
					useClass: HammerConfig
				},
				HttpClient,
				InitService,
				AuthService,
				AppStoreService,
				StorageService
			]
		})
		.compileComponents();

		fixture = TestBed.createComponent(AppComponent);
		app = fixture.componentInstance;

		appStoreService = TestBed.inject(AppStoreService)
		authService = TestBed.inject(AuthService);
		storageService = TestBed.inject(StorageService);
		initService = TestBed.inject(InitService);

		// localStorage.setItem(ApplicationAppId, JSON.stringify(SESS_DATA));
	}));

	it('should create the AppComponent', () => {
		expect(app).toBeTruthy();
	});

	it('test ngOnInit event handler', async () => {
		spyOn(app, 'ngOnInit').and.callThrough();
		app.ngOnInit();
		await timer(300).toPromise();
		expect(app.ngOnInit).toHaveBeenCalled();
	});

	it('test onActivateHamburgerMenuHandler', async () => {
		app.ngOnInit();
		await timer(300).toPromise();
		spyOn(app, 'onActivateHamburgerMenuHandler').and.callThrough();
		app.onActivateHamburgerMenuHandler(true);
		app.onActivateHamburgerMenuHandler(false);
		expect(app.onActivateHamburgerMenuHandler).toHaveBeenCalled();
	});

	it('test onClickGoHomeHandler', async () => {
		app.ngOnInit();
		await timer(300).toPromise();
		spyOn(app, 'onClickGoHomeHandler').and.callThrough();
		app.onClickGoHomeHandler();
		await timer(300).toPromise();
		expect(app.onClickGoHomeHandler).toHaveBeenCalled();
	});

	it('test onConsoleLog event', () => {
		spyOn(app, 'onConsoleLog').and.callThrough();
		app.onConsoleLog('test');
		expect(app.onConsoleLog).toHaveBeenCalled();
	});

	it('test onConsoleError event', () => {
		spyOn(app, 'onConsoleError').and.callThrough();
		app.onConsoleError('error');
		expect(app.onConsoleError).toHaveBeenCalled();
	});
});
