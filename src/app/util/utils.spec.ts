import { CancelableApiClient } from '@app/core/net/http/api/api-client';
import { LotteryGameCode } from '@app/core/configuration/lotteries';
import {
	alignCenter,
	alignLeft,
	alignRight, alignTwoItemsByWidth, b64EncodeUnicode,
	calculateStringHash, cameraErrorsHandler,
	convertDateToISOFormat, convertObjectToString, CSDateToRusLocale,
	daysInMonth,
	daysListInMonth,
	endOfDay, formattedAmount,
	generateRandomNumberWithExclude,
	getElapsed,
	getEsapRequestId,
	getGameNameKeyByLotteryCode,
	getWsRequestId,
	isDateString, ISOtoCSDate, loadImage,
	numberFromStringCurrencyFormat,
	startOfDay,
	stringFromNumberCurrencyFormat,
	updateObjectByPropertyChain
} from '@app/util/utils';

describe('utils tests', () => {
	it('test of startOfDay', () => {
		let sd = startOfDay(new Date(2019, 8, 12, 12, 33, 22, 111));
		expect(sd.getSeconds() + sd.getMinutes() + sd.getHours() + sd.getMilliseconds()).toEqual(0);

		sd = startOfDay();
		expect(sd.getSeconds() + sd.getMinutes() + sd.getHours() + sd.getMilliseconds()).toEqual(0);
	});

	it('test of endOfDay ', () => {
		let sd = endOfDay(new Date(2019, 8, 12, 12, 33, 22, 111));
		expect(sd.getSeconds() + sd.getMinutes() + sd.getHours() + sd.getMilliseconds()).toEqual(23 + 59 + 59 + 999);

		sd = endOfDay();
		expect(sd.getSeconds() + sd.getMinutes() + sd.getHours() + sd.getMilliseconds()).toEqual(23 + 59 + 59 + 999);
	});

	it('test of daysInMonth', () => {
		[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31].forEach((days, month) => {
			expect(daysInMonth(2019, month + 1)).toEqual(days);
		});
	});

	it('test of daysListInMonth ', () => {
		[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31].forEach((days, month) => {
			expect(daysListInMonth(2019, month + 1).length).toEqual(days);
		});
	});

	it('test of wsRequestId/esapRequestId', () => {
		let prevEsap: string;
		let prevWS: string;
		Array.from(Array(1000)).forEach(_ => {
			let newEsap = getEsapRequestId();
			expect(newEsap).toBeDefined();
			expect(newEsap).not.toEqual(prevEsap);
			prevEsap = newEsap;

			let newWS = getWsRequestId();
			expect(newWS).toBeDefined();
			expect(newWS).not.toEqual(prevWS);
			prevWS = newWS;
		});
	});

	it('test of isDateString', () => {
		expect(isDateString('1234567890')).toBeFalsy();
		expect(isDateString(new Date().toISOString())).toBeTruthy();
	});

	it('test of convertDateToISOFormat', () => {
		expect(convertDateToISOFormat('2018-11-28 13:03:05')).toEqual('2018-11-28T13:03:05');
		expect(convertDateToISOFormat('1234567890')).toBeUndefined();
	});

	it('test of calculateStringHash', () => {
		expect(calculateStringHash('1234567890')).toEqual('-2054162789');
		expect(calculateStringHash('11111')).toEqual('+46760945');
		expect(calculateStringHash('1234567890') === calculateStringHash('12345678901')).toBeFalsy()
	});

	it('test of generateRandomNumberWithExclude', () => {
		Array.from(Array(1000)).forEach(() => {
			expect(generateRandomNumberWithExclude(5, 10, 7) !== 7).toBeTruthy();
		});

		expect(generateRandomNumberWithExclude(NaN, NaN, NaN)).toBeUndefined();
	});

	it('test of numberFromStringCurrencyFormat', () => {
		expect(numberFromStringCurrencyFormat('11.00')).toEqual(1100);
		try {
			numberFromStringCurrencyFormat('xx.yy');
		} catch (e) {
			expect(true).toBeTruthy();
		}
		// expect(numberFromStringCurrencyFormat('xx.yy')).toThrow();//toThrowError('Currency format not valid');
	});

	it('test of stringFromNumberCurrencyFormat', () => {
		expect(stringFromNumberCurrencyFormat(12300)).toEqual('123.00');
	});

	it('test of getElapsed', () => {
		expect(getElapsed(0) !== undefined).toBeTruthy();
	});

	it('test of alignLeft', () => {
		expect(alignLeft(10, '123', '.')).toEqual('123.......');
		expect(alignLeft(10, '123')).toEqual('123       ');
		expect(alignLeft(5, '1234567890')).toEqual('1234567890');
	});

	it('test of alignRight', () => {
		expect(alignRight(10, '123', '.')).toEqual('.......123');
		expect(alignRight(10, '123')).toEqual('       123');
		expect(alignRight(5, '1234567890')).toEqual('1234567890');
	});

	it('test of alignCenter', () => {
		expect(alignCenter(11, '123', '.')).toEqual('....123....');
		expect(alignCenter(11, '123')).toEqual('    123    ');
		expect(alignCenter(2, '123')).toEqual('123');
	});

	it('test of alignTwoItemsByWidth', () => {
		expect(alignTwoItemsByWidth('111', '222', 10)).toEqual('111    222');
	});

	it('test of getGameNameKeyByLotteryCode', () => {
		const data: Array<{gameCode: number, key: string, response?: any}> = [
			{gameCode: LotteryGameCode.MegaLot, key: 'lottery.megalot.megalot_name'},
			{gameCode: LotteryGameCode.Sportprognoz, key: 'lottery.sportprognoz.game_name'},
			{gameCode: LotteryGameCode.Zabava, key: 'lottery.zabava.loto_zabava'},
			{gameCode: LotteryGameCode.Gonka, key: 'lottery.race_for_money.loto_gonka'},
			{gameCode: LotteryGameCode.Tip, key: 'lottery.tiptop.game_tip_name'},
			{gameCode: LotteryGameCode.Top, key: 'lottery.tiptop.game_top_name'},
			{gameCode: LotteryGameCode.TML_BML, key: 'lottery.tmlbml.game_name'},
			{gameCode: LotteryGameCode.Kare, key: 'lottery.kare.game_name'},
			{gameCode: LotteryGameCode.Undefined, key: '-'},
			{gameCode: LotteryGameCode.Undefined, key: '-', response: {}},
			{gameCode: LotteryGameCode.Undefined, key: '--', response: {ticket: []}},
			{gameCode: LotteryGameCode.Undefined, key: 'xxx', response: {ticket: [{game_name: 'xxx'}]}}
		];
		data.forEach(v => {
			const req: CancelableApiClient | any = {gameCode: v.gameCode};
			expect(getGameNameKeyByLotteryCode(req, v.response)).toEqual(v.key)
		});
	});

	it('test of updateObjectByPropertyChain', () => {
		const srcObj = {xxx: {yyy: {zzz: 0}}};
		expect(srcObj.xxx.yyy.zzz).toEqual(0);
		updateObjectByPropertyChain(srcObj, 'xxx.yyy.zzz', 1);
		expect(srcObj.xxx.yyy.zzz).toEqual(1);

		expect(srcObj.xxx.yyy['nnn']).toBeUndefined();
		updateObjectByPropertyChain(srcObj, 'xxx.yyy.nnn', 2);
		expect(srcObj.xxx.yyy['nnn']).toEqual(2);
	});

	it('test of formattedAmount', () => {
		expect(formattedAmount(undefined)).toEqual('0.00');
		expect(formattedAmount(0)).toEqual('0.00');
		expect(formattedAmount(1)).toEqual('1.00');
		expect(formattedAmount(1.23)).toEqual('1.23');
	});

	it('test of convertObjectToString', () => {
		try {
			convertObjectToString(undefined);
		} catch (e) {
			expect(true).toBeTruthy();
		}
		expect(convertObjectToString(1)).toEqual('1');
		expect(convertObjectToString('2')).toEqual('2');
		expect(convertObjectToString([3, '4', {v5: 5, v6: '6'}])).toEqual('3456');
		expect(convertObjectToString({})).toEqual('');
	});

	it('test CSDateToRusLocale', () => {
		expect(CSDateToRusLocale('2099-12-31T23:59:59', true)).toEqual('31.12.2099 23:59:59');
	});

	it('test cameraErrorsHandler function', () => {
		expect(cameraErrorsHandler('NotAllowedError')).toEqual('scanner.allow_camera');
	});

	it('test cameraErrorsHandler function 2', () => {
		expect(cameraErrorsHandler(null)).toEqual('');
	});

	it('test cameraErrorsHandler function 3', () => {
		expect(cameraErrorsHandler({
			name: 'Fake error',
			message: 'Fake error message'
		})).toEqual('Fake error: Fake error message');
	});

	it('test b64EncodeUnicode function', () => {
		expect(b64EncodeUnicode('test string')).toEqual('dGVzdCBzdHJpbmc=');
	});

	it('test loadImage function', async () => {
		const base64Code = await loadImage('/assets/img/1x1.png').toPromise();
		expect(base64Code.outerHTML).toEqual('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=">');
	});

	it('test ISOtoCSDate function', () => {
		expect(ISOtoCSDate('31-12-2099 23:59:59', true, true)).toEqual('2099-12-31 23:59:59');
	});

	it('test ISOtoCSDate function 2', () => {
		expect(ISOtoCSDate('31-12-2099 23:59:59', false, false)).toEqual('2099-12-31');
	});
});
