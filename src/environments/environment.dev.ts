/**
 * Объект среды выполнения приложения для окружения разработки
 * Настройки для DEV - 100605
 */
export const environment = {
	version: 'DEV-100605',
	production: false,
	terminal: false,
	enableProfile: true,
	enableActions: false,
	mockData: false,
	// login: '100605',
	// password: '100605',
	login: '8200035',
	password: '1357910',
	printerUrl: 'ws://127.0.0.1:2424',
	sms: ''
};
