// ////////////////////////////////////////////////////////////////////////////
//
// In development mode, to ignore zone related error stack frames such as
// `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
// import the following file, but please comment it out in production mode
// because it will have performance impact when throw error
//
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

/**
 * Этот файл может быть заменен во время сборки с помощью массива fileReplacements.
 * Список замен файлов можно найти в файле angular.json
 */
export const environment = {
	version: 'DEV-100604',
	production: false,
	terminal: false,
	enableProfile: true,
	enableActions: true,
	mockData: false,
	login: '100604',
	password: '100604',
	printerUrl: 'ws://127.0.0.1:2424',
	sms: ''
};
