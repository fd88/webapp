/**
 * Объект среды выполнения приложения для препродакшена
 */
export const environment = {
	version: '2.16.52',
	production: true,			// признак продуктивной среды
	terminal: false,			// признак, указывающий на реальный терминал
	enableProfile: true,		// разрешить профилирование и статистику
	enableActions: false,		// разрешить модуль "Акции"
	mockData: false,
	login: '',
	password: '',
	printerUrl: 'ws://127.0.0.1:2424',
	sms: ''
};
