const fs = require('fs');
const { exec } = require("child_process");

const run = (cmd, ignoreErrors = false) => {
	return new Promise((resolve, reject) => {
		exec(cmd, (error, stdout, stderr) => {
			if (error) {
				if (ignoreErrors) {
					resolve(error.message);
				} else {
					console.log('Error');
					reject(error.message);
				}
			}
			if (stderr) {
				if (ignoreErrors) {
					resolve(stderr);
				} else {
					console.log('Error');
					reject(stderr);
				}
			}
			resolve(stdout);
		});
	});
};

const main = async () => {
	const data = fs.readFileSync('changelog.md', 'utf8');
	const result = data.match(/^\#\# \[\d+\.\d+\.\d+\]([^\#\#]|\r|\n)+\#\# \[\d+\.\d+\.\d+\]/gm);
	if (result[0]) {
		const lastRecord = result[0].match(/^\#\# \[\d+\.\d+\.\d+\]([^\#\#]|\r|\n)+/gm);
		if (lastRecord[0]) {
			const comment = lastRecord[0]
				.trim();
			const packageRaw = fs.readFileSync('package.json', 'utf8');
			const packageJson = JSON.parse(packageRaw);
			const currentVer = packageJson.version
				.split('.')
				.map(dig => parseInt(dig, 10));
			const newVerMatches = comment.match(/\d+\.\d+\.\d+/);
			if (newVerMatches[0]) {
				const newVer = newVerMatches[0]
					.split('.')
					.map(dig => parseInt(dig, 10));

				let stdOut;

				stdOut = await run('git add .', true);
				console.log(stdOut);
				stdOut = await run(`git commit -m "Коммит и пуш всех изменений перед сборкой версии ${newVerMatches[0]}"`, true);
				console.log(stdOut);
				stdOut = await run('git push origin develop', true);
				console.log(stdOut);

				const verNum = ((newVer[0] - currentVer[0]) === 1) ? 'major' :
					((newVer[1] - currentVer[1]) === 1) ? 'minor' : 'regular';

				const command = `npm run build:webalt:${verNum}`;
				// const command = 'npm run build';
				try {
					stdOut = await run(command);
					console.log(stdOut);
				} catch (error) {
					console.log(error);
				} finally {
					const webChangeLog = fs.readFileSync('./src/assets/webchangelog.json', 'utf8');
					console.log('webChangeLog =', webChangeLog);
					const webChangeLogJson = JSON.parse(webChangeLog);
					webChangeLogJson.items[0].version = newVer.join('.');
					webChangeLogJson.items[0].date = (new Date()).toISOString();
					console.log('webChangeLogJson =', webChangeLogJson);
					fs.writeFileSync('./src/assets/webchangelog.json', JSON.stringify(webChangeLogJson, null, '\t'));

					stdOut = await run('git add .', true);
					console.log(stdOut);
					stdOut = await run(`git commit -m "${comment}"`, true);
					console.log(stdOut);
					stdOut = await run('git push origin develop && git push origin2 develop', true);
					console.log(stdOut);

					stdOut = await run(`git tag -a msl_main_web-${newVerMatches[0]} -m "${comment}"`, true);
					console.log(stdOut);
					stdOut = await run(`git push origin msl_main_web-${newVerMatches[0]}`, true);
					console.log(stdOut);

					// stdOut = await run(`ssh d.fedorov:7KpHt4ywszHrm8FJ@build-server-el7.emict.net sh rebuild.sh`);
					// console.log(stdOut);
				}
			}
		}
	}
};
main().then();

