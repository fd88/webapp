%define webalt_root /usr/share/emict/webalt

Name:       msl-webalt
Version:    %webalt_version
Release:	1%{?dist}
Summary:    WebALT - web application for MSL

BuildArch:  noarch
License:	MSL
Source0:	%name-%version.tar.gz

BuildRequires:	nodejs >= 10.13

%description
WebALT - web application for MSL

%prep
%setup -q


%build


%install
mkdir -p %{buildroot}%{webalt_root}
cp -r * %{buildroot}%{webalt_root}


%files
%{webalt_root}



%changelog
* Tue Feb 11 2020 Yuriy Kashirin <yura@emict.net> 2.0.1-1.el7
- [CS-3949] WebALT - initial rpmbuild

