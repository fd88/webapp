////////////////////////////////////////////////////////////////////////////
// Karma configuration for SonarQube
////////////////////////////////////////////////////////////////////////////

module.exports = function (config) {
	config.set({
		basePath: '',
		frameworks: ['jasmine', '@angular-devkit/build-angular'],
		files: [
			{ pattern: 'src/config-alt-web.json', included: false, served: true},
			{ pattern: 'src/assets/i18n/ru.json', included: false, served: true},
			{ pattern: 'src/assets/i18n/ua.json', included: false, served: true},
			{ pattern: 'src/assets/i18n/en.json', included: false, served: true}
		],
		plugins: [
			require('karma-jasmine'),
			require('karma-chrome-launcher'),
			require('karma-jasmine-html-reporter'),
			require('karma-junit-reporter'),
			require('karma-coverage'),
			require('@angular-devkit/build-angular/plugins/karma')
		],
		client: {
			clearContext: false, // leave Jasmine Spec Runner output visible in browser,
			captureConsole: true
		},
		// coverageIstanbulReporter: {
		// 	dir: require('path').join(__dirname, './coverage'),
		// 	reports: ['html', 'lcov', 'cobertura', 'text-summary'],
		// 	fixWebpackSourcePaths: true
		// },
		coverageReporter: {
			dir: require('path').join(__dirname, './coverage'),
			subdir: '.',
			reporters: [
				{type: 'html'},
				{type: 'text-summary'}
			],
		},
		browserConsoleLogOptions: {
			level: 'log',
			format: '%b %T: %m',
			terminal: true
		},
		// reporters: ['dots', 'junit', 'progress', 'kjhtml'],
		reporters: ['dots', 'junit'],
		junitReporter: {
			outputDir: './junit',
			outputFile: 'test-results.xml',
			xmlVersion: '1'
		},
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		autoWatch: false,
		browsers: ['ChromeHeadless'],
		// customLaunchers: {
		// 	ChromeHeadlessNoSandbox: {
		// 		base: 'ChromeHeadless',
		// 		flags: ['--no-sandbox']
		// 	}
		// },
		singleRun: true,
		restartOnFileChange: false
	});
};
