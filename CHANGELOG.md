# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.16.52](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.51...msl_main_web-2.16.52) (2025-02-11)


### Features

* **check-information.component:** предупреждение на стр. подтверждения при большой задержке сети ([af44e7c](https://git.emict.net/ALT/WebApp/commit/af44e7c5025f3d85b545aed23e79e4ce67810643))

### [2.16.51](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.50...msl_main_web-2.16.51) (2025-01-20)


### Bug Fixes

* **transaction.service:** улучшена обработка транзакций с помощью использования вложенных команд ([554db6b](https://git.emict.net/ALT/WebApp/commit/554db6b96d1f29055631de8fb8c6e5e55a6c1079))

### [2.16.50](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.49...msl_main_web-2.16.50) (2025-01-14)


### Bug Fixes

* **camera:** восстановлено запоминание состояния камеры ([f44725b](https://git.emict.net/ALT/WebApp/commit/f44725be00a571c26b7cfa851f332f4066cadfec))

### [2.16.49](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.48...msl_main_web-2.16.49) (2025-01-14)


### Bug Fixes

* **camera:** немедленная деинициализация камеры при уничтожении компонента камеры ([33a17fe](https://git.emict.net/ALT/WebApp/commit/33a17fef4a59eeabf245d776176610a9af67576a))

### [2.16.48](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.47...msl_main_web-2.16.48) (2025-01-13)


### Features

* **camera:** опция скрытия камеры ([403d171](https://git.emict.net/ALT/WebApp/commit/403d171fd07c8b411128b4beed81080746def483))
* **camera:** опция скрытия камеры ([f3889eb](https://git.emict.net/ALT/WebApp/commit/f3889eb127706ca858b70a8a21ec74d106451ed2))

### [2.16.47](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.46...msl_main_web-2.16.47) (2024-12-31)


### Bug Fixes

* **ticket-payment:** убрана подсказка для продавца при невыигрышном билете во избежание дублирования ([2476200](https://git.emict.net/ALT/WebApp/commit/2476200ec3c32cbb88e7183df91f61550a171619))

### [2.16.46](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.45...msl_main_web-2.16.46) (2024-12-30)

### [2.16.45](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.44...msl_main_web-2.16.45) (2024-12-20)


### Bug Fixes

* **сп:** правка в валидации штрих-кода ставки в СП при сканировании с камеры ([a31e253](https://git.emict.net/ALT/WebApp/commit/a31e25355456c6d0fb982be119b56ebbcf393e42))

### [2.16.44](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.43...msl_main_web-2.16.44) (2024-12-09)


### Features

* **https://msl.atlassian.net/browse/cs-1539:** валидация штрих-кода в СП ([1cb020a](https://git.emict.net/ALT/WebApp/commit/1cb020afb594e89d185b80079dd5450b040ee862))

### [2.16.43](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.42...msl_main_web-2.16.43) (2024-12-04)

### [2.16.42](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.41...msl_main_web-2.16.42) (2024-12-03)


### Features

* **sp:** печать чека программы новым способом ([14c04ee](https://git.emict.net/ALT/WebApp/commit/14c04ee7e5c25e402c8c09fc8c94db01218bf0f8))

### [2.16.41](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.40...msl_main_web-2.16.41) (2024-11-28)


### Features

* **loyalty:** скрыт функционал по карте лояльности ([719cf4d](https://git.emict.net/ALT/WebApp/commit/719cf4d9647ec1127d1398f69fd4aea7c7affd7e))

### [2.16.40](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.39...msl_main_web-2.16.40) (2024-11-27)


### Features

* **sportprognoz:** в спортпрогнозе скрыт блок с картой лояльности ([99287e4](https://git.emict.net/ALT/WebApp/commit/99287e4f404b772fd462252889988dad37c62e6e))

### [2.16.39](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.38...msl_main_web-2.16.39) (2024-11-22)


### Features

* **https://msl.atlassian.net/browse/cs-1456:** спортпрогноз с покупкой билетов через штрих-код ([9954a32](https://git.emict.net/ALT/WebApp/commit/9954a322727b5ac00b96027462388aa5fbf4fdaa))
* **scanner-input:** доработан и улучшен компонент сканирования штрих-кодов ([c63345e](https://git.emict.net/ALT/WebApp/commit/c63345e7021ba263361435666848e1405289efab))
* **scanner-input:** разработан отдельный компонент для ввода штрих-кодов ([d378558](https://git.emict.net/ALT/WebApp/commit/d3785586880ec406e41ab4eb6ff9475fafc4c7be))
* **sportprognoz:** добавлена возможность регистрации лотереи по сгенерированной комбинации ([d997762](https://git.emict.net/ALT/WebApp/commit/d9977622e0c36545201eac96056df2b82840b8bf))
* **sportprognoz:** промежуточное сохранение ([f5540fa](https://git.emict.net/ALT/WebApp/commit/f5540fa625ea576f4a478b704b8bec5bbd982bf7))


### Bug Fixes

* **сп:** правка в доступности кнопки при невалидном поле штрих-кода ([be393f8](https://git.emict.net/ALT/WebApp/commit/be393f8deafa54581df3089103c5ba15950be92e))

### [2.16.38](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.37...msl_main_web-2.16.38) (2024-11-19)


### Features

* **loyalty-card-info:** добавлена дополнительная информация об использовании фортуналов за месяц ([8ec9df0](https://git.emict.net/ALT/WebApp/commit/8ec9df08e59fd40c175cd7026111802deb0540fc))

### [2.16.37](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.36...msl_main_web-2.16.37) (2024-11-18)


### Features

* **loyalty:** разные значения для покупки с использованием бонусов и без ([60ed3dc](https://git.emict.net/ALT/WebApp/commit/60ed3dcea3961446770bc069c887c0bdd47d7fe0))

### [2.16.36](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.35...msl_main_web-2.16.36) (2024-11-17)


### Bug Fixes

* **scanner-input:** правка в верстке, aspect-ratio чето не работал ([1b9191d](https://git.emict.net/ALT/WebApp/commit/1b9191dc8014f3b70a77d98ca62eacad5c1ed828))

### [2.16.35](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.34...msl_main_web-2.16.35) (2024-11-17)


### Features

* **transport:** запросы в XML при регистрации лотерей ([d279cb1](https://git.emict.net/ALT/WebApp/commit/d279cb1ce2779d24214162e0c5a429c90468896b))

### [2.16.34](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.33...msl_main_web-2.16.34) (2024-11-15)

### [2.16.33](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.32...msl_main_web-2.16.33) (2024-11-12)


### Features

* **check-information:** переделка компонента регистрации чека с отображением информацией по чеку ([bc08a38](https://git.emict.net/ALT/WebApp/commit/bc08a381941913e30d111547fb798380af638ef9))
* **check-information:** переделка последовательности при продаже лотереи ([d867e55](https://git.emict.net/ALT/WebApp/commit/d867e557c3cd810bc5fe6b33ad8cfd90afda3728))
* **https://msl.atlassian.net/browse/cs-1294:** доработки: https://msl.atlassian.net/browse/CS-1294 ([fecff29](https://git.emict.net/ALT/WebApp/commit/fecff297a3bccfa49223936155dbe8dc53bdb46b))
* **loyalty:** переделка страницы подтверждения регистрации как в SX ([8d82d2d](https://git.emict.net/ALT/WebApp/commit/8d82d2dd445ead69982ff8365d6cb928ec8b7274))

### [2.16.32](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.31...msl_main_web-2.16.32) (2024-11-04)


### Bug Fixes

* **transaction.service:** убран тестовый графический билет ([255e990](https://git.emict.net/ALT/WebApp/commit/255e990f0f515a7099ea60d252c6ce58ad085100))

### [2.16.31](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.30...msl_main_web-2.16.31) (2024-10-22)


### Features

* **https://msl.atlassian.net/browse/cs-1294:** заменен компонент ввода номера телефона ([99492e3](https://git.emict.net/ALT/WebApp/commit/99492e39d01d06e36fc395fc6b24f700faef8395))
* **user-loyalty-auth:** создан phone-number component ([d0f98bb](https://git.emict.net/ALT/WebApp/commit/d0f98bb70994ea326781012745e6f904aab0c38f))

### [2.16.30](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.29...msl_main_web-2.16.30) (2024-10-22)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-1294:** правки, доработки ([a916ec5](https://git.emict.net/ALT/WebApp/commit/a916ec548df012c96c3db178cdd7284194d1aff6))

### [2.16.29](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.28...msl_main_web-2.16.29) (2024-10-21)

### [2.16.28](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.27...msl_main_web-2.16.28) (2024-10-21)


### Bug Fixes

* **cs-1495, cs-1496, cs-1497, cs-1498:** правки по функционалу карты лояльности ([c656c4b](https://git.emict.net/ALT/WebApp/commit/c656c4bca321d9dd769d462fc257bbd658efa33c))

### [2.16.27](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.26...msl_main_web-2.16.27) (2024-09-16)


### Bug Fixes

* **api-client:** добавление номера карты только для экшенов *RegBet ([f4f63b7](https://git.emict.net/ALT/WebApp/commit/f4f63b78a6d004b32ebe379d71ac9ee10da7862e))

### [2.16.26](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.25...msl_main_web-2.16.26) (2024-09-16)


### Bug Fixes

* **assigncard:** cARD_NUMBER => CARD_NUM ([5f10077](https://git.emict.net/ALT/WebApp/commit/5f10077865ea1b39409b16233930af892c40eba8))

### [2.16.25](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.24...msl_main_web-2.16.25) (2024-09-13)


### Bug Fixes

* **ticket-loyalty-payment:** правки в компоненте перевода выигрыша на карту лояльности - прелоадеры ([feb50b0](https://git.emict.net/ALT/WebApp/commit/feb50b0910fac3b842252501e4c1f7e3023414df))

### [2.16.24](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.23...msl_main_web-2.16.24) (2024-09-13)


### Bug Fixes

* **ticket-loyalty-payment:** правки в компоненте перевода выигрыша на карту лояльности ([1200f87](https://git.emict.net/ALT/WebApp/commit/1200f871b9c7ae30ddee2b01c5d691733c14210d))

### [2.16.23](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.22...msl_main_web-2.16.23) (2024-09-12)


### Bug Fixes

* **winpaytocard:** правки в API: WinPayToCard ([4aaf329](https://git.emict.net/ALT/WebApp/commit/4aaf3296802b8d598ae8197846ca8eaaecfd259d))

### [2.16.22](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.21...msl_main_web-2.16.22) (2024-09-12)


### Bug Fixes

* **ticket-loyalty-payment:** правки в компоненте зачисления выигрыша на карту лояльности ([9e6f63c](https://git.emict.net/ALT/WebApp/commit/9e6f63c2a3e51c3f3f0f9166fba744f49eca2786))

### [2.16.21](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.20...msl_main_web-2.16.21) (2024-09-10)


### Bug Fixes

* **bonuses-withdrawal:** правки в компоненте списания бонусов ([2ef7d14](https://git.emict.net/ALT/WebApp/commit/2ef7d14a0bf1b8188c21d74039fa811f10d06af7))

### [2.16.20](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.19...msl_main_web-2.16.20) (2024-09-10)


### Features

* **card-info:** добавлен прелоадер ([561162c](https://git.emict.net/ALT/WebApp/commit/561162c6de180ad6d5add2d4dd72bce5fe6583d5))

### [2.16.19](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.18...msl_main_web-2.16.19) (2024-09-09)


### Bug Fixes

* **bonus-withdrawal:** правки в компоненте списания бонусов ([309a17c](https://git.emict.net/ALT/WebApp/commit/309a17cdce6e7857c6faa4fda7cdff8d37470695))

### [2.16.18](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.17...msl_main_web-2.16.18) (2024-09-09)


### Bug Fixes

* **loyalty:** правки в валидации и списании бонусов ([80306b2](https://git.emict.net/ALT/WebApp/commit/80306b23a840b25ee8b1471142589bcdd355752a))

### [2.16.17](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.16...msl_main_web-2.16.17) (2024-09-09)


### Bug Fixes

* **assigncard:** assignCard: Form URLEncoded ([9057fdd](https://git.emict.net/ALT/WebApp/commit/9057fddbe1c9e982447848f5c8821946fc8e3dd8))

### [2.16.16](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.15...msl_main_web-2.16.16) (2024-09-09)


### Bug Fixes

* **assigncard:** изменен набор параметров в AssignCard ([170d3e7](https://git.emict.net/ALT/WebApp/commit/170d3e7d27693afcce85a099525d24cbf00e579d))

### [2.16.15](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.14...msl_main_web-2.16.15) (2024-08-05)


### Bug Fixes

* **tml:** правки в чеке о регистрации Стирачек ([bc61b0a](https://git.emict.net/ALT/WebApp/commit/bc61b0a4e493561fae2354b5f1c0a447a9d47045))

### [2.16.14](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.13...msl_main_web-2.16.14) (2024-08-05)


### Bug Fixes

* **tml:** правки в печати чека ТМЛ ([49dccaf](https://git.emict.net/ALT/WebApp/commit/49dccaf14fda1c05a4413f6f998b8a09f4790b12))

### [2.16.13](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.12...msl_main_web-2.16.13) (2024-08-05)

### [2.16.12](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.11...msl_main_web-2.16.12) (2024-08-02)


### Features

* **tml:** продажа без принтера и без карты ([fd5c239](https://git.emict.net/ALT/WebApp/commit/fd5c239f4d7e3f3884a40c9c8e167ab919d481a4))

### [2.16.11](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.10...msl_main_web-2.16.11) (2024-08-02)


### Bug Fixes

* **tmlbml:** правки в БМЛ ([796f030](https://git.emict.net/ALT/WebApp/commit/796f030a967c82e809eb48a4e69ffe9e7bdef9db))

### [2.16.10](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.9...msl_main_web-2.16.10) (2024-08-01)


### Bug Fixes

* **ticket-check:** правки в перечислении выигрыша на карту лояльности ([6e64120](https://git.emict.net/ALT/WebApp/commit/6e6412007f4a967359a62429dc731b7a6bf4dd68))

### [2.16.9](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.8...msl_main_web-2.16.9) (2024-07-31)


### Bug Fixes

* **loyalty-card-info:** очистка поля ввода проверочного кода при ошибке ([160f56e](https://git.emict.net/ALT/WebApp/commit/160f56e9bd2e28213d6dc4da7de11bc6ad18d823))

### [2.16.8](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.7...msl_main_web-2.16.8) (2024-07-31)


### Bug Fixes

* **loyalty:** правки в функционале для карты лояльности ([494abd7](https://git.emict.net/ALT/WebApp/commit/494abd7fa8e382aab12a959a91f6108fb5aa5a87))

### [2.16.7](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.6...msl_main_web-2.16.7) (2024-07-29)


### Bug Fixes

* **tml:** убран лишний тестовый код ([1e0590e](https://git.emict.net/ALT/WebApp/commit/1e0590e66a62e542b9bb61a809f424c7916a42d0))

### [2.16.6](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.5...msl_main_web-2.16.6) (2024-07-29)

### [2.16.5](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.4...msl_main_web-2.16.5) (2024-07-25)


### Bug Fixes

* **check-information:** правки в странице подтверждения регистрации ставки ([91f4306](https://git.emict.net/ALT/WebApp/commit/91f4306607b61501824860dbe87820deb448380d))

### [2.16.4](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.3...msl_main_web-2.16.4) (2024-07-24)


### Bug Fixes

* **check-information:** правки на странице инфорации о ставке ([cb95d44](https://git.emict.net/ALT/WebApp/commit/cb95d44a942db3859a2b7e64877c2e01fc888ab7))

### [2.16.3](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.2...msl_main_web-2.16.3) (2024-07-23)


### Bug Fixes

* **emit-loyalty-card:** правки в компоненте выдачи карты лояльности ([7dc131d](https://git.emict.net/ALT/WebApp/commit/7dc131d97115f39244750f7e4ba204578ac9ea76))

### [2.16.2](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.16.0...msl_main_web-2.16.2) (2024-07-22)


### Bug Fixes

* **cs-1294:** правки в выдаче карты лояльности ([6587cc6](https://git.emict.net/ALT/WebApp/commit/6587cc66e10f64d581bb26a1b52be6f2a0ab3bc8))

## [2.16.0](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.15.0...msl_main_web-2.16.0) (2024-07-19)


### Features

* **api-client:** добавлен параметр карты лояльности и бонусов в запрос регистрации ставки ([c8606d9](https://git.emict.net/ALT/WebApp/commit/c8606d94cbc20e1ec08298424536ec796add3f7d))
* **card-info:** передалана инфа про карту лояльности ([cf342d3](https://git.emict.net/ALT/WebApp/commit/cf342d31f6074d4bf8710d00bb0d43a989824337))
* **check-information:** доработка продажи лотерей ([845c9e3](https://git.emict.net/ALT/WebApp/commit/845c9e3d49cb888d157a7982a3bad23d4ee24895))
* **cs-1294:** промежуточное сохранение ([ea6f4fa](https://git.emict.net/ALT/WebApp/commit/ea6f4fad64afabc509296c2062320eec91f455c4))
* **cs-1294:** релизован функционал для работы с картами лояльности ([948848b](https://git.emict.net/ALT/WebApp/commit/948848b60bee7177bedfde56b26ed89a59b96f78))
* **design:** https://msl.atlassian.net/browse/CS-1294 ([d06ea0f](https://git.emict.net/ALT/WebApp/commit/d06ea0ff520c78c247307419741f09fda84ac1c1))
* **esap:** добавлен action AuthPhone ([4c1767a](https://git.emict.net/ALT/WebApp/commit/4c1767a2b05aed145cf7b4aaaa524891b484256a))
* **https://msl.atlassian.net/browse/cs-1294:** дизайн экранов выдачи карты лояльности ([93fae26](https://git.emict.net/ALT/WebApp/commit/93fae2649b3c6a833400b6f5ded66fb6e3387c63))
* **https://msl.atlassian.net/browse/cs-1294:** добавлены вызовы новых API в компонент ([092ff11](https://git.emict.net/ALT/WebApp/commit/092ff1153102a783977b5e655b507be2c6f7dc33))
* **https://msl.atlassian.net/browse/cs-1294:** доработка страницы подтверждения регистрации ставки ([c06a026](https://git.emict.net/ALT/WebApp/commit/c06a026e07a4aacefc7bf5ff79c6db452d4bc24e))
* **i18n:** добавлены переводы строк ([b1126f0](https://git.emict.net/ALT/WebApp/commit/b1126f07a8ae7a9f3734239b4aa31608f7f775cc))
* **loyalty-card-info:** реализована печать квитанции, промежуточное сохранение ([3c3edf7](https://git.emict.net/ALT/WebApp/commit/3c3edf7fac9223da565c5302c4fa18a099c3edb3))
* **loyalty:** добавлен функционал вывода бонусов на ТП ([c79f64b](https://git.emict.net/ALT/WebApp/commit/c79f64b76b9f39642b8382cfe23ab5e43b6c67f8))
* **loyalty:** доработана камера и некоторые экшены ([698d190](https://git.emict.net/ALT/WebApp/commit/698d1905c89f0934079ef8e67f8e2950c0d5074d))
* **loyalty:** промежуточное сохранение ([dfba0dc](https://git.emict.net/ALT/WebApp/commit/dfba0dc06ddca86a2f00b51dfa089031ce3cf105))
* **loyalty:** промежуточное сохранение ([19171ab](https://git.emict.net/ALT/WebApp/commit/19171abce94c0830d697a412c78fae0cb75a2c2c))
* **loyalty:** промежуточное сохранение ([cbfd7bd](https://git.emict.net/ALT/WebApp/commit/cbfd7bd403f727a4068d6ff5dab23c7a881610d9))
* **loyalty:** промежуточное сохранение ([215613e](https://git.emict.net/ALT/WebApp/commit/215613ebc1bce3c5877eca3a2b2c682cf004990d))
* **loyalty:** промежуточное сохранение ([ad4e8c3](https://git.emict.net/ALT/WebApp/commit/ad4e8c338fda1b86c3224cb351b2d741afc145f7))
* **other:** добавлен активный челлендж ([6e697ff](https://git.emict.net/ALT/WebApp/commit/6e697ff116c7041d459e5376e99c094ff8a1b60c))
* **receipts:** реализована печать 2-х квитанций ([a722d9a](https://git.emict.net/ALT/WebApp/commit/a722d9a0a3fa3ee3760b1ec8c94809f10de2623c))
* **tickets-check:** доделана возможность выплаты выигрыша на карту лояльности ([6ebce77](https://git.emict.net/ALT/WebApp/commit/6ebce77a0d222475caca067771a550585f771248))
* **tickets-check:** промежуточное сохранение ([fe62c30](https://git.emict.net/ALT/WebApp/commit/fe62c30eff6241367ef5d1bfcf3e54f34009b3de))
* **user-loyalty:** промежуточное сохранение ([df295e9](https://git.emict.net/ALT/WebApp/commit/df295e9fa35d7d3562dff9d82c9c80f301fbf4e3))


### Bug Fixes

* **loyalty:** правки в верстке ([3c5804a](https://git.emict.net/ALT/WebApp/commit/3c5804a34e596b73ae24fc69e690da9fca793d33))

## [2.15.0](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.11...msl_main_web-2.15.0) (2024-06-03)


### Features

* **browserprint:** переделана печать билетов, к запросу картинки нужно добавлять заголовок ([f9ef09b](https://git.emict.net/ALT/WebApp/commit/f9ef09b76921aaecc62a9f14a47999a8035b0699))
* **browserprint:** реализован расходный чек ([416cd49](https://git.emict.net/ALT/WebApp/commit/416cd49a5e505c4d61e36eed72e2e4c43cb7aa4e))
* **browserprint:** реализована печать билетов, а также фискальных и расходных чеков, через браузер ([10d93c5](https://git.emict.net/ALT/WebApp/commit/10d93c5c839d28bc14fbf8a4be4e001f98a90079))
* **fiscal_check:** промежуточное сохранение ([c049796](https://git.emict.net/ALT/WebApp/commit/c0497960529ce37d247b312aa23900169cebd1c4))
* **fiscal_check:** реализована печать фискального чека ([d652255](https://git.emict.net/ALT/WebApp/commit/d652255bf09b14bd180e40412d357eeb2857b98a))
* **fiscal-checks:** промежуточное сохранение ([29a82a7](https://git.emict.net/ALT/WebApp/commit/29a82a797b3331ef482275af8161c73b7f4df425))
* **https://msl.atlassian.net/browse/cs-1365:** добавлен функционал печати через браузер ([6921e9f](https://git.emict.net/ALT/WebApp/commit/6921e9fbfa9dcbc9b309534a14c33b65db4915c6))

### [2.14.11](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.10...msl_main_web-2.14.11) (2024-05-02)


### Bug Fixes

* **scanning:** исправление бага незакрытия сканера ([f9eeb12](https://git.emict.net/ALT/WebApp/commit/f9eeb127e95dc0b917b0f0a9c66ead63b6895c83))

### [2.14.10](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.9...msl_main_web-2.14.10) (2024-04-22)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-1316:** переработан вызов камеры для работы нативного сканера ([aeb416b](https://git.emict.net/ALT/WebApp/commit/aeb416b3fa296d3254be3083d10d9e11b43e269d))

### [2.14.9](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.5...msl_main_web-2.14.9) (2024-03-04)


### Bug Fixes

* **cs-1264, cs-1265:** cS-1264, CS-1265 ([84a7d2f](https://git.emict.net/ALT/WebApp/commit/84a7d2f2d21318d1a2bdbd56f0701aa9dab9e1bc))
* **cs-1264, cs-1265:** cS-1264, CS-1265 ([08f93b2](https://git.emict.net/ALT/WebApp/commit/08f93b29f7a75677df32da1c97deeb5c361537b3))
* **https://msl.atlassian.net/browse/cs-1264:** cS-1264, CS-1265 ([4a6a5bf](https://git.emict.net/ALT/WebApp/commit/4a6a5bf1135447d6f74c1b3f5368dba8a747acae))

### [2.14.7](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.5...msl_main_web-2.14.7) (2024-03-04)


### Bug Fixes

* **cs-1264, cs-1265:** cS-1264, CS-1265 ([08f93b2](https://git.emict.net/ALT/WebApp/commit/08f93b29f7a75677df32da1c97deeb5c361537b3))
* **https://msl.atlassian.net/browse/cs-1264:** cS-1264, CS-1265 ([4a6a5bf](https://git.emict.net/ALT/WebApp/commit/4a6a5bf1135447d6f74c1b3f5368dba8a747acae))

### [2.14.6](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.5...msl_main_web-2.14.6) (2024-03-04)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-1264:** cS-1264, CS-1265 ([4a6a5bf](https://git.emict.net/ALT/WebApp/commit/4a6a5bf1135447d6f74c1b3f5368dba8a747acae))

### [2.14.5](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.4...msl_main_web-2.14.5) (2024-02-15)


### Bug Fixes

* **tickets-check:** исправлен баг в проверке билетов, связаный с дублированием ввода при фокусе поля ([9ea697c](https://git.emict.net/ALT/WebApp/commit/9ea697c4129b5c4e4a02f4f4888efa1a5e3e4c32))

### [2.14.4](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.3...msl_main_web-2.14.4) (2024-02-13)


### Bug Fixes

* **ticket-check:** возможность сосканировать штрих-код аппаратным сканером при открытой камере ([b571956](https://git.emict.net/ALT/WebApp/commit/b5719569e00838059f2bc3603f96218d1337de7c))

### [2.14.3](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.2...msl_main_web-2.14.3) (2024-02-12)


### Bug Fixes

* **win_tickets_details:** обработка строкового win_allow в win_tickets_details ([7cae54c](https://git.emict.net/ALT/WebApp/commit/7cae54cedc456c0a0b717053cf776959acb93fad))

### [2.14.2](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.1...msl_main_web-2.14.2) (2024-02-09)


### Features

* **tickets-check:** скрытие фокуса в проверке билетов при первом заходе на страницу ([9237bb4](https://git.emict.net/ALT/WebApp/commit/9237bb4cb5972fd37bbdeff1efae8a4f20188a08))

### [2.14.1](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.14.0...msl_main_web-2.14.1) (2023-12-17)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-1162:** исправлена дата начала розыгрыша тиража в Лото-Забаве ([08a31e4](https://git.emict.net/ALT/WebApp/commit/08a31e4d2026687e1f565a52e1306d4f63ffdbb5))

## [2.14.0](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.38...msl_main_web-2.14.0) (2023-11-30)


### Features

* **https://msl.atlassian.net/browse/cs-1152:** scandit заменен на бесплатный аналог - HTML5 QRCode ([e925d63](https://git.emict.net/ALT/WebApp/commit/e925d63551974ee6c678fd60be86a4d69a2fcd2b))

### [2.13.38](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.37...msl_main_web-2.13.38) (2023-11-30)

### [2.13.37](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.36...msl_main_web-2.13.37) (2023-11-30)


### Features

* **html5-qrcode:** промежуточное сохранение ([386f389](https://git.emict.net/ALT/WebApp/commit/386f389ada42076114796f067196c439569400f7))
* **html5-qrcode:** сканер Scandit заменен на бесплатный аналог - HTML5 QRCode ([673795a](https://git.emict.net/ALT/WebApp/commit/673795a1b6ea7e61be2f2e669fc928084bca6905))
* **https://msl.atlassian.net/browse/cs-1152:** scandit заменен на бесплатный аналог - HTML5 QRCode ([48f1ed5](https://git.emict.net/ALT/WebApp/commit/48f1ed5d4d6fca4599098bdd318c6569210277b4))
* **tickets/check:** html5-qrcode ([bbd737d](https://git.emict.net/ALT/WebApp/commit/bbd737dae4b9b3ec4c5f00c5933ae407779beafc))

### [2.13.36](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.35...msl_main_web-2.13.36) (2023-09-19)


### Bug Fixes

* **раздел "другое":** восстановлено отображение полей в форме запроса при выборе операции из меню ([5c7f46d](https://git.emict.net/ALT/WebApp/commit/5c7f46d303e63d761aa0e77211c8b96df350d29b))

### [2.13.35](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.34...msl_main_web-2.13.35) (2023-08-28)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-995:** исправление формирования квитанции ([acd148c](https://git.emict.net/ALT/WebApp/commit/acd148c7cccea20ad74d9698a77e165dfef2ee56))

### [2.13.34](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.33...msl_main_web-2.13.34) (2023-08-28)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-995:** исправление формирования квитанции ([35d39ef](https://git.emict.net/ALT/WebApp/commit/35d39ef42137d8aafac1188a529c14665b29cda1))

### [2.13.33](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.32...msl_main_web-2.13.33) (2023-08-28)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-995:** исправление формирования квитанции ([a9fe15b](https://git.emict.net/ALT/WebApp/commit/a9fe15b01c3f75d543431b3177ec319141922572))

### [2.13.32](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.31...msl_main_web-2.13.32) (2023-08-28)


### Features

* **https://msl.atlassian.net/browse/cs-995:** формирование квитанции по шаблону для узкого принтера ([12d7aa4](https://git.emict.net/ALT/WebApp/commit/12d7aa4e382577282720b135819bdeda4887b78e))

### [2.13.31](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.30...msl_main_web-2.13.31) (2023-08-25)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-995:** форматирование квитанции ([891496c](https://git.emict.net/ALT/WebApp/commit/891496ceb118c6212b6b621d5f977ee9246f3fcd))

### [2.13.30](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.29...msl_main_web-2.13.30) (2023-08-25)


### Features

* **https://msl.atlassian.net/browse/cs-995:** попытка перенести на новую строку ([7903265](https://git.emict.net/ALT/WebApp/commit/790326566f0df76ba4196da2556cd92a2451db2d))

### [2.13.29](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.28...msl_main_web-2.13.29) (2023-08-25)


### Features

* **https://msl.atlassian.net/browse/cs-995:** попытка выровнять по правому краю ([23fa108](https://git.emict.net/ALT/WebApp/commit/23fa108eae25eac63c9ecbc7c0c1c6a326c0af8e))

### [2.13.28](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.27...msl_main_web-2.13.28) (2023-08-24)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-995:** выравнивание даты и время по правому краю ([7b21410](https://git.emict.net/ALT/WebApp/commit/7b214100d8a35a82ba2987c984b849bb8f700204))

### [2.13.27](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.26...msl_main_web-2.13.27) (2023-08-24)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-993:** попытка [#3](https://git.emict.net/ALT/WebApp/issues/3) исправить печать на Sunmi ([2d3e4b1](https://git.emict.net/ALT/WebApp/commit/2d3e4b13e68ce461a9e2d61a41e3c60c6342915d))

### [2.13.26](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.25...msl_main_web-2.13.26) (2023-08-23)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-993:** попытка №2 исправить печать ([d25f606](https://git.emict.net/ALT/WebApp/commit/d25f606c164192019d49c7d6461bbab9f04054dd))

### [2.13.25](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.24...msl_main_web-2.13.25) (2023-08-23)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-993:** попытка исправить печать ([fa8fbb8](https://git.emict.net/ALT/WebApp/commit/fa8fbb83109cee6c1b6d3da516ae5eb255f4ed47))

### [2.13.24](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.23...msl_main_web-2.13.24) (2023-08-23)

### [2.13.23](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.22...msl_main_web-2.13.23) (2023-08-22)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-987:** исправление печати квитанции бланка на узких принтерах ([2b8ad3d](https://git.emict.net/ALT/WebApp/commit/2b8ad3d1d817ff6a6ff77550e9166811c8ab9062))

### [2.13.22](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.21...msl_main_web-2.13.22) (2023-08-20)


### Bug Fixes

* **лото-забава:** исправлен переход на результаты Лото-Забавы из списка лотерей в результатах ([babd596](https://git.emict.net/ALT/WebApp/commit/babd596501ef6a3e9cc9787050f210d61e1e16d6))

### [2.13.21](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.20...msl_main_web-2.13.21) (2023-08-16)


### Features

* **https://msl.atlassian.net/browse/cs-954:** удаление штрих-кода бланка из инпута после ошибки ([9cbf67f](https://git.emict.net/ALT/WebApp/commit/9cbf67f4f7830b2dd7229df9c75e3165916cb0f7))

### [2.13.20](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.19...msl_main_web-2.13.20) (2023-08-11)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** при проверке бланков суммы выигрышей отображены в грн ([04b53da](https://git.emict.net/ALT/WebApp/commit/04b53da74eda8d2a1a93b8d1a89437abb51b612c))

### [2.13.19](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.18...msl_main_web-2.13.19) (2023-08-10)

### [2.13.18](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.16...msl_main_web-2.13.18) (2023-08-10)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-934:** попытка устранить задваивание штрихкода ([06d489c](https://git.emict.net/ALT/WebApp/commit/06d489c98e22a48d05273e4708ca1a68e274d930))

### [2.13.16](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.15...msl_main_web-2.13.16) (2023-08-08)


### Bug Fixes

* **cs-912, cs-914:** исправление ошибки TRS и скрытие клавиатуры при сканировании ручным сканером ([ea69c29](https://git.emict.net/ALT/WebApp/commit/ea69c293645904666b459f26c170e2b943126a74))

### [2.13.15](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.14...msl_main_web-2.13.15) (2023-08-08)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-925:** исправление неправильной суммы на странице регистрации ([0d1a08e](https://git.emict.net/ALT/WebApp/commit/0d1a08e749771ca4fda878218478771462c21333))

### [2.13.14](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.13...msl_main_web-2.13.14) (2023-08-07)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-914:** фокус на поле ввода после ошибки проверки бланка ([85256d0](https://git.emict.net/ALT/WebApp/commit/85256d00f64ac13b2b9f350cbe1cdddfc3244639))

### [2.13.13](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.12...msl_main_web-2.13.13) (2023-08-07)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-913:** при продаже бланков отключена ручная отмена ([e327c98](https://git.emict.net/ALT/WebApp/commit/e327c9829059020632667d471d98757699c81ef5))

### [2.13.12](https://git.emict.net/ALT/WebApp/compare/msl_main_web-0.0.1...msl_main_web-2.13.12) (2023-08-07)

### [0.0.1](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.11...msl_main_web-0.0.1) (2023-08-07)

### [2.13.11](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.10...msl_main_web-2.13.11) (2023-08-07)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-915:** исправление поведения курсора при сканировании ([cac4c14](https://git.emict.net/ALT/WebApp/commit/cac4c142ae2d2e48ff2b4518539ca1b3da0c226f))

### [2.13.10](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.9...msl_main_web-2.13.10) (2023-08-03)


### Features

* **https://msl.atlassian.net/browse/cs-887:** при регистрации бланков ЛЗ печаталается квитанция ([b06130b](https://git.emict.net/ALT/WebApp/commit/b06130b1f2045b801da54fea24c1151cb3f39d60))

### [2.13.9](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.8...msl_main_web-2.13.9) (2023-08-02)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** добавлен сброс количества ставок при выходе из ЛЗ ([e9602c3](https://git.emict.net/ALT/WebApp/commit/e9602c358a4ed7289daa9b4e7edde58d033fd025))

### [2.13.8](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.7...msl_main_web-2.13.8) (2023-08-02)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** исправлен баг с отправкой на принтер и режимом ставок ([e12cdaf](https://git.emict.net/ALT/WebApp/commit/e12cdaf07c2a4b043b0328bd984e47d0dd084c00))

### [2.13.7](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.6...msl_main_web-2.13.7) (2023-08-02)


### Features

* **https://msl.atlassian.net/browse/cs-887:** таблица подробной инфы о бланке в проверке билетов ([a2c7b4d](https://git.emict.net/ALT/WebApp/commit/a2c7b4de2cf1d906b4b31509714bf686b1c993c2))

### [2.13.6](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.5...msl_main_web-2.13.6) (2023-08-01)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** исправлен баг неправильного режима продажи ЛЗ ([a4e1e2f](https://git.emict.net/ALT/WebApp/commit/a4e1e2f9155be8430b057ca4c805965865ae179d))

### [2.13.5](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.4...msl_main_web-2.13.5) (2023-07-31)

### [2.13.4](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.3...msl_main_web-2.13.4) (2023-07-31)


### Features

* **https://msl.atlassian.net/browse/cs-887:** снятие фокуса с поля ввода при открытии камеры в ЛЗ ([808d387](https://git.emict.net/ALT/WebApp/commit/808d387d375bbdf8a27d68689c1929ea293fa562))

### [2.13.3](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.2...msl_main_web-2.13.3) (2023-07-31)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** в таблице маккодов бланков зафиксирован хедер ([e9df863](https://git.emict.net/ALT/WebApp/commit/e9df8636cbd40e22597dd71f43007c4588ddbaf0))

### [2.13.2](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.1...msl_main_web-2.13.2) (2023-07-31)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** добавлен параметр CLIENT_ID в запрос GetBlankInfo ([7898922](https://git.emict.net/ALT/WebApp/commit/78989224a6956892660030fbb51b59e6fe276e56))

### [2.13.1](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.13.0...msl_main_web-2.13.1) (2023-07-30)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-887:** правки в мобильной верстке в ЛЗ ([3834be3](https://git.emict.net/ALT/WebApp/commit/3834be38e77e12f0cee5f7d0035cb499d637ab5f))

## [2.13.0](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.10...msl_main_web-2.13.0) (2023-07-30)


### Features

* **лз: сканирование бланков:** дизайн страниці сканирования бланков ([f597b6b](https://git.emict.net/ALT/WebApp/commit/f597b6bd6a988e61b81eb85628a073398519967c))
* **лз: сканирование бланков:** добавлена возможность покупки с помощью бланков ([6513580](https://git.emict.net/ALT/WebApp/commit/65135802f2371eee8b06d82c14f3eda3c5dea4d9))
* **лз: сканирование бланков:** добавлена камера ([0777879](https://git.emict.net/ALT/WebApp/commit/0777879d223fe564bd5c781686914af69be7f360))
* **лз: сканирование бланков:** разработка функционала ([93aff2f](https://git.emict.net/ALT/WebApp/commit/93aff2ff80668f3673914e83ea3043fdca1039b1))
* **сканирование типографских бланков:** начало разработки функционала сканирования типогр. бланков ([c6ec0e0](https://git.emict.net/ALT/WebApp/commit/c6ec0e01e99e9aa54ab02050ce222028d15839cf))
* **https://msl.atlassian.net/browse/cs-887:** пробная версия для проверки продажи бланков в ЛЗ ([c55cbbb](https://git.emict.net/ALT/WebApp/commit/c55cbbbdc95e7ddacf789143018cbfbe6c5dbaa7))


### Bug Fixes

* **лз: сканирование бланков:** исправлено сохранение параметров при переходах от страницы к странице ([f34c165](https://git.emict.net/ALT/WebApp/commit/f34c165b1773231c8eac44c25b62d5c889cdb27c))
* **лз: сканирование бланков:** подправлены стили и запрет повторного добавления штрих-кода ([65ad1f4](https://git.emict.net/ALT/WebApp/commit/65ad1f462b8eae82c08fdf4d2530cd0cd7043dd0))
* **https://msl.atlassian.net/browse/cs-878:** исправлена неопределенная ошибка на этапе авторизации ([b8b79f6](https://git.emict.net/ALT/WebApp/commit/b8b79f6f8bab607e3a91fe9f6b566620be06d74f))
* **https://msl.atlassian.net/browse/cs-878:** исправлена ошибка при выборе точки продаж ([f5e4154](https://git.emict.net/ALT/WebApp/commit/f5e4154b72fef20910ebbd5bcefd1cb6b1d3f281))

### [2.12.15](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.14...msl_main_web-2.12.15) (2023-07-24)

### [2.12.14](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.13...msl_main_web-2.12.14) (2023-07-24)

### [2.12.13](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.12...msl_main_web-2.12.13) (2023-07-24)

### [2.12.12](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.11...msl_main_web-2.12.12) (2023-07-24)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-878:** исправлена ошибка при выборе точки продаж ([f5e4154](https://git.emict.net/ALT/WebApp/commit/f5e4154b72fef20910ebbd5bcefd1cb6b1d3f281))

### [2.12.11](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.10...msl_main_web-2.12.11) (2023-07-24)


### Bug Fixes

* **https://msl.atlassian.net/browse/cs-878:** исправлена неопределенная ошибка на этапе авторизации ([b8b79f6](https://git.emict.net/ALT/WebApp/commit/b8b79f6f8bab607e3a91fe9f6b566620be06d74f))

### [2.12.10](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.9...msl_main_web-2.12.10) (2023-01-24)

### [2.12.9](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.8...msl_main_web-2.12.9) (2022-05-30)

### [2.12.8](https://git.emict.net/ALT/WebApp/compare/msl_main_web-2.12.7...msl_main_web-2.12.8) (2022-05-24)


### Features

* **https://msl.atlassian.net/browse/cs-464:** установлены пакеты commitizen и standard-version ([50fbce6](https://git.emict.net/ALT/WebApp/commit/50fbce64a26ed7a13e5577d428a5b7f8eb1cc334))
